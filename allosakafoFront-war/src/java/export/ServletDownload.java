package export;

import java.io.*;
import java.util.Date;
import java.text.SimpleDateFormat;
import bean.ClassMAPTable;
import java.lang.reflect.Field;
import bean.CGenUtil;
import affichage.TableauRecherche;
import bean.ListeColonneTable;
import com.itextpdf.text.Document;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.FileOutputStream;
//import org.xhtmlrenderer.pdf.ITextRenderer;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Ravaka
 */
@WebServlet(name = "ServletDownload", urlPatterns = {"/download"})
public class ServletDownload extends HttpServlet {

    protected void doPost(HttpServletRequest arg0, HttpServletResponse arg1)
            throws ServletException, IOException {
        OutputStream os = arg1.getOutputStream();
        try {
            String ext = arg0.getParameter("ext");
            String awhere = arg0.getParameter("awhere");
            if (awhere == null) {
                awhere = "";
            }
            arg1.setContentType("text/plain");
            SimpleDateFormat fd = new SimpleDateFormat("yyyy-MM-dd");
            String d = fd.format(new Date());
            String fileName = "export-" + d + "." + ext;
            arg1.setHeader("Content-Disposition", "attachment;filename=" + fileName);

            String type = arg0.getParameter("donnee");

            if (type.compareTo("0") == 0) {
                if (ext.compareTo("xls") == 0) {
                    arg1.setContentType("application/vnd.ms-excel");
//                    System.out.println("ARG0 = "+arg0.getParameter("table"));
                    os.write(arg0.getParameter("table").replace('*', '"').getBytes());
                }
                if (ext.compareTo("pdf") == 0) {

                    arg1.setContentType("application/pdf");
                    //arg1.setHeader("Content-disposition", "attachment; filename=fichepersonne.pdf");
                    File fichier = new File(getServletContext().getRealPath("")+fileName);
                    String htmlString = "<html><head><title>PDF - output</title>"
                            + "</head><body>" + arg0.getParameter("table").replace('*', '"') + "</body></html>";
//                    ITextRenderer renderer = new ITextRenderer();
//                    renderer.setDocumentFromString(htmlString);
//                    renderer.layout();
//
//                    FileOutputStream fos = new FileOutputStream(fileNameWithPath);
//                    renderer.createPDF(fos);
//                    fos.close();

                    String fileNameWithPath = getServletContext().getRealPath("")+fileName;
                    OutputStream file = new FileOutputStream(new File(fileNameWithPath));
                    Document document = new Document();
                    try {
                        PdfWriter.getInstance(document, file);
                        document.open();
                        HTMLWorker htmlWorker = new HTMLWorker(document);
                        htmlWorker.parse(new StringReader(htmlString));
                        document.close();
                        file.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    System.out.println("PDF Created!");

                    // This should send the file to browser
                    OutputStream out = arg1.getOutputStream();
                    FileInputStream in = new FileInputStream(fileNameWithPath);

                    byte[] buffer = new byte[4096];
                    int length;
                    while ((length = in.read(buffer)) > 0) {
                        out.write(buffer, 0, length);
                    }
                    in.close();
                    out.flush();
                    fichier.delete();
                } else {
                    String donnee = arg0.getParameter(ext);
                    //System.out.println(donnee);
                    if(donnee!=null) donnee = donnee.replace('*', '"');
                    os.write(donnee.getBytes());
                    os.flush();
                }

            } else if (type.compareTo("1") == 0) {
                System.out.println("arg0.getSession().getAttribute(\"critere\") = " + arg0.getSession().getAttribute("critere"));
                ClassMAPTable o = (ClassMAPTable) arg0.getSession().getAttribute("critere");
                System.out.println(""+o.getClassName());

                Field[] field = ListeColonneTable.getFieldListeHeritage(o);
                String entete[] = new String[field.length];
                for (int i = 0; i < entete.length; i++) {
                    entete[i] = field[i].getName();
                    //System.out.println(entete[i]);
                }

                ClassMAPTable[] c = (ClassMAPTable[]) CGenUtil.rechercher(o, null, null, awhere);
                System.out.println("c.length:"+c.length);
                TableauRecherche tr = new TableauRecherche(c, entete);
                tr.makeHtml();
                //System.out.println(donn);
                if (ext.compareToIgnoreCase("xml") == 0) {
                    os.write(tr.getExpxml().getBytes());
                    //System.out.println(donn+tr.getExpxml());
                } else {
                    if (ext.compareToIgnoreCase("csv") == 0) {
                        os.write(tr.getExpcsv().getBytes());
                        //System.out.println(donn+tr.getExpcsv());
                    } else {
                        if (ext.compareToIgnoreCase("xls") == 0) {
                            arg1.setContentType("application/vnd.ms-excel");
                            //arg1.setHeader("Content-Disposition","attachment;filename=export-"+d+"."+ext);
                            os.write(tr.getHtml().getBytes());
                        }
                    }
                }
                os.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            os.close();
        }
    }
    /* private void performTask(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException 
     {

     File pdfFile = new File(fileName);

     response.setContentType("application/pdf");
     response.setContentLength((int) pdfFile.length());

     FileInputStream fileInputStream = new FileInputStream(pdfFile);
     OutputStream responseOutputStream = response.getOutputStream();
     int bytes;
     while ((bytes = fileInputStream.read()) != -1) {
     responseOutputStream.write(bytes);
     }
     }*/

    protected void doGet(HttpServletRequest arg0, HttpServletResponse arg1)
            throws ServletException, IOException {
    }
}
