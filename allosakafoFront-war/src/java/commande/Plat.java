/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commande;
import mg.allosakafo.commande.CommandeService;
import mg.allosakafo.commande.CommandeClientTotal;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mg.allosakafo.livraison.CommandeLivraison;
import user.UserEJB;
import utilitaire.UtilDB;

/**
 *
 * @author Jessi
 */
@WebServlet(name = "Plat", urlPatterns = {"/Plat"})
public class Plat extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        PrintWriter out = response.getWriter();
        Connection c=null;
        try{
            CommandeService cm = new CommandeService();
            c=new UtilDB().GetConn();
            c.setAutoCommit(false);
            if(request.getParameter("mode")!=null){
                if(request.getParameter("mode").compareTo("modif")==0){
                     cm.updatePlat(Integer.valueOf(request.getParameter("qt")),request.getParameter("id"),request.getParameter("idsauce"),null);
                     response.sendRedirect("home.jsp?but=pages/menu/liste-plat.jsp");
                }
                else if(request.getParameter("mode").compareTo("delete")==0){
                     cm.deletePlat(request.getParameter("id"));
                     response.sendRedirect("home.jsp?but=pages/menu/liste-plat.jsp");
                }
                else if(request.getParameter("mode").compareTo("cloturer")==0){
                    String[] mere = request.getParameter("id").split(";");
                    //ArrayList<String> idmere =(ArrayList<String>)Arrays.asList(mere); 
                    HttpSession session = request.getSession();
                    CommandeService.cloturerCommandeMobileLivraison(mere,(String)session.getAttribute("dateClot"),(String)session.getAttribute("heureClot"),(String)request.getSession().getAttribute("typeLivraison"),null);
                    session.removeAttribute("tableClient");
                    session.removeAttribute("typeLivraison");
                    session.removeAttribute("idlivraison");
                    session.removeAttribute("idResa");
                    response.sendRedirect("index.jsp?but=pages/choixresto.jsp");
                    //out.println("<script language='JavaScript'> document.location.replace('index.jsp?but=pages/choixTable.jsp'); </script>");
                }
                else if(request.getParameter("mode").compareTo("cloturerPayer")==0){
                    String[] mere = request.getParameter("id").split(";");
                    //ArrayList<String> idmere =(ArrayList<String>)Arrays.asList(mere); 
                    HttpSession session = request.getSession();
                    CommandeService.cloturerCommandeMobileLivraison(mere,(String)session.getAttribute("dateClot"),(String)session.getAttribute("heureClot"),(String)request.getSession().getAttribute("typeLivraison"),c);
                    UserEJB u = (UserEJB) session.getAttribute("u");
                    u.savePaiementCommande(mere[0], c);
                    session.removeAttribute("tableClient");
                    session.removeAttribute("typeLivraison");
                    session.removeAttribute("idlivraison");
                    session.removeAttribute("idResa");
                    response.sendRedirect("index.jsp?but=pages/choixresto.jsp");
                    //out.println("<script language='JavaScript'> document.location.replace('index.jsp?but=pages/choixTable.jsp'); </script>");
                }
            }
            else{
                String user = (String)request.getSession().getAttribute("tableClient");
                String resto = (String)request.getSession().getAttribute("restaurant");
                if(user!=null){
                    if(request.getSession().getAttribute("idResa")!=null&&request.getSession().getAttribute("idResa").toString().compareToIgnoreCase("")!=0)user=user+"--"+request.getSession().getAttribute("idResa");
                    CommandeClientTotal commandeclient = CommandeService.ajouterCommandeMobileLivraison(request.getParameter("produit"),user,Double.valueOf(request.getParameter("qt")),request.getParameter("idsauce"),request.getParameter("remarque"),resto,(String)request.getSession().getAttribute("idlivraison"),(String)request.getSession().getAttribute("choixresto"),null);
                  
                    if(commandeclient!=null){
                        //response.sendRedirect("home.jsp?but=pages/menu/liste-plat.jsp");
                        out.println("<script language='JavaScript'> document.location.replace('home.jsp?but=pages/menu/liste-plat.jsp'); </script>");
                    }
                    else{
                        out.println("<script language='JavaScript'> document.location.replace('index.jsp'); </script>");
                    }
                }
                else{
                    out.println("<script language='JavaScript'> document.location.replace('index.jsp'); </script>");
                }
                
            }
            c.commit();
            
        }
            
        catch(Exception ex){
            try
            {
            c.rollback();
            ex.printStackTrace();
            out.println("<script language='JavaScript'> alert('"+ex.getMessage()+"'); history.back();</script>");
            //throw new ServletException(ex.getMessage());
            }
            catch(Exception e)
            {
                
            }
        }
        finally
        {
            try{
            if(c!=null)c.close();
            }
            catch(Exception e)
            {
                
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
