/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import affichage.PageRecherche;
import bean.CGenUtil;
import bean.ClassMAPTable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utilitaire.Utilitaire;


/**
 *
 * @author Tahiry
 */
@WebServlet(name = "GenericWS", urlPatterns = {"/GenericWS"})
public class GenericWS extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String apresWhere = "";
        Gson basic_gson = new Gson();
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Data json = null;
        ClassMAPTable[] data = null;
        Error error = new Error(); 
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String classname = request.getParameter("classname");
            String tablename = request.getParameter("tablename");
            String criteria = request.getParameter("criteria");
            String afterWhere = request.getParameter("afterwhere");
            String nbParPage = request.getParameter("npp");
            String page = request.getParameter("page");
            if(afterWhere != null) afterWhere = " "+afterWhere;
            if(page != null){ 
                int numdebut = Integer.valueOf(nbParPage)*(Integer.valueOf(page) - 1);
                numdebut++;
                afterWhere += " limit "+nbParPage+" offset "+numdebut;
            }
            if(classname != null){
                Object obj = Class.forName(classname).newInstance();
                try{
                    ClassMAPTable cmt = (ClassMAPTable)obj;
                    if(criteria != null){
                        ClassMAPTable crit = basic_gson.fromJson(criteria, (java.lang.reflect.Type)obj.getClass());
                        Field[] fields = crit.getClass().getDeclaredFields();
                        String[] listCrt = new String[fields.length];
                        int i = 0;
                        for(Field field : fields){
                            listCrt[i] = field.getName();
                            i++;
                        }
                        apresWhere = Utilitaire.replaceDateApresWhere(crit);
                        data = (ClassMAPTable[])CGenUtil.rechercher(crit, null, null, apresWhere+afterWhere);
                    }else{
                        if(tablename != null){ cmt.setNomTable(tablename); }
                        data = (ClassMAPTable[])CGenUtil.rechercher(cmt, null, null, apresWhere+afterWhere);
                    }
                    System.out.println("GenericWS: Data type: "+classname+", length: "+data.length);
                    json = new Data(data, error);
                    gson.toJson(json, out);
                }catch(ClassNotFoundException exc){
                    json = new Data(null, new Error(1000,"","Info","Classe non valide."));
                    gson.toJson(json, response.getWriter());
                }
            }else{
                json = new Data(null, new Error(1000,"","Info","Parametre(s) manquant(s)."));
                gson.toJson(json, response.getWriter());
            }
        }catch(Exception exc){
            exc.printStackTrace();
            json = new Data(null, new Error(1,"","Exception",exc.getMessage()));
            gson.toJson(json, response.getWriter());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
