/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

/**
 *
 * @author rakotondralambo
 */
public class Champ {
    private String column_name;
    private String data_type;
    private String character_maximum_length;
    private String numeric_precision;

    public Champ(String column_name, String data_type, String character_maximum_length, String numeric_precision) {
        this.setColumn_name(column_name);
        this.setData_type(data_type);
        this.setCharacter_maximum_length(character_maximum_length);
        this.setNumeric_precision(numeric_precision);
    }

    
    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getCharacter_maximum_length() {
        return character_maximum_length;
    }

    public void setCharacter_maximum_length(String character_maximum_length) {
        this.character_maximum_length = character_maximum_length;
    }

    public String getNumeric_precision() {
        return numeric_precision;
    }

    public void setNumeric_precision(String numeric_precision) {
        this.numeric_precision = numeric_precision;
    }
    
    
    
}
