/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import bean.CGenUtil;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.server.ServerEndpointConfig;
import mg.allosakafo.commande.CommandeMobile;
import mg.allosakafo.commande.CommandeMobileDetails;
import utilitaire.UtilDB;
import ws.NewWSEndpoint.WSocketSetting;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import mg.allosakafo.commande.CommandeData;

/**
 *
 * @author HP
 */
@ServerEndpoint(value = "/endpoint", configurator = WSocketSetting.class)
public class NewWSEndpoint {
    ArrayList<Session> sessions = new ArrayList();
    Gson gsonS = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    
    public void sendMessage(HashMap<String,String> message){
        System.out.println("Send......"+message);
        try {
            for(int i=0;i<sessions.size();i++)
                sessions.get(i).getBasicRemote().sendText(gsonS.toJson(message));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void loadData(){
        try {
            CommandeData[] datas = (CommandeData[]) CGenUtil.rechercher(new CommandeData(), null, null, null, "");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    
    @OnOpen
    public void onOpen(Session session) {
        sessions.add(session);
        System.out.println("onOpen::" + session.getId());    
    }
    
    @OnClose
    public void onClose(Session session) {
        sessions.remove(session);
        System.out.println("onClose::" +  session.getId());
    }
    
    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("onMessage::From=" + session.getId() + " Message=" + message);
        try {
            if(message.compareTo("cmd.finish")==0){
                //Action changer �tat to finish
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @OnError
    public void onError(Throwable t) {
        System.out.println("onError::" + t.getMessage());
    }
    
    public static class WSocketSetting extends ServerEndpointConfig.Configurator {
        public static final NewWSEndpoint endpoint = new NewWSEndpoint();
        
        @Override
        public <T> T getEndpointInstance(Class<T> endpointClass) throws InstantiationException {
            return (T) endpoint;
        }
    }
}
