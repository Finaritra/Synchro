/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import bean.CGenUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mg.allosakafo.commande.CommandeMobile;
import mg.allosakafo.commande.CommandeMobileDetails;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;
import com.google.gson.Gson;
import java.util.HashMap;
import ws.NewWSEndpoint.WSocketSetting;

/**
 *
 * @author HP
 */
@WebServlet(name = "UpdateCommande", urlPatterns = {"/UpdateCommande"})
public class UpdateCommande extends HttpServlet {
private Gson gson = new Gson();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Data json = null;
        Connection c = null;
        try {
            if(c == null) c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            //get Current Table ID if not exist create table
                //Ajouter commande
                CommandeMobileDetails p = new CommandeMobileDetails();
                p.setId(request.getParameter("id"));
                p = (CommandeMobileDetails) CGenUtil.rechercher(p, null, null, c, "")[0];
                p.setQuantite(Utilitaire.stringToDouble(request.getParameter("qtt")));
                p.updateToTable(c);
                
                c.commit();
                HashMap<String,String> message = new HashMap();
                message.put("idDetail", p.getId());
                message.put("plat", p.getProduit());
                message.put("quantiter", ""+p.getQuantite());
                message.put("action", "update");
                
                //WSocketSetting.endpoint.sendMessage(message);
                
                //Return idCommande
            out.print(gson.toJson(p.getId()));
        }catch(Exception e){
            e.printStackTrace();
            json = new Data(null, new Error(1,"","Exception",e.getMessage()));
            out.print(gson.toJson(json));
        }finally{
            try{
                if(c != null) c.close();
            }catch(Exception e){ }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
