/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package livraison;

import bean.CGenUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mg.allosakafo.commande.CommandeClientRestaurantEtat;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author tahina
 */
@WebServlet(name = "CompleterLivraison", urlPatterns = {"/CompleterLivraison"})
public class CompleterLivraison extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)     throws ServletException, IOException 
    {
        response.setContentType("text/html;charset=UTF-8");
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    { 
        try
        {
            processRequest(request, response);
            PrintWriter out = response.getWriter();
            CommandeClientRestaurantEtat lv = new CommandeClientRestaurantEtat();
            String[] phone=utilitaire.Utilitaire.split(request.getParameter("telephone"), "-");
            if(phone.length>0) lv.setResponsable(phone[0]);
            CommandeClientRestaurantEtat[]listeCmd=(CommandeClientRestaurantEtat[])CGenUtil.rechercher(lv, null, null, " order by datecommande desc");
            if(listeCmd.length>0)
            {
                out.println("{\"client\":\"" + phone[0]+"-"+listeCmd[0].getClient() +"\",\"adresse\":\"" + listeCmd[0].getAdresseliv() +"\"}");
                //out.println("{\"client\":\"" + phone[0]+"-"+listeCmd[0].getClient() +"\",\"adresse\":\"" + listeCmd[0].getAdresseliv() +"\",\"id\":\"" + listeCmd[0].getId() +"\"}");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
