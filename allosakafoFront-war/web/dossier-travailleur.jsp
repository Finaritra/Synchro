<%@page import="mg.cnaps.accueil.PiecesRequises"%>
<%@page import="bean.CGenUtil"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.sig.SigTravailleurInfo"%>
<%@page import="mg.cnaps.sig.SigPersonnes"%>
<%@page import="mg.cnaps.accueil.PiecesRecues"%>
<%@page import="mg.cnaps.accueil.Dossiers"%>
<%@page import="mg.cnaps.sig.SigTravailleurs"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.cnaps.accueil.SigDossierSaisie"%>
<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    String idDossier = request.getParameter("idDossier");
    String idTravailleur = request.getParameter("idTravailleur");
    Dossiers dossier = null;
    SigTravailleurInfo travailleur = null;
    PiecesRecues[] piecesRecues = null;
    PiecesRequises[] piecesRequises = null;

    TypeObjet typeDep = new TypeObjet();
    typeDep.setNomTable("SIG_DEPOSITION_DOSSIERS");
    TypeObjet[] listeTypeDep = (TypeObjet[]) CGenUtil.rechercher(typeDep, null, null, "");

    if (idTravailleur != null) {
        SigDossierSaisie sigDossierSaisie = u.chargerDossier(idDossier, idTravailleur);
        travailleur = sigDossierSaisie.getTravailleur();
        dossier = sigDossierSaisie.getDossier();
        piecesRecues = sigDossierSaisie.getListePJ();
        piecesRequises = sigDossierSaisie.getListeRequises();
    }
%>
<% String lien = (String) session.getValue("lien");%>
<div class="content-wrapper">
    <section class="content-header">
        <div>
            <div class="col-sm-6">
                <h4>Reception Standard</h4>
            </div>
            <div class="col-sm-6">
                <a href="<%=lien%>?but=dossier/dossier-accueil.jsp" class="btn btn-primary pull-right">Employeur</a>
                <a href="<%=lien%>?but=dossier/dossier-accueil.jsp" class="btn btn-primary pull-right" style="margin-right: 5px;">Travailleur</a>
            </div>
        </div>
    </section>
    <section class="content">
        <form method="post" action="#" data-parsley-validate>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Information sur le travailleur</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="col-xs-6">
                                    <p>
                                        <strong>Matricule : </strong><a href="#"></a>
                                    </p>
                                    <p><strong>D&eacute;nomination : </strong><%=travailleur.getPers_nom()%></p>
                                    <p><strong>Adresse : </strong><%=travailleur.getPers_adresse_lot()%></p>
                                    <p><strong>RIB : </strong>123456789112345</p>
                                    <p><strong>Mail : </strong>rakoto.jean@gmail.com</p>
                                </div>
                                <div class="col-xs-6">
                                    <p><strong>T&eacute;lephone : </strong>+261320032232</p>
                                    <p><strong>Activit&eacute; : </strong>Banque</p>
                                    <p><strong>R&eacute;gime : </strong>Non specifi&eacute;</p>
                                    <p><strong>Situation : </strong>Celibataire</p>
                                </div>
                            </div>
                        </div>
                        <div class="box box-primary box-solid hide">
                            <div class="box-header with-border">
                                <h3 class="box-title">Nature du dossier</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <input type="text" id="nombretest" type="nombretest" value="411" /> &nbsp; &nbsp; &nbsp; AP &nbsp; &nbsp; &nbsp; Normal
                                </div>
                            </div>
                        </div>
                        <div align="left">
                            <button class="btn btn-primary">Enregistrer</button>
                            <button class="btn btn-primary">Valider</button>
                            <button class="btn btn-danger">Annuler</button>
                            <button type="button" class="btn btn-default">Note de retour</button>
                            <button type="button" class="btn btn-default">Dossier de r&eacute;ception</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">D&eacute;tail</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="numerodossier">Num&eacute;ro dossier</label>
                                        <input type="text" name="numerodossier" id="numerodossier" placeholder="" value="<%=(dossier != null && dossier.getDossier_matricule() != null) ? dossier.getDossier_matricule() : ""%>" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="matricule">Matricule</label>
                                        <input type="text" name="matricule" id="matricule" placeholder="" disabled="" value="<%=(travailleur.getTravailleur_matricule() != null) ? travailleur.getTravailleur_matricule() : ""%>" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="numerotelephone">Num&eacute;ro de t&eacute;l&eacute;phone</label>
                                        <input type="text" name="numerotelephone" id="numerotelephone" value="2613202512" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="datereception">Date de r&eacute;ception</label>
                                        <input type="text" name="datereception" id="datereception" placeholder="" class="form-control" value="<%=utilitaire.Utilitaire.dateDuJourSql()%>"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="naturedudossier">Nature du dossier</label>
                                        <input type="text" name="naturedudossier" id="naturedudossier" placeholder="" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="typedepotdossier">Type de d&eacute;p&ocirc;t de dossier</label>
                                        <select name="typedepotdossier" id="typedepotdossier" class="form-control">
                                            <%for (TypeObjet element : listeTypeDep) {%>
                                            <option value="<%=element.getId()%>" <%if (dossier != null && dossier.getCode_deposition().equals(element.getId())) {
                                                    out.print(" selected");
                                                }%>><%=element.getVal()%></option>
                                            <%}%>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="datesaisie">Date de saisie</label>
                                        <input type="text" name="datesaisie" id="datesaisie" value="<%=utilitaire.Utilitaire.dateDuJourSql()%>" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="lieureception">Lieu de r&eacute;ception</label>
                                        <input type="text" name="lieureception" id="lieureception" placeholder="" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="numbordereau">Numero bordereau</label>
                                        <input type="text" name="numbordereau" id="numbordereau" placeholder="" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label for="priority">Priorit&eacute;</label>
                                        <select class="form-control">
                                            <option value="30">30</option>
                                            <option value="20">20</option>
                                            <option value="10">10</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="observation">Observation</label>
                                    <textarea class="form-control" rows="4" placeholder="" name="observation" id="observation"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary box-solid">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="background-color:#bed1dd">RECU</th>
                                        <th style="background-color:#bed1dd">AUTHENTIFICATION</th>
                                        <th style="background-color:#bed1dd">NB</th>
                                        <th style="background-color:#bed1dd">CODE</th>
                                        <th style="background-color:#bed1dd">LIBELLE PIECE</th>
                                        <th style="background-color:#bed1dd">O/F</th>
                                        <th style="background-color:#bed1dd">OPTIONS</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                    <%if (dossier != null && piecesRecues.length > 0) {
                                            for (PiecesRecues elementPieces : piecesRecues) {%>
                                    <tr>
                                        <td class="td-content-center">
                                            <input type="checkbox" name="recu[]" id="recu"/>
                                        </td>
                                        <td class="td-content-center">
                                            <input type="checkbox" name="authentique[]" id="authentique"/>
                                        </td>
                                        <td>
                                            <input type="text" name="nb[]" id="nb" class="form-control" />
                                        </td>
                                        <td>
                                            <input type="text" name="code[]" id="code" class="form-control" />
                                        </td>
                                        <td>
                                            <input type="text" name="libellepiece[]" id="libellepiece" class="form-control" />
                                        </td>
                                        <td>
                                            <input type="checkbox[]" name="of" id="of"/>
                                        </td>
                                        <td>
                                            <a data-toggle="modal" href="#myModalMessage" onclick="changeValue('<%=elementPieces.getId()%>')"><i class="fa fa-child"></i></a>
                                            <a href="?but=dossier/dossier-numerisation.jsp"><i class="fa fa-search"></i></a>
                                            <a href="?but=dossier/dossier-saisie-piece.jsp"><i class="fa fa-edit"></i></a>
                                            <a href="#supprimer"><i class="fa fa-close"></i></a>
                                        </td>
                                    </tr>
                                    <%}
                                        }
                                        if (dossier != null && piecesRequises != null) {
                                            for (PiecesRequises elementRequises : piecesRequises) {%>
                                    <tr>
                                        <td class="td-content-center">
                                            <input type="checkbox" name="recu[]" id="recu"/>
                                        </td>
                                        <td class="td-content-center">
                                            <input type="checkbox" name="authentique[]" id="authentique"/>
                                        </td>
                                        <td>
                                            <input type="text" name="nb[]" id="nb" class="form-control" />
                                        </td>
                                        <td>
                                            <input type="text" name="code[]" id="code" class="form-control" />
                                        </td>
                                        <td>
                                            <input type="text" name="libellepiece[]" id="libellepiece" class="form-control" />
                                        </td>
                                        <td>
                                            <input type="checkbox" name="of[]" id="of"/>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <%}
                                        }%>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer" align="right">
                            <p>
                                <button type="submit" class="btn btn-default">Ajouter ligne</button>
                                <button type="submit" class="btn btn-default">Enregistrer piece jointe</button>
                                <button type="reset" class="btn btn-default">Annuler</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!-- popup-->
        <div class="modal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div id="popup" class="modal-body">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#enfants" aria-controls="home" role="tab" data-toggle="tab">Enfants</a>
                            </li>
                            <li role="presentation"><a href="#travailleurs" aria-controls="profile" role="tab" data-toggle="tab">Travailleurs</a>
                            </li>
                            <li role="presentation"><a href="#autres" aria-controls="profile" role="tab" data-toggle="tab">Autres</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <!--Enfants-->
                            <div role="tabpanel" class="tab-pane active" id="enfants">
                                <h4 class="modal-title text-center"><i class="fa fa-group"></i> <strong>Liste des Enfants</strong></h4>
                                <div class="box box-info">
                                    <div class="box-body no-padding" id="enfants">
                                        <form action="#" method="post" name="listevillechoix" id="listevillechoix">
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="box" style="border-top: none;">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Rechercher</h3>
                                                            <div class="box-tools pull-right">
                                                                <button class="btn btn-box-tool" data-widget="collapse">
                                                                    <i class="fa fa-minus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="box-body" style="display: block;">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-xs-6">
                                                                            <div class="form-group">
                                                                                <label>Numero ville</label>
                                                                                <input name="id" type="textbox" class="form-control" id="id" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Pays</label>
                                                                                <select name="idpays" class="form-control" id="idpays">
                                                                                    <option value="%">Tous</option>
                                                                                    <option value="PAY000001">Angleterre</option>
                                                                                    <option value="PAY000003">Autre</option>
                                                                                    <option value="PAY000005">Espagne</option>
                                                                                    <option value="PAY000004">Italie</option>
                                                                                    <option value="PAY000002">Madagascar</option>
                                                                                    <option value="PAY0001">Madagascar</option>
                                                                                    <option value="PAY000006">Turquie</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                            <div class="form-group">
                                                                                <label>Valeur</label>
                                                                                <input name="val" type="textbox" class="form-control" id="val" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Description</label>
                                                                                <input name="desce" type="textbox" class="form-control" id="desce" value="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6"></div>
                                                                        <div class="col-xs-6"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="box box-success">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Trier par</h3>
                                                            <div class="box-tools pull-right">
                                                                <button class="btn btn-box-tool" data-widget="collapse">
                                                                    <i class="fa fa-minus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="box-body" style="display: block;">
                                                            <div class="form-group">
                                                                <label>Colonne</label>
                                                                <select name="colonne" class="form-control" id="colonne">
                                                                    <option value="id">id</option>
                                                                    <option value="idpays">idpays</option>
                                                                    <option value="val">val</option>
                                                                    <option value="desce">desce</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Ordre</label>
                                                                <select name="ordre" class="form-control" id="ordre">
                                                                    <option value="DESC">DESC</option>
                                                                    <option value="ASC">ASC</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td align="center">
                                                            <button type="submit" name="Submit" class="btn btn-success"><i class="fa fa-check"></i>
                                                            </button>
                                                        </td>
                                                        <td align="center">
                                                            <button type="reset" name="Submit" class="btn btn-danger"><i class="fa fa-close"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </form>
                                        <p align="center"><strong><u>RECAP</u></strong>
                                        </p>
                                        <table class="table" table-bordered="" width="100%">
                                            <tbody>
                                                <tr class="active" head="">
                                                    <th align="center" valign="top" style="text-align:center"></th>
                                                    <th align="center" valign="top" style="text-align:center">Nombre</th>
                                                </tr>
                                                <tr>
                                                    <td align="center" data-test="0/0">Total </td>
                                                    <td align="center" data-test="0/1">1,00 </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <br/>

                                        <form action="http://localhost:8080/ketrikabo-war/bo/modeleAdmin.jsp?but=apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                                            <input type="hidden" name="champReturn" value="idville">

                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr class="info">
                                                        <th>#</th>
                                                        <th>Numero</th>
                                                        <th>Pays</th>
                                                        <th>Valeur</th>
                                                        <th>Description</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="flat-red" name="choix" id="enfant0" value="VIL0001">
                                                        </td>
                                                        <td align="center" id="numeroEnfant0">VIL0001</td>
                                                        <td align="center" id="paysEnfant0">PAY0001</td>
                                                        <td align="center" id="valeurEnfant0">Antananarivo</td>
                                                        <td align="center" id="descriptionEnfant0">Antananarivo</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="flat-red" name="choix" id="enfant1" value="VIL0002">
                                                        </td>
                                                        <td align="center" id="numeroEnfant1">VIL0002</td>
                                                        <td align="center" id="paysEnfant1">PAY0002</td>
                                                        <td align="center" id="valeurEnfant1">Antsiranana</td>
                                                        <td align="center" id="descriptionEnfant1">Antsiranana</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="flat-red" name="choix" id="enfant2" value="VIL0003">
                                                        </td>
                                                        <td align="center" id="numeroEnfant2">VIL0003</td>
                                                        <td align="center" id="paysEnfant2">PAY0003</td>
                                                        <td align="center" id="valeurEnfant2">Toamasina</td>
                                                        <td align="center" id="descriptionEnfant2">Toamasina</td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </form>

                                        <table class="table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td><b>Nombre de r�sultat :</b> 1,00</td>
                                                    <td align="right"><strong>page</strong> 1 <b>sur</b>1</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"></td>
                                                    <td align="right"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--travailleurs-->
                            <div role="tabpanel" class="tab-pane center-block" id="travailleurs">
                                <h4 class="modal-title text-center"><i class="fa fa-group"></i> <strong>Liste des Travailleurs</strong></h4>
                                <div class="box box-info">
                                    <div class="box-body no-padding" id="enfants">
                                        <form action="#" method="post" name="listevillechoix" id="listevillechoix">
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="box box-primary">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Rechercher</h3>
                                                            <div class="box-tools pull-right">
                                                                <button class="btn btn-box-tool" data-widget="collapse">
                                                                    <i class="fa fa-minus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="box-body" style="display: block;">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-xs-6">
                                                                            <div class="form-group">
                                                                                <label>Numero ville</label>
                                                                                <input name="id" type="textbox" class="form-control" id="id" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Pays</label>
                                                                                <select name="idpays" class="form-control" id="idpays">
                                                                                    <option value="%">Tous</option>
                                                                                    <option value="PAY000001">Angleterre</option>
                                                                                    <option value="PAY000003">Autre</option>
                                                                                    <option value="PAY000005">Espagne</option>
                                                                                    <option value="PAY000004">Italie</option>
                                                                                    <option value="PAY000002">Madagascar</option>
                                                                                    <option value="PAY0001">Madagascar</option>
                                                                                    <option value="PAY000006">Turquie</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                            <div class="form-group">
                                                                                <label>Valeur</label>
                                                                                <input name="val" type="textbox" class="form-control" id="val" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Description</label>
                                                                                <input name="desce" type="textbox" class="form-control" id="desce" value="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6"></div>
                                                                        <div class="col-xs-6"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="box box-success">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Trier par</h3>
                                                            <div class="box-tools pull-right">
                                                                <button class="btn btn-box-tool" data-widget="collapse">
                                                                    <i class="fa fa-minus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="box-body" style="display: block;">
                                                            <div class="form-group">
                                                                <label>Colonne</label>
                                                                <select name="colonne" class="form-control" id="colonne">
                                                                    <option value="id">id</option>
                                                                    <option value="idpays">idpays</option>
                                                                    <option value="val">val</option>
                                                                    <option value="desce">desce</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Ordre</label>
                                                                <select name="ordre" class="form-control" id="ordre">
                                                                    <option value="DESC">DESC</option>
                                                                    <option value="ASC">ASC</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td align="center">
                                                            <button type="submit" name="Submit" class="btn btn-success"><i class="fa fa-check"></i>
                                                            </button>
                                                        </td>
                                                        <td align="center">
                                                            <button type="reset" name="Submit" class="btn btn-danger"><i class="fa fa-close"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </form>
                                        <p align="center"><strong><u>RECAP</u></strong>
                                        </p>
                                        <table class="table" table-bordered="" width="100%">
                                            <tbody>
                                                <tr class="active" head="">
                                                    <th align="center" valign="top" style="text-align:center"></th>
                                                    <th align="center" valign="top" style="text-align:center">Nombre</th>
                                                </tr>
                                                <tr>
                                                    <td align="center" data-test="0/0">Total </td>
                                                    <td align="center" data-test="0/1">1,00 </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <br>
                                        <form action="http://localhost:8080/ketrikabo-war/bo/modeleAdmin.jsp?but=apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                                            <input type="hidden" name="champReturn" value="idville">

                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr class="info">
                                                        <th>#</th>
                                                        <th>Numero</th>
                                                        <th>Pays</th>
                                                        <th>Valeur</th>
                                                        <th>Description</th>
                                                    </tr>


                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="flat-red" name="choix" id="travailleur0" value="VIL0001">
                                                        </td>
                                                        <td align="center">VIL0001</td>
                                                        <td align="center">PAY0001</td>
                                                        <td align="center">Antananarivo</td>
                                                        <td align="center">Antananarivo</td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </form>


                                        <table class="table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td><b>Nombre de r�sultat :</b> 1,00</td>
                                                    <td align="right"><strong>page</strong> 1 <b>sur</b>1</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"></td>
                                                    <td align="right"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--Autre-->
                            <div role="tabpanel" class="tab-pane left-block" id="autres">
                                <h4 class="modal-title text-center"><i class="fa fa-reorder"></i> <strong>Autres</strong></h4>
                                <div class="box box-info">
                                    <div class="box-body no-padding" id="autres">
                                        <form action="#" method="post" name="listevillechoix" id="listevillechoix">
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="box box-primary">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Rechercher</h3>
                                                            <div class="box-tools pull-right">
                                                                <button class="btn btn-box-tool" data-widget="collapse">
                                                                    <i class="fa fa-minus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="box-body" style="display: block;">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-xs-6">
                                                                            <div class="form-group">
                                                                                <label>Numero ville</label>
                                                                                <input name="id" type="textbox" class="form-control" id="id" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Pays</label>
                                                                                <select name="idpays" class="form-control" id="idpays">
                                                                                    <option value="%">Tous</option>
                                                                                    <option value="PAY000001">Angleterre</option>
                                                                                    <option value="PAY000003">Autre</option>
                                                                                    <option value="PAY000005">Espagne</option>
                                                                                    <option value="PAY000004">Italie</option>
                                                                                    <option value="PAY000002">Madagascar</option>
                                                                                    <option value="PAY0001">Madagascar</option>
                                                                                    <option value="PAY000006">Turquie</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                            <div class="form-group">
                                                                                <label>Valeur</label>
                                                                                <input name="val" type="textbox" class="form-control" id="val" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Description</label>
                                                                                <input name="desce" type="textbox" class="form-control" id="desce" value="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6"></div>
                                                                        <div class="col-xs-6"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="box box-success">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Trier par</h3>
                                                            <div class="box-tools pull-right">
                                                                <button class="btn btn-box-tool" data-widget="collapse">
                                                                    <i class="fa fa-minus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="box-body" style="display: block;">
                                                            <div class="form-group">
                                                                <label>Colonne</label>
                                                                <select name="colonne" class="form-control" id="colonne">
                                                                    <option value="id">id</option>
                                                                    <option value="idpays">idpays</option>
                                                                    <option value="val">val</option>
                                                                    <option value="desce">desce</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Ordre</label>
                                                                <select name="ordre" class="form-control" id="ordre">
                                                                    <option value="DESC">DESC</option>
                                                                    <option value="ASC">ASC</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td align="center">
                                                            <button type="submit" name="Submit" class="btn btn-success"><i class="fa fa-check"></i>
                                                            </button>
                                                        </td>
                                                        <td align="center">
                                                            <button type="reset" name="Submit" class="btn btn-danger"><i class="fa fa-close"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </form>
                                        <p align="center"><strong><u>RECAP</u></strong>
                                        </p>
                                        <table class="table" table-bordered="" width="100%">
                                            <tbody>
                                                <tr class="active" head="">
                                                    <th align="center" valign="top" style="text-align:center"></th>
                                                    <th align="center" valign="top" style="text-align:center">Nombre</th>
                                                </tr>
                                                <tr>
                                                    <td align="center" data-test="0/0">Total </td>
                                                    <td align="center" data-test="0/1">1,00 </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <br>
                                        <form action="http://localhost:8080/ketrikabo-war/bo/modeleAdmin.jsp?but=apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                                            <input type="hidden" name="champReturn" value="idville">

                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr class="info">
                                                        <th>#</th>
                                                        <th>Numero</th>
                                                        <th>Pays</th>
                                                        <th>Valeur</th>
                                                        <th>Description</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="flat-red" name="choix" id="autre0" value="VIL0001">
                                                        </td>
                                                        <td align="center">VIL0001</td>
                                                        <td align="center">PAY0001</td>
                                                        <td align="center">Antananarivo</td>
                                                        <td align="center">Antananarivo</td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </form>
                                        <table class="table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td><b>Nombre de r�sultat :</b> 1,00</td>
                                                    <td align="right"><strong>page</strong> 1 <b>sur</b>1</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"></td>
                                                    <td align="right"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">
                            <i class="fa fa-check"></i>
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    function  addLine() {
        var content = '<tr>'
                + '<td class="td-content-center">'
                + '<input type="checkbox" name="recu" id="recu"/>'
                + '</td>'
                + '<td class="td-content-center">'
                + '<input type="checkbox" name="authentique" id="authentique"/>'
                + '</td>'
                + '<td>'
                + '<input type="text" name="nb" id="nb" class="form-control" />'
                + '</td>'
                + '<td>'
                + '<input type="text" name="code" id="code" class="form-control" />'
                + '</td>'
                + '<td>'
                + '<input type="text" name="libellepiece" id="libellepiece" class="form-control" />'
                + '</td>'
                + '<td>'
                + '<input type="checkbox" name="of" id="of"/>'
                + '</td>'
                + '<td>'
                + '<a data-toggle="modal" href="#myModalMessage"><i class="fa fa-child"></i></a>'
                + '<a href="?but=dossier/dossier-numerisation.jsp"><i class="fa fa-search"></i></a>'
                + '<a href="?but=dossier/dossier-saisie-piece.jsp"><i class="fa fa-edit"></i></a>'
                + '<a href="#supprimer"><i class="fa fa-close"></i></a>'
                + '</td>'
                + '</tr>';
        $(content).appendTo("#tbody");
    }
    function changeValue(obj){
        $('#idPiece').val(obj);
    }
</script>