<%@page import="lc.Direction"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="bean.TypeObjet"%>
<%@page import="service.AlloSakafoService"%>
<%
    String but=request.getParameter("but");
    if(but==null){
        but="pages/choixresto.jsp";
    }
%>
<!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <title>Choix restaurant</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.4 -->
      <link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <!-- Font Awesome Icons -->
      <link href="${pageContext.request.contextPath}/dist/js/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <!-- Theme style -->
      <link href="${pageContext.request.contextPath}/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
      <link href="${pageContext.request.contextPath}/dist/css/stylecustom.css" rel="stylesheet" type="text/css" />
      <!-- iCheck -->
      <link href="${pageContext.request.contextPath}/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
      <link href="${pageContext.request.contextPath}/assets/css/global.min.css" rel="stylesheet" />
      <link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet">
      <link href="${pageContext.request.contextPath}/assets/css/xzoom.css" rel="stylesheet">
      <link href="${pageContext.request.contextPath}/assets/css/magnific-popup.css" rel="stylesheet">
      <link href="${pageContext.request.contextPath}/assets/css/magnific-popup.css" rel="stylesheet">
      <link href="${pageContext.request.contextPath}/assets/css/customicomketrika.css" rel="stylesheet">
      <link href="${pageContext.request.contextPath}/assets/css/Animate.css" rel="stylesheet">

      <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
      <script src="${pageContext.request.contextPath}/assets/js/jquery.cookie.min.js"></script>
      <script src="${pageContext.request.contextPath}/assets/js/bootstrap-notify.min.js"></script>
      <script src="${pageContext.request.contextPath}/assets/js/soundmanager2-jsmin.js"></script>
      <script src="${pageContext.request.contextPath}/assets/js/respond.min.js"></script>
      
    </head>
    <body class="body-index">
        <jsp:include page="<%=but%>"/>
        

      <!-- jQuery 2.1.4 -->
      <script src="${pageContext.request.contextPath}/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
      <!-- Bootstrap 3.3.2 JS -->
      <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
      <!-- iCheck -->
      <script src="${pageContext.request.contextPath}/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
      <script>
        $(function () {
          $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
          });
        });
      </script>
    </body>
  </html>