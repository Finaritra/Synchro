<%-- 
    Document   : report-liste
    Created on : 11 jan. 2017, 15:42:37
    Author     : Narindra
--%>

<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.ded.Report"%>
<%@page import="affichage.PageRecherche"%>

<% 
    Report report = new Report();
    report.setNomTable("report");
    
    String listeCrt[] = {"daty", "montant", "devise", "caisse"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "montant", "devise", "caisse"};

    
    PageRecherche pr = new PageRecherche(report, request, listeCrt, listeInt, 4, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    affichage.Champ[] liste = new affichage.Champ[1];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("devise");
    liste[0] = new Liste("devise", ou, "VAL", "VAL");

    pr.getFormu().changerEnChamp(liste);
    
    
    pr.setApres("ded/report-liste.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste report</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=ded/report-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=ded/report-liste.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "Montant", "Devise", "Caisse"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>