<%-- 
    Document   : as-livraison-fiche
    Created on : 1 d�c. 2016, 14:21:55
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.ded.OrdonnerPayement"%>
<%@page import="mg.allosakafo.ded.OrdonnerPayementLibelle"%>
<%@page import="mg.allosakafo.ded.OpPaye"%>
<%
    OrdonnerPayement a = new OrdonnerPayement();
    //OrdonnerPayementLibelle a = new OrdonnerPayementLibelle();
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));

    pc.getChampByName("id").setLibelle("ID");
    pc.getChampByName("ded_Id").setLibelle("N� Facture");
    pc.getChampByName("montant").setLibelle("Montant");
    pc.getChampByName("remarque").setLibelle("Remarque");

    pc.getChampByName("daty").setLibelle("Date");

    pc.getChampByName("idLigne").setVisible(false);

    pc.setTitre("Consultation Ordonner Payement");
    System.out.println("*****************************");
   System.out.println(request.getParameter("id"));
     System.out.println("------------------------");
    OrdonnerPayement op = (OrdonnerPayement)pc.getBase();

    OpPaye opp = new OpPaye();
    opp.setNomTable("OPPAYE");
    OpPaye[] oppaye = (OpPaye[]) CGenUtil.rechercher(opp, null, null, " AND ID ='" + request.getParameter("id") + "'");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=ded/ordonnerpayement-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>

                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer" >
                            <% if (op.getEtat() == ConstanteEtat.getEtatCreer()) {%>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=ded/apresOrdonner.jsp&acte=validerOP&classe=mg.allosakafo.ded.OrdonnerPayement&bute=ded/ordonnerpayement-fiche.jsp&id=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Viser</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annulerVisa&classe=mg.allosakafo.ded.OrdonnerPayement&bute=ded/ordonnerpayement-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
                            <%
                            } else if (op.getEtat() == ConstanteEtat.getEtatValider() && oppaye.length == 0) {
                            %>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=fin/sortiecaisse-saisie.jsp&idordre=" + request.getParameter("id") + "&remarque=" + op.getRemarque()%>" style="margin-right: 10px">Payer</a>
                            <% } %>
                        </div>                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>
