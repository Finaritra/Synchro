<%-- 
    Document   : as-paiement-saisie
    Created on : 1 d�c. 2016, 15:42:08
    Author     : N
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.ded.Report"%>
<%@page import="mg.allosakafo.fin.Caisse"%>

<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    Report  r = new Report();
    PageInsert pi = new PageInsert(r, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));    
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("montant").setLibelle("Montant");
    pi.getFormu().getChamp("caisse").setLibelle("Caisse");
    pi.getFormu().getChamp("devise").setLibelle("Devise");
    affichage.Champ[] liste = new affichage.Champ[2];
    
    TypeObjet d = new TypeObjet();
    d.setNomTable("DEVISE");
    liste[0] = new Liste("devise", d, "val", "id");
    
    Caisse dcaisse = new Caisse();
    dcaisse.setNomTable("CAISSE");
    liste[1] = new Liste("caisse", dcaisse, "desccaisse", "idcaisse");
    
    pi.getFormu().changerEnChamp(liste);
    pi.preparerDataFormu();
    
%>
<div class="content-wrapper">
    <h1 class="box-title">Enregistrer report</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="report" id="report" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="ded/report-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.ded.Report">
    </form>
</div>
<script language="JavaScript">

</script>