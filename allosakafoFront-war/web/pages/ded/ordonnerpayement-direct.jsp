<%@page import="mg.allosakafo.facturefournisseur.FactureFournisseur"%>
<%@page import="user.*"%> 
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*"%>
<%@page import="utilitaire.*"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    FactureFournisseur  a = new FactureFournisseur();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));    
    
    affichage.Champ[] liste = new affichage.Champ[1];
    
    TypeObjet op = new TypeObjet();
    op.setNomTable("devise");
    liste[0] = new Liste("idDevise", op, "desce", "id");
    
    pi.getFormu().changerEnChamp(liste);
    
	
    pi.getFormu().getChamp("daty").setLibelle("Date");
	pi.getFormu().getChamp("idDevise").setLibelle("Devise");
	pi.getFormu().getChamp("idTVA").setLibelle("Montant TVA");
	pi.getFormu().getChamp("montantTTC").setLibelle("Montant TTC");
	pi.getFormu().getChamp("idFournisseur").setLibelle("Fournisseur");
	pi.getFormu().getChamp("datyecheance").setLibelle("Date d'echeance");
		
	pi.getFormu().getChamp("resp").setVisible(false);
	pi.getFormu().getChamp("etat").setVisible(false);
	pi.getFormu().getChamp("dateEmission").setVisible(false);
	pi.getFormu().getChamp("numFact").setVisible(false);
	
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("remarque").setType("textarea");
	pi.getFormu().getChamp("idFournisseur").setPageAppel("choix/listeFournisseurChoix.jsp");
	
	
	if (request.getParameter("daty")!= null && request.getParameter("daty").compareTo("") != 0) pi.getFormu().getChamp("daty").setDefaut(request.getParameter("daty"));
	if (request.getParameter("remarque")!= null && request.getParameter("remarque").compareTo("") != 0) pi.getFormu().getChamp("remarque").setDefaut(request.getParameter("remarque"));
	
	
	
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Enregistrer Ordonner payement</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=ded/apresOrdonner.jsp" method="post" name="starticle" id="starticle">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
	
    <input name="acte" type="hidden" id="nature" value="insertDirect">
    <input name="bute" type="hidden" id="bute" value="ded/ordonnerpayement-direct.jsp">
    </form>
</div>