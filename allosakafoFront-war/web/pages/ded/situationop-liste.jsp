<%-- 
    Document   : situationop-liste
    Created on : 21 f�vr. 2017, 11:43:08
    Author     : Doudou Tiarilala
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.ded.SituationOp"%>
<%@page import="affichage.PageRecherche"%>

<%
    SituationOp lv = new SituationOp();

    String listeCrt[] = {"id", "daty", "remarque", "apayer", "payer", "reste"};
    String listeInt[] = {"daty", "apayer", "payer", "reste"};
    String libEntete[] = {"id", "daty", "remarque", "apayer", "payer", "reste"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));

    pr.getFormu().getChamp("daty1").setLibelleAffiche("Date min ");
    pr.getFormu().getChamp("daty2").setLibelleAffiche("Date max ");
    pr.getFormu().getChamp("apayer1").setLibelleAffiche("a payer  min");
    pr.getFormu().getChamp("apayer2").setLibelleAffiche(" a payer max");
    pr.getFormu().getChamp("payer1").setLibelleAffiche("pay� min ");
    pr.getFormu().getChamp("payer2").setLibelleAffiche("pay� max");
    pr.getFormu().getChamp("reste1").setLibelleAffiche("reste min");
    pr.getFormu().getChamp("reste2").setLibelleAffiche("reste max ");
    pr.setApres("ded/situationop-liste.jsp");
    String[] colSomme = {"apayer", "payer", "reste"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Situation OP vis�</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=ded/situationop-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=ded/ordonnerpayement-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "Date", "remarque", "a payer", "pay�", "reste"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>