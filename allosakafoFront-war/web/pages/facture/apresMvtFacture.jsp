<%-- 
    Document   : apresMvtFacture
    Created on : 8 ao�t 2016, 09:59:52
    Author     : Murielle
--%>
<%@page import="java.lang.Object"%>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="bean.*" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.fin.FactureClient"%>
<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
        String nomtable;
        String typeBoutton;
    %>
    <%
        try {
            typeBoutton = request.getParameter("type");
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            bute = request.getParameter("bute");
            Object temp = null;
            String[] rajoutLien = null;
            String classe = request.getParameter("classe");
            ClassMAPTable t = null;
            String tempRajout = request.getParameter("rajoutLien");
            String val = "";

            String rajoutLie = "";
            if (tempRajout != null && tempRajout.compareToIgnoreCase("") != 0) {
                rajoutLien = utilitaire.Utilitaire.split(tempRajout, "-");
            }
            
            if (typeBoutton == null || typeBoutton.compareToIgnoreCase("") == 0) {
                typeBoutton = "3"; //par defaut modifier
            }

            int type = Utilitaire.stringToInt(typeBoutton);
            String id = request.getParameter("id");
            if (acte.compareToIgnoreCase("insert") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
                ClassMAPTable o = (ClassMAPTable) u.createObject(f);
                temp = (Object) o;
                if (o != null) {
                    id = o.getTuppleID();
                }
            }
			
			if (acte.compareToIgnoreCase("insertFactureClient") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                FactureClient f = (FactureClient)p.getObjectAvecValeur();
                f.setNomTable(nomtable);
				String nomtablec=request.getParameter("nomtablec")
				if (f.getIdobjet()==null || f.getIdobjet().compareTo("")==0){
					ClassMAPTable o = (ClassMAPTable) u.createObject(f);
					temp = (Object) o;
					if (o != null) {
						id = o.getTuppleID();
					}
				}
				else {
					id= u.createFactureClient(f,nomtablec);
				}
            }
            
       
        if (rajoutLien != null) {
            for (int o = 0; o < rajoutLien.length; o++) {
                String valeur = request.getParameter(rajoutLien[o]);
                if (classe.compareToIgnoreCase("mg.cmcm.fin.MvtCaisse") != 0) {
                    rajoutLie = rajoutLie + "&" + rajoutLien[o] + "=" + valeur;
                }
            }
            
        }
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&valeur=<%=val%>");</script>
    <%

    } catch (Exception e) {
        e.printStackTrace();
    %>

    <script language="JavaScript"> alert('<%=e.getMessage()%>');
        history.back();</script>
        <%
                return;
            }
        %>
</html>
