<%-- 
    Document   : factureclient-saisie
    Created on : 8 ao�t 2016, 09:46:55
    Author     : Murielle
--%>

<%@page import="mg.allosakafo.fin.FactureClient"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.commande.CommandeService"%>
<%@page import="user.*"%>
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    FactureClient  a = new FactureClient();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));

    pi.setLien((String) session.getValue("lien"));    
    
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("dateemission").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("dateecheance").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("remarque").setType("textarea");
    pi.getFormu().getChamp("dateemission").setLibelle("Date emission");
    pi.getFormu().getChamp("dateecheance").setLibelle("Date echeance");
    pi.getFormu().getChamp("client").setPageAppel("choix/listeClientChoix.jsp");
	pi.getFormu().getChamp("client").setAutre("readonly='true'");
    pi.getFormu().getChamp("tva").setLibelle("TVA");
    pi.getFormu().getChamp("idobjet").setAutre("readonly='true'");
	pi.getFormu().getChamp("idobjet").setLibelle("Objet");
    pi.getFormu().getChamp("etat").setVisible(false);
	
	if(request.getParameter("idCommande") != null && request.getParameter("idCommande").compareToIgnoreCase("") != 0){
        pi.getFormu().getChamp("idobjet").setDefaut(request.getParameter("idCommande"));
        
        double montant = CommandeService.calculerMontantCommande(request.getParameter("idCommande"));
       
        CommandeClient cmc = CommandeService.getInfoCommandeClient2(request.getParameter("idCommande"),nomtable);
        pi.getFormu().getChamp("client").setDefaut(cmc.getRemarque());
        pi.getFormu().getChamp("idobjet").setDefaut(cmc.getId());
    }
	String nomtable="";
        if(request.getParameter("nomtable") != null && request.getParameter("nomtable").compareToIgnoreCase("")!=0)
        {
            nomtable=request.getParameter("nomtable");
        }
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Saisie Facture Client</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=facture/apresMvtFacture.jsp" method="post" name="factureclient" id="factureclient">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insertFactureClient">
    <input name="bute" type="hidden" id="bute" value="facture/factureclientdetails-saisie.jsp&nomtable=<%=nomtable%>>
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.fin.FactureClient">
    <input name="nomtablec" type="hidden" id="nomtablec" value="<%=nomtable%>">
    </form>
    
</div>
