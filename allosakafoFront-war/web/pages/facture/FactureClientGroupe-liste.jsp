<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.fin.FactureFournisseurGroupe"%>
<%@page import="affichage.PageRechercheGroupe"%>
<%
    FactureFournisseurGroupe ff = new FactureFournisseurGroupe();
    ff.setNomTable("as_facture_client_groupe");
    String listeCrt[] = {"idfacture", "daty", "designation", "mois", "annee"};
    String listeInt[] = {"daty", "mois", "annee"};
    String libEntete[] = {"idfacture", "daty", "designation"};

    String colDefaut[] = {"idfacture", "daty", "designation"};
    String somDefaut[] = {"montant", "paye", "reste"};

    PageRechercheGroupe pr = new PageRechercheGroupe(ff, request, listeCrt, listeInt, 3, colDefaut, somDefaut, 3, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));

    affichage.Champ[] liste = new affichage.Champ[2];

    pr.getFormu().getChamp("idfacture").setLibelle("Id Facture");
    pr.getFormu().getChamp("Daty1").setLibelle("Date min");
    pr.getFormu().getChamp("Daty2").setLibelle("Date max");
    pr.getFormu().getChamp("designation").setLibelle("D�signation");
    pr.getFormu().getChamp("Mois1").setLibelle("Mois d�but");
    pr.getFormu().getChamp("Mois2").setLibelle("Mois fin");

    liste[0] = new Liste("mois1");
    ((Liste) (liste[0])).makeListeMois();
    liste[1] = new Liste("mois2");
    ((Liste) (liste[1])).makeListeMois();

    pr.setApres("fin/facture/FactureClientGroupe-liste.jsp");
    String[] colSomme = null;

    pr.getFormu().changerEnChamp(liste);
    pr.creerObjetPage();
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Etat facture client group�</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=fin/facture/FactureClientGroupe-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>	

        </form>
        <%
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
