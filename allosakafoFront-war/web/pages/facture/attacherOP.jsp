<%-- 
    Document   : attacherOP
    Created on : 16 ao�t 2016, 16:26:49
    Author     : Murielle
--%>

<%@page import="bean.UnionIntraTable"%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cmcm.fin.OrdreDePaiement"%>
<%@page import="bean.CGenUtil"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.cmcm.fin.FactureFournisseur"%>
<%@page import="utilitaire.Utilitaire"%>
<%
    String nomtable = "UNION_OP_FF";
    
    FactureFournisseur declaration = new FactureFournisseur();
    declaration.setNomTable("FACTURE_FOURNISSEUR_VUE");
    String id = request.getParameter("idfacture");
    FactureFournisseur[] listdr = (FactureFournisseur[]) CGenUtil.rechercher(declaration, null, null, " AND ID = '" + id + "'");

    UserEJB u = (UserEJB) session.getAttribute("u");

    OrdreDePaiement orCmp = new OrdreDePaiement();
    orCmp.setNomTable("ordrepayement_vue");
    String listeCrt[] = {"id", "description", "modepaiement", "idbudgetdepense", "daty"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "description", "modepaiement", "idbudgetdepense", "daty", "montant"};

    String lien = (String) session.getValue("lien");

    PageRechercheChoix pr = new PageRechercheChoix(orCmp, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("fin/facture/attacherOP.jsp");

    pr.getFormu().getChamp("modepaiement").setLibelle("Mode de paiement");
    pr.getFormu().getChamp("idbudgetdepense").setLibelle("Budget d�pense");
    pr.getFormu().getChamp("id").setLibelle("ID");
    pr.getFormu().getChamp("daty1").setLibelle("Date inf.");
    pr.getFormu().getChamp("daty2").setLibelle("Date sup.");

    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);

    UnionIntraTable uit = new UnionIntraTable();
    uit.setNomTable(nomtable);
    UnionIntraTable[] union = (UnionIntraTable[]) CGenUtil.rechercher(uit, null, null, " AND ID1 = '" + id + "'");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h1 class="box-title">D&eacute;tails facture</h1>
                </div>
                <div class="box-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Date</label>
                            <p><%=Utilitaire.format(listdr[0].getDaty())%></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Code:</label>
                            <p><%=listdr[0].getId()%></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>B�n�ficiaire</label>
                            <p><%=listdr[0].getBeneficiaire()%></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Observation:</label>
                            <p><%=listdr[0].getDesignation()%></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h1 class="box-title">Liste OP rattach&eacute;e</h1>
                </div>
                <div class="box-body">
                    <form action="<%=lien%>?but=fin/facture/apresRattache.jsp&idfacture=<%=id%>" method="post">
                        <input type="hidden" id="montantObjet" name="montantObjet" value="0">
                        
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>ID OP</th>
                                    <th>Remarque</th>
                                    <th>Montant</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (union != null && union.length > 0) {
                                        double sommeMontant = 0;
                                        for (int i = 0; i < union.length; i++) {
                                            sommeMontant += union[i].getMontantMere();
                                %>
                                <tr>
                            <input type="hidden" name="id<%=i%>" id="id<%=i%>" value="<%=union[i].getId()%>"/>
                            <td><%=union[i].getId()%></td>
                            <td><%=union[i].getId2()%></td>
                            <td><%=union[i].getRemarque()%></td>
                            <td><input class="form-control" type="text" name="montant<%=i%>" id="montant<%=i%>" value="<%=Utilitaire.doubleWithoutExponential(union[i].getMontantMere())%>"/></td>
                            <td><a type="button" class="btn btn-dager pull-right"  href="<%=lien%>?but=fin/facture/apresRattache.jsp&idfacture=<%=id%>&acte=annulerjointure&idobjet=<%=union[i].getId()%>&bute=fin/facture/attacherOP.jsp&nomtable=<%=nomtable%>" style="margin-right: 10px" >supprimer</a></td>
                            </tr>
                            <%}
                                %>
                                <tr>
                                <td></td>
                                <td></td>
                                <td>Total:</td>
                                <td><input class="form-control" type="text" name="total" id="total" value="<%=Utilitaire.doubleWithoutExponential(sommeMontant)%>"/></td>
                                </tr><%
                                }%>
                            </tbody>
                        </table>
                        <input type="hidden" id="acte" name="acte" value="updatejointureordr">
                        <input type="hidden" id="length" name="length" value="<%=union.length%>">
                        <input type="hidden" id="nomtable" name="nomtable" value="<%=nomtable%>">
                        <input type="hidden" name="bute" id="bute" value="fin/facture/attacherOP.jsp&idfacture=<%=id%>">
                        <button type="button" class="btn btn-default pull-right"  style="margin-right: 10px" onclick="calculere(<%=union.length%>)">Calculer</button>
                        <button type="submit" class="btn btn-primary pull-right"  style="margin-right: 10px" >Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h1 class="box-title">Rechercher OP</h1>
                </div>
                <div class="box-body">
                    <form action="<%=lien%>?but=fin/facture/attacherOP.jsp&idfacture=<%=id%>" method="post" name="bpc" id="bpc">
                        <% out.println(pr.getFormu().getHtmlEnsemble());%>
                    </form>
                    <form action="<%=lien%>?but=fin/facture/apresRattache.jsp&idfacture=<%=id%>" method="post">
                        <% out.println(pr.getTableau().getHtmlWithCheckbox());%>
                        <input type="hidden" name="nomtable" id="nomtable" value="<%=nomtable%>">
                        <input type="hidden" name="acte" id="acte" value="attacher">
                        <input type="hidden" name="bute" id="bute" value="fin/facture/attacherOP.jsp&idfacture=<%=id%>">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function calculere(i)
    {
        var c;
        var somme = 0;
        for (c = 0; c < i; c++)
        {
            var ge = document.getElementsByName("montant" + c)[0];
            var reg = new RegExp("[%]", "g");
            if (ge.value.indexOf("%") != -1)
            {
                var tableau = ge.value.split(reg);
                ge.value = parseFloat(document.getElementsByName("montantObjet")[0].value) * parseFloat(tableau[0]) / 100;
            }
            somme = somme + parseFloat(eval(ge.value));
            ge.value = eval(ge.value);
        }
        var h = document.getElementsByName("total")[0];
        h.value = somme;
    }
</script>
