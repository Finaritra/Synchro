<%-- 
    Document   : apresRattache
    Created on : 16 ao�t 2016, 16:46:28
    Author     : Murielle
--%>

<%@page import="mg.cmcm.fin.*"%>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="bean.*" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="affichage.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
    %>
    <%
        try {
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            bute = request.getParameter("bute");
            Object temp = null;
            String[] rajoutLien = null;
            String classe = request.getParameter("classe");
            ClassMAPTable t = null;
            String tempRajout = request.getParameter("rajoutLien");
            String val = "";
            
            String rajoutLie = "";
            if (tempRajout != null && tempRajout.compareToIgnoreCase("") != 0) {
                rajoutLien = utilitaire.Utilitaire.split(tempRajout, "-");
            }
            if (bute == null || bute.compareToIgnoreCase("") == 0) {
                bute = "pub/Pub.jsp";
            }

            if (classe == null || classe.compareToIgnoreCase("") == 0) {
                classe = "pub.Montant";
            }
            
            if(acte.compareToIgnoreCase("annulerjointure")==0){
                String idobjet = request.getParameter("idobjet");
                String nomtable = request.getParameter("nomtable");
                String bute = request.getParameter("bute");
                String id = request.getParameter("idfacture");
                UnionIntraTable uti = new UnionIntraTable();
                uti.setId(idobjet);
                uti.setNomTable(nomtable);
                uti.deleteToTable();
                %>
                <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>&idfacture=<%=id%>");</script>
                <%
            }
            if(acte.compareToIgnoreCase("updatejointureordr")==0){
                String idobjet = request.getParameter("idfacture");
                String nomtable = request.getParameter("nomtable");
                int length = Utilitaire.stringToInt(request.getParameter("length"));
                String[] montant = new String[length];
                String[] idor = new String[length];
                for(int i = 0; i < length; i++){
                    idor[i] = request.getParameter("id"+i);
                    montant[i] = request.getParameter("montant"+i);
                }
                u.updateMontantUnionIntra(nomtable,idobjet,idor,montant);
            }
            if(acte.compareToIgnoreCase("attacher")==0){
                String idobjet = request.getParameter("idfacture");
                String[] idor = request.getParameterValues("id");
                String nomtable = request.getParameter("nomtable"); 
                u.insertMontantUnionIntra(nomtable, idobjet,idor);
            }
            
        
        if (rajoutLien != null) {

            for (int o = 0; o < rajoutLien.length; o++) {
                String valeur = request.getParameter(rajoutLien[o]);
                rajoutLie = rajoutLie + "&" + rajoutLien[o] + "=" + valeur;
            }
        }

    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&valeur=<%=val%>");</script>
    <%

    } catch (Exception e) {
        e.printStackTrace();
    %>

    <script language="JavaScript"> alert('<%=e.getMessage()%>');
        history.back();</script>
        <%
                return;
            }
        %>
</html>
