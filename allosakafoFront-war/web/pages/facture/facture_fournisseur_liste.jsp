<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cmcm.fin.FactureFournisseur"%>
<%@page import="mg.cmcm.budget.BudgetDepense"%>
<%@page import="affichage.PageRecherche"%>

<% 
	FactureFournisseur ff = new FactureFournisseur();
	ff.setNomTable("facture_fournisseur_vue");
    String listeCrt[] = {"id","typefacture", "idbudgetdepense", "daty"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id","typefacture", "idbudgetdepense", "daty"};

    PageRecherche pr = new PageRecherche(ff, request, listeCrt, listeInt, 1, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    affichage.Champ[] liste = new affichage.Champ[2];
    
	TypeObjet f = new TypeObjet();
    f.setNomTable("typefacture");
    liste[0] = new Liste("typefacture", f, "VAL", "VAL");
    
    BudgetDepense bd = new BudgetDepense();
    liste[1] = new Liste("idbudgetdepense", bd, "DESIGNATION", "DESIGNATION");
    pr.getFormu().changerEnChamp(liste);
    
    pr.getFormu().getChamp("Daty1").setLibelle("Date de recherche");
    pr.getFormu().getChamp("Daty2").setLibelle("A");
    pr.getFormu().getChamp("Typefacture").setLibelle("Type facture");
    pr.getFormu().getChamp("Idbudgetdepense").setLibelle("Budget d�pense");
    pr.setApres("fin/facture_fournisseur_liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste facture fournisseur</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=fin/facture/facture_fournisseur_liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=fin/facture/facturefournisseur-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id","Type Facture", "Budget D�pense", "Date"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
