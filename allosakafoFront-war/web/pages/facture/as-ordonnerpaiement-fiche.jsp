<%-- 
    Document   : as-ordonnerpaiement-fiche
    Created on : 8 d�c. 2016, 08:28:08
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.fin.*"%>
<%
    OrdreDePaiement a = new OrdreDePaiement();
    //a.setNomTable("as_paiement_libelle");
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    
    pc.setTitre("Consultation paiement");

	OrdreDePaiement orp = (OrdreDePaiement) pc.getBase();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=facture/as-ordonnerpaiement-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
						<% if (orp.getEtat() == ConstanteEtat.getEtatCreer()){%>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=ded/apresOrdonner.jsp&acte=validerOR&classe=mg.allosakafo.fin.OrdreDePaiement&bute=facture/as-ordonnerpaiement-fiche.jsp&id=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Viser</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annulerVisa&classe=mg.allosakafo.fin.OrdreDePaiement&bute=facture/as-ordonnerpaiement-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
							<% 
							}
							else if (orp.getEtat() == ConstanteEtat.getEtatValider()){
							%>
							<a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=fin/entreecaisse-saisie.jsp&idordre=" + request.getParameter("id")+"&remarque="+ orp.getRemarque()%>" style="margin-right: 10px">Payer</a>
                            <% } %>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>