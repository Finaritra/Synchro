<%-- 
    Document   : facturefournisseur-fiche
    Created on : 8 ao�t 2016, 12:01:10
    Author     : Murielle
--%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cmcm.fin.*" %>
<%
    FactureFournisseur dma;
    double total = 0.0;
%>
<%
    dma = new FactureFournisseur();
    dma.setNomTable("FACTURE_FOURNISSEUR_VUE");
    PageConsulte pc = new PageConsulte(dma, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.getChampByName("daty").setLibelle("Date");
    pc.getChampByName("typefacture").setLibelle("Type facture");
    pc.getChampByName("idbudgetdepense").setLibelle("Budget Depense");
    pc.getChampByName("idobjet").setLibelle("Objet");
    pc.getChampByName("tva").setLibelle("TVA");
    
    pc.setTitre("Fiche Facture Fournisseur");
    
    FactureFournisseur bondecommande = (FactureFournisseur)pc.getBase();
    
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=fin/facture/facturefournisseur-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=fin/facture/facturefournisseur-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=fin/facture/facturefournisseur-liste.jsp&classe=mg.cmcm.fin.FactureFournisseur" style="margin-right: 10px">Supprimer</a>
                            <a class="btn btn-primary pull-right"  href="<%=(String) session.getValue("lien") + "?but=fin/facture/attacherOP.jsp&idfacture=" + request.getParameter("id")%>" style="margin-right: 10px">Attacher OP</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">D&eacute;tails Facture</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>D�tail</th>
                                    <th>Quantit&eacute;</th>
                                    <th>Montant</th>
                                </tr>
                            </thead>

                            <tbody>
                                <%
                                    FactureFournisseurDetails p = new FactureFournisseurDetails();
                                    FactureFournisseurDetails[] liste = (FactureFournisseurDetails[]) CGenUtil.rechercher(p, null, null, " and idmere = '" + request.getParameter("id") + "'");

                                    for (int i = 0; i < liste.length; i++) {
                                %>
                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                    <td  align="center"><%=liste[i].getDesignation()%> </td>
                                    <td  align="center"><%=(int) liste[i].getQuantite()%></td>
                                    <td  align="right"><%=Utilitaire.formaterAr(liste[i].getMontant())%></td>
                                </tr>
                                <%
                                    total += liste[i].getMontant();
                                    }
                                    double tva = (total * bondecommande.getTva()) / 100;
                                %>
                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                    <td  align="center"></td>
                                    <td  align="right">TVA</td>
                                    <td  align="right"><%=Utilitaire.formaterAr(tva)%></td>
                                </tr>
                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                    <td  align="center"></td>
                                    <td  align="right">Total</td>
                                    <td  align="right"><%=Utilitaire.formaterAr(tva + total)%></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
