<%--
    Document   : logPersonnelChoixMultiple
    Created on : 27 oct. 2015, 19:17:22
    Author     : user
--%>

<%@page import="mg.cnaps.log.LogCarteCarburant"%>
<%@page import="mg.cnaps.log.LogPersonnel"%>
<%@page import="mg.cnaps.log.LogDeplacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    affichage.Champ[] liste = new affichage.Champ[3];
    TypeObjet listeTypeCarte = new TypeObjet();
    listeTypeCarte.setNomTable("LOG_TYPE_CARTE");
    liste[0] = new Liste("idtype_carte", listeTypeCarte, "val", "val");
    TypeObjet listeDistribPetr = new TypeObjet();
    listeDistribPetr.setNomTable("LOG_DISTRIBUTEUR_PETROLIER");
    liste[1] = new Liste("id_distrib_petr", listeDistribPetr, "val", "val");
    TypeObjet listeTypeDetenteur = new TypeObjet();
    listeTypeDetenteur.setNomTable("LOG_TYPE_DETENTEUR");
    liste[2] = new Liste("dr_detenteur", listeTypeDetenteur, "val", "val");

    LogCarteCarburant e = new LogCarteCarburant();
    e.setNomTable("LOG_CARTE_CARBURANT_SOLDE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "numero", "montant", "iddetenteur", "date_expiration", "id_distrib_petr", "idtype_carte", "service_detenteur", "dr_detenteur"};
    String listeInt[] = {"date_expiration", "montant"};
    String libEntete[] = {"id", "numero", "montant", "iddetenteur", "date_expiration", "id_distrib_petr", "idtype_carte", "service_detenteur", "dr_detenteur"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 9);
    pr.getFormu().changerEnChamp(liste);
    pr.setChampReturn(champReturn);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.getFormu().getChamp("date_expiration1").setLibelle("Date d'expiration min");
    pr.getFormu().getChamp("date_expiration2").setLibelle("Date d'expiration max");
    pr.getFormu().getChamp("montant1").setLibelle("Solde min");
    pr.getFormu().getChamp("montant2").setLibelle("Solde max");
    pr.getFormu().getChamp("iddetenteur").setLibelle("D&eacute;tenteur");
    pr.getFormu().getChamp("numero").setLibelle("Num&eacute;ro");
    pr.getFormu().getChamp("idtype_carte").setLibelle("Type Carte");
    pr.getFormu().getChamp("id_distrib_petr").setLibelle("Distributeur p&eacute;trolier");
    pr.getFormu().getChamp("service_detenteur").setLibelle("Service/Dir. Régional d&eacute;tenteur");
    pr.getFormu().getChamp("dr_detenteur").setLibelle("Type Détenteur");

    pr.setLien((String) session.getValue("lien"));
    pr.setApres("logCarteCarbuChoixMultiple.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Cartes Carburant</h1>
            </section>
            <section class="content">
                <form action="logCarteCarbuChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="chauffeur" id="chauffeur">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
		    String lienTableau[] = {pr.getLien() + "?but=materiel_roulant/carte-carburant/logcartecarburant-fiche.jsp"};
		    String colonneLien[] = {"id"};
		    String libelles[] = {"Id", "Num&eacute;ro", "Solde actuelle", "D&eacute;tenteur", "Date d'expiration", "Distributeur pétrolier", "Type de la Carte", "Serv./DR Détenteur", "Type Détenteur"};
		    pr.getTableau().setLien(lienTableau);
		    pr.getTableau().setColonneLien(colonneLien);
		    pr.getTableau().setLibelleAffiche(libelles);
		    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>