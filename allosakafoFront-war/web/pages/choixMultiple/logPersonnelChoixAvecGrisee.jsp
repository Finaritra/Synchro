<%-- 
    Document   : deplacementChoix
    Created on : 8 sept. 2015, 21:26:25
    Author     : user
--%>
<%@page import="mg.cnaps.log.LogPersonnelValide"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    LogPersonnelValide e = new LogPersonnelValide();
    e.setNomTable("LOG_PERSONNEL_VALIDE_VIEW2");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "MATRICULE", "nom", "prenom", "direction", "service"};
    String listeInt[] = null;
    String libEntete[] = {"id", "MATRICULE", "nom", "prenom", "direction", "service"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("logPersonnelChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("prenom").setLibelleAffiche("Pr�nom");
    pr.getFormu().getChamp("direction").setLibelleAffiche("Direction");
    pr.getFormu().getChamp("service").setLibelleAffiche("Service");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
	
	
	LogPersonnelValide[] liste = (LogPersonnelValide[])pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste personnels</h1>
            </section>
            
                <form action="logPersonnelChoixAvecGrisee.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="chauffeur" id="chauffeur">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
					String libelles[] = {"id", "Matricule", "nom", "prenom", "direction", "service"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
			   %>
                <form action="../<%=pr.getLien()%>?but=choix/apresLogPersonnelGrisee.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <p align="center"><strong><u>LISTE</u></strong></p>
                    <div id="divchck">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table" table-hover="">
                            <tbody>
                                <tr class="head">
                                    <td align="center" valign="top"></td>
                                    <td width="33%" align="center" valign="top">Id</td>
                                    <td width="33%" align="center" valign="top">Nom</td>
                                    <td width="33%" align="center" valign="top">Prenom</td>
                                    <td width="33%" align="center" valign="top">Matricule</td>
                                    <td width="33%" align="center" valign="top">Sexe</td>
                                </tr>
                                <%for(int i = 0 ; i<liste.length ; i++){%>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
                                        <td align="center">
                                        <input type="radio" value="<%=liste[i].getId()%>;<%=liste[i].getService() %>" name="choix" onmousedown="getChoix()" id="choix" class="radio"></td>
                                        <td width="33%" align="center"><a href="<%=pr.getLien()%>?but=paie/employe/personnel-fiche.jsp&id=<%=liste[i].getId() %>"><%=liste[i].getId() %></a></td>
                                        <td width="33%" align="center"><%=liste[i].getNom() %></td>
                                        <td width="33%" align="center"><%=liste[i].getPrenom() %></td>
                                        <td width="33%" align="center"><%=liste[i].getMatricule() %></td>
                                        <td width="33%" align="center"><%=liste[i].getSexe() %></td>
                                    </tr>
                                <%}%>
                            </tbody>
                        </table>
                </form>
                <% out.println(pr.getBasPage());%>
            
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>