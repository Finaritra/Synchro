<%-- 
    Document   : cvExperienceChoixMultiple
    Created on : 7 d�c. 2015, 16:14:40
    Author     : user
--%>
<%@page import="mg.cnaps.cv.CvExperience"%>
<%@page import="mg.cnaps.log.LogPersonnel"%>
<%@page import="mg.cnaps.log.LogDeplacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    CvExperience e = new CvExperience();
    e.setNomTable("CV_EXPERIENCE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "date_debut", "date_fin","etablissement","poste"};
    String listeInt[] = {"date_debut", "date_fin"};
    String libEntete[] = {"id", "date_debut", "date_fin", "description","etablissement", "remarque","secteur","poste","region","pays"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("cvExperienceChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("Id");
    pr.getFormu().getChamp("date_debut1").setLibelleAffiche("Date debut min");
    pr.getFormu().getChamp("date_fin1").setLibelleAffiche("Date fin min");
    pr.getFormu().getChamp("date_debut2").setLibelleAffiche("Date debut max");
    pr.getFormu().getChamp("date_fin2").setLibelleAffiche("Date fin max");
	pr.getFormu().getChamp("etablissement").setLibelleAffiche("&eacute;tablissement");
	//pr.getFormu().getChamp("region").setLibelleAffiche("r&eacute;gion");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste experiences</h1>
            </section>
            <section class="content">
                <form action="cvExperienceChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="chauffeur" id="chauffeur">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/cv/experience-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] =  {"Id", "Date debut", "Date fin", "Description","Etablissement","Remarque","Secteur","Poste","R&eacute;gion","pays" };
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>