<%-- 
    Document   : serviceChoix
    Created on : 9 nov. 2015, 15:27:53
    Author     : Safidimahefa Romario
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.log.*" %>


<%
    String champReturn = request.getParameter("champReturn");
    
    LogService e = new LogService();
    e.setNomTable("log_service_libelle");
    String listeCrt[] = {"id","libelle","code_service", "dr_rattache","code_dr"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "libelle","code_service", "dr_rattache","code_dr"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("serviceChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("Code Service");
    pr.getFormu().getChamp("libelle").setLibelleAffiche("Libelle");
    pr.getFormu().getChamp("code_service").setLibelleAffiche("Code Service");
    pr.getFormu().getChamp("dr_rattache").setLibelleAffiche("Direction rattach�");
    pr.getFormu().getChamp("code_dr").setLibelleAffiche("Code DR");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Service</h1>
            </section>
            <section class="content">
                <form action="raServiceChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=service/service-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[]={"Id","Libelle","Code Service", "Direction rattach�","Code DR"};
		    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>