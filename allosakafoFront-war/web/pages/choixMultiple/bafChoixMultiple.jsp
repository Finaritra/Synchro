<%-- 
    Document   : bafChoixMultiple
    Created on : 27 mai 2016, 10:14:35
    Author     : Ignafah
--%>

<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.accueil.Retour_baf"%>
<%

    Retour_baf lv = new Retour_baf();
    lv.setNomTable("RETOUR_BAF_DISPO");
    String champReturn = request.getParameter("champReturn");
    
    String listeCrt[] = {"id", "nomEmployeur","date_retour","periode_baf","flag_retour"};
    String listeInt[] = {"date_retour"};
    String libEntete[] = {"id", "nomEmployeur","date_retour","periode_baf","remarque"};

    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("flag_retour").setLibelle("Etat");
    pr.getFormu().getChamp("date_retour1").setLibelle("Date retour min");
    pr.getFormu().getChamp("date_retour2").setLibelle("Date retour max");
    pr.getFormu().getChamp("periode_baf").setLibelle("P�riode");
    pr.getFormu().getChamp("nomEmployeur").setLibelle("Employeur");
    pr.setApres("bafChoixMultiple.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste BAF disponible</h1>
            </section>
            <section class="content">
                <form action="bafChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="incident" id="incident">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/courrier/envoirecept-baf-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Employeur","Date retour","P�riode","Remarque"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
