<%-- 
    Document   : comptaCompteChoix
    Created on : 15 sept. 2015, 21:28:07
    Author     : user
--%>

<%@page import="mg.cnaps.compta.ComptaCompte"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    ComptaCompte e = new ComptaCompte();
    e.setNomTable("compta_compte");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "compte", "libelle","typeCompte","classe"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "compte", "libelle","typeCompte","classe"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("exempleChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("compte").setLibelleAffiche("Compte");
    pr.getFormu().getChamp("libelle").setLibelleAffiche("Libelle");
    pr.getFormu().getChamp("typeCompte").setLibelleAffiche("Type compte");
    pr.getFormu().getChamp("classe").setLibelleAffiche("Classe");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste compte compta </h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="cc" id="cc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=compta/compte/compte-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] =  {"id", "compte", "libelle","type compte","classe"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>