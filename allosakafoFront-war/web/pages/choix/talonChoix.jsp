<%-- 
    Document   : talonChoix
    Created on : 17 nov. 2015, 23:35:44
    Author     : Safidimahefa Romario
--%>

<%@page import="mg.cnaps.recette.TalonInfo"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    TalonInfo t = new TalonInfo();
    t.setNomTable("Talon_info_choix");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "idemployeur","employeur", "periode", "datedepot"};
    String listeInt[] = {""};
    String libEntete[] =  {"id", "idemployeur","employeur", "periode", "datedepot"};
    PageRechercheChoix pr = new PageRechercheChoix(t, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("talonChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("idemployeur").setLibelleAffiche("Id Employeur");
    pr.getFormu().getChamp("employeur").setLibelleAffiche("Employeur");
    pr.getFormu().getChamp("periode").setLibelleAffiche("Periode");
    pr.getFormu().getChamp("datedepot").setLibelleAffiche("Date de depot");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    TalonInfo[] listeP = (TalonInfo[]) pr.getRs().getResultat();
    
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Talon</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="magasin" id="magasin">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=recette/declarationnominative/talon-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"ID","ID Employeur", "Employeur", "Periode", "Date de depot"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixTalon.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Id Employeur</th>
                                    <th>Employeur</th>
                                    <th>Periode</th>
                                    <th>Date de depot</th>

                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getIdemployeur()%>" class="radio" /></td>
                                    <td align=center><%=listeP[i].getId()%></td>
                                    <td align=center><%=listeP[i].getIdemployeur()%></td>
                                    <td align=center><%=listeP[i].getEmployeur()%></td>
                                    <td align=center><%=listeP[i].getPeriode()%></td>
                                    <td align=center><%=listeP[i].getDatedepot() %></td>
                                    
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
