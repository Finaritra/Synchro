<%-- 
    Document   : listeImmoCategorie
    Created on : 1 oct. 2015, 15:49:23
    Author     : GILEADA
--%>

<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.immo.ImmoCategorie"%>
<%
    String champReturn = request.getParameter("champReturn");
    ImmoCategorie e = new ImmoCategorie();
    String listeCrt[] = {"id", "categorie", "libelle", "compte_immo", "compte_ammortissement", "dotation", "compte_session", "compte_perte", "compte_profit"};

    String listeInt[] = null;
    String libEntete[] = {"id", "categorie", "libelle", "compte_immo", "compte_ammortissement", "dotation", "compte_session", "compte_perte", "compte_profit","compte_acquisition"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 9);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeImmoCategorieChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    ImmoCategorie[] listeP = (ImmoCategorie[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste categorie immo</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="comptaTiers" id="comptaTiers">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "categorie", "libelle", "compte_immo", "compte_ammortissement", "dotation", "compte_session", "compte_perte", "compte_profit"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Categorie</th>
                                    <th>Libelle</th>
                                    <th>Compte immo</th>
                                    <th>Compte amortissement</th>
                                    <th>Dotation</th>
                                    <th>Compte cession</th>
                                    <th>Compte perte</th>
                                    <th>Compte profit</th>
                                    <th>Compte Immo en cours</th>
                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                    <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>" class="radio" /></td>
                                    <td align=left><%=listeP[i].getId()%></td>
                                    <td align=left><%=listeP[i].getCategorie()%></td>
                                    <td align=left><%=listeP[i].getLibelle()%></td>
                                    <td align=left><%=listeP[i].getCompte_immo()%></td>
                                    <td align=left><%=listeP[i].getCompte_ammortissement()%></td>
                                    <td align=left><%=listeP[i].getDotation()%></td>
                                    <td align=left><%=listeP[i].getCompte_session()%></td>
                                    <td align=left><%=listeP[i].getCompte_perte()%></td>
                                    <td align=left><%=listeP[i].getCompte_profit()%></td>
                                    <td align=left><%=listeP[i].getCompte_acquisition() %></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>