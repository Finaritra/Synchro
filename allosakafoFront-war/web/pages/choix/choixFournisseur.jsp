<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="mg.cnaps.st.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    StTiers e = new StTiers();
    e.setNomTable("st_tiers");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "matrcnaps", "raisonsocial","adresse"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "matrcnaps", "raisonsocial","adresse"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setAWhere(" AND typetiers = 'TRS000001'");
    pr.setApres("choixFournisseur.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("matrcnaps").setLibelleAffiche("Matricule CNAPS");
    pr.getFormu().getChamp("raisonsocial").setLibelleAffiche("Raison social");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste fournisseurs</h1>
            </section>
            <section class="content">
                <form action="choixFournisseur.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    
                    String libelles[] =  {"id", "Matricule CNAPS", "Raison social","Adresse"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
		    <input type="hidden" name="element" value="fournisseur"/>
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>