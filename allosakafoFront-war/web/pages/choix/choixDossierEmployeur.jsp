<%-- 
    Document   : choixDossierEmployeur
    Created on : 30 sept. 2015, 11:27:38
    Author     : user
--%>
<%@page import="mg.cnaps.accueil.Dossiers"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    Dossiers e = new Dossiers();
    e.setNomTable("sig_dossiers_employeurs");//si_dossiers_travailleurs_lib
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "dossier_matricule", "sous_prestation_code", "dossier_date_reception"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "dossier_matricule", "sous_prestation_code", "dossier_date_reception"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choixDossierEmployeur.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("dossier_matricule").setLibelleAffiche("Matricule Dossier");
    pr.getFormu().getChamp("sous_prestation_code").setLibelleAffiche("Nature dossier");
    pr.getFormu().getChamp("dossier_date_reception").setLibelleAffiche("Date reception");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Dossier</h1>
            </section>
            <section class="content">
                <form action="choixDossierEmployeur.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=dossier/dossier-employeur.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "dossier_matricule", "sous_prestation_code", "dossier_date_reception"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>