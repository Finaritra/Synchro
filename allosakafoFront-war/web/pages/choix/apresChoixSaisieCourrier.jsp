<%-- 
    Document   : apresChoixSaisieCourrier
    Created on : 12 juin 2016, 18:21:29
    Author     : Ignafah
--%>

<%@page import="mg.cnaps.paie.PaieInfoPersonnel"%>
<%@page import="mg.cnaps.sig.SigEmployeursComptes"%>
<%@page import="bean.TypeObjet"%>
<%@page import="bean.CGenUtil"%>
<%@page import="utilitaire.Utilitaire"%>
<html>
    <%
        String champRet =(String) request.getParameter("champReturn");
        String choix = (String) request.getParameter("choix");
        String cible = (String) request.getParameter("cible");
        String[] champs = Utilitaire.split(champRet, ";");
        String[] lstChoix = Utilitaire.split(choix, ";");
        if(cible!=null && cible.compareToIgnoreCase("personnel")==0){    %>       
            <script language="JavaScript">
                window.opener.document.all.<%=champs[0]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[0])%>";
                window.opener.document.all.<%=champs[1]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[1])%>";
            </script>   <%                    
        } 
        if(cible!=null && cible.compareToIgnoreCase("travailleur")==0){    %>       
            <script language="JavaScript">
                window.opener.document.all.<%=champs[0]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[0])%>";
                window.opener.document.all.<%=champs[1]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[1])%>";
            </script>   <%                    
        }
        if(cible!=null && cible.compareToIgnoreCase("employeur")==0){    %>       
            <script language="JavaScript">
                window.opener.document.all.<%=champs[0]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[0])%>";
                window.opener.document.all.<%=champs[1]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[1])%>";
            </script>   <%                    
        }
    %>
<script language="JavaScript"> 
    window.open('','_parent','');
    window.close();
</script>
</html>
