<%-- 
    Document   : talonChoixMultiple
    Created on : 17 nov. 2015, 23:35:44
--%>

<%@page import="mg.cnaps.treso.TresoPayeEspece"%>
<%@page import="mg.cnaps.recette.TalonInfo"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    TalonInfo t = new TalonInfo();
    t.setNomTable("TALON_INFO_CHOIX_ESPECE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "idemployeur", "employeur", "periode", "datedepot", "netapayer"};
    String listeInt[] = {"datedepot","netapayer"};
    String libEntete[] = {"id", "idemployeur", "employeur", "periode", "datedepot", "netapayer"};
    PageRechercheChoix pr = new PageRechercheChoix(t, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("talonChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("idemployeur").setLibelleAffiche("Matricule Employeur");
    pr.getFormu().getChamp("employeur").setLibelleAffiche("Raison social");
    pr.getFormu().getChamp("periode").setLibelleAffiche("Periode");
    pr.getFormu().getChamp("datedepot1").setLibelleAffiche("Date de depot Min");
    pr.getFormu().getChamp("datedepot2").setLibelleAffiche("Date de depot Max");
    pr.getFormu().getChamp("netapayer1").setLibelleAffiche("Montant a payer Min");
    pr.getFormu().getChamp("netapayer2").setLibelleAffiche("Montant a payer Max");
    if(request.getParameter("premier").compareToIgnoreCase("true")==0){
        pr.setAWhere(" and datedesaisie = '"+Utilitaire.dateDuJour()+"'");
    }
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    TalonInfo[] listeP = (TalonInfo[]) pr.getRs().getResultat();
    TresoPayeEspece p = new  TresoPayeEspece();
    String seq = p.makePK("", "getSeqTresoPayeEsp");
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Talon</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>&premier=false" method="post" name="magasin" id="magasin">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=recette/declarationnominative/talon-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"ID", "ID Employeur", "Employeur", "Periode", "Date de depot"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th><b>#</b></th>
                            <th><b>Id</b></th>
                            <th><b>Matricule Employeur</b></th>
                            <th><b>Raison social</b></th>
                            <th><b>Periode</b></th>
                            <th><b>Date de depot</b></th>
                            <th><b>Montant &agrave; payer</b></th>
                        </tr>
                        <%
                            for (int i = 0; i < listeP.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=Utilitaire.formatterDaty(listeP[i].getDatedesaisie()) %>;<%=listeP[i].getEmployeur() %>;<%=listeP[i].getPeriode() %>;<%=listeP[i].getAnnee() %>;<%=listeP[i].getMois() %>;<%=Utilitaire.formaterAr(listeP[i].getCotiseemployeur()) %>;<%=Utilitaire.formaterAr(listeP[i].getCotisetravailleur()) %>;<%=Utilitaire.formaterAr(listeP[i].getMajorationretard()) %>;<%=Utilitaire.formaterAr(listeP[i].getNetapayer()) %>;<%=listeP[i].getReferencepaiement()%><%=seq%>" class="radio" /></td>
                            <td align=left><%=listeP[i].getId()%></td>
                            <td align=left><%=listeP[i].getIdemployeur()%></td>
                            <td align=left><%=listeP[i].getEmployeur()%></td>
                            <td align=left><%=listeP[i].getPeriode()%></td>
                            <td align=left><%=Utilitaire.formatterDaty(listeP[i].getDatedepot())%></td>
                            <td align=right><%=Utilitaire.formaterAr(listeP[i].getNetapayer()) %></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
