<%--
    Document   : listeVehiculeChoix
    Created on : 8 sept. 2015, 15:04:58
    Author     : user
--%>

<%@page import="mg.cnaps.log.LogVehicule"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    affichage.Champ[] liste = new affichage.Champ[2];
    TypeObjet listeTypeVehicule = new TypeObjet();
    listeTypeVehicule.setNomTable("LOG_TYPE_VEHICULE");
    liste[0] = new Liste("id_type_vehicule", listeTypeVehicule, "val", "val");

    TypeObjet listeTypeCarburant = new TypeObjet();
    listeTypeCarburant.setNomTable("LOG_CARBURANT");
    liste[1] = new Liste("carburant", listeTypeCarburant, "val", "val");

    LogVehicule e = new LogVehicule();
    e.setNomTable("log_vehicule_info");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "immatriculation", "modele", "id_marque", "nombre_place", "id_type_vehicule", "carburant"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "immatriculation", "modele", "id_marque", "nombre_place", "id_type_vehicule", "carburant"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 7);
    pr.getFormu().changerEnChamp(liste);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeAllVehiculeChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("immatriculation").setLibelleAffiche("Immatriculation");
    pr.getFormu().getChamp("modele").setLibelleAffiche("Mod�le");
    pr.getFormu().getChamp("nombre_place").setLibelleAffiche("Nombre de place");
    pr.getFormu().getChamp("id_marque").setLibelleAffiche("Marque");
    pr.getFormu().getChamp("id_type_vehicule").setLibelleAffiche("Type V�hicule");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste vehicule</h1>
            </section>
            <section class="content">
                <form action="listeAllVehiculeChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="vehicule" id="vehicule">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
		    String lienTableau[] = {pr.getLien() + "?but=matieriel_roulant/vehicule/vehicule-fiche.jsp"};
		    String colonneLien[] = {"id"};
		    String libelles[] = {"Id", "Immatriculation", "Mod�le", "Marque", "Nombre de place", "Type V�hicule", "Carburant"};
		    pr.getTableau().setLien(lienTableau);
		    pr.getTableau().setColonneLien(colonneLien);
		    pr.getTableau().setLibelleAffiche(libelles);
		    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>