<%@page import="mg.cnaps.accueil.Utilisateur_vue_dr"%>
<%@ page import="user.*" %>
<%@ page import="utilisateur.VueCnapsUserDispatche" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>

<%
    String champReturn = request.getParameter("champReturn");
    Utilisateur_vue_dr e = new Utilisateur_vue_dr();
    e.setNomTable("UTILISATEUR_DIRECTION_SERVICE");
    String listeCrt[] = {"nomuser","service","libelle"};
    String listeInt[] = {""};
    String libEntete[] = {"nomuser","service","libelle"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("utilisateurChoixLibelle.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("nomuser").setLibelleAffiche("Nom Utilisateur");
    pr.getFormu().getChamp("service").setLibelleAffiche("Service");
    pr.getFormu().getChamp("libelle").setLibelleAffiche("Direction");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    Utilisateur_vue_dr[] liste = (Utilisateur_vue_dr[])pr.getRs().getResultat();
%>  
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste utilisateur</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <div id="divchck">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table" table-hover="">
                            <tbody>
                                <tr class="head">
                                    <td align="center" valign="top"></td>
                                    <td width="33%" align="center" valign="top">Utilisateur</td>
                                    <td width="33%" align="center" valign="top">Service</td>
                                    <td width="33%" align="center" valign="top">Direction</td>
                                </tr>
                                <%for(int i = 0 ; i<liste.length ; i++){%>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
                                        <td align="center">
                                            <input type="radio" value="<%=liste[i].getRefuser()%>;<%=liste[i].getNomuser() %>" name="choix" onmousedown="getChoix()" id="choix" class="radio"></td>
                                        <td width="33%" align="center"><%=liste[i].getNomuser() %></td>
                                        <td width="33%" align="center"><%=liste[i].getService() %></td>
                                        <td width="33%" align="center"><%=liste[i].getLibelle() %></td>
                                    </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
