<%@page import="mg.cnaps.paie.PaieRubrique"%>
<%@page import="mg.cnaps.commun.Sig_commune"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    PaieRubrique e = new PaieRubrique();
    e.setNomTable("paie_rubrique");
    String listeCrt[] = {"id", "code", "desce", "formule"};
    String listeInt[] = null;
    String libEntete[] = {"id", "code", "desce", "formule"};

    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("elementPaieChoix.jsp");

    pr.getFormu().getChamp("id").setLibelleAffiche("Code rubrique");
    pr.getFormu().getChamp("code").setLibelleAffiche("Code");
    pr.getFormu().getChamp("desce").setLibelleAffiche("D&eacute;signation");
    pr.getFormu().getChamp("formule").setLibelleAffiche("Formule");
    pr.setChampReturn(request.getParameter("champReturn"));

    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des rubriques de paie</h1>
            </section>
            <section class="content">
                <form action="elementPaieChoix.jsp?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="elementpaiechoix" id="elementpaiechoix">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/element/rubrique-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libEnteteAffiche[] = {"ID", "Code", "Description", "Formule"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                    out.println(pr.getTableauRecap().getHtml());
                %>
               <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>