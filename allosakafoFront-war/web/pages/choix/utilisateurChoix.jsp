<%@ page import="user.*" %>
<%@ page import="utilisateur.VueCnapsUserDispatche" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>

<%
    String champReturn = request.getParameter("champReturn");
    VueCnapsUserDispatche e = new VueCnapsUserDispatche();
    String listeCrt[] = {"id_cnaps_user","user_init","loginuser"};
    String listeInt[] = {""};
    String libEntete[] = {"id_cnaps_user","user_init","loginuser"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 1, libEntete, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("utilisateurChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("user_init").setLibelleAffiche("Initial");
    pr.getFormu().getChamp("loginuser").setLibelleAffiche("Matricule");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste utilisateur</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=employeur/employeur-fiche.jsp"};
                    String colonneLien[] = {"id_cnaps_user"};
                    String libelles[]={"Id","Initial","Matricule"};
		    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>