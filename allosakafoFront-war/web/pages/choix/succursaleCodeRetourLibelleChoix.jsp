<%-- 
    Document   : succursaleCodeChoix
    Created on : 14 sept. 2015, 16:30:22
    Author     : Cnaps

--%>
<%@page import="mg.cnaps.sig.SigSuccursales"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    SigSuccursales e = new SigSuccursales();
    e.setNomTable("sig_succursales");
    String listeCrt[] = {"id","employeur_matricule","succursale_nom", "succursale_code_fkt","succursale_adresse"};
    String listeInt[] = {""};
	String libEntete[] = {"id","employeur_matricule","succursale_nom", "succursale_code_fkt","succursale_adresse"};
    PageRecherche pr = new PageRecherche(e, request, listeCrt, listeInt,2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("succursaleCodeChoix.jsp");

    
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("employeur_matricule").setLibelleAffiche("Employeur matricule");
    pr.getFormu().getChamp("succursale_nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("succursale_code_fkt").setLibelleAffiche("Fokontany");
    pr.getFormu().getChamp("succursale_adresse").setLibelleAffiche("Adresse");
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    SigSuccursales[] liste = (SigSuccursales[])pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste succursales </h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
					out.println(pr.getTableauRecap().getHtml());

                    String libelles[] = {"id","employeur_matricule","succursale_nom", "succursale_code_fkt","succursale_adresse"};
                    pr.getTableau().setLibelleAffiche(libelles);
				%>
				
				
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixIdvaldesceRetourLibelle.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" >
                    <input type="hidden" name="suffixe" value="succursale_code">
					
					
                    <p align="center"><strong><u>LISTE</u></strong></p>
                    <div id="divchck">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table" table-hover="">
                            <thead>
									<tr class="head">
									<th>#</th>
									<th>ID</th>
									<th>Employeur matricule</th>
									<th>Succursale nom</th>
									<th>Fokontany</th>
                            		<th>Adresse</th>
                               </tr>
								 </thead>
							<tbody>
								
                                <%for(int i = 0 ; i<liste.length ; i++){%>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
                                        <td align="center">
                                        <input type="radio" value="<%=liste[i].getId()%>;<%=liste[i].getSuccursale_nom() %>" name="choix" onmousedown="getChoix()" id="choix" class="radio"></td>
										<td align=center><%=liste[i].getId()%></td>
										<td align=center><%=liste[i].getEmployeur_matricule()%></td>
										<td align=center><%=liste[i].getSuccursale_nom()%></td>
										<td align=center><%=liste[i].getSuccursale_code_fkt()%></td>
										<td align=center><%=liste[i].getSuccursale_adresse()%></td>
						        </tr>
                                <%}%>
                            </tbody>
                        </table>
                </form>
				
				
				
				
				
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
