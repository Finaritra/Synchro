<%@page import="mg.cnaps.securite.SecPersonnel"%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.recette.LocLocation"%>

<%
    String champReturn = request.getParameter("champReturn");
    SecPersonnel fact = new SecPersonnel();
    String listeCrt[] = {"id", "nom","prenom","adresse","type"};
    String listeInt[] = null;
    String libEntete[] = {"id", "nom","prenom","adresse","type"};
    PageRechercheChoix pr = new PageRechercheChoix(fact, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("personnelsecuriteChoix.jsp");
    pr.setChampReturn("");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
	
	
	SecPersonnel[] liste = (SecPersonnel[])pr.getRs().getResultat();

%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Agents</h1>
            </section>
            <section class="content">
                <form action="personnelsecuriteChoix.jsp" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/aprespersonnelsecuriteChoix.jsp" method="post" name="frmchx" id="frmchx">
                <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <p align="center"><strong><u>LISTE</u></strong></p>
                    <div id="divchck">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table" table-hover="">
                            <tbody>
                                <tr class="head">
                                    <td align="center" valign="top"></td>
                                    <td width="33%" align="center" valign="top">Id</td>
                                    <td width="33%" align="center" valign="top">Nom</td>
                                    <td width="33%" align="center" valign="top">Prenom</td>
                                    <td width="33%" align="center" valign="top">Adresse</td>
                                    <td width="33%" align="center" valign="top">Type</td>
                                </tr>
                                <%for(int i = 0 ; i<liste.length ; i++){%>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
                                        <td align="center">
                                        <input type="radio" value="<%=liste[i].getId()%>;<%=liste[i].getNom() + " " + liste[i].getPrenom() %>" name="choix" onmousedown="getChoix()" id="choix" class="radio"></td>
                                        <td width="33%" align="center"><%=liste[i].getId() %></td>
                                        <td width="33%" align="center"><%=liste[i].getNom() %></td>
                                        <td width="33%" align="center"><%=liste[i].getPrenom() %></td>
                                        <td width="33%" align="center"><%=liste[i].getAdresse() %></td>
                                        <td width="33%" align="center"><%=liste[i].getType() %></td>
                                    </tr>
                                <%}%>
                            </tbody>
                        </table>  </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>