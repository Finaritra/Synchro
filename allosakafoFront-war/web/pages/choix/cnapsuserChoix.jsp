<%-- 
    Document   : cnapsuserChoix
    Created on : 17 sept. 2015, 16:51:41
    Author     : user
--%>
<%@page import="mg.cnaps.utilisateur.CNAPSUser"%>
<%@page import="mg.cnaps.budget.BudgetProjet"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    CNAPSUser e = new CNAPSUser();
    e.setNomTable("CNAPS_USER");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id","code_dr", "username", "code_service"};
    String listeInt[] = {""};
    String libEntete[] = {"id","code_dr", "username", "code_service"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("cnapsuserChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("code_dr").setLibelleAffiche("Direction regionale");
    pr.getFormu().getChamp("username").setLibelleAffiche("Nom  utilisateur");
    pr.getFormu().getChamp("code_service").setLibelleAffiche("Code service");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste budget projet</h1>
            </section>
            <section class="content">
                <form action="cnapsuserChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but="};
                    String colonneLien[] = {"id"};
                    String libelles[] =  {"id","code_dr", "username", "code_service"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>