<%-- 
    Document   : apresChoix
    Created on : 8 juin 2016, 19:58:43
    Author     : Paul M.
--%>

<%@page import="utilitaire.Utilitaire"%>
<html>
    <%
        String champRet = (String) request.getParameter("champReturn");
        String choix = (String) request.getParameter("choix");
        String[] champs = Utilitaire.split(champRet, ";");
        String[] lstChoix = Utilitaire.split(choix, ";");
    %>
    <script language="JavaScript">
        <%for (int i = 0; i < champs.length; i++) {%>
            window.opener.document.all.<%=champs[i]%>.value = "<%=lstChoix[i]%>";
        <%}%>
        window.close();
    </script>
</html>
