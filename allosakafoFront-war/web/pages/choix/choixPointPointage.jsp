<%@page import="mg.cnaps.pointage.PointPointage"%>
<%@page import="mg.cnaps.accueil.Dossiers"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    PointPointage e = new PointPointage ();
    e.setNomTable("POINT_POINTAGE_LIBELLE2");
//    e.setNomTable("sig_dossiers_lib");//sig_dossiers_travailleurs_lib
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "id_personnel", "id_type", "sig_dr", "daty", "heure", "date_heure"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "id_personnel", "id_type", "sig_dr", "daty", "heure", "date_heure"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choixPointPointage.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("id_personnel").setLibelleAffiche("personnel");
    pr.getFormu().getChamp("id_type").setLibelleAffiche("Type");
    pr.getFormu().getChamp("sig_dr").setLibelleAffiche("Sig dr");
    pr.getFormu().getChamp("date_heure").setLibelleAffiche("date et heure");
    pr.getFormu().getChamp("daty1").setLibelleAffiche("Min date");
    pr.getFormu().getChamp("daty2").setLibelleAffiche("Max date");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste point de pointage</h1>
            </section>
            <section class="content">
                <form action="choixPointPointage.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=dossier/dossier-employeur.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "Personnel", "Type", "Sig Dr", "date", "heure", "date et heure"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>