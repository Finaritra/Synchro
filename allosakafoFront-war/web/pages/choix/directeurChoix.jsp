<%@page import="utilisateur.VueCNAPSUser"%>
<%@page import="affichage.PageRecherche"%>
<%

    VueCNAPSUser cnaps = new VueCNAPSUser();
    cnaps.setNomTable("vuecnapsuser");
    String listeCrt[] = {"refuser", "loginuser", "nomuser", "adruser", "teluser", "idrole", "agent_disponible",};
    String listeInt[] = null;
    String libEntete[] = {"refuser", "loginuser", "nomuser", "adruser", "teluser", "idrole", "agent_disponible",};
    PageRecherche pr = new PageRecherche(cnaps, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("directeurChoix.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    VueCNAPSUser[] listeU = (VueCNAPSUser[]) pr.getRs().getResultat();

%>
<html>
     <head>
		<meta charset="UTF-8">
		<title>Cynthia 2.0 | CNaPS</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<jsp:include page='../elements/css.jsp'/>
	 </head>
	 <body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<section class="content-header">
				<h1>Liste Utilisateur</h1>
			</section>
			<section class="content">
				<form action="directeurChoix.jsp?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="drChoix" id="drChoix">
					<% out.println(pr.getFormu().getHtmlEnsemble());%>
				</form>
				<%
					
					
					out.println(pr.getTableauRecap().getHtml());
				%>
				<form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
					<input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
					<table class="table table-bordered">
						<tr class=info>
							<th>#</th>
							<th>ID</th>
							<th>Nom</th>
							<th>Login</th>
                                                        <th>Role</th>
                                                        <th>Telephone</th>
                                                        <th>Agent disponible</th>
						</tr>
						<%
							for (int i = 0; i < listeU.length; i++) {
						%>
						<tr>
							<td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeU[i].getRefuser()%>" class="radio" /></td>
							<td align=center><%=listeU[i].getRefuser()%></td>
							<td align=center><%=listeU[i].getNomuser()%></td>
							<td align=center><%=listeU[i].getLoginuser()%></td>
                                                        <td align=center><%=listeU[i].getIdrole()%></td>
                                                        <td align=center><%=listeU[i].getTeluser()%></td>
                                                        <td align=center><%=listeU[i].getAgent_disponible()%></td>
						</tr>
						<%}%>
					</table>
				</form>
				<%
					out.println(pr.getBasPage());
				%>
			</section>
		</div>
		<jsp:include page='../elements/js.jsp'/>
	 </body>
</html>