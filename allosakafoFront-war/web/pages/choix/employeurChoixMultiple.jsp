<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>


<%
    VueSigEmployeur e = new VueSigEmployeur();
    e.setNomTable("vuesigemployeurs");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[]={"id","nom","employeur_telephone", "code_dr"};
    String listeInt[]=null;
    String libEntete[]={"id","nom","employeur_telephone", "code_dr"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("employeurChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("employeur_telephone").setLibelleAffiche("T�l�phone");
    pr.getFormu().getChamp("code_dr").setLibelleAffiche("Direction r�gionale");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    VueSigEmployeur[] listeP = (VueSigEmployeur[]) pr.getRs().getResultat();
    //"matrcnaps;raisonsocial;nif;stat;adresse;taxe;codeposte"
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste employeurs</h1>
            </section>
            <section class="content">
                <form action="employeurChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=employeur/employeur-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[]={"Id","Nom", "T�l�phone", "Direction r�gionale"};
		    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                    //
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixComptee.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Nom</th>
                                    <th>T�l�phone</th>
                                    <th>Direction r�gionale</th>
                                    <th>Nif</th>
                                    <th>Stat</th>

                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                                        //"matrcnaps;raisonsocial;nif;stat;adresse;taxe;codeposte"
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getEmp_matricule() == null || listeP[i].getEmp_matricule().equals("")?"-": listeP[i].getEmp_matricule() %>;<%=listeP[i].getNom() == null || listeP[i].getNom().equals("") ?"-": listeP[i].getNom()%>;<%=listeP[i].getEmployeur_nif() == null || listeP[i].getEmployeur_nif().equals("") ?"-": listeP[i].getEmployeur_nif()%>;<%=listeP[i].getEmployeur_num_stat() == null || listeP[i].getEmployeur_num_stat().equals("") ?"-": listeP[i].getEmployeur_num_stat()%>;<%=listeP[i].getEmployeur_adresse_lot() == null || listeP[i].getEmployeur_adresse_lot().equals("") ?"-": listeP[i].getEmployeur_adresse_lot()%>" class="radio" /></td>
                                    <td align=center><%=listeP[i].getId()%></td>
                                    <td align=center><%=listeP[i].getNom()%></td>
                                    <td align=center><%=listeP[i].getEmployeur_telephone()%></td>
                                    <td align=center><%=listeP[i].getCode_dr() %></td>
                                    <td align=center><%=listeP[i].getEmployeur_nif() %></td>
                                    <td align=center><%=listeP[i].getEmployeur_num_stat() %></td>
                                    
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
