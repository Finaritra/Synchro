<%-- 
    Document   : budgetDepenseChoix
    Created on : 14 sept. 2015, 17:35:26
    Author     : Tafitasoa
--%>
<%@page import="mg.cnaps.budget.BudgetDepenseLibelle"%>
<%@page import="mg.cnaps.budget.BudgetDepense"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    BudgetDepenseLibelle e = new BudgetDepenseLibelle();
    e.setNomTable("budget_depense_view_dr");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "projet","code", "dr","compte","montant_credit","compte_numero","exercice"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "projet","code", "dr","compte","montant_credit","compte_numero","exercice"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("budgetDepenseChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("projet").setLibelleAffiche("Projet");
    pr.getFormu().getChamp("dr").setLibelleAffiche("Direct. R�gionale");
    
    String exercice=Utilitaire.getAnneeEnCours();
    
    pr.setAWhere(" and exercice = "+exercice);
    
    pr.getFormu().getChamp("code").setLibelleAffiche("Code Projet");
    pr.getFormu().getChamp("compte").setLibelleAffiche("Intitul� Compte");
    pr.getFormu().getChamp("montant_credit").setLibelleAffiche("Montant Credit");
    pr.getFormu().getChamp("compte_numero").setLibelle("Numero Compte");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste budget Depense</h1>
            </section>
            <section class="content">
                <form action="budgetDepenseChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bdDep" id="bdDep">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=budget/depense/depense-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "projet","Code Projet", "Direct. R�gionale","compte","montant_credit","N�Compte","Exercice"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>