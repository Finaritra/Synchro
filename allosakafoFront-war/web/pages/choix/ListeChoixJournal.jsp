<%-- 
    Document   : listeChoixJounal
    Created on : 5 juin 2016, 16:06:58
    Author     : Paul M.
--%>


<%@page import="mg.cnaps.comptamatiere.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    CmJournal devis = new CmJournal();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "debut","fin"};
    String libEntete[] = {"id", "debut","fin","remarque"};
    PageRechercheChoix pr = new PageRechercheChoix(devis, request, listeCrt, null, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("apresChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des journaux</h1>
            </section>
            <section class="content">
                <form action="listeChoixJournal.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bobj" id="bobj">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=compta-matiere/fiche-journal.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=compta-matiere/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

