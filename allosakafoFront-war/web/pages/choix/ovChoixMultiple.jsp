<%-- 
    Document   : ovChoix
    Created on : 15 sept. 2015, 15:33:06
    Author     : user
--%>
<%@page import="mg.cnaps.st.StDemandeAppro"%>
<%@page import="mg.cnaps.treso.TresoOv"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    TresoOv op = new TresoOv();
    op.setNomTable("TRESO_OV_INFO");
    String listeCrt[] = {"id", "libelle", "modedepayement", "comptesociete", "daty", "numerotation"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id","numerotation", "libelle", "modedepayement", "comptesociete", "daty", "montant"};
    PageRechercheChoix pr = new PageRechercheChoix(op, request, listeCrt, listeInt,3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("ovChoixMultiple.jsp");
    pr.setAWhere(" and etat = 11 and montant > 0");
    pr.getFormu().getChamp("id").setLibelleAffiche("Id");
    pr.getFormu().getChamp("libelle").setLibelleAffiche("Libelle");
    pr.getFormu().getChamp("modedepayement").setLibelleAffiche("Mode de paiement");
    pr.getFormu().getChamp("comptesociete").setLibelleAffiche("Compte societe");
    pr.getFormu().getChamp("daty1").setLibelleAffiche("Date Min");
    pr.getFormu().getChamp("daty2").setLibelleAffiche("Date Max");
    pr.getFormu().getChamp("numerotation").setLibelleAffiche("Numerotation OV");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    TresoOv[] liste = (TresoOv[]) pr.getRs().getResultat();
%>
  <!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste ordre de virement</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="ov" id="ov">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th>#</th>
                            <th>Id</th>
                            <th>Numerotation OV</th>
                            <th>Libelle</th>
                            <th>Mode de paiement</th>
                            <th>COmpte caisse</th>
                            <th>Date OV</th>
                            <th>Montant</th>
                        </tr>
                        <%
                        for (int i = 0; i < liste.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=liste[i].getId()%>;<%=liste[i].getLibelle() %>" class="radio" /></td>
                            <td align=center><%=liste[i].getId()%></td>
                            <td align=center><%=liste[i].getNumerotation() %></td>
                            <td align=center><%=liste[i].getLibelle() %></td>
                            <td align=center><%=liste[i].getModedepayement() %></td>
                            <td align=center><%=liste[i].getComptesociete() %></td>
                            <td align=center><%=Utilitaire.formatterDaty(liste[i].getDaty()) %></td>
                            <td align=center><%=Utilitaire.formaterAr(liste[i].getMontant()) %></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
