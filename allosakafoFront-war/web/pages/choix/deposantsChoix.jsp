
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    TypeObjet e = new TypeObjet();
    e.setNomTable("sig_deposants");
    String listeCrt[] = {"id", "val", "desce"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "val", "desce"};
    PageRecherche pr = new PageRecherche(e, request, listeCrt, listeInt, 2, libEntete, 2);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("deposantsChoix.jsp");
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("val").setLibelleAffiche("Valeur");
    pr.getFormu().getChamp("desce").setLibelleAffiche("Description");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    TypeObjet[] listeP = (TypeObjet[]) pr.getRs().getResultat();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste d&eacute;posants</h1>
            </section>
            <section class="content">
                <form action="deposantsChoix.jsp?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="deposantsChoix" id="deposantsChoix">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=configuration/fiche_idvaldesce.jsp"};
                    String colonneLien[] = {"id"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th>#</th>
                            <th>ID</th>
                            <th>Valeur</th>
                            <th>Description</th>
                        </tr>
                        <%
                            for (int i = 0; i < listeP.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>" class="radio" /></td>
                            <td align=center><%=listeP[i].getId()%></td>
                            <td align=center><%=listeP[i].getVal()%></td>
                            <td align=center><%=listeP[i].getDesce()%></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <%
                    out.println(pr.getBasPage());
                %>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>