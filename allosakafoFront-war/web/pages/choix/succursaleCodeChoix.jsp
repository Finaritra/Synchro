<%-- 
    Document   : succursaleCodeChoix
    Created on : 14 sept. 2015, 16:30:22
    Author     : Cnaps

--%>
<%@page import="mg.cnaps.sig.SigSuccursales"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    SigSuccursales e = new SigSuccursales();
    e.setNomTable("sig_succursales");
    String listeCrt[] = {"id","employeur_matricule","succursale_nom", "succursale_code_fkt","succursale_adresse"};
    String listeInt[] = {""};
	String libEntete[] = {"id","employeur_matricule","succursale_nom", "succursale_code_fkt","succursale_adresse"};
    PageRecherche pr = new PageRecherche(e, request, listeCrt, listeInt,2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("succursaleCodeChoix.jsp");

    
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("employeur_matricule").setLibelleAffiche("Employeur matricule");
    pr.getFormu().getChamp("succursale_nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("succursale_code_fkt").setLibelleAffiche("Fokontany");
    pr.getFormu().getChamp("succursale_adresse").setLibelleAffiche("Adresse");
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    SigSuccursales[] listeP = (SigSuccursales[]) pr.getRs().getResultat();
%>
  <!DOCTYPE html>
<html>
     <head>
		<meta charset="UTF-8">
		<title>Cynthia 2.0 | CNaPS</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<jsp:include page='../elements/css.jsp'/>
	 </head>
	 <body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<section class="content-header">
				<h1>Liste succursales</h1>
			</section>
			<section class="content">
				<form action="<%=pr.getApres()%>?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="succursale" id="succursale">
					<% out.println(pr.getFormu().getHtmlEnsemble());%>
				</form>
				<%
//					String lienTableau[] = {pr.getLien() + "?but=sto/fiche_idvaldesce.jsp"};
//					String colonneLien[] = {"id"};
//					pr.getTableau().setLien(lienTableau);
//					pr.getTableau().setColonneLien(colonneLien);
					out.println(pr.getTableauRecap().getHtml());
				%>
				<form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
					<input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
					<table class="table table-bordered">
						<tr class=info>
							<th>#</th>
							<th>ID</th>
							<th>Employeur matricule</th>
							<th>Succursale nom</th>
							<th>Fokontany</th>
                                                        <th>Adresse</th>
							
						</tr>
						<%
							for (int i = 0; i < listeP.length; i++) {
						%>
						<tr>
							<td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>" class="radio" /></td>
							<td align=center><%= Utilitaire.champNull(listeP[i].getId())%></td>
							<td align=center><%= Utilitaire.champNull(listeP[i].getEmployeur_matricule())%></td>
							<td align=center><%= Utilitaire.champNull(listeP[i].getSuccursale_nom())%></td>
							<td align=center><%= Utilitaire.champNull(listeP[i].getSuccursale_code_fkt())%></td>
                                                        <td align=center><%= Utilitaire.champNull(listeP[i].getSuccursale_adresse())%></td>
						</tr>
						<%}%>
					</table>
				</form>
				<%
					out.println(pr.getBasPage());
				%>
				</section>
		</div>
		<jsp:include page='../elements/js.jsp'/>
	 </body>
</html>