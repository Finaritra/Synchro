<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
	Sig_region e = new Sig_region();
    e.setNomTable("sig_region");
    String listeCrt[] = {"id", "val","desce", "code_faritany"};
    String listeInt[] = {""};
	String libEntete[] = {"id", "val","desce", "code_faritany"};
    PageRecherche pr = new PageRecherche(e, request, listeCrt, listeInt,2, libEntete, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("code_regionChoix.jsp");
    pr.getFormu().getChamp("id").setLibelleAffiche("Code r�gion");
    pr.getFormu().getChamp("val").setLibelleAffiche("Nom r�gion");
    pr.getFormu().getChamp("desce").setLibelleAffiche("Description");
    pr.getFormu().getChamp("code_faritany").setLibelleAffiche("Code faritany");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    Sig_region[] listeP = (Sig_region[]) pr.getRs().getResultat();
%>
  <!DOCTYPE html>
<html>
     <head>
		<meta charset="UTF-8">
		<title>Cynthia 2.0 | CNaPS</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<jsp:include page='../elements/css.jsp'/>
	 </head>
	 <body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<section class="content-header">
				<h1>Liste regions</h1>
			</section>
			<section class="content">
				<form action="code_regionChoix.jsp?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="code_regionChoix" id="code_regionChoix">
					<% out.println(pr.getFormu().getHtmlEnsemble());%>
				</form>
				<%
					String lienTableau[] = {pr.getLien() + "?but=configuration/fiche_idvaldesce.jsp"};
					String colonneLien[] = {"id"};
					pr.getTableau().setLien(lienTableau);
					pr.getTableau().setColonneLien(colonneLien);
					out.println(pr.getTableauRecap().getHtml());
				%>
				<form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
					<input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
					<table class="table table-bordered">
						<tr class=info>
							<th>#</th>
							<th>Code r�gion</th>
							<th>Nom r�gion</th>
							<th>Description</th>
							<th>Code faritany</th>
							
						</tr>
						<%
							for (int i = 0; i < listeP.length; i++) {
						%>
						<tr>
							<td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>" class="radio" /></td>
							<td align=center><%=listeP[i].getId()%></td>
							<td align=center><%=listeP[i].getVal()%></td>
							<td align=center><%=listeP[i].getDesce()%></td>
							<td align=center><%=listeP[i].getCode_faritany()%></td>
						</tr>
						<%}%>
					</table>
				</form>
				<%
					out.println(pr.getBasPage());
				%>
				</section>
		</div>
		<jsp:include page='../elements/js.jsp'/>
	 </body>
</html>