<%-- 
    Document   : pointDemandeAutoChoix
    Created on : 27 oct. 2015, 19:26:40
    Author     : user
--%>
<%@page import="mg.cnaps.pointage.PointDemandeAutorisation"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    PointDemandeAutorisation e = new PointDemandeAutorisation();
    e.setNomTable("point_dem_aut_libelle");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "id_type_autorisation", "id_personnel", "id_droit","date_debut", "date_fin"};
    String listeInt[] = null;
    String libEntete[] = {"id", "id_type_autorisation", "id_personnel", "id_droit","date_debut", "date_fin"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setAWhere(" AND ETAT = 11");
    pr.setApres("pointDemandeAutoChoix.jsp");
    pr.getFormu().getChamp("id_type_autorisation").setLibelle("Type d'autorisation");
    pr.getFormu().getChamp("id_personnel").setLibelle("Personnel");
    pr.getFormu().getChamp("id_droit").setLibelle("Droit");
    pr.getFormu().getChamp("date_debut").setLibelle("Date de d�but");
    pr.getFormu().getChamp("date_fin").setLibelle("Date de fin");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste demande autorisation</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="rubrique" id="rubrique">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "Type autorisation", "Personnel", "Droit","Date d�but", "Date fin"};
		    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>