<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.commun.*" %>



<%
    String champReturn = request.getParameter("champReturn");
    Sig_fokontany e = new Sig_fokontany();
    e.setNomTable("SIG_FOKONTANY_LIBELLE");
    String listeCrt[] = {"id","val","desce", "code_commune"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "val","desce", "code_commune"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("code_fkt-choix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("Code fokontany");
    pr.getFormu().getChamp("val").setLibelleAffiche("Nom fokontany");
    pr.getFormu().getChamp("desce").setLibelleAffiche("Description");
    pr.getFormu().getChamp("code_commune").setLibelleAffiche("Code commune");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
	Sig_fokontany[] liste = (Sig_fokontany[])pr.getRs().getResultat();

%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Fokontany</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
				         String lienTableau[] = {pr.getLien() + "?but=employeur/employeur-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[]={"Id","Nom","Description", "Code commune"};
					pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
             
				%>
				
				
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixIdvaldesceRetourLibelle.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <input type="hidden" name="suffixe" value="fokotany">
					
					
                    <p align="center"><strong><u>LISTE</u></strong></p>
                    <div id="divchck">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table" table-hover="">
                             <thead>
								 <tr class="head">
										<th align="center" valign="top"></th>
										<th width="33%" align="center" valign="top">Id</th>
										<th width="33%" align="center" valign="top">Nom</th>
										<th width="33%" align="center" valign="top">Description</th>
										<th width="33%" align="center" valign="top">Code commune</th>
								 </tr>
							 </thead>
							<tbody>
                               
                                <%for(int i = 0 ; i<liste.length ; i++){%>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
                                        <td align="center">
                                        <input type="radio" value="<%=liste[i].getId()%>;<%=liste[i].getVal() %>" name="choix" onmousedown="getChoix()" id="choix" class="radio"></td>
                                        <td width="33%" align="center"><%= liste[i].getId()%></td>
                                        <td width="33%" align="center"><%=liste[i].getVal() %></td>
                                        <td width="33%" align="center"><%=liste[i].getDesce() %></td>
                                       <td width="33%" align="center"><%=liste[i].getCode_commune() %></td>
                                     </tr>
                                <%}%>
                            </tbody>
                        </table>
                </form>
				
				
				
				
				
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
