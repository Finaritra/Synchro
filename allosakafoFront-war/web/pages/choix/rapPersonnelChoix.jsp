<%-- 
    Document   : deplacementChoix
    Created on : 8 sept. 2015, 21:26:25
    Author     : user
--%>
<%@page import="mg.cnaps.rapport.RapPersonnelValide"%>
<%@page import="mg.cnaps.log.LogPersonnelValide"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    RapPersonnelValide e = new RapPersonnelValide();
    e.setNomTable("RAP_PERSONNEL_VALIDE");
    String champReturn = request.getParameter("champReturn");
    
    String listeCrt[] = {"id", "matricule", "nom", "prenom", "direction", "service","description_poste"};
    String listeInt[] = null;
    String libEntete[] = {"id", "matricule", "nom", "prenom", "direction", "service","description_poste"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
   if(champReturn.compareTo("superviseur")==0){
     pr.setAWhere(" AND description_poste like 'DIRECTEUR DE DEPARTEMENT'");
   }
    if(champReturn.compareTo("chefprojet")==0){
     pr.setAWhere(" AND description_poste != 'DIRECTEUR DE DEPARTEMENT'");
   }
     
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("rapPersonnelChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("prenom").setLibelleAffiche("Pr�nom");
    pr.getFormu().getChamp("direction").setLibelleAffiche("Direction");
    pr.getFormu().getChamp("service").setLibelleAffiche("Service");
    
    
    pr.getFormu().getChamp("description_poste").setLibelleAffiche("Poste");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des personnels</h1>
            </section>
            <section class="content">
                <form action="rapPersonnelChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="chauffeur" id="chauffeur">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "Matricule", "nom", "prenom", "direction", "service","Poste"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>"/>
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>