<%--
    Document   : listeVehiculeChoix
    Created on : 8 sept. 2015, 15:04:58
    Author     : user
--%>

<%@page import="mg.cnaps.log.LogVehicule"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    try {
	String iddr = request.getParameter("iddr");
	String dateDep = request.getParameter("dateDep");
	String heureDep = request.getParameter("heureDep");
	String dateArr = request.getParameter("dateArr");
	String heureArr = request.getParameter("heureArr");

	String req_date = " and not exists ("
		+ "select * from LOG_VEH_PLAN_DEPL_DATE_DR l"
		+ " where (TO_DATE(l.DATE_DEPART||l.HEURE_DEPART,'dd/mm/yyHH24:mi') <= To_date('" + dateDep + " " + heureDep + "','dd/mm/yy HH24:mi')"
		+ " and TO_DATE(l.DATE_ARRIVEE||l.HEURE_ARRIVEE,'dd/mm/yyHH24:mi') >= To_date('" + dateDep + " " + heureDep + "','dd/mm/yy HH24:mi'))"
		+ " or (TO_DATE(l.DATE_DEPART||l.HEURE_DEPART,'dd/mm/yyHH24:mi') <= To_date('" + dateArr + " " + heureArr + "','dd/mm/yy HH24:mi')"
		+ " and TO_DATE(l.DATE_ARRIVEE||l.HEURE_ARRIVEE,'dd/mm/yyHH24:mi') >= To_date('" + dateArr + " " + heureArr + "','dd/mm/yy HH24:mi'))"
		+ " and l.ETAT <> 1"
		+ " and LOG_VEH_PLAN_DEPL_DATE_DR.id = l.id)"
		+ " and LOG_VEH_PLAN_DEPL_DATE_DR.IDDETENTEUR='" + iddr + "'";
	LogVehicule e = new LogVehicule();
	e.setNomTable("LOG_VEH_PLAN_DEPL_DATE_DR");
	String champReturn = request.getParameter("champReturn");
	String listeCrt[] = {"id", "immatriculation", "modele", "id_marque", "nombre_place"};
	String listeInt[] = {};
	String libEntete[] = {"id", "immatriculation", "modele", "id_marque", "nombre_place"};
	String colDefaut[] = {"id", "immatriculation", "modele", "id_marque", "nombre_place"};
	String somDefaut[] = {};
	PageRechercheGroupe pr = new PageRechercheGroupe(e, request, listeCrt, listeInt, 3, colDefaut, somDefaut, 5, 0);
	pr.setAWhere(req_date);
	pr.setUtilisateur((user.UserEJB) session.getValue("u"));
	pr.setLien((String) session.getValue("lien"));
//    pr.setApres("listeVehiculeChoix.jsp");
////    pr.setChampReturn(champReturn);
//    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
//    pr.getFormu().getChamp("immatriculation").setLibelleAffiche("Immatriculation");
//    pr.getFormu().getChamp("modele").setLibelleAffiche("Mod�le");
//    pr.getFormu().getChamp("nombre_place").setLibelleAffiche("Nombre de place");
//    pr.getFormu().getChamp("id_marque").setLibelleAffiche("Marque");
	pr.creerObjetPage();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste vehicule</h1>
            </section>
            <section class="content">
                <form action="listeVehiculeChoix.jsp?champReturn=<%=champReturn%>" method="post" name="vehicule" id="vehicule">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
		    String lienTableau[] = {pr.getLien() + "?but=matieriel_roulant/vehicule/vehicule-fiche.jsp"};
		    String colonneLien[] = {"id"};
		    String libelles[] = {"Id", "Immatriculation", "Mod�le", "Marque", "Nombre de place"};
		    pr.getTableau().setLien(lienTableau);
		    pr.getTableau().setColonneLien(colonneLien);
		    pr.getTableau().setLibelleAffiche(libelles);
		    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=champReturn%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
<% } catch (Exception e) { %>
<script>
    alert("Date/Heure de d�part ou Date/Heure d'arriv�e invalide");
    window.close();
</script>
<% } %>