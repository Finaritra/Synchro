<%-- 
    Document   : sigPersonnesRetourCompletChoix
    Created on : 13 juil. 2016, 11:28:18
    Author     : Safidimahefa
--%>
<%@page import="mg.cnaps.sig.SigPersonnes"%>
<%@page import="mg.cnaps.medecine.MedConsultation"%>
<%@page import="mg.cnaps.medecine.MedMedecin"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    SigPersonnes e = new SigPersonnes();
    e.setNomTable("SIG_PERSONNE_LIBELLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "pers_nom", "pers_prenom", "pers_sexe", "pers_numero_cin"};
    String listeInt[] = {""};
    String libEntete[] =  {"id", "pers_nom", "pers_prenom", "pers_sexe", "pers_numero_cin"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sigPersonnesRetourCompletChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("pers_nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("pers_prenom").setLibelleAffiche("Pr�nom");
    pr.getFormu().getChamp("pers_sexe").setLibelleAffiche("Sexe");
    pr.getFormu().getChamp("pers_numero_cin").setLibelleAffiche("Numero CIN");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    SigPersonnes[] liste = (SigPersonnes[])pr.getRs().getResultat();

%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Personnes</h1>
            </section>
            <section class="content">
                <form action="sigPersonnesRetourCompletChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personne" id="personne">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=personnne/personne-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Nom", "Pr�nom", "Sexe", "Numero CIN"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    //out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixRetourCompletPersonnes.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <p align="center"><strong><u>LISTE</u></strong></p>
                    <div id="divchck">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table" table-hover="">
                            <tbody>
                                <tr class="head">
                                    <td align="center" valign="top"></td>
                                    <td width="33%" align="center" valign="top">Id</td>
                                    <td width="33%" align="center" valign="top">Nom</td>
                                    <td width="33%" align="center" valign="top">Pr�nom</td>
                                    <td width="33%" align="center" valign="top">Sexe</td>
                                    <td width="33%" align="center" valign="top">Numero CIN</td>
                                </tr>
                                <%for(int i = 0 ; i<liste.length ; i++){
                                   String nom = liste[i].getPers_nom();
                                   nom = (liste[i].getPers_prenom() == null || liste[i].getPers_prenom() == "") ? nom : nom+" "+liste[i].getPers_prenom();
                                   String datenaissance = (liste[i].getPers_date_nais() == null) ? "Aucun" : Utilitaire.datetostring(liste[i].getPers_date_nais());
                                   String sexe = liste[i].getPers_sexe();
                                   String cin = (liste[i].getPers_numero_cin() == null || liste[i].getPers_numero_cin() == "") ? "Aucun" : liste[i].getPers_numero_cin();
                                   String datecin = (liste[i].getPers_date_cin() == null) ? "Aucun" : Utilitaire.datetostring(liste[i].getPers_date_cin());
                                   String adresse = (liste[i].getPers_adresse_lot() == null || liste[i].getPers_adresse_lot() == "") ? "Aucun" : liste[i].getPers_adresse_lot();
                                
                                %>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
                                        <td align="center">
                                        <input type="radio" value="<%=liste[i].getId()%>;<%=nom %>;<%= datenaissance %>;<%= sexe %>;<%= cin %>;<%= datecin %>;<%= adresse %>;<%=pr.getChampReturn()%>" name="choix" onmousedown="getChoix()" id="choix" class="radio"></td>
                                        <td width="33%" align="center"><%=liste[i].getId() %></td>
                                        <td width="33%" align="center"><%=liste[i].getPers_nom()%></td>
                                        <td width="33%" align="center"><%=liste[i].getPers_prenom()%></td>
                                        <td width="33%" align="center"><%=liste[i].getPers_sexe() %></td>
                                        <td width="33%" align="center"><%=liste[i].getPers_numero_cin() %></td>
                                    </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>        
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
