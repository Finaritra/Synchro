<%-- 
    Document   : apresChoixFactureMultiple
    Created on : 9 juin 2016, 20:22:35
    Author     : Joe
--%>
<%@page import="user.UserEJB"%>
<%@page import="mg.cnaps.accueil.OPComplet"%>
<%@page import="bean.CGenUtil"%>
<%@page import="mg.cnaps.recette.Facture"%>
<%@page import="utilitaire.Utilitaire"%>
<html>
    <%
        try{
        UserEJB u = (UserEJB) session.getAttribute("u");
        String champReturn = request.getParameter("champReturn");
        String[] choix = request.getParameterValues("choix");
        
        String req = Utilitaire.tabToString(choix, "'", ",");
        u.attacherOPFactureAppro(champReturn, req);
    %>
    <script language="JavaScript">
        window.close();
    </script>
    <%}catch(Exception ex){
        ex.printStackTrace();
        throw new Exception(ex.getMessage());
    }%>
</html>
