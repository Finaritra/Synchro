<%-- 
    Document   : budgetTefChoix
    Created on : 14 sept. 2015, 17:35:26
    Author     : user
--%>
<%@page import="mg.cnaps.budget.BudgetTef"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    BudgetTef e = new BudgetTef();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "daty", "libelle","montant","budget", "typetef", "tefliee"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "daty", "libelle","montant","budget", "typetef", "tefliee"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("budgetTefChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("daty").setLibelleAffiche("Date");
    pr.getFormu().getChamp("libelle").setLibelleAffiche("Libelle");
    pr.getFormu().getChamp("montant").setLibelleAffiche("Montant");
    pr.getFormu().getChamp("budget").setLibelleAffiche("Budget");
    pr.getFormu().getChamp("typetef").setLibelleAffiche("Type tef");
    pr.getFormu().getChamp("tefliee").setLibelleAffiche("Tef li�e");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste budget tef</h1>
            </section>
            <section class="content">
                <form action="budgetTefChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bdTef" id="bdTef">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=budget/programme/objectif-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "daty", "libelle","montant","budget","typetef", "tefliee"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>