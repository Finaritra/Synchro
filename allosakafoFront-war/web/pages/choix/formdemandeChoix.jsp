<%-- 
    Document   : formdemandeChoix
    Created on : 13 oct. 2015, 15:38:42
    Author     : user
--%>
<%@page import="mg.cnaps.formation.FormDemande"%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.formation.FormOffreFormation"%>
<%@page import="mg.cnaps.formation.FormEtablissement"%>
<%
    String champReturn = request.getParameter("champReturn");
    FormDemande e = new FormDemande();
//    e.setNomTable("");
    String listeCrt[]={"id","id_etablissement", "form_domaine","log_service","id_type","date_debut","date_fin","niveau_requis","idmatiere"};
    String listeInt[]=null;
    String libEntete[]={"id","id_etablissement", "form_domaine","log_service","id_type","date_debut","date_fin","niveau_requis","idmatiere"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 9);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("formdemandeChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("id_etablissement").setLibelleAffiche("Etablissement");
    pr.getFormu().getChamp("form_domaine").setLibelleAffiche("Domaine");
    pr.getFormu().getChamp("log_service").setLibelleAffiche("Service");
    pr.getFormu().getChamp("id_type").setLibelleAffiche("Type");
    pr.getFormu().getChamp("date_debut").setLibelleAffiche("Date d�but");
    pr.getFormu().getChamp("date_fin").setLibelleAffiche("Date fin");
    pr.getFormu().getChamp("niveau_requis").setLibelleAffiche("Niveau requis");
    pr.getFormu().getChamp("idmatiere").setLibelleAffiche("Mati�re");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Demande de formation</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="etab" id="etab">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
//                    String lienTableau[] = {pr.getLien() + "?but=employeur/employeur-fiche.jsp"};
//                    String colonneLien[] = {"id"};
                    String libelles[]={"ID","Etablissement", "Domaine","Service","Type","Date debut","Date fin","Niveau requis","Mati�re"};
//		    pr.getTableau().setLien(lienTableau);
//                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
