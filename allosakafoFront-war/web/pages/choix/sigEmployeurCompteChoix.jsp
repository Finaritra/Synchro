<%-- 
    Document   : sigEmployeurCompteChoix
    Created on : 11 sept. 2015, 16:34:05
    Author     : user
--%>
<%@page import="mg.cnaps.compta.ComptaEcriture"%>
<%@page import="mg.cnaps.sig.SigEmployeursComptes"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    SigEmployeursComptes e = new SigEmployeursComptes();

    e.setNomTable("sig_employeurs_comptes_libelle");
    String listeCrt[] = {"id","mere","succursale_code", "banque_code","agence_code","compte_numero","compte_cle","compte_date"};
    String listeInt[] = {"compte_date"};
    String libEntete[] = {"id","mere","succursale_code", "banque_code","agence_code","compte_numero","compte_cle","compte_date"};
    PageRecherche pr = new PageRecherche(e, request, listeCrt, listeInt,3, libEntete,8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sigEmployeurCompteChoix.jsp");

    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("mere").setLibelleAffiche("Compta �criture");
    pr.getFormu().getChamp("succursale_code").setLibelleAffiche("Succursale code");
    pr.getFormu().getChamp("banque_code").setLibelleAffiche("Banque code");
    pr.getFormu().getChamp("agence_code").setLibelleAffiche("Code agence");
    pr.getFormu().getChamp("compte_numero").setLibelleAffiche("Numero compte employeur");
    pr.getFormu().getChamp("compte_cle").setLibelleAffiche("Compte cl�");
    pr.getFormu().getChamp("compte_date1").setLibelleAffiche("Date 1");
    pr.getFormu().getChamp("compte_date2").setLibelleAffiche("Date 2");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    SigEmployeursComptes[] liste = (SigEmployeursComptes[]) pr.getRs().getResultat();
%>
  <!DOCTYPE html>
<html>
     <head>
		<meta charset="UTF-8">
		<title>Cynthia 2.0 | CNaPS</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<jsp:include page='../elements/css.jsp'/>
	 </head>
	 <body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<section class="content-header">
				<h1>Liste ecriture</h1>
			</section>
			<section class="content">
				<form action="<%=pr.getApres()%>?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="empCompte" id="empCompte">
					<% out.println(pr.getFormu().getHtmlEnsemble());%>
				</form>
				<%
					String lienTableau[] = {pr.getLien() + "?but=employeurs/employeursComptes-fiche.jsp"};
					String colonneLien[] = {"id"};
					pr.getTableau().setLien(lienTableau);
					pr.getTableau().setColonneLien(colonneLien);
					out.println(pr.getTableauRecap().getHtml());
				%>
				<form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
					<input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
					<table class="table table-bordered">
						<tr class=info>
							<th>#</th>
							<th>ID</th>
							<th>Compta ecriture</th>
							<th>Succursale code</th>
							<th>Banque code</th>
                                                        <th>Agence code</th>
                                                        <th>Compte numero</th>
                                                        <th>Compte cl�</th>
                                                        <th>Date compte</th>
							
						</tr>
						<%
							for (int i = 0; i < liste.length; i++) {
						%>
						<tr>
							<td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=liste[i].getId()%>" class="radio" /></td>
							<td align=center><%=liste[i].getId()%></td>
							<td align=center><%=liste[i].getMere()%></td>
							<td align=center><%=liste[i].getSuccursale_code()%></td>
							<td align=center><%=liste[i].getBanque_code()%></td>
                                                        <td align=center><%=liste[i].getAgence_code()%></td>
                                                        <td align=center><%=liste[i].getCompte_numero()%></td>
                                                        <td align=center><%=liste[i].getCompte_cle()%></td>
                                                        <td align=center><%=liste[i].getCompte_date()%></td>
						</tr>
						<%}%>
					</table>
				</form>
				<%
					out.println(pr.getBasPage());
				%>
				</section>
		</div>
		<jsp:include page='../elements/js.jsp'/>
	 </body>
</html>