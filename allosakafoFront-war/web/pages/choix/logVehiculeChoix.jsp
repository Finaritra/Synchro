<%-- 
    Document   : logVehiculeChoix
    Created on : 17 mars 2016, 09:27:17
    Author     : Tafitasoa
--%>

<%@page import="mg.cnaps.log.LogVehicule"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    LogVehicule e = new LogVehicule();
    e.setNomTable("log_vehicule_info");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "immatriculation", "modele", "id_marque", "nombre_place"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "immatriculation", "modele", "id_marque", "nombre_place"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setAWhere(" AND ETAT =0");
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeVehiculeChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("immatriculation").setLibelleAffiche("Immatriculation");
    pr.getFormu().getChamp("modele").setLibelleAffiche("Mod�le");
    pr.getFormu().getChamp("nombre_place").setLibelleAffiche("Nombre de place");
    pr.getFormu().getChamp("id_marque").setLibelleAffiche("Marque");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    LogVehicule[] voitures = (LogVehicule[]) pr.getTableau().getData();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste vehicule</h1>
            </section>
            <section class="content">
                <form action="listeVehiculeChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="vehicule" id="vehicule">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
		    String lienTableau[] = {pr.getLien() + "?but=matieriel_roulant/vehicule/vehicule-fiche.jsp"};
		    String colonneLien[] = {"id"};
		    String libelles[] = {"Id", "Immatriculation", "Mod�le", "Marque", "Nombre de place"};
		    pr.getTableau().setLien(lienTableau);
		    pr.getTableau().setColonneLien(colonneLien);
		    pr.getTableau().setLibelleAffiche(libelles);
		    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix2retour.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Immatriculation</th>
                                    <th>Mod&egrave;le</th>
                                    <th>Marque</th>
                                    <th>Nombre de place</th>

                            </tr>
                            <%
                                    for (int i = 0; i < voitures.length; i++) {
                            %>
                            <tr>
                                    <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=voitures[i].getId()%>;<%=voitures[i].getKilometrique_initiale()%>" class="radio" /></td>
                                    <td align=center><%=voitures[i].getId()%></td>
                                    <td align=center><%=voitures[i].getImmatriculation()%></td>
                                    <td align=center><%=voitures[i].getModele()%></td>
                                    <td align=center><%=voitures[i].getId_marque()%></td>
                                    <td align=center><%=(int)voitures[i].getNombre_place()%></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>