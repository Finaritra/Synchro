<%-- 
    Document   : employeurValideChoixSaisieCourrier
    Created on : 12 juin 2016, 19:16:08
    Author     : Ignafah
--%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>


<%
    String champReturn = request.getParameter("champReturn");
    VueSigEmployeur e = new VueSigEmployeur();
    e.setNomTable("VUESIGEMPLOYEURSVALIDE");
    String listeCrt[]={"id","nom","employeur_telephone", "code_dr"};
    String listeInt[]=null;
    String libEntete[]={"id","nom","employeur_telephone", "code_dr"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choix/employeurValideChoixSaisieCourrier.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("employeur_telephone").setLibelleAffiche("T�l�phone");
    pr.getFormu().getChamp("code_dr").setLibelleAffiche("Direction r�gionale");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste employeurs</h1>
            </section>
            <section class="content">
                <form action="choix/employeurValideChoixSaisieCourrier.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=employeurs/employeurs-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[]={"Id","Nom", "T�l�phone", "Direction r�gionale"};
		    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                    VueSigEmployeur[] vse = (VueSigEmployeur[]) pr.getListe();
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixSaisieCourrier.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <input type="hidden" name="cible" value="employeur"/>
                    <% for(int i=0; i<vse.length; i++){%>
                        <input type="hidden" name="choix" value="<%=vse[i].getId()%>;<%=vse[i].getNom()%>">
                    <%}%>
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

