<%-- 
    Document   : diplome
    Created on : 8 avr. 2016, 16:40:01
    Author     : Admin
--%>

<%@page import="mg.cnaps.paie.PaieDiplome"%>
<%@page import="affichage.PageRecherche"%>
<% PaieDiplome op = new PaieDiplome();
 //  op.setNomTable("optous");
    String listeCrt[] = {"id", "val"};
    String listeInt[] = {"date_op"};
    String libEntete[] = {"id", "val"};
    PageRecherche pr = new PageRecherche(op, request, listeCrt, listeInt, 2, libEntete, 2);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("date_op1").setLibelle("Date OP min");
    pr.getFormu().getChamp("date_op2").setLibelle("Date OP max");
    pr.setApres("choix/diplome.jsp");
    pr.setAWhere(" and etat=1 ");
    String[] colSomme = {"montant"};
    pr.setNpp(100);
    pr.creerObjetPage(libEntete, colSomme);%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste des diplomes </h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>/choix/diplome.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>

        <%  String lienTableau[] = {pr.getLien() + "/choix/diplome.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br/>
        <form action="<%=pr.getLien()%>?but=visa-multiple.jsp" method="post" name="incident" id="incident">
            <input type="hidden" name="bute" value="/choix/diplome.jsp"/>
            <input type="hidden" name="classe" value="mg.cnaps.cv.CvDiplome"/>
            <input type="hidden" name="acte" value="viser"/>
            <%
                String libEnteteAffiche[] = {"id", "Diplome"};
                pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                pr.getTableau().setNameActe("viser");
                pr.getTableau().setNameBoutton("Viser");
                out.println(pr.getTableau().getHtmlWithCheckbox());
            %>
        </form>
        <% out.println(pr.getBasPage());%>
    </section>
</div>

