<%-- 
    Document   : listeArticleChoixMultiple
    Created on : 6 d�c. 2016, 10:33:58
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@page import="mg.allosakafo.produits.ProduitsLibelle"%>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    ProduitsLibelle e = new ProduitsLibelle();
   // e.setNomTable("as_produits_libelleprix");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"nom", "designation", "typeproduit"};
    String listeInt[] = null;
    String libEntete[] = {"id", "nom", "designation", "typeproduit", "calorie", "poids", "pa"};

    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeArticleChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    ProduitsLibelle[] listeP = (ProduitsLibelle[]) pr.getRs().getResultat();

%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste produits</h1>
            </section>
            <section class="content">
                <form action="listeArticleChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th>#</th>
                            <th>Id</th>
                            <th>Nom</th>
                            <th>D�signation</th>
                            <th>Type</th>
                            <th>Calorie</th>
                            <th>Pois</th>
                            <th>PU</th>
                        </tr>
                        <%
                            for (int i = 0; i < listeP.length; i++) {
%>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()+";"+listeP[i].getNom()+";"+listeP[i].getPu()%>" class="radio" /></td>
                            <td align=center><%=listeP[i].getId()%></td>
                            <td align=center><%=listeP[i].getNom()%></td>
                            <td align=center><%=listeP[i].getDesignation()%></td>
                            <td align=center><%=listeP[i].getTypeproduit()%></td>
                            <td align=center><%=listeP[i].getCalorie()%></td>
                            <td align=center><%=listeP[i].getPoids()%></td>
                            <td align=center><%=listeP[i].getPu()%></td>

                        </tr>
                        <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
