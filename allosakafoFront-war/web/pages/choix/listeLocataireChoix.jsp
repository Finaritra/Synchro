<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="mg.cnaps.st.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.cnaps.recette.LocLocataire"%>

<%
    String champReturn = request.getParameter("champReturn");
	LocLocataire fact = new LocLocataire();   
    String listeCrt[] = {"id", "nom","prenom","bp"};
    String listeInt[] = null;
    String libEntete[] = {"id", "nom","prenom","raison_social","bp","adresse"};
	
	
	PageRechercheChoix pr = new PageRechercheChoix(fact, request, listeCrt, listeInt, 2, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeLocataireChoix.jsp");
    pr.setChampReturn(champReturn);
	
	
	pr.getFormu().getChamp("prenom").setLibelle("Pr&eacute;nom");
    pr.getFormu().getChamp("bp").setLibelle("BP");String[] colSomme = null;
    
	
	pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste locataires</h1>
            </section>
            <section class="content">
                <form action="listeLocataireChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    
					String libEnteteAffiche[] = {"Id", "Nom","Pr&eacute;nom","Raison social","BP","Adresse"};
                    pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>