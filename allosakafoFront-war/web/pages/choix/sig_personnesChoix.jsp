<%@page import="mg.cnaps.sig.SigPersonnes"%>
<%@page import="mg.cnaps.medecine.MedConsultation"%>
<%@page import="mg.cnaps.medecine.MedMedecin"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    SigPersonnes e = new SigPersonnes();
    e.setNomTable("SIG_PERSONNE_LIBELLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "pers_nom", "pers_prenom", "pers_sexe", "pers_numero_cin"};
    String listeInt[] = {""};
    String libEntete[] =  {"id", "pers_nom", "pers_prenom", "pers_sexe", "pers_numero_cin"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sig_personnesChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("pers_nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("pers_prenom").setLibelleAffiche("Pr�nom");
    pr.getFormu().getChamp("pers_sexe").setLibelleAffiche("Sexe");
    pr.getFormu().getChamp("pers_numero_cin").setLibelleAffiche("Numero CIN");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Personnes</h1>
            </section>
            <section class="content">
                <form action="sig_personnesChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personne" id="personne">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=personnne/personne-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Nom", "Pr�nom", "Sexe", "Numero CIN"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>