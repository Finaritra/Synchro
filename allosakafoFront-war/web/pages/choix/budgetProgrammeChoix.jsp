<%-- 
    Document   : budgetProgrammeChoix
    Created on : 14 sept. 2015, 15:38:46
    Author     : user
--%>
<%@page import="mg.cnaps.budget.BudgetProgramme"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    BudgetProgramme e = new BudgetProgramme();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "code", "intitule"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "code", "intitule"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("budgetProgrammeChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("code").setLibelleAffiche("Code");
    pr.getFormu().getChamp("intitule").setLibelleAffiche("Intitul�");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste programmes</h1>
            </section>
            <section class="content">
                <form action="budgetProgrammeChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="pbudget" id="pbudget">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=budget/programme/budgetprogramme-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "code", "intitule"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>