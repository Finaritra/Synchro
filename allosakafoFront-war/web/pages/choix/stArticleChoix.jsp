<%-- 
    Document   : stArticleChoix
    Created on : 29 oct. 2015, 16:17:45
    Author     : user
--%>
<%@page import="mg.cnaps.st.StArticle"%>
<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="mg.cnaps.log.LogPersonnel"%>
<%@page import="mg.cnaps.log.LogDeplacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    StArticle e = new StArticle();
    e.setNomTable("ST_ARTICLE_LIBELLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "code", "designation","seuil", "unite", "type"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "code", "designation","seuil", "unite", "type"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stArticleChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("code").setLibelleAffiche("Code");
    pr.getFormu().getChamp("designation").setLibelleAffiche("Designation");
    pr.getFormu().getChamp("seuil").setLibelleAffiche("Seuil");
    pr.getFormu().getChamp("unite").setLibelleAffiche("Unit�");
    pr.getFormu().getChamp("type").setLibelleAffiche("Type");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Article</h1>
            </section>
            <section class="content">
                <form action="stArticleChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="article" id="article">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=stock/article/article-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "code", "designation","seuil", "unite", "type"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>