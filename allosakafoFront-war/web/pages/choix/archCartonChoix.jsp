<%-- 
    Document   : archCartonChoix
    Created on : 23 d�c. 2015, 11:54:54
    Author     : user
--%>
<%@page import="mg.cnaps.archive.ArchCarton"%>
<%@page import="mg.cnaps.budget.BudgetSection"%>
<%@page import="mg.cnaps.budget.BudgetProjet"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    ArchCarton e = new ArchCarton();
    e.setNomTable("ARCH_CARTON_LIBELLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "idetagere", "numero", "description"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "idetagere", "numero", "description"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("archCartonChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("idetagere").setLibelleAffiche("Etag�re");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des cartons </h1>
            </section>
            <section class="content">
                <form action="archCartonChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=archive/carton/carton-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] =  {"ID", "Etagere", "Numero", "Description"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>