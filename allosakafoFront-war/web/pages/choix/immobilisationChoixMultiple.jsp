<%-- 
    Document   : immobilisationChoixMultiple
    Created on : 23 f�vr. 2016, 17:40:38
    Author     : ITU
--%>

<%@page import="mg.cnaps.immo.Immobilisation"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    Immobilisation e = new Immobilisation();
    e.setNomTable("immo_libelle_non_attribue");//si_dossiers_travailleurs_lib
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "numero_bl","nature_immo","type_d_amortissement","annee","daty"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "numero_bl","nature_immo","type_d_amortissement","annee","daty"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
//    pr.setApres("sig_personnesChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("numero_bl").setLibelleAffiche("Numero bon de livraison");
    pr.getFormu().getChamp("nature_immo").setLibelleAffiche("Nature");
    pr.getFormu().getChamp("type_d_amortissement").setLibelleAffiche("Type d'amortissement");
//    pr.getFormu().getChamp("categorie").setLibelleAffiche("Cat�gorie");
    pr.getFormu().getChamp("annee").setLibelleAffiche("Ann�e");
    pr.getFormu().getChamp("daty").setLibelleAffiche("Daty");
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste immobilisation</h1>
            </section>
            <section class="content">
                <form action="immobilisationChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="immo" id="immo">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=immo/immobilisation-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "numero_br","nature_immo","type_d_amortissement","annee","daty"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% 
                    pr.getTableau().setNameActe("Attribuer");
                    pr.getTableau().setNameBoutton("Attribution Multiple"); 
                    out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
