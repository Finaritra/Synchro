<%@page import="mg.cnaps.budget.BudgetTef"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    BudgetTef e = new BudgetTef();
    e.setNomTable("BUDGET_TEF_LIBELLE_VISE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "code", "daty", "nombeneficiaire", "cinbeneficiaire", "budget"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "code", "daty", "montant", "nombeneficiaire", "libelle", "budget"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("code").setLibelleAffiche("Code");
    pr.getFormu().getChamp("daty1").setLibelleAffiche("Date debut");
    pr.getFormu().getChamp("daty2").setLibelleAffiche("Date fin");
    pr.getFormu().getChamp("nombeneficiaire").setLibelleAffiche("B&eacute;n&eacute;ficiaire");
    pr.getFormu().getChamp("Budget").setLibelleAffiche("Budget");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Ref engagement</h1>
            </section>
            <section class="content">
                <form action="tefViseChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="tef" id="chauffeur">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=budget/tef/tef-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "code", "daty", "montant", "nombeneficiaire", "libelle", "budget"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>