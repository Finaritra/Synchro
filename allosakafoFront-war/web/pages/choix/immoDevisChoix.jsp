<%-- 
    Document   : immoDevisChoix
    Created on : 23 f�vr. 2016, 16:16:18
    Author     : ITU
--%>

<%@page import="mg.cnaps.immo.ImmoErDevis"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    ImmoErDevis devis = new ImmoErDevis();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "personnel","daty","titre"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "personnel","daty","titre"};
    PageRechercheChoix pr = new PageRechercheChoix(devis, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("immodemandeChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("daty1").setLibelleAffiche("Date Min");
    pr.getFormu().getChamp("daty2").setLibelleAffiche("Date Max");
    pr.getFormu().getChamp("personnel").setLibelleAffiche("Demandeur");
    //pr.getFormu().getChamp("service").setLibelleAffiche("Service");
    //pr.getFormu().getChamp("designation").setLibelleAffiche("Designation");
    //pr.getFormu().getChamp("priorite").setLibelleAffiche("Priorite");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste demande</h1>
            </section>
            <section class="content">
                <form action="immodemandeChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bobj" id="bobj">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=immo/demande/demande-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "personnel","daty","titre"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
