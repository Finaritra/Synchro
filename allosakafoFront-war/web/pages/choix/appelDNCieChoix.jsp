<%-- 
    Document   : appelDNCieChoix
    Created on : 21 juin 2016, 15:29:03
    Author     : Tafitasoa
--%>

<%@page import="mg.cnaps.recette.AppelDn"%>
<%@page import="mg.cnaps.log.LogPersonnel"%>
<%@page import="mg.cnaps.log.LogDeplacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    AppelDn lv = new AppelDn();
    String champReturn = request.getParameter("champReturn");
    //lv.setNomTable("appeldn_libelle");
    if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("") != 0) {
        lv.setNomTable(request.getParameter("etat"));
    }
    String listeCrt[] = {"id", "idtypeenvoi", "idemployeur", "periode", "resultat"};
    String listeInt[] = null;
    String libEntete[] = {"id", "idtypeenvoi", "idemployeur", "periode", "resultat"};

    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("idtypeenvoi").setLibelle("Type envoie");
    pr.getFormu().getChamp("idemployeur").setLibelle("Employeur");
    pr.getFormu().getChamp("periode").setLibelle("Periode");
    pr.setApres("appelDnChoix.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    AppelDn[] donnees = (AppelDn[]) pr.getTableau().getData();
%>
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Appel DN</h1>
            </section>
            <section class="content">
                <form action="appelDnChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="appelDn" id="appelDn">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=recette/appeldn/appeldn-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "idtypeenvoi", "idemployeur", "periode", "resultat"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Id Employeur</th>
                                    <th>Periode</th>
                                    <th>Motif</th>
                                    <th>Resultat</th>
                            </tr>
                            <%
                                    for (int i = 0; i < donnees.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=donnees[i].getId()%>;<%=donnees[i].getIdemployeur()%>;<%=donnees[i].getPeriode()%>" class="radio" /></td>
                                <td align=left><%=donnees[i].getId()%></td>
                                <td align=left><%=donnees[i].getIdemployeur()%></td>
                                <td align=left><%=donnees[i].getPeriode()%></td>
                                <td align=left><%=Utilitaire.champNull(donnees[i].getMotif())%></td>
                                <td align=left><%=Utilitaire.champNull(donnees[i].getResultat())%></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

