<%-- 
    Document   : pointageChoix
    Created on : 30 oct. 2015, 16:33:35
    Author     : user
--%>
<%@page import="mg.cnaps.pointage.PointPointage"%>
<%@page import="mg.cnaps.treso.TresoTitre"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    PointPointage t = new PointPointage();
    String listeCrt[] = {"id", "id_personnel", "id_type", "daty", "heure"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "id_personnel", "id_type", "daty", "heure"};
    PageRechercheChoix pr = new PageRechercheChoix(t, request, listeCrt, listeInt,3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("pointageChoix.jsp");
    
    pr.getFormu().getChamp("id").setLibelleAffiche("Id");
    pr.getFormu().getChamp("id_personnel").setLibelleAffiche("Numérotation");
    pr.getFormu().getChamp("daty").setLibelleAffiche("Date min");
    pr.getFormu().getChamp("id_type").setLibelleAffiche("Type");
    pr.getFormu().getChamp("heure").setLibelleAffiche("Heure");
    String [] colSomme=null;
    //String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
    //TresoTitre[] listeD = (TresoTitre[]) pr.getRs().getResultat();
    
%>
  <!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Pointage</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="titre" id="titre">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/pointage/pointage-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[]={"id", "id_personnel", "id_type", "daty", "heure"};
		    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>