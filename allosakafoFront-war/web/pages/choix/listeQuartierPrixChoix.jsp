<%-- 
    Document   : listeClientChoix.jsp
    Created on : 2 d�c. 2016, 13:46:50
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@page import="mg.allosakafo.secteur.TarifLivraison"%>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>

<%
    String champReturn = request.getParameter("champReturn");
    TarifLivraison e = new TarifLivraison();
	e.setNomTable("as_livraison_prixlibelle");
    String listeCrt[] = {"ID","QUARTIER", "OBSERVATION"};
    String listeInt[] = null;
    String libEntete[] = {"ID", "QUARTIER", "OBSERVATION", "MONTANT"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeQuartierPrixChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
	
	TarifLivraison[] listeP = (TarifLivraison[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Allo Sakafo 1.0</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Frais de livraison</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th><b>#</b></th>
                            <th><b>Id</b></th>
                            <th><b>Quartier</b></th>
                            <th><b>Secteur</b></th>
                            <th><b>Montant</b></th>
                        </tr>
                        <%
                            for (int i = 0; i < listeP.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getQuartier()%>;<%=listeP[i].getObservation()%>;<%=listeP[i].getMontant()%>" class="radio" /></td>
                            <td align=left><%=listeP[i].getId()%></td>
                            <td align=left><%=listeP[i].getQuartier()%></td>
                            <td align=left><%=listeP[i].getObservation()%></td>
                            <td align=left><%=Utilitaire.formaterAr(listeP[i].getMontant())%></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>