<%-- 
    Document   : archArchiveChoix
    Created on : 29 d�c. 2015, 14:37:06
    Author     : user
--%>
<%@page import="mg.cnaps.archive.Archive"%>
<%@page import="mg.cnaps.archive.ArchEtagere"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    Archive e = new Archive();
    e.setNomTable("ARCH_ARCHIVE_LIBELLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id",  "datearchive", "remarque", "idservice"};
    String listeInt[] = {"datearchive"};
    String libEntete[] = {"id",  "datearchive", "remarque", "idservice"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));    
    pr.setApres("archArchiveChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("datearchive1").setLibelle("Date min");
    pr.getFormu().getChamp("datearchive2").setLibelle("Date max");
    pr.getFormu().getChamp("idservice").setLibelle("Service");
   // pr.getFormu().getChamp("idservice").setLibelleAffiche("Service");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des archives </h1>
            </section>
            <section class="content">
                <form action="archArchiveChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=archive/archive-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] =  {"Id",  "Date", "Remarque", "Service"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>