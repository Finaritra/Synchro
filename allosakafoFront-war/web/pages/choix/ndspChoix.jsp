<%@page import="mg.cnaps.accident.NdspDetails"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>


<%
    String champReturn = request.getParameter("champReturn");
    NdspDetails nd = new NdspDetails();
    String listeCrt[] = {"num_sinitre", "dossier_matricule", "travailleur_matricule", "nom_travailleur", "dossier_date_reception"};
    String listeInt[] = {"dossier_date_reception"};
    String libEntete[] = {"num_sinitre", "dossier_matricule", "travailleur_matricule", "nom_travailleur", "dossier_date_reception"};
    PageRechercheChoix pr = new PageRechercheChoix(nd, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("ndspChoix.jsp");
    pr.getFormu().getChamp("num_sinitre").setLibelle("Numero sinistre");
    pr.getFormu().getChamp("dossier_matricule").setLibelle("Matricule dossier");
    pr.getFormu().getChamp("travailleur_matricule").setLibelle("Matricule travailleur");
    pr.getFormu().getChamp("nom_travailleur").setLibelle("Nom travailleur");
    pr.getFormu().getChamp("dossier_date_reception1").setLibelle("Date r&eacute;ception dossier inf");
    pr.getFormu().getChamp("dossier_date_reception2").setLibelle("Date r&eacute;ception dossier sup");
    String awere = "";
    if ((request.getParameter("dossier_date_reception1") == null && request.getParameter("dossier_date_reception2") == null)) {
        pr.getFormu().getChamp("dossier_date_reception1").setDefaut(Utilitaire.dateDuJour());
        pr.getFormu().getChamp("dossier_date_reception2").setDefaut(Utilitaire.dateDuJour());
        awere += " and dossier_date_reception='" + Utilitaire.dateDuJour() + "'";
    }
    pr.setAWhere(awere);
    String[] colSomme = null;
    pr.setNpp(50);
    pr.creerObjetPage(libEntete, colSomme);
    pr.setChampReturn(champReturn);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste N. DSP/DMP</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>