<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.commun.*" %>

<%
    String champReturn = request.getParameter("champReturn");
    Sig_fokontany e = new Sig_fokontany();
    e.setNomTable("SIG_FOKONTANY_LIBELLE");
    String listeCrt[] = {"id","val","desce", "code_commune"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "val","desce", "code_commune"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("code_fkt-choix.jsp");
    pr.setChampReturn(champReturn);
    affichage.Champ[] list = new affichage.Champ[1];
    TypeObjet com = new TypeObjet();
    com.setNomTable("SIG_COMMUNE");
    Liste liste = new Liste("code_commune",com,"val","val");
    list[0] = liste;
    pr.getFormu().changerEnChamp(list);
    pr.getFormu().getChamp("id").setLibelleAffiche("Code fokontany");
    pr.getFormu().getChamp("val").setLibelleAffiche("Nom fokontany");
    pr.getFormu().getChamp("desce").setLibelleAffiche("Description");
    pr.getFormu().getChamp("code_commune").setLibelleAffiche("Code commune");   
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);

%>
<html> 
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste fokontany</h1>
            </section>
            <section class="content">
                <form action="fkt-Choix2.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="livre" id="livre">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
