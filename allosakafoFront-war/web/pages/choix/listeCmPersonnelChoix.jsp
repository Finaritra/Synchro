<%-- 
    Document   : listePersonnelChoix
    Created on : 8 juin 2016, 19:34:09
    Author     : Paul M.
--%>

<%@page import="mg.cnaps.log.LogPersonnelLibelle"%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.immo.ImmoCategorie"%>
<%@page import="mg.cnaps.immo.Immo_nature"%>
<%@page import="bean.CGenUtil"%>

<%
    String champReturn = request.getParameter("champReturn");
    LogPersonnelLibelle e = new LogPersonnelLibelle();
    String listeCrt[] = {"id", "nom","prenom","service", "direction"};
    String listeInt[] = null;
    String libEntete[] ={"id", "nom","prenom", "service", "direction"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeCmPersonnelChoix.jsp");   
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("prenom").setLibelleAffiche("Pr�nom");
    pr.getFormu().getChamp("direction").setLibelleAffiche("Direction");
    pr.getFormu().getChamp("service").setLibelleAffiche("Service");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    LogPersonnelLibelle[] listeP=(LogPersonnelLibelle[])pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des personnels</h1>
            </section>
            <section class="content">
                <form action="listeCmPersonnelChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="chauffeur" id="chauffeur">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "Matricule", "nom", "prenom", "direction", "service"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixCm.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>"/>
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Nom</th>
                                    <th>Pr�nom</th>
                                    <th>Service</th>
                                    <th>Direction</th>
                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                    <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getService()%>" class="radio" /></td>
                                    <td align=left><%=listeP[i].getId()%></td>
                                    <td align=left><%=listeP[i].getNom()%></td>
                                    <td align=left><%=listeP[i].getPrenom()%></td>
                                    <td align=left><%=listeP[i].getService()%></td>
                                    <td align=left><%=listeP[i].getDirection()%></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
