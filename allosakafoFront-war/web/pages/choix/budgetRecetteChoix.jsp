<%-- 
    Document   : budgetRecetteChoix
    Created on : 30 sept. 2015, 09:28:53
    Author     : user
--%>
<%@page import="mg.cnaps.budget.BudgetRecette"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    BudgetRecette e = new BudgetRecette();
    e.setNomTable("budget_recette_info");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "projet", "section_budget","compte","montant_credit","exercice","etat"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "projet", "section_budget","compte","montant_credit","exercice","etat"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("budgetRecetteChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("projet").setLibelleAffiche("Projet");
    pr.getFormu().getChamp("section_budget").setLibelleAffiche("Budget Section");
    pr.getFormu().getChamp("compte").setLibelleAffiche("Compte");
    pr.getFormu().getChamp("montant_credit").setLibelleAffiche("Montant Credit");
    pr.getFormu().getChamp("exercice").setLibelleAffiche("Exercice");
    pr.getFormu().getChamp("Etat").setLibelleAffiche("Etat");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste budget Depense</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="recette" id="recette">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=budget/recette/recette-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "projet", "section_budget","compte","montant_credit","exercice","etat"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>