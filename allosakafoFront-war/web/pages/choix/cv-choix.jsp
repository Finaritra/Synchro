<%@page import="mg.cnaps.cv.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    CvInfo e = new CvInfo();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "nom_personne","telephone","date_naissance", "date_depot"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "nom_personne","telephone","date_naissance", "date_depot"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("cv-choix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("nom_personne").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("date_naissance").setLibelleAffiche("Date de naissance");
    pr.getFormu().getChamp("date_depot").setLibelleAffiche("Date de d�pot du CV");
    //pr.getFormu().getChamp("idpersonne").setVisible(false);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste magasin</h1>
            </section>
            <section class="content">
                <form action="apresChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="benef" id="benef">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "Nom","telephone","Date de naissance", "Date de d�pot"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>