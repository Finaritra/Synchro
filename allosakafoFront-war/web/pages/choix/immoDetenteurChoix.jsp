<%-- 
    Document   : immoDetenteurChoix
    Created on : 30 sept. 2015, 15:09:46
    Author     : user
--%>
<%@page import="mg.cnaps.immo.ImmoDetenteur"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    ImmoDetenteur e = new ImmoDetenteur();
    e.setNomTable("immo_detenteur_libelle");//si_dossiers_travailleurs_lib
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "service", "magasin","daty","date_mise_en_service","duree_ammortissement","etat"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "service", "magasin","daty","date_mise_en_service","duree_ammortissement","etat"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
//    pr.setApres("sig_personnesChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("service").setLibelleAffiche("Service");
    pr.getFormu().getChamp("magasin").setLibelleAffiche("Magasin");
    pr.getFormu().getChamp("daty").setLibelleAffiche("Date");
    pr.getFormu().getChamp("date_mise_en_service").setLibelleAffiche("Date de mise en service");
    pr.getFormu().getChamp("duree_ammortissement").setLibelleAffiche("Dur�e d'ammortissement");
    pr.getFormu().getChamp("etat").setLibelleAffiche("Etat");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste detenteur</h1>
            </section>
            <section class="content">
                <form action="immoDetenteurChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="detenteur" id="detenteur">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=immo/detenteur/detenteur-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "service", "magasin","daty","date_mise_en_service","duree_ammortissement","etat"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>