<%-- 
    Document   : listeTefFilleChoix
    Created on : 1 oct. 2015, 11:10:06
    Author     : GILEADA
--%>

<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.immo.ImmoDetenteurLibelle"%>
<%
    String champReturn = request.getParameter("champReturn");
    ImmoDetenteurLibelle e = new ImmoDetenteurLibelle();
    
    String listeCrt[]={"id","service", "ref_immo", "designation", "num_fd", "magasin", "remarque", "date_mise_en_service", "etat"};
    String listeInt[]={"date_mise_en_service"};
    String libEntete[]={"id","service", "ref_immo","designation","num_fd","magasin","remarque","date_mise_en_service","etat"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 9);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("immodetentionChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    ImmoDetenteurLibelle[] listeP = (ImmoDetenteurLibelle[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Detenteur Choix</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="ImmoDetenteurLibelle" id="comptaTiers">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id","service", "ref_immo","designation","num_fd","magasin","remarque","daty","etat"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixImmoDetenteur.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Service</th>
                                    <th>R&eacute;f&eacute;rence Immobilisation</th>
                                    <th>D&eacute;signation</th>
                                    <th>Num&acute;ro Fiche de D&eacute;tention</th>
                                    <th>Magasin</th>
                                    <th>Remarque</th>
                                    <th>Date de mise en service</th>
                                    <th>Etat</th>
                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                    <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getDesignation()%>" class="radio" /></td>
                                    <td align=left><%=listeP[i].getId()%></td>
                                    <td align=left><%=listeP[i].getService()%></td>
                                    <td align=center><%=listeP[i].getRef_immo()%></td>
                                    <td align=left><%=listeP[i].getDesignation()%></td>
                                    <td align=right><%=listeP[i].getNum_fd()%></td>
                                    <td align=right><%=listeP[i].getMagasin()%></td>
                                    <td align=left><%=listeP[i].getRemarque()%></td>
                                    <td align=left><%=Utilitaire.formatterDaty(listeP[i].getDate_mise_en_service())%></td>
                                    <td align=left><%=listeP[i].getEtat()%></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>