<%-- 
    Document   : apresChoixDroitDetailSaisie
    Created on : 5 juil. 2016, 11:21:57
    Author     : Ignafah
--%>

<%@page import="mg.cnaps.paie.PaieInfoPersonnel"%>
<%@page import="mg.cnaps.sig.SigEmployeursComptes"%>
<%@page import="bean.TypeObjet"%>
<%@page import="bean.CGenUtil"%>
<%@page import="utilitaire.Utilitaire"%>
<html>
    <%
        String champRet =(String) request.getParameter("champReturn");
        String choix = (String) request.getParameter("choix");
        String cible = (String) request.getParameter("cible");
        String[] champs = Utilitaire.split(champRet, ";");
        String[] lstChoix = Utilitaire.split(choix, ";");        
        if(cible!=null && cible.compareToIgnoreCase("droitdetail")==0){       
            %>
            <script language="JavaScript">
                window.opener.document.all.<%=champs[0]+"libelle"%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[0])%>"; 
                window.opener.document.all.<%=champs[1]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[1])%>"; 
                window.opener.document.all.<%=champs[2]+"libelle"%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[2])%>"; 
                window.opener.document.all.<%=champs[3]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[3])%>"; 
            </script> 
            <%   
        }   
        
    %>
<script language="JavaScript"> 
    window.close();
</script>
</html>

