<%-- 
    Document   : cieMedChoix
    Created on : 18 nov. 2015, 14:59:29
    Author     : user
--%>
<%@page import="mg.cnaps.sms.SmsTypeDestinataire"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    SmsTypeDestinataire e = new SmsTypeDestinataire();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "type_dest", "libelle", "code_service"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "type_dest", "libelle", "code_service"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("smsTypeDestChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("type_dest").setLibelleAffiche("Type Destinataire");
    pr.getFormu().getChamp("code_service").setLibelleAffiche("Code service");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste type destinataire</h1>
            </section>
            <section class="content">
                <form action="smsTypeDestChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="type" id="type">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"Id", "Type Destinataire", "Libelle", "Code Service"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>