<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.sig.SigActivites"%>

<%
    String champReturn = request.getParameter("champReturn");
    
    SigActivites e = new SigActivites();
    String listeCrt[] = {"id","activite_libelle","ventil_code", "code"};
    String listeInt[] = {""};
    String libEntete[] = {"id","activite_libelle","ventil_code", "code"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sigActiviteChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("activite_libelle").setLibelleAffiche("activit&eacute;");
    pr.getFormu().getChamp("ventil_code").setLibelleAffiche("Code ventil");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Activit&eacute;</h1>
            </section>
            <section class="content">
                <form action="sigActiviteChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[]={"Id","activit&eacute;","Code ventil","Code"};
		    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>