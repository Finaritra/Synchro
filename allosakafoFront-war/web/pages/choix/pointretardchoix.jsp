
<%@page import="mg.cnaps.pointage.PointRetardChoix"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>


<%
    PointRetardChoix e = new PointRetardChoix();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id","id_personnel", "daty","duree"};
    String listeInt[] = null;
    String libEntete[]= {"id","id_personnel", "daty","duree"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 2);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("pointretardchoix.jsp");
    //pr.setAWhere(" AND IS_DEDUIRE_CONGE = 'OUI'");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("daty").setLibelleAffiche("Date");
    pr.getFormu().getChamp("id_personnel").setLibelleAffiche("Personnel");
    pr.getFormu().getChamp("duree").setLibelleAffiche("Dur&eacute;e");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    PointRetardChoix[] listeP = (PointRetardChoix[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste retard</h1>
            </section>
            <section class="content">
                <form action="pointretardchoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="retard" id="retard">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"Id","Date"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixAutorisation.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                <th>#</th>
                                <th>Id</th>
                                <th>Personnel</th>
                                <th>Date</th>
                                <th>Dur&eacute;e</th>
                            </tr>
                            <%
                                for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getId_personnel()%>" class="radio" /></td>
                                    
                                <td align=center><%=listeP[i].getId()%></td>
                                <td align=center><%=listeP[i].getId_personnel()%></td>
                                <td align=center><%=listeP[i].getDaty()%></td>
                                <td align=center><%=listeP[i].getDuree()%></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

