<%@page import="java.lang.reflect.Field"%>
<%@page import="mg.cnaps.commun.Dr"%>
<%@page import="mg.cnaps.commun.Sig_commune"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String classe = request.getParameter("classe");
    
    ClassMAPTable e = ((bean.ClassMAPTable) (Class.forName(classe)).newInstance());
    // String nomt = e.getNomTable();
    if(request.getParameter("nomtable")!=null){
     e.setNomTable(request.getParameter("nomtable"));
    }
   
    Field listeCrt[] = e.getFieldList();
    String []l=new String[listeCrt.length];
    for(int i=0;i<listeCrt.length;i++){
        l[i]=listeCrt[i].getName();
    }
    String listeInt[] = null;
    String libEntete[] =l;
    PageRecherche pr = new PageRecherche(e, request, l, listeInt, 3, libEntete, l.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choix.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);

    String libEnteteAffiche[] = l;
    pr.getTableau().setLibelleAffiche(libEnteteAffiche);

%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
        <jsp:include page='../elements/js.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini" style="background-color: #ffffff">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste <%=e.getNomTable()%> </h1>
            </section>
            <section class="content">
                <form action="choix.jsp?classe=<%=request.getParameter("classe")%>&champReturn=<%=request.getParameter("champReturn")%>" method="post" name="choix" id="choix">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <div class="row col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Liste</h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                                <input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
                             
                                <%out.println(pr.getTableau().getHtmlWithRadioButton());%>
                            </form>
                            <%
                                out.println(pr.getBasPage());
                            %>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </body>
</html>