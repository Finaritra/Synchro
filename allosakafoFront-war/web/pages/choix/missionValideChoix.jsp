<%-- 
    Document   : contRendezVousChoix
    Created on : 8 d�c. 2015, 10:03:34
    Author     : user
--%>
<%@page import="mg.cnaps.mission.MissionValide"%>
<%@page import="mg.cnaps.controle.ContRendezvous"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    MissionValide e = new MissionValide();
    e.setNomTable("MISSION_VALIDE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "motif", "description", "remarque", "date_debut", "date_fin"};
    String listeInt[] = {"date_debut", "date_fin"};
    String libEntete[] = {"id", "idcat", "motif", "description", "remarque", "date_debut", "date_fin"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("missionValideChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("date_debut1").setLibelleAffiche("Date debut min");
    pr.getFormu().getChamp("date_fin1").setLibelleAffiche("Date fin min");
   pr.getFormu().getChamp("date_debut2").setLibelleAffiche("Date debut max");
    pr.getFormu().getChamp("date_fin2").setLibelleAffiche("Date fin max");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste missions non valides</h1>
            </section>
            <section class="content">
                <form action="missionValideChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=controle/rdv/rendezvous-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "cat�gory", "motif", "description", "remarque", "date d�but", "date fin"};
//                    pr.getTableau().setLien(lienTableau);
//                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>