<%-- 
    Document   : stTiersChoix
    Created on : 29 oct. 2015, 16:11:35
    Author     : user
--%>
<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="mg.cnaps.log.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    LogCarteCarburant e = new LogCarteCarburant();
    e.setNomTable("log_carte_carburant_libelle");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "numero", "iddetenteur","montant","date_expiration"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "numero", "iddetenteur","montant","date_expiration"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("CatreCarburant.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("iddetenteur").setLibelleAffiche("Détenteur");
    pr.getFormu().getChamp("date_expiration").setLibelleAffiche("Date d'expiration");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste tiers</h1>
            </section>
            <section class="content">
                <form action="stTiersChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="tiers" id="tiers">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "numero", "Détenteur","montant","Date d'expiration"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>