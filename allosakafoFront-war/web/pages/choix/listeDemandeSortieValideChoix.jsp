<%--
    Document   : listeDemandeSortieValideChoix
    Created on : 16 mars 2016, 20:43:21
    Author     : Joe
--%>

<%@page import="mg.cnaps.commun.Constante"%>
<%@page import="mg.cnaps.st.StDemandeApproSortiePrixView"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="mg.cnaps.st.StDemandeAppro"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="user.*"%>
<%@page import="bean.*"%>
<%@page import="utilitaire.*"%>
<%@page import="affichage.*"%>
<%@page import="mg.cnaps.sig.*"%>


<%
    String champReturn = request.getParameter("champReturn");
    StDemandeApproSortiePrixView e = new StDemandeApproSortiePrixView();
//    e.setNomTable("ST_DEMANDE_PIECEVEHICULE");

    String listeCrt[] = {"id", "daty", "service", "magasin", "designation", "priorite", "date_mouvement"};
    String listeInt[] = {"daty", "date_mouvement"};
    String libEntete[] = {"id", "daty", "service", "magasin", "designation", "montant", "projet", "priorite", "date_mouvement"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeDemandeSortieValideChoix.jsp");
    pr.setChampReturn(champReturn);
    String idArticleVehicule = Constante.listeIdArticleVehicule;
    pr.setAWhere(" and id not in (SELECT PIECE_A_CHANGER FROM LOG_MAINTENANCE where ST_DEMANDE_SORTIE_PRIX.id = LOG_MAINTENANCE.PIECE_A_CHANGER GROUP BY PIECE_A_CHANGER) and typee in (" + idArticleVehicule + ")");

    affichage.Champ[] liste = new affichage.Champ[1];

    TypeObjet d = new TypeObjet();
    d.setNomTable("ST_TYPE_ARTICLE");
    liste[0] = new Liste("typee", d, "VAL", "id");

    pr.getFormu().changerEnChamp(liste);

    if (request.getParameter("chapitre") != null && request.getParameter("chapitre").compareToIgnoreCase("") != 0) {
	//pr.getFormu().getChamp("type").setDefaut(request.getParameter("chapitre"));
	pr.setAWhere(" and type = '" + request.getParameter("chapitre") + "'");
    }

    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("designation").setLibelleAffiche("D&eacute;signation");
    pr.getFormu().getChamp("daty1").setLibelleAffiche("Date Min. Demande");
    pr.getFormu().getChamp("daty2").setLibelleAffiche("Date Max. Demande");
    pr.getFormu().getChamp("date_mouvement1").setLibelleAffiche("Date Min. Sortie Stock");
    pr.getFormu().getChamp("date_mouvement2").setLibelleAffiche("Date Max. Sortie Stock");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste demande de sortie</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
		    String libelles[] = {"ID", "Date Demande", "Service Demandeur", "Magasin", "Désignation", "Montant Total", "Projet", "Priorite", "Date Sortie Stock"};
		    pr.getTableau().setLibelleAffiche(libelles);
		    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
