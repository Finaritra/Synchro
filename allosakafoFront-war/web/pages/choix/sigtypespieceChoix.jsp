<%-- 
    Document   : archEtagereChoix
    Created on : 29 d�c. 2015, 14:16:37
    Author     : user
--%>
<%@page import="mg.cnaps.accueil.SousPrestations"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.cnaps.accueil.TypesPieces"%>
<%-- 
    Document   : archEtagereChoix
    Created on : 29 d�c. 2015, 14:16:37
    Author     : user
--%>
<%@page import="mg.cnaps.accueil.SousPrestations"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.cnaps.accueil.TypesPieces"%>
<%
    TypesPieces lv = new TypesPieces();
    //lv.setNomTable("sig_pieces_recues_libelle2");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "type_piece_libelle", "type_piece_abreviation","service_dest","lien"};
    String listeInt[] = null;
    String libEntete[] = {"id", "type_piece_libelle", "type_piece_abreviation","service_dest","lien"};
    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sigtypespieceChoix.jsp");
    pr.setChampReturn(champReturn);
    
    pr.getFormu().getChamp("type_piece_abreviation").setLibelleAffiche("Abr�viation du pi�ce");
    pr.getFormu().getChamp("type_piece_libelle").setLibelleAffiche(" Pi�ce libell�e");
    pr.getFormu().getChamp("service_dest").setLibelleAffiche(" Destination de service");
   
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des Types pi�ces </h1>
            </section>
            <section class="content">
                <form action="sigtypespieceChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    
                    String libelles[] =  {"id", "Type pi�ce", "Pi�ce abr�viation","Service destination","lien"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>