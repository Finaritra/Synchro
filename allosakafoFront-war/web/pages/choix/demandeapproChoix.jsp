<%-- 
    Document   : demandeapproChoix
    Created on : 11 sept. 2015, 11:04:55
    Author     : user
--%>
<%@page import="mg.cnaps.st.StDemandeAppro"%>
<%@page import="mg.cnaps.commun.Sig_commune"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    StDemandeAppro da = new StDemandeAppro();
    da.setNomTable("st_demande_appro");
    String listeCrt[] = {"id", "service", "direction", "code_dr", "daty", "designation", "priorite", "etat"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "service", "direction", "code_dr", "daty", "designation", "priorite", "etat"};
    PageRechercheChoix pr = new PageRechercheChoix(da, request, listeCrt, listeInt,3, libEntete, 8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("demandeapproChoix.jsp");
    
    pr.getFormu().getChamp("id").setLibelleAffiche("Id");
    pr.getFormu().getChamp("service").setLibelleAffiche("Service");
    pr.getFormu().getChamp("direction").setLibelleAffiche("Direction");
    pr.getFormu().getChamp("code_dr").setLibelleAffiche("Code regional");
    pr.getFormu().getChamp("daty1").setLibelleAffiche("Date 1");
    pr.getFormu().getChamp("daty2").setLibelleAffiche("Date 2");
    pr.getFormu().getChamp("designation").setLibelleAffiche("Designation");
    pr.getFormu().getChamp("priorite").setLibelleAffiche("Priorité");
    pr.getFormu().getChamp("etat").setLibelleAffiche("Etat");
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    StDemandeAppro[] listeD = (StDemandeAppro[]) pr.getRs().getResultat();
%>
  <!DOCTYPE html>
<html>
     <head>
		<meta charset="UTF-8">
		<title>Cynthia 2.0 | CNaPS</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<jsp:include page='../elements/css.jsp'/>
	 </head>
	 <body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<section class="content-header">
				<h1>Liste demande d'approvisionnement</h1>
			</section>
			<section class="content">
				<form action="demandeapproChoix.jsp?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="code_communeChoix" id="code_communeChoix">
					<% out.println(pr.getFormu().getHtmlEnsemble());%>
				</form>
				<%
					String lienTableau[] = {pr.getLien() + "?but=stock/appro/demandeappro-fiche.jsp"};
					String colonneLien[] = {"id"};
					pr.getTableau().setLien(lienTableau);
					pr.getTableau().setColonneLien(colonneLien);
					out.println(pr.getTableauRecap().getHtml());
				%>
				<form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
					<input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
					<table class="table table-bordered">
						<tr class=info>
							<th>#</th>
							<th>ID</th>
							<th>Service</th>
							<th>Direction</th>
							<th>Direction régionale</th>
                                                        <th>Date</th>
                                                        <th>Designation</th>
                                                        <th>Priorité</th>
                                                        <th>Etat</th>
							
						</tr>
						<%
							for (int i = 0; i < listeD.length; i++) {
						%>
						<tr>
							<td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeD[i].getId()%>" class="radio" /></td>
							<td align=center><%=listeD[i].getId()%></td>
                                                        <td align=center><%=listeD[i].getService()%></td>
							<td align=center><%=listeD[i].getDirection()%></td>
							<td align=center><%=listeD[i].getCode_dr()%></td>
							<td align=center><%=listeD[i].getDaty()%></td>
                                                        <td align=center><%=listeD[i].getDesignation()%></td>
                                                        <td align=center><%=listeD[i].getPriorite()%></td>
                                                        <td align=center><%=listeD[i].getEtat()%></td>
						</tr>
						<%}%>
					</table>
				</form>
				<%
					out.println(pr.getBasPage());
				%>
				</section>
		</div>
		<jsp:include page='../elements/js.jsp'/>
	 </body>
</html>