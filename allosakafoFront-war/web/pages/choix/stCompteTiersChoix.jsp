
<%@page import="mg.cnaps.st.StTiersInfo"%>
<%@page import="mg.cnaps.compta.ComptaTiersView"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    StTiersInfo e = new StTiersInfo();
    e.setNomTable("ST_TIERS_INFO");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "raisonsocial", "compte_tiers","matrcnaps","val", "compteg"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "raisonsocial",  "compte_tiers","matrcnaps","val", "compteg"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stCompteTiersChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("raisonsocial").setLibelleAffiche("Raison social");
    pr.getFormu().getChamp("compte_tiers").setLibelleAffiche("Compte tiers");
    pr.getFormu().getChamp("matrcnaps").setLibelleAffiche("Matricule Employeurs");
    pr.getFormu().getChamp("val").setLibelleAffiche("Type");
    pr.getFormu().getChamp("compteg").setLibelleAffiche("Compte general");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    StTiersInfo[] listeP = (StTiersInfo[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Tiers Compta</h1>
            </section>
            <section class="content">
                <form action="stCompteTiersChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="comptaTiers" id="comptaTiers">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "raisonsocial", "rubrique", "compte_tiers"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixCompte.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Raison social</th>
                                    <th>Matricule Employeurs</th>
                                    <th>Compte Tiers</th>
                                    <th>Compte general</th>
                                    <th>Type</th>
                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                    <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getCompte_tiers()%>;<%=listeP[i].getCompteg()%>;<%=listeP[i].getRaisonsocial()%>" class="radio" /></td>
                                    <td align=center><%=listeP[i].getId()%></td>
                                    <td align=center><%=listeP[i].getRaisonsocial()%></td>
                                    <td align=center><%=listeP[i].getMatrcnaps() %></td>
                                    <td align=center><%=listeP[i].getCompte_tiers()%></td>
                                    <td align=center><%=listeP[i].getCompteg() %></td>
                                    <td align=center><%=listeP[i].getVal() %></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>