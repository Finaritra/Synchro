<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.recette.LocLocation"%>

<%
    String champReturn = request.getParameter("champReturn");
    LocLocation fact = new LocLocation();
    fact.setNomTable("loc_location_libelle");
    String listeCrt[] = {"id", "id_batiment","id_locataire","montant","date_debut","date_fin"};
    String listeInt[] = null;
    String libEntete[] = {"id", "id_batiment","id_locataire","montant","date_debut","date_fin"};
    PageRechercheChoix pr = new PageRechercheChoix(fact, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeLocationChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id_locataire").setLibelle("Locataire");
    pr.getFormu().getChamp("id_batiment").setLibelle("Batiment");
    pr.getFormu().getChamp("date_debut").setLibelle("D&eacute;but de location");
    pr.getFormu().getChamp("date_fin").setLibelle("Fin de location");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste locataires</h1>
            </section>
            <section class="content">
                <form action="listeLocationChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    
					String libEnteteAffiche[] = {"Id", "Nom","Pr&eacute;nom","Raison social","BP","Adresse"};
                    pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>