<%-- 
    Document   : prtfPlacement
    Created on : 10 d�c. 2015, 09:33:05
    Author     : user
--%>
<%@page import="mg.cnaps.prtf.PrtfPlacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>


<%
    String champReturn = request.getParameter("champReturn");
    PrtfPlacement e = new PrtfPlacement();
    e.setNomTable("PRTF_PLACEMENT");
    String listeCrt[] = {"id", "idouverture", "numero", "ref_dossier", "id_type_placement","numero_compte","date_ouverture", "date_constatation"};
    String listeInt[] = {"date_ouverture", "date_constatation"};
    String libEntete[] = {"id", "idouverture", "numero", "ref_dossier", "id_type_placement","numero_compte","date_ouverture", "date_constatation", "montant_adjuge", "montant_propose", "montant_escompte"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 11);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("prtfPlacement.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("idouverture").setLibelleAffiche("Ouverture");
    pr.getFormu().getChamp("ref_dossier").setLibelleAffiche("Dossier");
    pr.getFormu().getChamp("id_type_placement").setLibelleAffiche("Type Placement");
    pr.getFormu().getChamp("numero_compte").setLibelleAffiche("Numero de compte");
    pr.getFormu().getChamp("date_ouverture1").setLibelleAffiche("Date d'ouverture min");
    pr.getFormu().getChamp("date_ouverture2").setLibelleAffiche("Date d'ouverture max");
    pr.getFormu().getChamp("date_constatation1").setLibelleAffiche("Date de constatation min");
    pr.getFormu().getChamp("date_constatation2").setLibelleAffiche("Date de constatation max");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste placement</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="ouverture" id="ouverture">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[]= {"Id", "Ouverture", "Numero", "Ref dossier", "Type placement","Numero compte","Date d'ouverture", "Date de constatation", "Montant adjuge", "Montant propose", "Montant escompte"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

