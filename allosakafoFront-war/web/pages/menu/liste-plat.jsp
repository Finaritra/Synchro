<%-- 
    Document   : liste-plat
    Created on : 17 sept. 2019, 11:28:24
    Author     : Notiavina
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Random"%>
<%@page import="affichage.*"%>
<%@page import="user.*"%>
<%@page import="mg.allosakafo.produits.ProduitsTypeLibelle"%>
<%@page import="bean.*"%>
<%@ page import="utilitaire.*" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="mg.allosakafo.produits.AccompagnementSauce"%>

<style>
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    .fourniseur{
        background: #000;
        top: inherit;
        border-bottom-right-radius: 10px;
        right: inherit;
        border-bottom-left-radius: inherit
    }
</style>
<%
    PageRecherche pr = null;
    try {
        UserEJB u = (user.UserEJB) UserEJBClient.lookupUserEJBBeanLocal();
        
        ProduitsTypeLibelle produit = new ProduitsTypeLibelle();
        produit.setNomTable("DISPONIBILITE_PRODUIT");
        if(session.getAttribute("idResa")!=null)produit.setNomTable("DISPONIBILITE_PRODUIT");
        String filtreproduit = "";
        String filtrecat = "";
        String tri = "";
        String awhere_filtre = "";
        String filtre_idtypeproduit = "";
        String filtre_idsouscategorie = "";
        String produitlibelle = "";
        request.setCharacterEncoding("UTF-8");
        if (request.getParameter("produit") != null) {
            produitlibelle = request.getParameter("produit");
        }
        if (produitlibelle != null && produitlibelle != "") {
            filtreproduit += "&produit=" + produitlibelle;
            awhere_filtre += " and upper(nom) like upper('%" + produitlibelle + "%')";
        }
        if (request.getParameter("type") != null && request.getParameter("type").compareToIgnoreCase("") != 0) {
            filtrecat += "&type=" + request.getParameter("type");
            awhere_filtre += " and idtypeproduit like '%" + request.getParameter("type") + "%'";
        }
        if (request.getParameter("idtypeproduit") != null && !"".equals(request.getParameter("idtypeproduit"))) {
            filtre_idtypeproduit += "&idtypeproduit=" + request.getParameter("idtypeproduit");
            awhere_filtre += " and idtypeproduit='" + request.getParameter("idtypeproduit") + "'";
        }
        if (request.getParameter("idsouscategorie") != null && !"".equals(request.getParameter("idsouscategorie"))) {
            filtre_idsouscategorie += "&idsouscategorie=" + request.getParameter("idsouscategorie");
            awhere_filtre += " and idsouscategorie='" + request.getParameter("idsouscategorie") + "'";
        }

        String listeCrt[] = {"id", "nom", "designation", "typeproduit"};
        String listeInt[] = {};
        String libEntete[] = {"id", "nom", "designation","calorie", "typeproduit", "poids", "pa"};
        String[] colSomme = null;

        pr = new PageRecherche(produit, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
        pr.setUtilisateur(u);

        pr.setLien("home.jsp");
        if (request.getParameter("tri") != null) {
            if (request.getParameter("tri").equals("N")) {
                awhere_filtre += " ORDER BY nom ASC";
                tri += "&tri=N";
            }
            if (request.getParameter("tri").equals("P")) {
                awhere_filtre += " ORDER BY pu ASC";
                tri += "&tri=P";
            }
        }
        
        awhere_filtre += "and idpoint = '"+session.getAttribute("restaurant").toString()+"' and pointindisp is null";
        pr.setAWhere(awhere_filtre);
        pr.setApres(request.getParameter("but") + filtreproduit + filtrecat + filtre_idtypeproduit + filtre_idsouscategorie + tri);
        pr.setNpp(20);
        pr.creerObjetPage(libEntete, colSomme);

        TypeObjet c = new TypeObjet();
        c.setNomTable("as_typeproduit");
        TypeObjet[] listeType = (TypeObjet[]) CGenUtil.rechercher(c, null, null, "");
        AccompagnementSauce acc = new AccompagnementSauce();
        acc.setNomTable("AS_ACCOMPAGNEMENT_SAUCE_LIB");
        AccompagnementSauce[] sauce = (AccompagnementSauce[]) CGenUtil.rechercher(acc, null, null, " and etat>0");
        double prix=0;
        String choixresto =(String) session.getAttribute("choixresto");
%>
<div class="row">
    <jsp:include page="../commande/listecommande.jsp"/>
    <div class="col-md-12 col-xs-12 content-box"> 
        <div class="col-xs-12 content-box-content-bottom">
            <div class="header-title header-content bon-plan header-content-title">
                <span class="header-gamepub-logo header-content-logo header-content-logo-bottom">
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                </span>
                BONS PLATS SUR Ph� RESTO
            </div>
            <div class="col-xs-12 content-body">
                <div class="col-xs-12">
                    <form class="form" action="home.jsp?but=pages/menu/liste-plat.jsp" method="POST" autocomplete="off"> 
                        <div class="col-sm-6 col-xs-12">
                            <div class="input-group form-input-label-mobile">
                                <div class="input-group-addon"><strong>Nom du plat</strong></div>
                                <%if (produitlibelle != null && produitlibelle != "") {%>
                                <input type="text" class="form-control" value="<%=produitlibelle%>" name="produit" id="produit"/>
                                <% } else { %>
                                <input type="text" class="form-control" placeholder="Taper le nom du plat" name="produit"  id="produit" />
                                <%}%>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 content-second-form">
                            <div class="input-group form-input-label-mobile">
                                <div class="input-group-addon"><strong>Cat&eacute;gorie du plat</strong></div>
                                <select class="form-control" name="type" id="type">
                                    <option value="">Tous</option>
                                    <%for (TypeObjet type : listeType) {%>
                                    <% if ((request.getParameter("type") != null && request.getParameter("type") != "") && (request.getParameter("type").compareToIgnoreCase(type.getId()) == 0)) {%>
                                    <option value="<%=type.getId()%>" selected=""><%=type.getVal()%></option>
                                    <% } else {%>
                                    <option value="<%=type.getId()%>"><%=type.getVal()%></option>
                                    <%}
                                        }%>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 content-second-form content-desk-first-form">
                            <div class="input-group form-input-label-mobile">
                                <div class="input-group-addon"><strong>Trier par:</strong></div>
                                <select class="form-control" name="tri" id="type">
                                    <option value=""></option>
                                    <%if (request.getParameter("tri") != null && request.getParameter("tri").equals("N")) {%>
                                    <option value="N" selected>Nom</option>
                                    <option value="P">Prix</option>
                                    <%}%>
                                    <%if (request.getParameter("tri") != null && request.getParameter("tri").equals("P")) {%>
                                    <option value="N">Nom</option>
                                    <option value="P" selected>Prix</option>
                                    <%}%>
                                    <%if (request.getParameter("tri") == null) {%>
                                    <option value="N">Nom</option>
                                    <option value="P">Prix</option>
                                    <%}%>

                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-product-search"><span class="glyphicon glyphicon-search"></span> Afficher</button>
                    </form>
                </div>
                <div class="col-xs-12 content-body-produit">
                    <%
                        int i = 0;
                        ProduitsTypeLibelle[] listeProduit = (ProduitsTypeLibelle[]) pr.getTableau().getData();
                        AccompagnementSauce[] tsauce = null;
                        for (ProduitsTypeLibelle plat : listeProduit) {
                            prix = plat.getPu();
                            if(choixresto!=null && choixresto.contains("lounge"))prix=plat.getPrixl();
                            Vector list = new Vector();
                            for (int j = 0; j < sauce.length; j++) {
                                if (plat.getId().equals(sauce[j].getRemarque())) {
                                    list.add(sauce[j]);
                                }
                            }
                            tsauce = new AccompagnementSauce[list.size()];
                            list.copyInto(tsauce);

                            /*AccompagnementSauce acc = new AccompagnementSauce();
                            acc.setNomTable("AS_ACCOMPAGNEMENT_SAUCE_LIB");
                            acc.setRemarque(plat.getId());
                            AccompagnementSauce[] sauce = (AccompagnementSauce[])CGenUtil.rechercher(acc, null, null, "");*/
                            String photo = "";
                            if(plat.getListePhoto() != null){
                                photo = plat.getListePhoto()[0];
                            }     
                            %>
                    <div class="col-xs-6 col-sm-4 col-md-3 box-product box-product-height">
                        <p class="box-product-name" onclick="ficheProduit('<%=plat.getId()%>')"><%=plat.getDesignation()%></p>
                        <div class="box-img">
                            <p class="box-stock fourniseur"><%=plat.getTypeproduit()%></p>
                            <img class="box-product-img box-product-list-img" src="${pageContext.request.contextPath}/../Dossier/img/plats/produit/<%= photo%>" height="150" onclick="ficheProduit('<%=plat.getId()%>')"/>
                        </div>
                        <div class="box-product-price-section">
                            <p class="product-box-price"><span class="prix-mga">Ar <%=(Utilitaire.formaterAr(prix).split(",")[0])%> </span></p>
                        </div>
                        <button class="btn-add-panier" data-toggle="modal" data-target="#Modal<%=plat.getId()%>">Commander</button>
                    </div>

                    <div class="modal fade modal-z-index" id="Modal<%=plat.getId()%>" tabindex="-1" role="dialog" aria-labelledby="ModalLabel2" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content" id="modal-content2">
                                <form action="Plat" method="GET" >
                                    <div class="modal-header" style="height: 43px;">
                                        <div class="header-title header-content">
                                            <span class="header-gamepub-logo">
                                                <span class="glyphicon glyphicon-shopping-cart"></span>
                                            </span>
                                            AJOUTER AU COMMANDE
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <img src="${pageContext.request.contextPath}/../Dossier/img/plats/produit/<%= photo%>" class="img-thumbnail" style="border-radius: 30px" height="150"/>
                                            </div>
                                            <div class="col-xs-6" style="margin-top: 27px;">
                                                <h4><%=plat.getNom()%></h4>
                                                <SELECT name="idsauce" class="form-control" style="margin-bottom: 10px">
                                                    <%for (int index = 0; index < tsauce.length; index++) {%> 
                                                    <OPTION value="<%=tsauce[index].getId()%>"><%=tsauce[index].getIdaccompagnement() + " " + tsauce[index].getIdsauce()%></OPTION>
                                                        <% }%>
                                                </SELECT>
                                                <h4>Quantit&eacute; : </h4>
                                                <div class="input-group">
                                                    <div id="updown" style="margin-left: -126px;">
                                                        <script language="JavaScript">
                                                            var v = document.URL;
                                                            var lien = v.replace('produit_home', 'panier');
                                                            document.write("<input type=\"hidden\" value=\"" + lien + "\" id=\"lien\" name=\"lien\"/>");
                                                        </script>
                                                        <input type="hidden" value="<%=plat.getId()%>" id="produit" name="produit"/>
                                                        <input type="hidden" value="<%=plat.getTypeproduit()%>" id="idtypep" name="idtypep"/>
                                                        <input type="hidden" value="<%=prix%>" id="p" name="p"/>
                                                        <input type="number" name="qt" class="form-control" placeholder="quantite" min="1" step="1" value="1" id="2qteprod<%=i%>" style="width: 70px; margin-left: 126px; padding-left: 24px; -webkit-appearance: none;-moz-appearance:textfield;" onchange="qteprod2(<%=i%>)"/>
                                                        <input type="hidden" value="<%=prix%>" id="2pu<%=i%>"/>
                                                        <div class="input-group-addon" style="height: 34px"><a class="glyphicon glyphicon-chevron-up" onclick="haut2(<%=i%>)"></a></div>
                                                        <div class="input-group-addon"><a class="glyphicon glyphicon-chevron-down" onclick="bas2(<%=i%>)"></a></div>
                                                    </div>
                                                </div>
                                                <h4>Remarque</h4>
                                                <input type="text" id="remarque" name="remarque" class="form-control" style="margin-bottom: 10px"/>
                                                <h4 style="text-decoration: underline">Prix unitaire : <span style="font-size: 17px;">Ar <%=Utilitaire.formaterAr(prix)%></span></h4>
                                                <h4>Total : Ar <span id="total<%=i%>"><%=Utilitaire.formaterAr(prix)%></span></h4>
                                                <p style="font-size: 17px"><%=plat.getDesignation()%></p>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-xs-6">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input   type="submit" class="btn btn-primary" value="OK"/>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <% i++;
                        }
                    %>
                    <% if (pr != null) {
                            out.print(pr.getBasPageFront());
                        }
                    %>
                </div>

            </div>
        </div>
    </div>
</div>
<%  } catch (Exception e) {
        e.printStackTrace();
        out.println("<script language='JavaScript'> alert('"+e.getMessage()+"'); history.back();</script>");
    }
%>