<%@page import="lc.Direction"%>
<%@page import="affichage.Liste"%>
<%@page import="historique.MapRoles"%>
<%@page import="affichage.PageUpdate"%>
<%@page import="user.UserEJB"%>
<%@page import="historique.MapUtilisateur"%>

<%
    UserEJB u;
    MapUtilisateur art;
%>
<%
    u = (user.UserEJB) session.getValue("u");
    art = u.getUser();
    PageUpdate pi = new PageUpdate(art, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));

    pi.getFormu().getChamp("loginuser").setLibelle("Login");
    pi.getFormu().getChamp("PWDUSER").setLibelle("Mot de passe");
    pi.getFormu().getChamp("PWDUSER").setDefaut("");
    pi.getFormu().getChamp("PWDUSER").setType("password");
    pi.getFormu().getChamp("ADRUSER").setLibelle("Direction");
    pi.getFormu().getChamp("IDROLE").setLibelle("Profil");
    pi.getFormu().getChamp("teluser").setLibelle("Personnel");
    pi.getFormu().getChamp("teluser").setVisible(false);
    affichage.Champ[] liste = new affichage.Champ[2];
    MapRoles m = new MapRoles();
    liste[0] = new Liste("idrole", m, "descrole", "idrole");
    Direction d = new Direction();
    liste[1] = new Liste("ADRUSER", d, "libelledir", "idDir");
    pi.getFormu().changerEnChamp(liste);
    pi.preparerDataFormu();


%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification Utilisateur</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=utilisateur/apresUser.jsp" method="post" name="starticle">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="utilisateur/utilisateur-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="historique.MapUtilisateur">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                        <input name="nomtable" type="hidden" id="nomtable" value="utilisateur">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
