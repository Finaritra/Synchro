<%-- 
    Document   : direction-fiche
    Created on : 17 d�c. 2015, 11:14:32
    Author     : Jetta
--%>

<%@page import="lc.Direction"%>
<%@page import="mg.cnaps.sig.SigPersonnes"%>
<%@page import="mg.cnaps.medecine.MedDemandeConsultation"%>
<%@page import="mg.cnaps.medecine.MedConsultation"%>
<%@page import="mg.cnaps.pointage.PointSanction"%>
<%@page import="mg.cnaps.formation.FormParticipantInfoComplet"%>
<%@page import="mg.cnaps.pret.PretDemande"%>
<%@page import="mg.cnaps.pret.PretEmprunt"%>
<%@page import="mg.cnaps.immo.ImmoCession"%>
<%@page import="java.sql.Connection"%>
<%@page import="mg.cnaps.immo.ImmoLibelleAttribue"%>
<%@page import="mg.cnaps.formation.FormParticipantInfoLog"%>
<%@page import="mg.cnaps.formation.FormFormation"%>
<%@page import="mg.cnaps.formation.FormParticipantDemande"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.log.*" %>
<%@page import="mg.cnaps.paie.*"%>
<%!
    UserEJB u = null;
    String lien = null;
    lc.Direction dir = null;
    PageConsulte pc = null;
%>
<%
    try {
        u = (UserEJB) session.getAttribute("u");
        lien = (String) session.getValue("lien");

        dir = new Direction();
        //dir.setNomTable("LOG_PERSONNEL_INFO");
        String[] libellePersonnelFiche = {"Id", "Libelle", "Description", "Directeur", "Abreviation"};
        pc = new PageConsulte(dir, request, (user.UserEJB) session.getValue("u"));
        pc.setLibAffichage(libellePersonnelFiche);
        pc.setTitre("Fiche Direction");

%>
<div class="content-wrapper">
    <div class="row">
        <div class="tab-pane active" id="tab_8-2">
            <div class="row">
                <div class="col-md-6">
                    <div class="box-fiche">
                        <div class="box">
                            <div class="box-title with-border">
                                <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=utilisateur/direction-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                            </div>
                            <div class="box-body">
                                <%
                                    out.println(pc.getHtml());
                                %>
                                <br/>
                                <div class="box-footer">
                                    <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=utilisateur/direction-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                                    <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=utilisateur/direction-liste.jsp&classe=lc.Direction" style="margin-right: 10px">Supprimer</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%out.println(pc.getBasPage());%>
        </div>
    </div><!-- /.tab-content -->
</div><!-- nav-tabs-custom -->
<%} catch (Exception e) {
        throw new Exception(e.getMessage());
    }
%>