<%-- 
    Document   : commandeLivraison-saisie
    Created on : 12 mars 2020, 09:58:53
    Author     : Maharo R.
--%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="service.AlloSakafoService"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.commun.Constante"%>

<script>
function reservation(valeur) 
{
    var param = {'telephone':valeur};
    $.ajax({
        type:'GET',
        url:'CompleterLivraison',
        contentType: 'application/json',
        data:param,
        success:function(ma){
            var data = JSON.parse(ma);
            $("#idclient").val(data.client);
            $("#adresse").val(data.adresse);

        }
    });

}
</script>

<%
    TypeObjet point = new TypeObjet();
    point.setNomTable("point");
    TypeObjet[] liste = (TypeObjet[]) AlloSakafoService.getListe(point, "");
    String defaut = session.getAttribute("restaurant").toString();
%>

<div class="container container-index container-mobile">
    <div class="row">
        <div class="col-xs-12 content-login">
            <img src="assets/img/logo.png" id="logo-ketrika" alt="Logo">
            <div class="col-xs-12 content-login-body">
                <div class="col-xs-4 col-sm-4 content-login-form">
                    <form class="form-inline login" action="pages/livraison/apresResa.jsp?choixresto=<%=request.getParameter("choixresto")%>" method="POST">
                         <input type="hidden" name="choixresto" value="normal" >
                        <h1>Saisie de reservation</h1>
                        <br>
                        

                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label> Num�ro de t�l�phone/Nom </label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                                <input type="text" name="idclient" id="idclient" placeholder="telephone - nom" onblur="reservation(this.value)">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                             <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label>Nombre de personne</label>
                             </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                                <input type="text" name="nbpersonne" id="nbpersonne">
                            </div>
                        </div>
                        <br>
                        
                       
                        <div class="row">
                             <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label>Date</label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                            <input type="text" name="daty" value="<%=Utilitaire.dateDuJour()%>">
                            </div>
                         </div>
                             <br>
                        <div class="row">
                             <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label>Heure</label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                                <input type="text" name="heure" value="<%=Utilitaire.ajoutHeure(Utilitaire.heureCouranteHM(), 0, Constante.dureeLivraisonDefaut  ,0) %>">
                            </div>
                        </div>
                            <br>
                        <div class="row">
                             <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label>Remarque</label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                                <input type="textarea" name="remarque" >
                            </div>
                        </div>
                           
                            <br>
                        <div class="row">
                             <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label>Point</label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                                <select name="point" >
                                    <% for (TypeObjet p : liste) {
                                        if(defaut.equals(p.getId())) {%>
                                            <option value="<%=p.getId()%>" selected><%=p.getVal()%></option>
                                        <% } else {%>
                                            <option value="<%=p.getId()%>"><%=p.getVal()%></option>
                                        <% }%>
                                    <% }%>
                                </select>
                            </div>
                        </div>
                      </br>
                        <div class="login-connect col-xs-12">

                          <button type="submit" name="defaulttable" class="btn btn-success col-xs-12 btn-choix-table"><strong>Valider</strong></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
