<%-- 
    Document   : facture-liste-appro.jsp
    Created on : 15 juin 2016, 10:46:45
    Author     : Joe
--%>
<%@page import="mg.cnaps.st.StFactureAppro"%>
<%@page import="affichage.PageRecherche"%>
<%
    StFactureAppro lv = new StFactureAppro();
    lv.setNomTable("st_facture_appro");
    if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("") != 0) {
        lv.setNomTable(request.getParameter("etat"));
    }
    String listeCrt[] = {"id", "daty", "facture", "commande", "montant"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "facture", "commande", "montant"};
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setLibelle("Date inf");
    pr.getFormu().getChamp("daty2").setLibelle("Date sup");

    pr.setApres("stock/bc/facture-liste-appro.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste facture</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/bc/facture-liste-appro.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="etat" class="champ" id="etat" onchange="changerDesignation()" >
                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("st_facture_appro") == 0) {%>    
                        <option value="st_facture_appro" selected>Tous</option>
                        <% } else { %>
                        <option value="st_facture_appro" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_FACTURE_APPRO_ATC") == 0) {%>
                        <option value="ST_FACTURE_APPRO_ATC" selected>Attach&eacute;e</option>
                        <% } else { %>
                        <option value="ST_FACTURE_APPRO_ATC">Attach&eacute;e</option>
                        <% } %>

                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_FACTURE_APPRO_NONATC") == 0) {%>
                        <option value="ST_FACTURE_APPRO_NONATC" selected>Non attach&eacute;e</option>
                        <% } else { %>
                        <option value="ST_FACTURE_APPRO_NONATC">Non attach&eacute;e</option>
                        <% } %>

                    </select>
                </div>
                <div class="col-md-4"></div>
            </div>

        </form>
        <%  
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "Facture", "Commande", "Montant"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
