<%-- 
    Document   : stEtatBonCommande-liste
    Created on : 22 f�vr. 2016, 09:04:35
    Author     : Tafitasoa
--%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageRechercheGroupe"%>
<%@page import="mg.cnaps.st.StEtatBc"%>
<%@page import="mg.cnaps.st.StBonDeCommande"%>
<%@page import="affichage.PageRecherche"%>
<%
    StEtatBc lv = new StEtatBc();
    String listeCrt[]={"id","daty","designation","type_article"};
    String listeInt[]={"daty"};
    
    
    String libEntete[]={"id","daty","designation","type_article","nbrcommande","nbrlivre","restecommande"};
    
    PageRecherche pr=new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.getFormu().getChamp("type_article").setLibelle("Chapitre");
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date max");

    pr.setUtilisateur((user.UserEJB)session.getValue("u"));
    pr.setLien((String)session.getValue("lien"));
    pr.setApres("stock/bc/stEtatBonCommande-liste.jsp");
    //pr.getFormu().getChamp("").setp
    //String[] colSomme = {"","",""};
    pr.creerObjetPage(libEntete, null);
    StEtatBc[] listEtat = (StEtatBc[]) pr.getTableau().getData();
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Etat de livraison de bon de Commande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/bc/stEtatBonCommande-liste.jsp" method="post" name="etat" id="etat">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <br>
        <div class="row">
            <div class="row col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Liste</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <th>Id</th>
                                <th>Date</th>
                                <th>Designation</th>
                                <th>Type article</th>
                                <th>Nombre Commande</th>
                                <th>Nombre Livr�</th>
                                <th>Reste Commande</th>
                            </thead>
                            <tbody>
                                <%
                                    for (StEtatBc element : listEtat) {
                                %>
                                <tr>
                                    <td><a href="<%=(String)session.getValue("lien") +"?but=stock/bc/stBonDeCommande-fiche.jsp&id="+element.getId()%>"><%=Utilitaire.champNull(element.getId())%></a></td>
                                    <td><%=Utilitaire.formatterDaty(element.getDaty())%></td>
                                    <td><%=Utilitaire.champNull(element.getDesignation())%></td>
                                    <td><%=Utilitaire.champNull(element.getType_article())%></td>
                                    <td align="right"><%=Utilitaire.formaterAr(element.getNbrcommande()) %></td>
                                    <td align="right"><%=Utilitaire.formaterAr(element.getNbrlivre()) %></td>
                                    <td align="right"><%=Utilitaire.formaterAr(element.getRestecommande()) %></td> 
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <%
            out.println(pr.getBasPage());
        %>
    </section>
</div>

