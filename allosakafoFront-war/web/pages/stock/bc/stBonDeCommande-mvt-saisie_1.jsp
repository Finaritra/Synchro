
<%@page import="mg.cnaps.st.StBonDeCommande"%>
<%@page import="mg.cnaps.budget.BudgetProjet"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.st.StBonDeLivraisonFille"%>
<%@page import="java.sql.Connection"%>
<%@page import="mg.cnaps.st.StBonDeCommandeFilleLibelle"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="java.util.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    String idmere = request.getParameter("id");
    String quantite = "";
    UserEJB u = (UserEJB) session.getAttribute("u");
    Connection c = null;
    try {
        c = (new utilitaire.UtilDB()).GetConn();

        StBonDeCommande bcmere = new StBonDeCommande();
        bcmere.setNomTable("STBONDECOMMANDE");
        StBonDeCommande listBC = ((StBonDeCommande[]) CGenUtil.rechercher(bcmere, null, null, " and ID = '" + idmere.trim() + "'"))[0];
        if (listBC.getEtat() < ConstanteEtat.getEtatValider()) {
%>
<script language="JavaScript">alert('Bon de commande non VISEE');
    history.back();</script>
    <%
        }
        if (listBC.getEtat() == ConstanteEtat.getEtatCloture()) {
    %>
<script language="JavaScript">alert('Bon de commande d�j� CLOTUREE');
    history.back();</script>
    <%
        }
        StBonDeCommandeFilleLibelle crt = new StBonDeCommandeFilleLibelle();
        crt.setNomTable("St_BonDeCommandeFille_Libelle");
        StBonDeCommandeFilleLibelle[] listFille = (StBonDeCommandeFilleLibelle[]) CGenUtil.rechercher(crt, null, null, " and idbc = '" + idmere + "'");
        String lien = (String) session.getValue("lien");

        StMagasin mg = new StMagasin();
        mg.setNomTable("ST_MAGASIN_LIBELLE");
        StMagasin[] lv = (StMagasin[]) CGenUtil.rechercher(mg, null, null, c, "order by desce");

        TypeObjet classe = new TypeObjet();
        classe.setNomTable("st_classe");
        TypeObjet[] cls = (TypeObjet[]) CGenUtil.rechercher(classe, null, null, c, "");

        TypeObjet tbn = new TypeObjet();
        tbn.setNomTable("st_type_besoin");
        TypeObjet[] tb = (TypeObjet[]) CGenUtil.rechercher(tbn, null, null, c, "");
        for (int co = 0; co < tb.length; co++) {
            if (tb[co].getDesce().equalsIgnoreCase("ordinaire")) {
                TypeObjet tmp = tb[0];
                tb[0] = tb[co];
                tb[co] = tmp;
            }
        }

        BudgetProjet prj = new BudgetProjet();
        prj.setNomTable("Budget_Projet");
        BudgetProjet[] lprj = (BudgetProjet[]) CGenUtil.rechercher(prj, null, null, c, " order by INTITULE desc");

        StBonDeLivraisonFille blf = new StBonDeLivraisonFille();
        blf.setNomTable("ST_BONDECOMMAND_LIVREE");
        StBonDeLivraisonFille[] somLivree = (StBonDeLivraisonFille[]) CGenUtil.rechercher(blf, null, null, " and id = '" + idmere + "'");
        System.out.println("Nombre somme: " + somLivree.length);

    %>
<div class="content-wrapper">
    <style>
        .champ
        {
            border-radius: 7px;
        }
    </style>
    <h1 align="center">Livraison bon de commande</h1>
    <form action="<%=lien%>?but=stock/bc/apresBC.jsp" method="post" name="approfille" id="approfille" data-parsley-validate>

        <table class="table" width="30%">
            <tr>
                <th>Date</th>
            <td><input id="excercice" name="excercice" class="form-control datepicker" type="text" value="<%=Utilitaire.dateDuJour()%>"></td>
            </tr>
            <tr>
                <th>Magasin</th>
            <td>
                <select name="magasin" class="champ form-control" id="magasin" tabindex="1" data-parsley-id="4">
                    <%
                        for (int i = 0; i < lv.length; i++) {
                    %>
                    <option value="<%=lv[i].getId()%>"><%=lv[i].getDesce()%></option>
                    <%
                        }
                    %>
                </select>
            </td>
            </tr>

            <tr>
                <th>Type</th>
            <td>
                <select name="typebesoin" class="champ form-control" id="typebesoin" tabindex="8" data-parsley-id="3">
                    <%
                        for (int i = 0; i < tb.length; i++) {
                    %>
                    <option value="<%=tb[i].getId()%>"><%=tb[i].getVal()%></option>
                    <%
                        }
                    %>
                </select>
            </td>
            </tr>
            <tr>
                <th>Num BL/Facture</th>
                <td>
                    <input type="text" name="numfacture" class="champ form-control" id="numfacture"  tabindex="9" data-parsley-id="4">
                </td>
            </tr>

        </table>
        <table class="table-striped table-hover table-bordered table-condensed" width="100%">
            <thead>
                <tr>
                    <th style="background-color:#bed1dd">Article</th>
                    <th style="background-color:#bed1dd">Livr&eacute;e</th>
                    <th style="background-color:#bed1dd">Reste &agrave; livr&eacute;e</th>
                    <th style="background-color:#bed1dd">Quantit&eacute; total</th>
                    <th style="background-color:#bed1dd">P.U</th>
                    <th style="background-color:#bed1dd">Remarque</th>
                </tr></thead>
            <tbody>
                <%
                    for (int i = 0; i < listFille.length; i++) {
                        double livree = 0;
                        quantite = listFille[i].getQuantite() + "";
                        for (int k = 0; k < somLivree.length; k++) {
                            if (somLivree[k].getId().compareToIgnoreCase(listFille[i].getIdbc()) == 0 && somLivree[k].getArticle().compareToIgnoreCase(listFille[i].getIdArticle()) == 0) {
                                double Qte = listFille[i].getQuantite() - somLivree[k].getQuantite();
                                livree = somLivree[k].getQuantite();
                                quantite = "" + Qte;
                            }
                        }
                %>
                <tr>
                    <td><%=listFille[i].getArticle()%></td>
                    <td><%=livree%></td>
                    <td>
                        <input type="hidden" name="id" id="id" value="<%=listFille[i].getIdArticle()%>" >
                        <input type="text" name="quantite" id="quantite" value="<%=quantite%>" class="form-control">
                        <input type="hidden" name="qtemax" id="qtemax" value="<%=quantite%>">
                    </td>
                    <td><%=listFille[i].getQuantite()%></td>

                    <td>
                        <%=listFille[i].getPu()%>
                        <input type="hidden" name="pu" id="pu" value="<%=listFille[i].getPu()%>"></td>
                    <td>
                        <%=listFille[i].getRemarque()%>
                        <input type="hidden" name="remarque" id="remarque" value="<%=listFille[i].getRemarque()%>"></td>

                </tr>
                <%
                            
                    }
                %>
            </tbody>
        </table>
        <input name="idmere" type="hidden" id="nature" value="<%=request.getParameter("id")%>">
        <input name="acte" type="hidden" id="nature" value="insertwbc">
        <input name="bute" type="hidden" id="bute" value="stock/bc/stBonDeCommande-liste.jsp">
        
        <div class="col-md-12">
            <button class="btn btn-primary pull-right" type="submit">Valider</button>
        </div>
    </form>
</div>
<% } catch (Exception ex) {
        ex.printStackTrace();
    } finally {
        if (c != null) {
            c.close();
        }
    }
%>
