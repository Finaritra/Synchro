<%--
    Document   : stBonDeCommande-appro
    Created on : 21 f�vr. 2016, 21:24:12
    Author     : Joe
--%>

<%@page import="mg.cnaps.st.StDemandeFounitureFille"%>
<%@page import="mg.cnaps.log.LogService"%>
<%@page import="mg.cnaps.budget.BudgetTef"%>
<%@page import="mg.cnaps.st.StDemandeFourniture"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.commun.Dr"%>
<%@page import="mg.cnaps.st.StBonDeCommande"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<script type='text/javascript'>
    function getXhr() {
        var xhr = null;

        if (window.XMLHttpRequest || window.ActiveXObject) {
            if (window.ActiveXObject) {
                try {
                    xhr = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
            } else {
                xhr = new XMLHttpRequest();
            }
        } else {
            alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
            return null;
        }

        return xhr;
    }
    function request(oSelect) {
        var xhr = getXhr();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                readData(xhr.responseXML);
                document.getElementById("loader").style.display = "none";
            }
        };
        xhr.open("POST", "ajaxService.jsp", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        var value = oSelect.options[oSelect.selectedIndex].value;
        xhr.send("IdEditor=" + value);
    }
    function readData(oData) {
        var nodes = oData.getElementsByTagName("item");
        var oSelect = document.getElementById("vendeur");
        var oOption, oInner;
        oSelect.innerHTML = "";
        var i = 0;
        for (i = 0; i < nodes.length; i++) {
            oOption = document.createElement("option");
            oInner = document.createTextNode(nodes[i].getAttribute("name"));
            oOption.value = nodes[i].getAttribute("id");

            oOption.appendChild(oInner);
            oSelect.appendChild(oOption);
        }
    }
    function checkVendeur(obj)
    {
        $.ajax({
            url: 'ajaxService.jsp?IdEditor=' + obj.value,
            success: function (data)
            {
                $("#service").html(data);
            }
        });
    }

    function loadVendeur(req)
    {
        $.ajax({
            url: 'ajaxService.jsp?IdEditor=' + req,
            success: function (data)
            {
                $("#service").html(data);
            }
        });
    }

</script>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StBonDeCommande da = new StBonDeCommande();
    PageInsert pi = new PageInsert(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    affichage.Champ[] liste = new affichage.Champ[3];
//
//
    TypeObjet d = new TypeObjet();
    d.setNomTable("log_direction");
    liste[0] = new Liste("direction", d, "val", "id");
//
    TypeObjet md = new TypeObjet();
    md.setNomTable("PAIE_MODEPAIEMENT");
    liste[1] = new Liste("modepayement", md, "desce", "id");

    LogService ds = new LogService();
    ds.setNomTable("LOG_SERVICE_LIBST");
    liste[2] = new Liste("service", ds, "libelle", "id");
    
    pi.getFormu().changerEnChamp(liste);
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("datelivraison").setVisible(false);
    pi.getFormu().getChamp("designation").setType("textarea");
    pi.getFormu().getChamp("fournisseur").setPageAppel("choix/listeFournisseurChoix.jsp");
    pi.getFormu().getChamp("code_dr").setVisible(false);
    pi.getFormu().getChamp("etat").setVisible(false);
    pi.getFormu().getChamp("numtef").setVisible(false);
    pi.getFormu().getChamp("modepayement").setLibelle("Mode de paiement");
    pi.getFormu().getChamp("direction").setAutre("onchange='checkVendeur(this);'");
    pi.getFormu().getChamp("service").setLibelle("Service");
    pi.getFormu().getChamp("tva").setDefaut("20");
    pi.getFormu().getChamp("tva").setLibelle("TVA");
    pi.preparerDataFormu();

    if (request.getParameter("id") != null && request.getParameter("id").compareToIgnoreCase("") != 0) {
        String id = request.getParameter("id");
        String apw = " AND IDDEMANDE = '" + id + "'";

        StDemandeFourniture dmf = new StDemandeFourniture();
        dmf.setNomTable("ST_DEMANDE_FOURNITURE_LIBELLE");
        StDemandeFourniture[] ld = (StDemandeFourniture[])u.getData(dmf, null, null, null, " AND ID = '" + id + "'");
        StDemandeFounitureFille tef = new StDemandeFounitureFille();
        tef.setNomTable("st_demande_fourniture_bc");
        StDemandeFounitureFille[] listFille = (StDemandeFounitureFille[]) u.getData(tef, null, null, null, apw);

%>
<div class="content-wrapper">
    <h1 align="center" class="box-title">Bon de commande</h1>
    <form action="<%=pi.getLien()%>?but=stock/bc/apresBC.jsp" method="post" name="appro" id="appro" data-parsley-validate>

        <%
            //
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>

        <div class="row">
            <div class="col-md-6">
                <div class="box-fiche">
                    <div class="box">
                       <div class="box-body">
                            <table class="table table-hover" width="95%">
                                <tbody>
                                    <tr>
                                        <td style="background-color:#bed1dd">Chapitre:</td>
                                        <td><%=ld[0].getChapitre()%></td>
                                    </tr>
                                    <tr>
                                        <td style="background-color:#bed1dd">Projet:</td>
                                        <td><%=ld[0].getFournisseur()%></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <div class="box">
                        <div class="box-title with-border">
                            <h1 class="box-title">D&eacute;tails</h1>
                        </div>
                        <div class="box-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th style="background-color:#bed1dd">Article</th>
                                        <th style="background-color:#bed1dd">Quantit&eacute;</th>
                                        <th style="background-color:#bed1dd">P.U</th>
                                        <th style="background-color:#bed1dd">Remise</th>
                                        <th style="background-color:#bed1dd">Remarque</th>
                                    </tr></thead>
                                <tbody>
                                    <%
                                        for (int i = 0; i < listFille.length; i++) {
                                    %>
                                    <tr>
                                        <td><input type="hidden" class="form form-control" name="id" id="id" value="<%=listFille[i].getId()%>" ><%=listFille[i].getCodearticle()%></td>
                                        <td><input type="text" class="form form-control" name="quantite" id="quantite" value="<%=listFille[i].getQuantite()%>"></td>
                                        <td><input type="text" class="form form-control" name="pu" id="pu" value="0"></td>
                                        <td><input type="text" class="form form-control" name="remise" id="remise" value="0"></td>
                                        <td>
                                            <textarea name="remarque" class="form form-control" id="remarque" data-parsley-id="6"><%=Utilitaire.champNull(listFille[i].getDesignation())%></textarea>
                                        </td>

                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>




                    </div>
                </div>
            </div>
        </div>

        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/bc/stBonDeCommande-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StBonDeCommande">
        <!--<input name="rajoutLien" type="hidden" id="rajoutLien" value="id-">-->
    </form></div>
    <%} else {%>
<div class="content-wrapper">
    <h1 class="box-title">Bon de commande</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/bc/stBonDeCommandeFille-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StBonDeCommande">
    </form>
    <%}%>
</div>
<script language="JavaScript">

    var options = $('#modepayement option');
    for (var co = 0; co < options.size(); co++) {
        if (options[co].value == 'MP3') {
            $('#modepayement option')[co].selected = true;
        } else {
            $('#modepayement option')[co].selected = false;
        }

    }

</script>