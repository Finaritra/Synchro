<%-- 
    Document   : tef-update-budget
    Created on : 6 juin 2016, 10:32:55
    Author     : Joe
--%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="mg.cnaps.budget.BudgetTefFille"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageUpdate"%>
<%@page import="mg.cnaps.budget.BudgetTef"%>
<%@page import="mg.cnaps.sig.SigEmployeursComptes"%>
<%@page import="mg.cnaps.sig.Employeurs"%>
<%@page import="mg.cnaps.st.StockService"%>
<%@page import="mg.cnaps.st.StTiers"%> 
<%@page import="mg.cnaps.st.StBondecommandeFilleTef"%>
<%@page import="mg.cnaps.st.StBonDeCommande"%>
<%@page import="bean.CGenUtil"%>
<%@page import="user.UserEJB"%>
<%
    UserEJB u;
    BudgetTef carb;

    carb = new BudgetTef();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(carb, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));

    String idtef = request.getParameter("id");

    BudgetTef budget = new BudgetTef();
    String acte = "insert";
    String bute = "tresorerie/OP/op-liste.jsp";
    if (idtef != null && idtef.compareToIgnoreCase("") != 0) {
        budget = (BudgetTef) CGenUtil.rechercher(new BudgetTef(), null, null, " AND ID = '" + idtef + "'")[0];
        acte = "update";
        bute = "stock/bc/tef-fiche.jsp&id=" + idtef;
    }
    String tefId = " AND TEF = '" + idtef + "'";
    BudgetTefFille teffille = new BudgetTefFille();
    BudgetTefFille[] teffilleliste = (BudgetTefFille[]) u.getData(teffille, null, null, null, tefId);

    TypeObjet[] typeTef = null;
    TypeObjet tt = new TypeObjet();
    tt.setNomTable("budget_type_tef");
    typeTef = (TypeObjet[]) CGenUtil.rechercher(tt, null, null, "");

    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>G�n�ration TEF</h1>
    </section>
    <section class="content">
        <form action="<%=session.getValue("lien")%>?but=stock/bc/apresBC.jsp" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">TITRE D'ENGAGEMENT FINANCIER</h3>                
                        </div>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input id="code" name="code" class="form-control" type="text" value="<%=budget.getCode()%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date TEF</label>
                                    <input id="daty" name="daty" class="form-control datepicker" type="text" value="<%=budget.getDaty()%>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Libell�</label>
                                    <input id="libelle" name="libelle" class="form-control" type="text" value="<%=budget.getLibelle()%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Montant</label>
                                    <input id="montant" name="montant" class="form-control" type="text" value="<%=budget.getMontant()%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Montant TVA</label>
                                    <input id="montant_tva" name="montant_tva" class="form-control" type="text" value="<%=budget.getMontant()%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Id B�n�ficiaire</label>
                                    <div class="input-group">                              
                                        <input id="idbeneficiaire" name="idbeneficiaire" class="form-control" type="text" value="<%=budget.getIdbeneficiaire()%>">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="button" onclick="pagePopUp('choix/beneficiaireChoix.jsp?champReturn=idbeneficiaire')">...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nom B&eacute;n&eacute;ficiaire</label>                         
                                    <input id="nombeneficiaire" name="nombeneficiaire" class="form-control" type="text" value="<%=budget.getNombeneficiaire()%>">
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mode de paiement</label>                         
                                    <input id="nombeneficiaire" name="modedepaiement" class="form-control" type="text" value="<%=budget.getModedepaiement()%>">
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Type B&eacute;n&eacute;ficiaire</label>
                                    <select id="typebeneficiaire" class="form-control" name="typebeneficiaire"> 
                                        <option value="fournisseur">Fournisseur</option>
                                        <option value="beneficiaire">B&eacute;n&eacute;ficiaire</option>
                                        <option value="travailleur">Travailleur</option>
                                        <option value="employeur">Employeur</option>
                                        <option value="autre">Autre</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code Agence</label>
                                    <input id="banqueagencecode" name="banqueagencecode" class="form-control" type="text" value="<%=budget.getBanqueagencecode()%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code Banque</label>
                                    <input id="banquecode" name="banquecode" class="form-control" type="text" value="<%=budget.getBanquecode()%>">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Num&eacute;ro de Compte</label>                              
                                    <input id="banquecomptenumero" name="banquecomptenumero" class="form-control" type="text" value="<%=budget.getBanquecomptenumero()%>">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cl&eacute;</label>                                                                   
                                    <input id="banquecomptecle" name="banquecomptecle" class="form-control" type="text" value="<%=budget.getBanquecomptecle()%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date Compte</label>
                                    <input id="banquecomptedate" name="banquecomptedate" class="form-control datepicker" type="text" value="<%=budget.getBanquecomptedate()%>">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Budget D&eacute;pense</label>
                                    <div class="input-group">                              
                                        <input id="budget" name="budget" class="form-control" type="text" value="<%=budget.getBudget()%>">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="button" onclick="pagePopUp('choix/budgetDepenseChoix.jsp?champReturn=budget')">...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Type TEF</label>
                                    <select id="typetef" class="form-control" name="typetef">
                                        <% for (int i = 0; i < typeTef.length; i++) {%>
                                        <option value="<%=typeTef[i].getId()%>">
                                            <%=typeTef[i].getVal()%>
                                        </option>
                                        <% }%>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Frais</label>
                                    <input id="frais" name="frais" class="form-control" type="text" value="0">
                                </div>
                            </div>           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Remarque</label>
                                    <input id="remarque" name="remarque" class="form-control" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" align="right">
                    <button class="btn btn-primary" type="submit">Valider</button>
                </div>
            </div>
            <input name="acte" type="hidden" id="nature" value="update">
            <input name="bute" type="hidden" id="bute" value="<%=bute%>">

            <div class="row">
                <div class="col-md-6">
                    <div class="box-fiche">
                        <div class="box">
                            <div class="box-title with-border">
                                <h1 class="box-title">D�tails</h1>
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Id</th>
                                        <th>Code</th>
                                        <th>D�signation</th>
                                        <th>Quantit�</th>
                                        <th>PU</th>
                                    </tr>
                                    <%for (int i = 0; i < teffilleliste.length; i++) {%>
                                    <tr>
                                        <td><%=teffilleliste[i].getId()%></td>
                                        <td><%=teffilleliste[i].getCode()%></td>
                                        <td><%=teffilleliste[i].getDesignation()%></td>
                                        <td><%=Utilitaire.formaterAr(teffilleliste[i].getQuantite())%></td>
                                        <td align="right"><%=Utilitaire.formaterAr(teffilleliste[i].getPu())%></td>
                                    </tr>
                                    <% }%>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </section>
</div>
