<%-- 
    Document   : as-commande-liste
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<% 
    BonDeCommandeEtat lv = new BonDeCommandeEtat();
    
    String nomTable = "AS_BONDECOMMANDE_LIBELLE";
    lv.setNomTable(nomTable);
	
    String listeCrt[] = {"id","daty", "fournisseur", "designation", "modepaiement", "remarque"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "fournisseur", "designation", "modepaiement", "remarque", "etat"};

    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    affichage.Champ[] liste = new affichage.Champ[1];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("modepaiement");
    liste[0] = new Liste("modepaiement", ou, "VAL", "VAL");
	
    pr.getFormu().changerEnChamp(liste);
    
    pr.setApres("stock/bc/asBonDeCommande-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste des bons de commande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/bc/asBonDeCommande-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/bc/asBonDeCommande-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "Fournisseur", "Designation", "Mode de paiement", "Remarque", "Etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
			  
				out.println(pr.getTableau().getHtml());
				out.println(pr.getBasPage());
		%>
    </section>
</div>