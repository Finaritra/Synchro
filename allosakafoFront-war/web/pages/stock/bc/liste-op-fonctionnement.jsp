<%-- 
    Document   : liste-op-fonctionnement
    Created on : 16 juin 2016, 14:19:47
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.accueil.OPComplet"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageRecherche"%>

<%  OPComplet op = new OPComplet("OPTOUS");
    if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("") != 0) {
        op.setNomTable(request.getParameter("etat"));
    }
    String listeCrt[] = {"id", "op_num", "sous_prestation","beneficiaire", "modedepaiement", "compte", "date_op", "dossier", "typeop", "banque_agence_code"};
    String listeInt[] = {"date_op"};
    String libEntete[] = {"id", "op_num","dossier",  "beneficiaire", "banque_compte_numero","banque_agence_code","montant", "date_op",  "typeop"};
    PageRecherche pr = new PageRecherche(op, request, listeCrt, listeInt, 4, libEntete, 9);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stock/bc/liste-op-fonctionnement.jsp");
    
    affichage.Champ[] liste = new affichage.Champ[2];
    TypeObjet m = new TypeObjet();
    m.setNomTable("PAIE_MODEPAIEMENT");
    liste[0] = new Liste("modedepaiement", m, "desce", "val");
    TypeObjet m1 = new TypeObjet();
    m1.setNomTable("BUDGET_TYPE_TEF");
    liste[1] = new Liste("typeop", m1, "val", "val");
    pr.getFormu().changerEnChamp(liste);
    pr.getFormu().getChamp("date_op1").setLibelle("Date OP min");
    pr.getFormu().getChamp("date_op2").setLibelle("Date OP max");
    pr.getFormu().getChamp("typeop").setLibelle("Type OP");
    pr.getFormu().getChamp("modedepaiement").setLibelle("Mode de paiement");
    pr.getFormu().getChamp("sous_prestation").setLibelle("Sous prestation");
    pr.getFormu().getChamp("op_num").setLibelle("Numero OP");
    pr.getFormu().getChamp("banque_agence_code").setLibelle("Agence b&eacute;n&eacute;ficiaire");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
    OPComplet[] data = (OPComplet[]) pr.getListe();
%>

<script>
    function changerDesignation() {
        document.op.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste OP Fonctionnement</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/bc/liste-op-fonctionnement.jsp" method="post" name="op" id="op">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="etat" class="champ" id="etat" onchange="changerDesignation()" >


                        <% if (request.getParameter("etat") == null || request.getParameter("etat").compareToIgnoreCase("") == 0) {%>    
                        <option value="" selected>Tous </option>
                        <% } else { %>
                        <option value="" >Tous </option>
                        <% } %>


                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("OP_FONCTIONNEMENT_ACLOTURER") == 0) {%>
                        <option value="OP_FONCTIONNEMENT_ACLOTURER" selected>� cloturer </option>
                        <% } else { %>
                        <option value="OP_FONCTIONNEMENT_ACLOTURER">� cloturer </option>
                        <% } %>

                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("OP_FONCTIONNEMENT_CLOTURER") == 0) {%>
                        <option value="OP_FONCTIONNEMENT_CLOTURER" selected>clotur&eacute; </option>
                        <% } else { %>
                        <option value="OP_FONCTIONNEMENT_CLOTURER">clotur&eacute; </option>
                        <% } %>
                        
                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("OP_FONCTIONNEMENT_VISER") == 0) {%>
                        <option value="OP_FONCTIONNEMENT_VISER" selected>vis&eacute; </option>
                        <% } else { %>
                        <option value="OP_FONCTIONNEMENT_VISER">vis&eacute; </option>
                        <% } %>

                        
                    </select>
                </div>
                <div class="col-md-4"></div>
            </div><br/><br/>


        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/bc/op-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
             String libEnteteAffiche[] = {"Id", "Numero OP", "Libelle" ,  "Beneficiaire", "Num Compte Beneficiaire","Agence Beneficiaire","Montant", "Date OP",  "Type OP"};
             pr.getTableau().setLibelleAffiche(libEnteteAffiche);
             out.println(pr.getTableau().getHtml());
             out.println(pr.getBasPage());

        %>
        
    </section>
</div>