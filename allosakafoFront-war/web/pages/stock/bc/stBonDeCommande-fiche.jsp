<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%
    StBonDeCommande dma;
    double total = 0.0;
%>
<%
    dma = new StBonDeCommande();
    dma.setNomTable("ST_BONDCOMMANDE_LIBELLE");
    PageConsulte pc = new PageConsulte(dma, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.getChampByName("code_dr").setVisible(false);
    pc.getChampByName("iduser").setVisible(false);
    pc.getChampByName("daty").setLibelle("Date");
    pc.getChampByName("datelivraison").setVisible(false);
    pc.getChampByName("modepayement").setLibelle("Mode de paiement");
    pc.setTitre("Fiche Bon de commande");
    
    StBonDeCommande bondecommande = (StBonDeCommande)pc.getBase();
    
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?butstock/bc/stBonDeCommande-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/bc/stBonDeCommande-apercu.jsp&id=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Aper&ccedil;u</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/bc/stBonDeCommandeFille-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/bc/stBonDeCommande-mvt-saisie_1.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px" onclick="verifLivraisonBC(<%=bondecommande.getEtat()%>)">Reception</a>
                            <%  
                                if(bondecommande.getNumtef()!= null && bondecommande.getNumtef().compareTo("") != 0){
                                    String op = StockService.findOPdetails(bondecommande.getNumtef());
                            %>
                                <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=budget/tef/tef-fiche.jsp&id=" + bondecommande.getNumtef()%>" style="margin-right: 10px" >Voir TEF</a>
                                <% if(op != null && op.compareToIgnoreCase("") != 0){ %>
                                <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/bc/op-fiche.jsp&id=" + op%>" style="margin-right: 10px" >Voir OP</a>
                                <% }%>
                            <% }else { %>
                                 <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/bc/tef-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px" >Editer TEF</a>
                            <% } %>
                           
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/bc/stEtatBonCommande-liste.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Etat de livraison</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/bc/stBonDeCommande-liste.jsp&classe=mg.cnaps.st.StBonDeCommande" style="margin-right: 10px">Supprimer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">D&eacute;tails bon de commande</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Article</th>
                                    <th>Quantit&eacute;</th>
                                    <th>P.U</th>
                                    <th>Remise</th>
                                    <th>Montant</th>
                                    <th>Remarque</th>
                                </tr>
                            </thead>

                            <tbody>
                                <%
                                    StBonDeCommandeFilleLibelle p = new StBonDeCommandeFilleLibelle();
                                    p.setNomTable("St_BonDeCommandeFille_Libelle"); // vue
                                    StBonDeCommandeFilleLibelle[] liste = (StBonDeCommandeFilleLibelle[]) CGenUtil.rechercher(p, null, null, " and idbc = '" + request.getParameter("id") + "'");

                                    for (int i = 0; i < liste.length; i++) {
                                        double remise = ((liste[i].getQuantite() * liste[i].getPu()) * liste[i].getRemise()) / 100;
                                %>
                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                    <td  align="center"><%=liste[i].getArticle()%> </td>
                                    <td  align="center"><%=(int) liste[i].getQuantite()%></td>
                                    <td  align="right"><%=Utilitaire.formaterAr(liste[i].getPu())%></td>
                                    <td  align="right"><%=Utilitaire.formaterAr(remise)%></td>
                                    <td  align="right"><%=Utilitaire.formaterAr((liste[i].getQuantite() * liste[i].getPu()) - remise)%></td>
                                    <td  align="center"><%=liste[i].getRemarque()%></td>
                                </tr>
                                <%
                                    total += ((liste[i].getQuantite() * liste[i].getPu()) - remise);
                                    }
                                    double tva = (total * bondecommande.getTva()) / 100;
                                %>
                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                    <td  align="center"></td>
                                    <td  align="center"></td>
                                    <td  align="right"></td>
                                    <td  align="right">TVA</td>
                                    <td  align="right"><%=Utilitaire.formaterAr(tva)%></td>
                                    <td  align="center"></td>
                                </tr>
                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                    <td  align="center"></td>
                                    <td  align="center"></td>
                                    <td  align="right"></td>
                                    <td  align="right">Total</td>
                                    <td  align="right"><%=Utilitaire.formaterAr(tva + total)%></td>
                                    <td  align="center"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
