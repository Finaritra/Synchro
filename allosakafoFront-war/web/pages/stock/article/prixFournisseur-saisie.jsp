<%-- 
    Document   : absence-saisie
    Created on : 29 oct. 2015, 15:23:06
    Author     : user
--%>
<%@page import="mg.cnaps.st.StPrixArticle"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageInsert"%>
<%@page import="mg.cnaps.pret.PretDemande"%>
<%
    try{
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StPrixArticle a = new StPrixArticle();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    
    pi.getFormu().getChamp("article").setLibelle("Article");
    pi.getFormu().getChamp("fournisseur").setLibelle("Fournisseur");
    pi.getFormu().getChamp("pu").setLibelle("Prix unitaire");
    pi.getFormu().getChamp("datyeffective").setLibelle("Date effective");
    pi.getFormu().getChamp("datyeffective").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pi.getFormu().getChamp("dureedelivraison").setLibelle("Dur�e de livraison");
    pi.getFormu().getChamp("remarque").setLibelle("Observation");
    pi.getFormu().getChamp("remarque").setType("textarea");
    
    pi.getFormu().getChamp("article").setPageAppel("choix/stArticleChoix.jsp");
    pi.getFormu().getChamp("fournisseur").setPageAppel("choix/stTiersChoix.jsp");
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 align="center">prix article fournisseur</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="prixFournisseur" id="prixFournisseur" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/article/prixFournisseur-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StPrixArticle">
    </form>
</div>
<%
} catch (Exception e) {
    e.printStackTrace();
%>
<script language="JavaScript"> alert('<%=e.getMessage()%>');
    history.back();</script>

<% } %>