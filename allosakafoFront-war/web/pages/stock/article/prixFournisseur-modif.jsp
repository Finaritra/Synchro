<%@page import="mg.cnaps.st.StPrixArticle"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    UserEJB u;
    StPrixArticle art;
%>
<%
    art = new StPrixArticle();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(art, request, (user.UserEJB) session.getValue("u"));
	
    pi.setLien((String) session.getValue("lien"));

    pi.getFormu().getChamp("article").setLibelle("Article");
    pi.getFormu().getChamp("fournisseur").setLibelle("Fournisseur");
    pi.getFormu().getChamp("pu").setLibelle("PU");
    pi.getFormu().getChamp("datyeffective").setLibelle("Date effective");
    pi.getFormu().getChamp("dureedelivraison").setLibelle("Dur&eacute;e de livraison");
    pi.getFormu().getChamp("remarque").setLibelle("Remarque");
	
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification prix article fournisseurs</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="stprixarticle">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/article/stprixarticle-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StPrixArticle">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
