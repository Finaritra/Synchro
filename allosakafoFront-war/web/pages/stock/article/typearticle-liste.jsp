<%-- 
    Document   : typearticle-liste
    Created on : 25 janv. 2016, 11:43:44
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.st.StTypeArticle"%>
<%@page import="affichage.PageRecherche"%>

<% StTypeArticle lv = new StTypeArticle();

    lv.setNomTable("ST_TYPE_ARTICLE_LIBELLE");
    String listeCrt[] = {"id", "val", "desce", "compta_compte", "imputable"};
    String listeInt[] = null;
    String libEntete[] = {"id", "val", "desce", "compta_compte", "imputable", "analytique"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("val").setLibelle("Valeur");
    pr.getFormu().getChamp("desce").setLibelle("Description");
    pr.getFormu().getChamp("compta_compte").setLibelle("Compta compte");
    
    pr.getFormu().getChamp("val").setLibelle("Libelle");
    pr.getFormu().getChamp("desce").setLibelle("Observation");
    pr.getFormu().getChamp("compta_compte").setLibelle("Compte comptable");
    
    pr.setApres("stock/article/typearticle-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Chapitre</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/article/typearticle-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/article/typearticle-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Libelle", "Observation", "Compte comptable", "Imputable", "Analytique"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>