
<%@page import="mg.cnaps.log.LogService"%>
<%@page import="bean.Champ"%>
<%@page import="affichage.Liste"%>
<%@page import="mg.cnaps.st.StRapportDemande"%>
<%@page import="mg.cnaps.st.StRapportDemandeAppro"%>
<%@page import="mg.cnaps.st.StockService"%>
<%@page import="bean.TypeObjet"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageRecherche"%>

<%
    StRapportDemandeAppro[] st = null;
    StRapportDemande tpo = new StRapportDemande();
    tpo.setNomTable("ST_RAPPORT_DEMANDEAPPRO");
    String listeCrt[] = {"mois", "annee", "service"};
    String listeInt[] = null;
    String libEntete[] = {"mois", "annee", "val", "desce", "service"};
    PageRecherche pr = new PageRecherche(tpo, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));

    affichage.Champ[] listee = new affichage.Champ[2];

    String affiche[] = {"Tous", "Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "D�cembre"};
    String value[] = {"%", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    Liste lst = new Liste();
    lst.ajouterValeur(value, affiche);

    listee[0] = lst;
    listee[0].setNom("mois");

    LogService s = new LogService();
    s.setNomTable("log_service");
    listee[1] = new Liste("service", s, "code_service", "libelle");

    pr.getFormu().changerEnChamp(listee);
    pr.getFormu().getChamp("mois").setLibelle("Mois");
    pr.setApres("stock/appro/rapportdemandeappro-liste.jsp");
    String[] colSomme = null;

    pr.creerObjetPage(libEntete, colSomme);

    StRapportDemande[] liste = (StRapportDemande[]) pr.getRs().getResultat();
    st = StockService.rapportDemandeAppro(liste);
%>

<div class="content-wrapper">
    <section class="content-header">
        <h1 id="titre-export">Rapport d'activit&eacute;</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/appro/rapportdemandeappro-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>

        <br>
        <div id="table-container">
            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                <thead>
                <th style="background-color:#bed1dd">Service</th>
                <th style="background-color:#bed1dd">Mois</th>
                <th style="background-color:#bed1dd">Ann&eacute;e</th>
                <th style="background-color:#bed1dd">Re&ccedil;u</th>
                <th style="background-color:#bed1dd">En attente</th>
                <th style="background-color:#bed1dd">Valid&eacute;e</th>
                <th style="background-color:#bed1dd">Livr&eacute;e</th>
                </thead>
                <tbody>
                    <%
                        if (st != null) {
                            for (int i = 0; i < st.length; i++) {
                    %>
                    <tr>
                        <td><%=st[i].getService()%></td>
                        <td><%=st[i].getMois()%></td>
                        <td><%=st[i].getAnnee()%></td>
                        <td><%=Utilitaire.formaterAr(st[i].getRecu())%></td>
                        <td><%=Utilitaire.formaterAr(st[i].getEnattente())%></td>
                        <td><%=Utilitaire.formaterAr(st[i].getValide())%></td>
                        <td><%=Utilitaire.formaterAr(st[i].getLivree())%></td>
                    </tr>
                    <%}
                    }%>
                </tbody>		
            </table>
        </div>
        <div class="box box-primary box-solid">

            <div class="box-header with-border">
                <h3 class="box-title">Exporter</h3>
                <div class="box-tools pull-right">
                    <button   class="btn btn-box-tool" data-widget="collapse" >
                        <i class="fa fa-minus"></i>
                    </button> 
                </div>
            </div>

            <div id="export-body" class="box-body" style="display: block;">
                <div class="row" style="text-align: center">
                    <label>exporter excel :</label>
                    <button onclick="exporterCsv()">
                        <img src="../dist/img/file_xls.png" alt="csv-export">	
                    </button>
                </div>
            </div>
        </div>
        <form id="form-export" action="../download" method="post">
            <input id="excel-input"  type="hidden" name="table" value="">
            <input type="hidden" name="ext"  value="xls"  checked="checked">                    
            <input type="hidden" name="donnee" value="0" checked="checked">
            <input type="hidden" value="Exporter" class="btn btn-default">
        </form>
    </section>
</div>
<script>

    function chargerExport()
    {
        var titre = "<h1>" + $('#titre-export').html() + "</h1>";
        var excel = titre + $('#table-container').html();


        var excelInput = document.getElementById("excel-input");
        excelInput.value = excel;
        console.log("valeur de l'excel" + excelInput.value);
    }

    function exporterCsv()
    {
        chargerExport()
        var form = $("#form-export");
        form.submit();
    }

</script>