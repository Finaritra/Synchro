<%-- 
    Document   : demmandeapprofille-saisie
    Created on : 11 sept. 2015, 10:37:17
    Author     : user
--%>
<%@page import="mg.cnaps.st.StQuantitedemandeLivree"%>
<%@page import="mg.cnaps.budget.BudgetProjet"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="java.sql.Connection"%>
<%@page import="mg.cnaps.st.StDemandeApproFilleLibelle"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    String idmere = request.getParameter("id");
    UserEJB u = (UserEJB) session.getAttribute("u");
    Connection c = null;
    try {
        c = (new utilitaire.UtilDB()).GetConn();
        StDemandeApproFilleLibelle crt = new StDemandeApproFilleLibelle();
        StQuantitedemandeLivree crtLivre = new StQuantitedemandeLivree();

        StDemandeApproFilleLibelle[] listFille = (StDemandeApproFilleLibelle[]) CGenUtil.rechercher(crt, null, null, " and demande = '" + idmere + "' order by article desc");
        StQuantitedemandeLivree[] listFilleLivre = (StQuantitedemandeLivree[]) CGenUtil.rechercher(crtLivre, null, null, " and iddemande = '" + idmere + "' order by code desc");

        String lien = (String) session.getValue("lien");

        StMagasin mg = new StMagasin();
        mg.setNomTable("ST_MAGASIN_LIBELLE");
        StMagasin[] lv = (StMagasin[]) CGenUtil.rechercher(mg, null, null, c, "");

        TypeObjet classe = new TypeObjet();
        classe.setNomTable("st_classe");
        TypeObjet[] cls = (TypeObjet[]) CGenUtil.rechercher(classe, null, null, c, "");

        TypeObjet tbn = new TypeObjet();
        tbn.setNomTable("st_type_besoin");
        TypeObjet[] tb = (TypeObjet[]) CGenUtil.rechercher(tbn, null, null, c, "");

        BudgetProjet prj = new BudgetProjet();
        prj.setNomTable("budget_projet");
        BudgetProjet[] lprj = (BudgetProjet[]) CGenUtil.rechercher(prj, null, null, c, "");
%>
<div class="content-wrapper">

    <h1 align="center">Enregistrer mouvement stock </h1>
    <form action="<%=lien%>?but=stock/mvtStock/apresMvtStock.jsp" method="post" name="approfille" id="approfille" data-parsley-validate>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>                
            </div>
            <div class="box-body">
                <div class="row" style="margin-left: 25%;">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Excercice:</label>
                            <input type="text" name="excercice" id="excercice" class="form-control" value="<%=Utilitaire.getAnneeEnCours()%>">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Magasin:</label>
                            <select name="magasin" class="form-control" id="magasin" tabindex="1" data-parsley-id="4">
                                <%
                                    for (int i = 0; i < lv.length; i++) {
                                %>
                                <option value="<%=lv[i].getId()%>"><%=lv[i].getDesce()%></option>
                                <%
                                    }
                                %>
                            </select>                                
                        </div>    
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">

                            <label>Type: </label>
                            <select name="typebesoin" class="form-control" id="typebesoin" tabindex="9" data-parsley-id="4">
                                <%
                                    for (int i = 0; i < tb.length; i++) {
                                %>
                                <option value="<%=tb[i].getId()%>"><%=tb[i].getVal()%></option>
                                <%
                                    }
                                %>
                            </select> 
                        </div>
                    </div>

                    
                </div>
                <div class="row col-md-12" style="margin-top: 5%;">
                    <table class="table table-bordered" width="80%">
                        <thead>
                            <tr>
                                <th style="background-color:#bed1dd">Id article</th>
                                <th style="background-color:#bed1dd">Libell�</th>
                                <th style="background-color:#bed1dd">Quantite demand&eacute;</th>
                                <th style="background-color:#bed1dd">Reste &agrave; livr&eacute;</th>
                                <th style="background-color:#bed1dd">Remarque</th>
                            </tr></thead>
                        <tbody>

                            <%
                                boolean livreeTotal = false;
                                int narticleLivree = 0;
                                int tailleAlivree = listFille.length;
                                for (int i = 0; i < listFille.length; i++) {

                                    StDemandeApproFilleLibelle f = listFille[i];
                            %>
                            <tr>
                                <%
                                    StQuantitedemandeLivree stqf = null;
                                    StQuantitedemandeLivree stqftmp = null;
                                    int tailleL = listFilleLivre.length;
                                    String id = "";
                                    for (int a = 0; a < tailleL; a++) {
                                        stqftmp = listFilleLivre[a];
                                        //System.out.println(stqftmp.getArticle() + " " + f.getArticle());
                                        boolean corresp = stqftmp.getArticle().equals(f.getArticle());
                                        if (corresp) {
                                            stqf = stqftmp;
                                            //System.out.println("correspond id = " + f.getArticle());

                                        }
                                    }

                                    boolean existLivr = stqf != null;
                                    double reste = f.getQuantite();
                                    if (existLivr) {
                                        double qlivre = stqf.getQuantite();
                                        double qalivre = listFille[i].getQuantite();
                                        reste = qalivre - qlivre;
                                    }
                                    boolean livree = reste <= 0 && stqf != null;
                                    if (livree) {
                                        narticleLivree++;
                                        livreeTotal = narticleLivree == tailleAlivree;

                                %>
                                <td><%=listFille[i].getIdArticle()%> </td>
                                <td><%=listFille[i].getArticle()%> </td>
                                <td> Livree</td>
                                <td><%=listFille[i].getRemarque()%></td>

                                <%
                                } else {
                                %>
                                <td><input type="text" class="form-control" name="id" id="id" value="<%=listFille[i].getIdArticle()%>" readonly="readonly"></td>
                                <td><%=listFille[i].getArticle()%> </td>
                                <td align="rigth"><%=Utilitaire.doubleWithoutExponential(listFille[i].getQuantite())%></td>
                                <td><input type="text"  name="quantite" class="form-control"  id="quantite" value="<%=Utilitaire.doubleWithoutExponential(reste)%>"></td>
                                <td><input type="text" name="remarque" class="form-control" id="remarque" value="<%=listFille[i].getRemarque()%>"></td>
                                    <%
                                        }
                                    %>
                            </tr>
                            <%
                                }
                            %>
                        </tbody>
                    </table>
                    <input name="idmere" type="hidden" id="idmere" value="<%=idmere%>">
                    <input name="acte" type="hidden" id="nature" value="insertmvt">
                    <input name="bute" type="hidden" id="bute" value="stock/appro/demandeappro-liste.jsp">
                    <hr/>
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right" type="submit">Valider</button>
                    </div>                                    
                </div>
            </div>
        </div>
    </form>
</div>
<%
        if (livreeTotal) {
            %>
            <script language="JavaScript">alert('Tout a d�ja �t� livr�e');history.back();</script>
            <%
        }

    } catch (Exception ex) {
        ex.printStackTrace();
        throw ex;
    } finally {
        if (c != null) {
            c.close();
        }
    }
%>