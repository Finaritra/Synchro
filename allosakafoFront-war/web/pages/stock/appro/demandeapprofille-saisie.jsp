<%-- 
    Document   : stDemandeFournitureFille-saisie
    Created on : 30 oct. 2015, 16:06:55
    Author     : user
--%>
<%@page import="mg.cnaps.st.StDemandeAppro"%>
<%@page import="mg.cnaps.st.StDemandeApproFille"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StDemandeApproFille da = new StDemandeApproFille();
    PageInsert pi = new PageInsert(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));

    pi.getFormu().getChamp("demande").setDefaut(request.getParameter("id"));
    pi.getFormu().getChamp("demande").setAutre("readOnly");
    pi.getFormu().getChamp("quantite").setLibelle("Quantit�");
    pi.getFormu().getChamp("remarque").setType("textarea");
    pi.getFormu().getChamp("remarque").setLibelle("Observation");

    pi.getFormu().getChamp("article").setLibelle("Article");

    pi.getFormu().getChamp("article").setPageAppel("choix/stArticleChoix.jsp");

    StDemandeAppro demandeFourniture = new StDemandeAppro();
    StDemandeAppro[] listeDemandeFourniture = (StDemandeAppro[]) CGenUtil.rechercher(demandeFourniture, null, null, " AND ID = '" + request.getParameter("id").trim() + "'");

    if (listeDemandeFourniture.length > 0) {
        pi.getFormu().getChamp("article").setApresLienPageappel(listeDemandeFourniture[0].getTypee());
    }

    pi.preparerDataFormu();

    String lienfinaliser = "but=apresTarif.jsp&acte=finaliser&bute=stock/appro/demandeappro-liste.jsp&classe=mg.cnaps.st.StDemandeAppro&id=" + request.getParameter("id");
%>
<div class="content-wrapper">
    <h1 align="center">D�tails de la demande</h1>
    <form action="<%=pi.getLien()%>?but=stock/mvtStock/apresMvtStock.jsp" method="post" name="demandeappro" id="demandeappro" autocomplete="off" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlAddTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/appro/demandeapprofille-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StDemandeApproFille">
        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-">
        <input name="iddemande" type="hidden" id="rajoutLien" value="<%=request.getParameter("id")%>">
    </form>
    <%
        StDemandeApproFille p = new StDemandeApproFille();
        p.setNomTable("ST_DEMANDEAPPROFILLE_LIB_INFO"); // vue
        StDemandeApproFille[] liste = (StDemandeApproFille[]) CGenUtil.rechercher(p, null, null, " and demande = '" + request.getParameter("id").trim() + "'");
    %>
    <div id="selectnonee">
        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
            <thead>
                <tr class="head">
                    <th style="background-color:#bed1dd">Article</th>
                    <th style="background-color:#bed1dd">Quantite</th>
                    <th style="background-color:#bed1dd">Remarque</th>
                    <th style="background-color:#bed1dd">Action</th>
                </tr>
            </thead>
            <tbody>
                <%
                    for (int i = 0; i < liste.length; i++) {
                %>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"><%=liste[i].getArticle()%> </td>
                    <td width="20%" align="center"><%=liste[i].getQuantite()%></td>
                    <td width="20%" align="center"><%=liste[i].getRemarque()%></td>
                    <td width="20%" align="center">
                        
                    <a href="<%=(String) session.getValue("lien") + "?but=stock/appro/demandeapprofille-modif.jsp&id=" + liste[i].getId()%>" style="margin-right: 10px">Modifier</a>|<a href="<%=(String) session.getValue("lien") + "?but=stock/mvtStock/apresMvtStock.jsp&id=" + liste[i].getTuppleID() + "&iddemande=" + request.getParameter("id")%>&acte=deleteFille&bute=stock/appro/demandeapprofille-saisie.jsp&classe=mg.cnaps.st.StDemandeApproFille&rajoutLien=id" style="margin-right: 10px">Supprimer</a>

                        
                    </td>
                </tr>
                <%
                    }
                %>
            </tbody>
        </table>
        <center><a href="<%=pi.getLien()%>?<%=lienfinaliser%>" class="btn btn-success pull-center" tabindex="81">Finaliser</a></center>
    </div>
</div>
