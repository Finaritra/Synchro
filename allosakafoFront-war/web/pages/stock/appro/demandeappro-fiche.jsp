<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%
    StDemandeAppro dma;
%>
<%
    dma = new StDemandeAppro();
    dma.setNomTable("ST_DEMANDE_APPRO_INFO");
    //String[] libelleInventaireFiche = {"Id", "Service", "Magasin", "Direction", "Localisation", "Date", "Designation","Priorite","Etat","Projet","Classe","Id User"};
    PageConsulte pc = new PageConsulte(dma, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    //pc.setLibAffichage(libelleInventaireFiche);
    
    pc.getChampByName("iduser").setVisible(false);
    pc.getChampByName("designation").setLibelle("Observation");
    pc.getChampByName("daty").setLibelle("Date");
    pc.getChampByName("classee").setVisible(false);
    pc.getChampByName("code_dr").setVisible(false);
    
    StDemandeAppro dapr = (StDemandeAppro) pc.getBase();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?butstock/appro/demandeappro-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a> Fiche de demande d'approvisionnement</h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/appro/demandeapprofille-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <% if (dapr.getEtat() == ConstanteEtat.getEtatValider()) {%>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/appro/demandeappro-mvt-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Livrer</a>
                            <% }%>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=valider&bute=stock/appro/demandeappro-fiche.jsp&classe=mg.cnaps.st.StDemandeAppro" style="margin-right: 10px">Valider</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/appro/demandeappro-liste.jsp&classe=mg.cnaps.st.StDemandeAppro" style="margin-right: 10px">Supprimer</a>
                            <a href="${pageContext.request.contextPath}/EtatStockServlet?action=ficheDemandeAppro&id=<%=request.getParameter("id")%>" class="btn btn-default pull-right" id="export"  style="margin-right: 10px">Imprimer PDF</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">D�tails demande d'approvisionnement</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-hover" width="80%">
                            <thead>
                                <tr>
                                    <th style="background-color:#bed1dd">Id article</th>
                                    <th style="background-color:#bed1dd">Article</th>
                                    <th style="background-color:#bed1dd">Quantite</th>
                                    <th style="background-color:#bed1dd">Remarque</th>
                                </tr></thead>
                            <tbody>
                                <%
                                    StDemandeApproFilleLibelle crt = new StDemandeApproFilleLibelle();
                                    StDemandeApproFilleLibelle[] listFille = (StDemandeApproFilleLibelle[]) CGenUtil.rechercher(crt, null, null, " and demande = '" + request.getParameter("id") + "'");
                                    for (int i = 0; i < listFille.length; i++) {%>
                                <tr>
                                    <td><%=listFille[i].getIdArticle()%></td>
                                    <td><%=listFille[i].getArticle()%></td>
                                    <td><%=listFille[i].getQuantite()%></td>
                                    <td><%=listFille[i].getRemarque()%></td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
