<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%@ page import="affichage.*" %>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

<%!
StDemandeAppro da;
%>
<%
da=new StDemandeAppro();
String listeCrt[]={"id", "magasin", "service", "direction", "code_dr", "daty", "designation", "etat"};
String listeInt[]={"daty"};
String[] pourcentage={"nombrepargroupe"};
String colDefaut[]={"id", "magasin", "service", "direction", "code_dr", "daty", "designation", "etat"};
String somDefaut[]={};
PageRechercheGroupe pr=new PageRechercheGroupe(da,request,listeCrt,listeInt,3,colDefaut,somDefaut,pourcentage,8,0);

pr.setUtilisateur((user.UserEJB)session.getValue("u"));
pr.setLien((String)session.getValue("lien"));
pr.setApres("stock/appro/demandeappro-analyse.jsp");
pr.creerObjetPagePourc();
%>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Analyse</h1>
	</section>
	<section class="content">
		<form action="<%=pr.getLien()%>?but=stock/appro/demandeappro-analyse.jsp" method="post" name="analyse" id="analyse">
			<%out.println(pr.getFormu().getHtmlEnsemble());%>
		</form>
		<%
		String lienTableau[]={pr.getLien()+"?but=stock/appro/demandeappro-fiche.jsp&id="+pr.getFormu().getListeCritereString()};
		String colonneLien[]={"id"};
		pr.getTableau().setLien(lienTableau);
		pr.getTableau().setColonneLien(colonneLien);
		out.println(pr.getTableauRecap().getHtml());%>
		<br>
		<%
		out.println(pr.getTableau().getHtml());
		out.println(pr.getBasPage());
		%>
	</section>
</div>