<%@page import="affichage.Liste"%>
<%@page import="mg.cnaps.st.StDemandeApproListe"%>
<%@page import="affichage.PageRecherche"%>
<% StDemandeApproListe lv = new StDemandeApproListe();

    lv.setNomTable("ST_DEMANDEAPPRO_LISTE");
    
    String listeCrt[] = {"id", "service", "magasin", "daty", "designation","etat"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "service", "magasin", "daty", "designation","etat"};
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date Max");
   //  pr.getFormu().getChamp("code_dr").setLibelle("Direction Regionale");

    //pr.getFormu().getChamp("code_dr").setLibelle("Direction Regionale");
    
    String[] affiche2 = {"TOUS","ENCOURS","VALIDE"};
    String[] value2 = {"%","ENCOURS","VALIDE"};
    affichage.Champ[] liste = new affichage.Champ[1];
    
    Liste lst2 = new Liste();
    lst2.ajouterValeur(value2, affiche2);
    liste[0] = lst2;
    liste[0].setNom("etat");
    
    pr.getFormu().changerEnChamp(liste);
    
    pr.setApres("stock/appro/demandeappro-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste de demande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/appro/demandeappro-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/appro/demandeappro-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"ID", "Service", "Magasin", "Date", "Observation","Etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
