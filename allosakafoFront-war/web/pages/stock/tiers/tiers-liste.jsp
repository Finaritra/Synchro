<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="affichage.PageRecherche"%>
<% StTiers lv = new StTiers();

    lv.setNomTable("ST_TIERS_INFO");
    String listeCrt[] = {"id", "raisonsocial", "matrcnaps", "contact", "compte_tiers", "compteg", "assujettis"};
    String listeInt[] = null;
    String libEntete[] = {"id", "raisonsocial", "matrcnaps", "contact", "compte_tiers","compteg","assujettis"};
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("raisonsocial").setLibelle("Raison social");
    pr.getFormu().getChamp("compte_tiers").setLibelle("Compte tiers");
    pr.getFormu().getChamp("compteg").setLibelle("Compte general");
    pr.setApres("stock/tiers/tiers-liste.jsp");
    String[] colSomme = null;
    
    affichage.Champ[] liste = new affichage.Champ[1];
    String[] affiche = {"tous","oui","non"};
    String[] value = {"%","oui","non"};
    Liste lst = new Liste();
    lst.ajouterValeur(value, affiche);
    liste[0] = lst;
    liste[0].setNom("assujettis");
    pr.getFormu().changerEnChamp(liste);
    
    pr.creerObjetPage(libEntete, colSomme);%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Tiers</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/tiers/tiers-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/tiers/tiers-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "Raison Social", "Matricule Employeurs", "Contact", "Compte tiers", "Compte generale", "Assujettis"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
