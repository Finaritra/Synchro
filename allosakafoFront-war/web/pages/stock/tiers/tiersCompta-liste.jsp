<%@page import="mg.cnaps.st.StTiersCompta"%>
<%@page import="affichage.PageRecherche"%>
<% StTiersCompta lv = new StTiersCompta();

    //lv.setNomTable("log_vehicule_info");
    String listeCrt[] = {"id", "compteG", "numerotiers", "idtiers"};
    String listeInt[] = null;
    String libEntete[] =  {"id", "compteG", "numerotiers", "idtiers"};
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stock/tiers/tiersCompta-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Tiers</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/tiers/tiersCompta-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/tiers/tiersCompta-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] =  {"id", "compteG", "numerotiers", "Tiers"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
