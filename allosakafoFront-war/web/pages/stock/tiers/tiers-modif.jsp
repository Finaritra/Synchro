<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="lc.Direction"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    UserEJB u;
    StTiers vtiers;
    vtiers = new StTiers();
    vtiers.setNomTable("ST_TIERS");
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(vtiers, request, (user.UserEJB) session.getValue("u"));

    pi.setLien((String) session.getValue("lien"));

    affichage.Champ[] liste = new affichage.Champ[2];
    TypeObjet typeT = new TypeObjet();
    typeT.setNomTable("st_type_tiers");
    liste[0] = new Liste("typetiers", typeT, "VAL", "id");
    
    String[] affiche = {"oui","non"};
    String[] value = {"oui","non"};
    Liste lst = new Liste();
    lst.ajouterValeur(value, affiche);
    liste[1] = lst;
    liste[1].setNom("assujettis");
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("raisonsocial").setLibelle("Raison social");
    pi.getFormu().getChamp("nif").setLibelle("NIF");
    pi.getFormu().getChamp("stat").setLibelle("Stat");
    pi.getFormu().getChamp("adresse").setLibelle("Adresse");
    pi.getFormu().getChamp("codeposte").setLibelle("Code poste");
    pi.getFormu().getChamp("taxe").setLibelle("Taxe");
    pi.getFormu().getChamp("rubrique").setLibelle("Rubrique");
    pi.getFormu().getChamp("contact").setLibelle("Contact");
    pi.getFormu().getChamp("matrcnaps").setLibelle("Matricule cnaps");
    
    pi.getFormu().getChamp("typetiers").setLibelle("Type tiers");
    pi.getFormu().getChamp("compteg").setLibelle("Compte G");
    pi.getFormu().getChamp("compteg").setPageAppel("choix/compteAuxChoix.jsp");
    //pi.getFormu().getChamp("matrcnaps").setPageAppel("choix/employeurChoixMultiple.jsp", "matrcnaps;raisonsocial;nif;stat;adresse");
    pi.getFormu().getChamp("compte_tiers").setLibelle("Compte tiers");
        //pi.getFormu().getChamp("numerotiers").setLibelle("Numero tiers");

    pi.getFormu().getChamp("remarque").setPageAppel("choix/employeurChoixMultipletrs.jsp","remarque;matrcnaps;raisonsocial;nif;stat;adresse");
    pi.getFormu().getChamp("remarque").setLibelle("Employeur");
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Mise &agrave; jour Tiers</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="stvuetiers">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/tiers/tiers-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StTiers">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
