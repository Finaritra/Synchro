<%-- 
    Document   : as-commande-liste
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<% 
    BonDeLivraisonEtat lv = new BonDeLivraisonEtat();
    
    String listeCrt[] = {"id","daty", "remarque"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "remarque", "etat"};

    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    pr.setApres("stock/bl/asBonDeLivraison-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste des bons de livraison</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/bl/asBonDeLivraison-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/bl/bondelivraison-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "Remarque", "Etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
			  
				out.println(pr.getTableau().getHtml());
				out.println(pr.getBasPage());
		%>
    </section>
</div>