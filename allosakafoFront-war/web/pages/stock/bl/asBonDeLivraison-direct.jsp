<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="mg.allosakafo.produits.Produits"%>
<%@page import="mg.allosakafo.produits.Ingredients"%>



<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    BonDeLivraison cmd = new BonDeLivraison();
	String idbc = request.getParameter("idbc");
	
    PageInsert pi = new PageInsert(cmd, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("remarque").setLibelle("remarque");
    pi.getFormu().getChamp("idbc").setLibelle("N� Bon de commande");
	pi.getFormu().getChamp("idbc").setDefaut(idbc);
	pi.getFormu().getChamp("etat").setVisible(false);
	pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 class="box-title">Enregistrer bon de livraison</h1>
    <form action="<%=pi.getLien()%>?but=stock/bl/apresBL.jsp" method="post" name="bondelivraison" id="bondecommande" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertCommande();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <div class="box">
                        <div class="box-title with-border">
                            <h1 class="box-title">D&eacute;tails</h1>
                        </div>
                        <div class="box-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th style="background-color:#bed1dd">ID</th>
                                        <th style="background-color:#bed1dd">Libelle</th>
                                        <th style="background-color:#bed1dd">Unite</th>
                                        <th style="background-color:#bed1dd">Quantite par pack</th>
                                        <th style="background-color:#bed1dd">Quantite</th>
                                    </tr></thead>
                                <tbody>
                                    <%
                                        BonDeCommandeFille p = new BonDeCommandeFille();
										p.setNomTable("as_bondecommande_ingredients");
                                        BonDeCommandeFille[] listef = (BonDeCommandeFille[]) CGenUtil.rechercher(p, null, null, " and idbc='"+idbc+"'");
                                         for (int i = 0; i < listef.length; i++) {
                                    %>
                                <input type="hidden" class="form form-control" name="nb" id="nb" value="<%=listef.length%>">
                                <tr>
                                    <td><input type="hidden" class="form form-control" name="id" id="id<%=i%>" value="<%=listef[i].getId()%>"><%=listef[i].getId()%></td>
                                    <td><%=listef[i].getProduit()%></td>
                                    <td>G</td>
                                    <td><input type="text" class="form form-control" name="quantiteparpack" id="quantiteparpack<%=i%>" value="<%=listef[i].getQuantiteparpack()%>"></td>
                                    <td><input type="text" class="form form-control" name="quantite" id="quantite<%=i%>" value="<%=listef[i].getQuantite()%>"></td>
                                </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                
        <input name="acte" type="hidden" id="nature" value="insertBLDirect">
        <input name="bute" type="hidden" id="bute" value="stock/bl/bondelivraison-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.stock.BonDeLivraison">
        <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;">Enregistrer</button>
        <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;">R&eacute;initialiser</button>
    </form>
</div>