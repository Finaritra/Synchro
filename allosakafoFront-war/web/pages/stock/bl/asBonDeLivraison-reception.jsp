<%-- 
    Document   : fiche-commande-chef
    Created on : 29 d�c. 2016, 05:35:45
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.stock.*"%>
<style>

.wrapper2 {
   width: 100%; height: 100% ;  border: 1px solid black;
   -webkit-transition: all .3s ease-out;
   -moz-transition: all .3s ease-out;
   -o-transition: all .3s ease-out;
   transition: all .3s ease-out;
}
.zoom:hover {
   -moz-transform: scale(2);
   -webkit-transform: scale(2);
   -o-transform: scale(2);
   transform: scale(2);
   -ms-transform: scale(2);
filter: progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand',
   M11=2, M12=-0, M21=0, M22=2);
   }

.emargement{
	margin-top:2px;
	width:95%;
}

.table-acheteur td{
	font-size: small;
}
</style>
<%
    BonDeLivraison dma = new BonDeLivraison();
    double total = 0.0;
    PageConsulte pc = new PageConsulte(dma, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    
    pc.setTitre("Fiche bon de livraison");
    
    BonDeLivraison bondelivraison = (BonDeLivraison)pc.getBase();
	
	BonDeLivraisonFille p = new BonDeLivraisonFille();
    p.setNomTable("as_bondelivraison_fille_lib"); // vue
    BonDeLivraisonFille[] liste = (BonDeLivraisonFille[]) CGenUtil.rechercher(p, null, null, " and numbl = '" + request.getParameter("id") + "'");
	
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Allo Sakafo 1.0</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini" background-color="#3c8dbc">
		<div class="wrapper2">
			<div class="row">
                <div class="col-md-4">
                    <div class="box-fiche" style="margin-left:0px; min-width:425px; height:100%">
						<div class="box">
                            <div class="box-title with-border">
								<table  width="95%" align="center">
									<tr>
										<td><h3>Fiche bon de livraison</h3></td>
										<td align="right"  colspan=2><h4 class="box-title" align="right">Ref livraison : <%=bondelivraison.getId()%></h4></td>
									</tr>
									<tr>
										<td><p>Date : <%=Utilitaire.formatterDaty(bondelivraison.getDaty())%></p></td>
										<td></td>
									</tr>
								</table>
                                
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered table-acheteur" width="95%" align="center" border="1px solid black" style="border-collapse:collapse">
                                    <thead>
                                        <tr>
                                            <th><strong>Produit</strong></th>
                                            <th><strong>Quantit&eacute;</strong></th>
											<th><strong>Quantit&eacute; par pack</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            for (int i = 0; i < liste.length; i++) {
                                        %>
                                        <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                            <td align="left"><%=Utilitaire.champNull(liste[i].getProduit())%></td>
                                            <td  align="right"><%=liste[i].getQuantiteparpack()%></td>
											<td  align="right"><%=Utilitaire.formaterAr(liste[i].getQuantite())%></td>
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
								
								<table class="emargement"  align="center">
									<tr>
										<td align="left"><p>Le livreur</p></td>
										<td align="right"><p>Le responsable</p></td>
									</tr>
								</table>
								<br>
								<br>
                            </div>
						</div>
					</div>
				</div>
			</div>
        </div>
        <jsp:include page='../../elements/js.jsp'/>
    </body>
</html>