<%-- 
    Document   : bondelivraison-liste
    Created on : 10 mars 2016, 09:10:46
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.st.StBonDeLivraison"%>
<%@page import="affichage.PageRecherche"%>
<% 
    
    StBonDeLivraison lv = new StBonDeLivraison();
    lv.setNomTable("STBONDELIVRAISON");
	if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("") != 0) {
        lv.setNomTable(request.getParameter("etat"));
    }
    String listeCrt[] = {"id", "daty", "idbc",  "numbl", "remarque"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "idbc",  "numbl", "remarque"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date Max");
    pr.setApres("stock/bl/bondelivraison-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste bon de livraison</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/bl/bondelivraison-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>
			<div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="etat" class="champ" id="etat" onchange="changerDesignation()" >
                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("STBONDELIVRAISON") == 0) {%>    
                        <option value="STBONDELIVRAISON" selected>Tous</option>
                        <% } else { %>
                        <option value="STBONDELIVRAISON" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_BONDLIVRAISON_VALIDER") == 0) {%>
                        <option value="ST_BONDLIVRAISON_VALIDER" selected>Valid�</option>
                        <% } else { %>
                        <option value="ST_BONDLIVRAISON_VALIDER">Valid�</option>
                        <% } %>

                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_BONDLIVRAISON_EN_COURS") == 0) {%>
                        <option value="ST_BONDLIVRAISON_EN_COURS" selected>En cours</option>
                        <% } else { %>
                        <option value="ST_BONDLIVRAISON_EN_COURS">En cours</option>
                        <% } %>

                    </select>
                </div>
                <div class="col-md-4"></div>
            </div>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/bl/bondelivraison-fiche.jsp", pr.getLien() + "?but=stock/bc/stBonDeCommande-fiche.jsp"};
            String colonneLien[] = {"id","idbc"};
            String[] urlLienMultiple = {"id","idbc"};
            String[] urlLienAffiche = {"id","id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);           
            pr.getTableau().setUrlLienAffiche(urlLienAffiche);
            pr.getTableau().setUrlLien(urlLienMultiple);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "N� BC",  "N� BL", "Remarque"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>

