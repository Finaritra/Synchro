<%-- 
    Document   : operationnel-fiche
    Created on : 8 d�c. 2015, 11:17:31
    Author     : Jetta
--%>


<%@page import="mg.cnaps.rapport.RapOperationnelInfo"%>
<%@page import="affichage.PageConsulte"%>

<%
    RapOperationnelInfo virement;
%>
<%
    virement = new RapOperationnelInfo();
    virement.setNomTable("RAP_OPRATIONNEL_STOCK");
    String[] libelleVirementFiche = {"id", "Activite", "Date", "heure", "objet","nom","prenom","matricule","service","direction"};
    PageConsulte pc = new PageConsulte(virement, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setLibAffichage(libelleVirementFiche);
    pc.setTitre("Fiche operationnel");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/rapport/operationnel/operationnel-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/rapport/operationnel/operationnel-liste.jsp&classe=mg.cnaps.rapport.RapOperationnel" style="margin-right: 10px">Supprimer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%=pc.getBasPage()%>
</div>
