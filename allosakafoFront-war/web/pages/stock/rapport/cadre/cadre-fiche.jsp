<%-- 
    Document   : cadre-fiche
    Created on : 8 d�c. 2015, 10:04:52
    Author     : Jetta
--%>

<%@page import="affichage.PageConsulte"%>
<%@page import="mg.cnaps.rapport.RapCadreInfo"%>
<%
    RapCadreInfo virement;
%>
<%
    virement = new RapCadreInfo();
    //virement.setNomTable("RECETTE_VIREMENT_LIBELLE");
    String[] libelleVirementFiche = {"id", "nom", "prenom", "matricule", "Date", "Activite", "Projet", "libelle", "Volume heure", "Fonction", "Service", "Direction"};
    PageConsulte pc = new PageConsulte(virement, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setLibAffichage(libelleVirementFiche);
    pc.setTitre("Fiche rapport d'activite");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/rapport/cadre/cadre-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/rapport/cadre/cadre-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/rapport/cadre/cadre-liste.jsp&classe=mg.cnaps.rapport.RapCadre" style="margin-right: 10px">Supprimer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%=pc.getBasPage()%>
</div>
