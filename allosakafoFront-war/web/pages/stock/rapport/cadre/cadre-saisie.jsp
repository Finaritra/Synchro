<%-- 
    Document   : cadre-saisie
    Created on : 7 d�c. 2015, 17:54:18
    Author     : user
--%>
<%@page import="mg.cnaps.rapport.RapCadre"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageInsert"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    RapCadre a = new RapCadre();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
 
    affichage.Champ[] liste=new affichage.Champ[1];
    TypeObjet c= new TypeObjet();
    c.setNomTable("RAP_ACTIVITE");
    liste[0]=new Liste("idactivite",c,"VAL","id");
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("idactivite").setLibelle("Activite");
    pi.getFormu().getChamp("idprojet").setLibelle("Projet");
    pi.getFormu().getChamp("libelle").setLibelle("Libelle");
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("volume_heure").setLibelle("Volume heure");
    pi.getFormu().getChamp("etat").setVisible(false);
    
    pi.getFormu().getChamp("idprojet").setPageAppel("choix/rapProjetChoix.jsp");
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 align="center">cadre</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="raport" id="raport" autocomplete="off" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="paie/rapport/raport-liste.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.rapport.RapCadre">
    </form>
</div>
