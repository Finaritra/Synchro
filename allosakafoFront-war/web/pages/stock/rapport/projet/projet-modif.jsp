<%@page import="affichage.PageUpdate"%>
<%@page import="mg.cnaps.rapport.RapProjet"%>
<%@page import="user.UserEJB"%>
<%
    UserEJB u;
    RapProjet art;
%>
<%
    art = new RapProjet();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(art, request, (user.UserEJB) session.getValue("u"));

    pi.setLien((String) session.getValue("lien"));
    pi.getFormu().getChamp("description_sommaire").setLibelle("Description sommaire");
    pi.getFormu().getChamp("date_debut").setLibelle("Date de debut");
    pi.getFormu().getChamp("date_fin").setLibelle("Date de fin");
    pi.getFormu().getChamp("idbudgetprojet").setLibelle("Budget projet");
    pi.getFormu().getChamp("volume_horaire").setLibelle("Volume horaire");
    pi.getFormu().getChamp("realisation_jour").setLibelle("Nombre de jour de realisation");
    pi.getFormu().getChamp("idbudgetprojet").setPageAppel("choix/budgetProjetChoix.jsp");

    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification d'un projet</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="starticle" id="rapprojet">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/rapport/projet/projet-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.rapport.RapProjet">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
