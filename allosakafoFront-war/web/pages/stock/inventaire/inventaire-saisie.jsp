<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>

<%
    StInventaire pj = new StInventaire();
    PageInsert pi = new PageInsert(pj, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("magasin").setPageAppel("choix/magasinChoix.jsp");
    pi.getFormu().getChamp("designation").setType("textarea");
    pi.getFormu().getChamp("designation").setLibelle("Observation");
    pi.getFormu().getChamp("remarque").setType("textarea");
    
    affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet o = new TypeObjet();
    o.setNomTable("st_type_inventaire");
    liste[0] = new Liste("typeinventaire", o, "VAL", "id");
    
    pi.getFormu().changerEnChamp(liste);
    pi.getFormu().getChamp("typeinventaire").setLibelle("Type inventaire");
    pi.getFormu().getChamp("etat").setVisible(false);

    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 align="center">Inventaire</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="inventaire" id="inventaire">


        <%
        //pi.getFormu().makeHtmlInsertTabIndex();
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/inventaire/inventaire_fille_saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StInventaire">

    </form>
</div>
