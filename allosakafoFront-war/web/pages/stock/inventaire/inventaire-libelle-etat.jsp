<%-- 
    Document   : inventaire-libelle-etat
    Created on : 9 f�vr. 2016, 09:27:47
    Author     : Joe

--%>

<%@page import="utilitaire.Utilitaire"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.cnaps.st.StInventaireLibelleEtat"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
    try {
        StInventaireLibelleEtat lv = new StInventaireLibelleEtat();
        lv.setNomTable("ST_INVENTAIRE_LIBELLE_ETAT");
        String listeCrt[] = {"chapitre", "magasin"};
        String listeInt[] = null;
        String libEntete[] = {"Code", "Article", "Reste", "PUMP"};

        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, 4);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        pr.setApres("stock/inventaire/inventaire-libelle-etat.jsp");

        affichage.Champ[] liste = new affichage.Champ[2];

        TypeObjet o = new TypeObjet();
        o.setNomTable("st_type_article");
        liste[0] = new Liste("chapitre", o, "VAL", "VAL");

        StMagasin mg = new StMagasin();
        mg.setNomTable("ST_MAGASIN_LIBELLE");
        liste[1] = new Liste("magasin", mg, "DESCE", "DESCE");

        pr.getFormu().changerEnChamp(liste);

        String[] colSomme = null;
        pr.creerObjetPage(libEntete);

        StInventaireLibelleEtat[] listeInv = (StInventaireLibelleEtat[]) pr.getRs().getResultat();
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Etat de Stock</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/inventaire/inventaire-libelle-etat.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%
            out.println(pr.getTableauRecap().getHtml());
        %>
        </br>
        <div class="row">
            <div class="row col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" align="center">LISTE</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <form name="mep" action="<%=pr.getLien()%>?but=pub/apresListe.jsp" method="post">
                                <input type="submit" value="valider">
                                <input type="hidden" name="acte" value="corriger">

                                <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                                    <thead>
                                        <tr class="head">
                                            <th align="left"></th>
                                            <th align="left">ID</th>
                                            <th align="left">Code Article</th>
                                            <th align="left">Intitul&eacute;</th>
                                            <th align="right">Reste</th>
                                            <th align="right">PUMP</th>
                                            <th align="right">Reste</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            for (int i = 0; i < listeInv.length; i++) {
                                        %>
                                        <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                            <td align="left"><%=(i + 1)%></td>
                                            <td align="left"><%=listeInv[i].getId()%></td>
                                            <td align="left"><%=listeInv[i].getCode()%></td>
                                            <td align="left"><%=listeInv[i].getArticle()%></td>
                                            <td align="right"><%=Utilitaire.formaterAr(listeInv[i].getReste())%></td>
                                            <td align="right"><%=Utilitaire.formaterAr(listeInv[i].getPump())%></td>
                                            <td align="right"><input type="text" name="reste" value="<%=listeInv[i].getReste()%>"></td>
                                            <input type="text" name="id" value="<%=listeInv[i].getId()%>">
                                            <input type="text" name="magasin" value="<%=listeInv[i].getMagasin()%>">
                                        </tr>
                                    </tbody>
                                    <input type="hidden" name="crt" value="<%=pr.getFormu().getListeCritereString()%>&numPage=<%=pr.getNumPage()%>">
                                    <%}%>
                                </table>
                                <input type="submit" value="valider">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%    } catch (Exception ex) {
        ex.printStackTrace();
    }

%>
