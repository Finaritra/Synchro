

<%@page import="mg.cnaps.st.StInventaireFille"%>
<%@page import="affichage.PageRecherche"%>
<% StInventaireFille lv = new StInventaireFille();

    lv.setNomTable("ST_INVENTAIRE_FILLE_LIBELLE");
    String listeCrt[] = {"id", "quantite", "inventaire", "article"};
    String listeInt[] = null;
    String libEntete[] = {"id", "quantite", "inventaire", "article"};
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stock/inventaire/inventaireFille-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste inventaire Fille</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/inventaire/inventaireFille-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/inventaire/inventaireFille-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "quantite", "inventaire", "article"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
