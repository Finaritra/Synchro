<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%
    StMagasin magasin;
%>
<%
    magasin = new StMagasin();
    magasin.setNomTable("ST_MAGASIN_LIBELLE");
    String[] libelleMagasinFiche = {"Id", "Code", "Description", "Code Direction Regionale","Type Magasin"};
    PageConsulte pc = new PageConsulte(magasin, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setLibAffichage(libelleMagasinFiche);
    pc.setTitre("Fiche Magasin");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/magasin/magasin-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                    <div class="box-footer">
                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/magasin/magasin-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                        <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/magasin/magasin-liste.jsp&classe=mg.cnaps.st.StMagasin" style="margin-right: 10px">Supprimer</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>
