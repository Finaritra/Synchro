<%-- 
    Document   : magasin-saisie
    Created on : 8 sept. 2015, 13:58:45
    Author     : user
--%>
<%@page import="mg.cnaps.commun.Dr"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StMagasin  a = new StMagasin();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));

    affichage.Champ[] liste = new affichage.Champ[2];
    Dr dr = new Dr();
    dr.setNomTable("sig_dr");
    liste[0] = new Liste("code_dr", dr, "val", "id");
    
    TypeObjet tpm = new TypeObjet();
    tpm.setNomTable("st_type_magasin");
    liste[1] = new Liste("typemagasin", tpm, "val", "id");
    
    pi.getFormu().changerEnChamp(liste);
    
    pi.setLien((String) session.getValue("lien"));
    pi.getFormu().getChamp("code").setLibelle("Code");
    pi.getFormu().getChamp("desce").setLibelle("Libelle");
    pi.getFormu().getChamp("code_dr").setLibelle("Direction regionale");
    pi.getFormu().getChamp("typemagasin").setLibelle("Type Magasin");
    
    
    pi.preparerDataFormu(); 
%>
<div class="content-wrapper">
    <h1>magasin</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="magasin" id="magasin" data-parsley-validate>
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="stock/magasin/magasin-saisie.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StMagasin">
    <input name="nomtable" type="hidden" id="nomtable" value="ST_MAGASIN">
    </form>
</div>
