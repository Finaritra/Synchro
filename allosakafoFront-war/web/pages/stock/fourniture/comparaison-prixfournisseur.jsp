<%-- 
    Document   : comparaison-prixfournisseur.jsp
    Created on : 4 nov. 2015, 14:45:34
    Author     : Joe
--%>


<%@page import="mg.cnaps.st.StPrixArticleFournisseur"%>
<%@page import="affichage.PageRecherche"%>

<% 
    StPrixArticleFournisseur lv = new StPrixArticleFournisseur();
    // lv.setNomTable("POINT_POINTAGE_LIBELLE");
    String listeCrt[] = {"codearticle", "idfournisseur", "dureedelivraison"};
    String listeInt[] = {"dureedelivraison"};
    String libEntete[] = {"article", "fournisseur", "designation", "pu", "dureedelivraison"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    pr.getFormu().getChamp("codearticle").setLibelle("Article");
    pr.getFormu().getChamp("idfournisseur").setLibelle("Fournisseur");
    pr.getFormu().getChamp("dureedelivraison1").setLibelle("Duree de livraison min");
    pr.getFormu().getChamp("dureedelivraison2").setLibelle("Duree de livraison max");
    
    pr.getFormu().getChamp("codearticle").setPageAppel("choix/listeArticleChoix.jsp");
    pr.getFormu().getChamp("idfournisseur").setPageAppel("choix/stTiersChoix.jsp");
    
    pr.setApres("stock/fourniture/comparaison-prixfournisseur.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Comparaison des prix</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/fourniture/comparaison-prixfournisseur.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/fourniture/demandefourniture-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Article", "Fournisseur", "Designation", "P.U", "Duree moyenne de livraison"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtmlWithCheckbox());
            out.println(pr.getBasPage());

        %>
    </section>
</div>

