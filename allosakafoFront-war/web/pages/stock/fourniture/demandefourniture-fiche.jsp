<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%

    UserEJB u;
    u = (UserEJB) session.getAttribute("u");
    StDemandeFourniture demandefourniture = new StDemandeFourniture();
    demandefourniture.setNomTable("ST_DEMANDE_FOURNITURE_LIBELLE");
    String[] libelleDemandeFournitureFiche = {"Id", "Date", "Magasin", "Direction", "Chapitre", "Description", "Etat", "Projet", "Ref user"};
    PageConsulte pc = new PageConsulte(demandefourniture, request, (user.UserEJB) session.getValue("u"));
    pc.setLibAffichage(libelleDemandeFournitureFiche);
    pc.setTitre("Fiche Demande d'achat");
    StDemandeFounitureFille demandefourniturefille = new StDemandeFounitureFille();
    demandefourniturefille.setNomTable("ST_DEMFOURNITURE_FILLE_LIBELLE");
    String demandefourniturefilleId = " AND IDDEMANDE = '" + request.getParameter("id") + "'";
    StDemandeFounitureFille[] demandefourniturefilles = (StDemandeFounitureFille[]) u.getData(demandefourniturefille, null, null, null, demandefourniturefilleId);
    StDemandeFourniture dmf = (StDemandeFourniture) pc.getBase();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/fourniture/demandefourniture-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <% if (dmf.getEtat() < ConstanteEtat.getEtatValider()) {%>
                            <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/fourniture/demandefourniturefille-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <% }%>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/fourniture/demandefourniture-liste.jsp&classe=mg.cnaps.st.StDemandeFourniture" style="margin-right: 10px">Supprimer</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=valider&bute=stock/fourniture/demandefourniture-fiche.jsp&classe=mg.cnaps.st.StDemandeFourniture" style="margin-right: 10px">Valider</a>

                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/bc/stBonDeCommande-appro.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Editer BC</a>
                            <a href="${pageContext.request.contextPath}/EtatStockServlet?action=ficheDemandeAchat&id=<%=request.getParameter("id")%>" class="btn btn-default pull-right" id="export" style="margin-right: 10px">Imprimer PDF</a>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">Liste fournitures demand&eacute;es</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Id</th>
                                <th>Article</th>
                                <th>Quantit&eacute;</th>
                                <th>D&eacute;signation</th>
                            </tr>
                            <%
                                for (int i = 0; i < demandefourniturefilles.length; i++) {%>
                            <tr>
                                <td><%=demandefourniturefilles[i].getId()%></td>
                                <td><%=demandefourniturefilles[i].getCodearticle()%></td>
                                <td><%=demandefourniturefilles[i].getQuantite()%></td>
                                <td><%=demandefourniturefilles[i].getDesignation()%></td>
                            </tr>
                            <% }%>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
