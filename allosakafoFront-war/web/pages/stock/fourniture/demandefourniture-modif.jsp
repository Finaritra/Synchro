<%@page import="mg.cnaps.st.StDemandeFourniture"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    UserEJB u;
    StDemandeFourniture a;
%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    a = new StDemandeFourniture();
    PageUpdate pi = new PageUpdate(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("magasin").setLibelle("Magasin");
    pi.getFormu().getChamp("code_dr").setLibelle("Code DR");
    pi.getFormu().getChamp("fournisseur").setLibelle("Projet");
    pi.getFormu().getChamp("chapitre").setLibelle("Chapitre");
    pi.getFormu().getChamp("description").setLibelle("Observation");
    pi.getFormu().getChamp("code_dr").setPageAppel("choix/drChoix.jsp");
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification d'une demande d'achat</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="stdemandefourniture">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/fourniture/demandefourniture-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StDemandeFourniture">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>