
<%@page import="mg.cnaps.commun.Dr"%>
<%@page import="mg.cnaps.log.LogService"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="bean.TypeObjet"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.cnaps.st.StockService"%>
<%@page import="bean.CGenUtil"%>
<%@page import="mg.cnaps.st.StPropCommande"%>
<%@page import="mg.cnaps.budget.BudgetProjet"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="java.sql.Connection"%>
<%@page import="mg.cnaps.st.StDemandeApproFilleLibelle"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    StPropCommande medc = new StPropCommande();
    //StMagasin magasin=new StMagasin();
    //StMagasin[] listeMagasin=(StMagasin[])CGenutil.rechercher(magasin,null,null,"");

    
    try {
        StMagasin mg = new StMagasin();
        if (request.getParameter("value") != null && request.getParameter("value").compareToIgnoreCase("cms") == 0) {
            mg.setNomTable("ST_MAGASINMEDICO_LIBELLE");
            if (request.getParameter("magasin") != null) {
                medc.setMagasin(request.getParameter("magasin"));
            } else {
                medc.setMagasin("MAG008");
            }
        } else {
            mg.setNomTable("ST_MAGASIN_LIBELLE");
            if (request.getParameter("magasin") != null) {
                medc.setMagasin(request.getParameter("magasin"));
            }
        }
        StMagasin[] lv = (StMagasin[]) CGenUtil.rechercher(mg, null, null, "");
        medc.setNomTable("PROP_COMMANDE_LIBELLE");
        StPropCommande[] listeMedCons = (StPropCommande[]) CGenUtil.rechercher(medc, null, null, "");
%>


<script type="text/javascript">
    function confirmAppts(object, val) {
        var input = object;
        if (input.type === "checkbox") {
            if (input.checked === true){
                input.value = val;
            }
            else{
                input.value = "";
            }
        }
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <div>

            <div class="col-md-12">
                <h1>Proposition de commande :  </h1>

            </div>
            <form action="<%=session.getAttribute("lien")%>?but=stock/proposition/propositionDemande-saisie.jsp" method="post">
                <select name="magasin" class="champ form-control" id="magasin" tabindex="1" data-parsley-id="4">
                    <%
                        for (int i = 0; i < lv.length; i++) {
                    %>
                    <option value="<%=lv[i].getId()%>"><%=lv[i].getDesce()%></option>
                    <%
                        }
                    %>
                </select>

                <%if (request.getParameter("value") != null && request.getParameter("value").compareToIgnoreCase("cms") == 0) {
                %>
                <input type="hidden" name="value" value="cms"/>
                <%
                    }
                %>

                <button  type="submit" class="btn btn-primary" id="btnListe">Rechercher</button>
            </form>


        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <form name="elem" action="<%=session.getValue("lien")%>?but=stock/proposition/apresProposition.jsp" method="post">

                        <input type="hidden" name="bute" id="bute" value="stock/fourniture/demandefourniture-fiche.jsp"/>
                        <input type="hidden" name="idcontrole" id="idcontrole" value=""/>
                        <input type="hidden" name="mode" id="libelle" value="rec"/>
                        <input name="acte" type="hidden" id="nature" value="insert">
                        <input name="nb" type="hidden" id="nb" value="<%=listeMedCons.length%>">

                        <!-- <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StDemandeAppro">-->

                        <div class="box-body">
                            <table class="table table-bordered" id="tablePiece">
                                <tr>
                                    <th></th>
                                    <th>Article</th>
                                    <th>Stock disponible</th>
                                    <th>Co�t Moyen par Mois</th>
                                    <th>Quantit&eacute; propos&eacute;e</th> 
                                </tr>
                                <tbody>
                                    <% 
                                    System.out.println(medc.getNomTable());
                                    for (int i = 0; i < listeMedCons.length; i++) {
                                    
                                    System.out.println(listeMedCons[i].getIdarticle());
                                    %>
                                    <tr>
                                        <td><input type="checkbox" name="id<%=i%>" id="id<%=i%>" onclick="confirmAppts(this, '<%=listeMedCons[i].getIdarticle()%>')"/></td>
                                        <td><label for="libelle_<%=i%>" tabindex="1"><%= listeMedCons[i].getLibelle()%></label></td>
                                        <td><label for="libelle_<%=i%>" tabindex="2"><%= Utilitaire.formaterAr(listeMedCons[i].getReste())%> </label></td>
                                        <td><label for="libelle_<%=i%>" tabindex="2"><%= Utilitaire.formaterAr(listeMedCons[i].getConsomme())%> </label></td>
                                        <td><input value="<%= StockService.getQuantitePropose(listeMedCons[i].getConsomme())%>" class="form-control" type="text" name="nbproposition<%=i%>" id="libelle1" placeholder="Recommandation"/></td>
                                        <input type="hidden" name="libelle_<%=i%>" value="<%= listeMedCons[i].getLibelle()%>">
                                    </tr>
                                <%}%>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <button name="valider" class="btn btn-primary pull-right" type="submit">Valider</button>
                            </div>

                        </div>
                    </form>

                </div> 
            </div>
        </div>
    </section>
</div>
<% } catch (Exception ex) {
        ex.printStackTrace();
        throw new Exception(ex.getMessage());
    }
%>