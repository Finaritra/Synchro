<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%!
    StAnnulationMere annulationMere;
%>
<%
    annulationMere = new StAnnulationMere();
    String[] libelleAnnulationMereFiche = {"Id", "Date", "Id Stock"};
    PageConsulte pc = new PageConsulte(annulationMere, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setLibAffichage(libelleAnnulationMereFiche);
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?butstock/mvtStock/annulationMere-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a> Fiche Annulation M&egrave;re</h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                    <div class="box-footer">
                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/mvtStock/annulationMere-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                        <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/mvtStock/annulationMere-liste.jsp&classe=mg.cnaps.st.StAnnulationMere" style="margin-right: 10px">Supprimer</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
