<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.st.StIntraMvtStock"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>
<%
StIntraMvtStock p = new StIntraMvtStock();
p.setNomTable("St_Intra_Mvt_Stock");

String listeCrt[]={"daty","magasinDep","magasinArr", "designation"};
String listeInt[]={"daty"};
String libEntete[]={"id","daty","magasinDep","magasinArr", "designation"};
PageRecherche pr=new PageRecherche(p,request,listeCrt,listeInt,3,libEntete,5);
pr.setUtilisateur((user.UserEJB)session.getValue("u"));
pr.setLien((String)session.getValue("lien"));
pr.setApres("stock/mvtStock/intraMvtStock-liste.jsp");
pr.getFormu().getChamp("daty1").setLibelle("Du");
pr.getFormu().getChamp("daty2").setLibelle("Au");
pr.getFormu().getChamp("magasinDep").setLibelle("Magasin depart");
pr.getFormu().getChamp("magasinArr").setLibelle("Magasin arrivee");
pr.getFormu().getChamp("designation").setLibelle("Designation");
affichage.Champ[] liste = new affichage.Champ[2];
    StMagasin dep = new StMagasin();
    dep.setNomTable("St_Magasin");
    liste[0] = new Liste("magasinDep", dep, "code", "id");
    
    StMagasin arr = new StMagasin();
    arr.setNomTable("St_Magasin");
    liste[1] = new Liste("magasinArr", arr, "code", "id");
    
    pr.getFormu().changerEnChamp(liste);
String[]colSomme=null;
pr.creerObjetPage(libEntete,colSomme);
%>
<div class="content-wrapper">
	<section class="content-header">
	<h1>Liste mouvement inter magasin</h1>
	</section>
	<section class="content">
		<form action="<%=pr.getLien()%>?but=stock/mvtStock/intraMvtStock-liste.jsp" method="post" name="personneliste" id="personneliste">
			<% out.println(pr.getFormu().getHtmlEnsemble());%>
		</form>
		<%
		String lienTableau[]={pr.getLien()+"?but=stock/mvtStock/intraMvtStock-fiche.jsp"};
		String colonneLien[]={"id"};
		String libelles[]={"Id","Date","Magasin depart","Magasin arrivee", "Designation"};
		pr.getTableau().setLien(lienTableau);
		pr.getTableau().setColonneLien(colonneLien);
		pr.getTableau().setLibelleAffiche(libelles);
		out.println(pr.getTableauRecap().getHtml());
		
                out.println(pr.getTableau().getHtml());
                out.println(pr.getBasPage()); %>
	</section>
</div>
