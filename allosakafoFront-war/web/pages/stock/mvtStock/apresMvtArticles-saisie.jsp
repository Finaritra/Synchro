<%@page import="mg.cnaps.st.StMvtStockFilleLib"%>
<%@page import="mg.cnaps.st.StMvtStockFille"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.st.StIntraMvtStock"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String idMvt = request.getParameter("id");
    String autreparsley = "data-parsley-range='[8, 40]' required";
    UserEJB u = (user.UserEJB) session.getValue("u");

//    private String id, idmvtstock,article;
//    private double entree,sortie,solde,pu,taxe,etat;
    StMvtStockFille pj = new StMvtStockFille();
    pj.setNomTable("St_Mvt_Stock_Fille");
    PageInsert pi = new PageInsert(pj, request, u);

    pi.setLien((String) session.getValue("lien"));

    pi.getFormu().getChamp("idmvtstock").setLibelle("ID Mere");
    pi.getFormu().getChamp("idmvtstock").setDefaut(idMvt);
    pi.getFormu().getChamp("idmvtstock").setAutre("readonly='readonly'");
    pi.getFormu().getChamp("article").setLibelle("Article");
    pi.getFormu().getChamp("article").setPageAppel("choix/listeArticleChoix.jsp");
    if (request.getParameter("chapitre") != null && request.getParameter("chapitre").compareToIgnoreCase("") != 0) {
        pi.getFormu().getChamp("article").setApresLienPageappel("&chapitre=" + request.getParameter("chapitre"));
    }

    pi.getFormu().getChamp("idmvt").setVisible(false);

    if (request.getParameter("action").toString().compareToIgnoreCase("entree") == 0) {
        pi.getFormu().getChamp("entree").setLibelle("Quantit&eacute; entr&eacute;e");
        pi.getFormu().getChamp("sortie").setVisible(false);
        pi.getFormu().getChamp("solde").setVisible(false);
        pi.getFormu().getChamp("pu").setLibelle("P.U");
        pi.getFormu().getChamp("taxe").setVisible(false);
    } else if (request.getParameter("action").compareToIgnoreCase("sortie") == 0) {
        pi.getFormu().getChamp("entree").setVisible(false);
        pi.getFormu().getChamp("sortie").setLibelle("Quantit&eacute; sortie");
        pi.getFormu().getChamp("solde").setVisible(false);
        pi.getFormu().getChamp("pu").setVisible(false);
        pi.getFormu().getChamp("taxe").setVisible(false);
    } else {
        pi.getFormu().getChamp("entree").setLibelle("Entree");
        pi.getFormu().getChamp("sortie").setVisible(false);
        pi.getFormu().getChamp("solde").setVisible(false);
        pi.getFormu().getChamp("pu").setVisible(false);
        pi.getFormu().getChamp("taxe").setVisible(false);
    }

    pi.getFormu().getChamp("etat").setVisible(false);

    pi.preparerDataFormu();    
    
    String lienfinaliser = "but=apresTarif.jsp&acte=finaliser&bute=stock/mvtStock/mvtStock-details.jsp&classe=mg.cnaps.st.StMvtStock";
%>
<div class="content-wrapper">
    <h1>Redressement/Régularisation</h1>
    <form action="<%=pi.getLien()%>?but=stock/mvtStock/apresSaisieMvtStockFille.jsp" method="post" name="AtDsp" id="AtDsp" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/mvtStock/apresMvtArticles-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StMvtStockFille">
        <input name="nomtable" type="hidden" id="nomtable" value="St_Mvt_Stock_Fille">
        <input name="idstock" type="hidden" value="<%=request.getParameter("id")%>">
        <input name="action" type="hidden" value="<%=request.getParameter("action")%>">
        <input name="chapitre" type="hidden" value="<%=request.getParameter("chapitre")%>">
        <input name="rajoutLien" type="hidden" value="id">
    </form>
    <%
        StMvtStockFilleLib p = new StMvtStockFilleLib();
        p.setNomTable("St_Mvt_Stock_Fille_lib"); // vue
        StMvtStockFilleLib[] liste = (StMvtStockFilleLib[]) CGenUtil.rechercher(p, null, null, " and idmvtstock = '" + idMvt + "'");
    %>
    <div id="selectnonee">
        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
            <thead>
                <tr class="head">
                    <th width="20%" align="center" valign="top" style="background-color:#bed1dd">Article</th>
                    <th width="20%" align="center" valign="top" style="background-color:#bed1dd">Entree</th>
                    <th width="20%" align="center" valign="top" style="background-color:#bed1dd">Sortie</th>
                    <th width="20%" align="center" valign="top" style="background-color:#bed1dd">P.U</th>
                    <th width="20%" align="center" valign="top" style="background-color:#bed1dd"></th>
                </tr>
            </thead>
            <tbody>
                <%
                    for (int i = 0; i < liste.length; i++) {
                %>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"><%=liste[i].getArticle()%> </td>
                    <td width="20%" align="center"><%=liste[i].getEntree()%></td>
                    <td width="20%" align="center"><%=liste[i].getSortie()%></td>
                    <td width="20%" align="center"><%=liste[i].getPu()%></td>
                    <td width="20%" align="center"><a href="<%=(String) session.getValue("lien") + "?but=stock/mvtStock/apresSaisieMvtStockFille.jsp&id=" + liste[i].getId() + "&action=" + request.getParameter("action") + "&chapitre=" + request.getParameter("chapitre") +"&idstock=" + request.getParameter("id")%>&acte=annuler&bute=stock/mvtStock/apresMvtArticles-saisie.jsp&classe=mg.cnaps.st.StMvtStockFille&rajoutLien=id" style="margin-right: 10px">annuler</a></td>
                </tr>
                <%
                    }
                %>
            </tbody>
        </table>
        <center><a href="<%=pi.getLien()%>?<%=lienfinaliser%>" class="btn btn-success pull-center" tabindex="81">Finaliser</a></center>
    </div>
</div>