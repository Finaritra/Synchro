<%@page import="mg.cnaps.st.StMvtStockFilleLib"%>
<%@page import="mg.cnaps.st.StMvtStockFille"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.st.StIntraMvtStock"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String entree = request.getParameter("ent");
    String sortie = request.getParameter("srt");
    String autreparsley = "data-parsley-range='[8, 40]' required";
    UserEJB u = (user.UserEJB) session.getValue("u");

//    private String id, idmvtstock,article;
//    private double entree,sortie,solde,pu,taxe,etat;
    StMvtStockFille pj = new StMvtStockFille();
    pj.setNomTable("St_Mvt_Stock_Fille");
    PageInsert pi = new PageInsert(pj, request, u);

    pi.setLien((String) session.getValue("lien"));

    pi.getFormu().getChamp("idmvtstock").setVisible(false);
    pi.getFormu().getChamp("article").setLibelle("Article");
    pi.getFormu().getChamp("article").setPageAppel("choix/listeArticleChoix.jsp");
    pi.getFormu().getChamp("entree").setLibelle("Quantite");
    pi.getFormu().getChamp("sortie").setVisible(false);
    pi.getFormu().getChamp("solde").setVisible(false);
    pi.getFormu().getChamp("taxe").setVisible(false);
    pi.getFormu().getChamp("pu").setVisible(false);
    pi.getFormu().getChamp("etat").setVisible(false);

    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Mouvement Articles</h1>
    <form action="<%=pi.getLien()%>?but=apresMvtIntra.jsp" method="post" name="AtDsp" id="AtDsp" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/mvtStock/apresMvtIntra-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StMvtStockFille">
        <input name="nomtable" type="hidden" id="nomtable" value="St_Mvt_Stock_Fille">
        <input name="srt" type="hidden" value="<%=request.getParameter("srt")%>">
        <input name="ent" type="hidden" value="<%=request.getParameter("ent")%>">
    </form>
    <%
        StMvtStockFilleLib p = new StMvtStockFilleLib();
        p.setNomTable("St_Mvt_Stock_Fille_lib"); // vue
        StMvtStockFilleLib[] liste = (StMvtStockFilleLib[]) CGenUtil.rechercher(p, null, null, " and idmvtstock in ('" + request.getParameter("srt") + "','" + request.getParameter("ent") + "')");
    %>
    <div id="selectnonee">
        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
            <thead>
                <tr class="head">
                    <th width="20%" align="center" valign="top" style="background-color:#bed1dd">Article</th>
                    <th width="20%" align="center" valign="top" style="background-color:#bed1dd">Entree</th>
                    <th width="20%" align="center" valign="top" style="background-color:#bed1dd">Sortie</th>
                    <th width="20%" align="center" valign="top" style="background-color:#bed1dd">P.U</th>
                </tr>
            </thead>
            <tbody>
                <%
                    for (int i = 0; i < liste.length; i++) {
                %>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"><%=liste[i].getArticle()%> </td>
                    <td width="20%" align="center"><%=liste[i].getEntree()%></td>
                    <td width="20%" align="center"><%=liste[i].getSortie()%></td>
                    <td width="20%" align="center"><%=liste[i].getPu()%></td>

                </tr>
                <%
                    }
                %>
            </tbody>
        </table>
    </div>
</div>