<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="bean.AdminGen"%>
<%@page import="user.UserEJB"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="mg.allosakafo.stock.EtatdeStockDate"%>
<%@page import="affichage.PageRechercheGroupe"%>

<% 
    EtatdeStockDate lv = new EtatdeStockDate();
    lv.setNomTable("as_etatstock_libelle1");
    String listeCrt[] = {"daty", "ingredients", "unite","point"};
    String listeInt[] = {"daty","entree", "sortie", "reste","report"};
    
	UserEJB u = (user.UserEJB) session.getValue("u");
	
    String colDefaut[] = {"ingredients", "unite","point"}; 
    String somDefaut[] = {"entree", "sortie", "reste","report"};
    PageRechercheGroupe pr = new PageRechercheGroupe(lv, request, listeCrt, listeInt, 3, colDefaut, somDefaut, null,7, 3);
	
    pr.setUtilisateur(u);
    pr.setLien((String) session.getValue("lien"));
     
	 affichage.Champ[] liste = new affichage.Champ[2];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("as_unite");
    liste[0] = new Liste("unite", ou, "VAL", "id");
    
    TypeObjet ou1 = new TypeObjet();
    ou1.setNomTable("point");
    liste[1] = new Liste("point", ou1, "VAL", "val");

    pr.getFormu().changerEnChamp(liste);
    
    pr.getFormu().getChamp("ingredients").setLibelleAffiche("Ingredients");
	pr.getFormu().getChamp("daty1").setLibelleAffiche("Date debut");
	pr.getFormu().getChamp("daty2").setLibelleAffiche("Date fin");
    pr.getFormu().getChamp("unite").setLibelleAffiche("Unit�");
    
    pr.setApres("stock/mvtStock/etatstock-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage();
	
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Etat de stock</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/etatstock-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  
			out.println(pr.getTableauRecap().getHtml());
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>