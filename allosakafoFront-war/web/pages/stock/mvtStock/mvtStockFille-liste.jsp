<%@page import="mg.cnaps.st.StMvtStockFille"%>
<%@page import="affichage.PageRecherche"%>
<% StMvtStockFille lv = new StMvtStockFille();

    lv.setNomTable("ST_MVT_STOCK_FILLE_LIBELLE");
    String listeCrt[] = {"id", "article", "entree", "sortie", "solde", "pu"};
    String listeInt[] = null;
    String libEntete[] = {"id", "article", "entree", "sortie", "solde", "pu"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));

    pr.setApres("stock/mvtStock/mvtStockFille-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste mouvement stock Fille</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/mvtStockFille-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/mvtStock/mvtStockFille-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "article", "entree", "sortie", "solde", "Prix unitaire"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
