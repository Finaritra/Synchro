<%@page import = "mg.allosakafo.stock.*"%>
<%@page import = "user.*" %>
<%@page import = "utilitaire.*" %>
<%@page import = "bean.*" %>
<%@page import = "java.sql.SQLException" %>
<%@page import = "affichage.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
        String nomtable;
        String typeBoutton;
    %>
    <%
        try {
            nomtable = request.getParameter("nomtable");
            typeBoutton = request.getParameter("type");
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            bute = request.getParameter("bute");
            Object temp = null;
            String[] rajoutLien = null;
            String classe = request.getParameter("classe");
            ClassMAPTable t = null;
            String tempRajout = request.getParameter("rajoutLien");
            String val = "";

            String rajoutLie = "";
            if (tempRajout != null && tempRajout.compareToIgnoreCase("") != 0) {
                rajoutLien = utilitaire.Utilitaire.split(tempRajout, "-");
            }

            if (acte.compareToIgnoreCase("insertInventaire") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                MvtStock mvt = (MvtStock) f;
                
                String[] id = request.getParameterValues("id");
                String[] quantiteparpack = request.getParameterValues("quantiteparpack");
                String[] quantite = request.getParameterValues("quantite");
                val = u.saveInventaire(mvt, id, quantiteparpack, quantite);
            }
			
			if (acte.compareToIgnoreCase("insertMvt") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                MvtStock mvt = (MvtStock) f;
				String typemvt = request.getParameter("type");
                String[] id = request.getParameterValues("id");
                String[] quantiteparpack = request.getParameterValues("quantiteparpack");
                String[] quantite = request.getParameterValues("quantite");
                val = u.saveInventaire(mvt, id,quantite, typemvt);
            }
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>&id=<%=val%>");</script>
    <%

    } catch (Exception e) {
        e.printStackTrace();
    %>

    <script language="JavaScript"> alert('<%=e.getMessage()%>');
        history.back();</script>
        <%
                return;
            }
        %>
</html>



