<%-- 
    Document   : as-commande-liste
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<% 
    EtatdeStockDate lv = new EtatdeStockDate();
    
    String nomTable = "as_mvt_stock_libfille";
    lv.setNomTable(nomTable);
	
    String listeCrt[] = {"ingredients", "daty", "unite"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"daty","ingredients","entree", "sortie", "unite"};

    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    
    pr.setApres("stock/mvtStock/mvtStockFille-etat.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Mouvements de stock</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/mvtStockFille-etat.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%  
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Date","Ingr&eacute;dient",  "Entr&eacute;e", "Sortie", "Unit&eacute;"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
			  
				out.println(pr.getTableau().getHtml());
				out.println(pr.getBasPage());
		%>
    </section>
</div>