<%@page import="utilitaire.Constante"%>
<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="java.sql.Connection"%>
<%@page import="mg.cnaps.accueil.PiecesInfoRecues"%>
<%@page import="mg.cnaps.accueil.PiecesInfoRequises"%>
<%@ page import="bean.CGenUtil"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.cnaps.accueil.SigPiecesChemin"%>
<%@page import="mg.cnaps.accueil.PiecesRecues"%>

<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    Connection c = null;
    try {
        c = (new utilitaire.UtilDB()).GetConn();
        
        StMagasin mg = new StMagasin();
        mg.setNomTable("ST_MAGASIN_LIBELLE");
        StMagasin[] lv = (StMagasin[]) CGenUtil.rechercher(mg, null, null, c, "");

        TypeObjet typmvt = new TypeObjet();
        typmvt.setNomTable("st_type_mvt");
        TypeObjet[] typ = (TypeObjet[]) CGenUtil.rechercher(typmvt, null, null, c, "");

        TypeObjet classe = new TypeObjet();
        classe.setNomTable("st_classe");
        TypeObjet[] cls = (TypeObjet[]) CGenUtil.rechercher(classe, null, null, c, "");

        StTiers prj = new StTiers();
        typmvt.setNomTable("st_tiers");
        StTiers[] lprj = (StTiers[]) CGenUtil.rechercher(prj, null, null, c, "");

        TypeObjet tbn = new TypeObjet();
        tbn.setNomTable("st_type_besoin");
        TypeObjet[] tb = (TypeObjet[]) CGenUtil.rechercher(tbn, null, null, c, "");
        
        TypeObjet chap = new TypeObjet();
        chap.setNomTable("ST_TYPE_ARTICLE");
        TypeObjet[] chapitre = (TypeObjet[]) CGenUtil.rechercher(chap, null, null, c, "");
%>
<div class="content-wrapper" style="min-height: 600px;">
    <h1>sortie stock</h1>
    <form action="stock.jsp?but=stock/mvtStock/apresMvtStock.jsp" method="post" name="AtDsp" id="AtDsp" data-parsley-validate="" novalidate="">
        <div class="row">
            <div class="col-md-6">
                <div class="box-insert">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>
                                            <label for="Libelle" tabindex="1">Magasin</label>
                                        </th>
                                        <td>
                                            <select name="magasin" class="form-control" id="magasin" tabindex="1" data-parsley-id="4">
                                                <%
                                                    for (int i = 0; i < lv.length; i++) {
                                                %>
                                                <option value="<%=lv[i].getId()%>"><%=lv[i].getDesce()%></option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </td>
                                        <td></td>
                                    </tr>
                                    

                                <input type="hidden" name="typemvt" value="<%=Constante.constanteSortieStock%>">
                                <input type="hidden" name="fournisseur" value="STTR9">
                                
                                <tr>
                                    <th>
                                        <label for="Libelle" tabindex="1">Date</label>
                                    </th>
                                    <td>
                                        <input name="daty" type="textbox" class="form-control" value="<%=Utilitaire.dateDuJour()%>" id="daty" tabindex="3" data-parsley-id="4">
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>
                                        <label for="Libelle" tabindex="1">Cls</label>
                                    </th>
                                    <td><select name="classee" class="form-control" id="classee" tabindex="4" data-parsley-id="4">
                                            <%
                                                for (int i = 0; i < cls.length; i++) {
                                            %>
                                            <option value="<%=cls[i].getId()%>"><%=cls[i].getVal()%></option>
                                            <%
                                                }
                                            %>
                                        </select>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>
                                        <label for="Libelle" tabindex="1">Exercice</label>
                                    </th>
                                    <td>
                                        <input name="exercice" type="textbox" class="form-control" id="exercice" value="<%=Utilitaire.getAneeEnCours()%>" tabindex="5" data-parsley-id="4">
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>
                                        <label for="Libelle" tabindex="1">Num BS</label>
                                    </th>
                                    <td>
                                        <input name="numbs" type="textbox" class="form-control" id="numbs" value="" tabindex="1" data-parsley-id="4">
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>
                                        <label for="Libelle" tabindex="1">Projet</label>
                                    </th>
                                    <td>
                                        <input name="projet" type="textbox" class="form-control" id="projet" value="" tabindex="1" data-parsley-id="4">
                                    </td>
                                    <td>
                                        <input name="choix" type="button" class="submit" onclick="pagePopUp('choix/listeProjetChoix.jsp?champReturn=projet')" value="...">
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <label for="Libelle" tabindex="1">Beneficiaire</label>
                                    </th>
                                    <td>
                                        <input name="beneficiaire" type="textbox" class="form-control" id="beneficiaire" value="" tabindex="1" data-parsley-id="4">
                                    </td>
                                    <td>
                                        <input name="choix" type="button" class="submit" onclick="pagePopUp('choix/listeBeneficiaireChoix.jsp?champReturn=beneficiaire')" value="...">
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <label for="Libelle" tabindex="1">Type</label>
                                    </th>
                                    <td><select name="typebesoin" class="form-control" id="typebesoin" tabindex="9" data-parsley-id="4">
                                            <%
                                                for (int i = 0; i < tb.length; i++) {
                                            %>
                                            <option value="<%=tb[i].getId()%>"><%=tb[i].getVal()%></option>
                                            <%
                                                }
                                            %>
                                        </select></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>
                                        <label for="Libelle" tabindex="1">Observation</label>
                                    </th>
                                    <td>
                                        <textarea name="observation" class="form-control" id="observation" value="" tabindex="10" data-parsley-id="4"></textarea>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <label for="Libelle" tabindex="1">Inputation analytique</label>
                                    </th>
                                    <td>
                                        <input name="imp_analytique" type="textbox" class="form-control" id="imp_analytique" value="" tabindex="11" data-parsley-id="4">
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>
                                        <label for="Libelle" tabindex="1">Chapitre</label>
                                    </th>
                                    <td><select name="chapitre" class="form-control" id="typebesoin" tabindex="9" data-parsley-id="4">
                                            <%
                                                for (int i = 0; i < chapitre.length; i++) {
                                            %>
                                            <option value="<%=chapitre[i].getId()%>"><%=chapitre[i].getVal()%></option>
                                            <%
                                                }
                                            %>
                                        </select></td>
                                    <td></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <div class="col-xs-12">
                                <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;" tabindex="61">Valider</button> 
                                <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;" tabindex="62">R&eacute;initialiser</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <input name="acte" type="hidden" id="nature" value="insert">
            <input name="bute" type="hidden" id="bute" value="stock/mvtStock/apresMvtArticles-saisie.jsp">
            <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StMvtStock">
            <input name="nomtable" type="hidden" id="nomtable" value="ST_MVT_STOCK">
            <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-">
            <input name="action" type="hidden" value="sortie">
        </div>
    </form>
</div>
<% } catch (Exception ex) {
        ex.printStackTrace();
    } finally {
        if (c != null) {
            c.close();
        }
    }%>
