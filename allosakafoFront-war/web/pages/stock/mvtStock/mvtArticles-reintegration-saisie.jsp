<%@page import="mg.cnaps.st.StMvtStockFilleLib"%>
<%@page import="mg.cnaps.st.StMvtStockFille"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="java.sql.Connection"%>
<%@page import="mg.cnaps.accueil.PiecesInfoRecues"%>
<%@page import="mg.cnaps.accueil.PiecesInfoRequises"%>
<%@ page import="bean.CGenUtil" %>
<%@page import="user.UserEJB"%>
<%@page import="mg.cnaps.accueil.SigPiecesChemin"%>
<%@page import="mg.cnaps.accueil.PiecesRecues"%>

<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    Connection c = null;
    try {
        c = (new utilitaire.UtilDB()).GetConn();
        StMagasin mg = new StMagasin();
        mg.setNomTable("ST_MAGASIN_LIBELLE");
        StMagasin[] lv = (StMagasin[]) CGenUtil.rechercher(mg, null, null, c, "");

        TypeObjet typmvt = new TypeObjet();
        typmvt.setNomTable("st_type_mvt");
        TypeObjet[] typ = (TypeObjet[]) CGenUtil.rechercher(typmvt, null, null, c, "");

        TypeObjet classe = new TypeObjet();
        classe.setNomTable("st_classe");
        TypeObjet[] cls = (TypeObjet[]) CGenUtil.rechercher(classe, null, null, c, "");

        TypeObjet tbn = new TypeObjet();
        tbn.setNomTable("st_type_besoin");
        TypeObjet[] tb = (TypeObjet[]) CGenUtil.rechercher(tbn, null, null, c, "");
%>
<div class="content-wrapper" style="min-height: 600px;">
    <h1> Mouvement de transfer stock n� <%=request.getParameter("id")%></h1>
    <form action="stock.jsp?but=stock/mvtStock/apresMvtStock.jsp" method="post" name="AtDsp" id="AtDsp" data-parsley-validate="" novalidate="">
        <div class="row">
            <div class="col-md-6">
                <div class="box-insert">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>
                                            <label for="Libelle" tabindex="1">Date</label>
                                        </th>
                                        <td>
                                            <input name="daty" type="textbox" class="form-control" value="<%=Utilitaire.dateDuJour()%>" id="daty" value="" tabindex="1" data-parsley-id="4">
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <label for="Libelle" tabindex="1">Magasin</label>
                                        </th>
                                        <td>
                                            <select name="magasin" class="form-control" id="magasin" tabindex="1" data-parsley-id="4">
                                                <%
                                                    for (int i = 0; i < lv.length; i++) {
                                                %>
                                                <option value="<%=lv[i].getId()%>"><%=lv[i].getDesce()%></option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <label for="Libelle" tabindex="1">Mouvement</label>
                                        </th>
                                        <td>
                                            <input type="text" class="form-control" name="idmouvement" id="idmouvement" value="<%=request.getParameter("id")%>" readonly="readonly"/>
                                        </td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <th>
                                            <label for="Libelle" tabindex="1">Observation</label>
                                        </th>  
                                        <td>
                                            <textarea name="designation"  class="form-control" id="designation" value="" tabindex="12" data-parsley-id="4"></textarea>
                                        </td>
                                        <td></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <div class="col-xs-12">
                                <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;" tabindex="61">Valider</button> 
                                <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;" tabindex="62">R�initialiser</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <input name="acte" type="hidden" id="nature" value="reintegr">
            <input name="bute" type="hidden" id="bute" value="stock/mvtStock/mvtStock-liste.jsp&premier=false">
            <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StMvtStock">
            <input name="nomtable" type="hidden" id="nomtable" value="ST_MVT_STOCK">

        </div>




        <%
            StMvtStockFilleLib mvtstockfille = new StMvtStockFilleLib();
            mvtstockfille.setNomTable("ST_MVT_STOCK_FILLE_LIB");
            String mvtstockId = "AND IDMVTSTOCK = '" + request.getParameter("id") + "'";
            StMvtStockFilleLib[] mvtstockfilles = (StMvtStockFilleLib[]) u.getData(mvtstockfille, null, null, null, mvtstockId);


        %>                          
        <div class="row">
            <div class="col-md-6">
                <div class="box-fiche">
                    <div class="box">
                        <div class="box-title with-border">
                            <h1 class="box-title">D&eacute;tails</h1>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Id</th>
                                    <th>Article</th>
                                    <th>Quantit&eacute; Total sortie</th>
                                    <th>Quantit&eacute; &agrave; r&eacute;int&eacute;gr&eacute;</th>
                                    <th>Prix Unitaire</th>
                                </tr>
                                <%  for (int i = 0; i < mvtstockfilles.length; i++) {
                                %>
                                <tr>
                                    <td><%=mvtstockfilles[i].getId()%></td>
                                <input type="hidden" name="idarticle" id="idarticle" value="<%=mvtstockfilles[i].getIdarticle()%>">
                                    <td><%=mvtstockfilles[i].getArticle()%></td>
                                    <td><%=mvtstockfilles[i].getSortie()%></td>
                                    <td>
                                        <input type="text" name="quantiter" value="<%=mvtstockfilles[i].getSortie()%>" id="quantiter" class="form-control"/>
                                        </td>
                                    <td>
                                        <input type="hidden" name="prixunitaire" id="prixunitaire" value="<%=Utilitaire.formaterAr(mvtstockfilles[i].getPu())%>"/>
                                        <%=Utilitaire.formaterAr(mvtstockfilles[i].getPu())%></td>
                                </tr>
                                <%
                                    }
                                %>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>    

<% } catch (Exception ex) {
        ex.printStackTrace();
    } finally {
        if (c != null) {
            c.close();
        }
    }
%>