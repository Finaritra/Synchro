<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.st.StMvtStockDetails"%>
<%@page import="mg.cnaps.st.StMvtStock"%>
<%@page import="affichage.PageRecherche"%>

<%
    try {
        StMvtStockDetails lv = new StMvtStockDetails();

        lv.setNomTable("ST_MVT_STOCK_DETAILS");
        String listeCrt[] = {"id", "daty", "article", "magasin", "service"};
        String listeInt[] = {"daty"};
        String libEntete[] = {"id", "daty", "magasin", "article", "entree", "sortie", "service"};

        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 7);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        pr.getFormu().getChamp("daty1").setLibelle("Date min");
        pr.getFormu().getChamp("daty2").setLibelle("Date max");

        pr.setApres("stock/mvtStock/mvtStock-details.jsp");
        String[] colSomme = null;
        pr.creerObjetPage(libEntete, colSomme);
        StMvtStockDetails[] liste = (StMvtStockDetails[]) pr.getRs().getResultat();
        StMvtStockDetails critere = (StMvtStockDetails) request.getSession().getAttribute("critere");
        String article = critere.getArticle() == null || critere.getArticle().length() == 0 ? "" : "&article=" + critere.getArticle();
        String magasin = critere.getMagasin() == null || critere.getMagasin().length() == 0 ? "" : "&magasin=" + critere.getMagasin();
%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Journal des mouvements de stock</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/mvtStock-details.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/mvtStock/mvtStockFille-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id","Date", "Magasin", "Article", "Entree", "Sortie", "Service"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
        %>
        <a href="<%=(String) session.getValue("lien")%>/../../EtatMedecineServlet?action=ficheStockMedicament<%=article%><%=magasin%>" class="btn btn-primary pull-right" id="export">Imprimer</a>
        <%
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%    } catch (Exception ex) {
        ex.printStackTrace();
    }

%>
<script>
    var dateMin = document.getElementById("daty1").value === "" ? "" : "&datemin=" + document.getElementById("daty1").value;
    var dateMax = document.getElementById("daty2").value === "" ? "" : "&datemax=" + document.getElementById("daty2").value;
    document.getElementById("export").href = document.getElementById("export").href + dateMin + dateMax;
</script>