<%@page import="mg.cnaps.st.StFicheStock"%>
<%@page import="mg.cnaps.st.StMvtStockEtat"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.Utilitaire"%>
<%
    try {
        StFicheStock lv = new StFicheStock();

      //  lv.setNomTable("ST_MVT_STOCK_LIBELLE_ETAT");
        String listeCrt[] = {"magasin","daty", "article", "entree", "sortie", "solde"};
        String listeInt[] = {"entree", "sortie", "solde"};
        String libEntete[] = {"magasin","daty", "article", "entree", "sortie", "solde", "pu"};

        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 6);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));

        pr.setApres("stock/mvtStock/mvtStock-etat.jsp");
        String[] colSomme = null;
        pr.creerObjetPage(libEntete, colSomme);
        StFicheStock[] liste = (StFicheStock[]) pr.getRs().getResultat();
        StFicheStock critere = (StFicheStock) request.getSession().getAttribute("critere");
        String magasin = critere.getMagasin() == null || critere.getMagasin().length() == 0 ? "" : "&magasin=" + critere.getMagasin();
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Etat de Stock</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/mvtStock-etat.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%
            out.println(pr.getTableauRecap().getHtml());
        %>
        </br>
        <div class="row">
            <div class="row col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" align="center">LISTE</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                                <thead>
                                    <tr class="head">
                                        <th align="center" valign="top" style="background-color:#bed1dd">Magasin</th>
                                            <th align="center" valign="top" style="background-color:#bed1dd">Date</th>
                                 
                                        <th align="center" valign="top" style="background-color:#bed1dd">Article</th>
                                        <th align="center" valign="top" style="background-color:#bed1dd">Solde</th>
                                        <th align="center" valign="top" style="background-color:#bed1dd">PU</th>
                                        <th align="center" valign="top" style="background-color:#bed1dd">Date</th>
                                        <th align="center" valign="top" style="background-color:#bed1dd">-</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% for (int i = 0; i < liste.length; i++) {%>
                                    <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                        <td align="left"><%=liste[i].getMagasin() %></td>
                                        <td align="left"><%=Utilitaire.formatterDaty(liste[i].getDaty())%></td>
                                        <td align="left">
                                            <a href="<%=pr.getLien()%>?but=stock/mvtStock/mvtStock-details.jsp&article=<%=liste[i].getArticle()%>&magasin=<%=liste[i].getMagasin()%>" title="Voir details">
                                                <%=liste[i].getArticle()%>
                                            </a>
                                             
                                        </td>
                                        
                                        <td align="right"><%=Utilitaire.formaterAr(liste[i].getSolde()) %></td>
                                        <td align="right"><%=Utilitaire.formaterAr(liste[i].getPu()) %></td>
                                        <td align="right"><%=Utilitaire.formatterDaty(liste[i].getDaty()) %></td>
                                        <td align="right"><a href="<%=(String) session.getValue("lien")%>/../../EtatStockServlet?action=ficheStock&article=<%=liste[i].getArticle()%><%=magasin%>" id="export" class="btn btn-primary">Imprimer</a></td>
                                    </tr>
                                    <% }%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%    } catch (Exception ex) {
        ex.printStackTrace();
    }

%>