<%-- 
    Document   : journaux-mouvements-details.jsp
    Created on : 7 mars 2016, 10:33:05
    Author     : Joe
--%>
<%@page import="mg.cnaps.st.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    String lien = (String) session.getValue("lien");
    String magasin = "";
    String daty = "";
    try {

        String date = Utilitaire.dateDuJour();
        magasin = "%";
        String article = "%";

        if (request.getParameter("date") != null && request.getParameter("date").compareToIgnoreCase("") != 0) {
            date = request.getParameter("date");
        }
        if (request.getParameter("magasin") != null && request.getParameter("magasin").compareToIgnoreCase("") != 0) {
            magasin = request.getParameter("magasin");
        }
        if (request.getParameter("article") != null && request.getParameter("article").compareToIgnoreCase("") != 0) {
            article = request.getParameter("article");
        }
        //pr.creerObjetPage(libEntete, null);
        StFicheStock[] listResultat = StockService.genererFicheStockDatee(article, date, magasin);
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1 align="center">D&eacute;tails mouvement de stock</h1>
    </section>
    <section class="content-header">
        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
            <thead>
                <tr class="head">
                    <th align="left">Magasin:</th>
                    <th align="left"><%=listResultat[0].getMagasin()%></th>
                </tr>
                <tr class="head">
                    <th align="left">Article</th>
                    <th align="left"><%=listResultat[0].getArticle()%></th>
                </tr>
            </thead>
        </table>
    </section>
    <section class="content">

        <div class="row">
            <div class="row col-md-12">
                <div class="box box-primary">
                    <div class="box-header">

                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                                <thead>
                                    <tr class="head">
                                        <th align="left">Date</th>
                                        <th align="right">Entr&eacute;e</th>
                                        <th align="right">Sortie</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% if (listResultat != null && listResultat.length > 0) {
                                            for (int i = 0; i < listResultat.length; i++) {%>
                                    <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                        <td align="left"><%=Utilitaire.convertDatyFormtoRealDatyFormat(listResultat[i].getDaty() + "")%></td>
                                        <td align="right"><%=Utilitaire.formaterAr(listResultat[i].getEntree())%></td>
                                        <td align="right"><%=Utilitaire.formaterAr(listResultat[i].getSortie())%></td>
                                    </tr>
                                    <% }
                                        }%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<% } catch (Exception ex) {
        ex.printStackTrace();
    }
%>