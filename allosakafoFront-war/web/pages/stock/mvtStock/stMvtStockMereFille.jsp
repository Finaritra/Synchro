<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.st.StMvtStockMereFille"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>
<%
    StMvtStockMereFille p = new StMvtStockMereFille();
    p.setNomTable("StMvtStockMereFille");

    String listeCrt[] = {"daty", "magasin", "designation", "typemvt", "typebesoin", "exercice", "article", "classee"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"daty", "magasin", "designation", "typemvt", "typebesoin", "exercice", "article", "classee"};
    PageRecherche pr = new PageRecherche(p, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stock/mvtStock/stMvtStockMereFille.jsp");
    pr.getFormu().getChamp("daty1").setLibelle("Du");
    pr.getFormu().getChamp("daty2").setLibelle("Au");
    pr.getFormu().getChamp("magasin").setLibelle("Magasin");
    pr.getFormu().getChamp("designation").setLibelle("Designation");

//affichage.Champ[] liste = new affichage.Champ[2];
//    StMagasin dep = new StMagasin();
//    dep.setNomTable("St_Magasin");
//    liste[0] = new Liste("magasinDep", dep, "code", "id");
//    
//    StMagasin arr = new StMagasin();
//    arr.setNomTable("St_Magasin");
//    liste[1] = new Liste("magasinArr", arr, "code", "id");
//    
//    pr.getFormu().changerEnChamp(liste);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste mouvement stock</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/stMvtStockMereFille.jsp" method="post" name="personneliste" id="personneliste">
            <% out.println(pr.getFormu().getHtmlEnsemble());%>
        </form>
        <%
            //String lienTableau[] = {pr.getLien() + "?but=stock/mvtStock/intraMvtStock-fiche.jsp"};
            //String colonneLien[] = {"id"};
            String libelles[] = {"daty", "magasin", "designation", "typemvt", "typebesoin", "exercice", "article", "classee"};
            //pr.getTableau().setLien(lienTableau);
            //pr.getTableau().setColonneLien(colonneLien);
            pr.getTableau().setLibelleAffiche(libelles);
            out.println(pr.getTableauRecap().getHtml());

            out.println(pr.getTableau().getHtml());
                    out.println(pr.getBasPage());%>
    </section>
</div>
