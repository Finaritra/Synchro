
<%@page import = "affichage.PageRecherche"%>
<%@page import = "mg.cnaps.st.StCalculEtatStock"%>
<%@page import = "mg.cnaps.st.StMagasin"%>
<%@page import = "user.UserEJB"%>
<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    String lien = (String) session.getValue("lien");
    
    StMagasin[] mags = (StMagasin[]) u.getData(new StMagasin(), null, null, null, "");
%>

<div class="content-wrapper">
    <h1 align="center">Fiche de stock</h1>
    <form action="<%=lien%>?but=stock/mvtStock/apresMvtStock.jsp" method="post" name="analyse" id="analyse" data-parsley-validate="" novalidate="">
        <div class="row">
            <div class="col-md-6">
                <div class="box-insert">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>
                                            <label tabindex="1" for="Date">Date</label>
                                        </th>
                                        <td>
                                            <input id="datefind" class="form-control datepicker" type="textbox" tabindex="1" name="datefind" data-parsley-id="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <label tabindex="2" for="magasin">Magasin</label>
                                        </th>
                                        <td>
                                            <select id="magasin" class="form-control" tabindex="2" name="magasin" data-parsley-id="2">
                                            <%
                                                if(mags != null && mags.length > 0){ 
                                                    for(int i = 0; i < mags.length; i++){%>
                                                    <option value="<%=mags[i].getDesce()%>"><%=mags[i].getDesce()%></option>
                                            <% }} %>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <div class="col-xs-12">
                                <button class="btn btn-success pull-right" tabindex="91" style="margin-right: 25px;" name="Submit2" type="submit">Valider</button>
                                <button class="btn btn-default pull-right" tabindex="92" style="margin-right: 15px;" name="Submit2" type="reset">Réinitialiser</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input name="acte" type="hidden" id="nature" value="genereranalyse">
        <input name="bute" type="hidden" id="bute" value="stock/mvtStock/analyseMvt-generer.jsp">
    </form>
<%
    String id = request.getParameter("valeur");
    String magasin = request.getParameter("magasin");
    if(id != null && magasin != null){
        StCalculEtatStock stce = new StCalculEtatStock();
        String listeCrt[] = {"article"};
        String listeInt[] = null;
        String libEntete[] = {"daty", "magasin", "article", "entree", "sortie"};
        PageRecherche pr = new PageRecherche(stce, request, listeCrt, listeInt, 3, libEntete, 4);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        String where = "AND IDUSER = '" + id + "' AND IDMAGASIN = '" + magasin + "' AND IDINVENTAIRE IS NULL";
        pr.setAWhere(where);
        String apres = "stock/mvtStock/analyseMvt-generer.jsp&valeur="+id+"&magasin="+magasin;
        pr.setApres(apres);
        pr.creerObjetPage(libEntete);
        
%>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/analyseMvt-generer.jsp&valeur=<%=id%>&magasin=<%=magasin%>" method="post" name="analyseliste" id="analyseliste">
            <% out.println(pr.getFormu().getHtmlEnsemble());%>
        </form>
        <%
            String libEnteteAffiche[] = {"Date", "Magasin", "Article", "Entree", "Sortie"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
<% } %>
</div>