<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.st.StIntraMvtStock"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    UserEJB u;
    StIntraMvtStock mag;
%>
<%
    mag = new StIntraMvtStock();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(mag, request, (user.UserEJB) session.getValue("u"));
	
    pi.setLien((String) session.getValue("lien"));
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("magasinDep").setLibelle("Magasin Depart");
    pi.getFormu().getChamp("magasinArr").setLibelle("Magasin Arrivee");
    pi.getFormu().getChamp("designation").setLibelle("Designation");
    pi.getFormu().getChamp("designation").setType("textarea");

    affichage.Champ[] liste = new affichage.Champ[2];
    StMagasin dep = new StMagasin();
    dep.setNomTable("St_Magasin");
    liste[0] = new Liste("magasinDep", dep, "code", "id");

    StMagasin arr = new StMagasin();
    arr.setNomTable("St_Magasin");
    liste[1] = new Liste("magasinArr", arr, "code", "id");

    pi.getFormu().changerEnChamp(liste);
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification mouvement inter magasin</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="stintramvtstock">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/mvtStock/intraMvtStock-modif.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StIntraMvtStock">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
