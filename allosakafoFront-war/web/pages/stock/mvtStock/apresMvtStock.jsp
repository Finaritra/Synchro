<%@page import="java.lang.Object"%>
<%@page import="mg.cnaps.immo.Immobilisation"%>
<%@page import="mg.cnaps.st.StMvtStockFille"%>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="bean.*" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="affichage.*" %>
<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
        String nomtable;
        String typeBoutton;
    %>
    <%
        try {
            typeBoutton = request.getParameter("type");
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            bute = request.getParameter("bute");
            Object temp = null;
            String[] rajoutLien = null;
            String classe = request.getParameter("classe");
            ClassMAPTable t = null;
            String tempRajout = request.getParameter("rajoutLien");
            String val = "";

            String rajoutLie = "";
            if (tempRajout != null && tempRajout.compareToIgnoreCase("") != 0) {
                rajoutLien = utilitaire.Utilitaire.split(tempRajout, "-");
            }
            if (bute == null || bute.compareToIgnoreCase("") == 0) {
                bute = "pub/Pub.jsp";
            }

            if (classe == null || classe.compareToIgnoreCase("") == 0) {
                classe = "pub.Montant";
            }

            if (typeBoutton == null || typeBoutton.compareToIgnoreCase("") == 0) {
                typeBoutton = "3"; //par defaut modifier
            }

            int type = Utilitaire.stringToInt(typeBoutton);
            String id = request.getParameter("id");
            if (acte.compareToIgnoreCase("insert") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
                ClassMAPTable o = (ClassMAPTable) u.createObject(f);
                temp = (Object) o;
                if (o != null) {
                    id = o.getTuppleID();
                }
            }
            if (acte.compareToIgnoreCase("insertmvt") == 0) {

                String idmere = request.getParameter("idmere");
                String numbs = request.getParameter("numbs");
                String[] article = request.getParameterValues("id");
                String[] quantite = request.getParameterValues("quantite");
                String[] remarque = request.getParameterValues("remarque");
                String exo = request.getParameter("excercice");
                //String classee = request.getParameter("classee");
                String typebesoin = request.getParameter("typebesoin");
                //String projet = request.getParameter("projet");
                String magasin = request.getParameter("magasin");
                u.enregistrerMvtFilleWithDAppro(idmere, exo, article, quantite, remarque, typebesoin, magasin, numbs);
            }
            if (acte.compareToIgnoreCase("corriger") == 0) {

                String[] idArt = request.getParameterValues("id");
                String[] magasin = request.getParameterValues("magasin");
                String[] inventaire = request.getParameterValues("reste");

                // appel fonction rollback
            }
            if (acte.compareToIgnoreCase("saisieInventaire") == 0) {
                String[] idArt = request.getParameterValues("id");
                String[] quantite = request.getParameterValues("quantite");
                String idInv = request.getParameter("inventaire");
                // System.out.println("idArt nbre: " + idArt[0] + " quantite nbre: " + quantite[0] + " idInv: " + idInv);

                u.saveInventaireMultiple(idArt, quantite, idInv);
    %>
    <script language="JavaScript">
        document.location.replace("<%=lien%>?but=<%=bute%>&id=<%=idInv%>");
    </script>
    <%
        }
        if (acte.compareToIgnoreCase("delete") == 0) {

            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            temp = (Object) t;
            u.deleteObject(t);

        }
        if (acte.compareToIgnoreCase("deletefille") == 0) {

            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            temp = (Object) t;
            u.deleteObjetFille(t);
        }
        if (acte.compareToIgnoreCase("transfert") == 0) {
            String idmouvement = request.getParameter("idmouvement");
            String daty = request.getParameter("daty");
            String designation = request.getParameter("designation");
            String magasin = request.getParameter("magasin");
            u.tranfererStock(idmouvement, daty, magasin, designation);

        }
        if (acte.compareToIgnoreCase("reintegr") == 0) {
            String idmouvement = request.getParameter("idmouvement");
            String daty = request.getParameter("daty");
            String designation = request.getParameter("designation");
            String magasin = request.getParameter("magasin");
            String[] idart = request.getParameterValues("idarticle");
            String[] quantiter = request.getParameterValues("quantiter");
            String[] prixunitaire = request.getParameterValues("prixunitaire");
            u.reintegrationStock(idmouvement, daty, magasin, designation, idart, quantiter, prixunitaire);

        }
        if (acte.compareToIgnoreCase("update") == 0) {
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            Page p = new Page(t, request);
            ClassMAPTable f = p.getObjectAvecValeur();
            temp = f;
            u.updateObject(f);
        }
        if (acte.compareToIgnoreCase("genereranalyse") == 0) {
            String daty = request.getParameter("datefind");
            String magasin = request.getParameter("magasin");
            val = u.genererAnalyseMvtStock(daty, magasin);
            if (!val.isEmpty()) {
                val = val + "&magasin=" + magasin;
            }
        }
        if (rajoutLien != null) {
            if (classe.compareToIgnoreCase("mg.cnaps.st.StMvtStock") == 0) {
                rajoutLie = "&id=" + ((ClassMAPTable) temp).getTuppleID() + "&action=" + request.getParameter("action") + "&chapitre=" + request.getParameter("chapitre");
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&id=<%=id%>");</script>
    <%
    } else if (classe.compareToIgnoreCase("mg.cnaps.st.StMvtStockFille") == 0) {
        rajoutLie = "&id=" + request.getParameter("idstock") + "&action=" + request.getParameter("action") + "&chapitre=" + request.getParameter("chapitre");
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&id=<%=id%>");</script>
    <%
    } else if (classe.compareToIgnoreCase("mg.cnaps.st.StIntraMvtStock") == 0) {
        rajoutLie = "&id=" + ((ClassMAPTable) temp).getTuppleID() + "&action=" + request.getParameter("action");
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&id=<%=id%>");</script>
    <%
    } else if (classe.compareToIgnoreCase("mg.cnaps.st.StBonDeCommande") == 0) {
        rajoutLie = "&id=" + ((ClassMAPTable) temp).getTuppleID();
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&id=<%=id%>");</script>
    <%
    } else if (classe.compareToIgnoreCase("mg.cnaps.st.StBonDeCommandeFille") == 0) {
        rajoutLie = "&id=" + request.getParameter("idbc");
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&id=<%=id%>");</script>
    <%
    } else if (classe.compareToIgnoreCase("mg.cnaps.st.StDemandeFourniture") == 0) {
        rajoutLie = "&id=" + ((ClassMAPTable) temp).getTuppleID();
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&id=<%=id%>");</script>
    <%
    } else if (classe.compareToIgnoreCase("mg.cnaps.st.StInventaire") == 0) {
        rajoutLie = "&id=" + ((ClassMAPTable) temp).getTuppleID();
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&id=<%=id%>");</script>
    <%
    } else if (classe.compareToIgnoreCase("mg.cnaps.st.StInventaireFille") == 0) {
        rajoutLie = "&id=" + request.getParameter("id_inventaire");
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&id=<%=id%>");</script>
    <%
    } else if (classe.compareToIgnoreCase("mg.cnaps.st.StDemandeAppro") == 0) {
        rajoutLie = "&id=" + ((ClassMAPTable) temp).getTuppleID();
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&id=<%=id%>");</script>
    <%
    } else if (classe.compareToIgnoreCase("mg.cnaps.st.StDemandeApproFille") == 0) {
        rajoutLie = "&id=" + request.getParameter("iddemande");
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>");</script>
    <%
    } else if (classe.compareToIgnoreCase("mg.cnaps.st.StDemandeFounitureFille") == 0) {
        rajoutLie = "&id=" + request.getParameter("iddemande");
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>");</script>
    <%
            } //                StDemandeApproFille
            else {
                for (int o = 0; o < rajoutLien.length; o++) {
                    String valeur = request.getParameter(rajoutLien[o]);
                    if (classe.compareToIgnoreCase("mg.cnaps.st.StMvtStock") != 0 && classe.compareToIgnoreCase("mg.cnaps.st.StMvtStockFille") != 0) {
                        rajoutLie = rajoutLie + "&" + rajoutLien[o] + "=" + valeur;
                    }
                }
            }
        }
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&valeur=<%=val%>");</script>
    <%

    } catch (Exception e) {
        e.printStackTrace();
    %>

    <script language="JavaScript"> alert('<%=e.getMessage()%>');
        history.back();</script>
        <%
                return;
            }
        %>
</html>
