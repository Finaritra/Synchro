<%-- 
    Document   : as-commande-chef
    Created on : 30 d�c. 2016, 08:34:37
    Author     : Joe
--%>

<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<% 
    CommandeClient lv = new CommandeClient();
    
    String nomTable = "as_commandeclient_libencours";
    if(request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("") != 0)nomTable = request.getParameter("etat");
    lv.setNomTable(nomTable);
    
    String listeCrt[] = {"datecommande", "client", "responsable", "typecommande", "dateliv", "adresseliv"};
    String listeInt[] = {"datecommande", "dateliv"};
    String libEntete[] = {"id", "numcommande", "datecommande", "client", "typecommande", "responsable", "dateliv", "adresseliv", "heureliv","remarque"};

    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 10);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    affichage.Champ[] liste = new affichage.Champ[1];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("as_typecommande");
    liste[0] = new Liste("typecommande", ou, "VAL", "VAL");

    pr.getFormu().changerEnChamp(liste);
    pr.getFormu().getChamp("datecommande1").setDefaut(Utilitaire.dateDuJour());
    pr.getFormu().getChamp("datecommande2").setDefaut(Utilitaire.dateDuJour());

    /*
    pr.getFormu().getChamp("groupee").setLibelleAffiche("Groupe");
    pr.getFormu().getChamp("typearticle").setLibelleAffiche("Type d'article");
    */
    pr.setApres("commande/as-commande-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    
    CommandeClient[] liste = (CommandeClient[])pr.getRs().getResultat();
%>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Commande</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-xs-2">
                        <label for="numerovisite">Numero visite</label>
                    </div>
                    <div class="col-xs-4">
                        <input type="text" name="numerovisite" readonly="true" id="numerovisite" placeholder=""  class="form-control" value ="<%=Utilitaire.getMaxSeq("getSeqMedVisite", c)%>" />
                    </div>
                    <div class="col-xs-2">
                        <label for="matricule">Matricule</label>
                    </div>
                    <div class="col-xs-3">
                        <input type="text" name="matricule" id="matricule" placeholder="" readonly=""class="form-control" />
                    </div>
                    <div class="col-xs-1">
                        <button class="btn btn-default btn-sm" type="button" onclick="pagePopUp('choix/logPersonnelChoixMultiple.jsp?champReturn=matricule;nomagent;ddn;age;sexe;idagent')">...</button>
                    </div>
                </div>
                <div class="box-body">
                    
                    <div class="col-xs-2">
                        <label for="nomagent">Nom du patient</label>
                    </div>
                    <div class="col-xs-4">
                        <input type="text" name="nomagent" id="nomagent" placeholder="" class="form-control col-md-4" readonly />
                    </div>
                    <div class="col-xs-2">
                        <label for="ddn">Date naissance</label>
                    </div>
                    <div class="col-xs-4">
                        <input type="text" name="ddn" id="ddn" placeholder="" class="form-control col-md-4" readonly/>
                    </div>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-3">
                        <label for="situationmatrimoniale">Livraison</label>
                    </div>
                    <div class="col-xs-3">
                        <select name="situationmatrimoniale" class="form-control">
                            
                            <option value=""></option>
                            
                        </select>
                    </div>
                </div>
                <input type="hidden" name="idagent" id="idagent">
            </div>
        </div>
    </div>
</div>