<%@page import="bean.*"%>
<%@page import="utilitaire.ConstanteEtat"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsSauce"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsAccompagne"%>
<%@page import="java.util.Map"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="java.util.HashMap"%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetails"%>
<%@page import="mg.allosakafo.commande.*"%>

<%
    /*if(session.getAttribute("tableClient") == null) {
        out.println("<script language='JavaScript'> document.location.replace('/index.jsp'); </script>");
        return;
    }
    String date = request.getParameter("date");
    String idTable=(String)session.getAttribute("tableClient");
    
    
    
    HashMap<String,Object> liste=AlloSakafoService.getListeCommandeEffectuees(idTable,date);
    HashMap<String,Object[]> listeCommande=(HashMap<String,Object[]>)liste.get("commande");
    HashMap<String,Object[]> listeDetailsCommande=(HashMap<String,Object[]>)liste.get("listedetails");
    HashMap<String,Object[]> listePhotos=(HashMap<String,Object[]>)liste.get("listePhotos");
    String somme = (String)liste.get("total");*/
    CommandeClientDetailsTypeP lv = new CommandeClientDetailsTypeP();
    lv.setNomTable("vue_cmd_addition_livraison");
	
    String listeCrt[] = {"id", "produit", "datecommande", "nomtable", "restaurant" ,"heureliv" };
    String listeInt[] = null;
    String libEntete[] = { "id","quantite" ,"produit", "acco_sauce", "nomtable", "heureliv", "etat"};
        
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, 6);
    pr.getFormu().getChamp("nomtable").setValeur((String)session.getAttribute("tableClient"));
    pr.getFormu().getChamp("datecommande").setValeur(utilitaire.Utilitaire.dateDuJour());
    
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
   
    
//    affichage.Champ[] listeDir = new affichage.Champ[1];
//    Direction dir = new Direction();
//    dir.setNomTable("restaurant");
//    listeDir[0] = new Liste("restaurant",dir,"libelledir","iddir");
//    pr.getFormu().changerEnChamp(listeDir);
//        

    String apreswhere = "";
    if(request.getParameter("datecommande") == null || request.getParameter("datecommande") == null){
        apreswhere = "and ( datecommande = '"+ utilitaire.Utilitaire.ajoutJourDateString( utilitaire.Utilitaire.dateDuJourSql() , -1)+"' or datecommande ='"+utilitaire.Utilitaire.dateDuJour() +"'  )";
    }
    if(request.getParameter("restaurant") == null){
        apreswhere += " and nomtable = '"+(String)session.getAttribute("tableClient")+"'";
    }
    System.out.println("Awhere-->" + apreswhere);
    pr.setAWhere(apreswhere);
    
    //pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());

    String[] colSomme = {"montant"};
    pr.setNpp(-1);
    pr.creerObjetPage(libEntete, colSomme);
    ResultatEtSomme rs=pr.getRs();
    CommandeClientDetailsTypeP[]listeComande=(CommandeClientDetailsTypeP[])(rs.getResultat());
    double somme= rs.getSommeEtNombre()[0]  ;
%>

<div class="row">
    <div class="col-md-12 col-xs-12 content-box">
        <div class="header-title header-content bon-plan header-content-title">
            <span class="header-gamepub-logo header-content-logo">
                <span class="glyphicon glyphicon-list-alt"></span>
            </span>
            <span class="header-content-text">Liste des commandes effectuées </span>
            <span class="header-content-text pull-right">Total : <%=Utilitaire.formaterAr(somme)%>   </span>
        </div>

        <div class="col-xs-12 content-body">
            <div class="col-xs-12">
                <form class="form" action="home.jsp" method="GET" autocomplete="off"> 
                    <div class="col-sm-4 col-xs-4">
                        <div class="input-group form-input-label-mobile">
                            <div class="input-group-addon"><strong>Date : </strong></div>
                            <input name="date" type="date" class="form-control" id="date" value="<%=Utilitaire.dateDuJourSql()%>" tabindex="8">
                            <input type="hidden" name="but" value="pages/commande/listeCommandesEffectuees.jsp">
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Afficher</button>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 content-body-produit">
                <form action="ProduitAchat" id="produitAchat" method="POST">
                    <div class="col-xs-12 content-jeton-list-tab">
                        <table class="table table-hover">
                            <tbody>
                            <div id="accordion">

                                <div class="card">
                                    <div class="card-body">
                                        <div class="content_match content_match_nopointer col-xs-12">
                                            <% if(listeComande!=null) { %>
                                            <div class="content_match-list col-xs-12">
                                                <div class="content_match-infos col-xs-4 col-sm-12">
                                                    <div class="col-xs-12 content-match-score-all">
                                                        <p class="col-xs-2 achatLibelle">Produit</p>
                                                        <p class="col-xs-2 achatLibelle">Sauce et accompagnement</p>
                                                        <p class="col-xs-2 achatLibelle">Prix et Qte</p>
                                                        <p class="col-xs-2 achatLibelle" align="right">Montant</p>
                                                        <p class="col-xs-2 achatLibelle">Date & Heure</p>
                                                        <p class="col-xs-2 achatLibelle">Etat</p>
                                                    </div>
                                                    <% 
                                                        for(int j=0;j<listeComande.length;j++){
                                                    %>
                                                    <div class="col-xs-12 content-match-score-all" style="font-size: 14px;">
                                                        <p class="col-xs-2"><%=listeComande[j].getProduit() %> </p>
                                                        <p class="col-xs-2"><%=Utilitaire.champNull(listeComande[j].getAcco_sauce()) %></p>
                                                        <p class="col-xs-2"><%=Utilitaire.formaterAr(listeComande[j].getPu()) %> * <%=listeComande[j].getQuantite() %></p>
                                                        <p class="col-xs-2" align="right"> <%=Utilitaire.formaterAr(listeComande[j].getMontant()) %> </p>
                                                        <p class="col-xs-2"><%=Utilitaire.formatterDaty(listeComande[j].getDatecommande()) +" " + listeComande[j].getHeureliv()%></p>
                                                        <p class="col-xs-2"><%= listeComande[j].getEtat() %></p>
                                                    </div>
                                                    <%  
                                                        }
                                                        if(session.getAttribute("typeLivraison") != null ){
                                                    %>
                                                    
                                                    <% } %>
                                                </div>
                                            </div>
                                            <% } %>
                                        </div>
                                            <span class="header-content-text pull-right"><b>Total : <%=Utilitaire.formaterAr(somme)%></b>   </span>
                                    </div>
                                </div>

                            </div>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>