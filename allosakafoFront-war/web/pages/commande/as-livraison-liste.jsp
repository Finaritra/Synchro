<%-- 
    Document   : as-livraison-liste
    Created on : 1 d�c. 2016, 14:21:35
    Author     : Joe
--%>
<%@page import="mg.allosakafo.commande.Livraisondetail"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="user.UserEJB"%>

<% 
    Livraisondetail lv = new Livraisondetail();
    
    String listeCrt[] = {"id", "produit","heureCommande","heureLivraison","dateCommande","dateLivraison"};
    String listeInt[] = {"dateCommande","dateLivraison"};
    String libEntete[] = {"id", "nomtable", "produit", "sauceAcoompagnement", "dateCommande", "heureCommande", "dateLivraison", "heureLivraison","difference"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    pr.getFormu().getChamp("heureCommande").setLibelle("Heure Commande");
    pr.getFormu().getChamp("heureLivraison").setLibelle("Heure Livraison");
    //pr.getFormu().getChamp("dateCommande").setLibelle("Date Commande");
    //pr.getFormu().getChamp("dateLivraison").setLibelle("Date Livraison");
    
    pr.setApres("commande/as-livraison-liste.jsp");
    String[] colSomme = {};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste livraison</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-livraison-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Table", "Produit", "Sauce Acoompagnement", "Date Commande", "Heure Commande", "Date Livraison", "Heure Livraison","Difference"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>