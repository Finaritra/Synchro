<%-- 
    Document   : as-commande-liste
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="bean.CGenUtil"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<%
    try {

        CommandeMobileEtat lv = new CommandeMobileEtat();
        lv.setNomTable("AS_COMMANDEMOBILE_LIBELLE2");

        String listeCrt[] = {"numcommande", "datecommande", "tables"};
        String listeInt[] = {"datecommande", "etat"};
        String libEntete[] = {"id", "tables", "responsable", "typecommande", "remarque", "numcommande", "datesaisie", "datecommande", "dateliv", "etat"};

        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));

        affichage.Champ[] liste = new affichage.Champ[1];

        TypeObjet ou = new TypeObjet();
        ou.setNomTable("as_typecommande");
        liste[0] = new Liste("typecommande", ou, "VAL", "VAL");

        pr.setApres("commande/as-commande-mobile-liste.jsp");

        String[] colSomme = {};
        pr.creerObjetPage(libEntete, colSomme);

%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande</h1>
    </section>

    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-commande-liste.jsp" method="post" name="incident" id="incident">
            <%
                String lienTableau[] = {pr.getLien() + "?but=commande/as-commande-mobile-fiche.jsp"};
                String colonneLien[] = {"id"};
                pr.getTableau().setLien(lienTableau);
                pr.getTableau().setColonneLien(colonneLien);
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%
            String libEnteteAffiche[] = {"id", "tables", "responsable", "typecommande", "remarque", "numcommande", "datesaisie", "datecommande", "dateliv", "etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>    


</div>

<%    } catch (Exception e) {
        e.printStackTrace();
    }
%>
