<%-- 
    Document   : as-commande-fiche
    Created on : 1 d�c. 2016, 09:52:34
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetails"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsLibelle"%>
<%
    CommandeClient dma = new CommandeClient();
    double total = 0.0;
    dma.setNomTable("as_commandepoint_libelle");
    PageConsulte pc = new PageConsulte(dma, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin

	pc.getChampByName("numcommande").setLibelle("N� commande");
	pc.getChampByName("datesaisie").setLibelle("Date de saisie");
	pc.getChampByName("datecommande").setLibelle("Date de commande");
	pc.getChampByName("typecommande").setLibelle("Type de commande");
	pc.getChampByName("adresseliv").setLibelle("Adresse de livraison");
	pc.getChampByName("heureliv").setLibelle("Heure de livraison");
	pc.getChampByName("dateliv").setLibelle("Date de livraison");
        pc.getChampByName("client").setLibelle("Point");
	
    pc.setTitre("Fiche commande");

    CommandeClient bondecommande = (CommandeClient) pc.getBase();

%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=commande/commandepoint-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
							<% if (bondecommande.getEtat() > ConstanteEtat.getEtatAnnuler() && bondecommande.getEtat() < ConstanteEtat.getEtatPaye()){ %>
							<a class="btn btn-primary pull-right"  href="#" onclick="pagePopUp('commande/fiche-commandepoint-chef.jsp?id=<%=request.getParameter("id")%>')" style="margin-right: 10px">Afficher</a>
							<% } %>
							<% if (bondecommande.getEtat() == ConstanteEtat.getEtatCreer()){ %>
								<a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=commande/as-commandefille-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Ajouter produits</a>
								<a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=commande/commandepoint-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
								<a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annuler&bute=commande/commandepoint-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&nomtable=as_commandepoint&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
								<a class="btn btn-primary pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=fait&bute=commande/commandepoint-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&nomtable=as_commandepoint&id=" + request.getParameter("id")%>" style="margin-right: 10px">Pr�t � livrer</a>
							<% } 
								else if (bondecommande.getEtat() == ConstanteEtat.getEtatFait()){ %>
                                                                <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=commande/commandepoint-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
								<a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annuler&bute=commande/commandepoint-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&nomtable=as_commandepoint&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
								<a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=commande/as-livraison-saisie.jsp&idCommande=" + request.getParameter("id")%>&nomtable=as_commandepoint" style="margin-right: 10px">Livrer</a>
							<% } 
							else if (bondecommande.getEtat() == ConstanteEtat.getEtatLivraison()){ %>
								<a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=commande/as-paiement-saisie.jsp&idCommande=" + request.getParameter("id")%>&nomtable=as_commandepoint" target="_blank" style="margin-right: 10px">Payer</a>
								<a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=facture/factureclient-saisie.jsp&idCommande=" + request.getParameter("id")%>&nomtable=as_commandepoint" target="_blank" style="margin-right: 10px">Editer Facture Client</a>
                                                                <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annuler&bute=commande/commandepoint-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&nomtable=as_commandepoint&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>

                            <% } %>
							
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">D&eacute;tails commande</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Produits</th>
                                    <th>Quantit&eacute;</th>
                                    <th>P.U</th>
                                    <th>Remise</th>
                                    <th>Montant</th>
                                    <th>Observation</th>
                                </tr>
                            </thead>

                            <tbody>
                                <%
                                    CommandeClientDetailsLibelle p = new CommandeClientDetailsLibelle();
                                    p.setNomTable("as_details_commande");
                                    CommandeClientDetailsLibelle[] liste = (CommandeClientDetailsLibelle[]) CGenUtil.rechercher(p, null, null, " and idmere = '" + request.getParameter("id") + "'");

                                    //private String id, idmere, produit, observation; private double quantite, pu, remise;
                                    for (int i = 0; i < liste.length; i++) {
                                        double remise = ((liste[i].getQuantite() * liste[i].getPu()) * liste[i].getRemise()) / 100;
                                %>
                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                    <td  align="center"><%=liste[i].getIdproduit()%></td>
                                    <td  align="center"><%=liste[i].getProduit()%></td>
                                    <td  align="center"><%=Utilitaire.formaterAr(liste[i].getQuantite())%></td>
                                    <td  align="right"><%=Utilitaire.formaterAr(liste[i].getPu())%></td>
                                    <td  align="right"><%=Utilitaire.formaterAr(remise)%></td>
                                    <td  align="right"><%=Utilitaire.formaterAr((liste[i].getQuantite() * liste[i].getPu()) - remise)%></td>
                                    <td  align="center"><%=Utilitaire.champNull(liste[i].getObservation())%></td>
                                </tr>
                                <%
                                        total += ((liste[i].getQuantite() * liste[i].getPu()) - remise);
                                    }
                                %>

                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                    <td  align="center"></td>
                                    <td  align="center"></td>
                                    <td  align="right"></td>
									<td  align="center"></td>
                                    <td  align="right">Total</td>
                                    <td  align="right"><%=Utilitaire.formaterAr(total)%></td>
                                    <td  align="center"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
