<%-- 
    Document   : as-commande-analyse
    Created on : 30 d�c. 2016, 04:57:15
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@page import="affichage.Liste"%>
<%@ page import="bean.*" %>
<%@page import="bean.TypeObjet"%>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.commande.AnalyseCommande"%>
<%@ page import="affichage.*" %>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

<%
    AnalyseCommande mvt = new AnalyseCommande();
	String nomTable = "as_analysecommande_details";
	if(request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0){
		nomTable = request.getParameter("table");
	}
	mvt.setNomTable(nomTable);
    String listeCrt[] = {"datecommande", "dateliv", "client", "produit", "remarque", "heureliv", "numcommande", "adresseliv", "etat","typeproduit", "secteur"};
    String libEntete[] = {"Date de commande", "Date de livarison", "Client", "Produit","Remarque","Heure de livraison","Numero de commande","Adresse de livraison", "Etat"};
    String listeInt[] = {"datecommande", "dateliv"};
    String[] pourcentage = {"nombrepargroupe"};
    String colDefaut[] = {"datecommande", "dateliv", "client", "produit", "remarque", "heureliv", "numcommande", "adresseliv", "etat","typeproduit", "secteur"}; 
    String somDefaut[] = {"quantite", "montant"};
    
    PageRechercheGroupe pr = new PageRechercheGroupe(mvt, request, listeCrt, listeInt, 3,colDefaut, somDefaut, pourcentage, 9, 2);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    affichage.Champ[] liste = new affichage.Champ[2];
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("as_secteur");
    liste[0] = new Liste("secteur", ou, "VAL", "VAL");
    
    TypeObjet ou1 = new TypeObjet();
    ou1.setNomTable("as_typeproduit");
    liste[1] = new Liste("typeproduit", ou1, "VAL", "VAL");
	
    pr.getFormu().changerEnChamp(liste);
    
    pr.setApres("commande/as-commande-analyse.jsp");
    pr.creerObjetPagePourc();
%>
<script>
    function changerDesignation() {
        document.analyse.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Analyse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-commande-analyse.jsp" method="post" name="analyse" id="analyse">
            <%out.println(pr.getFormu().getHtmlEnsemble());%>
			<div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    table : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_analysecommande_details") == 0) {%>    
                        <option value="as_analysecommande_details" selected>Tous</option>
                        <% } else { %>
                        <option value="as_analysecommande_details" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_analysecommande_libfait") == 0) {%>    
                        <option value="as_analysecommande_libfait" selected>Pr�t � livrer</option>
                        <% } else { %>
                        <option value="as_analysecommande_libfait" >Pr�t � livrer</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_analysecommande_libencours") == 0) {%>
                        <option value="as_analysecommande_libencours" selected>Encours</option>
                        <% } else { %>	
                        <option value="as_analysecommande_libencours">Encours</option>
                        <% } %>

                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_analysecommande_libvnp") == 0) {%>
                        <option value="as_analysecommande_libvnp" selected>Livr�e non pay�</option>
                        <% } else { %>
                        <option value="as_analysecommande_libvnp">Livr�e non pay�</option>
                        <% } %>
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_analysecommande_libvp") == 0) {%>
                        <option value="as_analysecommande_libvp" selected>Pay�</option>
                        <% } else { %>
                        <option value="as_analysecommande_libvp">Pay�</option>
                        <% } %>
						
						<% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_analysecommande_libannule") == 0) {%>
                        <option value="as_analysecommande_libannule" selected>Annul�</option>
                        <% } else { %>
                        <option value="as_analysecommande_libannule">Annul�</option>
                        <% } %>

                    </select>
                </div>
                <div class="col-md-4"></div>
            </div>
        </form>
        <%
           out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>