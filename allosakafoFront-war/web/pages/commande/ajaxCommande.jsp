<%@page import="user.UserEJB"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="mg.allosakafo.secteur.TarifLivraison"%>
<%@page import="mg.allosakafo.tiers.ClientAlloSakafo"%>
<%


	UserEJB u = (UserEJB) session.getAttribute("u");;
    String acte =request.getParameter("acte");
	
	ClientAlloSakafo clientAS = null;
	TarifLivraison tarif = null;
	String remarque = "";
	String adresse = "";
	boolean douteux = false;
	String quartier = "";
	String idquartier = "";
	String secteur = "";
	String montant = "";
    if (acte.compareToIgnoreCase("searchClient") == 0) { 
		clientAS = u.searchClientComplet(request.getParameter("numero"));
		if(clientAS != null){
			remarque = clientAS.getNom();
			douteux = clientAS.estDouteux();
			adresse = Utilitaire.champNull(clientAS.getAdresse());
		}
	}
	if (acte.compareToIgnoreCase("searchLieu") == 0) { 
		tarif =	u.searchTarifByAdresse(request.getParameter("adresse"));
		if (tarif != null){
			idquartier = Utilitaire.champNull(tarif.getId());
			quartier = Utilitaire.champNull(tarif.getQuartier());
			secteur = Utilitaire.champNull(tarif.getObservation());
			montant = Utilitaire.formaterAr(tarif.getMontant());
		}
	}
%>
<p id='douteux'><%=douteux%></p>
<p id='client'><%=remarque%></p>
<p id='adresseliv'><%=adresse%></p>
<p id='idquartier'><%=idquartier%></p>
<p id='quartier'><%=quartier%></p>
<p id='secteur'><%=secteur%></p>
<p id='montant'><%=montant%></p>
