<%-- 
    Document   : as-livraison-saisie
    Created on : 1 d�c. 2016, 14:21:14
    Author     : Joe
--%>
<%@page import="mg.allosakafo.commande.Livraison"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.commande.CommandeService"%>
<%@page import="mg.allosakafo.tiers.Vehicule"%>
<%@page import="user.*"%> 
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*"%>
<%@page import="utilitaire.*"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    Livraison  a = new Livraison();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));    
    
    affichage.Champ[] liste = new affichage.Champ[2];
    
    Vehicule op = new Vehicule();
    op.setNomTable("AS_VEHICULE");
    liste[0] = new Liste("cuisinier", op, "nom", "id");
    TypeObjet d2 = new TypeObjet();
    d2.setNomTable("as_secteur");
    liste[1] = new Liste("secteur", d2, "val", "id");
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("daty").setLibelle("Date");
	pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("etat").setVisible(false);
    pi.getFormu().getChamp("commande").setPageAppel("choixMultiple/listeCommandeChoixMultiple.jsp");
    pi.getFormu().getChamp("commande").setAutre("readonly='true'");
    pi.getFormu().getChamp("responsable").setAutre("readonly='true'");
    pi.getFormu().getChamp("cuisinier").setAutre("readonly='true'");
    pi.getFormu().getChamp("cuisinier").setLibelle("Coursier");
    pi.getFormu().getChamp("responsable").setPageAppel("choix/listeResponsableChoix.jsp");
    //pi.getFormu().getChamp("cuisinier").setPageAppel("choix/listeVehiculeChoix.jsp");
	pi.getFormu().getChamp("description").setLibelle("Description");
	String nomtable="";
    if(request.getParameter("nomtable") != null && request.getParameter("nomtable").compareToIgnoreCase("")!=0)
    {
        nomtable=request.getParameter("nomtable");
    }
    if(request.getParameter("idCommande") != null && request.getParameter("idCommande").compareToIgnoreCase("") != 0){
        pi.getFormu().getChamp("commande").setDefaut(request.getParameter("idCommande"));
        
        CommandeClient cmc = CommandeService.getInfoCommandeClient(request.getParameter("idCommande"),nomtable);
        pi.getFormu().getChamp("responsable").setDefaut(cmc.getResponsable());
        pi.getFormu().getChamp("heure").setDefaut(cmc.getHeureliv()+"");
        pi.getFormu().getChamp("adresse").setDefaut(cmc.getAdresseliv());
		pi.getFormu().getChamp("secteur").setDefaut(cmc.getSecteur());
        pi.getFormu().getChamp("distance").setDefaut(cmc.getDistance()+"");
        pi.getFormu().getChamp("daty").setDefaut(Utilitaire.formatterDaty(cmc.getDateliv()));
    } 
	
	if(request.getParameter("acte") != null && request.getParameter("acte").compareToIgnoreCase("attacher") == 0){
		String[] cmdLiv = request.getParameterValues("id");
		String lstLiv = Utilitaire.tabToString(cmdLiv, "", ";");
		pi.getFormu().getChamp("commande").setDefaut(lstLiv);
		CommandeClient cmc = CommandeService.getInfoCommandeClient(cmdLiv[0],nomtable);
        pi.getFormu().getChamp("responsable").setDefaut(cmc.getResponsable());
        pi.getFormu().getChamp("heure").setDefaut(cmc.getHeureliv()+"");
        pi.getFormu().getChamp("adresse").setDefaut(cmc.getAdresseliv());
		pi.getFormu().getChamp("secteur").setDefaut(cmc.getSecteur());
        pi.getFormu().getChamp("distance").setDefaut(cmc.getDistance()+"");
        pi.getFormu().getChamp("daty").setDefaut(Utilitaire.formatterDaty(cmc.getDateliv()));
		
	}
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Enregistrer livraison</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=commande/apresCommande.jsp" method="post" name="starticle" id="starticle">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insertLivraison">
    <input name="bute" type="hidden" id="bute" value="commande/as-livraison-fiche.jsp">
    <input name="rajoutLien" type="hidden" id="rajoutLien" value="id">
    <input name="nomTableC" type="hidden" id="nomTableC" value="<%=nomtable%>">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.commande.Livraison">
    </form>
</div>