<%-- 
    Document   : as-commande-liste
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<% 
    CommandeClientEtat lv = new CommandeClientEtat();
    
    String nomTable = "AS_COMMANDEPOINT_LIBELLE2";
    if(request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0){
		nomTable = request.getParameter("table");
	}
    lv.setNomTable(nomTable);
	
    String listeCrt[] = {"id","datecommande", "client", "responsable", "typecommande", "dateliv", "adresseliv", "secteur", "remarque", "coursier"};
    String listeInt[] = {"datecommande", "dateliv"};
    String libEntete[] = {"id", "datecommande", "client", "typecommande", "responsable", "dateliv", "adresseliv","secteur", "heureliv", "remarque", "coursier", "montant", "etat"};

    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, 13);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    affichage.Champ[] liste = new affichage.Champ[2];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("as_typecommande");
    liste[0] = new Liste("typecommande", ou, "VAL", "VAL");
	
	TypeObjet ou1 = new TypeObjet();
    ou1.setNomTable("as_secteur");
    liste[1] = new Liste("secteur", ou1, "VAL", "VAL");
    
    TypeObjet ou2 = new TypeObjet();
    ou2.setNomTable("point");
    liste[1] = new Liste("client", ou2, "VAL", "desce");
	
    pr.getFormu().changerEnChamp(liste);
    
	pr.getFormu().getChamp("client").setLibelle("Point");
    //pr.getFormu().getChamp("datecommande2").setDefaut(Utilitaire.dateDuJour());

    
    pr.setApres("commande/commandepoint-liste.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/commandepoint-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeclient_libelle2") == 0) {%>    
                        <option value="as_commandepoint_libelle2" selected>Tous</option>
                        <% } else { %>
                        <option value="as_commandepoint_libelle2" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeclient_libfait") == 0) {%>    
                        <option value="as_commandepoint_libfait" selected>Pr�t � livrer</option>
                        <% } else { %>
                        <option value="as_commandeclient_libfait" >Pr�t � livrer</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeclient_libencours") == 0) {%>
                        <option value="AS_COMMANDEPOINT_LIBENCOURS" selected>Encours</option>
                        <% } else { %>
                        <option value="AS_COMMANDEPOINT_LIBENCOURS">Encours</option>
                        <% } %>

                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeclient_livnp") == 0) {%>
                        <option value="AS_COMMANDEPOINT_LIVNP" selected>livr�e non pay�</option>
                        <% } else { %>
                        <option value="AS_COMMANDEPOINT_LIVNP">livr�e non pay�</option>
                        <% } %>
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeclient_livp") == 0) {%>
                        <option value="AS_COMMANDEPOINT_LIVP" selected>Pay�</option>
                        <% } else { %>
                        <option value="AS_COMMANDEPOINT_LIVP">Pay�</option>
                        <% } %>
						
						<% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeclient_libannule") == 0) {%>
                        <option value="AS_COMMANDEPOINT_LIBANNULE" selected>Annul�</option>
                        <% } else { %>
                        <option value="AS_COMMANDEPOINT_LIBANNULE">Annul�</option>
                        <% } %>
                    </select>
                </div>
            </div>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=commande/commandepoint-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date de commande", "Point", "Type commande", "R�sponsable", "Date de livraison", "Adresse de livraison", "Secteur", "Heure", "Remarque", "Coursier", "Montant", "Etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
			if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandepoint_libfait") == 0) {    
        %>
		<form method="post" name="e" action="<%=pr.getLien()%>?but=commande/as-livraison-saisie.jsp">
		<%
				out.println(pr.getTableau().getHtmlWithCheckbox());
		%>
				<input type="hidden" name="acte" value="livraison-multiple">
		</form>
		<%
            } 
			else{
				out.println(pr.getTableau().getHtml());
				out.println(pr.getBasPage());
			}
        %>
    </section>
</div>