<%-- 
    Document   : listecommande
    Created on : 19 sept. 2019, 12:31:54
    Author     : Jessi
--%>

<%@page import="bean.CGenUtil"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="mg.allosakafo.commande.CommandeService"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsAccompagne"%>
<%@page import="mg.allosakafo.produits.AccompagnementSauce"%>
<% 
    CommandeService cm = new CommandeService();
     String idclient = "";
    String idResa=null;
    if(session.getAttribute("tableClient")!=null){
        idclient = (String)request.getSession().getAttribute("tableClient");
    }
    if(session.getAttribute("idResa")!=null)idResa=session.getAttribute("idResa").toString();
    
   CommandeClientDetailsAccompagne[] commandes = cm.getCommandeType(idResa ,idclient,null);
   //CommandeClientDetailsAccompagne[] commandes=((CommandeClientDetailsAccompagne[])CGenUtil.rechercher(new CommandeClientDetailsAccompagne(), null, null,null, " and etat > 0 and etat< 8 and idclient='"+idclient+"'"));
   double total = 0;
   String idcommande="";
    AccompagnementSauce acc = new AccompagnementSauce();
    acc.setNomTable("AS_ACCOMPAGNEMENT_SAUCE_LIB");
   AccompagnementSauce[] sauce = (AccompagnementSauce[])bean.CGenUtil.rechercher(acc, null, null, "");
%>

<%if(commandes!=null && commandes.length>0){%>
<div class="col-md-12 col-xs-12 content-box">
        <div class="header-title header-content bon-plan header-content-title">
            <span class="header-gamepub-logo header-content-logo">
                <span class="glyphicon glyphicon-shopping-cart"></span>
            </span>
            Votre commande
        </div>
        <div class="col-xs-12 content-body">
            <table class="table table-hover" style="font-size: 18px">
                
                <thead>
                    <tr>

                        <th data-field="id" data-align="right" data-sortable="true">Produit</th>
                         <th data-field="name" data-align="center" data-sortable="true">Sauce et accompagnement</th>
                        <th data-field="name" data-align="center" data-sortable="true">Quantit�</th>
                        <th data-field="name" data-align="center" data-sortable="true">Prix Unitaire</th>
                        <th data-field="name" data-align="center" data-sortable="true">Montant</th>
                    </tr>
                </thead>
                <tbody>
                   <% for(int i=0;i<commandes.length;i++){
                      total+= commandes[i].getPu()*commandes[i].getQuantite();
                      idcommande+=commandes[i].getIdmere()+";";
                   %> 
                    <tr>
                        <td><%=commandes[i].getProduit() %></td>
                        <td><%=Utilitaire.champNull(commandes[i].getIdaccompagnement()) %></td>
                        <td><%=commandes[i].getQuantite() %></td>
                        <td><%=utilitaire.Utilitaire.formaterAr(commandes[i].getPu()) %> </td>
                        <td><%=utilitaire.Utilitaire.formaterAr(commandes[i].getQuantite()*commandes[i].getPu()) %> </td>
                        <td><a href="Plat?mode=delete&id=<%=commandes[i].getId() %>" class="btn btn-warning warning"><span class="glyphicon glyphicon-remove"> </span> </a></td>
                        <!--<td><a class="btn btn-info info" data-toggle="modal" data-target="#Modal<%=i%>"><span class="glyphicon glyphicon-edit"> </span> </a></td>-->
                    </tr>
                    <% } %>
                    
                     
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>TOTAL</strong> </td>
                        <td> <%=utilitaire.Utilitaire.formaterAr(total) %></td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td>
                            <!--<input type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal2" value="Payer maintenant!"/>-->
                            <%if(commandes!=null && commandes.length!=0){%>
                            <a class="btn btn-success" href="Plat?mode=cloturer&id=<%=idcommande%>">Cloturer</a>
                            <% } %>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <%if(commandes!=null && commandes.length!=0){%>
                            <a class="btn btn-danger" href="Plat?mode=cloturerPayer&id=<%=idcommande%>">Cloturer et PAYER</a>
                            <% } %>
                        </td>
                    </tr>
                </tfoot>


                
            </table>
        </div>
   
</div>
<% } %>
      <%  for(int i=0;i<commandes.length;i++){%> 
                         <div class="modal fade modal-z-index" id="Modal<%=i%>" tabindex="-1" role="dialog" aria-labelledby="ModalLabel2" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content" id="modal-content2">
                                        <form action="Plat" method="GET" >
                                            <div class="modal-header" style="height: 43px;">
                                                <div class="header-title header-content">
                                                    <span class="header-gamepub-logo">
                                                        <span class="glyphicon glyphicon-shopping-cart"></span>
                                                    </span>
                                                    AJOUTER AU COMMANDE
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-xs-6" style="margin-top: 27px;">
                                                        <div class="box-img">
                                                            <img class="box-product-img box-product-list-img" src="${pageContext.request.contextPath}/assets/img/plats/<%=commandes[i].getListePhoto()[0]%>" height="150"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6" style="margin-top: 27px;">
                                                        <h4><%=commandes[i].getProduit()%></h4>
                                                         <SELECT name="idsauce" class="form-control" style="margin-bottom: 10px">
                                                             <%for(int index=0;index<sauce.length;index++){%> 
                                                                <OPTION value="<%=sauce[index].getId() %>"><%=sauce[index].getIdaccompagnement()+" "+sauce[index].getIdsauce()%></OPTION>
                                                                <% }%>
                                                        </SELECT>
                                                        <h4>Quantit� : </h4>
                                                        <div class="input-group">
                                                            <div id="updown" style="margin-left: -126px;">
                                                                <script language="JavaScript">
                                                                    var v = document.URL;
                                                                    var lien = v.replace('produit_home', 'panier');
                                                                    document.write("<input type=\"hidden\" value=\"" + lien + "\" id=\"lien\" name=\"lien\"/>");
                                                                </script>
                                                                <input type="hidden" value="<%=commandes[i].getId()%>" id="id" name="id"/>
                                                                <input type="number" name="qt" class="form-control" placeholder="quantite" min="1" step="1" value="1" id="2qteprod<%=i%>" style="width: 70px; margin-left: 126px; padding-left: 24px; -webkit-appearance: none;-moz-appearance:textfield;" onchange="qteprod2(<%=i%>)"/>
                                                                <input type="hidden" value="<%=commandes[i].getPu()%>" id="2pu<%=i%>"/>
                                                                <input type="hidden" value="modif" id="mode"  name="mode"/>
                                                                <div class="input-group-addon" style="height: 34px"><a class="glyphicon glyphicon-chevron-up" onclick="haut2(<%=i%>)"></a></div>
                                                                <div class="input-group-addon"><a class="glyphicon glyphicon-chevron-down" onclick="bas2(<%=i%>)"></a></div>
                                                            </div>
                                                        </div>
                                                        <h4 style="text-decoration: underline">Prix unitaire : <span style="font-size: 17px;">Ar <%=Utilitaire.formaterAr(commandes[i].getPu())%></span></h4>
                                                        <h4>Total : Ar <span id="total<%=i%>"><%=Utilitaire.formaterAr(commandes[i].getPu())%></span></h4>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-xs-6">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">

                                                <input   type="submit" class="btn btn-primary" value="OK"/>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
               </div>
        <% }%>                    
   