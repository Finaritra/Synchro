<%-- 
    Document   : as-commande-modif.jsp
    Created on : 29 d�c. 2016, 19:50:47
    Author     : Joe
--%>
<%@ page import="user.*"%>
<%@ page import="bean.*"%>
<%@ page import="utilitaire.*"%>
<%@ page import="affichage.*"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsLibelle"%>
<%@page import="mg.allosakafo.tiers.Responsable"%>
<%@page import="mg.allosakafo.appel.Appel"%>
<%
    try{
    String autreparsley = "data-parsley-range='[8, 40]' required";
    CommandeClient da = new CommandeClient();
    PageUpdate pi = new PageUpdate(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");

    affichage.Champ[] liste = new affichage.Champ[3];

    TypeObjet d = new TypeObjet();
    d.setNomTable("as_typecommande");
    liste[0] = new Liste("typecommande", d, "val", "id");

    Responsable dd = new Responsable();
    dd.setNomTable("as_responsable");
    liste[1] = new Liste("responsable", dd, "nom", "id");

	TypeObjet d1 = new TypeObjet();
    d1.setNomTable("as_secteur");
    liste[2] = new Liste("secteur", d1, "val", "id");
	
    pi.getFormu().changerEnChamp(liste);

    pi.getFormu().getChamp("etat").setAutre("readonly=true");
    pi.getFormu().getChamp("datesaisie").setLibelle("date de saisie");
    pi.getFormu().getChamp("datecommande").setLibelle("Date de commande");
    pi.getFormu().getChamp("responsable").setLibelle("R�sponsable");
    pi.getFormu().getChamp("numcommande").setLibelle("N� commande");
    pi.getFormu().getChamp("typecommande").setLibelle("Type commande");
    pi.getFormu().getChamp("dateliv").setLibelle("Date de livraison");
    pi.getFormu().getChamp("adresseliv").setLibelle("Adresse de livraison");
    pi.getFormu().getChamp("heureliv").setLibelle("Heure de livraison");
    pi.getFormu().getChamp("distance").setLibelle("Distance");
    pi.getFormu().getChamp("remarque").setType("textarea");

    
    pi.preparerDataFormu();
    
    CommandeClient base = new CommandeClient();
    CommandeClient[] lda = (CommandeClient[])CGenUtil.rechercher(base, null, null, " and ID = '"+request.getParameter("id").trim()+"'");
    
    String typecommande = lda[0].getTypecommande();
    String responsable = lda[0].getResponsable(); 
%>
<div class="content-wrapper">
    <h1 class="box-title">Modification commande</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertCommande();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <div class="box">
                        <div class="box-title with-border">
                            <h1 class="box-title">D&eacute;tails</h1>
                        </div>
                        <div class="box-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th style="background-color:#bed1dd">ID</th>
                                        <th style="background-color:#bed1dd">Article</th>
                                        <th style="background-color:#bed1dd">Quantit&eacute;</th>
                                        <th style="background-color:#bed1dd">P.U</th>
                                        <th style="background-color:#bed1dd">Montant</th>
                                        <th style="background-color:#bed1dd">Remarque</th>
                                    </tr></thead>
                                <tbody>
                                    <%
                                        CommandeClientDetailsLibelle p = new CommandeClientDetailsLibelle();
                                        p.setNomTable("as_details_commande"); // vue
                                        CommandeClientDetailsLibelle[] listef = (CommandeClientDetailsLibelle[]) CGenUtil.rechercher(p, null, null, " and idmere = '"+request.getParameter("id").trim()+"' order by produit asc");

                                        for (int i = 0; i < listef.length; i++) {
                                    %>
                                    <input type="hidden" class="form form-control" name="nb" id="nb" value="<%=listef.length%>">
                                    <tr>
                                        <td><input type="hidden" class="form form-control" name="id<%=i%>" id="id<%=i%>" value="<%=listef[i].getId()%>"><%=listef[i].getId()%></td>
                                        <td><%=listef[i].getProduit()%></td>
                                        <td><input type="text" class="form form-control" name="quantite<%=i%>" id="quantite<%=i%>" value="<%=listef[i].getQuantite()%>" onblur="calculerMontant(<%=i%>)"></td>
                                        <td><input type="text" class="form form-control" name="pu<%=i%>" id="pu<%=i%>" value="<%=listef[i].getPu()%>" onblur="calculerMontant(<%=i%>)"></td>
                                        <td><input type="text" class="form form-control" name="montant<%=i%>" id="montant<%=i%>" value="<%=listef[i].getPu()*listef[i].getQuantite()%>" readonly="readonly"></td>
                                        <td><input type="text" class="form form-control" name="remarque<%=i%>" id="remarque<%=i%>" value="<%=Utilitaire.champNull(listef[i].getObservation())%>"></td>
                                    </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input name="acte" type="hidden" id="acte" value="updatecommande">
        <input name="bute" type="hidden" id="bute" value="commande/as-commande-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.commande.CommandeClient">
        <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;">Enregistrer</button>
        <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;">R&eacute;initialiser</button>
    </form>
</div>
<script language="JavaScript">

    var options = $('#typecommande option');
    for (var co = 0; co < options.size(); co++) {
        if (options[co].value === <%=typecommande%>) {
            $('#typecommande option')[co].selected = true;
        } else {
            $('#typecommande option')[co].selected = false;
        }

    }

    var responsables = $('#responsable option');
    for (var co = 0; co < responsables.size(); co++) {
        if (responsables[co].value === <%=responsable%>) {
            $('#responsable option')[co].selected = true;
        } else {
            $('#responsable option')[co].selected = false;
        }

    }
    
    function calculerMontant(indice) {
        var quantite, pu, montant;
        quantite = parseFloat($('#quantite' + indice).val());
        pu = parseFloat($('#pu' + indice).val());
        if (!isNaN(quantite) && !isNaN(pu)) {
            montant = quantite * pu;
            $('#montant' + indice).val(montant.toFixed(2));
        } else {
            $('#montant' + indice).val('');
        }
    }
</script>
<%} catch(Exception ex){
    ex.printStackTrace();
}%>