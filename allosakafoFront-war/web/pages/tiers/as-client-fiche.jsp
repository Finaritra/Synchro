<%-- 
    Document   : as-client-fiche
    Created on : 1 d�c. 2016, 09:31:11
    Author     : Joe
--%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.tiers.ClientAlloSakafo"%>
<%
    ClientAlloSakafo a = new ClientAlloSakafo();
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));
    
    pc.setTitre("Consultation client");

	ClientAlloSakafo client = (ClientAlloSakafo)pc.getBase();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=tiers/as-client-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
						<% if(!client.estDouteux()){ %>
							<a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=tiers/as-clientdouteux-saisie.jsp&idclient=" + request.getParameter("id")%>" style="margin-right: 10px">Client douteux</a>
						<% } %>
						<a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=tiers/as-client-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>