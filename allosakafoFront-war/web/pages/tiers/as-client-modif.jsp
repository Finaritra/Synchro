<%-- 
    Document   : as-commande-modif.jsp
    Created on : 29 d�c. 2016, 19:50:47
    Author     : Joe
--%>
<%@ page import="user.*"%>
<%@ page import="utilitaire.*"%>
<%@ page import="affichage.*"%>
<%@page import="mg.allosakafo.tiers.ClientAlloSakafo"%>
<%
    try{
    String autreparsley = "data-parsley-range='[8, 40]' required";
    ClientAlloSakafo  a = new ClientAlloSakafo();
    PageUpdate pi = new PageUpdate(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");

    affichage.Champ[] liste = new affichage.Champ[1];
    
    String[] affiche = {"M","F"};
    String[] value = {"M","F"};
    Liste lst = new Liste();
    lst.ajouterValeur(value, affiche);
    liste[0] = lst;
    liste[0].setNom("sexe");
    
    pi.getFormu().changerEnChamp(liste);

    pi.getFormu().getChamp("fb").setLibelle("Facebook");
    pi.getFormu().getChamp("prenom").setLibelle("Pr�nom");
    pi.getFormu().getChamp("datenaissance").setLibelle("Date de naissance");
    pi.getFormu().getChamp("datenaissance").setAutre("placeholder='JJ/MM/AAAA'");

    
    pi.preparerDataFormu();
    
%>
<div class="content-wrapper">
    <h1 class="box-title">Modification client</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro">
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="acte" value="update">
        <input name="bute" type="hidden" id="bute" value="tiers/as-client-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.tiers.ClientAlloSakafo">
    </form>
</div>

<%} catch(Exception ex){
    ex.printStackTrace();
}%>