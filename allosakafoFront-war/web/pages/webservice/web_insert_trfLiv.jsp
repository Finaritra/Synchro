<%-- 
    Document   : web_insert_trfLiv
    Created on : 28 avr. 2017, 00:58:43
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String id = request.getParameter("id");
    String quartier = request.getParameter("q");
    String montant = request.getParameter("m");
    String dateEffectivite = request.getParameter("d");
    String observation = request.getParameter("obs");
    String resultatInsertion = null;
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        resultatInsertion = ExecuteFunction.inserer_TarifLivraison(id, quartier, montant, dateEffectivite, observation);
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>