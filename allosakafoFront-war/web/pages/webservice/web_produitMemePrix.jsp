<%-- 
    Document   : web_produitMemePrix
    Created on : 8 mai 2017, 11:57:21
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    int prix = Integer.parseInt(request.getParameter("prix"));
    int marge = Integer.parseInt(request.getParameter("marge"));
    int limite = Integer.parseInt(request.getParameter("limite"));
    String idProduit = request.getParameter("idProduit");
    JSONArray resultat_final = ExecuteFunction.produitsMemePrix(idProduit,prix,marge,limite);
%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultat_final);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>