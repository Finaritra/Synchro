<%-- 
    Document   : web_select_where2_champ_limite_orderBy
    Created on : 24 avr. 2017, 16:45:44
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String table = request.getParameter("t");
    String champ1 = request.getParameter("c1");
    String valeurChamp1 = request.getParameter("vc1");
    String champ2 = request.getParameter("c2");
    String valeurChamp2 = request.getParameter("vc2");
    String champ_ordre = request.getParameter("co");
    String value_orderBy = request.getParameter("ob");
    int limite = Integer.parseInt(request.getParameter("l"));
    int debut = Integer.parseInt(request.getParameter("d"));
    JSONArray resultat_final = ExecuteFunction.select_listWhere2OrderByLimit(table,champ1,valeurChamp1,champ2,valeurChamp2,champ_ordre,value_orderBy,limite,debut);
%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultat_final);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>
