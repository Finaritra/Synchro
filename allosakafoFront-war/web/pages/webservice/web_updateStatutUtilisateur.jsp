<%-- 
    Document   : web_updateStatutUtilisateur
    Created on : 5 mai 2017, 14:54:44
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String idUtilisateur = request.getParameter("idUtilisateur");
    String statut = request.getParameter("statutUtilisateur");
    String resultatInsertion = null;
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        resultatInsertion = ExecuteFunction.update_statut_utilisateur(idUtilisateur, statut);
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>