<%-- 
    Document   : web_insert_abonnement
    Created on : 2 mai 2017, 16:30:22
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String idVisite = request.getParameter("idVisite");
    String dateVisite = request.getParameter("dateVisite");
    String idSession = request.getParameter("idSession");
    String resultatInsertion = new String();
    try{
        resultatInsertion = ExecuteFunction.inserer_visite(idVisite, dateVisite, idSession);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>
