<%-- 
    Document   : web_insert_util
    Created on : 28 avr. 2017, 16:00:26
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    
    String id = request.getParameter("id");
    String civilite = request.getParameter("civilite");
    String nom = request.getParameter("nom");
    String prenom = request.getParameter("prenom");
    String dateNaissance = request.getParameter("dateNaissance");
    String identifiant = request.getParameter("identifiant");
    String email = request.getParameter("mail");
    String mdp = request.getParameter("mdp");
    String statut = request.getParameter("statut");
    String idSession = request.getParameter("idSession");
    String commentaire = request.getParameter("commentaire");
    String photo = request.getParameter("photo");
    
    String action = request.getParameter("action");
    String resultatInsertion = new String();
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        if(action.compareTo("insert")==0){
            //Insertion
            resultatInsertion = ExecuteFunction.inserer_utilisateur(id, civilite, nom, prenom, dateNaissance, email, identifiant, mdp, statut, idSession, commentaire, photo);
        }else if(action.compareTo("update")==0){
            //Mise à jour
            resultatInsertion = ExecuteFunction.modifier_utilisateur(id, civilite, nom, prenom, dateNaissance, email, identifiant, mdp, statut, idSession, commentaire, photo);
        }
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>