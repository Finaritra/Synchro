<%-- 
    Document   : web_insert_promotionTypeAbonnement
    Created on : 4 mai 2017, 11:09:12
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String idTypeAbonnement = request.getParameter("idTypeAbonnement");
    String idPromotion = request.getParameter("idPromotion");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = ExecuteFunction.inserer_promotionTypeAbonnement(idTypeAbonnement, idPromotion, commentaire);
	
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>