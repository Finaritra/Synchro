<%-- 
    Document   : web_insert_pdt
    Created on : 27 avr. 2017, 13:45:18
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String idTypeAbonnement = request.getParameter("idTypeAbonnement");
    String nbrMois = request.getParameter("nbrMois");
    String commentaire = request.getParameter("commentaire");
    String idSession = request.getParameter("idSession");
    String resultatInsertion = ExecuteFunction.modifier_dureeAbonnement(id, idTypeAbonnement, nbrMois, commentaire, idSession);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>