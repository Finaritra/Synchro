<%-- 
    Document   : web_insert_adresse
    Created on : 26 avr. 2017, 13:05:24
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String quartier = request.getParameter("quart");
    String utilisateur = request.getParameter("util");
    String typeadresse = request.getParameter("tpe");
    String adresse = request.getParameter("add");
    String commentaire = request.getParameter("cmt");
    String resultatInsertion = null;
    try{
        resultatInsertion = ExecuteFunction.inserer_adresse(id, quartier,utilisateur,typeadresse, adresse, commentaire);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>