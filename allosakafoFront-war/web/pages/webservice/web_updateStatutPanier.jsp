<%-- 
    Document   : web_updateStatutPanier
    Created on : 4 mai 2017, 16:13:56
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String table = request.getParameter("table");
    String nomColonne_statut = request.getParameter("nomColonne_statut");
    String nouvelleValeur = request.getParameter("nouvelleValeur");
    String condition_colonne_identification = request.getParameter("condition_colonne_identification");
    String condition_colonne_valeur = request.getParameter("condition_colonne_valeur");
    String resultatInsertion = null;
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        resultatInsertion = ExecuteFunction.updateStatut_panier(table,nomColonne_statut,nouvelleValeur,condition_colonne_identification,condition_colonne_valeur);
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>