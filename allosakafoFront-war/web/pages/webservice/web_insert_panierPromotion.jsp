<%-- 
    Document   : web_insert_panierPromotion
    Created on : 4 mai 2017, 15:34:15
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String idPromotion = request.getParameter("idPromotion");
    String idUtilisateur = request.getParameter("idUtilisateur");
    int quantite = Integer.parseInt(request.getParameter("quantite"));
    float pu = Float.parseFloat(request.getParameter("pu"));
    int statut = Integer.parseInt(request.getParameter("statutPanier"));
    String observation = request.getParameter("observation");
    String resultatInsertion = ExecuteFunction.inserer_panier_promotion(id, idPromotion, idUtilisateur, quantite, pu, statut, observation);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>