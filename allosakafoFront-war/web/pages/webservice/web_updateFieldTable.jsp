<%-- 
    Document   : web_updateImageProduit
    Created on : 5 mai 2017, 12:38:35
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String table = request.getParameter("table");
    String champ = request.getParameter("champ");
    String valChamp = request.getParameter("valChamp");
    String conditionChamp = request.getParameter("conditionChamp");
    String conditionValChamp = request.getParameter("conditionValChamp");
    String resultatModification = new String();
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setContentType("application/json");
    try{
        resultatModification = ExecuteFunction.modifier_champTable(table,champ,valChamp,conditionChamp, conditionValChamp);
	out.println(resultatModification);
    }catch(Exception e){
        resultatModification = e.getMessage();
    }
%>