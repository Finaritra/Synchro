<%-- 
    Document   : web_insert_demandeAbonnement
    Created on : 27 juillet 2017, 14:30:22
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String statut = request.getParameter("statut");
    String resultatInsertion = new String();
    try{
        resultatInsertion = ExecuteFunction.update_statutDemandeAbonnement(id,statut);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>
