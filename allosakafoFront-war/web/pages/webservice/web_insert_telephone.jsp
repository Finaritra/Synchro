<%-- 
    Document   : web_insert_telephone
    Created on : 26 avr. 2017, 12:13:25
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String id = request.getParameter("id");
    String utilisateur = request.getParameter("util");
    String numero = request.getParameter("num");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = null;
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        resultatInsertion = ExecuteFunction.inserer_telephone(id, utilisateur, numero, commentaire);
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>