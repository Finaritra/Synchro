<%-- 
    Document   : web_insert_message
    Created on : 25 avr. 2017, 14:36:34
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String id = request.getParameter("id");
    String idTypeMessage = request.getParameter("tpe");
    String nom = request.getParameter("nom");
    String email = request.getParameter("email");
    String contenu = request.getParameter("cnt");
    int etat = 0;
    if(request.getParameter("etat")!=null){
        etat = Integer.parseInt(request.getParameter("etat"));
    }
    String dateEnvoi = request.getParameter("date");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = null;
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        resultatInsertion = ExecuteFunction.inserer_message(id, idTypeMessage, nom, email, contenu, etat, dateEnvoi, commentaire);
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>