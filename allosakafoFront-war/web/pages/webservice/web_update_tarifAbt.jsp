<%-- 
    Document   : web_insert_admin
    Created on : 25 avr. 2017, 09:51:45
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String idDureeTarifAbonnement = request.getParameter("idDureeTarifAbonnement");
    String dateApplication = request.getParameter("dateApplication");
    String montant = request.getParameter("montant");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = null;
    try{
        resultatInsertion = ExecuteFunction.modifier_tarifAbonnement(id, idDureeTarifAbonnement, dateApplication, montant, commentaire);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>