<%-- 
    Document   : travailleurJson
    Created on : 17 nov. 2015, 10:17:12
    Author     : Safidimahefa Romario
--%>
<%@page import="java.sql.Connection"%>
<%@page import="mg.cnaps.sig.SigTravailleurInfo"%>
<%@page import="mg.cnaps.sig.Employeurs"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.google.gson.GsonBuilder"%>
<%@page import="bean.CGenUtil"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<% 
    List<SigTravailleurInfo> all = new ArrayList<SigTravailleurInfo>();
    Connection con = null;
    String json = null;
    final GsonBuilder builder = new GsonBuilder();
    final Gson gson = builder.create();
    try {
            SigTravailleurInfo[] allTrav = (SigTravailleurInfo[]) CGenUtil.rechercher(new SigTravailleurInfo(), null, null, con, "");
            for (SigTravailleurInfo element : allTrav) {
                
                all.add(element);
            }
             json = gson.toJson(all);

            
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (con != null) {
                con.close();
            }
        }
%>

<%
  out.println(json);
%>