<%-- 
    Document   : as-produits-fiche
    Created on : 1 d�c. 2016, 10:40:08
    Author     : Joe
--%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.produits.ProduitsLibelle"%>
<%
    ProduitsLibelle a = new ProduitsLibelle();
    a.setNomTable("as_produits_libelleprix");
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    
    pc.getChampByName("id").setLibelle("ID");
	pc.getChampByName("pu").setLibelle("Prix Unitaire");
    pc.getChampByName("typeproduit").setLibelle("Type produit");
    pc.setTitre("Consultation produits");


%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=produits/as-produits-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
						%>
							<br/>
                        <div class="box-footer">
							<a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=produits/as-recette.jsp&id="+request.getParameter("id")%>" style="margin-right: 10px" >Editer Recette</a>
                        </div>
                        <br/>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>