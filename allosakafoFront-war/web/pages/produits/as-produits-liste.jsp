<%-- 
    Document   : as-produits-liste
    Created on : 1 d�c. 2016, 10:39:44
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.produits.ProduitsLibelle"%>
<%@page import="affichage.PageRecherche"%>

<% 
    ProduitsLibelle lv = new ProduitsLibelle();
    String listeCrt[] = {"nom","designation", "typeproduit"};
    String listeInt[] = null;
    String libEntete[] = {"id", "nom", "designation", "typeproduit", "calorie", "poids", "pu"};

    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    affichage.Champ[] liste = new affichage.Champ[1];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("as_typeproduit");
    liste[0] = new Liste("typeproduit", ou, "VAL", "VAL");

    pr.getFormu().changerEnChamp(liste);
    
    
    pr.setApres("produits/as-produits-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste produits</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=produits/as-produits-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=produits/as-produits-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Nom", "D�signation", "Type produit", "Calorie", "Poids", "Prix Unitaire"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>