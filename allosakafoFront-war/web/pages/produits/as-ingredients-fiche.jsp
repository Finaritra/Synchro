<%-- 
    Document   : as-produits-fiche
    Created on : 1 d�c. 2016, 10:40:08
    Author     : Joe
--%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.produits.Ingredients"%>
<%
    Ingredients a = new Ingredients();
    a.setNomTable("as_ingredients_libelle");
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    
    pc.getChampByName("id").setLibelle("ID");
    pc.getChampByName("unite").setLibelle("Unit�");
    pc.getChampByName("pu").setLibelle("Prix Unitaire");
    pc.getChampByName("quantiteparpack").setLibelle("Quantit� par pack");

    pc.setTitre("Consultation ingredients");


%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=produits/as-ingredients-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
						<a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=produits/as-ingredients-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>