<%@page import="bean.*"%>
<%@page import="affichage.*"%>
<%@page import="java.sql.Connection"%>
<%@page import="utilitaire.UtilDB"%>

<%
	UtilDB utildb = null;
	String valeur = (String) request.getParameter("valeur");
	String idchamp = (String) request.getParameter("idchamp");
	String idsuggestion = (String) request.getParameter("idsuggestion");
	String idliste = (String) request.getParameter("idliste");
	String colonne = (String) request.getParameter("colonne");
	String nomclasse = (String) request.getParameter("nomclasse");
	String critere = "AND lower("+colonne+") LIKE lower('%"+valeur+"%')";
	String javascript = ChampAutocomplete.makeSelectionEvent(idchamp);
	try{
		ClassMAPTable typeObjet = (ClassMAPTable) Class.forName(nomclasse).newInstance();
		ListeMultiple liste = new ListeMultiple(idliste, javascript, typeObjet, colonne, colonne);
		liste.setApresW(critere);
		utildb = new UtilDB();
		liste.findData(null);
		liste.makeHtml();
		out.println(liste.getHtml());
	}catch(Exception ex){
		ex.printStackTrace();
		throw ex;
	}
%>