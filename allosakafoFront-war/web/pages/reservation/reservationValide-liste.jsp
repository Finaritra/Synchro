<%-- 
    Document   : reservationValide-liste
    Created on : 3 mars 2020, 09:26:50
    Author     : Maharo
--%>

<%@page import="bean.*"%>
<%@page import="utilitaire.ConstanteEtat"%>
<%@page import="java.util.Map"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="java.util.HashMap"%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.reservation.ReservationValideEffectue"%>


<%
    /*if(session.getAttribute("tableClient") == null) {
        out.println("<script language='JavaScript'> document.location.replace('/index.jsp'); </script>");
        return;
    }
    String date = request.getParameter("date");
    String idTable=(String)session.getAttribute("tableClient");
    
    
    
    HashMap<String,Object> liste=AlloSakafoService.getListeCommandeEffectuees(idTable,date);
    HashMap<String,Object[]> listeCommande=(HashMap<String,Object[]>)liste.get("commande");
    HashMap<String,Object[]> listeDetailsCommande=(HashMap<String,Object[]>)liste.get("listedetails");
    HashMap<String,Object[]> listePhotos=(HashMap<String,Object[]>)liste.get("listePhotos");
    String somme = (String)liste.get("total");*/
    ReservationValideEffectue lv = new ReservationValideEffectue();
    lv.setNomTable("RESERVATIONLIBVALIDE");
	
    String listeCrt[] = {"id","source","nbpersonne","datesaisie","datereservation","idClient"};
    String listeInt[] = {"nbpersonne","datesaisie","datereservation"};
    String libEntete[] = {"id","source","heuresaisie","heuredebutreservation","heurefinreservation","nbpersonne","datesaisie","datereservation","idClient"};
        
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
    //pr.getFormu().getChamp("nomtable").setValeur((String)session.getAttribute("tableClient"));
    //pr.getFormu().getChamp("datecommande").setValeur(utilitaire.Utilitaire.dateDuJour());
    
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
//    affichage.Champ[] listeDir = new affichage.Champ[1];
//    Direction dir = new Direction();
//    dir.setNomTable("restaurant");
//    listeDir[0] = new Liste("restaurant",dir,"libelledir","iddir");
//    pr.getFormu().changerEnChamp(listeDir);
     

    String apreswhere = "";
  
  
        apreswhere += " and tablenom = '"+session.getValue("tableNom")+"'";
    System.out.println("ilay nom Table =" +session.getValue("tableNom") );
    pr.setAWhere(apreswhere);
    
    //pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());

    String[] colSomme = {};
   // pr.setNpp(-1);
    pr.creerObjetPage(libEntete, colSomme);
    ResultatEtSomme rs=pr.getRs();
    ReservationValideEffectue[]listeComande=(ReservationValideEffectue[])(rs.getResultat());
    double somme=rs.getSommeEtNombre()[0];
%>

<div class="row">
    <div class="col-md-12 col-xs-12 content-box">
        <div class="header-title header-content bon-plan header-content-title">
            <span class="header-gamepub-logo header-content-logo">
                <span class="glyphicon glyphicon-list-alt"></span>
            </span>
            <span class="header-content-text">Liste des Reservations effectu&eacute;es Valid&eacutes; </span>
            <span class="header-content-text pull-right">Total : <%=Utilitaire.formaterAr(somme)%>   </span>
        </div>
        
        <div class="col-xs-12 content-body">
            <div class="col-xs-12">
                <form class="form" action="home.jsp" method="GET" autocomplete="off"> 
                    <div class="col-sm-4 col-xs-4">
                        <div class="input-group form-input-label-mobile">
                                <div class="input-group-addon"><strong>Date : </strong></div>
                                <input name="date" type="date" class="form-control" id="date" value="<%=Utilitaire.dateDuJourSql()%>" tabindex="8">
                        <input type="hidden" name="but" value="pages/reservation/reservationValide-liste.jsp">
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Afficher</button>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 content-body-produit">
                <form action="ProduitAchat" id="produitAchat" method="POST">
                    <div class="col-xs-12 content-jeton-list-tab">
                        <table class="table table-hover">
                            <tbody>
                                <div id="accordion">
                                    
                                        <div class="card">
                                                <div class="card-body">
                                                    <div class="content_match content_match_nopointer col-xs-12">
                                                        <%
                                                            if(listeComande!=null)
                                                            {
                                                            for(int j=0;j<listeComande.length;j++){
                                                        %>
                                                        <div class="content_match-list col-xs-12">
                                                            <div class="content_match-infos col-xs-4 col-sm-12">
                                                                <div class="col-xs-12 content-match-score-all">
                                                                    <p class="col-xs-2 achatLibelle">Client</p>
                                                                    <p class="col-xs-2 achatLibelle">Source</p>                                                       
                                                                    <p class="col-xs-2 achatLibelle">Heure et date de Saisie </p>                                                      
                                                                    <p class="col-xs-2 achatLibelle">Date de Reservation</p>
                                                                    <p class="col-xs-2 achatLibelle">Heure Debut de Reservation</p>
                                                                    <p class="col-xs-2 achatLibelle">Heure Fin de Reservation</p>                                                                   
                                                                    <p class="col-xs-2 achatLibelle"> Nombre de Personnes</p>
                                                                   
                                                                    <p class="col-xs-2 achatLibelle">TableNom</p>
                                                                </div>
                                                                <div class="col-xs-12 content-match-score-all" style="font-size: 14px;">
                                                                   
                                                                    <p class="col-xs-2"><%=listeComande[j].getIdClient() %></p>
                                                                    <p class="col-xs-2"><%=listeComande[j].getSource() %></p>
                                                                    <p class="col-xs-2"><%=Utilitaire.formatterDaty(listeComande[j].getDatesaisie()) +" " + listeComande[j].getHeuresaisie()%></p>
                                                                    <p class="col-xs-2"><%=listeComande[j].getDatereservation() %></p>
                                                                    <p class="col-xs-2"><%=listeComande[j].getHeuredebutreservation() %></p>
                                                                    <p class="col-xs-2"><%=listeComande[j].getHeurefinreservation() %></p>
                                                                    <p class="col-xs-2"><%=listeComande[j].getNbpersonne() %></p>
                                             
                                                                    <p class="col-xs-2"><%=listeComande[j].getTablenom() %></p>
                                                                          
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                                
                                                        <% }} %>
                                                        
                                                        
            
                                                       
                                                    </div>
                                                    
                                                </div>
                                        </div>
                                    
                                </div>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>