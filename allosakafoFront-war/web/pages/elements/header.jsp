<%@page import="bean.CGenUtil"%>
<%@page import="mg.cnaps.messagecommunication.MessageLibelle"%>
<%@page import="historique.MapUtilisateur"%>

<%@page import="user.UserEJB"%>

<%
    String sess=(String)session.getAttribute("tableClient");
    if(sess==null){
        out.println("<script language='JavaScript'> document.location.replace('index.jsp'); </script>");
    }
%> 
<header class="main-header" style="position: fixed; left: 0; right: 0;">
    <div class="navigation-header">
        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4 header-logo-index header-logo-not-connected">
            <a href="home.jsp?but=pages/menu/liste-plat.jsp">
                <img src="assets/img/logo.png" alt="Ph� Resto" class="logo-header-home2">
            </a>
        </div>
        <div class="header-data header-data-index col-sm-8 col-md-9 col-lg-10 col-xs-12">                
            <div class="col-xs-12 col-sm-12 col-lg-10" style="text-align: right">
                <div class="col-sm-8 col-md-4">
                    <a href="home.jsp?but=pages/commande/listecommande.jsp"><span style="display: none;" class="header-welcome" ><i class="glyphicon glyphicon-list-alt"></i>     Vos commande</span> <br> </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <a href="home.jsp?but=pages/commande/listeCommandesEffectuees.jsp&idCommandeLivraison="<%=request.getParameter("idCommandeLivraison")%>><span class="header-welcome" ><i class="glyphicon glyphicon-book"></i>     Vos commandes effectu�es</span> <br> </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <a href="deconnexion.jsp"><span class="header-welcome" > Quitter <%=session.getValue("tableNom")%>   <i class="glyphicon glyphicon-log-out"></i></span></a>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="menu/menu-header.jsp"/>
</header>