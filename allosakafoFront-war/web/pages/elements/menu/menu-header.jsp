<%-- 
    Document   : menu-header
    Created on : 19 sept. 2019, 17:02:46
    Author     : Notiavina
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.produits.MenuCategorie"%>

<% MenuCategorie[] listeMenu = AlloSakafoService.getMenuCategorie(); %>
<div class="menu-header menu-header-connected col-xs-12">
    <ul class="header-list list-style-none">
        <li><a class="link-default" href="home.jsp?but=pages/menu/liste-plat.jsp"><span class="menu-header-title" onmouseover="showRubrique('0')">Tous</span></a></li>
        <% for(MenuCategorie cat : listeMenu) { %>
            <li><a class="link-default" href="home.jsp?but=pages/menu/liste-plat.jsp&idtypeproduit=<%= cat.getIdtypeproduit() %>"><span class="menu-header-title" onmouseover="showRubrique('<%= cat.getLibelle_type() %>',this)"><%= cat.getLibelle_type() %></span></a></li>
        <% } %>
    </ul>
    <div class="col-xs-12 menu-header-sous-rubrique">
        <% 
            for(MenuCategorie cat : listeMenu) { 
                if(cat.getSouscategorie() != null && cat.getSouscategorie().length > 0){ %>
                    <ul class="list-no-padding list-style-none list-display-sous list-display-none js-<%=cat.getLibelle_type()%> ">
                        <%
                            MenuCategorie[] listeSousCat = cat.getSouscategorie();
                            for(MenuCategorie sousCat : listeSousCat){ %>
                                <li><a class="link-default" href="home.jsp?but=pages/menu/liste-plat.jsp&idtypeproduit=<%=cat.getIdtypeproduit()%>&idsouscategorie=<%=sousCat.getIdtypeproduit()%>"><%= sousCat.getLibelle_type() %></a></li>
                        <%  }
                        %>
                    </ul>
        <%      } 
            }  %>
    </div>
</div>
<script>
function showRubrique(rubrique) {
    <%for(MenuCategorie cat : listeMenu){%>
        $(".js-<%=cat.getLibelle_type()%>").css('display', 'none');
    <%}%>
    
    $(".js-"+rubrique+"").css('display', 'block');
    $(".menu-header-sous-rubrique").css('height', 'auto');
}
function showRubrique(rubrique, element) {
    <%for(MenuCategorie cat : listeMenu){%>
        $(".js-<%=cat.getLibelle_type()%>").css('display', 'none');
    <%}%>
    
    var place = getOffset(element);
    
    $(".js-"+rubrique+"").css('display', 'block');
    $(".menu-header-sous-rubrique").css('height', 'auto');
    $(".menu-header-sous-rubrique").css(place);
}
function getOffset( el ) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: 0, left: _x };
} 
</script>
