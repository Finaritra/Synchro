<%@page import="bean.CGenUtil"%>
<%@page import="mg.cnaps.messagecommunication.MessageLibelle"%>
<%@page import="historique.MapUtilisateur"%>

<%@page import="user.UserEJB"%>

        
<header class="main-header" style="position: fixed; left: 0; right: 0;">
    <div class="navigation-header">
        <div class="header-data header-data-index col-sm-8 col-md-9 col-lg-10 col-xs-12">                
            <div class="col-xs-12 col-sm-12 col-lg-12 " style="text-align: right">
                <div class="col-xs-12 col-sm-4 col-md-4 pull-right">
                    <% if(session.getValue("restaurant")!=null || session.getValue("nomRestaurant")!=null){%>
                    <a pull-right href="deconnexion.jsp?sess=1"><span class="header-welcome" > Quitter <%=session.getValue("nomRestaurant")%>   <i class="glyphicon glyphicon-log-out"></i></span></a>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
</header>