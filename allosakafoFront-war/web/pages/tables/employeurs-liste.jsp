<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>
<%
Employeurs emp = new Employeurs();

String listeCrt[]={"id","employeur_nom","statut_type", "employeur_num_registre", "employeur_date_registre", "employeur_date_adhesion", "employeur_date_embauche", "employeur_num_stat", "employeur_date_stat", "employeur_nif", "employeur_date_nif", "employeur_type_pointage", "code_dr", "code_dr_adr", "flag_fram", "activite_code", "activite_code_secondaire", "employeur_nom_com", "employeur_telephone", "employeur_fax", "employeur_mail"};
String listeInt[]={"employeur_date_regisre", "employeur_date_adhesion", "employeur_date_embauche"};
String libEntete[]={"id","employeur_nom","statut_type", "employeur_num_registre", "employeur_date_registre", "employeur_date_adhesion", "employeur_date_embauche", "employeur_num_stat", "employeur_date_stat", "employeur_nif", "employeur_date_nif", "employeur_type_pointage", "code_dr", "code_dr_adr", "flag_fram", "activite_code", "activite_code_secondaire", "employeur_nom_com", "employeur_telephone", "emplyeur_fax", "employeur_mail"};
PageRecherche pr=new PageRecherche(emp,request,listeCrt,listeInt,3,libEntete,19);
pr.setUtilisateur((user.UserEJB)session.getValue("u"));
pr.setLien((String)session.getValue("lien"));
pr.setApres("tables/employeur-liste.jsp");
pr.getFormu().getChamp("employeur_nom").setLibelle("Nom");
pr.getFormu().getChamp("statut_type").setLibelle("Statut");
pr.getFormu().getChamp("employeur_num_registre").setLibelle("Numero de registre");
pr.getFormu().getChamp("employeur_date_registre").setLibelle("Date de registre");
pr.getFormu().getChamp("employeur_date_adhesion").setLibelle("Date d'adhesion");
pr.getFormu().getChamp("employeur_date_embauche").setLibelle("Date d'embauche");
pr.getFormu().getChamp("employeur_num_stat").setLibelle("Num�ro Stat");
pr.getFormu().getChamp("employeur_date_stat").setLibelle("Date Stat");
pr.getFormu().getChamp("employeur_nif").setLibelle("NIF");
pr.getFormu().getChamp("employeur_date_nif").setLibelle("NIF");
pr.getFormu().getChamp("employeur_type_pointage").setLibelle("Type de pointage");
pr.getFormu().getChamp("code_dr").setLibelle("Code DR");
pr.getFormu().getChamp("code_dr_adr").setLibelle("Code DR adr");
pr.getFormu().getChamp("flag_fram").setLibelle("Flag fram");
pr.getFormu().getChamp("activite_code").setLibelle("Activit� code");
pr.getFormu().getChamp("activite_code_secondaire").setLibelle("Activit� code secondaire");
pr.getFormu().getChamp("employeur_nom_com").setLibelle("Nom com");
pr.getFormu().getChamp("employeur_telephone").setLibelle("T�l�phone");
pr.getFormu().getChamp("employeur_fax").setLibelle("Fax");
pr.getFormu().getChamp("employeur_Mail").setLibelle("Mail");

String[]colSomme=null;
pr.creerObjetPage(libEntete,colSomme);
%>
<div class="content-wrapper">
	<h1>Liste Employeur</h1>
	<div class="row">
		<form action="<%=pr.getLien()%>?but=tables/employeur-liste.jsp" method="post" name="employeurliste" id="employeurliste">
			<% out.println(pr.getFormu().getHtmlEnsemble());%>
		</form>
	</div>
	<div class="row">
		<%
		String lienTableau[]={pr.getLien()+"?but=tables/emplyeur-fiche.jsp"};
		String colonneLien[]={"id"};
		pr.getTableau().setLien(lienTableau);
		pr.getTableau().setColonneLien(colonneLien);
		out.println(pr.getTableauRecap().getHtml());
		%>
	</div>
	<div class="row">
		<% out.println(pr.getTableau().getHtml()); %>
	</div>
	<div class="row">
		<% out.println(pr.getBasPage()); %>
	</div>
</div>
