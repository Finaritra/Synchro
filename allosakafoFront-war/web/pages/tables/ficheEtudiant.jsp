<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="etudiant.*" %>
<%!
Etudiant p;
String lien="note/ficheResultatOptionnel.jsp";
String finLien="";
%>
<%
p=new Etudiant();
p.setNomTable("etudiantlib");
PageConsulte pc=new PageConsulte(p,request,(user.UserEJB)session.getValue("u"));//ou avec argument liste Libelle si besoin
//Etudiant e=(Etudiant)pc.getBase();
%>
<div class="content-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="box box-primary">
					<div class="box-title with-border">
						<h1 class="box-title">Consultation d'une fiche &eacute;tudiant</h1>
					</div>
					<div class="box-body">
						<%String sary=request.getParameter("id");%>
						<!--<img src="img/etudiant/<%=sary%>.jpg"  alt="loading" width="150">-->
						<img width="150" src="<%=request.getContextPath()+"/imageetudiant?imageName="+sary%>"  alt="loading">
						<!--img src="/../etudiant/ETU000161.jpg" /-->
						<%
						out.println(pc.getHtml());
						%>
					</div>
					<div class="box-footer">
						<a href="<%=(String)session.getValue("lien")+"?but=certificatScolarite.jsp&id="+request.getParameter("id")%>"><button class="btn btn-info">Certificat de scolarit&eacute;</button></a>
						<a href="<%=(String)session.getValue("lien")+"?but=tables/modifEtudiant.jsp&id="+request.getParameter("id")%>"><button class="btn btn-warning">Modifier</button></a>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box box-primary">
					<div class="box-title with-border">
						<h1 class="box-title">PARCOURS SCOLAIRE</h1>
					</div>
					<div class="box-body">
						<%
							Parcours p = new Parcours();
							Parcours[] lp = (Parcours[])CGenUtil.rechercher(p,null,null," and idetudiant='"+request.getParameter("id")+"' order by semestre");
							if(lp.length!=0){
						%>
						<table class="table table-bordered">
							<tr>
								<th class="left" width="25%">Semestre</th>
								<th  class="left" width="25%">Moyenne</th>
								<th class="left" width="25%">Option</th>
								<th class="left" width="25%">Delib</th>
							</tr>
						<%for(int i=0;i<lp.length;i++){%>
						<%
						if(lp[i].getSemestre().compareTo("S1")==0 || lp[i].getSemestre().compareTo("S2")==0 || lp[i].getSemestre().compareTo("S3")==0) lien="note/ficheResultat.jsp";
						else
						{
						lien="note/ficheResultatOptionnel.jsp";
						finLien="&optionetu=all";
						}
						%>
							<tr>
								<td><a href="note/Releve2Notes.jsp?id=<%=lp[i].getCodeclasse()+" "+lp[i].getNumero()+" "+lp[i].getPromotion()+"&option="+lp[i].getIdoptionne()%>"><%=lp[i].getSemestre()%></a></td>
								<td><%=lp[i].getMoyenne()%></td>
								<td><%=lp[i].getOptionne()%></td>
							<td><a href="<%=lien%>?id=<%=request.getParameter("id")+"&semestre="+lp[i].getSemestre()+"&promotion="+lp[i].getPromotion()%><%=finLien%>">delib</a></td>
							</tr>
						<%}%>
						</table>
						<%}%>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
