<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>
<%!
Employeurs emp;
%>
<%
emp=new Employeurs();


PageConsulte pc=new PageConsulte(emp,request,(user.UserEJB)session.getValue("u"));//ou avec argument liste Libelle si besoin
//pc.getFormu().getChamp("dateNaissance").setLibelle("Date de naissance");
%>
<div class="content-wrapper">
	<div class="row">
		<div class="col-md-6">
			<div class="box">
				<div class="box-title with-border">
					<h1 class="box-title">Consultation d'une fiche employeur</h1>
				</div>
				<div class="box-body">
					<%
					out.println(pc.getHtml());
					%>
				</div>
				<div class="box-footer">
					<p>
						<a href="<%=(String)session.getValue("lien")+"?but=tables/employeur-modif.jsp&id="+request.getParameter("id")%>"><button class="btn btn-warning">Modifier</button></a>
						<a href="<%=(String)session.getValue("lien")+"?but=apresTarif.jsp&id="+request.getParameter("id")%>&acte=delete&bute=tables/employeur-liste.jsp&classe=mg.cnaps.sig.Employeurs"><button class="btn btn-danger">Supprimer</button></a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
