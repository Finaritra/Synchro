<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.facturefournisseur.FactureFournisseurLettre"%>
<%@page import="affichage.PageRecherche"%>

<% 
    FactureFournisseurLettre lv = new FactureFournisseurLettre();
    lv.setNomTable("AS_FACTUREFOURNISSEURLETTRE");
    
    String listeCrt[] = {"id", "daty", "idFournisseur","idDevise","datyecheance"};
    String listeInt[] = {"daty", "datyecheance"};
    String libEntete[] = {"id", "daty", "idFournisseur", "idDevise", "datyecheance", "montantTTC", "idTVA","etat"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
        
    affichage.Champ[] liste = new affichage.Champ[1];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("Devise");
    liste[0] = new Liste("idDevise", ou, "VAL", "VAL");

    pr.getFormu().changerEnChamp(liste);
    /*
    pr.getFormu().getChamp("groupee").setLibelleAffiche("Groupe");
    pr.getFormu().getChamp("typearticle").setLibelleAffiche("Type d'article");
    */
    pr.setApres("facturefournisseur/facturefournisseur-liste.jsp");
    String[] colSomme = {"montantTTC", "idTVA"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Facture Fournisseur</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=facturefournisseur/facturefournisseur-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=facturefournisseur/facturefournisseur-fiche.jsp", pr.getLien()};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "Fournisseur", "Devise", "Echeance", "Montant TTC", "Montant TVA","etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>