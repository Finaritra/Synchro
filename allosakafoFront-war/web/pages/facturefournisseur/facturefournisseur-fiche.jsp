<%-- 
    Document   : as-livraison-fiche
    Created on : 1 d�c. 2016, 14:21:55
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.facturefournisseur.FactureFournisseur"%>
<%
    FactureFournisseur a = new FactureFournisseur();
    a.setNomTable("FACTUREFOURNISSEURLETTRE");
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    
    pc.getChampByName("id").setLibelle("ID");
	pc.getChampByName("idFournisseur").setLibelle("Fournisseur");
	pc.getChampByName("idTVA").setLibelle("Montant TVA");
	pc.getChampByName("montantTTC").setLibelle("Montant TTC");
	pc.getChampByName("dateEmission").setVisible(false);

	pc.getChampByName("datyEcheance").setLibelle("Echeance");
	pc.getChampByName("idDevise").setLibelle("Devise");
	pc.getChampByName("numFact").setLibelle("N� Facture");
	
    pc.setTitre("Consultation Facture Fournisseur");

	FactureFournisseur ff = (FactureFournisseur)pc.getBase();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=facturefournisseur/facturefournisseur-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
					
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
						<div class="box-footer" >
							<% if (ff.getEtat() == ConstanteEtat.getEtatCreer()){%>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&classe=mg.allosakafo.facturefournisseur.FactureFournisseur&bute=facturefournisseur/facturefournisseur-fiche.jsp&id=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Viser</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annulerVisa&classe=mg.allosakafo.facturefournisseur.FactureFournisseur&bute=facturefournisseur/facturefournisseur-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
							<% 
							}
							else if (ff.getEtat() == ConstanteEtat.getEtatValider()){
							%>
							<a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=ded/ordonnerpayement-saisie.jsp&ded_Id=" + request.getParameter("id")%>" style="margin-right: 10px">Editer OP</a>
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>