<%@page import="mg.allosakafo.fin.MvtCaisse"%>
<%@page import="mg.allosakafo.fin.OrdreDePaiement"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%@page import="user.*"%> 
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*"%>
<%@page import="utilitaire.*"%>
<%@page import="bean.*"%>

<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    MvtCaisse  a = new MvtCaisse();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));    
    
	pi.getFormu().getChamp("idordre").setLibelle("Reference");
	//pi.getFormu().getChamp("idordre").setPageAppel("choix/listeORViseChoix.jsp");
	
    pi.getFormu().getChamp("daty").setLibelle("Date");
	pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
	pi.getFormu().getChamp("datyvaleur").setLibelle("Date valeur");
	
	pi.getFormu().getChamp("credit").setLibelle("Montant");
	pi.getFormu().getChamp("designation").setLibelle("Designation");
	pi.getFormu().getChamp("numpiece").setLibelle("N� piece");
	pi.getFormu().getChamp("numcheque").setLibelle("N� cheque");
	pi.getFormu().getChamp("typemvt").setLibelle("Type mouvement");
	pi.getFormu().getChamp("remarque").setLibelle("Remarque");
	pi.getFormu().getChamp("remarque").setType("textarea");
	
	/* ??? */
	pi.getFormu().getChamp("tiers").setLibelle("Tiers");
	
	affichage.Champ[] liste = new affichage.Champ[4];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("devise");
    liste[0] = new Liste("iddevise", ou, "VAL", "id");
	
	TypeObjet ou1 = new TypeObjet();
    ou1.setNomTable("modepaiement");
    liste[1] = new Liste("idmode", ou1, "VAL", "id");
	
	Caisse ou2 = new Caisse();
    ou2.setNomTable("caisse");
    liste[2] = new Liste("idcaisse", ou2, "desccaisse", "idcaisse");

	TypeObjet ou3 = new TypeObjet();
    ou3.setNomTable("typemvt");
    liste[3] = new Liste("typemvt", ou3, "VAL", "id");
	
    pi.getFormu().changerEnChamp(liste);

	pi.getFormu().getChamp("iddevise").setLibelle("Devise");
	pi.getFormu().getChamp("idmode").setLibelle("Mode de paiement");
	pi.getFormu().getChamp("idcaisse").setLibelle("Caisse");
	
	pi.getFormu().getChamp("debit").setVisible(false);
	pi.getFormu().getChamp("etat").setVisible(false);

	if (request.getParameter("idordre")!= null && request.getParameter("idordre").compareTo("") != 0){
		String ord = request.getParameter("idordre");
		OrdreDePaiement opg = new OrdreDePaiement();
		OrdreDePaiement op = ((OrdreDePaiement[]) CGenUtil.rechercher(opg, "select * from AS_ORDONNERPAYEMENT where id='"+ord+"'"))[0];
		pi.getFormu().getChamp("credit").setDefaut(Utilitaire.doubleWithoutExponential(op.getMontant()));
		pi.getFormu().getChamp("idordre").setDefaut(ord);
	}
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Enregistrer Entree Caisse</h1>
    <form action="<%=pi.getLien()%>?but=fin/apresMvt.jsp" method="post" name="ded_op" id="ded_op">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insertMvtCaisse">
    <input name="bute" type="hidden" id="bute" value="fin/entreecaisse-saisie.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.fin.MvtCaisse">
    </form>
</div>