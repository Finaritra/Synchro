<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.fin.MvtCaisse"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%@page import="affichage.PageRecherche"%>

<% 
    MvtCaisse lv = new MvtCaisse();
    lv.setNomTable("MVTCAISSELETTRE");
	
    String listeCrt[] = {"id", "designation", "iddevise","idcaisse","numpiece", "numcheque", "daty", "datyvaleur"};
    String listeInt[] = {"daty", "datyvaleur"};
    String libEntete[] = {"id", "designation", "iddevise","idcaisse","numpiece", "numcheque", "daty", "datyvaleur", "debit", "credit"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 10);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
        
    affichage.Champ[] liste = new affichage.Champ[4];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("devise");
    liste[0] = new Liste("iddevise", ou, "VAL", "id");
	
	TypeObjet ou1 = new TypeObjet();
    ou1.setNomTable("modepaiement");
    liste[1] = new Liste("idmode", ou1, "VAL", "val");
	
	Caisse ou2 = new Caisse();
    ou2.setNomTable("caisse");
    liste[2] = new Liste("idcaisse", ou2, "desccaisse", "desccaisse");

	TypeObjet ou3 = new TypeObjet();
    ou3.setNomTable("typemvt");
    liste[3] = new Liste("typemvt", ou3, "VAL", "val");
	
    pr.getFormu().changerEnChamp(liste);
    
    pr.getFormu().getChamp("idcaisse").setLibelleAffiche("Caisse");
    pr.getFormu().getChamp("iddevise").setLibelleAffiche("Devise");
    
    pr.setApres("fin/mvtcaisse-liste.jsp");
    String[] colSomme = {"debit", "credit"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Mouvement de caisse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=fin/mvtcaisse-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=fin/mvtcaisse-fiche.jsp", pr.getLien()};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Designation", "Devise", "Caisse", "N� piece", "N� cheque", "Date", "Date valeur", "Montant debit", "Montant credit"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>