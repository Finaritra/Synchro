
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.fin.MvtCaisse"%>
<%
    MvtCaisse a = new MvtCaisse();
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));
	
    pc.getChampByName("id").setLibelle("ID");
	pc.getChampByName("idordre").setLibelle("Reference");
    pc.getChampByName("daty").setLibelle("Date");
	pc.getChampByName("datyvaleur").setLibelle("Date valeur");
	pc.getChampByName("designation").setLibelle("Designation");
	pc.getChampByName("numpiece").setLibelle("N� piece");
	pc.getChampByName("numcheque").setLibelle("N� cheque");
	pc.getChampByName("typemvt").setLibelle("Type mouvement");
	pc.getChampByName("remarque").setLibelle("Remarque");
	pc.getChampByName("remarque").setType("textarea");
	pc.getChampByName("tiers").setLibelle("Tiers");
	pc.getChampByName("iddevise").setLibelle("Devise");
	pc.getChampByName("idmode").setLibelle("Mode de paiement");
	pc.getChampByName("idcaisse").setLibelle("Caisse");
	
	MvtCaisse mvt = (MvtCaisse)pc.getBase();
	
	String libelle = "";
	if (mvt.getDebit() == 0){
		libelle = "Entree";
		pc.getChampByName("credit").setLibelle("Montant");
		pc.getChampByName("debit").setVisible(false);
	}
	else {
		libelle = "Sortie";
		pc.getChampByName("debit").setLibelle("Montant");
		pc.getChampByName("credit").setVisible(false);
	}
	
	pc.setTitre("Consultation "+libelle+" de caisse");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=fin/mvtcaisse-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
					
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
						<div class="box-footer" >
							<% if (mvt.getEtat() == ConstanteEtat.getEtatCreer()){%>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&classe=mg.allosakafo.fin.MvtCaisse&bute=fin/mvtcaisse-fiche.jsp&id=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Viser</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annulerVisa&classe=mg.allosakafo.fin.MvtCaisse&bute=fin/mvtcaisse-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>