<%-- 
    Document   : home
    Created on : 17 sept. 2019, 11:18:34
    Author     : Notiavina
--%>
<%@page import="bean.TypeObjet"%>
<%@page import="service.AlloSakafoService"%>
<%
    String but = "";
    if (request.getParameter("but") != null && request.getParameter("but").compareToIgnoreCase("")!=0) {
        but = request.getParameter("but");
    }
%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <title>AlloSakafo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" >
        <meta http-equiv="cache-control" content="no-store">
        <meta http-equiv="cache-control" content="no-cache">
        <!-- Bootstrap 3.3.4 -->
        <link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <!--<link href="${pageContext.request.contextPath}/dist/js/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />-->
        <link href="${pageContext.request.contextPath}/assets/css/global.min.css" rel="stylesheet" />
        <link href="${pageContext.request.contextPath}/assets/css/xzoom.css" rel="stylesheet" />
    </head>
    <body>
        <jsp:include page="pages/elements/header.jsp"/>
        <div class="container container-bloc container-bloc-top">
            <!--<div class="col-xs-12 col-sm-5 maintenance-container">
                <p>Site en maintenance dans <span class="js-countdown"></span></p>
            </div>-->
            
            <jsp:include page="<%=but%>"/>
        </div>
        <jsp:include page="pages/elements/js.jsp"/>
    </body>
</html>
