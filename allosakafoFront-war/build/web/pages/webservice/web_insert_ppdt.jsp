<%-- 
    Document   : web_insert_ppdt
    Created on : 27 avr. 2017, 15:32:22
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String produit = request.getParameter("pdt");
    String dateApplication = request.getParameter("da");
    String montant = request.getParameter("m");
    String observation = request.getParameter("obs");
    String resultatInsertion = ExecuteFunction.inserer_prix_produit(id, produit, dateApplication, montant, observation);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultatInsertion);
%>