<%-- 
    Document   : test
    Created on : 25 avr. 2017, 10:03:06
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%-- Set the content type header with the JSP directive --%>
        <%-- Set the content disposition header --%>
        <%
           // Returns all employees (active and terminated) as json.
           response.setContentType("application/json");
           response.setHeader("Content-Disposition", "inline");
        %>

        <%
            String table = request.getParameter("t");
            JSONArray resultat_final = ExecuteFunction.select_list_all(table);
        %>
        <%
            out.println(resultat_final);
%>
    </body>
</html>
