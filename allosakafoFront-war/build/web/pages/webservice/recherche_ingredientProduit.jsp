<%-- 
    Document   : recherche_ingredientProduit
    Created on : 3 mai 2017, 10:09:55
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String motcle = request.getParameter("motcle");
    int utilisateur = Integer.parseInt(request.getParameter("util"));
    JSONArray resultat_final = ExecuteFunction.rechercher_ingredientProduit(motcle,utilisateur);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultat_final);
%>
