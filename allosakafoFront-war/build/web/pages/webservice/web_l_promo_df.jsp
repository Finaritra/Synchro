<%-- 
    Document   : web_l_promo_df
    Created on : 26 avr. 2017, 16:03:53
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String champ = request.getParameter("c");
    String ordre = request.getParameter("o");
    int limite = Integer.parseInt(request.getParameter("l"));
    int debut = Integer.parseInt(request.getParameter("d"));
    String dateDebut = request.getParameter("dd");
    String dateFin = request.getParameter("df");
    if(dateDebut==null) dateDebut = "null";
    if(dateFin==null) dateFin = "null";
    JSONArray resultat_final = ExecuteFunction.select_list_promotion_limite(champ,ordre,limite,debut,dateDebut,dateFin);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultat_final);
%>