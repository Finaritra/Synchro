<%-- 
    Document   : web_delete_id_val_desce
    Created on : 27 avr. 2017, 22:25:01
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String tab = request.getParameter("tab");
    String id = request.getParameter("id");
    String resultatInsertion = null;
    try{
        resultatInsertion = ExecuteFunction.supprimer_id_val_desce(tab, id);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultatInsertion);
%>