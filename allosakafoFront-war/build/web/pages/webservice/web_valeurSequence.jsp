<%-- 
    Document   : web_lastNumber_sequence
    Created on : 24 avr. 2017, 11:08:57
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String nomSequence = request.getParameter("seq");
    JSONArray resultat_final = ExecuteFunction.valeurSequenceSuivante(nomSequence);
%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultat_final);
%>