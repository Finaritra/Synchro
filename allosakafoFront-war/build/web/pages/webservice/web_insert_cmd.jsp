<%-- 
    Document   : web_insert_cmd
    Created on : 28 avr. 2017, 10:26:49
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String dateSaisie = request.getParameter("ds");
    String numCommande = request.getParameter("nm");
    String responsable = request.getParameter("resp");
    String client = request.getParameter("clt");
    String typeCommande = request.getParameter("tpeCmd");
    String dateLivraison = request.getParameter("dl");
    String adresseLivraison = request.getParameter("al");
    String heureLivraison = request.getParameter("hl");
    String etat = request.getParameter("etat");
    String secteur = request.getParameter("sec");
    String quartier = request.getParameter("quart");
    String idSession = request.getParameter("sess");
    String idUtilisateur = request.getParameter("idUtil");
    String remarque = request.getParameter("rmq");
    String observation = request.getParameter("obs");
    String distance = request.getParameter("dst");
    String resultatInsertion = ExecuteFunction.inserer_commandeClient(id, dateSaisie, numCommande, responsable, client, typeCommande, dateLivraison, adresseLivraison, heureLivraison, etat, secteur, quartier, idSession, idUtilisateur, remarque, observation, distance);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultatInsertion);
%>