<%-- 
    Document   : web_insert_telephone
    Created on : 26 avr. 2017, 12:13:25
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String utilisateur = request.getParameter("util");
    String numero = request.getParameter("num");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = null;
    try{
        resultatInsertion = ExecuteFunction.inserer_telephone(id, utilisateur, numero, commentaire);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultatInsertion);
%>