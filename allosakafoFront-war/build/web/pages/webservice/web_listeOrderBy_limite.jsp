<%-- 
    Document   : web_listeOrderBy_limite
    Created on : 20 avr. 2017, 14:03:42
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String table = request.getParameter("t");
    String champ = request.getParameter("c");
    String ordre = request.getParameter("o");
    int limite = Integer.parseInt(request.getParameter("l"));
    JSONArray resultat_final = ExecuteFunction.select_list_order_by_limit(table,champ,ordre,limite);
%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultat_final);
%>
