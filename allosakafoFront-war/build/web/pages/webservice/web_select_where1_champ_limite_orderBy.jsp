<%-- 
    Document   : web_select_where_champ_limite_orderBy
    Created on : 24 avr. 2017, 16:30:52
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String table = request.getParameter("t");
    String champ = request.getParameter("c");
    String valeurChamp = request.getParameter("vc");
    String champ_ordre = request.getParameter("co");
    String value_orderBy = request.getParameter("ob");
    int limite = Integer.parseInt(request.getParameter("l"));
    int debut = Integer.parseInt(request.getParameter("d"));
    JSONArray resultat_final = ExecuteFunction.select_listWhere1OrderByLimit(table,champ,valeurChamp,champ_ordre,value_orderBy,limite,debut);
%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultat_final);
%>