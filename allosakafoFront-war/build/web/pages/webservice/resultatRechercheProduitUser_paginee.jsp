<%-- 
    Document   : resultatRechercheProduitUser
    Created on : 3 mai 2017, 11:59:12
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    int limite = Integer.parseInt(request.getParameter("limite"));
    int debut = Integer.parseInt(request.getParameter("debut"));
    float prixMin = 0;
    String v_prixMin = request.getParameter("prixMin");
    if(v_prixMin!=null) prixMin = Float.parseFloat(v_prixMin);
            else prixMin = 0;
    
    float prixMax = 0;
    String v_prixMax = request.getParameter("prixMax");
    if(v_prixMax!=null) prixMax = Float.parseFloat(v_prixMax);
            else prixMax = 0;
    
    String nom = request.getParameter("s");
    String champ = request.getParameter("champ");
    String ordre = request.getParameter("ordre");
    JSONArray resultat_final = ExecuteFunction.rechercher_produit_utilisateur_paginee(limite,debut,prixMin,prixMax,nom,champ,ordre);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultat_final);
%>