<%@page import="mg.allosakafo.facture.ClientPoint"%>
<%@page import="service.AlloSakafoService"%>
<%@page import="bean.TypeObjet"%>

<%
    try {
        session.removeAttribute("tableClient");
        session.removeAttribute("idlivraison");
        
        session.removeAttribute("typeLivraison");
        session.removeAttribute("restaurant");
        session.removeAttribute("heureClot");
        session.removeAttribute("dateClot");
        session.removeAttribute("montantLivraison");
        
        ClientPoint to = new ClientPoint();
        if (request.getParameter("resto") == null || request.getParameter("resto").compareToIgnoreCase("") == 0) {
            throw new Exception("Veuillez choisir un restaurant");
        }
        session.removeAttribute("typeLivraison");
        String resto = request.getParameter("resto");
        String vue=null;
        String after="";
        if(resto.compareToIgnoreCase("normal") == 0){
            vue ="TableClientPointVue";
            after=" and (point = '"+AlloSakafoService.getNomRestau()+"' or point is null)";
        }
        else if (resto.compareToIgnoreCase("lounge") == 0){
            vue="tableclientlounge";
        }
        /*String vue = resto.compareToIgnoreCase("normal") == 0 ? "tableclientvue2" : "";
        vue = resto.compareToIgnoreCase("lounge") == 0 ? "tableclientlounge" : vue;*/
        to.setNomTable(vue);
        TypeObjet[] liste = (TypeObjet[]) AlloSakafoService.getListe(to, after);
        String but = "?but=pages/livraison/commandeLivraison-saisie.jsp&choixresto=" + resto;
        String butResa = "?but=pages/livraison/commandeResa-saisie.jsp&choixresto=" + resto;
        
        if (liste.length > 0) {
%>
<div class="container container-index container-mobile">
    <div class="row">
        <div class="col-xs-12 content-login">
            <img src="assets/img/logo.png" id="logo-ketrika" alt="Logo">
            <div class="col-xs-12 content-login-body">
                <div class="col-xs-12 col-sm-4 content-login-form">
                    <form class="form-inline login" action="pages/testLogin.jsp" method="POST">
                        <input type="hidden" name="choixresto" value="<%=resto%>" >
                        <div class="form-group ">
                            <label for="heureClot" class="">Heure envoi cmd</label>
                            <input type="text" class="form-control" name="heureClot" placeholder="HH:MM:SS">
                        </div>
                        <div class="form-group">
                            <label for="dateClot" class="">date envoi cmd</label>
                            <input type="text" class="form-control" name="dateClot" value=" <%=utilitaire.Utilitaire.dateDuJour()%>" placeholder="DD/MM/YYYY">
                        </div>
                        
                        
                        <h1>Choix des tables</h1>
                        <div class="header_login-email header-input col-xs-12">

                            <select name="tableClient" class="form-control" style="font-size: 18px;">
                                <% for (int i = 0; i < liste.length; i++) {%>
                                <option value="<%=liste[i].getId() + "&&" + liste[i].getVal()%>"><%=liste[i].getVal()%></option>
                                <% }%>
                            </select>
                        </div>
                        <div class="login-connect col-xs-12">
                            <button type="submit" name="defaulttable" class="btn btn-success col-xs-12 btn-choix-table"><strong>Valider</strong></button>
                            <hr>

                        </div>

                    </form>
                    <div class="header_login-email header-input col-xs-12">
                        <div>
                            <a class="btn btn-success col-xs-12 btn-choix-table" style="margin-bottom: 10px;" href="<%=but%>"><button class="btn btn-success col-xs-12 btn-choix-table"><strong>Livraison</strong></button></a>
                        </div>
                        <div>
                            <a class="btn btn-success col-xs-12 btn-choix-table" style="margin-bottom: 10px;" href="<%=butResa%>"><button class="btn btn-success col-xs-12 btn-choix-table"><strong>R&eacute;servation</strong></button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% }
} catch (Exception e) {
    e.printStackTrace();

%>

<script language="JavaScript">
    alert('<%= e.getMessage()%>');
    document.location.replace('/pho/index.jsp?but=pages/choixresto.jsp');
</script>
<%
        return;
    }
%>