<!-- jQuery 2.1.4 -->
<script src="${pageContext.request.contextPath}/assets/js/jquery-1.11.2.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery.alerts.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery.metadata.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery.plugins.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery.tablesorter.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery-latest.js"></script>

<script type="text/javascript">
    function changeImage(url, image, image1024) {
        $("#xzoom-default").attr("src", "" + url + "/assets/img/plats/" + image + "");
        $("#xzoom-default").attr("xoriginal", "" + url + "/assets/img/plats/" + image1024 + "");
    }
    function ficheProduit(idProduit) {
        window.location.href = "home.jsp?but=pages/menu/fiche-plat.jsp&cleproduit=" + idProduit;
    }
    function haut2(i) {
        var val = parseFloat($("#2qteprod" + i).val());
        val += 1;
        $("#2qteprod" + i).val(val);
        qteprod2(i);
    }
    function bas2(i) {
        var val = parseFloat($("#2qteprod" + i).val());
        val -= 1;
        $("#2qteprod" + i).val(val);
        qteprod2(i);
    }
    function qteprod2(i) {
        var val = parseFloat($("#2pu" + i).val());
        val = val * $("#2qteprod" + i).val();
        document.getElementById("total" + i).innerHTML = val;
    }
    
    
    window.ParsleyValidator.setLocale('fr');
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });
//    $(".timepicker").timepicker({
//        showInputs: false
//    });

    $(window).bind("load", function() {
//        getMessageDeploiement();
//        window.setInterval(getMessageDeploiement, 30000);
    });
    
    function getMessageDeploiement(){
        var text = 'ok';
        $.ajax({
            type:'GET',
            url:'${pageContext.request.contextPath}/MessageDeploiement',
            contentType: 'application/json',
            data: {'mes':text},
            success:function(ma){
                if(ma!=null){
                    var data = JSON.parse(ma);
                    if(data.message!=null){
                        alert(data.message);
                    }
                    if(data.erreur!=null){
                        alert(data.erreur);
                    }
                }

            },
            error: function(e) {
                //alert("Erreur Ajax");
            }

        });
    }
    
    function pagePopUp(page, width, height) {
        w = 750;
        h = 600;
        t = "D&eacute;tails";

        if (width != null || width == "")
        {
            w = width;
        }
        if (height != null || height == "") {
            h = height;
        }
        window.open(page, t, "titulaireresizable=no,scrollbars=yes,location=no,width=" + w + ",height=" + h + ",top=0,left=0");
    }
    function searchKeyPress(e)
    {
        // look for window.event in case event isn't passed in
        e = e || window.event;
        if (e.keyCode == 13)
        {
            document.getElementById('btnListe').click();
            return false;
        }
        return true;
    }
    function back() {
        history.back();
    }
    function getChoix() {
        setTimeout("document.frmchx.submit()", 800);
    }
    $('#sigi').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": false
    });
    $(function () {
        $(".select2").select2();
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    function CocheToutCheckbox(ref, name) {
            var form = ref;

            while (form.parentNode && form.nodeName.toLowerCase() != 'form') {
                form = form.parentNode;
            }

            var elements = form.getElementsByTagName('input');

            for (var i = 0; i < elements.length; i++) {
                if (elements[i].type == 'checkbox' && elements[i].name == name) {
                    elements[i].checked = ref.checked;
                }
            }
        }
</script>
<script src="${pageContext.request.contextPath}/assets/js/script.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/assets/js/controleTj.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/apjplugins/champcalcul.js" defer></script>

<script src="${pageContext.request.contextPath}/assets/js/soundmanager2-jsmin.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/js/messagews.js" type="text/javascript"></script>
<script type="text/javascript">
if(typeof(Storage) !== "undefined") {
    // Code for localStorage/sessionStorage.
    var collapse=localStorage.getItem("menuCollapse");
    
} else {
    // Sorry! No Web Storage support..
}
$(document).ready(function(){
    
    if(localStorage.getItem("menuCollapse")=="true"){
        $("body").addClass("sidebar-collapse");
    }
    
    $(".sidebar-toggle").click(function(){
        if(localStorage.getItem("menuCollapse")=="false" || localStorage.getItem("menuCollapse")==""){
            localStorage.setItem("menuCollapse","true");
        }else{
            localStorage.setItem("menuCollapse","false");
        }
    });
});


</script>