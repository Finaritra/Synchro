
<%@page import="mg.cnaps.compta.ComptaSousEcriture"%>
<%@page import="mg.cnaps.compta.ComptaEcriture" %>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.immo.*" %>
<%
    
    //PlanAmortissement plan = new PlanAmortissement(request.getParameter("id")); //Tableau d'amortissement
    String id_immo=(String) request.getParameter("id");
    Immobilisation[] tab_immobilisation = (Immobilisation[]) CGenUtil.rechercher(new Immobilisation(), null, null, " AND ID = '" + id_immo + "'");
    Immobilisation immob=(tab_immobilisation!=null && tab_immobilisation.length>0)?tab_immobilisation[0]:null;    
    //ImmoCession[] imoCession = (ImmoCession[]) CGenUtil.rechercher(new ImmoCession(), null, null, " AND IMMO = '" + request.getParameter("id") + "'");
    UserEJB u = (UserEJB) session.getAttribute("u");
    String lien = (String) session.getValue("lien");

    Immobilisation immo = new Immobilisation();
    immo.setNomTable("IMMO_TEMP");
    String[] libelleImmobilisationFiche = {"Id","Exercice","Date d'acquisition", "Num&eacute;ro BL", "Num&eacute;ro BE", "Nombre","Prix unitaire", "Frais", "TVA",
"Categorie","Nature Immobilisation","Ancien code immo","Compte immo en cours", "Compte immobilisation", "Compte amortissement","Dotation", 
            "Compte c�ssion", "Compte perte", "Compte profit", "Compte d�bit","Identification",
            "Designation","Description","Codification", 
              "Amortissable","Type d'amortissement","Dur�e d'amortissement",   "Valeur d'acquisition", "Date de mise en service",
            "Etat", "Id_user"};
    PageConsulte pc = new PageConsulte(immo, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setLibAffichage(libelleImmobilisationFiche);
    pc.setTitre("Fiche Immobilisation");

    String immoId = "AND REF_IMMO='" + request.getParameter("id") + "'";
    ImmoHistorique[] historiques = (ImmoHistorique[]) u.getData(new ImmoHistorique(), null, null, null, immoId);

    ComptaEcriture ce = new ComptaEcriture();
    ce.setIdobjet(request.getParameter("id"));
    ce.setJournal("1");
    ComptaEcriture[] liste = (ComptaEcriture[])CGenUtil.rechercher(ce, null, null, "");
    ComptaSousEcriture[] liste_cse = null;
    if(liste.length > 0){
        ComptaSousEcriture cse = new ComptaSousEcriture();
        cse.setMere(liste[0].getId());
        liste_cse = (ComptaSousEcriture[])CGenUtil.rechercher(cse, null, null, "");     
    }
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=immo/immobilisation-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <%if(tab_immobilisation[0].getEtat()==1){%>
                                <a class="btn btn-danger pull-right"  href="<%=lien + "?but=apresTarif.jsp&id=" + id_immo %>&acte=delete&bute=immo/immobilisation-liste.jsp&classe=mg.cnaps.immo.Immobilisation" style="margin-right: 10px">Supprimer</a>
                                <a class="btn btn-warning pull-right"  href="<%=lien + "?but=immo/immobilisation-modif.jsp&id=" + id_immo %>" style="margin-right: 10px" >Modifier</a>
                                <a class="btn btn-success pull-right"  href="<%=lien+ "?but=apresTarif.jsp&id=" + id_immo %>&acte=valider&bute=immo/immobilisation-liste.jsp&classe=mg.cnaps.immo.Immobilisation" style="margin-right: 10px">Viser</a>
                                <a class="btn btn-primary pull-right" href="<%=lien +"?but=immo/immobilisation-saisie.jsp" %>" style="margin-right: 10px">Nouveau</a>
                            <%}%>
                            
                            <%if(tab_immobilisation[0].getEtat()==11 ||tab_immobilisation[0].getEtat()==15 ){%>
                                <a class="btn btn-primary pull-right"  href="<%=lien + "?but=immo/detenteur/detenteur-saisie.jsp&id=" + id_immo+"&etat="+immob.getEtat() %>" style="margin-right: 10px">Attribuer</a>
                                <a class="btn btn-default pull-right"  href="<%=lien + "?but=immo/condamnation/condamnation-saisie.jsp&id=" + id_immo+"&etat="+immob.getEtat() %>" style="margin-right: 10px">Condamner</a>
                                 <a class="btn btn-default pull-right"  href="<%=lien + "?but=immo/entretien/cession-saisie.jsp&id=" + id_immo+"&etat="+immob.getEtat()%>" style="margin-right: 10px">C�der</a>
                                <a class="btn btn-primary pull-right" href="<%=lien +"?but=immo/immobilisation-saisie.jsp" %>" style="margin-right: 10px">Nouveau</a>
                            <%}%>
                            
                            <%if(tab_immobilisation[0].getEtat()>=17 ){%>
                                <a class="btn btn-primary pull-right"  href="<%=lien + "?but=immo/detenteur/transfert-saisie.jsp&id=" + id_immo%>" style="margin-right: 10px">Transf�rer</a>
                                <a class="btn btn-default pull-right"  href="<%=lien + "?but=immo/condamnation/condamnation-saisie.jsp&id=" + id_immo+"&etat="+immob.getEtat() %>" style="margin-right: 10px">Condamner</a>
                                <a class="btn btn-default pull-right"  href="<%=lien + "?but=immo/entretien/cession-saisie.jsp&id=" + id_immo+"&etat="+immob.getEtat()%>" style="margin-right: 10px">C�der</a>
                                <a class="btn btn-primary pull-right" href="<%=lien +"?but=immo/immobilisation-saisie.jsp" %>" style="margin-right: 10px">Nouveau</a>
                            <%}%>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%=pc.getBasPage()%>
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">Historique</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style='background-color:#bed1dd'>Id</th>
                                    <th style='background-color:#bed1dd'>Immobilisation</th>
                                    <th style='background-color:#bed1dd'>Date</th>
                                    <th style='background-color:#bed1dd'>D�tenteur</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (int i = 0; i < historiques.length; i++) {
                                %>
                                <tr>
                                    <td ><%=Utilitaire.formatterDaty(historiques[i].getId())%></td>
                                    <td><%=Utilitaire.champNull(historiques[i].getRef_immo())%></td>
                                    <td><%=Utilitaire.formatterDaty(historiques[i].getDaty())%></td>
                                    <td><a href="<%=pc.getLien() + "?but=immo/detenteur/detenteur-fiche.jsp&id="+historiques[i].getDetenteur() %>" ><%=Utilitaire.champNull(historiques[i].getDetenteur())%></a></td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <%
            Amortissement amor= new Amortissement(id_immo);
            LigneAmortissement[] plan= amor.getPlan();
            if(plan!=null && plan.length>0){ 
        
        %>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">Plan d'amortissement</h1>
                        <%=amor.getTitre() %>
                    </div>
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>                                    
                                    <%
                                    out.print(plan[0].getEntete());
                                    if (immob.getEtat() >= 11) {%>
                                        <th style="background-color:#bed1dd; text-align: right;"><strong>Ecriture ammortissement</strong></th>
                                    <%}%>
                                </tr>
                            </thead>
                            <tbody>
                                <% for (LigneAmortissement ligne : plan) {%>
                                <tr>                                   
                              <% 
                                        out.print(ligne.toHtml());
                                        if (immob.getEtat() >= 11) {
                                            ComptaEcriture compta_ecriture = new ComptaEcriture();
                                            int exercice = ligne.getAnnee();
                                            compta_ecriture.setExercice("" + exercice);
                                            compta_ecriture.setIdobjet(request.getParameter("id"));
					    compta_ecriture.setJournal("3");
                                            ComptaEcriture[] liste_ce = (ComptaEcriture[])CGenUtil.rechercher(compta_ecriture, null, null, "");
                                           
                                           if (liste_ce.length > 0) {
                                  
				%>
                                    <td style="text-align: center;"><a href="<%=lien%>?but=compta/ecriture/ecriture-fiche.jsp&id=<%=liste_ce[0].getId()%>" style="margin-right: 10px"><%=liste_ce[0].getId()%></a></td>       
                                        <%      } else {
                                        %>
                                    <td style="text-align: center;"><a class="btn btn-info pull-right"  href="immo.jsp?but=immo/apresImmo.jsp&id=<%=id_immo%>&acte=genererEcritureAmmorti&bute=immo/immobilisation-fiche.jsp&classe=mg.cnaps.immo.Immobilisation&exercice=<%=ligne.getAnnee()%>&valeur=<%=ligne.getAmortissement()%>" style="margin-right: 10px">G&eacute;n&eacute;rer</a></td>
                                    <% 
                                        } 
                                        }
                                    %>
                                    
                                </tr>
                                <%
                                }
                                
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <%
            }
            
        %>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">Ecriture Acquisition Immo</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Date</th>
                                    <th>D&eacute;signation</th>
                                    <th>Compte</th>
                                    <th>D&eacute;bit</th>
                                    <th>Cr&eacute;dit</th>
                                    
                                    <th>Id</th>
                                </tr>
                                <%if(liste_cse!=null){
                                    for (int i = 0; i < liste_cse.length; i++) {
                                %>
                                <tr>
                                    <td><%=Utilitaire.datetostring(liste[0].getDateComptable())%></td>
                                    <td><%=Utilitaire.champNull(liste_cse[i].getRemarque())%></td>
                                    <td><%=Utilitaire.champNull(liste_cse[i].getCompte())%></td>
                                    <td><%=Utilitaire.formaterAr(liste_cse[i].getDebit())%></td>
                                    <td><%=Utilitaire.formaterAr(liste_cse[i].getCredit())%></td>
                                    
                                    <td><a href="<%=lien%>?but=compta/ecriture/ecriture-fiche.jsp&id=<%=liste[0].getId()%>"><%=liste[0].getId()%></a></td>
                                </tr>
                                <%
                                    }
                                }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
