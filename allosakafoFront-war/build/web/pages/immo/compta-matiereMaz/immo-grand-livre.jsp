
<%@page import="mg.cnaps.immo.*" %>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>

<%
    int exercice=Utilitaire.getAneeEnCours();
    ImmoGrandLivreLibelle igl = new ImmoGrandLivreLibelle();
    String listeCrt[] = {"exercice","nomenclature"};
    String listeInt[] =null;
    String listeEntete[] = {"exercice", "nomenclature", "designation","codification","q_existant","v_existant","q_entree","v_entree","q_sortie","v_sortie","q_restant","v_restant", "daty_es","origine_es"};
    PageRecherche pr = new PageRecherche(igl , request, listeCrt, listeInt,2, listeEntete, 14);
   
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres(request.getParameter("but"));
    //pr.getFormu().getChamp("code_immo").
    pr.getFormu().getChamp("exercice").setLibelle("Exercice");
    pr.getFormu().getChamp("exercice").setDefaut(Utilitaire.getAnneeEnCours());
    pr.getFormu().getChamp("nomenclature").setLibelle("Nomenclature");
    if(request.getParameter("exercice")==null){ 
        exercice=Utilitaire.getAneeEnCours();
        pr.setAWhere(" AND EXERCICE='"+exercice+"'");
    }else{
        exercice=Integer.parseInt(request.getParameter("exercice"));
    }
    String colSomme[] = {"q_existant","v_existant","q_entree","v_entree","q_sortie","v_sortie","q_restant","v_restant"};
    pr.creerObjetPage(listeEntete, colSomme);

    int q_existant=0, q_entree=0, q_sortie=0, q_restant=0;
    double v_existant=0d, v_entree=0d, v_sortie=0d, v_restant=0d;
    ImmoGrandLivreLibelle[] report=ImmoRecapGrandLivre.getReportLibelle(exercice);
    
%>
<script>
    function changerAnnee() {
        document.immograndlivre.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Grand livre  <%=exercice %></h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=immo/compta-matiereMaz/immo-grand-livre.jsp" method="post" name="immograndlivre" id="immograndlivre">
            <% out.println(pr.getFormu().getHtmlEnsemble());%>
        </form>
        
        <div class="row">
            <div class="row col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" style="text-align: center"><span> EXERCICE <%=request.getParameter("exercice")==null?" "+Utilitaire.getAnneeEnCours():" "+request.getParameter("exercice")%></span></h3>
                    </div>
                    
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered" >            
                            <thead style='text-align:center'>
					<tr>
						<th rowspan='2' style='background-color:#bed1dd'>Nom.</th>
						<th rowspan='2' style='background-color:#bed1dd'>Unit�</th>
						<th rowspan='2' style='background-color:#bed1dd'>D�signation</th>
                                                <th rowspan='2' style='background-color:#bed1dd'>Codification</th>
						<th colspan='2' style='background-color:#bed1dd; text-align:center'>Existant le 1 Jan. <%=exercice %></th>
						<th rowspan='2' style='background-color:#bed1dd'>Mvt effecu�</th>
						<th rowspan='2' style='background-color:#bed1dd'>Num P.J.</th>
						<th rowspan='2' style='background-color:#bed1dd'>Date d'E/S</th>
						<th rowspan='2' style='background-color:#bed1dd'>Origine des E/S </th>
						<th colspan='2' style='background-color:#bed1dd; text-align:center'>Entr�e <%=exercice %></th>
						<th colspan='2' style='background-color:#bed1dd; text-align:center'>Sortie <%=exercice %></th>
						<th colspan='2' style='background-color:#bed1dd; text-align:center'>Existant le 31 D�c. <%=exercice %></th>
					</tr>
					<tr>
						<th style='background-color:#bed1dd'>Qt�</th>
						<th style='background-color:#bed1dd'>Valeur</th>
						<th style='background-color:#bed1dd'>Qt�</th>
						<th style='background-color:#bed1dd'>Valeur</th>
						<th style='background-color:#bed1dd'>Qt�</th>
						<th style='background-color:#bed1dd'>Valeur</th>
						<th style='background-color:#bed1dd'>Qt�</th>
						<th style='background-color:#bed1dd'>Valeur</th>
					</tr>

				</thead>

                            <tbody>
                                 <%
                                    //ImmoGrandLivreLibelle[] tableau = (ImmoGrandLivreLibelle[]) pr.getTableau().getData();
                                 ImmoGrandLivreLibelle[] tableau = (ImmoGrandLivreLibelle[]) CGenUtil.rechercher(new ImmoGrandLivreLibelle(),null, null, " AND exercice LIKE '%"+exercice+"%' AND nomenclature LIKE '%"+request.getParameter("nomenclature")+"%' ORDER BY nomenclature, daty_es, typee");     //pr.getTableau().getData();
                                      
                                  String ancien_nomencl="";
                                  ImmoGrandLivreLibelle element=null;
                                  if(tableau!=null && tableau.length>0){  
                                    int size=tableau.length;
                                   int i=0;
                                    while(i<size){
                                        
                                        element=tableau[i];
                                        q_existant+=element.getQ_existant(); q_entree+=element.getQ_entree(); q_sortie+=element.getQ_sortie(); q_restant+=element.getQ_restant();
                                        v_existant+=element.getV_existant(); v_entree+=element.getV_entree(); v_sortie+=element.getV_sortie(); v_restant+=element.getV_restant();
                                        
                                        if(element.getNomenclature().equals(ancien_nomencl)){
                                            //System.out.println("EGALE="+ancien_nomencl+ "  NOUVEAU="+element.getNomenclature());
                                         %>
                                         <tr>
                                                <td></td>
                                                <td style='text-align:center'><%=Utilitaire.champNull(element.getUnite()) %></td>
                                                <td><%=Utilitaire.champNull(element.getDesignation()) %></td>
                                                <td><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/immobilisation-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getId() %>" ><%=Utilitaire.champNull(element.getCodification()) %></a></td>
                                                <td style='text-align:center'><%=element.getQ_existant()==0?"":element.getQ_existant() %></td>
                                                <td style='text-align:right'><%=element.getV_existant()==0d?"":Utilitaire.formaterAr(element.getV_existant()) %></td>
                                                <td style='text-align:left'><%=Utilitaire.champNull(element.getMouvement()) %></td>
                                                 <td style='text-align:center'><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/materiel-a-evaluer-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getNumero_pj() %>" ><%=Utilitaire.champNull(element.getNumero_pj()) %></a></td>
                                                <td style='text-align:left'><%=Utilitaire.formatterDaty(element.getDaty_es()) %></td>
                                                 <% String origine=Utilitaire.champNull(element.getOrigine_es());
                                                    if(origine.startsWith("TEF")){%>
                                                     <td style='text-align:left'><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/tef-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getId() %>" ><%=Utilitaire.champNull(element.getOrigine_es()) %></a></td>
                                                    <%}
                                                    if(origine.startsWith("CON")){%>
                                                    <td style='text-align:left'><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/condamnation-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getId() %>" ><%=Utilitaire.champNull(element.getOrigine_es()) %></a></td>
                                                    <%}
                                                    if(origine.startsWith("PV")){%>
                                                    <td style='text-align:left'><%=Utilitaire.champNull(element.getOrigine_es()) %></td>
                                                    <%}
                                                    %>
                                                <td style='text-align:center'><%=element.getQ_entree()==0?"":element.getQ_entree() %></td>
                                                <td style='text-align:right'><%=element.getV_entree()==0?"":Utilitaire.formaterAr(element.getV_entree()) %></td>
                                                <td style='text-align:center'><%=element.getQ_sortie()==0?"":element.getQ_sortie() %></td>
                                                <td style='text-align:right'><%=element.getV_sortie()==0?"":Utilitaire.formaterAr(element.getV_sortie()) %></td>
                                                <td style='text-align:center'><%=element.getQ_restant()==0?"":element.getQ_restant() %></td>
                                                <td style='text-align:right'><%=element.getV_restant()==0d?"":Utilitaire.formaterAr(element.getV_restant()) %></td>
                                         </tr>
                                         <%
                                        
                                        }else{
                                           // System.out.println(" TSY EGALE="+ancien_nomencl+ "  NOUVEAU="+element.getNomenclature());
                                            if(report[Integer.valueOf(element.getNomenclature())].getQ_existant()!=0){
                                                ImmoGrandLivreLibelle ereport=report[Integer.valueOf(element.getNomenclature())];
                                                q_existant+=ereport.getQ_existant();  q_restant+=ereport.getQ_restant();
                                                v_existant+=ereport.getV_existant(); v_restant+=ereport.getV_restant();
                                                %>
                                                 <tr>
                                                    <th style='text-align:center'><%=Utilitaire.champNull(ereport.getNomenclature()) %></th> 
                                                    <td style='text-align:center'><%=Utilitaire.champNull(ereport.getUnite()) %></td>
                                                    <td><%=Utilitaire.champNull(ereport.getDesignation()) %></td>
                                                    <td><%=Utilitaire.champNull(ereport.getCodification()) %></td>
                                                    <td style='text-align:center'><%=ereport.getQ_existant()==0?"":ereport.getQ_existant() %></td>
                                                    <td style='text-align:right'><%=ereport.getV_existant()==0d?"":Utilitaire.formaterAr(ereport.getV_existant()) %></td>
                                                    <td style='text-align:left'><%=Utilitaire.champNull(ereport.getMouvement()) %></td>
                                                    <td style='text-align:center'><%=Utilitaire.champNull(ereport.getNumero_pj()) %></td>
                                                    <td style='text-align:left'><%=Utilitaire.formatterDaty(ereport.getDaty_es()) %></td>
                                                    <td style='text-align:left'><%=Utilitaire.champNull(ereport.getOrigine_es()) %></td>
                                                    <td style='text-align:center'><%=ereport.getQ_entree()==0?"":ereport.getQ_entree() %></td>
                                                    <td style='text-align:right'><%=ereport.getV_entree()==0?"":Utilitaire.formaterAr(ereport.getV_entree()) %></td>
                                                    <td style='text-align:center'><%=ereport.getQ_sortie()==0?"":ereport.getQ_sortie() %></td>
                                                    <td style='text-align:right'><%=ereport.getV_sortie()==0?"":Utilitaire.formaterAr(ereport.getV_sortie()) %></td>
                                                    <td style='text-align:center'><%=ereport.getQ_restant()==0?"":ereport.getQ_restant() %></td>
                                                    <td style='text-align:right'><%=ereport.getV_restant()==0d?"":Utilitaire.formaterAr(ereport.getV_restant()) %></td>
                                                </tr>
                                                 <tr>
                                                <td></td>
                                                <td style='text-align:center'><%=Utilitaire.champNull(element.getUnite()) %></td>
                                                <td><%=Utilitaire.champNull(element.getDesignation()) %></td>
                                                <td><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/immobilisation-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getId() %>" ><%=Utilitaire.champNull(element.getCodification()) %></a></td>
                                                <td style='text-align:center'><%=element.getQ_existant()==0?"":element.getQ_existant() %></td>
                                                <td style='text-align:right'><%=element.getV_existant()==0d?"":Utilitaire.formaterAr(element.getV_existant()) %></td>
                                                <td style='text-align:left'><%=Utilitaire.champNull(element.getMouvement()) %></td>
                                                 <td style='text-align:center'><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/materiel-a-evaluer-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getNumero_pj() %>" ><%=Utilitaire.champNull(element.getNumero_pj()) %></a></td>
                                                <td style='text-align:left'><%=Utilitaire.formatterDaty(element.getDaty_es()) %></td>
                                                <% String origine2=Utilitaire.champNull(element.getOrigine_es());
                                                    if(origine2.startsWith("TEF")){%>
                                                     <td style='text-align:left'><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/tef-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getOrigine_es() %>" ><%=Utilitaire.champNull(element.getOrigine_es()) %></a></td>
                                                    <%}
                                                    if(origine2.startsWith("CON")){%>
                                                    <td style='text-align:left'><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/condamnation-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getOrigine_es() %>" ><%=Utilitaire.champNull(element.getOrigine_es()) %></a></td>
                                                    <%}
                                                    if(origine2.startsWith("PV")){%>
                                                    <td style='text-align:left'><%=Utilitaire.champNull(element.getOrigine_es()) %></td>
                                                    <%}
                                                    %>
                                                <td style='text-align:center'><%=element.getQ_entree()==0?"":element.getQ_entree() %></td>
                                                <td style='text-align:right'><%=element.getV_entree()==0?"":Utilitaire.formaterAr(element.getV_entree()) %></td>
                                                <td style='text-align:center'><%=element.getQ_sortie()==0?"":element.getQ_sortie() %></td>
                                                <td style='text-align:right'><%=element.getV_sortie()==0?"":Utilitaire.formaterAr(element.getV_sortie()) %></td>
                                                <td style='text-align:center'><%=element.getQ_restant()==0?"":element.getQ_restant() %></td>
                                                <td style='text-align:right'><%=element.getV_restant()==0d?"":Utilitaire.formaterAr(element.getV_restant()) %></td>
                                             </tr>
                                            <%
                                            }else{
                                            %>
                                            <tr>
                                                <td><%=Utilitaire.champNull(element.getNomenclature()) %></td> 
                                                <td style='text-align:center'><%=Utilitaire.champNull(element.getUnite()) %></td>
                                                <td><%=Utilitaire.champNull(element.getDesignation()) %></td>
                                                <td><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/immobilisation-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getId() %>" ><%=Utilitaire.champNull(element.getCodification()) %></a></td>
                                                <td style='text-align:center'><%=element.getQ_existant()==0?"":element.getQ_existant() %></td>
                                                <td style='text-align:right'><%=element.getV_existant()==0d?"":Utilitaire.formaterAr(element.getV_existant()) %></td>
                                                <td style='text-align:left'><%=Utilitaire.champNull(element.getMouvement()) %></td>
                                                 <td style='text-align:center'><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/materiel-a-evaluer-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getNumero_pj() %>" ><%=Utilitaire.champNull(element.getNumero_pj()) %></a></td>
                                                <td style='text-align:left'><%=Utilitaire.formatterDaty(element.getDaty_es()) %></td>
                                                <% String origine=Utilitaire.champNull(element.getOrigine_es());
                                                    if(origine.startsWith("TEF")){%>
                                                     <td style='text-align:left'><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/tef-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getId() %>" ><%=Utilitaire.champNull(element.getOrigine_es()) %></a></td>
                                                    <%}
                                                    if(origine.startsWith("CON")){%>
                                                    <td style='text-align:left'><a href="<%=pr.getLien() + "?but=immo/compta-matiereMaz/condamnation-fiche.jsp&fichier=immo-grand-livre.jsp&id="+element.getId() %>" ><%=Utilitaire.champNull(element.getOrigine_es()) %></a></td>
                                                    <%}
                                                    if(origine.startsWith("PV")){%>
                                                    <td style='text-align:left'><%=Utilitaire.champNull(element.getOrigine_es()) %></td>
                                                    <%}
                                                %>
                                                <td style='text-align:center'><%=element.getQ_entree()==0?"":element.getQ_entree() %></td>
                                                <td style='text-align:right'><%=element.getV_entree()==0?"":Utilitaire.formaterAr(element.getV_entree()) %></td>
                                                <td style='text-align:center'><%=element.getQ_sortie()==0?"":element.getQ_sortie() %></td>
                                                <td style='text-align:right'><%=element.getV_sortie()==0?"":Utilitaire.formaterAr(element.getV_sortie()) %></td>
                                                <td style='text-align:center'><%=element.getQ_restant()==0?"":element.getQ_restant() %></td>
                                                <td style='text-align:right'><%=element.getV_restant()==0d?"":Utilitaire.formaterAr(element.getV_restant()) %></td>
                                             </tr>
                                            
                                            
                                            <%
                                            
                                        }
                                            ancien_nomencl=element.getNomenclature();
                                    
                                    }
                                    i++;
                                  }
                                  }else{
                                      System.out.println("ATOOOOOOOOOOOOOOOOOOOOO");
                                      if(request.getParameter("nomenclature")==null || request.getParameter("nomenclature").equals("")){
                                          for(int i=1; i<report.length; i++){
                                            if(report[i]!=null && report[i].getQ_existant()!=0){
                                                element=report[i];
                                                q_existant+=element.getQ_existant(); q_entree+=element.getQ_entree(); q_sortie+=element.getQ_sortie(); q_restant+=element.getQ_restant();
                                                v_existant+=element.getV_existant(); v_entree+=element.getV_entree(); v_sortie+=element.getV_sortie(); v_restant+=element.getV_restant();
                                        
                                %>
                                       <tr>
                                                    <th style='text-align:center'><%=Utilitaire.champNull(report[i].getNomenclature()) %></th> 
                                                    <td style='text-align:center'><%=Utilitaire.champNull(report[i].getUnite()) %></td>
                                                    <td><%=Utilitaire.champNull(report[i].getDesignation()) %></td>
                                                    <td><%=Utilitaire.champNull(report[i].getCodification()) %></td>
                                                    <td style='text-align:center'><%=report[i].getQ_existant()==0?"":report[i].getQ_existant() %></td>
                                                    <td style='text-align:right'><%=report[i].getV_existant()==0d?"":Utilitaire.formaterAr(report[i].getV_existant()) %></td>
                                                    <td style='text-align:left'><%=Utilitaire.champNull(report[i].getMouvement()) %></td>
                                                    <td style='text-align:center'><%=Utilitaire.champNull(report[i].getNumero_pj()) %></td>
                                                    <td style='text-align:left'><%=Utilitaire.formatterDaty(report[i].getDaty_es()) %></td>
                                                     <td style='text-align:left'><%=Utilitaire.champNull(report[i].getOrigine_es()) %></td>
                                                    <td style='text-align:center'><%=report[i].getQ_entree()==0?"":report[i].getQ_entree() %></td>
                                                    <td style='text-align:right'><%=report[i].getV_entree()==0?"":Utilitaire.formaterAr(report[i].getV_entree()) %></td>
                                                    <td style='text-align:center'><%=report[i].getQ_sortie()==0?"":report[i].getQ_sortie() %></td>
                                                    <td style='text-align:right'><%=report[i].getV_sortie()==0?"":Utilitaire.formaterAr(report[i].getV_sortie()) %></td>
                                                    <td style='text-align:center'><%=report[i].getQ_restant()==0?"":report[i].getQ_restant() %></td>
                                                    <td style='text-align:right'><%=report[i].getV_restant()==0d?"":Utilitaire.formaterAr(report[i].getV_restant()) %></td>
                                    </tr>          
                                            
                                 <%          
                                            }
    }
                                          
                                      }else{
                                          
                                      }
                                  
                                  }
                                 %>
                                
                                
                                
                            </tbody>
                            <tfoot>
					<tr>
						<th colspan='4' style='background-color:#bed1dd; text-align:center'>Total</th>
						<th style='background-color:#bed1dd; text-align:center'> <%=q_existant %> </th>
                                                <th style='background-color:#bed1dd; text-align:center'> <%=Utilitaire.formaterAr(v_existant) %> </th>
                                                <th colspan='4' style='background-color:#bed1dd; text-align:center'> </th>
						<th style='background-color:#bed1dd; text-align:center'> <%=q_entree %> </th>
                                                <th style='background-color:#bed1dd; text-align:center'> <%=Utilitaire.formaterAr(v_entree) %> </th>
                                                <th style='background-color:#bed1dd; text-align:center'> <%=q_sortie%> </th>
                                                <th style='background-color:#bed1dd; text-align:center'> <%=Utilitaire.formaterAr(v_sortie) %> </th>
                                                <th style='background-color:#bed1dd; text-align:center'> <%=q_restant %> </th>
                                                <th style='background-color:#bed1dd; text-align:center'> <%=Utilitaire.formaterAr(v_restant) %> </th>
					</tr>
                            </tfoot>
                         </table>
                    </div>
                </div>
            </div>
        </div>
                                        
         <div class="row">
            <div class="row col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" style="text-align: center"><span> RECAPITULATION <%=request.getParameter("exercice")==null?" "+Utilitaire.getAnneeEnCours():" "+request.getParameter("exercice")%></span></h3>
                    </div>
                    
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered" >            
                            <thead style='text-align:center'>
					<tr>
						<th rowspan='2' style='background-color:#bed1dd'>Nomenclature</th>
						<th rowspan='2' style='background-color:#bed1dd'>Unit�</th>
						<th colspan='2' style='background-color:#bed1dd; text-align:center'>Existant le 1 Jan. <%=exercice %></th>
						<th colspan='2' style='background-color:#bed1dd; text-align:center'>Entr�e <%=exercice %></th>
						<th colspan='2' style='background-color:#bed1dd; text-align:center'>Sortie <%=exercice %></th>
						<th colspan='2' style='background-color:#bed1dd; text-align:center'>Existant le 31 D�c. <%=exercice %></th>
					</tr>
					<tr>
						<th style='background-color:#bed1dd'>Qt�</th>
						<th style='background-color:#bed1dd'>Valeur</th>
						<th style='background-color:#bed1dd'>Qt�</th>
						<th style='background-color:#bed1dd'>Valeur</th>
						<th style='background-color:#bed1dd'>Qt�</th>
						<th style='background-color:#bed1dd'>Valeur</th>
						<th style='background-color:#bed1dd'>Qt�</th>
						<th style='background-color:#bed1dd'>Valeur</th>
					</tr>

				</thead>

                            <tbody>
                                 
                                
                            </tbody>
                            <tfoot>
					<tr>
						<th style='text-align:center'>Total</th>
                                                <th style='text-align:center'>NBR</th>
						<th style='text-align:center'> <%=q_existant %> </th>
                                                <th style='text-align:center'> <%=Utilitaire.formaterAr(v_existant) %> </th>
						<th style='text-align:center'> <%=q_entree %> </th>
                                                <th style='text-align:center'> <%=Utilitaire.formaterAr(v_entree) %> </th>
                                                <th style='text-align:center'> <%=q_sortie%> </th>
                                                <th style='text-align:center'> <%=Utilitaire.formaterAr(v_sortie) %> </th>
                                                <th style='text-align:center'> <%=q_restant %> </th>
                                                <th style='text-align:center'> <%=Utilitaire.formaterAr(v_restant) %> </th>
					</tr>
                            </tfoot>
                         </table>
                    </div>
                </div>
            </div>
        </div>
        <a href="${pageContext.request.contextPath}/Immo?action=grandlivretriennal&exercice=<%=exercice%>" class="btn btn-default pull-right">Imprimer</a>
    </section>
</div>


