<%-- 
    Document   : apres-evaluation
    Created on : 20 janv. 2016, 20:45:08
    Author     : ITU
--%>

<%@page import="mg.cnaps.immo.ImmoAEntrer"%>
<%@page import="mg.cnaps.immo.ImmoGrandLivreMaz"%>
<%@page import="mg.cnaps.immo.ImmoMaterielInventorie"%>
<%@page import="mg.cnaps.immo.ImmoEtat"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.Connection"%>
<%@page import="mg.cnaps.immo.ImmoEvaluationMaz"%>
<%@page import="mg.cnaps.formation.FormAbsence"%>
<%@page import="mg.cnaps.immo.Immobilisation"%>
<%@page import="mg.cnaps.immo.ImmoAEntrer"%>
<%@page import="mg.cnaps.st.StMvtStockFille"%>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="bean.*" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="affichage.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
        String nomtable;
        String typeBoutton;
    %>
    <%
        try {
            System.out.println("ATOOOOOOOOOOOOOOO");
            
            nomtable = request.getParameter("nomtable");
            typeBoutton = request.getParameter("type");
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            bute = request.getParameter("bute");
            Object temp = null;
            String[] rajoutLien = null;
            String classe = request.getParameter("classe");
            ClassMAPTable t = null;
            String tempRajout = request.getParameter("rajoutLien");
            String val = "";
			String id = request.getParameter("id");

            String rajoutLie = "";
            if (tempRajout != null && tempRajout.compareToIgnoreCase("") != 0) {
                rajoutLien = utilitaire.Utilitaire.split(tempRajout, "-");
            }
            if (bute == null || bute.compareToIgnoreCase("") == 0) {
                bute = "pub/Pub.jsp";
            }

            if (classe == null || classe.compareToIgnoreCase("") == 0) {
                classe = "pub.Montant";
            }

            if (typeBoutton == null || typeBoutton.compareToIgnoreCase("") == 0) {
                typeBoutton = "3"; //par defaut modifier
            }

            int type = Utilitaire.stringToInt(typeBoutton);
            
            if (acte.compareToIgnoreCase("insert") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ImmoEvaluationMaz f =(ImmoEvaluationMaz) p.getObjectAvecValeur();
                String etat= ImmoEtat.getEtat(f.getObservation());                
                f.setObservation(etat);
                f.setNomTable(nomtable);
                ClassMAPTable o = (ClassMAPTable)u.createObject(f);
                temp = (Object)o;
                if (o != null) {
                    id = o.getTuppleID();
                }
            }
            if (acte.compareToIgnoreCase("insertMat") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ImmoAEntrer f =(ImmoAEntrer) p.getObjectAvecValeur();
                //String etat= ImmoEtat.getEtat(f.getObservation());                
                //f.setObservation(etat);
                f.setNomTable(nomtable);
                ClassMAPTable o = (ClassMAPTable)u.createObject(f);
                temp = (Object)o;
                if (o != null) {
                    id = o.getTuppleID();
                }
            }
            
            if (acte.compareToIgnoreCase("insertGl") == 0) {
                //System.out.println("TAFIDITRAAAAAAAAA INSERGL");
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
                ClassMAPTable o = (ClassMAPTable)u.createObject(f);
                temp = (Object)o;
                if (o != null) {
                    id = o.getTuppleID();
                }
                %>
                <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>");</script>
                <%
            }
            if (acte.compareToIgnoreCase("insertRecap") == 0) {
                System.out.println("TAFIDITRAAAAAAAAA INSERRECAP");
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
                ClassMAPTable o = (ClassMAPTable)u.createObject(f);
                temp = (Object)o;
                if (o != null) {
                    id = o.getTuppleID();
                }
                %>
                <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>");</script>
                <%
            }
            
            if (acte.compareToIgnoreCase("delete") == 0) {
                String error = ""; %>
           <%
                try {
                    
                    t = (ClassMAPTable) (Class.forName(classe).newInstance());
                    t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
                    //t.setNomTable(nomtable);
                    u.deleteObject(t);
                    } catch (Exception e) {%>
                    <script language="JavaScript">alert('<%=e.getMessage()%>');history.back();</script>
                    <%
                    }%>

              <%
            }
            if (acte.compareToIgnoreCase("update") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                Page p = new Page(t, request);
                ClassMAPTable f = (ClassMAPTable)p.getObjectAvecValeur();
                temp = f;
                u.updateObject(f);
            }
            
            if (acte.compareToIgnoreCase("updateEv") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                Page p = new Page(t, request);
                 ClassMAPTable f = (ClassMAPTable)p.getObjectAvecValeur();
                temp = f;
                f.setNomTable(nomtable);
                u.updateObject(f);
            }
            
            if (acte.compareToIgnoreCase("updateRec") == 0) {
                ImmoEvaluationMaz[] immo = (ImmoEvaluationMaz[])CGenUtil.rechercher(new ImmoEvaluationMaz(), null, null, " AND id='"+request.getParameter("id")+"'");
                ImmoEvaluationMaz f=immo[0];
                temp = f;
                f.setEvalue(2);
                f.setNomTable(nomtable);
                u.updateObject(f);
                %>
                <script language="JavaScript"> document.location.replace("<%=request.getParameter("url")%>");</script>
                <%
            }
            if (acte.compareToIgnoreCase("insertImmoMaterielInventorie") == 0) {
                int size=Integer.parseInt(request.getParameter("isany"));
                String[] observ=new String[size]; 
                String[] code=new String[size];
                String[] folio=new String[size];                
                int annee=Integer.parseInt(request.getParameter("annee"));
                for(int i=0; i<size; i++){
                    code[i]=request.getParameter("id"+i);
                    observ[i]=request.getParameter("obs"+i);
                    folio[i]=request.getParameter("folio"+i);
                    System.out.println("CODE="+code[i]+"\t OBS="+observ[i]+",\t FOLIO="+folio[i]);
                }
                u.insertImmoMaterielInventorie(code,observ,folio, annee);
            }
            if (acte.compareToIgnoreCase("updateRecAll") == 0) {
                Connection c = null;
                String sql="update immo_a_evalue set evalue=2 where evalue=1 and annee="+request.getParameter("annee");
                Statement stmt=c.createStatement();
                try {
                    c = new UtilDB().GetConn();
                    c.setAutoCommit(false);
                    stmt.execute(sql);
                    c.commit();
                   
                } catch (Exception ex) {
                    ex.printStackTrace();
                    c.rollback();
                    throw ex;
                } finally {
                    if (c != null) {
                        c.close();
                    }
                }
            }
            if (acte.compareToIgnoreCase("updateEnt") == 0) {
                ImmoEvaluationMaz[] immo = (ImmoEvaluationMaz[])CGenUtil.rechercher(new ImmoEvaluationMaz(), null, null, " AND id='"+request.getParameter("id")+"'");
                ImmoEvaluationMaz f=immo[0];
                temp = f;
                f.setEvalue(3);
                f.setNomTable(nomtable);
                u.updateObject(f);
                %>
                <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>&id=<%=f.getId()%>");</script>
                <%
            }
            
           
            
            if (acte.compareToIgnoreCase("dupliquer") == 0) {
                String classeFille = request.getParameter("nomClasseFille");
                String nomColonneMere = request.getParameter("nomColonneMere");
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
                Object o = u.dupliquerObject(t,classeFille,nomColonneMere);
                val = o.toString();
                %>
                <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>&id=<%=val%>");</script>
                <%
            }
            if (acte.compareToIgnoreCase("annuler") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
                u.annulerObject(t);
            }
            
           
            
            
            if (acte.compareToIgnoreCase("valider") == 0) {     // VISER
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
                ClassMAPTable o = (ClassMAPTable)u.validerObject(t);
                temp = t;
                val = o.getTuppleID();
                
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&id=<%=val%>");</script>
    <%
            }
 
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&valeur=<%=val%>&id=<%=id%>");</script>
    <%

    } catch (Exception e) {
        e.printStackTrace();
       System.out.println( "ERREUR BEEEEEEEEE="+ e.getMessage() );
    %>

    <script language="JavaScript"> alert('<%=e.getMessage()%>');
        history.back();</script>
        <%
                return;
            }
        %>
</html>




