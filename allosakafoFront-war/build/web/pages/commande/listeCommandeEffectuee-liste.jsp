<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="java.util.HashMap"%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetails"%>
    
<%
    if(session.getAttribute("tableClient") == null) {
        out.println("<script language='JavaScript'> document.location.replace('/index.jsp'); </script>");
        return;
    }
    String idTable=(String)session.getAttribute("tableClient");
    
    CommandeClient cc=new CommandeClient();
    cc.setNomTable("AS_COMMANDECLIENT_LIBELLE");
    CommandeClient[] listeCommande=(CommandeClient[]) AlloSakafoService.getListe(cc," and client='"+idTable+"' and etat>=9");
    
    CommandeClientDetails ccd=new CommandeClientDetails();
    ccd.setNomTable("AS_DETAILSCOMMANDE_LIB_2");
    CommandeClientDetails[] liste=null;
    
    HashMap<String,String[]> listePhotos=null;
%>

<div class="row">
    <div class="col-md-8 col-xs-12 content-box">
        <div class="header-title header-content bon-plan header-content-title">
            <span class="header-gamepub-logo header-content-logo">
                <span class="glyphicon glyphicon-list-alt"></span>
            </span>
            <span class="header-content-text">Liste des commandes non effectu�es </span>
        </div>
        
        <div class="col-xs-12 content-body">
            <div class="col-xs-12 content-body-produit">
                <form action="ProduitAchat" id="produitAchat" method="POST">
                    <div class="col-xs-12 content-jeton-list-tab">
                        <table class="table table-hover">
                            <tbody>
                                <div id="accordion">
                                    <% for (int i=0; i<listeCommande.length;i++) {
                                        liste= (CommandeClientDetails[])AlloSakafoService.getListe(ccd," and idMere='"+listeCommande[i].getId()+"'");
                                        listePhotos=AlloSakafoService.getPhotos(liste);
                                    %>
                                        <div class="card">
                                            <div class="card-header" id="headingTwo">
                                                <h5 class="mb-0">
                                                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<%=i%>" aria-expanded="false" aria-controls="collapse<%=i%>">
                                                        <%= listeCommande[i].getId() %>
                                                    </a>
                                                        <span><%=Utilitaire.formatterDaty(listeCommande[i].getDatecommande()) %></span>
                                                </h5>
                                                
                                            </div>
                                            <div id="collapse<%=i%>" class="collapse" aria-labelledby="heading<%=i%>" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="content_match content_match_nopointer col-xs-12">
                                                        <%
                                                            if(liste.length>0){
                                                            for(int j=0;j<liste.length;j++){
                                                        %>
                                                        <div class="content_match-list col-xs-12">
                                                            <div class="content_match-infos col-xs-4 col-sm-12">
                                                                <div class="col-xs-12 content-match-score-all">
                                                                    <p class="col-xs-6 achatLibelle">Produit</p>
                                                                    <p class="col-xs-3 achatLibelle">Prix</p>
                                                                    <p class="col-xs-3 achatLibelle">Quantit�</p>
                                                                </div>
                                                                <div class="col-xs-12 content-match-score-all" style="font-size: 14px;">
                                                                    <p class="col-xs-3">
                                                                        <img class="drapeau img-thumbnail content_match-team-drapeau" src="assets/img/plats/<%=listePhotos.get(liste[j].getProduit())[0]%>"/>
                                                                    </p>
                                                                    <p class="col-xs-3"><%=liste[j].getProduit() %></p>
                                                                    <p class="col-xs-3"><%=Utilitaire.formaterAr(liste[j].getPu()) %></p>
                                                                    <p class="col-xs-3"><%=liste[j].getQuantite() %></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%}}%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <% } %>
                                </div>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>