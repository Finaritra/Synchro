<%@page import="utilitaire.ConstanteEtat"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsSauce"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsAccompagne"%>
<%@page import="java.util.Map"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="java.util.HashMap"%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetails"%>
    
<%
    if(session.getAttribute("tableClient") == null) {
        out.println("<script language='JavaScript'> document.location.replace('/index.jsp'); </script>");
        return;
    }
    String idTable=(String)session.getAttribute("tableClient");
    
    HashMap<String,HashMap<String,Object[]>> liste=AlloSakafoService.getListeCommandeEffectuees(idTable);
    HashMap<String,Object[]> listeCommande=liste.get("commande");
    HashMap<String,Object[]> listeDetailsCommande=liste.get("listedetails");
    HashMap<String,Object[]> listePhotos=liste.get("listePhotos");
%>

<div class="row">
    <div class="col-md-8 col-xs-12 content-box">
        <div class="header-title header-content bon-plan header-content-title">
            <span class="header-gamepub-logo header-content-logo">
                <span class="glyphicon glyphicon-list-alt"></span>
            </span>
            <span class="header-content-text">Liste des commandes effectu�es </span>
        </div>
        
        <div class="col-xs-12 content-body">
            <div class="col-xs-12 content-body-produit">
                <form action="ProduitAchat" id="produitAchat" method="POST">
                    <div class="col-xs-12 content-jeton-list-tab">
                        <table class="table table-hover">
                            <tbody>
                                <div id="accordion">
                                    <% 
                                        CommandeClient[] listeCommande2=(CommandeClient[])listeCommande.get("listeCommande");
                                        for(int i=0;i<listeCommande2.length;i++){
                                    %>
                                        <div class="card">
                                            <div class="card-header" id="headingTwo">
                                                <h5 class="mb-0">
                                                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<%=i%>" aria-expanded="false" aria-controls="collapse<%=i%>">
                                                        <%= listeCommande2[i].getId() %>
                                                    </a>
                                                        <span><%=Utilitaire.formatterDaty(listeCommande2[i].getDatecommande()) %></span>
                                                </h5>
                                                
                                            </div>
                                            <div id="collapse<%=i%>" class="collapse" aria-labelledby="heading<%=i%>" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="content_match content_match_nopointer col-xs-12">
                                                        <%
                                                            CommandeClientDetailsSauce[] lccd=(CommandeClientDetailsSauce[])listeDetailsCommande.get(listeCommande2[i].getId());
                                                            if(lccd.length>0){
                                                            for(int j=0;j<lccd.length;j++){
                                                        %>
                                                        <div class="content_match-list col-xs-12">
                                                            <div class="content_match-infos col-xs-4 col-sm-12">
                                                                <div class="col-xs-12 content-match-score-all">
                                                                    <p class="col-xs-4 achatLibelle">Produit</p>
                                                                    <p class="col-xs-2 achatLibelle">Sauce et acompagnement</p>
                                                                    <p class="col-xs-2 achatLibelle">Prix</p>
                                                                    <p class="col-xs-2 achatLibelle">Quantit�</p>
                                                                    <p class="col-xs-2 achatLibelle">Etat</p>
                                                                </div>
                                                                <div class="col-xs-12 content-match-score-all" style="font-size: 14px;">
                                                                    <p class="col-xs-2">
                                                                        <img class="drapeau img-thumbnail content_match-team-drapeau" src="assets/img/plats/<%=listePhotos.get(lccd[j].getProduit())[0]%>"/>
                                                                    </p>
                                                                    <p class="col-xs-2">                                                                      
                                                                        <%=lccd[j].getProduit() %>
                                                                    </p>
                                                                    <p class="col-xs-2"><%=lccd[j].getIdaccompagnement() %></p>
                                                                    <p class="col-xs-2"><%=Utilitaire.formaterAr(lccd[j].getPu()) %></p>
                                                                    <p class="col-xs-2"><%=lccd[j].getQuantite() %></p>
                                                                    <p class="col-xs-2"><%= ConstanteEtat.etatToChaine(String.valueOf(lccd[j].getEtat())) %></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%}}%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <% } %>
                                </div>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>