<%-- 
    Document   : as-commande-liste
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<%
    try {

        CommandeClientDetailsTypeP p = new CommandeClientDetailsTypeP();
        String nomTable = "vue_cmd_clt_dtls_typeP_cloture";

        /*if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0) {
            nomTable = request.getParameter("table");
        }*/
        p.setNomTable(nomTable);

        String listeCrt[] = {"id", "produit", "datecommande", "nomtable"};
        String listeInt[] = { "datecommande"};
        String libEntete[] = { "id","quantite" ,"produit", "acco_sauce", "nomtable", "heureliv",   "etat"};
        
        PageRecherche pr = new PageRecherche(p, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        //pr.setAWhere(" order by id desc");
        pr.setApres("commande/as-commande-liste-details.jsp");
        String[] colSomme = {};
        pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande d�tail</h1>
    </section>
    <section class="content">
    <form action="<%=pr.getLien()%>?but=commande/as-commande-liste-details.jsp" method="post" name="incident" id="incident">
        <%
            out.println(pr.getFormu().getHtmlEnsemble());
        %>
        <div class="row col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                Etat : 
                <select name="table" class="champ" id="table" disabled="">
                    <option value="" selected>Clotur&eacute;</option>
                </select>
            </div>
        </div>
    </form>
    <form action="<%=pr.getLien()%>?but=modifierEtatMultiple.jsp" method="post" name="incident" id="incident">
        <%  
            String lienTableau[] = {pr.getLien() + "?but=commande/as-commande-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            // { "quantite" ,"produit", "nomtable", "heureliv",  "acco_sauce", "etat","id"};
            String libEnteteAffiche[] = {"Id", "Quantite","Produit","Sauce accompagnement", "Table","Heure",  "Etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            pr.getTableau().setNameBoutton("Pr&ecirc;t &agrave; livrer");
            pr.getTableau().setNameActe("fait");
            out.println(pr.getTableau().getHtmlWithCheckbox());
        %>
        <input type="hidden" name="bute" value="commande/as-commande-liste-details.jsp"/>
    </form>
    <% out.println(pr.getBasPage()); %>
    </section>
</div>

<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>