<%@page import="mg.cnaps.rappro.RapproCompte"%>
<%@page import="java.sql.Connection"%>
<%@page import="mg.cnaps.rappro.RapproService"%>
<%@page import="mg.cnaps.rappro.RapprochementGrandLivre"%>
<%@page import="mg.cnaps.rappro.RapproReleve"%>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="bean.*" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="affichage.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String acteG=null;
        String lien = null;
        String bute;
        String buteG=null;
        String nomtable;
        String typeBoutton;
        String id;
        String idgl[];
        String idr[];
    %>
    <%
        try {
            nomtable = request.getParameter("nomtable");
            typeBoutton = request.getParameter("type");
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            bute = request.getParameter("bute");
            //buteG=request.getParameter("buteG");
            id = request.getParameter("id");
           // acte=request.getParameter("acteG");
           // System.out.println("acteG="+acteG);
            System.out.println("acte1="+acte);
            idgl=request.getParameterValues("manuelg");
            idr=request.getParameterValues("manuelr");
            /*System.out.println("idgl1"+idgl[0]);
            System.out.println("idgl2"+idgl[1]);
            System.out.println("idr1"+idr[0]);
            System.out.println("idr2"+idr[1]);*/
          /*  System.out.println("idrel1="+idr[0]);
            System.out.println("idrel1="+idr[1]);
            System.out.println("idrel1="+idr[2]);
            System.out.println("ireto ny id grand livre:");
            System.out.println("idgl1="+idgl[0]);
            System.out.println("idgl1="+idgl[1]);
            System.out.println("idgl1="+idgl[2]);
           // System.out.println("idreleve="+idr[0]);*/
            Object temp = null;
            String[] rajoutLien = null;
            String classe = request.getParameter("classe");
            ClassMAPTable t = null;
            String tempRajout = request.getParameter("rajoutLien");
            String idouverture = request.getParameter("id");
            String val = idouverture;
            session.setAttribute("idreleve", idr);
            String rajoutLie = "";
            if (tempRajout != null && tempRajout.compareToIgnoreCase("") != 0) {
                rajoutLien = utilitaire.Utilitaire.split(tempRajout, "-");
            }
            if (bute == null || bute.compareToIgnoreCase("") == 0) {
                bute = "pub/Pub.jsp";
            }

            if (classe == null || classe.compareToIgnoreCase("") == 0) {
                classe = "pub.Montant";
            }

            if (typeBoutton == null || typeBoutton.compareToIgnoreCase("") == 0) {
                typeBoutton = "3"; //par defaut modifier
            }

            int type = Utilitaire.stringToInt(typeBoutton);
            if (acte.compareToIgnoreCase("rapprocher") == 0) {
                RapproReleve [] releve=null;
                RapprochementGrandLivre [] gl=null;
                try
                {
                    String [] idrelv={"id"};
                    String [] idgrangliv={"id"};
                    RapproReleve rr = new RapproReleve();
                    rr.setNomTable("RAPPRO_RELEVE_LIBELLE");
                    RapproCompte c =new RapproCompte();
                    c.setNomTable("RAPPRO_COMPTE_LIBELLE");
                    RapproCompte[] listecompte =(RapproCompte[]) CGenUtil.rechercher(c, null, null, "");
                    int lc=listecompte.length;
                    int idc=lc-1;
                    String [] att=null;
                    RapproCompte [] compt=null;
                    for(int i=0;i<idgl.length;i++)
                    {
                        gl=(RapprochementGrandLivre []) CGenUtil.rechercher(new RapprochementGrandLivre() , null, null, " and id='"+idgl[i]+"' and compte ='"+listecompte[idc].getCompte()+"' and id not in (select idsousecriture from rappro_sous_ecriture) order by id desc");
                    }
                    for(int i=0;i<idr.length;i++)
                    {
                        releve =(RapproReleve [])CGenUtil.rechercher(rr, null, null, " and compte='"+listecompte[idc].getCompte()+"' and id='"+idr[i]+"' and id not in (select idreleve from rappro_sous_releve)");
                    }
         
                    if(releve!=null && gl!=null)
                    {   
                        /* RapproReleve [] r=(RapproReleve[])AdminGen.findAvecOrder(releve,idrelv , idr);
                        System.out.println("tailleReleve="+r.length);
                        RapprochementGrandLivre [] glc=(RapprochementGrandLivre [] )AdminGen.findAvecOrder(gl,idgrangliv , idgl);
                         System.out.println("tailleReleve="+glc.length);*/
                        RapproService.rapprocher(releve, gl, "", u);
                         %>
                    <script language="JavaScript"> document.location.href = "<%=lien%>?but=rappro/lettrage/rapprochement.jsp";</script>
                 <%
                    }
                   
                    else
                    {
                        throw new Exception("rapprochement non valide");
                    }
                    
                }
                catch(Exception ex)
                {
                    throw ex;
                }
                 
            }
            if(acte.compareToIgnoreCase("generer")==0){
                try{
                    String idreleve=idr[0];
                    session.setAttribute("idr", idreleve);
                 %>
                    <script language="JavaScript"> document.location.href = "<%=lien%>?but=compta/ecriture/saisie_ecriture_compta.jsp&idreleve=<%=idr[0]%>";</script>
                 <%
                }
                catch(Exception e)
                {
                    throw e;
                }
                
            }
            
            if(acte.compareToIgnoreCase("saisieReleve")==0){
                try{
                 %>
                    <script language="JavaScript"> document.location.href = "<%=lien%>?but=rappro/releve/releve-saisie.jsp";</script>
                 <%
                }
                catch(Exception e)
                {
                    throw e;
                }
                
            }
            if(acte.compareToIgnoreCase("filtrer")==0)
            {
                RapproReleve [] releve=null;
                RapprochementGrandLivre [] gl=null;
                RapproCompte [] listec=null;
                double soldeInitialgl=0;
                double soldeInitialrl=0;
                double soldefinalRl=0;
                double soldefinalgl=0;
                RapproReleve rr = new RapproReleve();
                rr.setNomTable("RAPPRO_RELEVE_LIBELLE");
                RapproCompte c =new RapproCompte();
                c.setNomTable("RAPPRO_COMPTE_LIBELLE");
                RapproCompte[] listecompte =(RapproCompte[]) CGenUtil.rechercher(c, null, null, "order by id");
                int lc=listecompte.length;
                int idc=lc-1;
                String [] att={"compte"};
                String [] valAtt={""+listecompte[idc].getCompte()+""};
                System.out.println("comptefarany="+listecompte[idc].getCompte());
                RapproCompte [] compt= (RapproCompte [])AdminGen.findAvecOrder(listecompte, att, valAtt);
                
                int l=compt.length;
                if(l>1)
                {
                    soldeInitialrl= compt[l-2].getSoldebanque();
                    soldeInitialgl= compt[l-2].getSoldecompta();
                    releve =(RapproReleve [])CGenUtil.rechercher(rr, null, null, "and compte='"+compt[l-1].getCompte()+"' and id not in (select idreleve from rappro_sous_releve)");
                    gl=(RapprochementGrandLivre []) CGenUtil.rechercher(new RapprochementGrandLivre() , null, null, " and compte ='"+compt[l-1].getCompte()+"' and id not in (select idsousecriture from rappro_sous_ecriture)");
                    soldefinalRl=RapproService.calculsoldefinalReleve(soldeInitialrl, releve);
                    soldefinalgl=RapproService.calculsoldefinalgl(soldeInitialgl, gl);
                    RapproCompte cpt=new RapproCompte();
                    listec=(RapproCompte [])CGenUtil.rechercher(cpt, null, null, "order by id");
                    if(compt[l-1].getSoldebanque()==0 && compt[l-1].getSoldecompta()==0)
                    { 
                        listec[idc].setSoldebanque(soldefinalRl);
                        listec[idc].setSoldecompta(soldefinalgl);
                        listec[idc].upDateToTable();
                    }
                }
                else if(l==1)
                {
                    soldeInitialrl= compt[l-1].getSoldebanque();
                    soldeInitialgl= compt[l-1].getSoldecompta();
                    releve =(RapproReleve [])CGenUtil.rechercher(rr, null, null, "and compte='"+compt[l-1].getCompte()+"' and id not in (select idreleve from rappro_sous_releve)");
                    gl=(RapprochementGrandLivre []) CGenUtil.rechercher(new RapprochementGrandLivre() , null, null, " and compte ='"+compt[l-1].getCompte()+"' and id not in (select idsousecriture from rappro_sous_ecriture)");
                    soldefinalRl=RapproService.calculsoldefinalReleve(soldeInitialrl, releve);
                    System.out.println("soldefinalRl="+soldefinalRl);
                    soldefinalgl=RapproService.calculsoldefinalgl(soldeInitialgl, gl);
                    System.out.println("soldefinalgl="+soldefinalgl);
                    RapproCompte cpt=new RapproCompte();
                    listec=(RapproCompte [])CGenUtil.rechercher(cpt, null, null, "order by id" );
                    listec[idc].setSoldebanque(soldefinalRl);
                    listec[idc].setSoldecompta(soldefinalgl);
                    listec[idc].upDateToTable();
                    
                }
                
                %>
                    <script language="JavaScript"> document.location.href = "<%=lien%>?but=rappro/lettrage/rapprochement.jsp";</script>
                <%    
                
            }

    } catch (Exception e) {
        e.printStackTrace();
    %>

    <script language="JavaScript"> alert('<%=e.getMessage()%>');
        history.back();</script>
        <%
                return;
            }
        %>
</html>



