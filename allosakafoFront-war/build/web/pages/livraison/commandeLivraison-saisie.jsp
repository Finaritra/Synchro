<%-- 
    Document   : commandeLivraison-saisie
    Created on : 12 mars 2020, 09:58:53
    Author     : Maharo R.
--%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="service.AlloSakafoService"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.commun.Constante"%>

<script>
function livraison(valeur) 
{
    var param = {'telephone':valeur};
    $.ajax({
        type:'GET',
        url:'CompleterLivraison',
        contentType: 'application/json',
        data:param,
        success:function(ma){
            var data = JSON.parse(ma);
            $("#idclient").val(data.client);
            $("#adresse").val(data.adresse);

        }
    });

}
</script>
<div class="container container-index container-mobile">
    <div class="row">
        <div class="col-xs-12 content-login">
            <img src="assets/img/logo.png" id="logo-ketrika" alt="Logo">
            <div class="col-xs-12 content-login-body">
                <div class="col-xs-4 col-sm-4 content-login-form">
                    <form class="form-inline login" action="Livraison" method="POST">
                         <input type="hidden" name="choixresto" value="normal" >
                        <h1>Livraison Commande Saisie</h1>
                        <br>
                        

                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label> Num�ro de t�l�phone/Nom </label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                                <input type="text" name="idclient" id="idclient" placeholder="telephone - nom" onblur="livraison(this.value)">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                             <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label>Adresse de Livraison</label>
                             </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                                <input type="text" name="adresse" id="adresse">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label>Point de Livraison</label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                                <select name="idpoint" id="saisie">
                                     <option value=""> </option>
                                    <%
                                      TypeObjet to=new TypeObjet(); 
                                       to.setNomTable("point");
                                       TypeObjet[] liste=(TypeObjet[])AlloSakafoService.getListe(to,"");
                                           for(TypeObjet dir : liste){
                                    %>
                                    <option value="<%=dir.getId()%>"><%=dir.getVal()%></option>
                                    <% }%>

                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                             <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label>Date</label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                            <input type="text" name="daty" value="<%=Utilitaire.dateDuJour()%>">
                            </div>
                         </div>
                            <br>
                        <div class="row">
                             <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label>Frais de Livraison (Ar)</label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                                <input type="text" name="frais" value="<%=Constante.getFraisLivraison()%>" >
                            </div>
                        </div>
                            <br>
                        <div class="row">
                             <div class="col-sm-5 col-md-5 col-xs-4 col-lg-5">
                                <label>Heure</label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6">
                                <input type="text" name="heure" value="<%=Utilitaire.ajoutHeure(Utilitaire.heureCouranteHM(), 0, Constante.dureeLivraisonDefaut  ,0) %>">
                            </div>
                        </div>
                        <div class="row">
                        </div>
                      </br>
                        <div class="login-connect col-xs-12">
                          <button type="submit" name="defaulttable" class="btn btn-success col-xs-12 btn-choix-table"><strong>Valider</strong></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
