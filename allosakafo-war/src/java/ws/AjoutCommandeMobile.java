/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mg.allosakafo.commande.CommandeClientDetails;
import mg.allosakafo.commande.CommandeClientTotal;
import utilitaire.UtilDB;
import com.google.gson.Gson;
import java.util.HashMap;
import mg.allosakafo.commande.CommandeClient;
import utilitaire.Utilitaire;
import bean.CGenUtil;
import bean.ClassMAPTable;
import mg.allosakafo.commande.CommandeService;
import mg.allosakafo.produits.Produits;
import mg.allosakafo.produits.ProduitsLibelle;
/*
 * @author Jessi
 */
@WebServlet(name = "AjoutCommandeMobile", urlPatterns = {"/AjoutCommandeMobile"})
public class AjoutCommandeMobile extends HttpServlet {
    private Gson gson = new Gson();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Data json = null;
        try {
            CommandeClientTotal total = CommandeService.ajouterCommandeMobile(request.getParameter("idplat"), request.getParameter("idclient"),Utilitaire.stringToDouble(request.getParameter("qte")),request.getParameter("sauce"),request.getParameter("remarque"));
            out.print(gson.toJson(total));
        }catch(Exception e){
            e.printStackTrace();
            json = new Data(null, new Error(1,"","Exception",e.getMessage()));
            out.print(gson.toJson(json));
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
