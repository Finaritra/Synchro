/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import com.google.gson.annotations.Expose;
import bean.ClassMAPTable;

/**
 *
 * @author rakotondralambo
 */
public class Data {
    @Expose
    private ClassMAPTable[] data;
    @Expose
    private String token;
    @Expose
    private Error error;

    public Data(ClassMAPTable[] data ,Error error) {
        this.setData(data);
        this.setToken("");
        this.setError(error);
    }

    public ClassMAPTable[] getData() {
        return data;
    }

    public void setData(ClassMAPTable[] data) {
        this.data = data;
    }
    
    public void setToken(String token){
        this.token = token;
    }
    
    public String getToken(){
        return this.token;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Error getError() {
        return error;
    }
    
    
    
}
