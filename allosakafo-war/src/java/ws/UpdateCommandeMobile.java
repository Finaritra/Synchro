/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import bean.CGenUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mg.allosakafo.commande.CommandeClient;
import utilitaire.UtilDB;
import com.google.gson.Gson;
import mg.allosakafo.commande.CommandeClientDetails;
import utilitaire.Utilitaire;

/**
 *
 * @author HP
 */
@WebServlet(name = "UpdateCommandeMobile", urlPatterns = {"/UpdateCommandeMobile"})
public class UpdateCommandeMobile extends HttpServlet {
private Gson gson = new Gson();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Data json = null;
        Connection c = null;
        
        String acte=null;
        String idDetail=null;
        String quantiter=null;
        try {
            if(c == null) c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            
            acte=request.getParameter("acte");
            idDetail = request.getParameter("id");
            quantiter = request.getParameter("qte");
            if(acte.compareTo("update")==0){
                CommandeClientDetails cmd = new CommandeClientDetails();
                cmd.setId(idDetail);
                    CommandeClientDetails[] cmds = (CommandeClientDetails[]) CGenUtil.rechercher(cmd, null, null, c,"");
                    if(cmds.length<=0){
                        throw new Exception("Commande non trouv�");
                    }else{
                        cmd=cmds[0];
                    }
                cmd.setQuantite(Utilitaire.stringToDouble(quantiter));
                cmd.updateToTable(c);
                out.print(gson.toJson(cmd));
            }else if(acte.compareTo("suppr")==0){
                CommandeClientDetails cmd = new CommandeClientDetails();
                cmd.setId(idDetail);
                    CommandeClientDetails[] cmds = (CommandeClientDetails[]) CGenUtil.rechercher(cmd, null, null, c,"");
                    if(cmds.length<=0){
                        throw new Exception("Commande non trouv�");
                    }else{
                        cmd=cmds[0];
                    }
                cmd.deleteToTable(c);
                out.print(gson.toJson(cmd));
            }else{
                throw new Exception("Action non implementer");
            }
        }catch(Exception e){
            e.printStackTrace();
            json = new Data(null, new Error(1,"","Exception",e.getMessage()));
            out.print(gson.toJson(json));
        }finally{
            try{
                if(c != null) c.close();
            }catch(Exception e){ }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
