/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import bean.CGenUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Time;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mg.allosakafo.produits.Produits;
import mg.allosakafo.produits.ProduitsPrix;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import user.UserEJB;
import utilitaire.Utilitaire;

/**
 *
 * @author ordi
 */
@WebServlet("/UpdateUploadDownloadFileServlet")
public class UpdateUploadDownloadFileServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private ServletFileUpload uploader = null;

    private void createDir(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    @Override
    public void init() {
        DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        File filesDir = (File) getServletContext().getAttribute(StringUtil.FILES_DIR_FILE);
        fileFactory.setRepository(filesDir);
        this.uploader = new ServletFileUpload(fileFactory);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            
            String fileName = request.getParameter(StringUtil.FILE_NAME);
           if (fileName == null || fileName.equals("")) {
                throw new ServletException("File Name can't be null or empty");
            }
            File file = new File(request.getServletContext().getAttribute(StringUtil.FILES_DIR) + fileName);

            if (!file.exists()) {
                throw new ServletException("File doesn't exists on server.");
            }

            InputStream inputStream = new FileInputStream(file);
            ServletContext context = getServletContext();
            String mimeType = context.getMimeType(file.getAbsolutePath());

            response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
            response.setContentLength((int) file.length());
            response.setHeader("Content-Disposition", "attachment; name=\"" + fileName + "\"");

            ServletOutputStream os = response.getOutputStream();
            byte[] bufferData = new byte[1024];
            int read;
            while ((read = inputStream.read(bufferData)) != -1) {
                os.write(bufferData, 0, read);
            }
            os.flush();
            os.close();
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        HashMap<String, String> params = new HashMap<>();
        HashMap<String, String> fichiers = new HashMap<>();
        try {
            
            String dossier = request.getParameter(StringUtil.DOSS);

            if (!ServletFileUpload.isMultipartContent(request)) {
                throw new ServletException("Content type is not multipart/form-data");
            }

            String path = request.getSession(true).getServletContext().getRealPath("/")+File.separator+".."+File.separator+"Dossier.war"+File.separator+"img"+File.separator+"plats"+ File.separator + dossier;
            createDir(path);
            List<FileItem> fileItemsList = uploader.parseRequest(request);
              System.out.println(" path fileName = "+path);
            String filenames = request.getParameter("photo2");
            String dateTimeInsert = "";
            for (FileItem fileItem : fileItemsList) {
                if (fileItem.getName() != null && !fileItem.isFormField() && !fileItem.getString().isEmpty()) {
                    dateTimeInsert = Utilitaire.heureCourante() + Utilitaire.dateDuJour();
                    
                    String name = dateTimeInsert + fileItem.getName();
                    String nameVal = name
                            .replace(":", "")
                            .replace("/", "-")
                            .replace(" ", "");
                    filenames = nameVal;
                    
                    dateTimeInsert = dateTimeInsert
                            .replace(":", "")
                            .replace("/", "-")
                            .replace(" ", "");
                    
                    String dir = path + File.separator + nameVal;
                    File file = new File(dir);
                    fileItem.write(file);
                    fichiers.put(nameVal, fileItem.getFieldName());
                } else {
                    params.put(fileItem.getFieldName(), fileItem.getString());
                }
            }
//            filenames = params.containsKey("photo2") ? params.get("photo2") : filenames ;
                filenames = filenames == null || filenames.compareToIgnoreCase("")==0 ? params.get("photo2") : filenames ;
//            if(params.containsKey("photo2")) 
//                filenames = params.get("photo2");
//            else if (filenames.equals(dateTimeInsert))
//                filenames = params.get("photo");
            this.updateTheObject(dossier, params, filenames, (UserEJB) request.getSession().getAttribute("u"));
            response.sendRedirect("/phobo/pages/module.jsp?but=" + params.get("bute"));
        
        } catch (Exception e) {
            e.printStackTrace();
            out.println("<script language='JavaScript'> alert('"+e.getMessage()+"');history.back(); </script>");
            out.write("Exception in uploading file. Cause : " + e.getMessage());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    private void updateTheObject(String dossier, HashMap<String, String> params, String filenames, UserEJB u) throws Exception {
         switch (dossier) {
            case "produit" :
                    String nom = params.get("nom"),
                        designation = params.get("designation"),
                        id = params.get("id"),
                        type = params.get("typeproduit"),
                        calorie = params.get("calorie"),
                        photo = filenames;
                    double poids=Utilitaire.stringToDouble(params.get("poids")),
                            prixl=Utilitaire.stringToDouble(params.get("prixl"));
                            
                    Produits produit = new Produits();
                    produit.setId(id);
                    produit.setNom(nom);
                    produit.setCalorie(calorie);
                    produit.setTypeproduit(type);
                    produit.setPoids(poids);
                    produit.setDesignation(designation);
                    produit.setPhoto(photo);
                    produit.setPrixl(prixl);
                    
                    u.updateObject(produit);
                break;
        }
    }
}
