/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import bean.CGenUtil;
import bean.ClassMAPTable;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mg.allosakafo.commande.CommandeMobileDetails;
import utilitaire.UtilDB;
import com.google.gson.Gson;
import java.util.HashMap;
import mg.allosakafo.commande.CommandeMobile;
import utilitaire.Utilitaire;
import ws.NewWSEndpoint.WSocketSetting;

/**
 *
 * @author HP
 */
@WebServlet(name = "AjouterCommande", urlPatterns = {"/AjouterCommande"})
public class AjouterCommande extends HttpServlet {
    private Gson gson = new Gson();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Data json = null;
        Connection c = null;
        try {
            if(c == null) c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            //get Current Table ID if not exist create table
            CommandeMobile cmd = new CommandeMobile();
            cmd.setTables(request.getParameter("table"));
            
            CommandeMobile[] cmds = (CommandeMobile[]) CGenUtil.rechercher(cmd, null, null, c, "");
            if(cmds.length<=0){
                //Creer commande
                cmd.construirePK(c);
                cmd.setDatecommande(Utilitaire.dateDuJourSql());
                cmd.setNumcommande(cmd.getId());
                cmd.setDatesaisie(cmd.getDatecommande());
                cmd.setTypecommande("TPC00002");
                cmd.setEtat(1);
                cmd.insertToTable(c);
            }else{
                cmd=cmds[0];
            }
                //Ajouter commande
                CommandeMobileDetails p = new CommandeMobileDetails();
                p.construirePK(c);
                p.setIdmere(cmd.getId());
                p.setProduit(request.getParameter("idplat"));
                p.setQuantite(Utilitaire.stringToDouble(request.getParameter("qtt")));
                p.setObservation("");
                p.insertToTable(c);
            //Send to Cuisine socket server
                /*
                CommandeMobileDetails cd = new CommandeMobileDetails();
                cd.setIdmere(cmd.getId());
                cmd.setDetails((CommandeMobileDetails[]) CGenUtil.rechercher(cmd, null, null, c, ""));
                */
                /*CommandeMobileDetails[] tab = {p};
                cmd.setDetails(tab);*/
                
                HashMap<String,String> message = new HashMap();
                message.put("table", cmd.getTables());
                message.put("commande", cmd.getId());
                message.put("idDetail", p.getId());
                message.put("plat", p.getProduit());
                message.put("quantiter", ""+p.getQuantite());
                message.put("action", "ajout");
                
                //WSocketSetting.endpoint.sendMessage(message);
            
            c.commit();
            //Return idCommande
            out.print(gson.toJson(cmd.getId()));
        }catch(Exception e){
            e.printStackTrace();
            json = new Data(null, new Error(1,"","Exception",e.getMessage()));
            out.print(gson.toJson(json));
        }finally{
            try{
                if(c != null) c.close();
            }catch(Exception e){ }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
