package ws;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import service.UploadService;
import user.UserEJB;
import utilitaire.Utilitaire;

@WebServlet("/UploadFileServlet")
public class UploadFileServlet extends HttpServlet {
    private static final long              serialVersionUID = 1L;
    private              ServletFileUpload uploader         = null;

    private void createDir(String path) {
        File file = new File(path);
        if (!file.exists()) file.mkdirs();
    }

    @Override
    public void init() {
        DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        File                filesDir    = (File) getServletContext().getAttribute(StringUtil.FILES_DIR_FILE);
        fileFactory.setRepository(filesDir);
        this.uploader = new ServletFileUpload(fileFactory);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fileName = request.getParameter(StringUtil.FILE_NAME);

            if (fileName == null || fileName.equals("")) {
                throw new ServletException("File Name can't be null or empty");
            }
            File file = new File(request.getServletContext().getAttribute(StringUtil.FILES_DIR) + fileName);

            if (!file.exists()) {
                throw new ServletException("File doesn't exists on server.");
            }

            InputStream    inputStream = new FileInputStream(file);
            ServletContext context     = getServletContext();
            String         mimeType    = context.getMimeType(file.getAbsolutePath());


            response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
            response.setContentLength((int) file.length());
            response.setHeader("Content-Disposition", "attachment; name=\"" + fileName + "\"");

            ServletOutputStream os         = response.getOutputStream();
            byte[]              bufferData = new byte[1024];
            int                 read;
            while ((read = inputStream.read(bufferData)) != -1) {
                os.write(bufferData, 0, read);
            }
            os.flush();
            os.close();
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter             out      = response.getWriter();
        HashMap<String, String> params   = new HashMap<>();
        HashMap<String, String> fichiers = new HashMap<>();
        try {
            if (!ServletFileUpload.isMultipartContent(request)) {
                throw new ServletException("Content type is not multipart/form-data");
            }
            String dossier = request.getParameter(StringUtil.DOSS);
            String path    = request.getServletContext().getAttribute(StringUtil.FILES_DIR) + File.separator + dossier;
            createDir(path);
            System.out.println("pathhhhhh : --------"+path);
            List<FileItem> fileItemsList = uploader.parseRequest(request);

            for (FileItem fileItem : fileItemsList) {
                if (fileItem.getName() != null && !fileItem.isFormField()) {
                    String name = Utilitaire.heureCourante() + Utilitaire.dateDuJour() + fileItem.getName();
                    String nameVal = name
                            .replace(":", "")
                            .replace("/", "-")
                            .replace(" ", "");

                    String dir  = path + File.separator + nameVal;
                    System.out.println("dirrr : ---"+dir);
                    File   file = new File(dir);
                    fileItem.write(file);
                    fichiers.put(nameVal, fileItem.getFieldName());
                } else {
                    params.put(fileItem.getFieldName(), fileItem.getString());
                }
            }
            //AttacherFichier fichier
            String iddossier            = params.get("dossier");
            String natureDuDossierchoix = params.get("natureDuDossierchoix");
            String lienenf              = "";
            if (iddossier != null)
            { lienenf = "&iddossier=" + iddossier; }

            if (natureDuDossierchoix != null)
            { lienenf = "&natureDuDossierchoix=" + natureDuDossierchoix; }

            UserEJB  u    = (UserEJB) request.getSession().getAttribute("u");
            Iterator it   = fichiers.entrySet().iterator();

//            u.createUploadedPjService(iddossier, params, it, params.get("nomtable"), params.get("procedure"), params.get("id"));
            System.out.println("----but "+params.get("bute"));
            String mere=null;
            String lienRedirection = "/phobo/pages/module.jsp?but=" + params.get("bute");
            if(params.get("id")!=null){
                lienRedirection += "&id=" + params.get("id");
                mere=params.get("id");
            }
            if(params.get("idDir")!=null){
                lienRedirection += "&idDir=" + params.get("idDir");
                mere=params.get("idDir");
            }
            if(params.get("tab")!=null){
                lienRedirection += "&tab=" + params.get("tab");
            }
            UploadService.createUploadedPj(iddossier, u.getUser().getTuppleID(), params, it, params.get("nomtable"), params.get("procedure"), mere);
            response.sendRedirect(lienRedirection);
        } catch (Exception e) {
            e.printStackTrace();
            out.write("Exception in uploading file. Cause : " + e.getMessage());
            //throw e;
        } finally {
            if (out != null) out.close();
        }
    }

}
