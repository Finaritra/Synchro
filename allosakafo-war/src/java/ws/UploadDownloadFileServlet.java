package ws;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mg.allosakafo.produits.ProduitsPrix;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import user.UserEJB;
import utilitaire.Utilitaire;

@WebServlet("/UploadDownloadFileServlet")
public class UploadDownloadFileServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private ServletFileUpload uploader = null;

    private void createDir(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    @Override
    public void init() {
        DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        File filesDir = (File) getServletContext().getAttribute(StringUtil.FILES_DIR_FILE);
        fileFactory.setRepository(filesDir);
        this.uploader = new ServletFileUpload(fileFactory);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fileName = request.getParameter(StringUtil.FILE_NAME);

            if (fileName == null || fileName.equals("")) {
                throw new ServletException("File Name can't be null or empty");
            }
            File file = new File(request.getServletContext().getAttribute(StringUtil.FILES_DIR) + fileName);

            if (!file.exists()) {
                throw new ServletException("File doesn't exists on server.");
            }

            InputStream inputStream = new FileInputStream(file);
            ServletContext context = getServletContext();
            String mimeType = context.getMimeType(file.getAbsolutePath());

            response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
            response.setContentLength((int) file.length());
            response.setHeader("Content-Disposition", "attachment; name=\"" + fileName + "\"");

            ServletOutputStream os = response.getOutputStream();
            byte[] bufferData = new byte[1024];
            int read;
            while ((read = inputStream.read(bufferData)) != -1) {
                os.write(bufferData, 0, read);
            }
            os.flush();
            os.close();
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println(" ato am dopost");
        PrintWriter out = response.getWriter();
        HashMap<String, String> params = new HashMap<>();
        HashMap<String, String> fichiers = new HashMap<>();
        try {
            if (!ServletFileUpload.isMultipartContent(request)) {
                throw new ServletException("Content type is not multipart/form-data");
            }

            String dossier = request.getParameter(StringUtil.DOSS);
            String path = request.getSession(true).getServletContext().getRealPath("/")+File.separator+".."+File.separator+"Dossier.war"+File.separator+"img"+File.separator+"plats"+ File.separator + dossier;
//            String path = request.getServletContext().getRealPath("../pho.war")+"\\assets\\img\\plats"+ File.separator + dossier;
            createDir(path);
            System.out.println(path);
            List<FileItem> fileItemsList = uploader.parseRequest(request);

            String filenames = "";
            for (FileItem fileItem : fileItemsList) {
                if (fileItem.getName() != null && !fileItem.isFormField()) {

                    String name = Utilitaire.heureCourante() + Utilitaire.dateDuJour() + fileItem.getName();

                    String nameVal = name
                            .replace(":", "")
                            .replace("/", "-")
                            .replace(" ", "");

                    filenames = nameVal;
                    String dir = path + File.separator + nameVal;
                    
                    System.out.println("dir : "+dir);
                    
                    File file = new File(dir);
                    fileItem.write(file);
                    fichiers.put(fileItem.getFieldName(), nameVal);
                } else {
                    params.put(fileItem.getFieldName(), fileItem.getString());
                }
            }

            //System.out.println("****** aty ambany filenames = " +  filenames);
            
            UserEJB u = (UserEJB) request.getSession().getAttribute("u");
            //Iterator it = fichiers.entrySet().iterator();            
            //u.createUploadedPjService(params, it, params.get("nomtable"), params.get("procedure"), params.get("id"));
            
            this.createTheObject(dossier, params, fichiers, filenames, u, response);
            response.sendRedirect("/phobo/pages/module.jsp?but=" + params.get("bute"));
            
        } catch (Exception e) {
            out.println("<script language='JavaScript'> alert('" + e.getMessage() + "');history.back(); </script>");
            //out.write("Exception in uploading file. Cause : " + e.getMessage());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    private void createTheObject(String dossier, HashMap<String, String> params, HashMap<String, String> fichiers, String filenames, UserEJB u,HttpServletResponse response) throws Exception {
        try{
            switch (dossier) {
                case "produit" :
                    String nom = params.get("nom"),
                        designation = params.get("designation"),
                        type = params.get("typeproduit"),
                        calorie = params.get("calorie"),
                        pa = params.get("pa"),
                        observation = params.get("designation"),
                        photo = filenames;
                    System.out.println("Prix L "+params.get("prixl"));
                    double montant=Utilitaire.stringToDouble(params.get("montant")),
                            prixl=Utilitaire.stringToDouble(params.get("prixl")),
                            poids=Utilitaire.stringToDouble(params.get("poids"));
                    
                    Date dateapplication = Utilitaire.stringDate(params.get("dateapplication")) ;
                            
                    ProduitsPrix produit = new ProduitsPrix();
                    produit.setNom(nom);
                    produit.setDateapplication(dateapplication);
                    produit.setCalorie(calorie);
                    produit.setMontant(montant);
                    produit.setTypeproduit(type);
                    produit.setObservation(observation);
                    produit.setPa(pa);
                    produit.setPoids(poids);
                    produit.setDesignation(designation);
                    produit.setPhoto(photo);
                    produit.setPrixl(prixl);
                    u.createObject(produit);
                break;
            }         
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
