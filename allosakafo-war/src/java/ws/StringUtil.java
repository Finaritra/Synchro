package ws;

class StringUtil {
    public static final String FILES_DIR_FILE = "FILES_DIR_FILE";
    public static final String FILES_DIR      = "FILES_DIR";
    public static final String FILE_NAME      = "fileName";
    public static final String PATH_DIR       = System.getProperty("jboss.server.base.dir") + "/deployments/dossier.war";
    public static final String DOSS           = "dossier";
}
