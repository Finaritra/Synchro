/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utilitaire.*;

/**
 *
 * @author Jetta
 */
@WebServlet(name = "SupplementaireSaisie", urlPatterns = {"/SupplementaireSaisie"})
public class SupplementaireSaisie extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        try {
            String idp = request.getParameter("id_pers");
            java.sql.Date dateSupp = Utilitaire.string_date("-",request.getParameter("date_supp"));
            String hdeb = request.getParameter("heure_debut");
            String hfin = request.getParameter("heure_fin");
			System.out.println(request.getParameter("ferie"));
			int ferie = 0;
            response.sendRedirect("/cmcm-war/pages/module.jsp?but=paie/supplementaire-saisie.jsp");
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
		try {
            String idp = request.getParameter("id_pers");
            java.util.Date dateSupp = Utilitaire.stringToDate("/",request.getParameter("date_supp"));
            String hdeb = request.getParameter("heure_debut");
            String hfin = request.getParameter("heure_fin");
			String fer = request.getParameter("ferie");
			int ferie = 0;
			if(fer != null){
				ferie = 1;
			}
            System.out.println(ferie);
            response.sendRedirect("/cmcm-war/pages/module.jsp?but=paie/supplementaire-saisie.jsp");
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
