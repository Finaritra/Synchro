/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mg.cnaps.utilisateur.CNAPSUser; 
import user.UserEJB;

/**
 *
 * @author user
 */
@WebServlet(name = "NewUserbo", urlPatterns = {"/NewUserbo"})
public class NewUserbo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        UserEJB u = (UserEJB) session.getAttribute("u"); 
        try {

            String nomuser = (String) request.getParameter("idjoueur");
            String login = (String) request.getParameter("login");
            String pwduser = (String) request.getParameter("pwduser");
            String idrole = (String) request.getParameter("idrole");
            String teluser = (String) request.getParameter("teluser");
            String adr = (String) request.getParameter("adr");

            String code_dr = (String) request.getParameter("code_dr");
            String user_init = (String) request.getParameter("user_init");
            String agent_mo = (String) request.getParameter("agent_mo");
            String agent_disponible = (String) request.getParameter("agent_disponible");
            String username = (String) request.getParameter("username");
            String code_service = (String) request.getParameter("code_service");
            String id = u.createUtilisateurs(login, pwduser, nomuser, adr, teluser, idrole);
            username = login;
            CNAPSUser cu = new CNAPSUser(code_dr, user_init, agent_mo, agent_disponible, username, code_service, id);
           try{
               u.createObject(cu);
           }catch(Exception e){
               if(id!=null)u.deleteUtilisateurs(id);
               throw e;
           }
            /*cu.construirePK(null);
            CGenUtil.save(cu);*/

            String lien = (String) request.getSession().getAttribute("lien");
            response.sendRedirect("/cnaps-war/pages/" + lien + "?but=utilisateur/utilisateur-saisie.jsp");

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage());

        }

    }

  
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
