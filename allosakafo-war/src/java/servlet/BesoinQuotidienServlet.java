/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.AlloSakafoService;
import user.UserEJB;

/**
 *
 * @author Notiavina
 */
@WebServlet(name = "BesoinQuotidienServlet", urlPatterns = {"/BesoinQuotidienServlet"})
public class BesoinQuotidienServlet extends HttpServlet{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {
            System.out.println("TAFIDITRA ATO: BesoinQuotidienServlet");
            UserEJB u = (UserEJB) request.getSession().getAttribute("u");
            String acte = request.getParameter("acte");
            String bute = request.getParameter("bute");
            switch(acte){
                case "ajout":
                    String daty = request.getParameter("date");
                    String[] idunites = request.getParameterValues("idunite");
                    String[] quantites = request.getParameterValues("quantite");
                    String[] idingredients = request.getParameterValues("id");
                    String[] qteRestants = request.getParameterValues("restant");
                    AlloSakafoService.ajoutBesoinQuotidienMultiple(daty, idunites, quantites, idingredients, qteRestants, u);
                break;
                case "saisieProduit":
                    String[] idbesoins = request.getParameterValues("id");
                    String[] quantiters = request.getParameterValues("quantite");
                    AlloSakafoService.updateBesoin(idbesoins,quantiters,null);
                break;
                case "achat":
                    String[] idingrs = request.getParameterValues("idingredient");
                    String[] qtes = request.getParameterValues("quantite");
                    String[] idunits = request.getParameterValues("idunite");
                    String[] prix = request.getParameterValues("prixunitaire");
                    String datyAchat = request.getParameter("daty");
                    String[] fournisseur = request.getParameterValues("fournisseur");
                    AlloSakafoService.achatBesoinQuotidien(datyAchat,idingrs,qtes,idunits,prix,fournisseur,u);
                break;
            }
            response.sendRedirect("/phobo/pages/module.jsp?but="+bute);
        } 
        catch (Exception e) {
            e.printStackTrace();
            out.println("<script language='JavaScript'> alert('"+e.getMessage()+"'); history.back();</script>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
