// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   NewUserbo.java

package utilisateur;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import mg.cnaps.utilisateur.CNAPSUser;
import user.UserEJB;

public class NewUserbo extends HttpServlet
{

    public NewUserbo()
    {
    }

    protected void processRequest(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse)
        throws ServletException, IOException
    {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        HttpSession session = request.getSession(true);
        UserEJB u = (UserEJB)session.getAttribute("u");
        try
        {
            String nomuser = request.getParameter("idjoueur");
            String login = request.getParameter("login");
            String pwduser = request.getParameter("pwduser");
            String idrole = request.getParameter("idrole");
            String teluser = request.getParameter("teluser");
            String adr = request.getParameter("adr");
            String code_dr = request.getParameter("code_dr");
            String user_init = request.getParameter("user_init");
            String agent_mo = request.getParameter("agent_mo");
            String agent_disponible = request.getParameter("agent_disponible");
            String username = request.getParameter("username");
            String code_service = request.getParameter("code_service");
            String id = u.createUtilisateurs(login, pwduser, nomuser, adr, teluser, idrole);
            username = login;
            CNAPSUser cu = new CNAPSUser(code_dr, user_init, agent_mo, agent_disponible, username, code_service, id);
            try
            {
                u.createObject(cu);
            }
            catch(Exception e)
            {
                if(id != null)
                    u.deleteUtilisateurs(id);
                throw e;
            }
            String lien = (String)request.getSession().getAttribute("lien");
            response.sendRedirect((new StringBuilder()).append("/cnaps-war/pages/").append(lien).append("?but=utilisateur/utilisateur-saisie.jsp").toString());
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
    }

    public String getServletInfo()
    {
        return "Short description";
    }
}
