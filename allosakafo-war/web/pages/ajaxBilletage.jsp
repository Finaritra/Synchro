<%-- 
    Document   : ajaxBilletage
    Created on : 5 juil. 2016, 10:34:15
    Author     : hp
--%>

<%@page import="bean.CGenUtil"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="bean.AdminGen"%>
<%@page import="mg.cnaps.treso.TresoDescompte"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.cnaps.treso.TresoBillet"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%!
    UserEJB u = null;
    String lien = "";
    TresoBillet[] liste_billet = null;
%>
<%
      try {
        u = (UserEJB) session.getAttribute("u");
        lien = (String) session.getValue("lien");
  
    TresoBillet d = new TresoBillet();
    liste_billet = (TresoBillet[]) u.getData(d, null, null, null, "");

    
    String code_dr = request.getParameter("code_dr");
    
    
    String filtreDr = " and code_dr='" + code_dr + "' ";
    TresoDescompte criteria = new TresoDescompte();
    criteria.setNomTable("TRESO_DESCOMPTE_BILLET");
    TresoDescompte[] liste_descompte = (TresoDescompte[]) CGenUtil.rechercher(criteria, null, null, filtreDr +  " order by billet desc " );


    boolean aucun_resultat = liste_descompte.length == 0; 
    if (aucun_resultat)
    {
        %>
        <tbody>
            <tr >
                
                <td colspan="5" >Aucun resultat pour cette direction r�gional</td>
            </tr>
        </tbody>
        <%
    }else
    {
%>


            <thead>
               <tr>
                   <td>Billet</td>
                   <td>Dispo</td>
                   <td>Montant total Dispo</td>
                   <td>Sortie</td>
                   <td>Montant total Sortie</td>
               </tr>
           </thead>
           <tbody>
                                    
                    
               
                    <% for (int i = 0; i < liste_billet.length; i++) {
                        String[] attributET = {"billet"};
                        String[] valET = {liste_billet[i].getBillet() + ""};
                        TresoDescompte[] liste_dispo = (TresoDescompte[]) AdminGen.findAvecOrder(liste_descompte, attributET, valET);
                    %>
                        <tr>
                            <td style="text-align:right;"><input style="text-align:right;" type="text" placeholder="" class="form-control col-md-4" readonly value="<%=Utilitaire.formaterAr(liste_billet[i].getBillet())%>"/><input type="hidden" name="billet_<%=i%>" id="billet_<%=i%>" placeholder="" class="form-control col-md-4 billet" readonly value="<%=liste_billet[i].getBillet()%>"/></td>
                            <td style="text-align:right;"><input style="text-align:right;" type="text" name="dispo_<%=i%>" id="dispo_<%=i%>" placeholder="" class="form-control col-md-4" readonly value="<%if (liste_dispo.length > 0) {
                                    out.print(Utilitaire.formaterAr(liste_dispo[0].getDispo()));
                                } else {
                                    out.print("0");
                                }%>"/></td>
                            <td style="text-align:right;"><input style="text-align:right;" type="text" name="montantTotalDispo_<%=i%>" id="montantTotalDispo_<%=i%>" placeholder="" class="form-control col-md-4" readonly value="<%if (liste_dispo.length > 0) {
                                    out.print(Utilitaire.formaterAr(liste_dispo[0].getDispo() * liste_billet[i].getBillet()));
                                } else {
                                    out.print("0");
                                }%>"/></td>
                            <td style="text-align:right;"><input style="text-align:right;" type="text" name="sortie_<%=i%>" id="sortie_<%=i%>" placeholder="" onkeyup="calculMontantSortie('<%=i%>')" class="form-control col-md-4 sortie"/></td>
                            <td style="text-align:right;"><input style="text-align:right;" type="text" name="montantTotalSortie_<%=i%>" id="montantTotalSortie_<%=i%>" placeholder="" class="form-control col-md-4 totalsortie"/></td>
                        </tr>
                        <% }%>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right;">Total:</td>
                            <td style="text-align:right;"><input style="text-align:right;" type="text" name="total_montant1" id="total_montant1" placeholder="" class="form-control col-md-4 total_montant1"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align:right;">Reste:</td>
                            <td style="text-align:right;"><input style="text-align:right;" type="text" name="reste" id="reste" placeholder="" class="form-control col-md-4"/></td>
                         </tr>            
                
           
           
           </tbody>
<%  
        }
            
            } catch (Exception e) {
        e.printStackTrace();
        throw e;
    }
%>
