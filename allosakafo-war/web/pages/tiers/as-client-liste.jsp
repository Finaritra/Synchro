<%-- 
    Document   : as-client-liste
    Created on : 1 d�c. 2016, 09:30:49
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.tiers.ClientAlloSakafo"%>
<%@page import="affichage.PageRecherche"%>

<% 
    ClientAlloSakafo lv = new ClientAlloSakafo();
    
	String nomTable = "as_client";
    if(request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0){
		nomTable = request.getParameter("table");
	}
    lv.setNomTable(nomTable);
	
	
    String listeCrt[] = {"nom","sexe", "adresse", "telephone"};
    String listeInt[] = null;
    String libEntete[] = {"id", "nom", "sexe", "datenaissance", "adresse", "telephone", "fb"};

    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    /*
    affichage.Champ[] liste = new affichage.Champ[4];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("unite");
    liste[0] = new Liste("unite", ou, "VAL", "VAL");

    pr.getFormu().changerEnChamp(liste);
    
    pr.getFormu().getChamp("groupee").setLibelleAffiche("Groupe");
    pr.getFormu().getChamp("typearticle").setLibelleAffiche("Type d'article");
    */
    pr.setApres("tiers/as-client-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste client</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=tiers/as-client-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
			<div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_client") == 0) {%>    
                        <option value="as_client" selected>Tous</option>
                        <% } else { %>
                        <option value="as_client" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_clientnondouteux_libelle") == 0) {%>    
                        <option value="as_clientnondouteux_libelle" selected>Client non douteux</option>
                        <% } else { %>
                        <option value="as_clientnondouteux_libelle" >Client non douteux</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_clientdouteux_libelle") == 0) {%>
                        <option value="as_clientdouteux_libelle" selected>Client douteux</option>
                        <% } else { %>
                        <option value="as_clientdouteux_libelle">Client douteux</option>
                        <% } %>
                    </select>
                </div>
                <div class="col-md-4"></div>
            </div>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=tiers/as-client-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Nom & pr�nom", "Genre", "Date de naissance", "Adresse", "T�l�phone", "Facebook"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>