<%-- 
    Document   : as-vehicule-saisie
    Created on : 1 d�c. 2016, 15:20:21
    Author     : Joe
--%>
<%@page import="mg.allosakafo.tiers.Vehicule"%>
<%@page import="user.*"%> 
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    Vehicule  a = new Vehicule();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));    
    
    affichage.Champ[] liste = new affichage.Champ[1];
    
    TypeObjet op = new TypeObjet();
    op.setNomTable("AS_TYPEVEHICULE");
    liste[0] = new Liste("typevehicule", op, "VAL", "id");
    
    pi.getFormu().changerEnChamp(liste);
    pi.getFormu().getChamp("dateacquisition").setLibelle("Date d'acquisition");
    pi.getFormu().getChamp("typevehicule").setLibelle("Type");
    pi.getFormu().getChamp("etat").setVisible(false);
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Enregistrer vehicule</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="starticle" id="starticle">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="tiers/as-vehicule-saisie.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.tiers.Vehicule">
    </form>
</div>