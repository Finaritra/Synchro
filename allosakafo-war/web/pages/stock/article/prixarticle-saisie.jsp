<%-- 
    Document   : prixarticle-saisie
    Created on : 13 d�c. 2015, 20:13:41
    Author     : user
--%>

<%@page import="mg.cnaps.st.StPrixArticle"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StPrixArticle  a = new StPrixArticle();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
        
    pi.getFormu().getChamp("article").setLibelle("Article");
    pi.getFormu().getChamp("fournisseur").setLibelle("Fournisseur");
    pi.getFormu().getChamp("pu").setLibelle("Prix unitaire");
    pi.getFormu().getChamp("datyeffective").setLibelle("Date effective");
    pi.getFormu().getChamp("datyeffective").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("dureedelivraison").setLibelle("Dur�e de livraison ");
    pi.getFormu().getChamp("remarque").setLibelle("Observation");
    pi.getFormu().getChamp("article").setPageAppel("choix/stArticleChoix.jsp");
    pi.getFormu().getChamp("fournisseur").setPageAppel("choix/stTiersChoix.jsp");
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 align="center">Prix Article</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="prixarticle" id="prixarticle" data-parsley-validate>
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="stock/article/prixarticle-liste.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StPrixArticle">
    </form>
</div>
