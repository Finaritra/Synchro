<%-- 
    Document   : article-saisie
    Created on : 8 sept. 2015, 12:21:56
    Author     : user
--%>
<%@page import="mg.cnaps.st.StArticle"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StArticle  a = new StArticle();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));

    pi.setLien((String) session.getValue("lien"));
    affichage.Champ[] liste = new affichage.Champ[6];
    
    TypeObjet o = new TypeObjet();
    o.setNomTable("st_unite");
    liste[0] = new Liste("unite", o, "VAL", "id");
    
    TypeObjet op = new TypeObjet();
    op.setNomTable("ST_TYPE_ARTICLE");
    liste[1] = new Liste("type", op, "VAL", "id");
    
    TypeObjet cls = new TypeObjet();
    cls.setNomTable("ST_CLASSE");
    liste[2] = new Liste("classee", cls, "VAL", "id");
    
    TypeObjet grp = new TypeObjet();
    grp.setNomTable("ST_GROUPE_ARTICLE");
    liste[3] = new Liste("groupee", grp, "VAL", "id");
    
    TypeObjet sgrp = new TypeObjet();
    sgrp.setNomTable("ST_SOUSGROUPE_ARTICLE");
    liste[4] = new Liste("sousgroupe", sgrp, "VAL", "id");
    
    TypeObjet prs = new TypeObjet();
    prs.setNomTable("ST_TYPE_ARTICLE_PRESENTATION");
    liste[5] = new Liste("presentation", prs, "DESCE", "id");
    
    pi.getFormu().changerEnChamp(liste);
//    LogPersonnel chauf = new LogPersonnel();
//    chauf.setNomTable("log_personnel");
//    liste[1]= new Liste("chauffeur",chauf, "nom","id");
    
    pi.getFormu().getChamp("code").setVisible(false);
    pi.getFormu().getChamp("designation").setLibelle("Libelle");
    pi.getFormu().getChamp("seuil").setLibelle("Seuil");
    pi.getFormu().getChamp("unite").setLibelle("Unit�");
    pi.getFormu().getChamp("presentation").setLibelle("Presentation");
    pi.getFormu().getChamp("type").setLibelle("Chapitre");
    pi.getFormu().getChamp("classee").setLibelle("Classe");
    pi.getFormu().getChamp("groupee").setLibelle("Groupe");
    pi.getFormu().getChamp("sousgroupe").setLibelle("Sous groupe");
    pi.getFormu().getChamp("codemnemonique").setLibelle("Code m�monique");
//    pi.getFormu().getChamp("vehicule").setPageAppel("choix/choix.jsp?classe=mg.cnaps.mg.log.LogVehicule&champReturn=vehicule");
//    pi.getFormu().getChamp("id_assureur").setPageAppel("choix/choix.jsp?classe=bean.TypeObjet&nomtable=log_assureur&champReturn=id_assureur");
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Article</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="article" id="article" data-parsley-validate>
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="stock/article/article-saisie.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StArticle">
    <input name="nomtable" type="hidden" id="nomtable" value="ST_ARTICLE">
    </form>
</div>
