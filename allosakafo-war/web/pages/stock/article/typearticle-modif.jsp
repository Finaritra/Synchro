<%-- 
    Document   : typearticle-modif
    Created on : 25 janv. 2016, 11:44:11
    Author     : Joe
--%>
<%@page import="mg.cnaps.st.StTypeArticle"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    UserEJB u;
    StTypeArticle art;
    art = new StTypeArticle();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(art, request, (user.UserEJB) session.getValue("u"));

    pi.setLien((String) session.getValue("lien"));

    pi.getFormu().getChamp("val").setLibelle("Libelle");
    pi.getFormu().getChamp("desce").setLibelle("Observation");
    pi.getFormu().getChamp("compta_compte").setLibelle("Compte comptable");

    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification Chapitre</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="starticle">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/article/typearticle-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StTypeArticle">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>