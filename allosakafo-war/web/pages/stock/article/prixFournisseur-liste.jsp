<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>

<% StPrixArticle lv = new StPrixArticle();

    lv.setNomTable("ST_PRIX_ARTICLE_LIBELLE");
    String listeCrt[] = {"id", "article", "fournisseur", "pu", "datyeffective","dureedelivraison","remarque"};
    String listeInt[] = null;
    String libEntete[] = {"id", "article", "fournisseur", "pu", "datyeffective","dureedelivraison","remarque"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    pr.setApres("stock/article/prixFournisseur-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Article</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/article/prixFournisseur-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/article/prixFournisseur-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Article", "Fournisseur", "Prix unitaire", "Date d&apos; &eacute;ffectivit&eacute; ","Dur&eacute;e de livraison","Remarque"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
