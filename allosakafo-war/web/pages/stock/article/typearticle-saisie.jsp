<%-- 
    Document   : typearticle-saisie
    Created on : 25 janv. 2016, 11:43:18
    Author     : Joe
--%>
<%@page import="mg.cnaps.st.StTypeArticle"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    try{
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StTypeArticle  a = new StTypeArticle();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));

    pi.setLien((String) session.getValue("lien"));
    
    
    pi.getFormu().getChamp("val").setLibelle("Libelle");
    pi.getFormu().getChamp("desce").setType("textarea");
    pi.getFormu().getChamp("desce").setLibelle("Observation");
    pi.getFormu().getChamp("compta_compte").setLibelle("Compte comptable");
    affichage.Champ[] liste = new affichage.Champ[1];
    Liste lst = new Liste();
    String[] val = {"I","N"};
    String[] valAffiche = {"Imputable","Non imputable"};
    lst.ajouterValeur(val, valAffiche);
    liste[0] = lst;
    liste[0].setNom("imputable");
    pi.getFormu().changerEnChamp(liste);

    pi.getFormu().getChamp("compta_compte").setPageAppel("choix/comptaCompteChoix.jsp");
  
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>chapitre</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="typearticle" id="typearticle" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/article/typearticle-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StTypeArticle">
    </form>
</div>
<% }catch(Exception ex){
    ex.printStackTrace();
}%>