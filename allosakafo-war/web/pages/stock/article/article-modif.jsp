<%@page import="mg.cnaps.st.StArticle"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    StArticle art = new StArticle();
    UserEJB u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(art, request, (user.UserEJB) session.getValue("u"));

    pi.setLien((String) session.getValue("lien"));

    affichage.Champ[] liste = new affichage.Champ[6];
    
    TypeObjet o = new TypeObjet();
    o.setNomTable("st_unite");
    liste[0] = new Liste("unite", o, "VAL", "id");
    
    TypeObjet op = new TypeObjet();
    op.setNomTable("ST_TYPE_ARTICLE");
    liste[1] = new Liste("type", op, "VAL", "id");
    
    TypeObjet cls = new TypeObjet();
    cls.setNomTable("ST_CLASSE");
    liste[2] = new Liste("classee", cls, "VAL", "id");
    
    TypeObjet grp = new TypeObjet();
    grp.setNomTable("ST_GROUPE_ARTICLE");
    liste[3] = new Liste("groupee", grp, "VAL", "id");
    
    TypeObjet sgrp = new TypeObjet();
    sgrp.setNomTable("ST_SOUSGROUPE_ARTICLE");
    liste[4] = new Liste("sousgroupe", sgrp, "VAL", "id");
    
    TypeObjet prs = new TypeObjet();
    prs.setNomTable("ST_TYPE_ARTICLE_PRESENTATION");
    liste[5] = new Liste("presentation", prs, "DESCE", "id");
    
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("code").setVisible(false);
    pi.getFormu().getChamp("designation").setLibelle("Libelle");
    pi.getFormu().getChamp("seuil").setLibelle("Seuil");
    pi.getFormu().getChamp("unite").setLibelle("Unit�");
    pi.getFormu().getChamp("presentation").setLibelle("Presentation");
    pi.getFormu().getChamp("type").setLibelle("Chapitre");
    pi.getFormu().getChamp("classee").setLibelle("Classe");
    pi.getFormu().getChamp("groupee").setLibelle("Groupe");
    pi.getFormu().getChamp("sousgroupe").setLibelle("Sous groupe");
    pi.getFormu().getChamp("codemnemonique").setLibelle("Code mn�monique");

    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification Article</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="starticle">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/article/article-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StArticle">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                        <input name="nomtable" type="hidden" id="nomtable" value="st_article">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
