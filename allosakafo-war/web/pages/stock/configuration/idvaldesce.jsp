<%@page import="java.util.Map"%>
<%@page import="java.util.concurrent.ConcurrentHashMap"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String cible = request.getParameter("ciblename");
    Map<String, String> titre = new ConcurrentHashMap<String, String>();
    titre.put("st_classe", "Classe");
    titre.put("st_projet", "Projet");
    titre.put("st_type_besoin", "Type besoin");
    titre.put("ST_TYPE_MVT", "Type mouvement");
    titre.put("st_unite", "Type unit�");
    

    TypeObjet act = new TypeObjet();
    act.setNomTable(cible);
    //act.setNomProcedureSequence("seq"+cible);
    PageInsert pi = new PageInsert(act, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    pi.getFormu().getChamp("val").setLibelle("Valeur");
    pi.getFormu().getChamp("desce").setLibelle("Description");
    pi.getFormu().getChamp("desce").setType("textarea");
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1 align="center"><%=titre.get(cible)%></h1>
    </section>
    <section class="content">

        <form action="<%=pi.getLien()%>?but=configuration/apresIdvaldesce.jsp&ciblename=<%out.print(cible);%>" method="post" name="configuration" id="idvaldesce">
            <%
                pi.getFormu().makeHtmlInsertTabIndex();
                out.println(pi.getFormu().getHtmlInsert());
            %>

            <input name="acte" type="hidden" id="nature" value="insert">
            <input name="bute" type="hidden" id="bute" value="configuration/idvaldesce.jsp&ciblename=<%out.print(cible);%>">
            <input name="classe" type="hidden" id="classe" value="bean.TypeObjet">
            <input name="nomtable" type="hidden" id="nomtable" value="<%out.print(cible);%>">
        </form>
        <%
            TypeObjet e = new TypeObjet();
            e = act;

            e.setNomTable(cible);
            String listeCrt[] = {"id", "val", "desce"};
            String listeInt[] = null;
            String libEntete[] = {"id", "val", "desce"};
            PageRecherche pr = new PageRecherche(e, request, listeCrt, listeInt, 3, libEntete, 2);
            pr.setUtilisateur((user.UserEJB) session.getValue("u"));
            pr.setLien((String) session.getValue("lien"));
            pr.setApres("configuration/idvaldesce.jsp&ciblename=" + cible);

            String[] colSomme = null;
            pr.creerObjetPage(libEntete, colSomme);
        %>
        <form action="<%=pr.getLien()%>?but=configuration/idvaldesce.jsp&ciblename=<%out.println(cible);%>"
              method="post" name="configuration" id="idvaldesce">
            <%
                pr.getFormu().getChamp("id").setLibelle("Identifiant");
                pr.getFormu().getChamp("val").setLibelle("Valeur");
                pr.getFormu().getChamp("desce").setLibelle("Description");
                out.println(pr.getFormu().getHtmlEnsemble());%>
        </form>
        <%
            String lienTableau[] = {pr.getLien() + "?but=configuration/modif_idvaldesce.jsp&ciblename=" + cible};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <%
            String libEnteteAffiche[] = {"Identifiant", "Valeur", "Description"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>