<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="mg.allosakafo.produits.Produits"%>
<%@page import="mg.allosakafo.produits.Ingredients"%>



<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    BonDeLivraison cmd = new BonDeLivraison();
	
    PageUpdate pi = new PageUpdate(cmd, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    
    affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet depo = new TypeObjet();
    depo.setNomTable("point");
    liste[0] = new Liste("point", depo, "val", "id");
    
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("remarque").setLibelle("remarque");
    pi.getFormu().getChamp("idbc").setLibelle("N� Facture Fournisseur");
     pi.getFormu().getChamp("idbc").setAutre("readonly");
      pi.getFormu().getChamp("idbc").setPageAppel("choix/listeFactureFournisseurChoix.jsp");
	pi.getFormu().getChamp("etat").setVisible(false);
	pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 class="box-title">Modification bon de livraison</h1>
    <form action="<%=pi.getLien()%>?but=stock/bl/apresBL.jsp" method="post" name="bondelivraison" id="bondelivraison" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertCommande();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <div class="box">
                        <div class="box-title with-border">
                            <h1 class="box-title">D&eacute;tails</h1>
                        </div>
                        <div class="box-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th style="background-color:#bed1dd">ID</th>
                                        <th style="background-color:#bed1dd">Libelle</th>
                                        <th style="background-color:#bed1dd">Unite</th>
                                        <!--<th style="background-color:#bed1dd">Quantite par pack</th>-->
                                        <th style="background-color:#bed1dd">Quantite</th>
                                    </tr></thead>
                                <tbody>
                                    <%
                                        BonDeLivraisonFille p = new BonDeLivraisonFille();
										p.setNomTable("as_bondelivraison_fille_lib");
                                        BonDeLivraisonFille[] listef = (BonDeLivraisonFille[]) CGenUtil.rechercher(p, null, null, " and NUMBL='"+request.getParameter("id").trim()+"'");
                                         for (int i = 0; i < listef.length; i++) {
                                    %>
                                <input type="hidden" class="form form-control" name="nb" id="nb" value="<%=listef.length%>">
                                <tr>
                                    <td><input type="hidden" class="form form-control" name="idFille" id="idFille<%=i%>" value="<%=listef[i].getId()%>"><%=listef[i].getId()%></td>
                                    <td><%=listef[i].getProduit()%></td>
                                    <td>G</td>
                                   <input type="hidden" class="form form-control" name="quantiteparpack" id="quantiteparpack<%=i%>" value="<%=listef[i].getQuantiteparpack()%>">
                                    <td><input type="text" class="form form-control" name="quantite" id="quantite<%=i%>" value="<%=listef[i].getQuantite()%>"></td>
                                </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                
        <input name="acte" type="hidden" id="nature" value="updateBL">
        <input name="bute" type="hidden" id="bute" value="stock/bl/bondelivraison-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.stock.BonDeLivraison">
        <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;">Enregistrer</button>
        <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;">R&eacute;initialiser</button>
    </form>
</div>