<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<%
    try {

        BonDeLivraisonDateFournisseur lv = new BonDeLivraisonDateFournisseur();

        String listeCrt[] = {"id", "daty", "produit","iddetailsfacturefournisseur","nomfournisseur"};
        String listeInt[] = {"daty"};
        String libEntete[] = {"id", "produit", "numbl", "quantite", "quantiteparpack", "daty","nomfournisseur"};

        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));

        pr.setApres("stock/bl/asBondeLivraisonFille-liste.jsp");
        String[] colSomme = null;
        pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste des bons de livraison fille</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/bl/asBondeLivraisonFille-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/bl/asBonDeLivraisonFille-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Produit", "Numero Bl", "Quantite", "Quantite par pack", "Date","Fournisseur"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);

            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>