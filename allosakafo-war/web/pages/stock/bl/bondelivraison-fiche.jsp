<%-- 
    Document   : bondelivraison-fiche
    Created on : 10 mars 2016, 09:20:48
    Author     : Joe
--%>
<%@page import="java.sql.Date"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.allosakafo.stock.*" %>
<%
    try {
        BonDeLivraison inventaire = new BonDeLivraison();
        PageConsulte pc = new PageConsulte(inventaire, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
        pc.getChampByName("idbc").setLibelle("Num BC");
        pc.setTitre("Fiche bon de livraison");
		
		BonDeLivraison livraison = (BonDeLivraison)pc.getBase();
		
        BonDeLivraisonFille fille = new BonDeLivraisonFille();
        fille.setNomTable("as_bondelivraison_fille_lib");
        String idbl = " AND NUMBL = '" + request.getParameter("id").trim() + "'";

        BonDeLivraisonFille[] filles = (BonDeLivraisonFille[]) CGenUtil.rechercher(fille, null, null, idbl);
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/bl/asBonDeLivraison-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
							<% if (livraison.getEtat() == ConstanteEtat.getEtatCreer()){ %>
								<a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=annuler&bute=stock/bl/bondelivraison-fiche.jsp&classe=mg.allosakafo.stock.BonDeLivraison" style="margin-right:10px">Annuler</a>
								<a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/bl/bondelivraison-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right:10px">Modifier</a>
								<!--<a class="btn btn-success pull-right"  href="<%//=(String) session.getValue("lien") + "?but=stock/bl/apresBL.jsp&id=" + request.getParameter("id")%>&acte=validerBL&bute=stock/bl/bondelivraison-fiche.jsp&classe=mg.allosakafo.stock.BonDeLivraison" style="margin-right: 10px">Viser</a>
                                                                -->
                                                                <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&classe=mg.allosakafo.stock.BonDeLivraison&bute=stock/bl/bondelivraison-fiche.jsp&id=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Viser</a>
                  
							<% } %>
							<a class="btn btn-primary pull-right"  href="#" onclick="pagePopUp('stock/bl/asBonDeLivraison-reception.jsp?id=<%=request.getParameter("id")%>')" style="margin-right: 10px">Aper&ccedil;u</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title with-border">
                    <h1 class="box-title">D�tails</h1>
                </div>
                <div class="box-body no padding">
                    <table class="table table-bordered">
                        <tr>
                            <th align="center">ID</th>
                            <th align="center">Produit</th>
                            <th align="center">Quantit&eacute; par pack</th>
                            <th align="center">Quantit&eacute;</th>
                            <th align="center"></th>
                        </tr>

                        <%
                            for (int i = 0; i < filles.length; i++) {%>
                        <tr>
                            <td align="left"><%=filles[i].getId()%></td>
                            <td align="left"><%=filles[i].getProduit()%></td>
                            <td align="right"><%=Utilitaire.formaterAr(filles[i].getQuantiteparpack())%></td>
                            <td align="right"><%=Utilitaire.formaterAr(filles[i].getQuantite())%></td>
                            <td  align="right"> <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=delete&classe=mg.allosakafo.stock.BonDeLivraisonFille&bute=stock/bl/bondelivraison-fiche.jsp&id="+filles[i].getId()%>" style="margin-right: 10px">Supprimer</a>
                                       
                        </tr>
                        <% }%> 
                    </table>
                </div>


            </div> 
        </div> 

    </div> 

</div>
<% } catch (Exception ex) {
        ex.printStackTrace();
        throw new Exception(ex.getMessage());
    }%>