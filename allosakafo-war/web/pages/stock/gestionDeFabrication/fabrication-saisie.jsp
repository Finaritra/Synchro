<%-- 
    Document   : magasin-saisie
    Created on : 8 sept. 2015, 13:58:45
    Author     : user
--%>

<%@page import="service.AlloSakafoService"%>
<%@ page import="mg.allosakafo.produits.Fabrication" %>
<%@ page import="mg.allosakafo.produits.Ingredients" %>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    Fabrication  a = new Fabrication();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    
    affichage.Champ[] liste = new affichage.Champ[1];
    
    TypeObjet depot = new TypeObjet();
    depot.setNomTable("point");
    liste[0] = new Liste("depot", depot, "val", "id");
    
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("ingredient").setPageAppel("choix/listeIngredientChoix.jsp");
    pi.setLien((String) session.getValue("lien"));
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("ingredient").setLibelle("Ingr&eacute;dient");
    pi.getFormu().getChamp("qte").setLibelle("Quantit&eacute;");
    pi.getFormu().getChamp("remarque").setLibelle("Remarque");
    pi.getFormu().getChamp("etat").setVisible(false);
    pi.getFormu().getChamp("depot").setDefaut(AlloSakafoService.getNomRestau());
    
    
    pi.preparerDataFormu(); 
%>
<div class="content-wrapper">
    <h1>Gestion de fabrication</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="magasin" id="magasin" data-parsley-validate>
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="stock/gestionDeFabrication/fabrication-fiche.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.produits.Fabrication">
    <input name="nomtable" type="hidden" id="nomtable" value="fabrication">
    </form>
</div>
