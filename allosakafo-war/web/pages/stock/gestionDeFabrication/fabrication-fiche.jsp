<%-- 
    Document   : fabrication-fiche
    Created on : 10 mars 2020, 15:43:41
    Author     : Maharo R.
--%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.allosakafo.produits.Fabrication" %>
<%@ page import="mg.allosakafo.produits.Recette" %>
<%
    Fabrication magasin;
%>
<%
    magasin = new Fabrication();
    magasin.setNomTable("FABRICATION_LIB");
    //String[] libelleMagasinFiche = {"Id", "Daty", "Ingredient", "Code Direction Regionale","Type Magasin"};
    PageConsulte pc = new PageConsulte(magasin, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.getChampByName("id").setLibelle("Id");
    pc.getChampByName("daty").setLibelle("Date");
    pc.getChampByName("ingredient").setLibelle("Ingr&eacute;dient");
    pc.getChampByName("qte").setLibelle("Quantit&eacute;");
    pc.getChampByName("remarque").setLibelle("Remarque");
    pc.getChampByName("depot").setLibelle("D�pot");
    //pc.setLibAffichage(libelleMagasinFiche);
    pc.setTitre("Fiche Fabrication");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/gestionDeFabrication/fabrication-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                    <div class="box-footer">
                        <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&classe=mg.allosakafo.produits.Fabrication&bute=stock/gestionDeFabrication/fabrication-fiche.jsp&id=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Viser</a>
                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/gestionDeFabrication/fabrication-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                        <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id="+ request.getParameter("id")%>&acte=delete&bute=stock/gestionDeFabrication/fabrication-liste.jsp&classe=mg.allosakafo.produits.Fabrication" style="margin-right: 10px">Supprimer</a>
                        
                        
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
    
            <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">Liste Composants Ingr&eacute;dients</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                   
                                    <th>Ingr&eacute;dient</th>
                                    <th>Quantit&eacute;</th>
                                    <th>Unit&eacute;</th>
                                </tr>
                            </thead>

                            <tbody>
                                <%
                                    Recette p = new Recette();
				    p.setNomTable("as_recette_libellev"); // vue
                                    Fabrication fab=new Fabrication();
                                    Fabrication[] nomIngredient = (Fabrication[]) CGenUtil.rechercher(fab, null, null, " and ID = '" + request.getParameter("id") + "'");
                                    Recette[] liste = (Recette[]) CGenUtil.rechercher(p, null, null, " and IDPRODUITS = '" + nomIngredient[0].getIngredient() + "'");
                                    System.out.println("ITO ILAY SOUTV " + nomIngredient[0].getIngredient());
                                    for (int i = 0; i < liste.length; i++) {
										
                                %>
                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                    <td  align="center"><%=liste[i].getId()%> </td>
                                  
                                    <td  align="right"><%=liste[i].getIdingredients()%></td>
                                    <td  align="right"><%=liste[i].getQuantite()%></td>
                                    <td  align="right"><%=liste[i].getUnite()%></td>
                                  
                                </tr>
                                <%
                                  
                                    }
                                  
                                %>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
