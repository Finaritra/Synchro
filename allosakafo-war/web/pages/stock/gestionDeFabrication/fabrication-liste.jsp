
<%@page import="mg.allosakafo.produits.FabricationLibelle"%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.produits.Fabrication"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%
    try {
        FabricationLibelle lv = new FabricationLibelle();
        String listeCrt[] = {"id", "daty", "ingredient", "depot"};
        String listeInt[] = {"daty"};
        String libEntete[] = {"id", "daty", "ingredient", "qte", "remarque", "depotlib"};
        
        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 5);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        
        pr.getFormu().getChamp("daty1").setLibelle("Date min");
        pr.getFormu().getChamp("daty2").setLibelle("Date Max");
        pr.getFormu().getChamp("ingredient").setLibelle("Ingr&eacute;dient");
        
        affichage.Champ[] liste = new affichage.Champ[1];
        TypeObjet d1 = new TypeObjet();
        d1.setNomTable("point");
        liste[0] = new Liste("depot", d1, "val", "val");
        pr.getFormu().changerEnChamp(liste);
        
        pr.getFormu().getChamp("depot").setDefaut(AlloSakafoService.getNomRestau());

        pr.setApres("stock/gestionDeFabrication/fabrication-liste.jsp");
        String[] colSomme = null;
        pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Fabrication</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/gestionDeFabrication/fabrication-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/gestionDeFabrication/fabrication-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "Date", "Ingr&eacute;dient", "Quantit&eacute;", "Remarque", "Depot"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%} catch (Exception e) {
e.printStackTrace();
    }%>