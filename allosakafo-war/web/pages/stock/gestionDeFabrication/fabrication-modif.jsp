<%-- 
    Document   : fabrication-modif
    Created on : 10 mars 2020, 16:32:35
    Author     : Maharo R.
--%>

<%@page import="mg.cnaps.commun.Dr"%>
<%@ page import="mg.allosakafo.produits.Fabrication" %>
<%@ page import="mg.allosakafo.produits.Ingredients" %>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    UserEJB u;
    Fabrication mag;
%>
<%
    mag = new Fabrication();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(mag, request, (user.UserEJB) session.getValue("u"));
	
    pi.setLien((String) session.getValue("lien"));
    affichage.Champ[] liste = new affichage.Champ[2];
   
    
    Ingredients tpm = new Ingredients();
    tpm.setNomTable("as_ingredients_fabrique");
    liste[0] = new Liste("ingredient", tpm, "libelle", "id");
    
    TypeObjet depot = new TypeObjet();
    depot.setNomTable("point");
    liste[1] = new Liste("depot", depot, "val", "id");
    
    pi.getFormu().changerEnChamp(liste);
    
   
    
    
     pi.getFormu().getChamp("id").setLibelle("Id");
     pi.getFormu().getChamp("daty").setLibelle("Date");
     pi.getFormu().getChamp("ingredient").setLibelle("Ingr&eacute;dients");
     pi.getFormu().getChamp("qte").setLibelle("Quantit&eacute;s");
     pi.getFormu().getChamp("remarque").setLibelle("Remarques");
      pi.getFormu().getChamp("etat").setVisible(false);
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification Fabrication</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="stmagasin">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/gestionDeFabrication/fabrication-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.produits.Fabrication">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                        <input name="nomtable" type="hidden" id="nomtable" value="fabrication">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
