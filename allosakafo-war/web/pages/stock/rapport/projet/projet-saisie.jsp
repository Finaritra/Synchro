<%-- 
    Document   : operationnel-saisie
    Created on : 7 d�c. 2015, 22:07:05
    Author     : user
--%>
<%@page import="mg.cnaps.rapport.RapProjet"%>
<%@page import="mg.cnaps.rapport.RapOperationnel"%>
<%@page import="mg.cnaps.cv.Cv"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageInsert"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    RapProjet a = new RapProjet();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    
    pi.getFormu().getChamp("description_sommaire").setLibelle("Description sommaire");
    pi.getFormu().getChamp("date_debut").setLibelle("Date debut");
    pi.getFormu().getChamp("date_debut").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("date_fin").setLibelle("Date fin");
    pi.getFormu().getChamp("idbudgetprojet").setLibelle("Budget projet");
    pi.getFormu().getChamp("volume_horaire").setLibelle("Volume horaire");
    pi.getFormu().getChamp("realisation_jour").setLibelle("Nombre de jour de r�alisation");
    
    pi.getFormu().getChamp("idbudgetprojet").setPageAppel("choix/budgetProjetChoix.jsp");
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 align="center">projet</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="projet" id="projet" autocomplete="off" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="paie/rapport/projet/projet-liste.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.rapport.RapProjet">
    </form>
</div>
