<%@page import="mg.cnaps.rapport.RapCadreAnalyse"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="mg.cnaps.sig.*" %>
<%@ page import="affichage.*" %>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

<%!
    RapCadreAnalyse rp;
%>
<%
    rp = new RapCadreAnalyse();
    String listeCrt[] = {"nom","projet"};
    String listeInt[] = {};
    String[] pourcentage = {"nombrepargroupe"};
    String colDefaut[] = {"nom","matricule","fonction","projet","volume_horaire","volume_horaire_passe"};
    String somDefaut[] = {};
    PageRechercheGroupe pr = new PageRechercheGroupe(rp, request, listeCrt, listeInt, 3, colDefaut, somDefaut, pourcentage, 7, 0);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stock/rapport/cadre/cadre-analyse.jsp");
    //pr.setAWhere("and traitement like upper('%recette%')");
    pr.creerObjetPagePourc();
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Analyse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/rapport/cadre/cadre-analyse.jsp" method="post" name="analyse" id="analyse">
            <%out.println(pr.getFormu().getHtmlEnsemble());%>
        </form>
        <%
             out.println(pr.getTableauRecap().getHtml());
        %>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
    <div id="id" class="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="100px">
    </div>
</div>
<script>
//    $(document).ready(function () {
//        var values = [6, 4, 8];
//        $('#sparkline').sparkline('html', values);
//        $('#sparkline').sparkline('html', $this.data());
//    });
    $(".sparkline").each(function () {
        var $this = $(this);
        $(this).sparkline('html', $(this).data());
    });
</script>