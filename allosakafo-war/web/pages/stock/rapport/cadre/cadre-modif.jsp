<%@page import="mg.cnaps.rapport.RapProjet"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageUpdate"%>
<%@page import="mg.cnaps.rapport.RapCadre"%>
<%@page import="user.UserEJB"%>
<%
    UserEJB u;
    RapCadre art;
%>
<%
    art = new RapCadre();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(art, request, (user.UserEJB) session.getValue("u"));

    pi.setLien((String) session.getValue("lien"));
    
    affichage.Champ[] liste = new affichage.Champ[2];
    TypeObjet c = new TypeObjet();
    c.setNomTable("RAP_ACTIVITE");
    liste[0] = new Liste("idactivite", c, "val", "id");
    RapProjet c1 = new RapProjet();
    liste[1] = new Liste("idprojet", c1, "description_sommaire", "id");
    
    pi.getFormu().getChamp("idactivite").setLibelle("Activite");
    pi.getFormu().getChamp("idprojet").setLibelle("Projet");
    pi.getFormu().getChamp("libelle").setLibelle("Libelle");
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("volume_heure").setLibelle("Volume horaire");

    pi.getFormu().changerEnChamp(liste);
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification d'un rapport d'activit&eacute;</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="starticle" id="rapcadre">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/rapport/cadre/cadre-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.rapport.RapCadre">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
