<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.commun.Dr"%>
<%@page import="mg.cnaps.log.LogService"%>
<%@page import="mg.cnaps.st.StDemandeAppro"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    UserEJB u;
    StDemandeAppro art;
%>
<%
    art = new StDemandeAppro();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(art, request, (user.UserEJB) session.getValue("u"));
    
    pi.setLien((String) session.getValue("lien"));
    affichage.Champ[] liste = new affichage.Champ[5];
    LogService s = new LogService();
    s.setNomTable("log_service");
    liste[0] = new Liste("service", s, "libelle", "id");
    
    TypeObjet d = new TypeObjet();
    d.setNomTable("log_direction");
    liste[1] = new Liste("direction", d, "VAL", "id");
    
    Dr dr = new Dr();
    dr.setNomTable("sig_dr");
    liste[2] = new Liste("code_dr", dr, "VAL", "id");
    
    
    StMagasin dep = new StMagasin();
    dep.setNomTable("St_Magasin");
    liste[3] = new Liste("magasin", dep, "code", "id");
    
    TypeObjet cl = new TypeObjet();
    cl.setNomTable("st_classe");
    liste[4] = new Liste("classee", cl, "val", "id");
    
    pi.getFormu().getChamp("service").setLibelle("Service");
    pi.getFormu().getChamp("direction").setLibelle("Direction");
    pi.getFormu().getChamp("code_dr").setLibelle("Direction régionale");
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("daty").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pi.getFormu().getChamp("designation").setLibelle("Désignation");
    pi.getFormu().getChamp("priorite").setLibelle("Priorité");
    pi.getFormu().getChamp("magasin").setLibelle("Magasin");
    
    pi.getFormu().getChamp("projet").setLibelle("Projet");
    pi.getFormu().getChamp("classee").setLibelle("Classe");
    
    pi.getFormu().getChamp("projet").setPageAppel("choix/listeProjetChoix.jsp");
    pi.getFormu().getChamp("code_dr").setPageAppel("choix/listeBeneficiaireChoix.jsp");
    
    pi.getFormu().getChamp("etat").setVisible(false);

    pi.getFormu().changerEnChamp(liste);
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification demande</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="stdemandeappro">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/appro/demandeappro-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StDemandeAppro">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
