<%-- 
    Document   : demandeapprofille-fiche.jsp
    Created on : 16 nov. 2015, 13:38:25
    Author     : Joe
--%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%
    StDemandeApproFille dma;
%>
<%
    dma = new StDemandeApproFille();
    //dma.setNomTable("ST_DEMANDE_APPRO_INFO");
    String[] libelleInventaireFiche = {"Id", "Demande", "Quantite","Remarque", "Article"};
    PageConsulte pc = new PageConsulte(dma, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setLibAffichage(libelleInventaireFiche);
    pc.setTitre("Fiche de demande appro");

    StDemandeAppro dapr = (StDemandeAppro) pc.getBase();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?butstock/appro/demandeapprofille-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a> Fiche de demande d'approvisionnement</h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                           
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/appro/demandeapprofille-liste.jsp&classe=mg.cnaps.st.StDemandeAppro" style="margin-right: 10px">Supprimer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
    
</div>
