

<%@page import="mg.cnaps.st.StDemandeApproFille"%>
<%@page import="affichage.PageRecherche"%>

<% StDemandeApproFille lv = new StDemandeApproFille();

    lv.setNomTable("ST_DEMANDEAPPROFILLE_INFO");
    String listeCrt[] = {"id", "demande" ,"article", "remarque", "quantite"};
    String listeInt[] = null;
    String libEntete[] = {"id", "demande","article", "remarque", "quantite"};
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("article").setLibelle("Article");
    pr.getFormu().getChamp("article").setPageAppel("choix/listeArticleChoix.jsp");
    pr.setApres("stock/appro/demandeapprofille-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste d&eacute;tails demande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/appro/demandeapprofille-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/appro/demandeapprofille-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "demande","article", "remarque", "quantite"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
