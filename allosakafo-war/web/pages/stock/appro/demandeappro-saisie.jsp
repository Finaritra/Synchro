<%-- 
    Document   : demmandeappro-saisie
    Created on : 11 sept. 2015, 10:15:59
    Author     : user
--%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.commun.Dr"%>
<%@page import="mg.cnaps.log.LogService"%>
<%@page import="mg.cnaps.st.StDemandeAppro"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StDemandeAppro  da = new StDemandeAppro();
    PageInsert pi = new PageInsert(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    
    affichage.Champ[] liste = new affichage.Champ[4];
    LogService s = new LogService();
    s.setNomTable("log_service_libst");
    liste[0] = new Liste("service", s, "libelle", "id");
    
    /*TypeObjet d = new TypeObjet();
    d.setNomTable("log_direction");
    liste[1] = new Liste("direction", d, "VAL", "id");
    
    Dr dr = new Dr();
    dr.setNomTable("sig_dr");
    liste[2] = new Liste("code_dr", dr, "VAL", "id");*/
    
    
    StMagasin dep = new StMagasin();
    dep.setNomTable("St_Magasin");
    liste[1] = new Liste("magasin", dep, "desce", "id");
    
    /*TypeObjet cl = new TypeObjet();
    cl.setNomTable("st_classe");
    liste[1] = new Liste("classee", cl, "val", "id");*/
    
    TypeObjet typ = new TypeObjet();
    typ.setNomTable("st_type_article");
    liste[2] = new Liste("typee", typ, "val", "id");
    
    String[] affiche = {"Urgent","Intermediaire","Normale"};
    String[] value = {"1","2","3"};
    Liste lst = new Liste();
    lst.ajouterValeur(value, affiche);
    liste[3] = lst;
    liste[3].setNom("priorite");
    
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("service").setLibelle("Demandeur");
    pi.getFormu().getChamp("direction").setVisible(false);
    pi.getFormu().getChamp("code_dr").setVisible(false);
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("daty").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pi.getFormu().getChamp("designation").setType("textarea");
    pi.getFormu().getChamp("designation").setLibelle("Observation");
    pi.getFormu().getChamp("priorite").setLibelle("Priorit�");
    pi.getFormu().getChamp("magasin").setLibelle("Magasin");
    
    pi.getFormu().getChamp("projet").setLibelle("Projet");
    pi.getFormu().getChamp("typee").setLibelle("Chapitre");
    pi.getFormu().getChamp("classee").setVisible(false);
    
    pi.getFormu().getChamp("projet").setPageAppel("choix/listeProjetChoix.jsp");
    //pi.getFormu().getChamp("code_dr").setPageAppel("choix/listeBeneficiaireChoix.jsp");
    
    pi.getFormu().getChamp("etat").setVisible(false);

    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 align="center">Demande</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro" data-parsley-validate>
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="stock/appro/demandeapprofille-saisie.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StDemandeAppro">
    </form>
</div>
