<%-- 
    Document   : demandeapprofille-modif
    Created on : 26 avr. 2016, 21:38:06
    Author     : Joe
--%>
<%@page import="mg.cnaps.st.StDemandeApproFille"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    StDemandeApproFille dmd = new StDemandeApproFille();
    UserEJB u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(dmd, request, (user.UserEJB) session.getValue("u"));

    pi.setLien((String) session.getValue("lien"));

    pi.getFormu().getChamp("demande").setDefaut(request.getParameter("id"));
    pi.getFormu().getChamp("demande").setAutre("readOnly");
    pi.getFormu().getChamp("quantite").setLibelle("Quantit�");
    pi.getFormu().getChamp("remarque").setType("textarea");
    pi.getFormu().getChamp("remarque").setLibelle("Observation");

    pi.getFormu().getChamp("article").setLibelle("Article");

    pi.getFormu().getChamp("article").setPageAppel("choix/stArticleChoix.jsp");
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification demande</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="starticle">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Modifier</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/appro/demandeappro-liste.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StDemandeApproFille">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
