<%-- 
    Document   : test-update-multiple
    Created on : 11 juin 2020, 12:25:06
    Author     : pro
--%>

<%@page import="mg.allosakafo.achat.Besoin"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="user.UserEJB"%>
<%@page import="affichage.PageRechercheUpdate"%>

<%
    try {
        String nomtable=request.getParameter("cible");
        UserEJB u = (UserEJB) session.getAttribute("u");
        Besoin ce = new Besoin();
        ce.setNomTable(nomtable);
        String bouton = "Passer en mode Quotidien";
        if(nomtable.contains("hebdo"))bouton="Passer en mode Hebdomadaire";
        String listeCrt[] = {"id", "remarque", "quantite", "idproduit"};
        String listeInt[] = null;
        String libEntete[] = {"id", "remarque", "quantite", "libelleProduit", "idproduit"};

        PageRechercheUpdate pr = new PageRechercheUpdate(ce, request, listeCrt, listeInt, 3, libEntete, "id");
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        pr.setApres("stock/besoin/modif-besoin-multiple-quotidien.jsp");

        pr.getFormuLigne().getChamp("idproduit").setPageAppel("choix/ChoixProduitEtIngredients.jsp");
        pr.getFormuLigne().getChamp("idproduit").setAutre("readonly");
        pr.getFormuLigne().getChamp("libelleProduit").setAutre("readonly");

        String[] colSomme = null;

        pr.setNpp(300);

        pr.getFormu().getChamp("idproduit").setLibelle("id produit");
        pr.getFormu().getChamp("quantite").setLibelle("quantit�");
        pr.creerObjetPage(libEntete, colSomme);

        int nombreLigne = pr.getTableau().getData().length;
%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Modification besoins</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/besoin/modif-besoin-multiple-quotidien.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%
            out.println(pr.getTableauRecap().getHtml());
        %>
        <br>

        <form action="<%=pr.getLien()%>?but=apresMultiple.jsp" method="post" data-parsley-validate>
            <%
                String libEnteteAffiche[] = {"id", "remarque", "quantite", "libell� Produit", "id produit"};
                pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                out.println(pr.getTableau().getHtmlWithCheckboxUpdateMultiple());

            %>
            <input name="acte" type="hidden" id="acte" value="updateMultiple">
            <input name="bute" type="hidden" id="bute" value="stock/besoin/modif-besoin-multiple-quotidien.jsp&cible=<%=nomtable%>">
            <input name="classe" type="hidden" id="classe" value="mg.allosakafo.achat.Besoin">
            <input name="nombreLigne" type="hidden" id="nomtable" value="<%=nombreLigne%>">
            <button type="submit" class="btn btn-primary" onclick="conf()" class="btn-default"><%=bouton %></button>
        </form>
    </section>
</div>
<script> 
        function conf(){
            document.getElementById('acte').value='passerCommande';
        }
    </script> 
<%} catch (Exception ex) {
        ex.printStackTrace();
    }%>


