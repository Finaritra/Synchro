<%-- 
    Document   : liste-achat-besoin-compose
    Created on : 10 juin 2020, 14:13:13
    Author     : rajao
--%>

<%@page import="affichage.Page"%>
<%@page import="bean.ClassMAPTable"%>
<%@page import="mg.allosakafo.produits.RecetteLib"%>
<%@page import="affichage.TableauRecherche"%>
<%@page import="mg.allosakafo.achat.Besoin"%>
<%@page import="bean.AdminGen"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.allosakafo.stock.EtatdeStockDate"%>
<%@page import="mg.allosakafo.achat.AchatFournisseur"%>
<%@page import="mg.allosakafo.achat.Achat"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.Utilitaire"%>
<% try {
        RecetteLib ac = new RecetteLib();
        String listeCrt[] = {"libelleproduit","libelleingredient","idingredients","idproduits"};
        String listeInt[] = null;
        String libEntete[] = {"idingredients","libelleingredient","quantite","valunite"};
        String indice[] = null;
        if(request.getParameterValues("id")!=null && request.getParameterValues("id").length!=0){
           indice=request.getParameterValues("id");
        }
        else{
             throw new Exception("Veuillez sélectionner au mois 1 ingredients");
        }
        PageRecherche pr = new PageRecherche(ac, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
        pr.setLien((String)session.getValue("lien"));
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.creerObjetPage(libEntete, null);
         pr.setApres("stock/besoin/simuler-besoin.jsp");
         
        RecetteLib[] tmp = Besoin.simulerBesoin(request,request.getParameterValues("id"),null);
        String somDefaut[] = null;
        pr.creerObjetPage(libEntete, somDefaut);
        String[] enteteAuto = {"idingredients","libelleingredient","quantite","valunite"} ;
        pr.setNpp(10000);
        pr.setTableau(new TableauRecherche(tmp, enteteAuto));

%>


<script>
    function changerDesignation() {
        document.incident.submit();
    }
             
    
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste simulation besoin</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/besoin/simuler-besoin.jsp" method="post" name="listeachat" id="listeachat">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
             <% 
            if(indice!=null && indice.length>0){ 
            for(int i=0;i<indice.length;i++){
             %>
                <input type="hidden" name="id" value="<%=indice[i]%>" />
                <input type="hidden" name="idproduit_<%=indice[i]%>" value="<%=request.getParameter("idproduit_"+indice[i])%>" />
                <input type="hidden" name="quantite_<%=indice[i]%>" value="<%=request.getParameter("quantite_"+indice[i])%>" />
            <% } } %>
        </form>
        <%
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
       
    </section>
</div>
    
<% } catch (Exception ex) {
        ex.printStackTrace();
        %>
        <script language="JavaScript">
            alert(<%=ex.getMessage() %>);
            history.back();
        </script>
<%}%>