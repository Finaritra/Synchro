<%-- 
    Document   : liste-achat
    Created on : 15 oct. 2019, 15:22:24
    Author     : ionyr
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.allosakafo.achat.Besoin" %>
<% 
    Besoin b = new Besoin();
    b.setNomTable("besoinlibelle");
    String listeCrt[] = {"id","idproduit"};
    String listeInt[] = {};
    String libEntete[] = {"id","idproduit","remarque", "quantite"};
    PageRecherche pr = new PageRecherche(b, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stock/besoin/liste-produit.jsp");
    String[] colSomme = null;
    pr.getFormu().getChamp("idproduit").setLibelle("Produit");
    pr.creerObjetPage(libEntete, colSomme);
    Besoin[] listeproduit = (Besoin[]) pr.getTableau().getData(); 
%>


<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Produit</h1>
    </section>
    <section class="content">

        </br>
        <div class="row">
            <div class="row col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" align="center">LISTE</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <form name="mep" action="<%=pr.getLien()%>?but=stock/besoin/liste-produit.jsp" method="post">
                                <%
                                      out.println(pr.getFormu().getHtmlEnsemble());
                                 %>
                            </form>
                            <form action="../BesoinQuotidienServlet" method="POST">
                                <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                                    <thead>
                                        <tr class="head">
                                            <th align="left">ID</th>
                                            <th align="left">Produit</th>
                                            <th align="left">Quantit&eacute</th>
                                        </tr>
                                    </thead>
                                    <tbody class="searchable">
                                        <%
                                            for (int i = 0; i < listeproduit.length; i++) { 

                                        %>
                                        <tr>

                                            <td align="left"><input type="text" name="id" value="<%=listeproduit[i].getId()%>" readonly="true" /></td>
                                            <td align="left"><%=listeproduit[i].getIdproduit()%></td>
                                            <td align="left"><input type="text" name="quantite" value="<%=listeproduit[i].getQuantite()%>"</td>

                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
                                <input type="hidden" name="acte" value="saisieProduit">
                                <input type="hidden" name="bute" value="stock/besoin/liste-achat.jsp">
                                <center><input type="submit" class="btn btn-primary" value="Enregistrer"></center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

