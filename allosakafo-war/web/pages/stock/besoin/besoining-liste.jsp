
<%@page import="mg.allosakafo.produits.RecetteLib"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<% 
    try{
    RecetteLib lv = new RecetteLib();
    lv.setNomTable("besoinetingredient");
    String listeCrt[] = {"idingredients","libelleproduit", "libelleingredient"};
    String listeInt[] = {};
    String libEntete[] = {"libelleproduit", "quantite"};
    
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, listeCrt.length, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("idingredients").setVisible(false);
    pr.setApres("stock/besoin/besoining-liste.jsp");
    String[] colSomme = {"quantite"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste des bons de livraison</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/besoin/besoining-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"ingredient", "quantite"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
			  
				out.println(pr.getTableau().getHtml());
				out.println(pr.getBasPage());
		%>
    </section>
</div>
    <% }
catch(Exception ex){
    ex.printStackTrace();
}%>