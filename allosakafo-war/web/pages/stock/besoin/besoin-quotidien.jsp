<%-- 
    Document   : besoin-quotidien.jsp
    Created on : 15 oct. 2019, 15:11:52
    Author     : HP
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="java.util.HashMap"%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.achat.BesoinQuotidienGroupe"%>
<%@page import="mg.allosakafo.achat.Besoin"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.allosakafo.produits.Ingredients"%>
<%
try{
   
    Besoin lv = new Besoin();
    lv.setNomTable("besoinlibpoint");
    
    /*private String id, idproduit, remarque;
    private double quantite;*/
    String listeCrt[] = {"id","idproduit", "remarque"};
    String listeInt[] = null;
    String libEntete[] = {"id","idproduit","remarque","quantite"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setLien((String)session.getValue("lien"));
    pr.getFormu().getChamp("idproduit").setLibelle("produit");
    pr.getFormu().getChamp("remarque").setLibelle("point");

    affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet liste1 = new TypeObjet();
    liste1.setNomTable("point");
    liste[0] = new Liste("remarque", liste1,"val", "val");
    pr.getFormu().changerEnChamp(liste);
    
    pr.getFormu().getChamp("remarque").setLibelle("point");
    
    if(request.getParameter("remarque")== null){
        pr.getFormu().getChamp("remarque").setDefaut(AlloSakafoService.getRestau(null).getVal());
    }
    
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.creerObjetPage(libEntete, null);
     pr.setApres("stock/besoin/besoin-quotidien.jsp");
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste besoin</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/besoin/besoin-quotidien.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  
            String lienTableau[] = {pr.getLien() + "?but=stock/besoin/besoin-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id","produit","point","quantit�"};
             
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
<% }catch(Exception e){
    e.printStackTrace();
    throw e;
}%>