
<%@page import="mg.allosakafo.produits.RecetteResteLib"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<%
    try {
        RecetteResteLib lv = new RecetteResteLib();
        lv.setNomTable("resteingredient");
        String listeCrt[] = {"idingredients", "libelleproduit", "libelleingredient", "daty"};
        String listeInt[] = {"daty"};
        String libEntete[] = {"libelleproduit", "quantite"};

        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, listeCrt.length, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        pr.getFormu().getChamp("idingredients").setVisible(false);
        pr.setApres("stock/besoin/reste-ingredient-liste.jsp");
        String[] colSomme = {"quantite"};
//        String daty =  request.getParameter("daty") != null ? " and daty <='"+request.getParameter("daty") + "' " : " and daty <='"+ Utilitaire.dateDuJour() +"'" ;
//        pr.setAWhere(" a ");
        pr.getFormu().getChamp("idingredients").setLibelle("id ingrédients");
        pr.getFormu().getChamp("libelleproduit").setLibelle("libellé produit");
        pr.getFormu().getChamp("libelleingredient").setLibelle("libellé ingredient");
        pr.getFormu().getChamp("daty1").setLibelle("date");


        pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Reste Ingr&eacute;dients</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/besoin/reste-ingredient-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"ingredient", "quantite"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);

            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<% } catch (Exception ex) {
        ex.printStackTrace();
    }%>