<%-- 
    Document   : liste-achat-besoin-compose
    Created on : 10 juin 2020, 14:13:13
    Author     : rajao
--%>

<%@page import="bean.AdminGen"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.allosakafo.stock.EtatdeStockDate"%>
<%@page import="mg.allosakafo.achat.AchatFournisseur"%>
<%@page import="mg.allosakafo.achat.Achat"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.Utilitaire"%>
<% try {
        EtatdeStockDate ac = new EtatdeStockDate();
        ac.setNomTable("AS_ETATSTOCKVIDE");
        String listeCrt[] = {"daty", "ingredients", "idCategorieIngredient","idfournisseur", "point"};
        String listeInt[] = {};
        String libEntete[] = {"ingredients", "unite", "daty", "report", "entree", "sortie", "reste","reportSuiv","point"};

        PageRecherche pr = new PageRecherche(ac, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
        UserEJB u = (user.UserEJB) session.getValue("u");
        pr.setUtilisateur(u);
        pr.setLien((String) session.getValue("lien"));
        
        affichage.Champ[] liste = new affichage.Champ[1];
        TypeObjet liste1 = new TypeObjet();
        liste1.setNomTable("point");
        liste[0] = new Liste("point", liste1, "val", "id");
        pr.getFormu().changerEnChamp(liste);
	
        String pointDefaut = session.getAttribute("restaurant").toString();
        pr.getFormu().getChamp("point").setDefaut(pointDefaut);
        
        pr.getFormu().getChamp("ingredients").setLibelleAffiche("Ingredients");
        pr.getFormu().getChamp("daty").setLibelleAffiche("Date");
        pr.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
        pr.getFormu().getChamp("idfournisseur").setLibelle("QteInf");
        pr.setApres("stock/besoin/liste-achat-besoin-compose.jsp");
        String daty2;
        daty2 = request.getParameter("daty");
        if (daty2 == null || daty2.compareToIgnoreCase("") == 0) {
            daty2 = Utilitaire.dateDuJour();
        }
        String colonne=null,ordre=null;
        if(request.getParameter("colonne")!=null && request.getParameter("ordre")!=null){
            colonne=request.getParameter("colonne");
            ordre=request.getParameter("ordre");
        }

        EtatdeStockDate[] tmp = EtatdeStockDate.caculEtatStockBesoinCompose(daty2, request.getParameter("ingredients"), request.getParameter("point"), request.getParameter("idCategorieIngredient"), null, null,colonne,ordre);

        double montantInf=0;
        if(request.getParameter("idfournisseur")!=null&&request.getParameter("idfournisseur").compareToIgnoreCase("")!=0)montantInf=Utilitaire.stringToDouble(request.getParameter("idfournisseur"));
        EtatdeStockDate[] listeachat = ac.getMontantInf(tmp, montantInf);
        
        double montantTotal=AdminGen.calculSommeDouble(listeachat, "montant");
        double montantbesoin=AdminGen.calculSommeDouble(listeachat, "montantbesoin");        
        
        String somDefaut[] = null;
        
        pr.creerObjetPage(libEntete, somDefaut);

%>


<script>
    function changerDesignation() {
        document.incident.submit();
    }
           function exporterMannuel() {
            var data= <%= ac.getJsonAchatBesoin( listeachat , montantInf) %>;
           /* this line is only needed if you are not adding a script tag reference */
            if(typeof XLSX === "undefined") XLSX = require("xlsx");
            /* make the worksheet */
            var ws = XLSX.utils.json_to_sheet(data);
            /* add to workbook */
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "Liste_achat");
            /* generate an XLSX file */
            XLSX.writeFile(wb, "liste-achat-besoin-composer.xlsx");
        }    
    
</script>
<script src="../assets/js/xlsx.full.min.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste des Achats des Besoins Compos�e</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/besoin/liste-achat-besoin-compose.jsp" method="post" name="listeachat" id="listeachat">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        </br>
        <div class="row">
            <div class="row col-md-12"> 
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" align="center">LISTE</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <form action="<%=pr.getLien() %>?but=modifierEtatMultiple.jsp" method="POST">
                               <input type="hidden" name="acte" id="acte" value="achat">
                                 <input type="hidden" name="bute" id="bute" value="stock/besoin/liste-achat.jsp">
                                                            
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                                <thead>
                                    <tr class="head">
                                       
                                        <th align="left">Ingredients</th>
                                        <th align="left">Point</th>
                                        <th align="left">Report Avant</th>
                                        <th align="left">Daty</th>  
                                        
                                        <th align="left">Entree</th>  
                                        <th align="left">Sortie</th>  
                                        
                                        <th align="left">Reste theorique</th>  
                                        <th align="left">Inventaire du jour</th>  
                                        <th align="left">Besoin</th>  
                                        <th align="left">A Fabriquer</th>
                                      
                                         
                                        <th align="left"></th>  
                                    </tr>
                                </thead>
                              

                                    <tbody class="searchable">

                                        <%
                                            for (int i = 0; i < listeachat.length; i++) {
                                        %>
                                        <tr>
                                          
                                            <td align="left"><%=listeachat[i].getIngredients()%></td>
                                            <td align="left"><%=listeachat[i].getPoint()%></td>
                                            <td align="left"><%=Utilitaire.formaterAr(listeachat[i].getReport()) %></td>
                                            <td align="left"><%=Utilitaire.formatterDaty(listeachat[i].getDaty()) %></td>
                                            <td align="left"><%=Utilitaire.formaterAr(listeachat[i].getEntree()) %></td>
                                            <td align="left"><%=Utilitaire.formaterAr(listeachat[i].getSortie()) %></td>

                                            <td align="left"><%=Utilitaire.formaterAr(listeachat[i].getReste())%></td>
                                            <td align="left"><%=Utilitaire.formaterAr(listeachat[i].getReportSuiv() )%></td>
                                            <input type="hidden" id="besoin<%=i%>" value="<%=listeachat[i].getBesoin()%>" tabindex="3">   
                                            <td align="left"><%=Utilitaire.formaterAr(listeachat[i].getBesoin())%></td>   

                                            <td align="left">
                                              <%=Utilitaire.formaterAr(listeachat[i].getAacheter()) %>
                                            </td>  
                                           
                                        </tr>
                                        <% } %>

                                    </tbody>
                                
                            </table>
                         </form>
                             <button class="btn btn-primary" onclick="exporterMannuel()">Export</button>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
    
<% } catch (Exception ex) {
        ex.printStackTrace();
}%>