<%-- 
    Document   : test-update-multiple
    Created on : 11 juin 2020, 12:25:06
    Author     : pro
--%>

<%@page import="mg.allosakafo.achat.Besoin"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="user.UserEJB"%>
<%@page import="affichage.PageRechercheUpdate"%>

<%
    try {
        UserEJB u = (UserEJB) session.getAttribute("u");
        Besoin ce = new Besoin();
        ce.setNomTable("BesoinETLibProduit");
        String listeCrt[] = {"id", "remarque", "quantite", "idproduit"};
        String listeInt[] = null;
        String libEntete[] = {"id", "remarque", "quantite", "libelleProduit", "idproduit","jourssemaine"};

        PageRechercheUpdate pr = new PageRechercheUpdate(ce, request, listeCrt, listeInt, 3, libEntete, "id");
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        pr.setApres("stock/besoin/modif-besoin-multiple.jsp");

        affichage.Champ[] liste = new affichage.Champ[2];



       Liste li = new Liste("jourssemaine");
       String [] val_li = {"0","1","2","3","4","5","6"};
       String [] aff_li = { "Dimanche","Lundi", "Mardi" , "Mercredi" ,"Jeudi" ,"Vendredi" , "Samedi"};
       li.ajouterValeur(val_li, aff_li);
       liste[0]=li;
       
       TypeObjet o = new TypeObjet();
        o.setNomTable("point");
        liste[1] = new Liste("remarque", o, "val", "id");

        pr.getFormuLigne().changerEnChamp(liste);

        pr.getFormuLigne().getChamp("idproduit").setPageAppel("choix/ChoixProduitEtIngredients.jsp");
        pr.getFormuLigne().getChamp("libelleProduit").setAutre("readonly");
        //pr.getFormuLigne().getChamp("remarque").setDefaut("defaut");

        String[] colSomme = null;

        pr.setNpp(300);

        pr.getFormu().getChamp("idproduit").setLibelle("id produit");
        pr.getFormu().getChamp("quantite").setLibelle("quantit�");
        pr.creerObjetPage(libEntete, colSomme);

        int nombreLigne = pr.getTableau().getData().length;
%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Modification besoins</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/besoin/modif-besoin-multiple.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%
            out.println(pr.getTableauRecap().getHtml());
        %>
        <br>

        <form action="<%=pr.getLien()%>?but=apresMultiple.jsp" id='modif-form' method="post" data-parsley-validate>
            <%
                String libEnteteAffiche[] = {"id", "point", "quantite", "libell� Produit", "id produit","jours"};
                pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                out.println(pr.getTableau().getHtmlWithCheckboxUpdateMultiple());

            %>
            <input name="acte" type="hidden" id="acte" value="updateMultiple">
            <input name="bute" type="hidden" id="bute" value="stock/besoin/modif-besoin-multiple.jsp">
            <input name="classe" type="hidden" id="classe" value="mg.allosakafo.achat.Besoin">
            <input name="nomtable" type="hidden" id="classe" value="besoin">
            <input name="nombreLigne" type="hidden" id="nomtable" value="<%=nombreLigne%>">
            <button type='button' onclick="simuler()" >Simmuler Besoin</button>
        </form>
    </section>
</div>
<script language="JavaScript">
   
        function simuler(){
            $('#modif-form').attr('action','<%=pr.getLien()%>?but=stock/besoin/simuler-besoin.jsp');
            $("#modif-form button[name='Submit2']").click();
        }
    
</script>
<%} catch (Exception ex) {
        ex.printStackTrace();
    }%>


