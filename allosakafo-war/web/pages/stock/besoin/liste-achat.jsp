<%-- 
    Document   : liste-achat
    Created on : 15 oct. 2019, 15:22:24
    Author     : ionyr
--%>

<%@page import="bean.AdminGen"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.allosakafo.stock.EtatdeStockDate"%>
<%@page import="mg.allosakafo.achat.AchatFournisseur"%>
<%@page import="mg.allosakafo.achat.Achat"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.Utilitaire"%>
<% try {
        EtatdeStockDate ac = new EtatdeStockDate();
        ac.setNomTable("AS_ETATSTOCKVIDE");
        String listeCrt[] = {"daty", "ingredients", "point", "idCategorieIngredient","idfournisseur"};
        String listeInt[] = {};
        String libEntete[] = {"ingredients", "unite", "daty", "report", "entree", "sortie", "reste"};

        PageRecherche pr = new PageRecherche(ac, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
        UserEJB u = (user.UserEJB) session.getValue("u");
        pr.setUtilisateur(u);
        pr.setLien((String) session.getValue("lien"));
        pr.getFormu().getChamp("ingredients").setLibelleAffiche("Ingredients");
        pr.getFormu().getChamp("daty").setLibelleAffiche("Date");
        pr.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
        pr.getFormu().getChamp("idfournisseur").setLibelle("QteInf");
        pr.setApres("stock/besoin/liste-achat.jsp");
        String daty2;
        daty2 = request.getParameter("daty");
        if (daty2 == null || daty2.compareToIgnoreCase("") == 0) {
            daty2 = Utilitaire.dateDuJour();
        }
        String colonne=null,ordre=null;
        if(request.getParameter("colonne")!=null && request.getParameter("ordre")!=null){
            colonne=request.getParameter("colonne");
            ordre=request.getParameter("ordre");
        }
        EtatdeStockDate[] listeachat = EtatdeStockDate.caculEtatStockBesoin(daty2, request.getParameter("ingredients"), request.getParameter("point"), request.getParameter("idCategorieIngredient"), null, null,colonne,ordre);
        String somDefaut[] = null;
        pr.creerObjetPage(libEntete, somDefaut);
        double montantTotal=AdminGen.calculSommeDouble(listeachat, "montant");
        double montantbesoin=AdminGen.calculSommeDouble(listeachat, "montantbesoin");
        double montantInf=0;
        if(request.getParameter("idfournisseur")!=null&&request.getParameter("idfournisseur").compareToIgnoreCase("")!=0)montantInf=Utilitaire.stringToDouble(request.getParameter("idfournisseur"));
%>

<script src="../assets/js/xlsx.full.min.js"></script>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
    
        function exporterMannuel() {
            var data= <%= ac.getJsonAchat( listeachat , montantInf) %>;
           /* this line is only needed if you are not adding a script tag reference */
            if(typeof XLSX === "undefined") XLSX = require("xlsx");
            /* make the worksheet */
            var ws = XLSX.utils.json_to_sheet(data);
            /* add to workbook */
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "Liste_achat");
            /* generate an XLSX file */
            XLSX.writeFile(wb, "liste-achat.xlsx");
        }   
    
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Achat</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/besoin/liste-achat.jsp" method="post" name="listeachat" id="listeachat">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        </br>
        <div class="row">
            <div class="row col-md-12">
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" align="center">RECAPITULATION</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover table-bordered">
                                <thead>
                                    <tr class="head">
                                        <th width="50%" align="center" valign="top" style="background-color:#ffffff">
                                            
                                        </th>
                                        <th width="50%" align="center" valign="top" style="background-color:#ffffff">Nombre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''" style="">
                                        <td width="50%" align="left">Total montant besoin </td>
                                        <td width="50%" align="left"><%=Utilitaire.formaterAr(montantbesoin) %> </td>
                                    </tr>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''" style="">
                                        <td width="50%" align="left">Total montant total  </td>
                                        <td width="50%" align="left"><%=Utilitaire.formaterAr(montantTotal)  %> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" align="center">LISTE</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <form action="<%=pr.getLien() %>?but=modifierEtatMultiple.jsp" method="POST">
                               <input type="hidden" name="acte" id="acte" value="achat">
                                 <input type="hidden" name="bute" id="bute" value="stock/besoin/liste-achat.jsp">
                                <input class="btn btn-primary" type="submit" value="valider">                            
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                                <thead>
                                    <tr class="head">
                                        <th align="left">#</th>
                                        <th align="left">Ingredients</th>
                                        <th align="left">Reste</th>  
                                        <th align="left">Besoin</th>  
                                        <th align="left">A acheter</th>
                                        <th align="left">Pu</th>  
                                        <th align="left">Montant Besoin</th>
                                        <th align="left">Montant</th>
                                        <th align="left">Fournisseur</th> 
                                         
                                        <th align="left"></th>  
                                    </tr>
                                </thead>
                              

                                    <tbody class="searchable">

                                        <%
                                            for (int i = 0; i < listeachat.length; i++) {
                                                if(listeachat[i].getAacheter()>=montantInf){
                                        %>
                                        <tr>
                                            <td align="center"><input type="checkbox" value="<%=listeachat[i].getIdingredient()+"-"+i%>" name="id" ></td>
                                            <td align="left"><%=listeachat[i].getIngredients()%></td>

                                            <td align="left"> <a href="?but=stock/besoin/reste-ingredient-liste.jsp&idingredients=<%=listeachat[i].getIdingredient()%>&daty1=<%=utilitaire.Utilitaire.datetostring( listeachat[i].getDaty() )%>"><%=Utilitaire.formaterAr(listeachat[i].getReste())%></a> </td>                            
                                            <input type="hidden" id="besoin<%=i%>" value="<%=listeachat[i].getBesoin()%>" tabindex="3">   
                                            <td align="left"><a href="?but=stock/besoin/besoining-liste.jsp&idingredients=<%=listeachat[i].getIdingredient()%>"><%=Utilitaire.formaterAr(listeachat[i].getBesoin())%></a></td>   

                                            <td align="left">
                                                <input name="aacheter" type="text" class="form-control" onchange='calculMontant(<%=i%>)' name="aacheter" id="aacheter<%=i%>" value="<%=Utilitaire.formaterAr(listeachat[i].getAacheter()) %>" tabindex="3">
                                            </td>  
                                            <td align="left">
                                                <input name="pu" type="text" class="form-control" onchange='calculMontant(<%=i%>)' name="pu" id="pu<%=i%>" value="<%=Utilitaire.formaterAr(listeachat[i].getPu())%>" tabindex="3">
                                            </td>
                                            <td align="left">
                                                <input name="montant" type="text" class="form-control" readonly name="montantbesoin" id="montantbesoin<%=i%>" value="<%=Utilitaire.formaterAr(listeachat[i].getMontantbesoin())%>" tabindex="3">
                                            </td>
                                            <td align="left">
                                                <input name="montant" type="text" class="form-control" readonly name="montant" id="montant<%=i%>" value="<%=Utilitaire.formaterAr(listeachat[i].getMontant())%>" tabindex="3">
                                            </td>  

                                            <td>
                                                <input type="text" class="form-control" readonly name="libelleidFournisseur" id="libelleidFournisseur<%=i%>" value="<%=Utilitaire.champNull(listeachat[i].getFournisseur()) %>">
                                          
                                                <input type="hidden" class="form-control" name="idFournisseur" id="idFournisseur<%=i%>" value="<%=Utilitaire.champNull(listeachat[i].getIdfournisseur()) %>">
                                            </td>

                                            <td>
                                                <input name="choix" type="button" class="submit" onclick="pagePopUp('choix/listeFournisseurChoix.jsp?champReturn=idFournisseur<%=i%>;libelleidFournisseur<%=i%>')" value="...">
                                            </td>

                                        </tr>
                                        <% }} %>

                                    </tbody>
                                
                            </table>
                         </form>
                          <button class="btn btn-primary" onclick="exporterMannuel()"  >Export</button>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
    <script> 
       
        function calculMontant(i){
            var pu=document.getElementById("pu"+i).value;
            var montant=document.getElementById("aacheter"+i).value;
            var besoin=document.getElementById("besoin"+i).value;
            document.getElementById("montant"+i).value=parseFloat(pu)*parseFloat(montant);
             document.getElementById("montantbesoin"+i).value=Math.round(parseFloat(pu)*parseFloat(besoin),2);
        }
    </script>    
<% } catch (Exception ex) {
        ex.printStackTrace();
    }%>