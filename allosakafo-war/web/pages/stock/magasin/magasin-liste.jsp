<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.commun.Dr"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="affichage.PageRecherche"%>
<% StMagasin lv = new StMagasin();

    lv.setNomTable("ST_MAGASIN_LIBELLE");
    String listeCrt[] = {"id", "code", "desce", "code_dr"};
    String listeInt[] = null;
    String libEntete[] = {"id", "code", "desce", "code_dr","typemagasin"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));

    affichage.Champ[] liste = new affichage.Champ[2];
    Dr dr = new Dr();
    dr.setNomTable("sig_dr");
    liste[0] = new Liste("code_dr", dr, "val", "val");
    
    TypeObjet tpm = new TypeObjet();
    tpm.setNomTable("st_type_magasin");
    liste[1] = new Liste("typemagasin", tpm, "val", "id");
    
    pr.getFormu().changerEnChamp(liste);
    
    pr.setApres("stock/magasin/magasin-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste magasin</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/magasin/magasin-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/magasin/magasin-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Code", "Description", "Code Direction Regionale","Type Magasin"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
