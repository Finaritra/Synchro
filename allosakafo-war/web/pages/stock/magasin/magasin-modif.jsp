<%@page import="mg.cnaps.commun.Dr"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    UserEJB u;
    StMagasin mag;
%>
<%
    mag = new StMagasin();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(mag, request, (user.UserEJB) session.getValue("u"));
	
    pi.setLien((String) session.getValue("lien"));
    affichage.Champ[] liste = new affichage.Champ[2];
    Dr dr = new Dr();
    dr.setNomTable("sig_dr");
    liste[0] = new Liste("code_dr", dr, "val", "id");
    
    TypeObjet tpm = new TypeObjet();
    tpm.setNomTable("st_type_magasin");
    liste[1] = new Liste("typemagasin", tpm, "val", "id");
    
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("code").setLibelle("Code");
    pi.getFormu().getChamp("desce").setLibelle("Description");
    pi.getFormu().getChamp("code_dr").setLibelle("Direction regionale");
    pi.getFormu().getChamp("typemagasin").setLibelle("Type Magasin");
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification Magasin</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="stmagasin">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/magasin/magasin-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StMagasin">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                        <input name="nomtable" type="hidden" id="nomtable" value="st_magasin">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
