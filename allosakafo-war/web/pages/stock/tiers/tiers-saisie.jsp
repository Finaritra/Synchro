<%-- 
    Document   : tiers-saisie
    Created on : 8 sept. 2015, 14:05:18
    Author     : user
--%>
<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="mg.cnaps.st.StVueTiers"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StTiers a = new StTiers();
    //PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));

    //pi.setLien((String) session.getValue("lien"));
    //affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet t = new TypeObjet();
    t.setNomTable("st_type_tiers");
    TypeObjet[] lstp = (TypeObjet[]) CGenUtil.rechercher(t, null, null, ""); //new Liste("typetiers", t, "val", "id");
    String remarque = "", adresse = "";

    remarque = request.getParameter("montant");
    if (remarque == null) {
        remarque = "";
    }
    adresse = request.getParameter("adresse");
    if (adresse == null) {
        adresse = "";
    }
    /*
     pi.getFormu().changerEnChamp(liste);

     pi.getFormu().getChamp("raisonsocial").setLibelle("Raison social");
     pi.getFormu().getChamp("nif").setLibelle("Nif");
     pi.getFormu().getChamp("stat").setLibelle("Stat");
     pi.getFormu().getChamp("adresse").setLibelle("Adresse");
     pi.getFormu().getChamp("codeposte").setLibelle("Code poste");
     pi.getFormu().getChamp("rubrique").setLibelle("Rubrique");
     pi.getFormu().getChamp("matrcnaps").setLibelle("Matricule cnaps"); //employeur
     pi.getFormu().getChamp("matrcnaps").setPageAppel("choix/employeurChoixMultiple.jsp", "matrcnaps;raisonsocial;nif;stat;adresse");
     pi.getFormu().getChamp("remarque").setLibelle("Employeur");
     pi.getFormu().getChamp("remarque").setType("textarea");
     pi.getFormu().getChamp("typetiers").setLibelle("Type tiers");
     pi.getFormu().getChamp("taxe").setLibelle("Taxe");
     pi.getFormu().getChamp("compte_tiers").setLibelle("Compte tiers");
     pi.getFormu().getChamp("compteg").setLibelle("Compte G");
     //    pi.getFormu().getChamp("idcompta").setLibelle("Compta");
     //    pi.getFormu().getChamp("compteg").setLibelle("Compte g�n�rale");
     //    pi.getFormu().getChamp("numerotiers").setLibelle("Numero tiers");

     pi.getFormu().getChamp("compteg").setPageAppel("choix/compteAuxChoix.jsp");
     pi.getFormu().getChamp("remarque").setPageAppel("choix/employeurChoix.jsp");

     pi.preparerDataFormu();
     */
%>
<div class="content-wrapper">
    <h1 align="center">Auxilliaire</h1>

    <form action="<%=session.getValue("lien")%>?but=apresTarif.jsp" method="post" name="tiers" id="tiers" data-parsley-validate="" novalidate="">
        <div class="row">
            <div class="col-md-6">
                <div class="box-insert">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th><label for="Type tiers" tabindex="10">Type tiers</label></th>
                                        <td>

                                            <select name="typetiers" class="form-control" id="typetiers" tabindex="10" data-parsley-id="22">
                                                <% for (int k = 0; k < lstp.length; k++) {%>
                                                <option value="<%=lstp[k].getId()%>" onclick="changePopupBenef('<%=lstp[k].getVal()%>')"><%=lstp[k].getVal()%></option>

                                                <%}%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr><th><label for="Employeur" tabindex="11">Employeur</label></th>
                                        <td>
                                            <input name="remarque" class="form-control" id="remarque" data-parsley-id="24" value="<%
                                                if (remarque.compareTo("") != 0) {
                                                    out.print(utilitaire.Utilitaire.champNull(remarque));
                                                }
                                                   %>"> 
                                        </td>
                                        <td>
                                            <div class="input-group-btn">
                                                <button class="btn btn-default btn-sm" id="bouttonEmp" type="button">...</button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><label for="Matricule cnaps" tabindex="1">Matricule cnaps</label></th>
                                        <td><input name="matrcnaps" type="textbox" class="form-control" id="matrcnaps" tabindex="1" data-parsley-id="4"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th><label for="Raison social" tabindex="2">Raison social</label></th>
                                        <td><input name="raisonsocial" type="textbox" class="form-control" id="raisonsocial" value="" tabindex="2" data-parsley-id="6"></td>
                                    </tr>
                                    <tr><th><label for="Nif" tabindex="3">NIF</label></th><td><input name="nif" type="textbox" class="form-control" id="nif" value="" tabindex="3" data-parsley-id="8"></td></tr>
                                    <tr><th><label for="Stat" tabindex="4">STAT</label></th><td><input name="stat" type="textbox" class="form-control" id="stat" value="" tabindex="4" data-parsley-id="10"></td></tr>
                                    <tr>
                                        <th>
                                            <label for="Adresse" tabindex="5">Adresse</label>
                                        </th>
                                        <td>
                                            <input name="adresse" type="textbox" class="form-control" id="adresse" value="<%
                                                if (adresse.compareTo("") != 0) {
                                                    out.print(utilitaire.Utilitaire.champNull(adresse));
                                                }
                                                   %>" tabindex="5" data-parsley-id="12">
                                        </td>
                                    </tr>
                                    <tr><th><label for="Code poste" tabindex="6">Code poste</label></th><td><input name="codeposte" type="textbox" class="form-control" id="codeposte" value="" tabindex="6" data-parsley-id="14"></td></tr>
                                    <tr><th><label for="Taxe" tabindex="7">Taxe</label></th><td><input name="taxe" type="textbox" class="form-control" id="taxe" value="0" onblur="calculer('taxe')" tabindex="7" data-parsley-id="16"></td></tr>
                                    <tr><th><label for="Rubrique" tabindex="8">Rubrique</label></th><td><input name="rubrique" type="textbox" class="form-control" id="rubrique" value="" tabindex="8" data-parsley-id="18"></td></tr>
                                    <tr><th><label for="contact" tabindex="9">Contact</label></th><td><input name="contact" type="textbox" class="form-control" id="contact" value="" tabindex="9" data-parsley-id="20"></td></tr>


                                    <tr>
                                        <th><label for="Compte general" tabindex="12">Compte general</label></th><td><input name="compteg" type="textbox" class="form-control" id="compteg"  tabindex="12" data-parsley-id="26"></td>
                                        <td><input name="choix" type="button" class="submit" onclick="pagePopUp('choix/compteAuxChoix.jsp?champReturn=compteg')" value="..."></td></tr>
                                    </tr>
                                    <tr><th><label for="Compte tiers" tabindex="12">Compte tiers</label></th><td><input name="compte_tiers" type="textbox" class="form-control" id="compte_tiers"  tabindex="12" data-parsley-id="26"></td></tr>
                                    <tr>
                                        <th><label for="Type tiers" tabindex="10">Assujettis</label></th>
                                        <td>

                                            <select name="assujettis" class="form-control" id="typetiers" tabindex="10" data-parsley-id="22">

                                                <option value="oui">oui</option>
                                                <option value="non">non</option>
                                            </select>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <div class="col-xs-12">

                                <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;" tabindex="131">Valider</button> 
                                <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;" tabindex="132">R�initialiser</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/tiers/tiers-liste.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StTiers">
    </form>

    <!--<form action="<=pi.getLien()%>?but=apresTarif.jsp" method="post" name="tiers" id="tiers" data-parsley-validate>
    
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="stock/tiers/tiers-liste.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StTiers">
    </form>-->
</div>
<script>
    function changePopupBenef(text) {
        //var buttonBenef = document.getElementById("bouttonBenef");
        if (text === 'Fournisseur') {
            jQuery('#bouttonEmp').unbind('click');
            $("#bouttonEmp").click(function () {
                pagePopUp('choix/employeurChoixMultipletrs.jsp?champReturn=remarque;matrcnaps;raisonsocial;nif;stat;adresse');
            });
        }
        if (text === 'Client') {
            jQuery('#bouttonEmp').unbind('click');
            $("#bouttonEmp").click(function () {
                pagePopUp('choix/employeurChoixMultipletrs.jsp?champReturn=remarque;matrcnaps;raisonsocial;nif;stat;adresse');
            });
        }
        if (text === 'Salarie') {
            jQuery('#bouttonEmp').unbind('click');
            $("#bouttonEmp").click(function () {
                pagePopUp('choix/logPersonnelChoixSaisieTiers.jsp?champReturn=remarque;adresse;matrcnaps;raisonsocial');
            });
        }
    }
</script>