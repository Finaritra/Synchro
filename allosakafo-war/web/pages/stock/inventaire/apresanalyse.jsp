<%@page import="user.*" %>
<%@page import="bean.*" %>
<%@page import="affichage.*" %>

<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
        String typeBoutton;
        String val;
    %>
    <%
        try {
            typeBoutton = request.getParameter("type");
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            bute = request.getParameter("bute");
            String classe = request.getParameter("classe");
            ClassMAPTable t = null;
            val = "";

            if (bute == null || bute.compareToIgnoreCase("") == 0) {
                bute = "pub/Pub.jsp";
            }

            if (classe == null || classe.compareToIgnoreCase("") == 0) {
                classe = "pub.Montant";
            }

            if (typeBoutton == null || typeBoutton.compareToIgnoreCase("") == 0) {
                typeBoutton = "3"; //par defaut modifier
            }

            if (acte.compareToIgnoreCase("genereranalyse") == 0) {
                String daty = request.getParameter("dateinv");
                String magasin = request.getParameter("magasin");
                val = u.genererAnalyseEtatEcart(daty, magasin, "STI000009");
                if(!val.isEmpty()){
                    val = val + "&magasin=" + magasin + "&inventaire=STI000009";
                }
            }
         
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>&valeur=<%=val%>");</script>
    <%
        } catch (Exception e) {
            e.printStackTrace();
    %>
    <script language="JavaScript"> alert('<%=e.getMessage()%>');history.back();</script>
    <%
            return;
        }
    %>
</html>