    
<%@page import="utilitaire.Utilitaire"%>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>

<%@page import="affichage.PageRecherche"%>
<% 
    InventaireMere lv = new InventaireMere();
    lv.setNomTable("INVENTAIREMERELIBINCIDENT");
    String listeCrt[] = {"id", "daty", "type",  "depot", "idcategorieIngredient"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "type",  "depot", "idcategorieIngredient","observation","nbincident","etat"};
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    affichage.Champ[] listePoint = new affichage.Champ[1];
    TypeObjet liste1 = new TypeObjet();
    liste1.setNomTable("point");
    listePoint[0] = new Liste("depot", liste1, "val", "id");
    pr.getFormu().changerEnChamp(listePoint);
    
    pr.getFormu().getChamp("depot").setDefaut(session.getAttribute("restaurant").toString());
    
    pr.getFormu().getChamp("daty1").setDefaut(Utilitaire.dateDuJour());
    pr.getFormu().getChamp("daty2").setDefaut(Utilitaire.dateDuJour());
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date Max");
    pr.setApres("stock/inventaire/inventaire-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste inventaire</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/inventaire/inventaire-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/inventaire/inventaire-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            //String libEnteteAffiche[] = {"id", "Date", "Designation", "Magasin", "Remarque"};
            //pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
