<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%
    UserEJB u;
    StInventaire art;
%>
<%
    art = new StInventaire();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(art, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("magasin").setPageAppel("choix/magasinChoix.jsp");
    pi.getFormu().getChamp("designation").setType("textarea");
    pi.getFormu().getChamp("remarque").setType("textarea");
    pi.getFormu().getChamp("etat").setVisible(false);
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification inventaire</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="stinventaire">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="<%=(String) session.getValue("lien")%>?but=stock/inventaire/inventaire-details.jsp?id_inventaire=<%out.print(request.getParameter("id"));%>" class="btn btn-primary  pull-right" style="margin-right: 10px">Modifier d&eacute;tails</a>
                                <button class="btn btn-success pull-right" name="Submit2" type="submit" style="margin-right: 10px">Valider</button>
                            </div>
                            <br><br>
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/inventaire/inventaire-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StInventaire">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
