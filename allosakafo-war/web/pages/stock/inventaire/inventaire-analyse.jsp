<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%@ page import="affichage.*" %>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

<%!
StInventaire invtr;
%>
<%
invtr=new StInventaire();
String listeCrt[]={"id", "daty", "designation",  "magasin", "remarque", "etat"};
String listeInt[]={"daty"};
String[] pourcentage={"nombrepargroupe"};
String colDefaut[]={"id", "daty", "designation",  "magasin", "remarque", "etat"};
String somDefaut[]={};
PageRechercheGroupe pr=new PageRechercheGroupe(invtr,request,listeCrt,listeInt,3,colDefaut,somDefaut,pourcentage,6,0);

pr.setUtilisateur((user.UserEJB)session.getValue("u"));
pr.setLien((String)session.getValue("lien"));
pr.setApres("stock/inventaire/inventaire-analyse.jsp");
pr.creerObjetPagePourc();
%>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Analyse</h1>
	</section>
	<section class="content">
		<form action="<%=pr.getLien()%>?but=stock/inventaire/inventaire-analyse.jsp" method="post" name="analyse" id="analyse">
			<%out.println(pr.getFormu().getHtmlEnsemble());%>
		</form>
		<%
		String lienTableau[]={pr.getLien()+"?but=stock/inventaire/inventaire-fiche.jsp&id="+pr.getFormu().getListeCritereString()};
		String colonneLien[]={"id"};
		pr.getTableau().setLien(lienTableau);
		pr.getTableau().setColonneLien(colonneLien);
		out.println(pr.getTableauRecap().getHtml());%>
		<br>
		<%
		out.println(pr.getTableau().getHtml());
		out.println(pr.getBasPage());
		%>
	</section>
</div>