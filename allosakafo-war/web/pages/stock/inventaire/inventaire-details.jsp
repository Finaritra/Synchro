<%-- 
    Document   : inventaire-details
    Created on : 12 nov. 2015, 15:21:08
    Author     : Joe
--%>
<%@page import="mg.cnaps.st.St_inventaire_lib"%>
<%@page import="mg.cnaps.st.StInventaire"%>
<%@page import="affichage.PageRecherche"%>
<% 
    try{
    St_inventaire_lib lv = new St_inventaire_lib();
    lv.setNomTable("ST_INVENTAIRE_LIB");

   // lv.setNomTable("ST_INVENTAIRE_LIBELLE");
    String listeCrt[] = {"id", "id_inventaire", "daty", "designation", "code_article", "idMagasin", "remarque"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "magasin", "article", "designation", "quantite", "remarque"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date Max");
    pr.getFormu().getChamp("idMagasin").setLibelle("Magasin");
    pr.getFormu().getChamp("code_article").setLibelle("Article");
    
    pr.getFormu().getChamp("code_article").setPageAppel("choix/listeArticleChoix.jsp");
    pr.getFormu().getChamp("idMagasin").setPageAppel("choix/listeMagasinChoix.jsp");
    
    pr.setApres("stock/inventaire/inventaire-details.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste inventaire</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/inventaire/inventaire-details.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/inventaire/inventaire_fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());
        %>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "Date", "Magasin", "Article" ,"Designation", "Quantite", "Remarque"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
    <%
    }catch(Exception ex){
        ex.printStackTrace();
    }
    %>
        