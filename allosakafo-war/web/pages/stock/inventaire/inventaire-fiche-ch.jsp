<%-- 
    Document   : inventaire-fiche-ch
    Created on : 6 janv. 2016, 09:51:39
    Author     : Joe
--%>



<%@page import="mg.cnaps.st.StInventaireFiche"%>
<%@page import="affichage.PageRecherche"%>
<% 
    StInventaireFiche inv = new StInventaireFiche();

     inv.setNomTable("st_inventaire_fiche");

   // lv.setNomTable("ST_INVENTAIRE_LIBELLE");
    String listeCrt[] = {"daty", "magasin", "article", "chapitre"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"Daty", "Magasin", "Observation", "Article", "Quantite", "Chapitre"};

    PageRecherche pr = new PageRecherche(inv, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date Max");
    pr.getFormu().getChamp("magasin").setLibelle("Magasin");
    pr.getFormu().getChamp("article").setLibelle("Magasin");
    pr.getFormu().getChamp("chapitre").setLibelle("Chapitre");
    
    pr.setApres("stock/inventaire/inventaire-fiche-ch.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste inventaire</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/inventaire/inventaire-fiche-ch.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Date", "Magasin", "Observation", "Article", "Quantite", "Chapitre"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
