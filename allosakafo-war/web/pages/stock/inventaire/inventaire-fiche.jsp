<%-- 
    Document   : as-produits-fiche
    Created on : 1 d�c. 2016, 10:40:08
    Author     : Joe
--%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.stock.*"%>
<%
    InventaireMere a = new InventaireMere();
    a.setNomTable("inventaireMereLib");
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin

    
    pc.setTitre("Consultation inventaire");
    InventaireMere mere=(InventaireMere)pc.getBase();
    InventaireDetails[] filles=mere.getFille("INVENTAIREDETAILSLIB",null);
     String csv="[";
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/inventaire/inventaire-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-warning pull-left"  href="<%=(String) session.getValue("lien") + "?but=produits/as-produits-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <a class="btn btn-info pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&classe=mg.allosakafo.stock.InventaireMere&bute=stock/inventaire/inventaire-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">valider</a>
                            <a class="btn btn-info pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annulerVisa&classe=mg.allosakafo.stock.InventaireMere&bute=stock/inventaire/inventaire-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler valider</a>
                            <button class="btn btn-primary pull-right" onclick="exporter()"  >Export</button>    
                        </div>
                        <br/>

                    </div>
                </div>
            </div>
        </div>
    </div>
                        <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title with-border">
                    <h1 class="box-title">D&eacute;tails</h1>
                </div>
                <div class="box-body no padding">
                    <table class="table table-bordered">
                        <tr>
                            <th align="center">ID</th>
                            <th align="center">Produit</th>
                            <th align="center">Quantite</th>
                            
                        </tr>

                        <%
                            for (int i = 0; i < filles.length; i++) {
                                   csv +="['" +filles[i].getId() + "','" ;
                                                csv += filles[i].getIdingredient() + "','" ;
                                                csv +=  filles[i].getQuantiter() + "','" ;
                                                csv += mere.getId() + "','" ;
                                                csv += mere.getDaty() + "','" ;
                                                csv += mere.getDepot() + "','" ;
                                                csv += mere.getIdcategorieingredient()+ "','" ;
                                                csv += mere.getType() +"'],";
                        %>
                        <tr>
                            <td align="left"><%=filles[i].getId()%></td>
                            <td align="left"><%=filles[i].getIdingredient() %></td>
                            <td align="right"><%=filles[i].getQuantiter() %></td>
                        
                        </tr>
                        <% }%> 
                    </table>
                </div>


            </div> 
        </div> 

    </div> 
</div>
                     <% csv+="];"; %>
    <script> 
       
       function exporter() {
             var csv = 'Id;Ingredient;Quantiter;IdMere;Date;Depot;Categorie;Type\n';
             var data= <%=csv%>;
            data.forEach(function(row) {
                    csv += row.join(';');
                    csv += "\n";
            });

            console.log(csv);
            var hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
            hiddenElement.target = '_blank';
            hiddenElement.download = 'inventaire.csv';
            hiddenElement.click();
        }
        
        
    
    </script>