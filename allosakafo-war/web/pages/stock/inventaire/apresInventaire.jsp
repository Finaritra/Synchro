<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%@ page import="java.sql.*" %>
<%!
    UserEJB u = null;
    String acte = null;
    String lien = null;
    int nb = 0;
    String[] idInventaire;
    String[] idArticle;
    String[] quantite;
    Connection c = null;
%>
<%

    try {
        c = new UtilDB().GetConn();
        lien = (String) session.getValue("lien");
        u = (UserEJB) session.getAttribute("u");
        String h = request.getParameter("nb");
        acte = request.getParameter("acte");
        if (h != null && h.compareToIgnoreCase("") != 0) {
            nb = Utilitaire.stringToInt(h);
        }
        idInventaire = new String[nb];
        idArticle = new String[nb];
        quantite = new String[nb];
        StInventaireFille[] liste_inventaire = new StInventaireFille[nb];

        for (int i = 0; i < nb; i++) {
            idInventaire[i] = request.getParameter("idInventaire" + i);
            idArticle[i] = request.getParameter("idArticle" + i);
            quantite[i] = request.getParameter("quantite" + i);
            StInventaireFille nc = new StInventaireFille(Utilitaire.stringToDouble(quantite[i]), idInventaire[i], idArticle[i]);
            liste_inventaire[i] = nc;
        }
        u.createInventaireMultiple(liste_inventaire, c);
        u.createMvtStock(idInventaire[0],c,idArticle,quantite);
%>
<script language="JavaScript"> document.location.replace("<%=lien%>?but=stock/inventaire/inventaire-saisie.jsp");</script>
<%       } catch (Exception e) {
    e.printStackTrace();
%>
<script language="JavaScript"> alert('<%=e.getMessage()%>');
    history.back();</script>
    <%
            return;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    %>
