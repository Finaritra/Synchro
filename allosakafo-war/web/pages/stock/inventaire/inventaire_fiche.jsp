<%--
    Document   : inventaire_fiche
    Created on : 12 nov. 2015, 15:21:59
    Author     : Joe
--%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>

<%
    St_inventaire_lib lv = new St_inventaire_lib();
    lv.setNomTable("ST_INVENTAIRE_LIB");
    String[] libelleInventaireFiche = {"id", "REF Inventaire", "Date", "Designation", "REF Magasin", "Magasin","Remarque", "Quantite", "Code Article", "Article" };
    PageConsulte pc = new PageConsulte(lv, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setLibAffichage(libelleInventaireFiche);
    pc.setTitre("Fiche Inventaire");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/inventaire/inventaire-details.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <div class="box-footer">
                            <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/inventaire/inventaire-fille-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/mvtStock/apresMvtStock.jsp&id=" + request.getParameter("id")%>&acte=deletefille&bute=stock/inventaire/inventaire-liste.jsp&classe=mg.cnaps.st.StInventaireFille" style="margin-right: 10px">Supprimer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
