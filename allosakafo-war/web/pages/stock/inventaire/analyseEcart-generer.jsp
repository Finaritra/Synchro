
<%@page import = "mg.cnaps.st.StEtatMvtInventaire"%>
<%@page import = "affichage.PageRecherche"%>
<%@page import = "mg.cnaps.st.StCalculEtatStock"%>
<%@page import = "mg.cnaps.st.StMagasin"%>
<%@page import = "user.UserEJB"%>
<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    String lien = (String) session.getValue("lien");
    
    StMagasin[] mags = (StMagasin[]) u.getData(new StMagasin(), null, null, null, "");
%>

<div class="content-wrapper">
    <h1 align="center">Analyser etat des �carts</h1>
    <form action="<%=lien%>?but=stock/inventaire/apresanalyse.jsp" method="post" name="analyse" id="analyse" data-parsley-validate="" novalidate="">
        <div class="row">
            <div class="col-md-6">
                <div class="box-insert">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>
                                            <label tabindex="1" for="magasin">Magasin</label>
                                        </th>
                                        <td>
                                            <select id="magasin" class="form-control" tabindex="1" name="magasin" data-parsley-id="2">
                                            <%
                                                if(mags != null && mags.length > 0){ 
                                                    for(int i = 0; i < mags.length; i++){%>
                                                    <option value="<%=mags[i].getId()%>"><%=mags[i].getDesce()%></option>
                                            <% }} %>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <label tabindex="2" for="magasin">Inventaire du</label>
                                        </th>
                                        <!--<td>
                                            <select id="inventaire" class="form-control" tabindex="1" name="inventaire" data-parsley-id="2">
                                            
                                            </select>
                                        </td>-->
                                        <td>
                                            <input id="dateinv" class="form-control datepicker" type="textbox" tabindex="1" name="dateinv" data-parsley-id="1">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <div class="col-xs-12">
                                <button class="btn btn-success pull-right" tabindex="91" style="margin-right: 25px;" name="Submit2" type="submit">Valider</button>
                                <button class="btn btn-default pull-right" tabindex="92" style="margin-right: 15px;" name="Submit2" type="reset">R�initialiser</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input name="acte" type="hidden" id="nature" value="genereranalyse">
        <input name="bute" type="hidden" id="bute" value="stock/inventaire/analyseEcart-generer.jsp">
    </form>
<%
    String id = request.getParameter("valeur");
    String magasin = request.getParameter("magasin");
    String inventaire = request.getParameter("inventaire");
    if(id != null && magasin != null && inventaire != null){
        StEtatMvtInventaire stee = new StEtatMvtInventaire();
        String listeCrt[] = {"article"};
        String listeInt[] = null;
        String libEntete[] = {"daty", "magasin", "article", "quantite_reel", "quantite_inventorie"};
        PageRecherche pr = new PageRecherche(stee, request, listeCrt, listeInt, 3, libEntete, 4);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        String where = " AND (IDMAGASIN = '" + magasin + "' OR IDMAGASIN IS NULL) AND IDINVENTAIRE = '" + inventaire + "'";
        pr.setAWhere(where);
        String apres = "stock/inventaire/analyseEcart-generer.jsp&valeur="+id+"&magasin="+magasin+"&inventaire="+inventaire;
        pr.setApres(apres);
        pr.creerObjetPage(libEntete);
%>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/inventaire/analyseEcart-generer.jsp&valeur=<%=id%>&magasin=<%=magasin%>&inventaire=<%=inventaire%>" method="post" name="analyseliste" id="analyseliste">
            <% out.println(pr.getFormu().getHtmlEnsemble());%>
        </form>
        <%
            String libEnteteAffiche[] = {"Date", "Magasin", "Article", "Quantit� r�elle", "Quantit� inventori�e"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
<% } %>
</div>
                                            
<script type="text/javascript">
    
    $(function() {
        //loadInventaire();
    });
    
    function loadInventaire()
    {
	var mag = $("#magasin").val();
	$.ajax({
	    type: 'GET',
	    url: '${pageContext.request.contextPath}/DoRecupererListeInventaire',
	    contentType: 'application/json',
	    data: {
		'magasin': mag
	    },
	    success: function (data) {
		
	    }
	});
    }
    
    
</script>                        
                                            
