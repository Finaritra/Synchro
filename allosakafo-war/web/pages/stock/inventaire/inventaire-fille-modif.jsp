<%-- 
    Document   : inventaire-fille-modif
    Created on : 9 mars 2016, 15:39:47
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%
    UserEJB u;
    StInventaireFille art = new StInventaireFille();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pi = new PageUpdate(art, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification inventaire</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=stock/mvtStock/apresMvtStock.jsp" method="post" name="stinventaire">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success pull-right" name="Submit2" type="submit" style="margin-right: 10px">Valider</button>
                            </div>
                            <br><br>
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="stock/inventaire/inventaire-details.jsp&id=<%=request.getParameter("id")%>">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StInventaireFille">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
