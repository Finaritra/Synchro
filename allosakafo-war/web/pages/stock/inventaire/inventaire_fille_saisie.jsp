<%--
    Document   : inventaire-libelle-etat
    Created on : 9 f�vr. 2016, 09:27:47
    Author     : Joe

--%>

<%@page import="mg.cnaps.st.StockService"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.cnaps.st.StInventaireLibelleEtat"%>
<%@page import="mg.cnaps.st.StArticle"%>
<%@page import="mg.cnaps.st.StInventaireFille"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
    try {
        StInventaireLibelleEtat lv = new StInventaireLibelleEtat();
        StArticle article = new StArticle();
        article.setNomTable("ST_ARTICLE_LIBELLE");
        String listeCrt[] = {"type"};
        String listeInt[] = null;
        String libEntete[] = {"id", "designation"};

        
                //System.out.println("ID INVENTAIRE: "+request.getParameter("id"));
        PageRecherche pr = new PageRecherche(article, request, listeCrt, listeInt, 2, libEntete, 2);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        pr.setApres("stock/inventaire/inventaire_fille_saisie.jsp");

        UserEJB u = (UserEJB) session.getValue("u");

        StInventaireFille dejaSaisi = new StInventaireFille();
        dejaSaisi.setNomTable("ST_INVENTAIRE_FILLE");
        StInventaireFille[] listeDejaSaisie = (StInventaireFille[]) u.getData(dejaSaisi, null, null, null, " AND INVENTAIRE LIKE '%" + request.getParameter("id") + "%'");

        if(listeDejaSaisie != null && listeDejaSaisie.length > 0){
            String temp = StockService.getAWhere(listeDejaSaisie);
            pr.setAWhere(" and ID not in "+temp);
        }
        String[] colSomme = null;
        pr.creerObjetPage(libEntete);

        StArticle[] listeInv = (StArticle[]) pr.getRs().getResultat();
        String tmp = "0";
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>INVENTAIRE ARTICLE</h1>
    </section>
    <section class="content">

        </br>
        <div class="row">
            <div class="row col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" align="center">LISTE</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <form name="mep" action="<%=pr.getLien()%>?but=stock/mvtStock/apresMvtStock.jsp" method="post">
                                <input type="hidden" name="acte" value="saisieInventaire">
                                <input type="hidden" name="bute" value="stock/inventaire/inventaire_fille_saisie.jsp">
                                <input type="hidden" name="inventaire" value="<%=request.getParameter("id")%>">

                                <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                                    <input id="filter" type="text" class="form-control" style="width: 200px" placeholder="Filtre...">
                                    <a class="btn btn-primary pull-right" href="<%=pr.getLien()%>?but=stock/inventaire/inventaire-fiche.jsp&id=<%=request.getParameter("id")%>">Voir fiche</a>
                                    <thead>
                                        <tr class="head">
                                            <th align="left">ID</th>
                                            <th align="left">ARTICLE</th>
                                            <th align="left">Chapitre</th>
                                            <th align="right">Quantit&eacute; inventori&eacute;e</th>
                                        </tr>
                                    </thead>
                                    <tbody class="searchable">
                                        <%
                                            for (int i = 0; i < listeInv.length; i++) {
                                                
                                        %>
                                    <input type="hidden" name="id" value="<%=listeInv[i].getId()%>">
                                    <tr>
                                        <td align="left"><%=listeInv[i].getId()%></td>
                                        <td align="left"><%=listeInv[i].getDesignation()%></td>
                                        <td align="left"><%=listeInv[i].getType()%></td>
                                        <td align="left"><input type="text" name="quantite" value="<%=tmp%>"></td>
                                    </tr>
                                    <%}%>
                                    </tbody>
                                </table>
                                <center><input type="submit" class="btn btn-primary" value="valider"></center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%    } catch (Exception ex) {
        ex.printStackTrace();
    }

%>
<script type="text/javascript">
    $(document).ready(function () {

        (function ($) {

            $('#filter').keyup(function () {

                var rex = new RegExp($(this).val(), 'i');
                $('.searchable tr').hide();
                $('.searchable tr').filter(function () {
                    return rex.test($(this).text());
                }).show();

            })

        }(jQuery));

    });
</script>
