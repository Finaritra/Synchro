<%-- 
    Document   : inventaire-details
    Created on : 12 nov. 2015, 15:21:08
    Author     : Joe
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.stock.InventaireDetailsComplet"%>
<%@page import="affichage.PageRecherche"%>
<% 
    try{
        /*  private Date daty;
    private String depot, type, observation, etatfille,libelleingredient,unite,photo,categorieingredient,libelledepot;
    private double seuil,quantiteparpack,pu;
    private int compose,etat;*/
    InventaireDetailsComplet lv = new InventaireDetailsComplet();
    lv.setNomTable("INVENTAIREDETAILSLIBCOMPLET");

   // lv.setNomTable("ST_INVENTAIRE_LIBELLE");
    String listeCrt[] = {"id", "idmere", "daty", "categorieingredient","libelleingredient", "libelledepot"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id","idmere", "libelleingredient","quantiter","qtetheorique", "libelledepot", "categorieingredient", "daty","etat"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stock/inventaire/inventaire-details-liste.jsp");
    
    affichage.Champ[] listePoint = new affichage.Champ[1];
    TypeObjet liste1 = new TypeObjet();
    liste1.setNomTable("point");
    listePoint[0] = new Liste("libelledepot", liste1, "val", "val");
    pr.getFormu().changerEnChamp(listePoint);
    
    pr.getFormu().getChamp("libelledepot").setDefaut(AlloSakafoService.getRestau(null).getVal());
    pr.getFormu().getChamp("libelledepot").setLibelle("depot");
    
    String[] colSomme = null;
    pr.setNpp(200);
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste inventaire details</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/inventaire/inventaire-details-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  
            out.println(pr.getTableauRecap().getHtml());
        %>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "idmere","ingredient","quantiter","qte theorique", "depot", "categorie ingredient","daty", "etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
    <%
    }catch(Exception ex){
        ex.printStackTrace();
    }
    %>
        