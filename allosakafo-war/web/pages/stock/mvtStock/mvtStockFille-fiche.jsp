<%-- 
    Document   : mvtStockFille-fiche.jsp
    Created on : 11 nov. 2015, 11:09:25
    Author     : Joe
--%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%!
    StMvtStockDetails mvt;
    UserEJB u;
%>
<%
    u= (UserEJB) session.getAttribute("u");
    mvt = new StMvtStockDetails();
    mvt.setNomTable("ST_MVT_STOCK_DETAILS");
    //String[] libelleAffiche = {"Id", "Date", "Designation", "Type Mouvement", "Observation", "Magasin", "Class&eacute;e", "Projet", "Imp Analytique", "Num&eacute;ro BS", "Type Besoin", "Num&eacute;ro BR", "Exercice", "Etat", "Intra", "Fournisseur", "TVA"};
    PageConsulte pc = new PageConsulte(mvt, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    //pc.setLibAffichage(libelleAffiche);
    pc.setTitre("Details Mouvement de Stock");
    
    
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?butstock/mvtStock/mvtStock-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%=pc.getBasPage()%>
    </div>