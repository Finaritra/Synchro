<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.st.StMvtStockEtat"%>
<%@page import="affichage.PageRecherche"%>

<%
    try {
        StMvtStockEtat lv = new StMvtStockEtat();

        lv.setNomTable("ST_MVT_STOCK_LIBELLE_CRITIQUE");
        String listeCrt[] = {"article", "magasin", "reste"};
        String listeInt[] = null;
        String libEntete[] = {"article", "magasin", "reste"};

        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 3);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        pr.setApres("stock/mvtStock/mvtStock-critique-liste.jsp");
        String[] colSomme = null;

        pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Rapport d'alertes de stock</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/mvtStock-critique-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%
            out.println(pr.getTableauRecap().getHtml());
        %>
        </br>
        <%
            String libEnteteAffiche[] = {"Article", "Magasin", "Reste"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
			
			
			%>
			<div class="pull-right"><a href="<%=pr.getLien()%>?but=stock/proposition/propositionDemande-saisie.jsp" class="btn btn-primary">Proposition de commande</a></div>
			<%
            out.println(pr.getBasPage());

        %>
    </section>
</div>
<%    } catch (Exception ex) {
        ex.printStackTrace();
    }

%>