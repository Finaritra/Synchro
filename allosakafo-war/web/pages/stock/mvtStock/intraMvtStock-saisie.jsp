<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.st.StIntraMvtStock"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    UserEJB u = (user.UserEJB) session.getValue("u");

    StIntraMvtStock pj = new StIntraMvtStock();
    pj.setNomTable("St_Intra_Mvt_Stock");
    PageInsert pi = new PageInsert(pj, request, u);

    pi.setLien((String) session.getValue("lien"));

    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("magasinDep").setLibelle("Magasin Depart");
    pi.getFormu().getChamp("magasinArr").setLibelle("Magasin Arrivee");
    pi.getFormu().getChamp("designation").setLibelle("Observation");
    pi.getFormu().getChamp("designation").setType("textarea");

    affichage.Champ[] liste = new affichage.Champ[2];
    StMagasin dep = new StMagasin();
    dep.setNomTable("St_Magasin");
    liste[0] = new Liste("magasinDep", dep, "code", "id");

    StMagasin arr = new StMagasin();
    arr.setNomTable("St_Magasin");
    liste[1] = new Liste("magasinArr", arr, "code", "id");

    pi.getFormu().changerEnChamp(liste);
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Mouvement Inter Magasin</h1>
    <form action="<%=pi.getLien()%>?but=apresMvtIntra.jsp" method="post" name="AtDsp" id="AtDsp" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/mvtStock/apresMvtIntra-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StIntraMvtStock">
    </form>
</div>


