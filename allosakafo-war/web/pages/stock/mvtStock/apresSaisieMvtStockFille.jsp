<%-- 
    Document   : apresSaisieMvtStockFille
    Created on : 20 juil. 2016, 10:06:35
    Author     : Ignafah
--%>

<%@page import="mg.cnaps.formation.FormAbsence"%>
<%@page import="mg.cnaps.st.StMvtStockFille"%>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="bean.*" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="affichage.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
        String classe;
        String nomtable;
        String idstock;
        String id;
        String action;
        String chapitre;
        String typeBoutton;
        
       
    %>
    <%
        try {
            bute = request.getParameter("bute");
            classe = request.getParameter("classe");
            nomtable = request.getParameter("nomtable");
            idstock = request.getParameter("idstock");
            action = request.getParameter("action");
            chapitre = request.getParameter("chapitre");
            typeBoutton = request.getParameter("type");
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            
            Object temp = null;
            String[] rajoutLien = null;
            
            ClassMAPTable t = null;
           
            if (bute == null || bute.compareToIgnoreCase("") == 0) {
                bute = "pub/Pub.jsp";
            }

            if (classe == null || classe.compareToIgnoreCase("") == 0) {
                classe = "pub.Montant";
            }

            if (typeBoutton == null || typeBoutton.compareToIgnoreCase("") == 0) {
                typeBoutton = "3"; //par defaut modifier
            }            


            int type = Utilitaire.stringToInt(typeBoutton);
            if (acte.compareToIgnoreCase("insert") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
                ClassMAPTable o = (ClassMAPTable) u.createObject(f);
                temp = (Object) o;
                if (o != null) {
                    id = o.getTuppleID();
                }                
            } 
            if (acte.compareToIgnoreCase("annuler") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
                u.annulerObject(t);
            }
        %>
            <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>&id=<%=idstock%>&action=<%=action%>&chapitre=<%=chapitre%>");</script>
        <%  
        
    } catch (Exception e) {
        e.printStackTrace();
    %>

    <script language="JavaScript"> alert('<%=e.getMessage()%>');
        history.back();</script>
        <%
                return;
            }
        %>
</html>




