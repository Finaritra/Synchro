
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.achat.Besoin"%>
<%@page import="bean.TypeObjet"%>
<%@ page import="affichage.*" %>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>


<%
try{
    UserEJB u = null;
    u = (UserEJB) session.getValue("u");
    
    Besoin pj = new Besoin();
    PageInsertMultiple pi = new PageInsertMultiple(pj, request, 10, u);
    pi.setLien((String) session.getValue("lien"));
    
    affichage.Champ[] liste2 = new affichage.Champ[pi.getNombreLigne()];
    Liste[] liste3 = new Liste[pi.getNombreLigne()];

    String[] val_li = {"1", "2", "3", "4", "5", "6", "0"};
    String[] aff_li = { "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi","Dimanche"};

    TypeObjet o = new TypeObjet();
    o.setNomTable("point");
    for (int i = 0; i < pi.getNombreLigne(); i++) {
        pi.getFormufle().getChamp("idproduit_" + i).setPageAppelIndice("choix/ChoixProduitEtIngredients.jsp", i);
        pi.getFormufle().getChamp("remarque_" + i).setDefaut(AlloSakafoService.getNomRestau());
        pi.getFormufle().getChamp("idproduit_" + i).setAutre("readonly");
        liste2[i] = new Liste("remarque_" + i, o, "val", "id");
        liste3[i] = new Liste("jourssemaine_" + i);
        liste3[i].ajouterValeur(val_li, aff_li);
    }
    
    pi.getFormufle().getChamp("idproduit_0").setLibelle("Designation Plat");
    pi.getFormufle().getChamp("quantite_0").setLibelle("Quantite");
    pi.getFormufle().getChamp("jourssemaine_0").setLibelle("Jours");
    affichage.Champ.setVisible(pi.getFormufle().getChampFille("id"), false);
    pi.getFormufle().changerEnChamp(liste2);
    pi.getFormufle().changerEnChamp(liste3);
        pi.getFormufle().getChamp("jourssemaine_0").setLibelle("Jours");
    pi.getFormufle().getChamp("remarque_0").setLibelle("Point");
    
    pi.preparerDataFormuFille();
%>
<div class="content-wrapper">
    <h1 align="center"> saisie multiple</h1>
    <form action="<%=pi.getLien()%>?but=apresMultiple.jsp" method="post" name="inventairestockmere" id="inventairestockmere">
        <%
            pi.getFormufle().makeHtmlInsertTableauIndex();
            out.println(pi.getFormufle().getHtmlTableauInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insertFille">
        <input name="bute" type="hidden" id="bute" value="stock/besoin/saisie-besoin-multiple.jsp">
        <input name="classefille" type="hidden" id="classefille" value="mg.allosakafo.achat.Besoin">
        <input name="nomtable" type="hidden" id="nature" value="Besoin">
        <input name="nombreLigne" type="hidden" id="nombreLigne" value="10">
    </form>
</div>
<%}catch(Exception ex){
    ex.printStackTrace();
}%>
