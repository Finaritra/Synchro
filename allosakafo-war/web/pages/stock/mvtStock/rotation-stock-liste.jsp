<%-- 
    Document   : rotation-stock-liste.jsp
    Created on : 22 mars 2016, 10:27:22
    Author     : Joe
--%>

<%@page import="mg.cnaps.st.StRotationStock"%>
<%@page import="mg.cnaps.st.StockService"%>
<%@page import="bean.TypeObjet"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageRecherche"%>
<%
    StRotationStock lv = new StRotationStock();

    lv.setNomTable("st_rotation_stock");

    String listeCrt[] = {"daty", "code", "libelle"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"daty", "code", "duree", "libelle"};
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date Max");

    pr.setApres("stock/mvtStock/rotation-stock-liste.jsp");
    String[] colSomme = null;
    pr.setOrdre(" order by libelle, daty  asc");

    pr.creerObjetPage(libEntete, colSomme);
    StRotationStock[] liste = (StRotationStock[]) pr.getRs().getResultat();
    StockService.calculerDureeRotation(liste);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>liste
<div class="content-wrapper">
    <section class="content-header">
        <h1 id="titre-export">Rotation de stock</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/rotation-stock-liste.jsp" method="post" name="incident" id="incident">
            <%
                //out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="box box-primary box-solid collapsed">
                <div class="box-header with-border">
                    <h3 class="box-title" color="#edb031">
                        <span color="#edb031">Recherche avancée</span></h3>
                    <div class="box-tools pull-right">
                        <button data-original-title="Collapse" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""><i class="fa fa-plus"></i></button> </div></div>
                <div class="box-body" id="pagerecherche">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="Date" min="">Date min</label>
                            <input name="daty1" type="textbox" class="form-control" id="daty1" onkeydown="return searchKeyPress(event)"></div>
                        <div class="col-md-4">
                            <label for="Date" max="">Date Max</label>
                            <input name="daty2" type="textbox" class="form-control" id="daty2"  onkeydown="return searchKeyPress(event)"></div>
                        <div class="col-md-4">
                            <label for="code">code</label>
                            <input name="code" type="textbox" class="form-control" id="code" value="" onkeydown="return searchKeyPress(event)"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="libelle">libelle</label>
                            <input name="libelle" type="textbox" class="form-control" id="libelle" value="" onkeydown="return searchKeyPress(event)"></div>
                        <div class="col-md-4"></div></div>
                    <div class="form-group"><div class="col-md-4"></div></div></div>

                <div class="box-footer">
                    <div class="row col-md-12"></div><div class="row col-md-12"><div class="col-xs-6" align="right"><button type="submit" class="btn btn-primary" id="btnListe">Afficher</button></div><div class="col-xs-6" align="left"><button type="reset" class="btn btn-default">Réinitialiser</button></div></div></div></div>
        </form>
        <br>
        <div id="table-container">
            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                <thead>
                <th style="background-color:#bed1dd">Code article</th>
                <th style="background-color:#bed1dd">Date de mouvement</th>
                <th style="background-color:#bed1dd">Dur&eacute;e</th>
                <th style="background-color:#bed1dd">Libell&eacute;</th>
                </thead>
                <tbody>
                    <%
                        if (liste.length > 0) {
                            String stmp = liste[0].getCode();
                            double tmp0 = liste[0].getDuree();
                            double nb = 1;
                    %>
                    <tr>
                        <td><%=liste[0].getCode()%></td>
                        <td><%=utilitaire.Utilitaire.convertDatyFormtoRealDatyFormat(liste[0].getDaty() + "")%></td>
                        <td><%=liste[0].getDuree()%></td>
                        <td><%=liste[0].getLibelle()%></td>
                    </tr>
                    <%
                        for (int i = 1; i < liste.length; i++) {
                            if (stmp.compareToIgnoreCase(liste[i].getCode()) == 0 ) {
                                if(Utilitaire.compareDaty(liste[i].getDaty(), liste[i-1].getDaty()) != 0){
                                tmp0 += liste[i].getDuree();
                                nb++;
                    %>
                    <tr>
                        <td><%=liste[i].getCode()%></td>
                        <td><%=utilitaire.Utilitaire.convertDatyFormtoRealDatyFormat(liste[i].getDaty() + "")%></td>
                        <td><%=liste[i].getDuree()%></td>
                        <td><%=liste[i].getLibelle()%></td>
                    </tr>
                    <%
                            }
                    } else {
                    %>
                    <tr>
                        <td></td>
                        <td><b>Moyenne:</b></td>
                        <td><b><%out.println(Utilitaire.formaterAr(tmp0 / nb));%></b></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><%=liste[i].getCode()%></td>
                        <td><%=utilitaire.Utilitaire.convertDatyFormtoRealDatyFormat(liste[i].getDaty() + "")%></td>
                        <td><%=liste[i].getDuree()%></td>
                        <td><%=liste[i].getLibelle()%></td>
                    </tr>
                    <%
                                tmp0 = liste[i].getDuree();
                                nb = 1;
                                stmp = liste[i].getCode();
                            }
                        }
                    %>
                    
                    <tr>
                        <td></td>
                        <td><b>Moyenne:</b></td>
                        <td><b><%out.println(Utilitaire.formaterAr(tmp0 / nb));%></b></td>
                        <td></td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>		
            </table>
        </div>
        <div class="box box-primary box-solid">

            <div class="box-header with-border">
                <h3 class="box-title">Exporter</h3>
                <div class="box-tools pull-right">
                    <button   class="btn btn-box-tool" data-widget="collapse" >
                        <i class="fa fa-minus"></i>
                    </button> 
                </div>
            </div>

            <div id="export-body" class="box-body" style="display: block;">
                <div class="row" style="text-align: center">
                    <label>exporter excel :</label>
                    <button onclick="exporterCsv()">
                        <img src="../dist/img/file_xls.png" alt="csv-export">	
                    </button>
                </div>
            </div>
        </div>
        <form id="form-export" action="../download" method="post">
            <input id="excel-input"  type="hidden" name="table" value="">
            <input type="hidden" name="ext"  value="xls"  checked="checked">                    
            <input type="hidden" name="donnee" value="0" checked="checked">
            <input type="hidden" value="Exporter" class="btn btn-default">
        </form>
    </section>
</div>
<script>

    function chargerExport()
    {
        var titre = "<h1>" + $('#titre-export').html() + "</h1>";
        var excel = titre + $('#table-container').html();


        var excelInput = document.getElementById("excel-input");
        excelInput.value = excel;
        console.log("valeur de l'excel" + excelInput.value);
    }

    function exporterCsv()
    {
        chargerExport()
        var form = $("#form-export");
        form.submit();
    }

</script>