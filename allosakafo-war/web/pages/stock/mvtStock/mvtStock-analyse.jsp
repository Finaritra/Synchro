<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%@ page import="affichage.*" %>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

<%!
    StMvtStockWTypeM mvt;
%>
<%
    mvt = new StMvtStockWTypeM();
    mvt.setNomTable("ST_MVT_STOCK_LIBELLE_TYPE");
    String listeCrt[] = {"id", "daty", "designation", "typemvt", "observation", "magasin", "classee", "projet","typemagasin"};
    String listeInt[] = {"daty"};
    String[] pourcentage = {"nombrepargroupe"};
    String colDefaut[] = {"id", "daty", "designation", "typemvt", "observation", "magasin", "classee", "projet"};
    String somDefaut[] = {};
    PageRechercheGroupe pr = new PageRechercheGroupe(mvt, request, listeCrt, listeInt, 3, colDefaut, somDefaut, pourcentage, 8, 0);

    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stock/mvtStock/mvtStock-analyse.jsp");
    
    affichage.Champ[] liste = new affichage.Champ[3];
    
    TypeObjet tp = new TypeObjet();
    tp.setNomTable("ST_TYPE_MVT");
    Liste l = new Liste("typemvt", tp, "VAL", "VAL");
    l.setDefaultSelected("TYPEIMMO1");
    liste[0] = l;
 
    StMagasin m = new StMagasin();
    m.setNomTable("ST_MAGASIN_LIBELLE");
    liste[1]=new Liste("magasin",m,"DESCE","DESCE");
    
    TypeObjet tm = new TypeObjet();
    tm.setNomTable("ST_TYPE_MAGASIN");
    liste[2] = new Liste("typemagasin", tm, "VAL", "VAL");
    
    pr.getFormu().changerEnChamp(liste);
    
    pr.creerObjetPagePourc();
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Analyse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/mvtStock-analyse.jsp" method="post" name="analyse" id="analyse">
            <%out.println(pr.getFormu().getHtmlEnsemble());%>
        </form>
        <%
            String lienTableau[] = {pr.getLien() + "?but=stock/mvtStock/mvtStock-fiche.jsp&id=" + pr.getFormu().getListeCritereString()};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
                    out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>