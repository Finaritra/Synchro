<%@page import="java.lang.reflect.Field"%>
<%@page import="affichage.TableauRecherche"%>
<%@page import="utilitaire.UtilDB"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="bean.AdminGen"%>
<%@page import="user.UserEJB"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="mg.allosakafo.stock.EtatdeStockDate"%>
<%@page import="affichage.*"%>

<%  
    try {
        EtatdeStockDate lv = new EtatdeStockDate();
        lv.setNomTable("AS_ETATSTOCKVIDE"); 
        String listeCrt[] = {"daty", "ingredients","point","idCategorieIngredient"};
        String listeInt[] = {"daty"};
        String libEntete[] = {"ingredients","idCategorieIngredient","unite", "daty", "report", "entree", "sortie", "reste","reportSuiv","montantStock"} ;

        UserEJB u = (user.UserEJB) session.getValue("u");

        //String colDefaut[] = {"ingredients", "unite","point"}; 
        String somDefaut[] = null;
        //PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 5);
        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete);
        //PageRechercheGroupe pr = new PageRechercheGroupe(lv, request, listeCrt, listeInt, 3, colDefaut, somDefaut, null,7, 3);
        
        pr.setUtilisateur(u);
        pr.setLien((String) session.getValue("lien"));
        
        affichage.Champ[] listePoint = new affichage.Champ[1];
        TypeObjet liste1 = new TypeObjet();
        liste1.setNomTable("point");
        listePoint[0] = new Liste("point", liste1, "val", "id");
        pr.getFormu().changerEnChamp(listePoint);
        pr.getFormu().getChamp("point").setDefaut(session.getAttribute("restaurant").toString());
        
        pr.getFormu().getChamp("ingredients").setLibelleAffiche("Ingredients");
        pr.getFormu().getChamp("daty1").setLibelleAffiche("Date debut");
        pr.getFormu().getChamp("daty1").setDefaut(Utilitaire.dateDuJour());
        pr.getFormu().getChamp("daty2").setLibelleAffiche("Date fin");
        pr.getFormu().getChamp("daty2").setDefaut(Utilitaire.dateDuJour());
        pr.setApres("stock/mvtStock/etatstock-liste.jsp");
        String daty1,daty2;
        daty1=request.getParameter("daty1");
        daty2=request.getParameter("daty2");
        if(daty1==null||daty1.compareToIgnoreCase("")==0)daty1=Utilitaire.dateDuJour();
        if(daty2==null||daty2.compareToIgnoreCase("")==0)daty2=Utilitaire.dateDuJour();
        String colonne=null,ordre=null;
        if(request.getParameter("colonne")!=null && request.getParameter("ordre")!=null){
            colonne=request.getParameter("colonne");
            ordre=request.getParameter("ordre");
        }
        EtatdeStockDate[] listeachat = EtatdeStockDate.caculEtatStock(daty2, request.getParameter("ingredients"),request.getParameter("point"),request.getParameter("idCategorieIngredient"),null, null,colonne,ordre,daty1);
        //pr.creerObjetPage();
        
        pr.creerObjetPage(libEntete, somDefaut);
        
        //pr.setBaseTableau(stock);
        String[] enteteAuto = {"ingredients","idcategorieingredient","unite", "daty", "report", "entree", "sortie", "reste","reportSuiv","montantStock"} ;
        //pr.getRs().setResultat(stock);
        
        pr.setTableau(new TableauRecherche(listeachat, enteteAuto));

%>
        <script>
            console.log(<%= pr.getTableau().getHtml() %>);
        </script>
        <div class="content-wrapper">
            <section class="content-header">
                <h1>Etat de stock DECOMPOSE AVEC FABRICATION ET RESTE</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getLien()%>?but=stock/mvtStock/etatstock-liste.jsp" method="post" name="incident" id="incident">
                    <%
                        out.println(pr.getFormu().getHtmlEnsemble());
                    %>

                </form>
                    TOTAL MONTANT STOCK : <%=utilitaire.Utilitaire.formaterAr(AdminGen.calculSommeDouble(listeachat, "montantStock"))%> Ar
                 <div class="row">
            <div class="row col-md-12">
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" align="center">LISTE</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                         
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                                <thead> 
                                    <tr class="head">
                                        <th align="left">Ingredients</th>
                                        <th align="left">Categorie Ingredient</th>  
                                        <th align="left">Unit�</th>  
                                        <th align="left">Date</th>
                                        <th align="left">Report</th>  
                                        <th align="left">Entr�e</th>
                                        <th align="left">Sortie</th>
                                        <th align="left">Reste</th> 
                                        <th align="left">Report Suivant</th> 
                                        <th align="left">Montant Stock</th> 
                                         
                                        <th align="left"></th>  
                                    </tr>
                                </thead>
                                    <tbody class="searchable">
                                        <%
                                            for (int i = 0; i < listeachat.length; i++) {
                                        %>
                                        <tr>
                                            <td align="left"><%=listeachat[i].getIngredients()%></td>
                                            <td align="left"><%=listeachat[i].getIdCategorieIngredient()%></td>
                                            <td align="left"><%=listeachat[i].getUnite()%></td>
                                            <td align="left"><%=listeachat[i].getDaty()%></td>
                                            <td align="left"><a href="<%=pr.getLien() %>?but=stock/besoin/reste-ingredient-liste.jsp&idingredients=<%=listeachat[i].getIdingredient()%>&daty1=<%=utilitaire.Utilitaire.datetostring( listeachat[i].getDaty() )%>"><%=listeachat[i].getReport()%></a></td>
                                            <td align="left"><%=listeachat[i].getEntree()%></td>
                                            <td align="left"><%=listeachat[i].getSortie()%></td>
                                            <td align="left"><%=listeachat[i].getReste()%></td>
                                            <td align="left"><%=listeachat[i].getReportSuiv()%></td>
                                            <td align="left"><%=listeachat[i].getMontantStock()%></td>                            
                                            

                                        </tr>
                                        <% } %>

                                    </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </section>
        </div>
<%  
    } catch(Exception ex) {
        ex.printStackTrace();
    }
%>
