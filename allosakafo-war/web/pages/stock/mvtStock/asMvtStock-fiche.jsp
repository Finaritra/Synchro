<%-- 
    Document   : bondelivraison-fiche
    Created on : 10 mars 2016, 09:20:48
    Author     : Joe
--%>
<%@page import="java.sql.Date"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.allosakafo.stock.*" %>
<%
    try {
        MvtStock inventaire = new MvtStock();
        inventaire.setNomTable("AS_MVT_STOCKLIBPOINT");
        PageConsulte pc = new PageConsulte(inventaire, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
        pc.getChampByName("numbc").setLibelle("Reference");

        
        pc.getChampByName("typebesoin").setVisible(false);
        pc.getChampByName("beneficiaire").setVisible(false);
        

        pc.setTitre("Fiche mouvement de stock");

        MvtStock livraison = (MvtStock) pc.getBase();

        MvtStockFille fille = new MvtStockFille();
        fille.setNomTable("as_mvtstock_fille_libl");
        String idbl = " AND idmvtstock = '" + request.getParameter("id").trim() + "'";

        MvtStockFille[] filles = (MvtStockFille[]) CGenUtil.rechercher(fille, null, null, idbl);
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/mvtStock/asMvtStock-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=annuler&bute=stock/mvtStock/asMvtStock-fiche.jsp&classe=mg.allosakafo.stock.MvtStock" style="margin-right:10px">Annuler</a>
                            <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/mvtStock/asMvtStock-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right:10px">Modifier</a>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=valider&bute=stock/mvtStock/asMvtStock-fiche.jsp&classe=mg.allosakafo.stock.MvtStock" style="margin-right: 10px">Viser</a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title with-border">
                    <h1 class="box-title">D&eacute;tails</h1>
                </div>
                <div class="box-body no padding">
                    <table class="table table-bordered">
                        <tr>
                            <th align="center">ID</th>
                            <th align="center">Produit</th>
                            <th align="center">Entr&eacute;e</th>
                            <th align="center">Sortie</th>
                            <th align="center">Unite</th>
                        </tr>

                        <%
                            for (int i = 0; i < filles.length; i++) {%>
                        <tr>
                            <td align="left"><%=filles[i].getId()%></td>
                            <td align="left"><%=filles[i].getIngredients()%></td>
                            <td align="right"><%=Utilitaire.formaterAr(filles[i].getEntree())%></td>
                            <td align="right"><%=Utilitaire.formaterAr(filles[i].getSortie())%></td>
                            <td align="right"><%=filles[i].getIdunite()%></td>
                        </tr>
                        <% }%> 
                    </table>
                </div>


            </div> 
        </div> 

    </div> 

</div>
<% } catch (Exception ex) {
        ex.printStackTrace();
        throw new Exception(ex.getMessage());
    }%>