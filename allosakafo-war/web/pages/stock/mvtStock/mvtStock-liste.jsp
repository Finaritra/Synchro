<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.st.StMvtStockWTypeM"%>
<%@page import="mg.cnaps.st.StMvtStock"%>
<%@page import="affichage.PageRecherche"%>

<% 
    try{
    StMvtStockWTypeM lv = new StMvtStockWTypeM();

    lv.setNomTable("ST_MVT_STOCK_LIBELLE_TYPE");
    String listeCrt[] = {"id", "daty", "designation", "typemvt", "observation", "magasin", "classee", "projet","typemagasin"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "designation", "typemvt", "observation", "magasin", "classee", "projet"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    affichage.Champ[] liste = new affichage.Champ[3];
    
    TypeObjet tp = new TypeObjet();
    tp.setNomTable("ST_TYPE_MVT");
    Liste l = new Liste("typemvt", tp, "VAL", "VAL");
    l.setDefaultSelected("TYPEIMMO1");
    liste[0] = l;
 
    StMagasin m = new StMagasin();
    m.setNomTable("ST_MAGASIN_LIBELLE");
    liste[1]=new Liste("magasin",m,"DESCE","DESCE");
    
    TypeObjet tm = new TypeObjet();
    tm.setNomTable("ST_TYPE_MAGASIN");
    liste[2] = new Liste("typemagasin", tm, "VAL", "VAL");
    
    
    pr.getFormu().changerEnChamp(liste);
    
    //String listeCrt[] = {"id", "daty", "designation", "typemvt", "observation", "magasin", "classee", "projet","typemagasin"};
    pr.getFormu().getChamp("daty1").setLibelle("Date (min)");
    pr.getFormu().getChamp("daty2").setLibelle("Date (max)");
    pr.getFormu().getChamp("designation").setLibelle("D&eacute;signation");
    pr.getFormu().getChamp("typemvt").setLibelle("Type du mouvement");
    pr.getFormu().getChamp("classee").setLibelle("Classe");
    pr.getFormu().getChamp("typemagasin").setLibelle("Type du magasin");
    
    pr.setApres("stock/mvtStock/mvtStock-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Mouvement Stock</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/mvtStock-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/mvtStock/mvtStock-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        </br>
        <%
            String libEnteteAffiche[] = {"id", "Date", "D&eacute;signation", "Type du mouvement", "Observation", " Magasin", "Classe", " Projet"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
<%
    }catch(Exception ex){
        ex.printStackTrace();
    }

        %>