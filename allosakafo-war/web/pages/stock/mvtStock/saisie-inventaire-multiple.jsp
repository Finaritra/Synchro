<%@page import="service.AlloSakafoService"%>
<%@page import="user.*" %>
<%@page import="bean.*" %>
<%@page import="utilitaire.*" %>
<%@page import="affichage.*" %>
<%@page import="utilitaire.*" %>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="mg.allosakafo.produits.Produits"%>
<%@page import="mg.allosakafo.produits.Ingredients"%>



<%
    String idcategorie = request.getParameter("idcategorieingredient");
    String filtreDepot= session.getAttribute("restaurant").toString();
    
    String filtreDaty=Utilitaire.dateDuJour();
    if(request.getParameter("daty")!=null&&request.getParameter("daty").compareToIgnoreCase("")!=0)filtreDaty=request.getParameter("daty");
    if(request.getParameter("depot")!=null&&request.getParameter("depot").compareToIgnoreCase("")!=0)filtreDepot=request.getParameter("depot");

    UserEJB u = (UserEJB) session.getAttribute("u");

    InventaireMere cmd = new InventaireMere();
    PageInsert pi = new PageInsert(cmd, request, u);
    
    affichage.Champ[] liste = new affichage.Champ[3];
    TypeObjet o = new TypeObjet();
    o.setNomTable("typeinventaire");
    liste[0] = new Liste("type", o, "VAL", "id");
    
    TypeObjet o2 = new TypeObjet();
    o2.setNomTable("categorieingredient");
    liste[1] = new Liste("idcategorieingredient", o2, "VAL", "id");
    
     TypeObjet o3 = new TypeObjet();
    o3.setNomTable("point");
    liste[2] = new Liste("depot", o3, "VAL", "id");
    
       pi.getFormu().changerEnChamp(liste);
    // pi.getFormu().getChamp("depot").setDefaut((String)session.getAttribute("restaurant"));
    pi.getFormu().getChamp("depot").setLibelle("Lieu du depot");
    pi.getFormu().getChamp("depot").setDefaut(filtreDepot);
    //pi.getFormu().getChamp("depot").setAutre("readonly");
    
       
    pi.getFormu().getChamp("idcategorieingredient").setLibelle("Categorie ingredient");
    pi.getFormu().getChamp("idcategorieingredient").setDefaut(idcategorie);
    pi.getFormu().getChamp("observation").setDefaut(request.getParameter("observation"));
    pi.getFormu().getChamp("type").setDefaut(request.getParameter("type"));
    
    pi.getFormu().getChamp("etat").setVisible(false);
    
 
    pi.getFormu().getChamp("daty").setDefaut(filtreDaty);
    
    pi.setLien((String) session.getValue("lien"));
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 class="box-title">Enregistrer Inventaire</h1>
    <form action="<%=pi.getLien()%>?but=stock/mvtStock/apresMvt.jsp" method="post" name="inventaire" id="inventaire" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertCommande();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <button type="button" onclick="afficher()" class="btn btn-primary col-md-offset-6">Afficher</button>
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <div class="box">
                        <div class="box-title with-border">
                            <h1 class="box-title">D&eacute;tails</h1>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                Libell� : <input name="lib" id="lib" type="text" value="<%= Utilitaire.champNull(request.getParameter("lib"))%>">
                            </div>
                        </div>
                        <div class="box-body table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th style="background-color:#bed1dd">ID</th>
                                        <th style="background-color:#bed1dd">Libelle</th>
                                        <th style="background-color:#bed1dd">Unite</th>
<!--                                        <th style="background-color:#bed1dd">Quantite par pack</th>
                                        <th style="background-color:#bed1dd">Quantite th&eacute;orique</th>-->
                                        <th style="background-color:#bed1dd">Quantite r&eacute;el</th>
					<!--<th style="background-color:#bed1dd">Total</th>-->
                                    </tr></thead>
                                <tbody>
                                    <%
                                        if(idcategorie != null) {
                                            if(!idcategorie.isEmpty()) {
                                        Ingredients p = new Ingredients();
                                        p.setCategorieingredient(idcategorie);
                                        String suite = "";
                                        String lib = request.getParameter("lib");
                                            if ( lib != null && !lib.equals("")) {
                                                suite += " and lower(libelle) like '%' || lower('"+lib+"') || '%'";
                                            }
                                            suite += " and id not in (select inv.explication from INVENTAIREDETAILSLIB inv where inv.daty='"+filtreDaty+"' and inv.etat>=11 and inv.depot='"+filtreDepot+"') order by libelle asc";
					p.setNomTable("AS_INGREDIENTS_LIB_SAISIE");
                                        Ingredients[] listef = (Ingredients[]) CGenUtil.rechercher(p, null, null, suite);
                                         for (int i = 0; i < listef.length; i++) {
                                    %>
                                <input type="hidden" class="form form-control" name="nb" id="nb" value="<%=listef.length%>">
                                <tr>
                                    <td><input type="hidden" class="form form-control" name="ids" value="<%=listef[i].getId()%>"><%=listef[i].getId()%></td>
                                    <td><%=listef[i].getLibelle()%></td>
                                    <td><%=listef[i].getUnite()%></td>
<!--                                    <td><input type="text" class="form form-control" name="quantiteparpack"  id="quantiteparpack<%=i%>" value="<%=listef[i].getQuantiteparpack()%>" onblur="calculerTotal(<%=i%>)"></td>
                                    <td><%=listef[i].getSeuil()/listef[i].getQuantiteparpack()%></td>-->
                                    <td><input type="text" class="form form-control" name="quantite" id="quantite<%=i%>" onblur="calculerTotal(<%=i%>)"></td>
                                    <input type="hidden" class="form form-control" name="qtetheorique" id="qtetheorique" value="0" >
                                    <!--<td><input type="text" class="form form-control" name="total" id="total<%=i%>" value="<%=listef[i].getSeuil()%>" readonly="true"></td>-->
                                </tr>
                                <%
                                    }}}
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
                                
        <input name="acte" type="hidden" id="nature" value="insertInventaire">
        <input name="bute" type="hidden" id="bute" value="stock/mvtStock/saisie-inventaire-multiple.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.stock.InventaireMere">
        <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;">Enregistrer</button>
        <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;">R&eacute;initialiser</button>
    </form>
    <script>
        function afficher() {
            window.location.href = "/phobo/pages/module.jsp?but=stock/mvtStock/saisie-inventaire-multiple.jsp&depot="+document.getElementById("depot").value+"&idcategorieingredient="+document.getElementById("idcategorieingredient").value+"&type="+document.getElementById("type").value+"&observation="+document.getElementById("observation").value+"&daty="+document.getElementById("daty").value+"&lib="+document.getElementById("lib").value;
        }
    </script>
</div>