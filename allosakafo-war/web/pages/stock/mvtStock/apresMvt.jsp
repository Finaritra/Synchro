<%@page import="mg.allosakafo.achat.Besoin"%>
<%@page import = "mg.allosakafo.stock.*"%>
<%@page import = "user.*" %>
<%@page import = "utilitaire.*" %>
<%@page import = "bean.*" %>
<%@page import = "java.sql.SQLException" %>
<%@page import = "affichage.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
        String nomtable;
        String typeBoutton;
    %>
    <%
        try {
            nomtable = request.getParameter("nomtable");
            typeBoutton = request.getParameter("type");
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            bute = request.getParameter("bute");
            Object temp = null;
            String[] rajoutLien = null;
            String classe = request.getParameter("classe");
            ClassMAPTable t = null;
            String tempRajout = request.getParameter("rajoutLien");
            String val = "";

            String rajoutLie = "";
            
            if (tempRajout != null && tempRajout.compareToIgnoreCase("") != 0) {
                rajoutLien = utilitaire.Utilitaire.split(tempRajout, "-");
            }
            if(acte.compareToIgnoreCase("insertBesoin")==0){
                 t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                 String[] valeurs = request.getParameterValues("valeur");
                Besoin besoin = (Besoin) t;
                String[] id = request.getParameterValues("ids");
                besoin.insertBesoinMultiple(valeurs,id,u.getUser().getTuppleID(),null);
                //u.saisieInventaireCategorie(besoin, id, quantite, null);
            }
            if (acte.compareToIgnoreCase("insertInventaire") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
//                MvtStock mvt = (MvtStock) f;
                
                String[] id = request.getParameterValues("ids");
//                String[] quantiteparpack = request.getParameterValues("quantiteparpack");
                String[] quantite = request.getParameterValues("quantite");
                String[] qtetheorique = request.getParameterValues("qtetheorique");
//                val = u.saveInventaire(mvt, id, quantiteparpack, quantite);
                InventaireMere inv = (InventaireMere) t;
                inv.setIdcategorieingredient(request.getParameter("idcategorieingredient"));
                u.saisieInventaireCategorieTheorique(inv, id, quantite,qtetheorique, null);
                
            }
	    if (acte.compareToIgnoreCase("insertMvt") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                MvtStock mvt = (MvtStock) f;
		String typemvt = request.getParameter("type");
                String[] id = request.getParameterValues("ids");
                String[] quantiteparpack = request.getParameterValues("quantiteparpack");
                String[] quantite = request.getParameterValues("quantite");
                val = u.saveInventaire(mvt, id,quantite, typemvt);
            }
             if (acte.compareToIgnoreCase("transfertMvt") == 0) {
                 System.out.println("");
                  t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                TransfertStock mvt = (TransfertStock) f;
                String[] id = request.getParameterValues("ids");
                String[] quantite = request.getParameterValues("quantite");
                val=u.transfertInventaire(mvt, id,quantite,null);
             }
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>&id=<%=val%>");</script>
    <%

    } catch (Exception e) {
        e.printStackTrace();
    %>

    <script language="JavaScript"> alert('<%=e.getMessage()%>');
        history.back();</script>
        <%
                return;
            }
        %>
</html>



