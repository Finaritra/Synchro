<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%
    StMvtStock mvt;
    UserEJB u;
    u= (UserEJB) session.getAttribute("u");
    mvt = new StMvtStock();
    mvt.setNomTable("ST_MVT_STOCK_LIBELLE");
    String[] libelleAffiche = {"Id", "Date", "Designation", "Type Mouvement", "Observation", "Magasin", "Class&eacute;e", "Projet","Direction", "Num&eacute;ro BS", "Type Besoin", "Imp Analytique","Num&eacute;ro BR", "Exercice", "Etat"};
    PageConsulte pc = new PageConsulte(mvt, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setLibAffichage(libelleAffiche);
    pc.setTitre("Fiche Mouvement de Stock");

    StMvtStock temp = (StMvtStock)pc.getBase();
    StMvtStockFille mvtstockfille = new StMvtStockFille();
    mvtstockfille.setNomTable("ST_MVT_STOCK_FILLE_LIBELLE");
    String mvtstockId = "AND IDMVTSTOCK = '" + request.getParameter("id") + "'";
    StMvtStockFille[] mvtstockfilles = (StMvtStockFille[]) u.getData(mvtstockfille, null, null, null, mvtstockId);
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?butstock/mvtStock/mvtStock-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">

                            <% if(temp.getTypemvt().compareToIgnoreCase("entr�e") == 0){%><a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/mvtStock/mvtArticles-tranfert-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Transf&eacute;rer</a><%}%>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/mvtStock/mvtStock-liste.jsp&classe=mg.cnaps.st.StMvtStock" style="margin-right: 10px">Annuler</a>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=valider&bute=stock/mvtStock/mvtStock-fiche.jsp&classe=mg.cnaps.st.StMvtStock" style="margin-right: 10px">Viser</a>
                            <% if(temp.getTypemvt().compareToIgnoreCase("sortie") == 0){%><a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/mvtStock/mvtArticles-reintegration-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">R&eacute;int&eacute;grer</a><%}%>
                            <a href="${pageContext.request.contextPath}/EtatStockServlet?action=mouvementStock&id=<%=request.getParameter("id")%>" class="btn btn-default">Exporter</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%=pc.getBasPage()%>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">D&eacute;tails</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Id</th>
                                <th>Article</th>
                                <th>Entr&eacute;e</th>
                                <th>Sortie</th>
                                <th>Prix Unitaire</th>
                                <th>Taxe</th>
                                <th>Etat</th>
                            </tr>
                            <%
                            for(int i=0;i<mvtstockfilles.length;i++)
                            {
                            %>
                            <tr>
                                <td><%=mvtstockfilles[i].getId()%></td>
                                <td><%=mvtstockfilles[i].getArticle()%></td>
                                <td><%=(int)mvtstockfilles[i].getEntree()%></td>
                                <td><%=(int)mvtstockfilles[i].getSortie()%></td>
                                <td><%=Utilitaire.formaterAr(mvtstockfilles[i].getPu())%></td>
                                <td><%=(int)mvtstockfilles[i].getTaxe()%></td>
                                <td><%=(int) mvtstockfilles[i].getEtat()%></td>
                            </tr>
                            <%
                            }
                            %>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
