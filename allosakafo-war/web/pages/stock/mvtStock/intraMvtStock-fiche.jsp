<%@page import="mg.cnaps.st.StIntraMvtStock"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%!
StIntraMvtStock p;
%>
<%
p=new StIntraMvtStock();
p.setNomTable("St_Intra_Mvt_Stock");
String [] libellePersonneFiche={"Id","Date","Magasin depart","Magasin arrivee", "Designation"};
PageConsulte pc=new PageConsulte(p,request,(user.UserEJB)session.getValue("u"));//ou avec argument liste Libelle si besoin
pc.setLibAffichage(libellePersonneFiche);
%>
<div class="content-wrapper">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
                    <div class="box-fiche">
			<div class="box">
				<div class="box-title with-border">
					<h1 class="box-title"><a href="<%=(String)session.getValue("lien")%>?but=stock/mvtStock/intraMvtStock-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a> Detail mouvement inter magasin</h1>
				</div>
				<div class="box-body">
					<%
					out.println(pc.getHtml());
					%>
					<br/>
					<div class="pull-right">
						<p>
							<a href="<%=(String)session.getValue("lien")+"?but=stock/mvtStock/intraMvtStock-modif.jsp&id="+request.getParameter("id")%>"><button class="btn btn-warning">Modifier</button></a>
							<a href="<%=(String)session.getValue("lien")+"?but=apresTarif.jsp&id="+request.getParameter("id")%>&acte=delete&bute=stock/mvtStock/intraMvtStock-liste.jsp.jsp&classe=mg.cnaps.st.StIntraMvtStock"><button class="btn btn-danger">Supprimer</button></a>
						</p>
					</div>
				</div>
			</div>
                    </div>
		</div>
	</div>
</div>
