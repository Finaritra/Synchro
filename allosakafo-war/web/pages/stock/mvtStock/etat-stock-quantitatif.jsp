<%-- 
    Document   : etat-stock-quantitatif
    Created on : 23 f�vr. 2016, 11:15:55
    Author     : Joe
--%>
<%@page import="mg.cnaps.st.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    String lien = (String) session.getValue("lien");

    StMagasin[] mags = (StMagasin[]) u.getData(new StMagasin(), null, null, null, "");
    String magasin="";
    String daty="";
%>
<div class="content-wrapper">
    <section class="content-header">

        <h1 align="center">Rechercher</h1>
    </section>
    <section class="content">

        <div class="row">
            <form action="<%=lien%>?but=stock/mvtStock/etat-stock-quantitatif.jsp" method="post" name="analyse" id="analyse" data-parsley-validate="" novalidate="">
                <div class="row">
                    <div class="col-md-6">
                        <div class="box-insert">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    <label tabindex="1" for="Date">Date</label>
                                                </th>
                                                <td>
                                                    <input id="datefind" class="form-control datepicker" value="<%=Utilitaire.dateDuJour()%>" type="textbox" tabindex="1" name="datefind" data-parsley-id="1">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <label tabindex="2" for="magasin">Magasin</label>
                                                </th>
                                                <td>
                                                    <select id="magasin" class="form-control" tabindex="2" name="magasin" data-parsley-id="2">
                                                        <option value="%">Tous</option>
                                                        <%
                                                            if (mags != null && mags.length > 0) {
                                                                for (int i = 0; i < mags.length; i++) {%>
                                                        <option value="<%=mags[i].getDesce()%>"><%=mags[i].getDesce()%></option>
                                                        <% }
                                                            } %>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="box-footer">
                                    <div class="col-xs-12">
                                        <button class="btn btn-success pull-right" tabindex="91" style="margin-right: 25px;" name="Submit2" type="submit">Valider</button>
                                        <button class="btn btn-default pull-right" tabindex="92" style="margin-right: 15px;" name="Submit2" type="reset">R�initialiser</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input name="form" type="hidden" id="nature" value="form">
                <input name="bute" type="hidden" id="bute" value="stock/mvtStock/etat-stock-quantitatif.jsp">
            </form>
        </div>
        <%
            try {

                String date = Utilitaire.dateDuJour();
                magasin = "%";

                if (request.getParameter("form") != null && request.getParameter("form").compareToIgnoreCase("") != 0) {
                    date = request.getParameter("datefind");
                    magasin = request.getParameter("magasin");
                }

                //pr.creerObjetPage(libEntete, null);
                StMvtStockEtatDate[] listResultat = StockService.genererEtatStockDate(date, magasin);

                String j = date.substring(0, 2);
                int mon = Utilitaire.stringToInt(date.substring(3, 5));

                String monLettre = Utilitaire.nbToMois(mon);
                String an = date.substring(6, 10);
        %>

        <%
            if(request.getParameter("datefind")!=null && request.getParameter("magasin").compareToIgnoreCase("%")!=0){
                magasin = "&magasin="+request.getParameter("magasin");
                daty = "&daty="+request.getParameter("datefind");
            }
            else{
                request.setAttribute("magasin", "%");
                magasin = "";
                daty = "";
            }
        %>
        <div class="row">
            <div class="row col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h1>Etat de stock du <% out.println(j + " " + monLettre + " " + an);%></h1>
                        <%
                            if(daty!="" && request.getParameter("magasin").compareToIgnoreCase("%")!=0){ 
                        %>
                        <a href="<%=(String) session.getValue("lien")%>/../../EtatStockServlet?action=ficheStock<%=magasin%><%=daty%>" id="export" class="btn btn-primary pull-right">Imprimer fiche de stock du magasin : <%=request.getParameter("magasin")%></a>
                        <%  } 
                        %>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                                <thead>
                                    <tr class="head">
                                        <th align="left">Magasin</th>
                                        <th align="left">Code</th>
                                        <th align="left">Article</th>
                                        <th align="right">Reste</th>
                                        <th align="left">Chapitre</th>
                                        <th align="left">-</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% if (listResultat != null && listResultat.length > 0) {
                                            for (int i = 0; i < listResultat.length; i++) {%>
                                    <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                        <td align="left"><%=listResultat[i].getMagasin()%></td>
                                        <td align="left"><%=listResultat[i].getCode()%></td>
                                        <td align="left">
                                            <a href="<%=lien%>?but=stock/mvtStock/mvtStock-details.jsp&article=<%=listResultat[i].getArticle()%>&magasin=<%=listResultat[i].getMagasin()%>" title="Voir details">
                                                <%=listResultat[i].getArticle()%>
                                            </a>
                                        </td>                                           
                                        <td align="right"><%=Utilitaire.formaterAr(listResultat[i].getReste())%></td>
                                        <td align="left"><%=listResultat[i].getChapitre()%></td>
                                        <td align="right"><a href="<%=(String) session.getValue("lien")%>/../../EtatStockServlet?action=ficheStock&article=<%=listResultat[i].getArticle()%><%=magasin%><%=daty%>" id="export" class="btn btn-primary">Imprimer</a></td>
                                    </tr>
                                    <% }
                                        }%>
                                </tbody>									

                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<% } catch (Exception ex) {
        ex.printStackTrace();
    }
%>