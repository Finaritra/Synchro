
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.cnaps.medecine.FicheStockCMS"%>
<%
String article=request.getParameter("article");
String magasin=request.getParameter("magasin");
FicheStockCMS fiche = new FicheStockCMS();
fiche.setArticle(article);
UserEJB u = (UserEJB) session.getAttribute("u");
String aWhere = " and article like '"+article+"'";
String listeCrt[] = {"daty"};
String listeInt[] = {"daty"};
String libEntete[] =  {"fournisseur", "destinataire", "unite", "article", "entree", "sortie", "solde", "pu"};
PageRecherche pr = new PageRecherche(fiche, request, listeCrt, listeInt, 2, libEntete, 2);
pr.setUtilisateur((user.UserEJB) session.getValue("u"));
pr.setLien((String) session.getValue("lien"));
pr.setAWhere(aWhere);
pr.setNpp(10000);
pr.getFormu().getChamp("daty1").setLibelle("Date min");
pr.getFormu().getChamp("daty2").setLibelle("Date max");
pr.setApres("stock/mvtStock/ficheStock-details.jsp&article="+article+"&magasin="+magasin);
String[] colSomme =null;
pr.creerObjetPage(libEntete, colSomme);
FicheStockCMS[] ficheStock = (FicheStockCMS[]) pr.getListe();
String unite = "";
if(ficheStock.length>0){
    unite = ficheStock[0].getUnite();
}
%>
<div class="content-wrapper">
    <section class="content-header">
        <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=cms/etat/medicament-etat.jsp"><i class="fa fa-arrow-circle-left"></i></a>Fiche de Stock</h1>
                    </div>
    </section>
 <section class="content">
     <p><u>NOM DU PRODUIT:</u> <b><%=article%></b></p>
 <p><u>UNITE DE STOCK :</u> <b><%=unite%></b></p>
        <div class="row">
             <div class="row col-md-12">
                  <div class="box box-primary">

       <div class="box-body table-responsive no-padding">
			<form action="<%=pr.getLien()+"?but=stock/mvtStock/ficheStock-details.jsp&article="+article+"&magasin="+magasin%>" method="post" name="incident" id="incident">
			    <%
				out.println(pr.getFormu().getHtmlEnsemble());
			    %>
			</form>
			<br>
			
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;" colspan="4">ENTREE</th><th style="text-align: center; vertical-align: middle;" colspan="4">SORTIE</th>
                                </tr>
                            <th style='background-color:#bed1dd'>Date</th>
                            <th style='background-color:#bed1dd'>Provenance</th>
                            <th style='background-color:#bed1dd'>Qte</th>
                            <th style='background-color:#bed1dd'>PU</th>
                            <th style='background-color:#bed1dd'>Date</th>
                            <th style='background-color:#bed1dd'>Destination</th>
                            <th style='background-color:#bed1dd'>Qte</th>
                            <th style='background-color:#bed1dd'>Solde</th>
                            </thead>
                            <tbody>
                                <%
                                double solde=0;
                                    for (FicheStockCMS element : ficheStock) {
                                %>
                                <tr>
                                    <td><%=Utilitaire.formatterDaty(element.getDaty())%></td>
				    <td><%=Utilitaire.champNull(element.getFournisseur())%></td>
				    <td><%=element.getEntree()%></td>
				    <td><%=element.getPu()%></td>
				    <td><%=Utilitaire.formatterDaty(element.getDaty())%></td>
                                    <td><%=Utilitaire.champNull(element.getDestinataire())%></td>
				    <td><%=element.getSortie()%></td>
				    <%solde=+(element.getEntree()+solde)-element.getSortie();%>
                                     <td><%=solde%></td>
                                </tr>
                                <%}
                                %>
                            </tbody>
                        </table>

                    </div>
                  </div>
             </div>
    </div>
                            <a href="<%=(String) session.getValue("lien")%>/../../EtatMedecineServlet?action=ficheStockMedicament&article=<%=article%>&magasin=<%=magasin%>" class="btn btn-primary pull-right" id="export">Imprimer</a>
 </section>
</div>
