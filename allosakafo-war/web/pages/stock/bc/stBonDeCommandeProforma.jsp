
<%@ page import = "mg.cnaps.log.LogService"%>
<%@ page import = "mg.cnaps.st.StBonDeCommande"%>
<%@ page import = "user.*" %>
<%@ page import = "bean.*" %>
<%@ page import = "utilitaire.*" %>
<%@ page import = "affichage.*" %>

<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StBonDeCommande da = new StBonDeCommande();
    PageInsert pi = new PageInsert(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    affichage.Champ[] liste = new affichage.Champ[2];
    
    TypeObjet d = new TypeObjet();
    d.setNomTable("log_direction");
    liste[0] = new Liste("direction", d, "val", "id");
  
    TypeObjet md = new TypeObjet();
    md.setNomTable("PAIE_MODEPAIEMENT");
    liste[1] = new Liste("modepayement", md, "desce", "id");
    
    pi.getFormu().changerEnChamp(liste);
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("numtef").setLibelle("REFERENCE TEF");
    pi.getFormu().getChamp("numtef").setVisible(false);
    pi.getFormu().getChamp("datelivraison").setVisible(false);
    pi.getFormu().getChamp("designation").setType("textarea");
    pi.getFormu().getChamp("fournisseur").setPageAppel("choix/listeFournisseurChoix.jsp");
    pi.getFormu().getChamp("code_dr").setVisible(false);
    pi.getFormu().getChamp("etat").setVisible(false);
    pi.getFormu().getChamp("modepayement").setLibelle("Mode de paiement");
    pi.getFormu().getChamp("service").setVisible(false);
    pi.getFormu().getChamp("tva").setDefaut("20");
    pi.getFormu().getChamp("tva").setLibelle("TVA");
    
    pi.preparerDataFormu();
%>

<div class="content-wrapper">
    <h1 align="center" class="box-title">Cr&eacute;ation proforma</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/bc/stBonDeCommandeFille-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StBonDeCommande">
    </form>
</div>