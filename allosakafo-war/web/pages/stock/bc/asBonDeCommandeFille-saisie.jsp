<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.stock.BonDeCommandeFille"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    BonDeCommandeFille cmdf = new BonDeCommandeFille();
    PageInsert pi = new PageInsert(cmdf, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    pi.getFormu().getChamp("produit").setLibelle("produit");
    pi.getFormu().getChamp("idbc").setLibelle("idbc");
    pi.getFormu().getChamp("pu").setLibelle("pu");
    pi.getFormu().getChamp("quantiteparpack").setLibelle("quantiteparpack");
    pi.getFormu().getChamp("quantite").setLibelle("quantite");
    pi.getFormu().getChamp("montant").setLibelle("montant");
    pi.getFormu().getChamp("remarque").setLibelle("remarque");

    pi.preparerDataFormu();

%>
<div class="content-wrapper">
    <h1 class="box-title">Enregistrer bon de commande fille</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="bondecommandefille" id="bondecommandefille" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/bc/asBonDeCommandeFille-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.stock.BonDeCommandeFille">
    </form>
</div>