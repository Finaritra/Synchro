<%@page import="mg.cnaps.st.StBondecommandepdf"%>
<%@page import="mg.cnaps.st.StBonDeCommandeFilleLibelle"%>
<%@page import="mg.cnaps.st.StBonDeCommande"%>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="bean.*" %>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%
    UserEJB u = null;
    String apres = "facture/apresInit.jsp";
    String lien = null;
    String idBC = request.getParameter("id");
    //Date dt = Utilitaire.stringToDate()
    StBonDeCommande bcm = new StBonDeCommande();
    bcm.setNomTable("STBONDECOMMANDE");
    bcm.setId(idBC);
    StBonDeCommande[] bbc = (StBonDeCommande[]) CGenUtil.rechercher(bcm, null, null, "");
    Societe soc = null;
    String j = bbc[0].getDaty().toString().substring(8, 10);
    int mon = Integer.parseInt(bbc[0].getDaty().toString().substring(5, 7));
    
    String monLettre = Utilitaire.nbToMois(mon);
    String an = bbc[0].getDaty().toString().substring(0, 4);

    u = (user.UserEJB) session.getValue("u");
    lien = (String) session.getValue("lien");
    soc = new Societe();
    soc.setIdSpat("SOC001");
    Societe[] societe = (Societe[]) CGenUtil.rechercher(soc, null, null, "");
    
    StBonDeCommandeFilleLibelle aBC = new StBonDeCommandeFilleLibelle();
    aBC.setIdbc(idBC);
    StBonDeCommandeFilleLibelle[] listeArticle = (StBonDeCommandeFilleLibelle[]) CGenUtil.rechercher(aBC, null, null, " and IDBC = '"+ idBC +"'");
    try {
        String[] colAffiche = {"Designation", "Quantit�", "PU", "Montant"};
        String temp = idBC;
    } catch (Exception ex) {
        ex.printStackTrace();
    }
%>
<div class="content-wrapper">
    <section class="content-header" align="center">
        <h2>Aper&ccedil;u avant impression</h2>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <p class="pull-right"><i>Antananarivo, le <% out.println(j + " " + monLettre + " " + an);%></i></p>
                        <p>
                            
                            <div class="pull-right">
                                <u><font size="+1"><strong>Bon de Com N�</strong></font></u> : <font size="+1"><%=idBC%> </font>
                            </div>
                        </p>
                        <p><u><font size="+1"><strong>Fournisseur </strong></font></u> :<font size="+1"> <%=bbc[0].getFournisseur()%></font></p>
                        <div class="table-bordered">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="headFact">
                                        <th style='background-color:#bed1dd'></th>
                                        <th style='background-color:#bed1dd'>DESIGNATION</th>
                                        <th style='background-color:#bed1dd'>Quantit�</th>
                                        <th style='background-color:#bed1dd'>PU</th>
                                        <th style='background-color:#bed1dd'>Remise</th>
                                        <th style='background-color:#bed1dd'>MONTANT (en Ar)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        double totalMontant = 0;
                                        for (int i = 0; i < listeArticle.length; i++) {
                                            double remise = ((listeArticle[i].getQuantite() * listeArticle[i].getPu()) * listeArticle[i].getRemise()) / 100;
                                    %>
                                    <tr class="gradeA">
                                        <td><%=(i+1)%></td>
                                        <td style="border-bottom:none; border-top:none;  " align="center" ><%=listeArticle[i].getArticle()%></td>
                                        <td align="center" style="border-bottom:none; border-top:none; "><%=(int) listeArticle[i].getQuantite()%></td>
                                        <td align="right" style="border-bottom:none; border-top:none; "><%=Utilitaire.formaterAr(listeArticle[i].getPu())%></td>
                                        <td align="right" style="border-bottom:none; border-top:none; "><%=Utilitaire.formaterAr(remise)%></td>
                                        <td align="right" style="border-bottom:none; border-top:none; "><%=Utilitaire.formaterAr(listeArticle[i].getQuantite() * listeArticle[i].getPu()-remise)%></td>
                                    </tr>
                                    <%
                                        totalMontant += listeArticle[i].getQuantite() * listeArticle[i].getPu()-remise;
                                        }
                                        double tva = (totalMontant * bbc[0].getTva()) / 100;
                                    %>
                                    <tr>  
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td align="right">TVA</td>
                                        <td align="right"><%=Utilitaire.formaterAr(tva)%></td>
                                    </tr>
                                    <tr>  
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td align="right">Total</td>
                                        <td align="right"><%=Utilitaire.formaterAr(tva + totalMontant)%></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p class="pull-right">
                            Le fournisseur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Le Responsable de La Commande&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La Direction
                        </p>
                        <br /><br /><br /><br /><br />
                    </div>
                    <div class="box-footer" align="center">
                        <%=societe[0].getNom()%>: Anio mikarakara, Ampitso mananjara - votre partenaire le plus proche<br/>
                        T&eacute;l : <%=societe[0].getTelephone()%> - Fax: <%=societe[0].getFax()%><br/>
                        Email : <%=societe[0].getEMail()%> - Site web: <%=societe[0].getRegime()%><br/>
                        <%=societe[0].getAdresse()%>
                    </div>
                </div>
            </div>
                        
                        <div class="pull-right"><a onclick="imprimer()" class="btn btn-primary">Imprimer</a></div>
                        <%
                            StBondecommandepdf bondecommandepdf=new StBondecommandepdf();
                            bondecommandepdf.setNomTable("ST_BONDCOMMANDE_LIBELLEE");
                            StBondecommandepdf[] bondecommandes = (StBondecommandepdf[]) CGenUtil.rechercher(bondecommandepdf, null, null, null, "AND ID='"+request.getParameter("id")+"'");
                            %>
                        
                        <script language='JavaScript'>
                            function imprimer() {
                                <%
                                    if(bondecommandes.length!=0){%>
                        
                                    document.location.replace('<%=(String) session.getValue("lien")%>/../../EtatStockServlet?action=boncommande2&id=<%=request.getParameter("id")%>&tva=<%=bbc[0].getTva()%>');
                                <% }
                                    else{
                                %>
                                    alert('Le bon de commande ne peut pas �tre imprim� sans avoir �diter le TEF');
                                <%
                                }
                                %>
                            }
                        </script>
        </div>
    </section>    
</div>
