<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.st.StBonDeCommande"%>
<%@page import="affichage.PageRecherche"%>
<%
    StBonDeCommande lv = new StBonDeCommande();
    lv.setNomTable("ST_BONDCOMMANDE_LIBELLE");
    if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("") != 0) {
        lv.setNomTable(request.getParameter("etat"));
    }
    String listeCrt[] = {"id", "daty", "designation", "direction", "modepayement"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "designation", "direction", "modepayement"};
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date Max");
    
    affichage.Champ[] liste = new affichage.Champ[2];
    
    TypeObjet md = new TypeObjet();
    md.setNomTable("PAIE_MODEPAIEMENT");
    liste[0] = new Liste("modepayement", md, "desce", "id");
    
    TypeObjet d = new TypeObjet();
    d.setNomTable("log_direction");
    liste[1] = new Liste("direction", d, "desce", "desce");
    
    pr.getFormu().changerEnChamp(liste);
    
    pr.getFormu().getChamp("modepayement").setLibelle("Mode de paiement");

    pr.setApres("stock/bc/stBonDeCommande-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste bon de commande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/bc/stBonDeCommande-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="etat" class="champ" id="etat" onchange="changerDesignation()" >
                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_BONDCOMMANDE_LIBELLE") == 0) {%>    
                        <option value="ST_BONDCOMMANDE_LIBELLE" selected>Tous</option>
                        <% } else { %>
                        <option value="ST_BONDCOMMANDE_LIBELLE" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_BONDCOMMANDE_VALIDE") == 0) {%>
                        <option value="ST_BONDCOMMANDE_VALIDE" selected>Valid�</option>
                        <% } else { %>
                        <option value="ST_BONDCOMMANDE_VALIDE">Valid�</option>
                        <% } %>

                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_BONDCOMMANDE_EN_COURS") == 0) {%>
                        <option value="ST_BONDCOMMANDE_EN_COURS" selected>En cours</option>
                        <% } else { %>
                        <option value="ST_BONDCOMMANDE_EN_COURS">En cours</option>
                        <% } %>

                    </select>
                </div>
                <div class="col-md-4"></div>
            </div>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/bc/stBonDeCommande-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "Designation", "Direction", "Mode de payement"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
