<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="bean.*" %>
<%@ page import="affichage.*" %>

<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>Bon de commande</h1>
    </section>
    <section class="content">
        <form action="../BonCommandeServlet" method="POST">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="col-md-3">                            
                                <div class="form-group">
                                    <label>Date de saisie</label>
                                    <input id="datesaisie" name="datesaisie" class="form-control datepicker" type="text">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Magasin</label>
                                    <select id="magasin" class="form-control" name="magasin">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Date de livraison</label>
                                    <input id="datelivraison" name="datelivraison" class="form-control datepicker" value="" type="text">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6" style="padding: 0;">
                                    <div class="form-group">
                                        <label>Observation</label>
                                        <textarea style="resize: none;padding: 5px;" id="designation" class="form-control" name="designation" data-parsley-id="6">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Direction</label>
                                    <select id="direction" class="form-control" name="direction">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Mode de paiement</label>
                                    <select id="modepaiement" class="form-control" name="modepaiement">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Code DR</label>
                                    <div class="input-group">                              
                                        <input class="form-control" id="codedr" name="codedr" type="text">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="button" onclick="">...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Projet</label>
                                    <div class="input-group">                              
                                        <input class="form-control" id="projet" name="projet" type="text">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="button" onclick="">...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Classe</label>
                                    <select id="classee" class="form-control" name="classee">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table class="table table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th>Article</th>
                                        <th>Quantit&eacute;</th>
                                        <th>Prix unitaire</th>
                                        <th>Observation</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody  id="ajout_ligne">
                                    <%
                                        int nbLine = 5;
                                        for (int i = 0; i <= nbLine; i++) {%>
                                    <tr>
                                        <td>
                                            <div class="input-group">                              
                                                <input class="form-control" id="article<%=i + 1%>" value="" name="article<%=i + 1%>" type="text">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default btn-sm" type="button" onclick="pagePopUp('choix/listeArticleChoix.jsp?champReturn=article<%=i + 1%>'">...</button>
                                                </div>
                                            </div>
                                        </td>
                                        <td><input type="text" id="quantite<%=i + 1%>" class="form-control" value="" name="quantite<%=i + 1%>"></td>
                                        <td><input type="text" id="pu<%=i + 1%>" class="form-control" value="" name="pu<%=i + 1%>"></td>
                                        <td><input type="text" id="remarque<%=i + 1%>" class="form-control" value="" name="remarque<%=i + 1%>"></td>
                                    </tr>
                                    <% }%>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <button type="button" class="btn-sm btn-default pull-right" onclick="addLineBCFille()">Ajouter une ligne</button>
                            <button type="button" class="btn-sm btn-default pull-right" onclick="remove_line()">Supprimer une ligne</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <input type="hidden" id="taille" name="taille" value="<%=nbLine%>" >
                <div class="col-md-6 pull-right" align="right">
                    <%if (request.getParameter("idbc") != null) {%>
                    <button class="btn btn-primary" type="submit" name="action" value="update">Modifier</button>
                    <%} else {%>
                    <button class="btn btn-primary" type="submit" name="action" value="insert">Enregistrer</button>
                    <%}%>
                </div>
            </div>
        </form>
    </section>
</div>
<script>
    function add_div_popup(name, choix, champReturn) {
        var div_mere = document.createElement('div');
        div_mere.className = "input-group";

        var input = document.createElement('input');
        input.className = "form-control";
        input.id = name;
        input.name = name;
        input.type = "text";

        var div_button = document.createElement('div');
        div_button.className = "input-group-btn";

        var button = document.createElement('button');
        button.className = "btn btn-default btn-sm";
        button.type = "button";


        button.onclick = function () {
            pagePopUp('choix/' + choix + '?champReturn=' + champReturn);
        };

        var text_button = document.createTextNode("...");
        button.appendChild(text_button);
        div_button.appendChild(button);
        div_mere.appendChild(input);
        div_mere.appendChild(div_button);
        return div_mere;
    }
    function addLineBCFille() {
    <%
        nbLine++;
    %>
        document.getElementById("taille").value = <%=nbLine%>;
        var ligne = document.createElement('tr');
        var col1 = document.createElement('td');
        var col2 = document.createElement('td');
        var col3 = document.createElement('td');
        var col4 = document.createElement('td');
        var article = document.createElement('input');
        article.type = "text";
        article.id = "article<%=nbLine%>";
        article.name = "article<%=nbLine%>";
        var quantite = document.createElement('input');
        quantite.type = "text";
        quantite.id = "quantite<%=nbLine%>";
        quantite.name = "quantite<%=nbLine%>";
        var pu = document.createElement('input');
        pu.type = "text";
        pu.id = "pu<%=nbLine%>";
        pu.name = "pu<%=nbLine%>";
        var remarque = document.createElement('input');
        remarque.type = "text";
        remarque.id = "remarque<%=nbLine%>";
        remarque.name = "remarque<%=nbLine%>";
        remarque.className = "remarque";

        var champReturnArticle = "article<%=nbLine%>";
        col1.appendChild(add_div_popup("article<%=nbLine%>", "listeArticleChoix.jsp.jsp", champReturnArticle));
        col2.appendChild(quantite);
        col3.appendChild(pu);
        col4.appendChild(remarque);

        var table = document.getElementById('ajout_ligne');
        var ligne = document.createElement('tr');
        table.appendChild(ligne);
    }
    function remove_line() {
        var ligne = document.getElementById("ajout_ligne").lastChild;
        ligne.parentNode.removeChild(ligne);
    <%
        nbLine--;
    %>
        document.getElementById("taille").value = <%=nbLine%>;
    }
</script>
