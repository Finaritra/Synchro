
<%@page import="mg.cnaps.sig.SigEmployeursComptes"%>
<%@page import="mg.cnaps.sig.Employeurs"%>
<%@page import="mg.cnaps.st.StockService"%>
<%@page import="mg.cnaps.st.StTiers"%> 
<%@page import="mg.cnaps.st.StBondecommandeFilleTef"%>
<%@page import="mg.cnaps.st.StBonDeCommande"%>
<%@page import="bean.CGenUtil"%>
<%@page import="user.UserEJB"%>

<%@page import = "bean.TypeObjet"%>
<%@page import = "utilitaire.Utilitaire"%>

<%
    try {

        UserEJB u = (UserEJB) session.getAttribute("u");

        TypeObjet[] typeTef = null;
        TypeObjet tt = new TypeObjet();
        tt.setNomTable("budget_type_tef");
        typeTef = (TypeObjet[]) CGenUtil.rechercher(tt, null, null, "");

        TypeObjet[] uniteListe = null;
        TypeObjet unite = new TypeObjet();
        unite.setNomTable("st_unite");
        uniteListe = (TypeObjet[]) CGenUtil.rechercher(unite, null, null, "");

        TypeObjet ssto = new TypeObjet();
        ssto.setNomTable("SOUS_TYPE_TEF");
        TypeObjet[] sstptef = (TypeObjet[]) CGenUtil.rechercher(ssto, null, null, "");

        // UserEJB uniteListeu = null;
        String lien = null;
        String bute = "";

        // u = (UserEJB) session.getValue("u");
        lien = (String) session.getValue("lien");
        StBondecommandeFilleTef[] stFille = null;
        double montant = 0;
        double montanttva = 0;
        String date = "";
        String id = request.getParameter("id");
        StBonDeCommande stbc = new StBonDeCommande();
        StBonDeCommande[] bon = null;
        if (id != null) {
            // System.out.println(" AND ID = '" + id.trim() + "'");
            bon = (StBonDeCommande[]) u.getData(new StBonDeCommande(), null, null, null, " AND ID = '" + id.trim() + "'");
            // System.out.println(" length ======= " + bon.length);
            if (bon != null && bon.length > 0) {
                stbc = bon[0];
                if (stbc.getNumtef() != null && !stbc.getNumtef().isEmpty()) {
                    bute = "budget/tef/tef-saisie.jsp&id=" + stbc.getNumtef();
                } else {
                    bute = "stock/bc/stBonDeCommande-fiche.jsp&id=" + id.trim();
                }
                String where = " AND IDBC = '" + id.trim() + "'";
                stFille = (StBondecommandeFilleTef[]) u.getData(new StBondecommandeFilleTef(), null, null, null, where);

                if (stFille != null && stFille.length > 0) {
                    double montantTmp = 0;
                    for (StBondecommandeFilleTef item : stFille) {
                        double rem = 0;
                        if (item.getRemise() != 0) {
                            rem = (item.getQuantite() * item.getPu() * item.getRemise()) / 100;
                            montantTmp += (item.getQuantite() * item.getPu()) - rem;
                        } else {
                            montantTmp += (item.getQuantite() * item.getPu());
                        }
                    }
                    montant = montantTmp;
                    montanttva = (montantTmp * bon[0].getTva()) / 100;
                }
                date = Utilitaire.datetostring(stbc.getDaty());
            } else {
                throw new Exception("bon de commande introuvable");
            }
        } else {
            throw new Exception("id bon de commande null");
        }
        StTiers tiers = new StTiers();
        Employeurs emp = StockService.getEmployeurs(bon[0].getFournisseur());
        SigEmployeursComptes empCompte = new SigEmployeursComptes();
        SigEmployeursComptes empComptet = StockService.getSigEmployeursComptes(emp.getId());

        String succ = "";
        String banqueagencecode = "";
        String banquecode = "";
        String banquecomptenumero = "";
        String banquecomptecle = "";
        if (empComptet != null) {
            succ = empComptet.getSuccursale_code();
            banqueagencecode = empComptet.getAgence_code();
            banquecode = empComptet.getBanque_code();
            banquecomptenumero = empComptet.getCompte_numero();
            banquecomptecle = empComptet.getCompte_cle();

        }


%>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>G�n�ration TEF</h1>
    </section>
    <section class="content">
        <form action="<%=lien%>?but=stock/bc/apresBC.jsp" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">TITRE D'ENGAGEMENT FINANCIER</h3>                
                        </div>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input id="code" name="code" class="form-control" type="text" value="<%=succ%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date TEF</label>
                                    <input id="daty" name="daty" class="form-control datepicker" type="text" value="<%=date%>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Libell�</label>
                                    <input id="libelle" name="libelle" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Montant</label>
                                    <input id="montant" name="montant" class="form-control" type="text" value="<%=Utilitaire.doubleWithoutExponential(montant)%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Montant TVA</label>
                                    <input id="montant_tva" name="montant_tva" class="form-control" type="text" value="<%=Utilitaire.doubleWithoutExponential(montanttva)%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Id B�n�ficiaire</label>
                                    <div class="input-group">                              
                                        <input name="idbeneficiairelibelle" id="idbeneficiairelibelle" class="form-control" type="text" value="<% if (bon[0] != null && bon[0].getFournisseur() != null && bon[0].getFournisseur().compareTo("") != 0) {
                                                out.print(bon[0].getFournisseur());
                                            } %>">
                                        <input name="idbeneficiaire" id="idbeneficiaire" class="form-control" type="hidden" value="<% if (bon[0] != null && bon[0].getFournisseur() != null && bon[0].getFournisseur().compareTo("") != 0) {
                                                out.print(bon[0].getFournisseur());
                                            } %>">
                                        
                                        <div class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="button" onclick="pagePopUp('choix/beneficiaireChoix.jsp?champReturn=idbeneficiaire;idbeneficiairelibelle')">...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nom B&eacute;n&eacute;ficiaire</label>
                                    <input id="nombeneficiaire" name="nombeneficiaire" class="form-control" type="text" value="<% if (bon[0] != null && bon[0].getFournisseur() != null && bon[0].getFournisseur().compareTo("") != 0) {
                                                out.print(emp.getEmployeur_nom());
                                            }%>">
                                </div>
                            </div>
                            <input id="nombeneficiaire" name="modedepaiement" class="form-control" type="hidden" value="<%=bon[0].getModepayement()%>" />
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Type B&eacute;n&eacute;ficiaire</label>
                                    <select id="typebeneficiaire" class="form-control" name="typebeneficiaire"> 
                                        <option value="fournisseur">Fournisseur</option>
                                        <option value="beneficiaire">B&eacute;n&eacute;ficiaire</option>
                                        <option value="travailleur">Travailleur</option>
                                        <option value="employeur">Employeur</option>
                                        <option value="autre">Autre</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code Agence</label>
                                    <input id="banqueagencecode" name="banqueagencecode" class="form-control" type="text" value="<%=banqueagencecode%>">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code Banque</label>
                                    <input id="banquecode" name="banquecode" class="form-control" type="text" value="<%=banquecode%>">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Num&eacute;ro de Compte</label>                              
                                    <input id="banquecomptenumero" name="banquecomptenumero" class="form-control" type="text" value="<%=banquecomptenumero%>">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cl&eacute;</label>                                                                   
                                    <input id="banquecomptecle" name="banquecomptecle" class="form-control" type="text" value="<%=banquecomptecle%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date Compte</label>
                                    <input id="banquecomptedate" name="banquecomptedate" class="form-control datepicker" type="text" value="<%=Utilitaire.dateDuJour()%>">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Budget D&eacute;pense</label>
                                    <div class="input-group">                              
                                        <input id="budgetlibelle" name="budgetlibelle" class="form-control" type="text">
                                        <input id="budget" name="budget" class="form-control" type="hidden">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="button" onclick="pagePopUp('choix/budgetDepenseChoix.jsp?champReturn=budget;budgetlibelle')">...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Type TEF</label>
                                    <select id="typetef" class="form-control" name="typetef">
                                        <% for (int i = 0; i < typeTef.length; i++) {%>
                                        <option value="<%=typeTef[i].getId()%>">
                                            <%=typeTef[i].getVal()%>
                                        </option>
                                        <% }%>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Frais</label>
                                    <input id="frais" name="frais" class="form-control" type="text" value="0">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Sous Type TEF (*)</label>
                                    <select id="sous_type_tef" class="form-control" name="sous_type_tef">
                                        <% for (int i = 0; i < sstptef.length; i++) {%>
                                        <option value="<%=sstptef[i].getId()%>">
                                            <%=sstptef[i].getVal()%>
                                        </option>
                                        <% }%>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Remarque</label>
                                    <input id="remarque" name="remarque" class="form-control" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                                   
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>D&eacute;signation</th>
                                        <th>Qt&eacute;</th>
                                        <th>Unit&eacute;�</th>
                                        <th>Remise</th>
                                        <th>P.U</th>
                                        <th>Montant</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        if (stFille != null && stFille.length > 0) {
                                            for (int i = 0; i < stFille.length; i++) {
                                                double remise = 0;
                                                double prixremise = stFille[i].getPu();
                                                if (stFille[i].getRemise() != 0) {
                                                    remise = ((stFille[i].getQuantite() * stFille[i].getPu()) * stFille[i].getRemise()) / 100;
                                                    prixremise = stFille[i].getPu() - ((stFille[i].getPu() * stFille[i].getRemise()) / 100);
                                                }
                                    %>
                                    <tr>
                                        <td>
                                            <div class="input-group">                              
                                                <input readonly="true" class="form-control" id="code_<%=i + 1%>" name="code_<%=i + 1%>" type="text" value="<%=stFille[i].getId()%>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">                              
                                                <input class="form-control" readonly="true" id="designation_<%=i + 1%>" name="designation_<%=i + 1%>" type="text" value="<%=stFille[i].getDesignation()%>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">                              
                                                <input class="form-control" readonly="true" id="quantite_<%=i + 1%>" name="quantite_<%=i + 1%>" type="text" value="<%=Utilitaire.doubleWithoutExponential(stFille[i].getQuantite())%>">
                                            </div>
                                        </td>
                                        <td>
                                            <select class="form-control" id="unite_<%=i + 1%>" name="unite_<%=i + 1%>">
                                                <% for (int index = 0; index < uniteListe.length; index++) {%>
                                                <option value="<%=uniteListe[index].getId()%>" <% if (stFille[i].getUnite().compareToIgnoreCase(uniteListe[index].getId()) == 0) {
                                                        out.print("selected");
                                                    }%> ><%=uniteListe[index].getVal()%></option>
                                                <% }%>
                                            </select> 
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <%=Utilitaire.doubleWithoutExponential(stFille[i].getRemise())%> %
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">

                                                <input class="form-control" readonly="true" type="text" id="pu_<%=i + 1%>" name="pu_<%=i + 1%>" value="<%=Utilitaire.doubleWithoutExponential(prixremise)%>">
                                            </div>
                                        </td>

                                        <td>
                                            <div class="input-group">                              
                                                <input class="form-control" readonly="true" id="montant_<%=i + 1%>" name="code_<%=i + 1%>" type="text" value="<%=Utilitaire.doubleWithoutExponential((stFille[i].getQuantite() * stFille[i].getPu()) - remise)%>">
                                            </div>
                                        </td>
                                    </tr>
                                    <% }
                                        }%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a href="<%=lien + "?but=stock/bc/stBonDeCommandeFille-saisie.jsp&id=" + request.getParameter("id")%>"><button class="btn btn-primary" type="submit">Modifier</button></a>
                </div>
                <div class="col-md-6" align="right">
                    <button class="btn btn-primary" type="submit">Enregistrer</button>
                </div>
            </div>
            <input name="acte" type="hidden" id="nature" value="inserttef">
            <input name="narticle" type="hidden" id="narticle" value="<%=stFille.length%>">
            <input name="idbc" type="hidden" id="idbc" value="<%=id%>">
            <input name="bute" type="hidden" id="bute" value="<%=bute%>">
        </form>
    </section>
</div>
<%} catch (Exception ex) {
                ex.printStackTrace();
            }%>