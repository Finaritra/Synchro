
<%@page import="mg.cnaps.st.StBonDeCommande"%>
<%@page import="mg.cnaps.budget.BudgetProjet"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="java.sql.Connection"%>
<%@page import="mg.cnaps.st.StBonDeCommandeFilleLibelle"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    String idmere = request.getParameter("id");
    UserEJB u = (UserEJB) session.getAttribute("u");
    Connection c = null;
    try {
        c = (new utilitaire.UtilDB()).GetConn();
        
        StBonDeCommande bcmere = new StBonDeCommande();
        bcmere.setNomTable("STBONDECOMMANDE");
        StBonDeCommande listBC = ((StBonDeCommande[])CGenUtil.rechercher(bcmere, null, null, " and ID = '" + idmere.trim() + "'"))[0];
        if(listBC.getEtat() < ConstanteEtat.getEtatValider()){
            %>
            <script language="JavaScript">alert('Bon de commande non VIS&Eacute;E');history.back();</script>
            <%
        }
        if(listBC.getEtat() == ConstanteEtat.getEtatCloture()){
            %>
            <script language="JavaScript">alert('Bon de commande d&eacute;j&agrave; CLOTUR&Eacute;E');history.back();</script>
            <%
        }
        StBonDeCommandeFilleLibelle crt = new StBonDeCommandeFilleLibelle();
        crt.setNomTable("St_BonDeCommandeFille_Libelle");
        StBonDeCommandeFilleLibelle[] listFille = (StBonDeCommandeFilleLibelle[]) CGenUtil.rechercher(crt, null, null, " and idbc = '" + idmere + "'");
        String lien = (String) session.getValue("lien");

        StMagasin mg = new StMagasin();
        mg.setNomTable("ST_MAGASIN_LIBELLE");
        StMagasin[] lv = (StMagasin[]) CGenUtil.rechercher(mg, null, null, c, "");
        
        TypeObjet classe = new TypeObjet();
        classe.setNomTable("st_classe");
        TypeObjet[] cls = (TypeObjet[]) CGenUtil.rechercher(classe, null, null, c, "");

        TypeObjet tbn = new TypeObjet();
        tbn.setNomTable("st_type_besoin");
        TypeObjet[] tb = (TypeObjet[]) CGenUtil.rechercher(tbn, null, null, c, "");

        BudgetProjet prj = new BudgetProjet();
        prj.setNomTable("Budget_Projet");
        BudgetProjet[] lprj = (BudgetProjet[]) CGenUtil.rechercher(prj, null, null, c, " order by INTITULE desc");
%>
<div class="content-wrapper">
    <h1 align="center">Enregistrer mouvement stock</h1>
    <form action="<%=lien%>?but=stock/bc/apresBC.jsp" method="post" name="approfille" id="approfille" data-parsley-validate>

        <table class="table" width="30%">
            <tr>
                <th>Exercice:</th>
                <td><input type="text" name="excercice" class="champ" id="excercice" value="<%=Utilitaire.getAnneeEnCours()%>" class="form-control"></td>
            </tr>
            <tr>
                <th>Magasin</th>
                <td>
                    <select name="magasin" class="champ" id="magasin" tabindex="1" data-parsley-id="4">
                        <%
                            for (int i = 0; i < lv.length; i++) {
                        %>
                        <option value="<%=lv[i].getId()%>"><%=lv[i].getDesce()%></option>
                        <%
                            }
                        %>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Classe</th>
                <td>
                    <select name="classee" class="champ" id="classee" tabindex="4" data-parsley-id="4">
                        <%
                            for (int i = 0; i < cls.length; i++) {
                        %>
                        <option value="<%=cls[i].getId()%>"><%=cls[i].getVal()%></option>
                        <%
                            }
                        %>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Type</th>
                <td>
                    <select name="typebesoin" class="champ" id="typebesoin" tabindex="9" data-parsley-id="4">
                        <%
                            for (int i = 0; i < tb.length; i++) {
                        %>
                        <option value="<%=tb[i].getId()%>"><%=tb[i].getVal()%></option>
                        <%
                            }
                        %>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Num Facture</th>
                <td>
                    <input type="text" name="numfacture" class="champ" id="excercice" class="form-control">
                </td>
            </tr>
            <tr>
                <th>Projet</th>
                <td>
                    <select name="projet" class="champ" id="projet" tabindex="9" data-parsley-id="4">
                        <%
                            for (int i = 0; i < lprj.length; i++) {
                        %>
                        <option value="<%=lprj[i].getId()%>"><%=lprj[i].getCode() + " "+ lprj[i].getIntitule() %></option>
                        <%
                            }
                        %>
                    </select>
                
                </td>
            </tr>
        <table class="table table-hover" width="80%">
            <thead>
                <tr>
                    <th style="background-color:#bed1dd">Id article</th>
                    <th style="background-color:#bed1dd">Article</th>
                    <th style="background-color:#bed1dd">Quantite</th>
                    <th style="background-color:#bed1dd">P.U.</th>
                    <th style="background-color:#bed1dd">Remarque</th>
                </tr></thead>
            <tbody>
                <%
                    for (int i = 0; i < listFille.length; i++) {
                %>
                <tr>
                    <td><input type="text" name="id" id="id" value="<%=listFille[i].getIdArticle()%>" readonly="readonly"></td>
                    <td><%=listFille[i].getArticle()%></td>
                    <td>
                        <input type="text" name="quantite" id="quantite" value="<%=listFille[i].getQuantite()%>">
                        <input type="hidden" name="qtemax" id="qtemax" value="<%=listFille[i].getQuantite()%>">
                    </td>
                    <td><input type="text" name="pu" id="pu" value="<%=listFille[i].getPu()%>"></td>
                    <td><input type="text" name="remarque" id="remarque" value="<%=listFille[i].getRemarque()%>"></td>
                    
                </tr>
                <%
                    }
                %>
            </tbody>
        </table>
        <input name="idmere" type="hidden" id="idmere" value="<%=idmere%>">
        <input name="acte" type="hidden" id="nature" value="insertwbc">
        <input name="bute" type="hidden" id="bute" value="stock/bc/stBonDeCommande-liste.jsp">
        <hr/>
        <div class="col-md-12">
            <button class="btn btn-primary pull-right" type="submit">Valider</button>
        </div>
    </form>
</div>
<% } catch (Exception ex) {
        ex.printStackTrace();
    } finally {
        if (c != null) {
            c.close();
        }
    }
%>