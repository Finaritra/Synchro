<%@page import="mg.cnaps.budget.BudgetTefFille"%>
<%@page import="mg.cnaps.st.StockService"%>
<%@page import="mg.cnaps.st.StFactureAppro"%>
<%@page import="mg.cnaps.op.OpDetails"%>
<%@page import="mg.cnaps.sig.SigTiers"%>
<%@page import="mg.cnaps.sig.SigTiersPJAttache"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.accueil.*" %>
<%
    try {
        OpCompletLibelle opcomplet;
        UserEJB u;
        String op = request.getParameter("id");
        u = (UserEJB) session.getAttribute("u");
        opcomplet = new OpCompletLibelle();
        String[] libelleOPCompletFiche = {"Id", "OP Numero", "Dossier", "Date OP", "Montant", "Beneficiaire", "Trav Emp Numero", "Trav emp Nom", "Trav emp Employeur Nom", "Trav emp Employeur Num", "Sous Prestation", "Compte", "Id B&eacute;n&eacute;ficiaire", "Type B&eacute;n&eacute;ficiaire", "CIN B&eacute;n&eacute;ficiaire", "Date CIN B&eacute;n&eacute;ficiaire", "Banque Succursale Code", "Banque Code", "Agence Banque Code", "Banque Compte Num&eacute;ro", "Banque Compte Cl&eacute;", "Banque Compte Date", "&Eacute;tat", "Compte Cr&eacutedit", "Exercice", "Type OP", "Code Direction R&eacute;gional", "Code Direction R&eacute;gional Libelle", "IdUser", "Mode de paiement", "Reference"};
        PageConsulte pc = new PageConsulte(opcomplet, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
        pc.getChampByName("code_dr").setVisible(false);
        pc.setCanUploadFile(true);
        pc.setFichier_table_cible("OP_PJ");
        pc.setFichier_table_procedure("getSeqOpPj");

        pc.setLibAffichage(libelleOPCompletFiche);
        OPComplet opcomp = (OPComplet) pc.getBase();
        

        UploadPj criteria = new UploadPj("OP_PJ");
        UploadPj[] tierspjattache = (UploadPj[]) CGenUtil.rechercher(criteria, null, null, " AND MERE = '" + op + "'");
        
        UploadPj criteria2 = new UploadPj("BUDGET_TEF_PJ");
        UploadPj[] tierspjattache2 = (UploadPj[]) CGenUtil.rechercher(criteria2, null, null, " AND MERE = '" + op + "'");
        
        OpDetails opd = new OpDetails();

        //OpDetails[] liste_opd = (OpDetails[])u.getData(opd, null, null, null, "");
        boolean testDossier = (opcomp.getDossier() != null && !opcomp.getDossier().equals("") && !opcomp.getDossier().equals("-"));

        StFactureAppro fct = new StFactureAppro();
        StFactureAppro[] facture  = (StFactureAppro[]) CGenUtil.rechercher(fct, null, null, " AND IDOBJET = '" + op + "'");
        
        BudgetTefFille[] tefille = StockService.getDetailsOP(op);
        String cdn = configuration.CynthiaConf.properties.getProperty("cdnReadUri");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=tresorerie/OP/op-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a> Fiche OP</h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>

                    </div>
                    <div class="box-footer">
                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=tresorerie/OP/op-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                        <a class="btn btn-default pull-right"  href="${pageContext.request.contextPath}/OP?id=<%=opcomplet.getId()%>" style="margin-right: 10px">Imprimer</a>
                        <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + op%>&acte=cloturer&bute=stock/bc/op-fiche.jsp&classe=mg.cnaps.accueil.OPComplet" style="margin-right: 10px">Cloturer</a>
                        <a class="btn btn-default pull-right"  href="<%=(String) session.getValue("lien") + "?but=tresorerie/OP/op-details.jsp&id=" + op%>" style="margin-right: 10px">Voir d&eacute;tails</a>
                        <a class="btn btn-primary pull-right" onclick="pagePopUp('choix/choixFactureMultiple.jsp?champReturn=<%=op%>')" style="margin-right: 10px">Attacher facture</a>
                        <%if (testDossier && opcomp.getDossier().substring(0, 4).compareToIgnoreCase("DOSS") == 0) {%>
                        <a href="<%out.print(session.getAttribute("lien") + "?but=dossier/dossier-employeur.jsp&id=" + opcomp.getDossier());%>" class="btn btn-info pull-right">Voir Dossier</a>
                        <%} else if (testDossier && opcomp.getDossier().substring(0, 4).compareToIgnoreCase("TEF") == 0) {%>
                        <a href="<%out.print(session.getAttribute("lien") + "?but=budget/tef/tef-fiche.jsp&id=" + opcomp.getDossier());%>" class="btn btn-info pull-right">Voir DDossier</a>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%=pc.getBasPage()%>


    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">D&eacute;tails OP</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Observation</th>
                                    <th>Quantit&eacute;</th>
                                    <th>PU</th>
                                    <th>Montant</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                for(int i = 0; i < tefille.length; i++){
                                    %>
                                    <tr>
                                        <td><%=tefille[i].getCode()%></td>
                                        <td><%=tefille[i].getDesignation()%></td>
                                        <td><%=Utilitaire.formaterAr(tefille[i].getQuantite())%></td>
                                        <td><%=Utilitaire.formaterAr(tefille[i].getPu())%></td>
                                        <td><%=Utilitaire.formaterAr(tefille[i].getQuantite() * tefille[i].getPu())%></td>
                                        
                                    </tr>
                                    <%
                                }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

                            
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">Liste facture attach&eacute;e</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Facture</th>
                                    <th>Commande</th>
                                    <th>Montant</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                for(int i = 0; i < facture.length; i++){
                                    %>
                                    <tr>
                                        <td><%=Utilitaire.format(facture[i].getDaty())%></td>
                                        <td><%=facture[i].getFacture()%></td>
                                        <td><%=facture[i].getCommande()%></td>
                                        <td><%=Utilitaire.formaterAr(facture[i].getMontant())%></td>
                                        <td><a href="#">Modifier</a></td>
                                    </tr>
                                    <%
                                }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>                        
                            
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-content">
                <div class="box">
                    <div class="box-title with-border">
                        <h2 class="box-title" style="margin-left: 10px;">Les fichiers d&eacute;j&agrave; attach&eacute;s (OP)</h2>
                    </div>
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="background-color:#bed1dd;">Libell&eacute;</th>
                                    <th style="background-color:#bed1dd;">Fichier</th>
                                    <th style="background-color:#bed1dd;">Voir</th>
                                    <th style="background-color:#bed1dd;">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%if (tierspjattache != null && tierspjattache.length > 0) {
                                        for (UploadPj element : tierspjattache) {%>
                                <tr>
                                    <td><%=utilitaire.Utilitaire.champNull(element.getLibelle())%></td>
                                    <td><%=element.getChemin()%></td>
                                    <td><a href="#" class="btn btn-primary" onclick="javascript:pagePopUp('<%=cdn + element.getChemin()%>')">Voir</a></td>
                                    <td><a class="btn btn-danger" href="../DeletePj?but=<%=request.getParameter("but")%>&idpj=<%=element.getId()%>&id=<%=request.getParameter("id")%>&nomtable=OP_PJ&procedure=getSeqOpPj">supprimer</a></td>
                                </tr>
                                <%}
                                } else {%>
                                <tr><td colspan="3" style="text-align: center;"><strong>Aucun fichier</strong></td></tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-content">
                <div class="box">
                    <div class="box-title with-border">
                        <h2 class="box-title" style="margin-left: 10px;">Les fichiers d&eacute;j&agrave; attach&eacute;s (TEF)</h2>
                    </div>
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="background-color:#bed1dd;">Libell&eacute;</th>
                                    <th style="background-color:#bed1dd;">Fichier</th>
                                    <th style="background-color:#bed1dd;">Voir</th>
                                    <th style="background-color:#bed1dd;">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%if (tierspjattache2 != null && tierspjattache2.length > 0) {
                                        for (UploadPj element : tierspjattache2) {%>
                                <tr>
                                    <td><%=utilitaire.Utilitaire.champNull(element.getLibelle())%></td>
                                    <td><%=element.getChemin()%></td>
                                    <td><a href="#" class="btn btn-primary" onclick="javascript:pagePopUp('<%=cdn + element.getChemin()%>')">Voir</a></td>
                                    <td><a class="btn btn-danger" href="../DeletePj?but=<%=request.getParameter("but")%>&idpj=<%=element.getId()%>&id=<%=request.getParameter("id")%>&nomtable=BUDGET_PJ_TEF
&procedure=getSeqBudgetPjTef">supprimer</a></td>
                                </tr>
                                <%}
                                } else {%>
                                <tr><td colspan="3" style="text-align: center;"><strong>Aucun fichier</strong></td></tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%
} catch (Exception ex) {
    ex.printStackTrace();
%>
<script language="JavaScript">
            alert('<%=ex.getMessage()%>');
            history.back();
</script>
<%
    }
%>