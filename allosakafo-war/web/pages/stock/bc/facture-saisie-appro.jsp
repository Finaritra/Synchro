<%-- 
    Document   : facture-saisie-appro
    Created on : 10 juin 2016, 10:01:18
    Author     : Joe
--%>
<%@page import="utilitaire.Utilitaire"%>
<div class="content-wrapper">
    <h1 align="center">Saisie facture</h1>
    <form action="<%=session.getAttribute("lien")%>?but=stock/bc/apresBC.jsp" method="post" name="bordereau" id="bordereau" >
        <div class="row">
            <div class="col-md-6">
                <div class="box-insert">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>
                                            <label for="daty" tabindex="1">Date:</label>
                                        </th>
                                        <td>
                                            <input name="daty" type="text" class="form-control" id="daty" tabindex="1" value="<%=Utilitaire.dateDuJour()%>" data-parsley-id="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <label for="numfacture" tabindex="2">Num Facture:</label>
                                        </th>
                                        <td>
                                            <input name="numfacture" type="text" class="form-control" id="numfacture"  tabindex="2" data-parsley-id="4">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><label for="commande" tabindex="3">Numero commande:</label></th>
                                        <td><input name="commande" type="text" class="form-control" id="commande"  tabindex="3" data-parsley-id="6"></td>
                                    </tr>
                                    <tr>
                                        <th><label for="datecomande" tabindex="4">Date commande:</label></th>
                                        <td><input name="datecomande" type="text" class="form-control" id="datecomande" value="<%=Utilitaire.dateDuJour()%>" tabindex="4" data-parsley-id="8"></td>
                                    </tr>
                                    <tr>
                                        <th><label for="montant" tabindex="5">Montant</label></th>
                                        <td><input name="montant" type="text" class="form-control" id="montant" value="0" onblur="calculer('tva')" tabindex="5" data-parsley-id="10"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <div class="col-xs-12">
                                <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;" tabindex="51">Valider</button>
                                <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;" tabindex="52">Réinitialiser</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input name="acte" type="hidden" id="nature" value="insertfacture">
        <input name="bute" type="hidden" id="bute" value="stock/bc/facture-saisie-appro.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.recette.FactureFille">
    </form>
</div>