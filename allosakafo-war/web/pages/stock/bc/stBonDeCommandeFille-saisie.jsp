<%@page import="mg.cnaps.st.StBonDeCommande"%>
<%@page import="mg.cnaps.st.StBonDeCommandeFilleLibelle"%>
<%@page import="mg.cnaps.st.StBonDeCommandeFille"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StBonDeCommandeFille da = new StBonDeCommandeFille();
    PageInsert pi = new PageInsert(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));

    String idBC = request.getParameter("id");
    pi.getFormu().getChamp("idbc").setDefaut(idBC);
    String lienfinaliser = "but=apresTarif.jsp&acte=finaliser&bute=stock/bc/stBonDeCommande-liste.jsp&classe=mg.cnaps.st.StBonDeCommande&id="+ idBC;
    pi.getFormu().getChamp("article").setPageAppel("choix/listeArticleChoix.jsp");
    pi.getFormu().getChamp("remarque").setLibelle("Observation");
    pi.getFormu().getChamp("remise").setDefaut("0");
    pi.getFormu().getChamp("remarque").setType("textarea");
    pi.getFormu().getChamp("idbc").setVisible(false);
    pi.getFormu().getChamp("pu").setDefaut("0");
    pi.getFormu().getChamp("etat").setVisible(false);

    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 align="center">Ajouter article</h1>
    <form action="<%=pi.getLien()%>?but=stock/mvtStock/apresMvtStock.jsp" method="post" name="appro" id="appro" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlAddTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/bc/stBonDeCommandeFille-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StBonDeCommandeFille">
        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-">
        <input name="rajoutLien" type="hidden" id="rajoutLien" value="<%=idBC%>">
        <input name="idbc" type ="hidden" id="idbc" value="<%=idBC%>">
    </form>
    <%
        StBonDeCommandeFilleLibelle p = new StBonDeCommandeFilleLibelle();
        p.setNomTable("St_BonDeCommandeFille_Libelle"); // vue
        StBonDeCommandeFilleLibelle[] liste = (StBonDeCommandeFilleLibelle[]) CGenUtil.rechercher(p, null, null, " and idbc = '" + request.getParameter("id") + "'");
        StBonDeCommande[] bc = (StBonDeCommande[])CGenUtil.rechercher(new StBonDeCommande(), null, null, " AND ID = '" + request.getParameter("id") + "'");
    %>
    <div id="selectnonee">
        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
            <thead>
                <tr class="head">
                    <th style="background-color:#bed1dd">Article</th>
                    <th style="background-color:#bed1dd">Quantit&eacute;</th>
                    <th style="background-color:#bed1dd">PU</th>
                    <th style="background-color:#bed1dd">Remise </th>
                    <th style="background-color:#bed1dd">Montant</th>
                    <th style="background-color:#bed1dd">Remarque</th>
                    <th style="background-color:#bed1dd">Action</th>
                </tr>
            </thead>
            <tbody>
                <%
                    double somme = 0;
                    double tva = 0;
                    for (int i = 0; i < liste.length; i++) {
                        double remise = ((liste[i].getQuantite() * liste[i].getPu()) * liste[i].getRemise()) / 100;
                %>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"><%=liste[i].getArticle()%> </td>
                    <td width="20%" align="center"><%=liste[i].getQuantite()%></td>
                    <td width="20%" align="center"><%=Utilitaire.formaterAr(liste[i].getPu()) %></td>
                    <td width="20%" align="center"><%=Utilitaire.formaterAr(remise)%></td>
                    <td width="20%" align="center"><%=Utilitaire.formaterAr((liste[i].getQuantite() * liste[i].getPu()) - remise)%></td>
                    
                    <td width="20%" align="center"><%=liste[i].getRemarque()%></td>
                    <td width="20%" align="center"><a href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + liste[i].getTuppleID() + "&idbc=" + request.getParameter("id")%>&acte=delete&bute=stock/bc/stBonDeCommandeFille-saisie.jsp&classe=mg.cnaps.st.StBonDeCommandeFille&rajoutLien=id" style="margin-right: 10px">annuler</a></td>
                </tr>
                <%
                    somme += ((liste[i].getQuantite() * liste[i].getPu()) - remise);
                    }
                    
                    tva = (somme * bc[0].getTva()) / 100;
                %>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"><b>Montant TVA</b></td>
                    <td width="20%" align="center"><b><%=Utilitaire.formaterAr(tva)%></b></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                </tr>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"><b>TOTAL</b></td>
                    <td width="20%" align="center"><b><%=Utilitaire.formaterAr(somme + tva)%></b></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                </tr>
            </tbody>
        </table>
        <center><a href="<%=pi.getLien()%>?<%=lienfinaliser%>" class="btn btn-success pull-center" tabindex="81">Finaliser</a></center>
    </div>
</div>
