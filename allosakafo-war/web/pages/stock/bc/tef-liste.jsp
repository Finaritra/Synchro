
<%@page import="mg.cnaps.budget.BudgetTef"%>
<%@page import="affichage.PageRecherche"%>
<% BudgetTef lv = new BudgetTef();
    lv.setNomTable("STBUDGET_TEF_LIBELLE");
    String listeCrt[] = {"id", "code", "daty", "nombeneficiaire", "cinbeneficiaire", "budget"};
    String listeInt[] = {"daty", "montant"};
    String libEntete[] = {"id", "code", "daty", "montant", "nombeneficiaire", "libelle", "budget"};
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.getFormu().getChamp("nombeneficiaire").setLibelle("Nom bénéficiaire");
    pr.getFormu().getChamp("cinbeneficiaire").setLibelle("CIN bénéficiaire");
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.getFormu().getChamp("Daty1").setLibelle("Date Min");
    
    pr.getFormu().getChamp("Daty2").setLibelle("Date Max");
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stock/bc/tef-liste.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste TEF</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/bc/tef-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <br/><br/>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/bc/tef-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "Code", "Date", "Montant", "Nom du bénéficiaire", "Libelle ", "Budget"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
