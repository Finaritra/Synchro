<%-- 
    Document   : demande-liste
    Created on : 30 oct. 2015, 15:36:07
    Author     : Jetta
--%>



<%@page import="mg.cnaps.st.StDemandeFourniture"%>
<%@page import="affichage.PageRecherche"%>

<% 
    String nomTable = "ST_DEMANDE_FOURNITURE_LIBELLE";
    StDemandeFourniture lv = new StDemandeFourniture();
    if(request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("") != 0)nomTable = request.getParameter("etat");
    lv.setNomTable(nomTable);
    String listeCrt[] = {"id", "magasin", "code_dr", "daty", "chapitre","fournisseur"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "magasin", "code_dr", "daty", "chapitre","fournisseur"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    pr.getFormu().getChamp("code_dr").setLibelle("Direction");
    pr.getFormu().getChamp("fournisseur").setLibelle("Projet");
    pr.getFormu().getChamp("daty1").setLibelle("date  min");
    pr.getFormu().getChamp("daty2").setLibelle("Date  max");
    pr.setApres("stock/fourniture/demandefourniture-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste des demandes d'achat</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/fourniture/demandefourniture-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="etat" class="champ" id="etat" onchange="changerDesignation()" >
                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_DEMANDE_FOURNITURE_LIBELLE") == 0) {%>    
                        <option value="ST_DEMANDE_FOURNITURE_LIBELLE" selected>Tous</option>
                        <% } else { %>
                        <option value="ST_DEMANDE_FOURNITURE_LIBELLE" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_DEMANDE_FOURNITURE_VALIDE") == 0) {%>
                        <option value="ST_DEMANDE_FOURNITURE_VALIDE" selected>Valid�</option>
                        <% } else { %>
                        <option value="ST_DEMANDE_FOURNITURE_VALIDE">Valid�</option>
                        <% } %>

                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_DEMANDE_FOURNITURE_EN_COURS") == 0) {%>
                        <option value="ST_DEMANDE_FOURNITURE_EN_COURS" selected>Cr��</option>
                        <% } else { %>
                        <option value="ST_DEMANDE_FOURNITURE_EN_COURS">Cr��</option>
                        <% } %>
                        
                        <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("ST_DEMANDE_FOURNITURE_ANNULEE") == 0) {%>
                        <option value="ST_DEMANDE_FOURNITURE_ANNULEE" selected>Annul�</option>
                        <% } else { %>
                        <option value="ST_DEMANDE_FOURNITURE_ANNULEE">Annul�</option>
                        <% } %>

                    </select>
                </div>
                <div class="col-md-4"></div>
            </div>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/fourniture/demandefourniture-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"ID", "Magasin", "Direction", "Date", "Chapitre","Projet"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>

