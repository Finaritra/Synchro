<%-- 
    Document   : stDemandeFournitureFille-saisie
    Created on : 30 oct. 2015, 16:06:55
    Author     : user
--%>
<%@page import="mg.cnaps.st.StDemandeFourniture"%>
<%@page import="mg.cnaps.st.StDemandeFounitureFille"%>
<%@page import="mg.cnaps.st.StBonDeCommandeFilleLibelle"%>
<%@page import="mg.cnaps.st.StBonDeCommandeFille"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    StDemandeFounitureFille da = new StDemandeFounitureFille();
    PageInsert pi = new PageInsert(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));

    pi.getFormu().getChamp("iddemande").setDefaut(request.getParameter("id"));
    pi.getFormu().getChamp("iddemande").setAutre("readonly");
    pi.getFormu().getChamp("iddemande").setLibelle("Demande");
    pi.getFormu().getChamp("codearticle").setLibelle("Code article");
    pi.getFormu().getChamp("quantite").setLibelle("Quantit�");
    pi.getFormu().getChamp("designation").setType("textarea");
    pi.getFormu().getChamp("designation").setLibelle("Observation");
    
    pi.getFormu().getChamp("codearticle").setPageAppel("choix/listeArticleChoix.jsp");
    
    StDemandeFourniture demandeFourniture = new StDemandeFourniture();
    StDemandeFourniture[] listeDemandeFourniture = (StDemandeFourniture[])CGenUtil.rechercher(demandeFourniture, null, null, " AND ID = '" + request.getParameter("id").trim() + "'");
    
    if(listeDemandeFourniture.length > 0){
		//System.out.println(" miditra ato ============= ");
        pi.getFormu().getChamp("codearticle").setApresLienPageappel(listeDemandeFourniture[0].getChapitre());
        //System.out.println(" �iditra ato ============= " + "&chapitre="+listeDemandeFourniture[0].getChapitre());
    }
    pi.preparerDataFormu();
    
    String lienfinaliser = "but=apresTarif.jsp&acte=finaliser&bute=stock/fourniture/demandefourniture-liste.jsp&classe=mg.cnaps.st.StDemandeFourniture&id="+ request.getParameter("id");
    
    
%>
<div class="content-wrapper">
    <h1 align="center">Demande de fourniture</h1>
    <form action="<%=pi.getLien()%>?but=stock/mvtStock/apresMvtStock.jsp" method="post" name="demandefourniturefille" id="demandefourniturefille" autocomplete="off" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlAddTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/fourniture/demandefourniturefille-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StDemandeFounitureFille">
        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-">
        <input name="idbc" type="hidden" id="rajoutLien" value="<%=request.getParameter("id")%>">
    </form>
    <%
        StDemandeFounitureFille p = new StDemandeFounitureFille();
        p.setNomTable("ST_DEM_FOURNITURE_LIBELLE"); // vue
        StDemandeFounitureFille[] liste = (StDemandeFounitureFille[]) CGenUtil.rechercher(p, null, null, " and iddemande = '" + request.getParameter("id") + "'");
    %>
    <div id="selectnonee">
        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
            <thead>
                <tr class="head">
                    <th style="background-color:#bed1dd">Article</th>
                    <th style="background-color:#bed1dd">Quantit&eacute;</th>
                    <th style="background-color:#bed1dd">Observation</th>
                    <th style="background-color:#bed1dd">Action</th>
                </tr>
            </thead>
            <tbody>
                <%
                    for (int i = 0; i < liste.length; i++) {
                %>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"><%=liste[i].getCodearticle()%> </td>
                    <td width="20%" align="center"><%=Utilitaire.formaterAr(liste[i].getQuantite())%></td>
                    <td width="20%" align="center"><%=liste[i].getDesignation()%></td>
                    <td width="20%" align="center">
                        <a href="<%=(String) session.getValue("lien") + "?but=stock/fourniture/demandefourniturefille-modif.jsp&id=" + liste[i].getId()%>" style="margin-right: 10px">Modifier</a>|<a href="<%=(String) session.getValue("lien") + "?but=stock/mvtStock/apresMvtStock.jsp&id=" + liste[i].getTuppleID() + "&iddemande=" + request.getParameter("id")%>&acte=deleteFille&bute=stock/fourniture/demandefourniturefille-saisie.jsp&classe=mg.cnaps.st.StDemandeFounitureFille&rajoutLien=id" style="margin-right: 10px">Supprimer</a>
                    </td>
                </tr>
                <%
                    }
                %>
            </tbody>
        </table>
            <center><a href="<%=pi.getLien()%>?<%=lienfinaliser%>" class="btn btn-success pull-center" tabindex="81">Finaliser</a></center>
    </div>
</div>
