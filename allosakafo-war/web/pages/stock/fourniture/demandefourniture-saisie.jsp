<%@page import="mg.cnaps.commun.Dr"%>
<%@page import="mg.cnaps.st.StDemandeFourniture"%>
<%@page import="bean.CGenUtil"%>
<%@page import="mg.cnaps.compta.ComptaExercice"%>
<%@page import="mg.cnaps.log.LogService"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.cnaps.formation.FormOffreFormation"%>
<%@page import="mg.cnaps.formation.FormFormation"%>
<%@page import="affichage.PageInsert"%>
<%@page import="mg.cnaps.accueil.SousPrestCat"%>
<%
    try {
        String autreparsley = "data-parsley-range='[8, 40]' required";
        StDemandeFourniture a = new StDemandeFourniture();
        PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
        pi.setLien((String) session.getValue("lien"));

        affichage.Champ[] liste = new affichage.Champ[2];
        TypeObjet d = new TypeObjet();
        d.setNomTable("log_direction_service");
        liste[0] = new Liste("code_dr", d, "VAL", "id");

        TypeObjet op = new TypeObjet();
        op.setNomTable("ST_TYPE_ARTICLE");
        liste[1] = new Liste("chapitre", op, "VAL", "id");

        pi.getFormu().changerEnChamp(liste);

        pi.getFormu().getChamp("daty").setLibelle("Date");
        pi.getFormu().getChamp("daty").setDefaut(utilitaire.Utilitaire.dateDuJour());
        pi.getFormu().getChamp("magasin").setLibelle("Magasin");
        pi.getFormu().getChamp("code_dr").setLibelle("Direction");
        pi.getFormu().getChamp("description").setLibelle("Observation");
        pi.getFormu().getChamp("fournisseur").setLibelle("Projet");
        pi.getFormu().getChamp("description").setType("textarea");
        pi.getFormu().getChamp("etat").setVisible(false);
//        stProjetChoix

        pi.getFormu().getChamp("fournisseur").setPageAppel("choix/listeProjetChoix.jsp");
        pi.getFormu().getChamp("magasin").setPageAppel("choix/magasinChoix.jsp");

        pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 align="center">Demande d'achat</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="demandefourniture" id="demandefourniture" autocomplete="off" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="stock/fourniture/demandefourniturefille-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.st.StDemandeFourniture">
    </form>
</div>
<%
} catch (Exception e) {
    e.printStackTrace();
%>
<script language="JavaScript"> alert('<%=e.getMessage()%>');
    history.back();</script>

<% }%>
