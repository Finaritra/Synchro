<%@page import="mg.allosakafo.facture.DetailsFactureGrouper"%>
<%@page import="mg.allosakafo.facture.Facture"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsLibelle"%>
<%@page import="service.AlloSakafoService"%>

<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%
    try{
    DetailsFactureGrouper dma = new DetailsFactureGrouper();
    //IDPRODUIT,PU,ID,MODEPAIEMENT,ADRESSECLIENT,DATEFACTURE,MONTANT,QTE
    String listeCrt[] = {"id", "client", "modepaiement", "adresseclient","idproduit"};
    String listeInt[] = null;
    String libEntete[] = {"id", "client", "modepaiement", "adresseclient","datefacture","montant","qte","pu","idproduit"};
    String[] colSomme = {"montant"};
    
    PageRecherche pr = new PageRecherche(dma, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.creerObjetPage(libEntete, colSomme);
    DetailsFactureGrouper[] details = (DetailsFactureGrouper[]) pr.getListe();
    System.out.println(" details : "+details.length);
    
%>

<link href="style.css" rel="stylesheet" type="text/css">

<title>Apercu facture </title>

<div class="content-wrapper">
    <section class="content" id="table-container">
        <div class="box-body table-responsive no-padding">
        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3">
            <tr>
                <td><img src="../../pho/assets/img/logo.png"  height="70"></td>
            </tr>
            <tr>
                <td><table class="table table-hover" width="100%"  align="center">
                        <tr>
                            <td> <table class="table table-hover" width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
                                  <tr>
                                        <td height="21" colspan="4" align="center"><div align="left"><%=session.getAttribute("restaurant")%><br></div></td>
                                    </tr>
                                    <tr>
                                        <td height="21" colspan="4" align="center"><div align="left">T&eacute;l : <%=Constante.tel%><br></div></td>
                                    </tr>
                                    <tr>
                                        <td height="21" colspan="4" align="center"><div align="left">STAT : <%=Constante.stat%><br></div></td>
                                    </tr>
                                    <tr>
                                        <td height="21" colspan="4" align="center"><div align="left">NIF : <%=Constante.nif%><br></div></td>
                                    </tr>
                                     <tr>
                                        <td height="21" colspan="4" align="center"><div align="left">RCS : <%=Constante.rcs%><br></div></td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><table class="table table-hover" width="100%"  align="center">
                        <tr>
                            <td> <table class="table table-hover" width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
                                    <!--DWLayoutTable-->
                                                                    <tr>
                                        <td height="21" colspan="4"  align="right">Date: <%=Utilitaire.formatterDaty(details[0].getDatefacture())%></td>
                                                                    </tr>
                                    <tr>
                                        <td width="368" height="21" colspan="4" align="center"><div align="left"><strong><font color="15" size="+2">Facture 
                                                    </font></strong><font size="+1"><%=Utilitaire.champNull(details[0].getId()) %></font><br>
                                            </div>
                                                                            </td>
                                    </tr>
                                    <tr>
                                        <td height="23" colspan="4" valign="top"><strong>Doit :</strong> <%=Utilitaire.champNull(details[0].getClient())%></td>
                                    </tr>
                                    
                                    <tr>
                                        <td><strong>Devise en :</strong> <font color="15">Ariary</font></td>
                                        <td><font color="15"><strong>Mode de paiement :  <%= Utilitaire.champNull(details[0].getModepaiement())%></strong></font></td>

                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="border-collapse: collapse;background-color:white" border="1" width="100%" align="center" cellpadding="8" cellspacing="0">

                        <tr class="headFact">
                            <td align="center" style="background-color:#bed1dd" valign="top"><strong> D&eacute;signation</strong></td>
                            <td align="center" style="background-color:#bed1dd" valign="top"><strong> Prix Unitaire</strong> </td>
                            <td align="center" style="background-color:#bed1dd" valign="top"><strong> Qantit&eacute; </strong></td>
                            <td align="center" valign="top" style="background-color:#bed1dd" ><strong> Montant</strong></td>
                        </tr>
                        <%
                            double totalmontant=0;
                            for (int i = 0; i < details.length; i++) {
                                 totalmontant+=details[i].getMontant();
                            
                        %>
                        <tr>
                            <td><%=details[i].getIdproduit()%> </td>
                            <td align="right"><font color="15"><%=Utilitaire.formaterAr(details[i].getPu()) %></font></td>
                            <td align="center"><font color="15"><%=details[i].getQte()%></font></td>
                            <td align="right"><font color="15"><%=Utilitaire.formaterAr(details[i].getMontant()) %></font></td>
                        </tr>
                        <%
                            }


                        %>
                        <tr>
                            <td colspan="2" align="right"></td>
                            <td><b>Total</b></td>
                            <td align="right"><font color="15"><%=Utilitaire.formaterAr(totalmontant)%> </font></td>
                        </tr>
                    </table>

                </td>
            </tr>


            <tr>
                <td>
                    <table class="table table-hover" align="center" width="100%">

                        <tr>
                            <td height="26" colspan="5" valign="top"> &Agrave; la somme de : <i> <font color="15"><%=Utilitaire.formaterAr(totalmontant)%> Ariary</font><font color="15"></font></i><br></td>
                        </tr>
                        <tr>
                            <td height="26" colspan="5" valign="top" align="right">
                                <table class="table table-hover" width="100%" border="0">
                                    <tr>
                                        <td><div align="left">Le Client</div></td>
                                        <td><div align="right">Le Responsable</div></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    <td align="center"> <font face="Verdana, Arial, Helvetica, sans-serif">
           </font>
                    </td>
                    </tr>
        </table>
      </div>
    </section>

    <div align="center"></div>
     <div class="box-body" style="display: block;">
                            <div class="row" style="text-align: center">
                                <label>Exporter pdf :</label> 
                                <button id="btnPrint">                       
                                    <img src="../dist/img/file_pdf.png" alt="pdf-export">
                                </button>  
                            </div>
     </div>
</div>
<script type="text/javascript">
    $("#btnPrint").click(function () {
        var divContents = $("#table-container").html();
        
        var printWindow = window.open('', '', 'height=400,width=800');
        printWindow.document.write('<html>');
        printWindow.document.write('<body>');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
    });
</script>
<% }catch(Exception ex){
    ex.printStackTrace();
}%>