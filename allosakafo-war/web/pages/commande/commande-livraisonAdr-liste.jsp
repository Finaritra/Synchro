<%-- 
    Document   : commande-livraisonAdr-liste
    Created on : 12 mars 2020, 17:50:45
    Author     : Maharo R.
--%>


<%@page import="bean.CGenUtil"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.livraison.CommandeLivraisonLibAdresse"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="mg.allosakafo.fin.Caisse"%>

<%
    try {

        CommandeLivraisonLibAdresse lv = new CommandeLivraisonLibAdresse();
        String nomTable = "commandelivraisonlible";
        String name = request.getParameter("table");
        if (name != null && name.compareTo("") != 0) {
            nomTable = name;
        }
        lv.setNomTable(nomTable);

        String listeCrt[] = {"id", "idLivraison", "idmere", "produit", "idAccompagnementSauce", "quantite", "pu", "remise", "adresse", "idpoint"};
        String listeInt[] = {"quantite", "pu"};
        String libEntete[] = {"id", "idmere", "produit", "idAccompagnementSauce", "quantite", "pu", "remise", "adresse", "idpoint"};

        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));

        pr.setApres("commande/commande-livraisonAdr-liste.jsp");

        String[] colSomme = {};
        pr.creerObjetPage(libEntete, colSomme);
        Caisse c = new Caisse();
        Caisse[] listeCaisse = c.getAllCaisse(null);
        CommandeLivraisonLibAdresse[] liste = (CommandeLivraisonLibAdresse[]) pr.getTableau().getData();
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content">
        <div class="row" style="padding-top: 15px;">
            <h1>Liste commande livraison</h1>
        </div>
        <div class="row">
            <form action="<%=pr.getLien()%>?but=commande/commande-livraisonAdr-liste.jsp" method="post" name="incident" id="incident">
                <%
                    out.println(pr.getFormu().getHtmlEnsemble());
                %>
                <div class="row col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        Adresse ou Livraison : 
                        <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                            <% if (nomTable != null && nomTable.compareToIgnoreCase("commandelivraisonlible") == 0) {%>    
                            <option value="commandelivraisonlible" selected>A domicile</option>
                            <% } else { %>
                            <option value="commandelivraisonlible" >A domicile</option>
                            <% } %>
                            <% if (nomTable != null && nomTable.compareToIgnoreCase("commandelivraisonliblead") == 0) {%>    
                            <option value="commandelivraisonliblead" selected>Point </option>
                            <% } else { %>
                            <option value="commandelivraisonliblead" >Point</option>
                            <% }%>


                        </select>
                    </div>
                </div>

            </form>
        </div>
        <div class="row">                
            <form action="<%=pr.getLien()%>?but=modifierEtatMultiple.jsp" method="post" name="incident" id="incident">
                <% if (liste.length > 0) {%>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" align="center">LISTE</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                                <thead>
                                    <tr class="head">
                                        <th align="center" valign="top" style="background-color:#bed1dd">
                                            <input onclick="CocheToutCheckbox(this, 'id')" type="checkbox">
                                        </th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Id</th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Livraison</th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Produit</th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Accompagnement sauce</th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Quantit&eacute;</th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">PU</th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Remise</th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Priorit&eacute;</th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Adresse</th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Point</th>
                                        <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Etat</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        for (int i = 0; i < liste.length; i++) {
                                    %>
                                    <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''" style="">
                                        <td align="center">
                                            <input type="checkbox" value="<%=liste[i].getId()%>" name="id" id="checkbox0">
                                        </td>
                                        <td width="14%" align="center">
                                            <a href="/phobo/pages/module.jsp?but=commande/commande-livraison-fiche.jsp&id=<%=liste[i].getId()%>"><%=liste[i].getId()%></a>
                                        </td>

                                        <td width="14%" align="center"><a href="/phobo/pages/module.jsp?but=commande/commande-livraison-fiche.jsp&id=<%=liste[i].getId()%>"><%=liste[i].getIdLivraison()%></a></td>
                                        <td width="14%" align="center"><%=liste[i].getProduit()%></td>
                                        <td width="14%" align="center"><%=liste[i].getIdAccompagnementSauce()%></td>
                                        <td width="14%" align="center"><%=liste[i].getQuantite()%></td>
                                        <td width="14%" align="center"><%=liste[i].getPu()%></td>
                                        <td width="14%" align="center"><%=liste[i].getRemise()%></td>
                                        <td width="14%" align="center"><%=liste[i].getPrioriter()%></td>
                                        <td width="14%" align="center"><%=liste[i].getAdresse()%></td>
                                        <td width="14%" align="center"><%=liste[i].getIdpoint()%></td>
                                        <td width="14%" align="center"><%=liste[i].getEtat()%></td>

                                    </tr>
                                    <%}%>
                                <input type="hidden" name="ids" value="null">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="bute" value="commande/commande-livraison-liste.jsp"/>
                <%}%>
                <%if (nomTable.compareTo("commandelivraisonliblAcloturer") == 0) {%>
                <input type="hidden" name="bute" value="commande/commande-livraison-liste.jsp">
                <button class="btn btn-success" type="submit" name="acte" value="cloturer">Cloturer</button>
                <button class="btn btn-danger" type="submit" name="acte" value="annuler">Annuler</button>
                <% } %>
                <%
                    out.println(pr.getBasPage());
                %>
            </form>
        </div>
    </section>    


</div>

<%    } catch (Exception e) {
        e.printStackTrace();
    }
%>
