<%-- 
    Document   : as-livraison-liste
    Created on : 1 d�c. 2016, 14:21:35
    Author     : Joe
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.commande.Livraisondetail"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="user.UserEJB"%>
<%@page import="lc.Direction"%>

<% 
    Livraisondetail lv = new Livraisondetail();
   
    String listeCrt[] = {"id","nomtable", "produit","restaurant","heureCommande","heureLivraison","dateCommande","dateLivraison"};
    String listeInt[] = {"dateCommande","dateLivraison"};
    String libEntete[] = {"id", "nomtable", "produit", "sauceAcoompagnement", "dateCommande", "heureCommande", "dateLivraison", "heureLivraison","restaurantlib","difference"};
        
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    String apreswhere = "";
    if(request.getParameter("dateCommande1") == null || request.getParameter("dateCommande2") == null){
        apreswhere = "and dateCommande = '"+utilitaire.Utilitaire.dateDuJour()+"'";
    }
    if(request.getParameter("restaurant") == null){
        apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
    }
    
    pr.setAWhere(apreswhere);
    
    affichage.Champ[] listePoint = new affichage.Champ[1];
    TypeObjet liste1 = new TypeObjet();
    liste1.setNomTable("point");
    listePoint[0] = new Liste("restaurant", liste1, "val", "id");
    pr.getFormu().changerEnChamp(listePoint);


    pr.getFormu().getChamp("restaurant").setDefaut(session.getAttribute("restaurant").toString());
    
    pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("datecommande2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    
    pr.setApres("commande/as-livraison-liste.jsp");
    String[] colSomme = {"difference"};
    pr.setNpp(300);
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste livraison</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-livraison-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Idd", "Table", "Produit", "Sauce Acoompagnement", "Date Commande", "Heure Commande", "Date Livraison", "Heure Livraison","Restaurant","Difference"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            //out.println(pr.getBasPage());
        %>
    </section>
</div>