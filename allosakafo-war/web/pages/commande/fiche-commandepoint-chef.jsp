<%-- 
    Document   : fiche-commande-chef
    Created on : 29 d�c. 2016, 05:35:45
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetails"%>
<style>
.wrapper {
   width: 100%; height: 50% ;  border: 1px solid black;
   -webkit-transition: all .3s ease-out;
   -moz-transition: all .3s ease-out;
   -o-transition: all .3s ease-out;
   transition: all .3s ease-out;
}
.wrapper2 {
   width: 100%; height: 100% ;  border: 1px solid black;
   -webkit-transition: all .3s ease-out;
   -moz-transition: all .3s ease-out;
   -o-transition: all .3s ease-out;
   transition: all .3s ease-out;
}
.zoom:hover {
   -moz-transform: scale(2);
   -webkit-transform: scale(2);
   -o-transform: scale(2);
   transform: scale(2);
   -ms-transform: scale(2);
filter: progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand',
   M11=2, M12=-0, M21=0, M22=2);
   }
.recu{
	margin-left: 5px;
	width: 95%;
	height: 350px;
	margin-top:0px;
}
.emargement{
	margin-top:2px;
	width:95%;
}
.table-entete th, p{
	font-size:small;
}
.table-corps th, td{
	font-size: x-small;
}

.table-cuisinier td{
	font-size: medium;
}

.wrapper3 {
   width: 100%; height: 100% ; border: 1px solid black;
   -webkit-transition: all .3s ease-out;
   -moz-transition: all .3s ease-out;
   -o-transition: all .3s ease-out;
   transition: all .3s ease-out;
}
.etiquette td{
	-webkit-transform: rotate(270deg); /* Safari and Chrome */
    -moz-transform: rotate(270deg);   /* Firefox */
    -ms-transform: rotate(270deg);   /* IE 9 */
    -o-transform: rotate(270deg);   /* Opera */
    transform: rotate(270deg);
}
.etiquette img{
	width:101px; 
	height:75px;
}

.etiquette p{
	font-size:x-small;
}
</style>
<%
    CommandeClient dma = new CommandeClient();
    double total = 0.0;
    dma.setNomTable("as_commandepoint_libelle");
    PageConsulte pc = new PageConsulte(dma, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    
    pc.setTitre("Fiche commande");
    
    CommandeClient bondecommande = (CommandeClient)pc.getBase();
	
	CommandeClientDetails p = new CommandeClientDetails();
    p.setNomTable("as_detailscommande_lib"); // vue
    CommandeClientDetails[] liste = (CommandeClientDetails[]) CGenUtil.rechercher(p, null, null, " and idmere = '" + request.getParameter("id") + "'");
	
	CommandeClientDetails aliment =  new CommandeClientDetails();
	aliment.setNomTable("as_detailscommande_aliment");
    CommandeClientDetails[] listesakafo =  (CommandeClientDetails[]) CGenUtil.rechercher(aliment, null, null, " and idmere = '" + request.getParameter("id") + "'");
	
	int qteSakafoTotal = 0;
	for (int a=0; a<listesakafo.length; a++){
		if (listesakafo[a].getQuantite()<1) qteSakafoTotal++;
		qteSakafoTotal += (int) listesakafo[a].getQuantite();
	}
	
	CommandeClientDetails[] listeS = new CommandeClientDetails[qteSakafoTotal];
	int compteur = 0;
	for (int a=0; a<listesakafo.length; a++){
		if (listesakafo[a].getQuantite()<1){
			listeS[compteur] = (CommandeClientDetails)listesakafo[a].clone();
			listeS[compteur].setQuantite(1);
			compteur ++;
		}
		for (int b=0; b< (int)listesakafo[a].getQuantite(); b++){
			listeS[compteur] = (CommandeClientDetails)listesakafo[a].clone();
			listeS[compteur].setQuantite(1);
			compteur ++;
		}
	}
	
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Allo Sakafo 1.0</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini" background-color="#3c8dbc">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-4">
                    <div class="box-fiche" style="margin-left:0px; min-width:425px">
                        <div class="box recu">
                            <div class="box-title with-border">
								<table class="table-entete" width="95%" align="center">
									<tr>
										<td rowspan=2><img src="${pageContext.request.contextPath}/dist/img/logo.jpg" width="126.4px" height="78.4px"></td>
										<td><p>BL/Re&ccedil;u</p></td>
										<td align="right"  colspan=2><p class="box-title" align="right">Ref commande : <%=bondecommande.getId()%></p></td>
									</tr>
									<tr>
										<td><p>Livraison &agrave; : <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p></td>
										<td><p><%=Utilitaire.formatterDaty(bondecommande.getDateliv())%> <%=Utilitaire.champNull(bondecommande.getHeureliv())%></p></td>
									</tr>
									<tr>
										<td colspan=2><p class="box-title">Doit  &agrave; : <%=Utilitaire.champNull(bondecommande.getRemarque())%></p></td>
										<td><p class="box-title"><%=Utilitaire.champNull(bondecommande.getClient())%></p></td>
									</tr>
								</table>
                            </div>
                            <div class="box-body">
                                <table class="table-corps" width="95%" align="center" border="1px solid black" style="border-collapse:collapse">
                                    <thead>
                                        <tr>
                                            <th><strong>Designation</strong></th>
                                            <th><strong>Quantit&eacute;</strong></th>
											<th><strong>PU (AR)</strong></th>
											<th><strong>Montant (AR)</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            
                                            for (int i = 0; i < liste.length; i++) {
												total += liste[i].getMontant();
                                        %>
                                        <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                            <td  align="left"><%=liste[i].getProduit()%></td>
                                            <td  align="right"><%=Utilitaire.formaterAr(liste[i].getQuantite())%></td>
											<td  align="right"><%=Utilitaire.formaterAr(liste[i].getPu())%></td>
                                            <td  align="right"><%=Utilitaire.formaterAr(liste[i].getMontant())%></td>
                                        </tr>
                                        <%}%>
										<tr>
											<td align="right" colspan=3><strong>Total</strong></td>
											<td align="right"><strong><%=Utilitaire.formaterAr(total)%></strong></td>
										</tr>
                                    </tbody>
                                </table>
								<table class="emargement"  align="center">
									<tr>
										<td align="left"><p>Le client</p></td>
										<td align="right"><p>Le responsable</p></td>
									</tr>
								</table>
								<br>
								<br>
								<p align="center">Tel : 033 33 300 30 / 032 07 300 30 / 034 11 300 30</p>
                            </div>
						</div>
						<div class="box recu">
                            <div class="box-title with-border">
								<table class="table-entete"  width="95%" align="center">
									<tr>
										<td rowspan=2><img src="${pageContext.request.contextPath}/dist/img/logo.jpg" width="126.4px" height="78.4px"></td>
										<td><p>BL/Re&ccedil;u</p></td>
										<td align="right"  colspan=2><p class="box-title" align="right">Ref commande : <%=bondecommande.getId()%></p></td>
									</tr>
									<tr>
										<td><p>Livraison &agrave; : <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p></td>
										<td><p><%=Utilitaire.formatterDaty(bondecommande.getDateliv())%> <%=Utilitaire.champNull(bondecommande.getHeureliv())%></p></td>
									</tr>
									<tr>
										<td colspan=2><p class="box-title">Doit  &agrave; : <%=Utilitaire.champNull(bondecommande.getRemarque())%></p></td>
										<td><p class="box-title"><%=Utilitaire.champNull(bondecommande.getClient())%></p></td>
									</tr>
								</table>
                                
                            </div>
                            <div class="box-body">
                                <table class="table-corps" width="95%" align="center" border="1px solid black" style="border-collapse:collapse">
                                    <thead>
                                        <tr>
                                            <th><strong>Designation</strong></th>
                                            <th><strong>Quantit&eacute;</strong></th>
											<th><strong>PU (AR)</strong></th>
											<th><strong>Montant (AR)</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            for (int i = 0; i < liste.length; i++) {
                                        %>
                                        <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                            <td  align="left"><%=liste[i].getProduit()%></td>
                                            <td  align="right"><%=Utilitaire.formaterAr(liste[i].getQuantite())%></td>
											<td  align="right"><%=Utilitaire.formaterAr(liste[i].getPu())%></td>
                                            <td  align="right"><%=Utilitaire.formaterAr(liste[i].getMontant())%></td>
                                        </tr>
                                        <%}%>
										<tr>
											<td align="right" colspan=3><strong>Total</strong></td>
											<td align="right"><strong><%=Utilitaire.formaterAr(total)%></strong></td>
										</tr>
                                    </tbody>
                                </table>
								
								<table class="emargement"  align="center">
									<tr>
										<td align="left"><p>Le client</p></td>
										<td align="right"><p>Le responsable</p></td>
									</tr>
								</table>
								<br>
								<br>
								<p align="center">Tel : 033 33 300 30 / 032 07 300 30 / 034 11 300 30</p>
                            </div>
						</div>
                    </div>
                </div>
            </div>
		</div>
		<div class="wrapper2">
			<div class="row">
                <div class="col-md-4">
                    <div class="box-fiche" style="margin-left:0px; min-width:425px">
						<div class="box">
                            <div class="box-title with-border">
								<table  width="95%" align="center">
									<tr>
										<td><h3>Fiche de commande</h3></td>
										<td align="right"  colspan=2><h4 class="box-title" align="right">Ref commande : <%=bondecommande.getId()%></h4></td>
									</tr>
									<tr>
										<td><p class="box-title">Doit  &agrave; : <%=Utilitaire.champNull(bondecommande.getRemarque())%></p></td>
										<td><p>Livraison &agrave; : <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p></td>
									</tr>
									<tr>
										<td><p>Date : <%=Utilitaire.formatterDaty(bondecommande.getDateliv())%></p></td>
										<td><h5>Heure : <%=Utilitaire.champNull(bondecommande.getHeureCuisine())%></h5></td>
									</tr>
								</table>
                                
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered table-cuisinier" width="95%" align="center" border="1px solid black" style="border-collapse:collapse">
                                    <thead>
                                        <tr>
                                            <th><strong>Designation</strong></th>
                                            <th><strong>Quantit&eacute;</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            for (int i = 0; i < listesakafo.length; i++) {
                                        %>
                                        <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                            <td align="left"><%=Utilitaire.champNull(listesakafo[i].getProduit())%></td>
                                            <td  align="right"><%=listesakafo[i].getQuantite()%></td>
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
								
								<table class="emargement"  align="center">
									<tr>
										<td align="left"><p>Le cuisinier</p></td>
										<td align="right"><p>Le responsable</p></td>
									</tr>
								</table>
								<br>
								<br>
                            </div>
						</div>
					</div>
				</div>
			</div>
        </div>
		<% 
			int count = listeS.length;
			int nbct = 0;
			int nbtab = count/6;
			for (int nb=0; nb < nbtab; nb++){ 
		%>
		<div class="wrapper3">
			<div class="col-md-4">
                <div class="box-fiche" style="margin-left:0px; min-width:425px">
					<div class="box etiquette">
						<table height="99%" width="100%" border="1px solid black">
							<tr height="50%">
								<td width="33%">
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
								</td>
								<td width="33%">
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct+1].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
								</td>
								<td width="33%">
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct+2].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
								</td>
							</tr>
							<tr height="50%">
								<td width="33%">
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct+3].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
								</td>
								<td width="33%">
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct+4].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
								</td>
								<td width="33%">
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct+5].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
								</td>
							</tr>
						</table>
						
					</div>
				</div>
			</div>
		</div>
		<% 
			nbct = nbct+6;
		} 
		%>
		<% 
			int reste = count - nbtab * 4; 
			if (reste > 0){
		%>
			<div class="wrapper3">
			<div class="col-md-4">
                <div class="box-fiche" style="margin-left:0px; min-width:425px">
					<div class="box etiquette">
						<table height="99%" width="100%" border="1px solid black">
							<tr height="50%">
								<td width="33%">
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
								</td>
								<td width="33%">
									<% if (reste > 1) { %>
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct+1].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
									<% } %>
								</td>
								<td width="33%">
									<% if (reste > 2) { %>
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct+2].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
									<% } %>
								</td>
							</tr>
							<tr height="50%">
								<td width="33%">
									<% if (reste > 3) { %>
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct+3].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
									<% } %>
								</td>
								<td width="33%">
									<% if (reste > 4) { %>
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct+4].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
									<% } %>
								</td>
								<td width="33%">
									<% if (reste > 5) { %>
									<center>
									<h5><%=bondecommande.getId()%></h5>
									<h5><%=Utilitaire.champNull(listeS[nbct+5].getProduit())%></h5>
									<p><%=Utilitaire.champNull(bondecommande.getRemarque())%> &agrave; <%=Utilitaire.champNull(bondecommande.getAdresseliv())%></p>
									</center>
									<% } %>
								</td>
							</tr>
						</table>
						
					</div>
				</div>
			</div>
		</div>
		<%
			}
		%>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>