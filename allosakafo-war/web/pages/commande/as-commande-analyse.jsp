<%-- 
    Document   : as-commande-analyse
    Created on : 30 d�c. 2016, 04:57:15
    Author     : Joe
--%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsRestaurant"%>
<%@ page import="user.*" %>
<%@page import="affichage.Liste"%>
<%@ page import="bean.*" %>
<%@page import="bean.TypeObjet"%>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.commande.AnalyseCommande"%>
<%@ page import="affichage.*" %>
<%@page import="lc.Direction"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

<%
try{    
    CommandeClientDetailsRestaurant mvt = new CommandeClientDetailsRestaurant();
    
    String nomTable = "vue_cmd_client_details_rest";
    if(request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0){
        nomTable = request.getParameter("table");
    }
    mvt.setNomTable(nomTable);
    
    String listeCrt[] = {"datecommande", "nomTable", "produit","restaurant", "typeproduit", "heureliv"};
    String libEntete[] = {"Id", "Table", "Type", "Plat", "Heure", "Date", "Quantite", "Prix Unitaire", "Montant"};
    String listeInt[] = {"datecommande", "heureliv"};
    String[] pourcentage = {"nombrepargroupe"};
    String colDefaut[] = {"id", "nomtable", "typeproduit", "produit", "heureliv", "datecommande"}; 
    String somDefaut[] = {"quantite", "pu", "montant"};
    
    PageRechercheGroupe pr = new PageRechercheGroupe(mvt, request, listeCrt, listeInt, 3,colDefaut, somDefaut, pourcentage, 9, 2);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    
    affichage.Champ[] listePoint = new affichage.Champ[1];
    TypeObjet liste1 = new TypeObjet();
    liste1.setNomTable("point");
    listePoint[0] = new Liste("restaurant", liste1, "val", "id");
    pr.getFormu().changerEnChamp(listePoint);
    pr.getFormu().getChamp("restaurant").setDefaut(session.getAttribute("restaurant").toString());
            
//    affichage.Champ[] liste = new affichage.Champ[2];
//    
//    Direction dir = new Direction();
//    dir.setNomTable("restaurant");
//    liste[0] = new Liste("restaurant",dir,"libelledir","iddir");
//    
//    TypeObjet ou1 = new TypeObjet();
//    ou1.setNomTable("as_typeproduit");
//    liste[1] = new Liste("typeproduit", ou1, "VAL", "VAL");
//    
//    pr.getFormu().changerEnChamp(liste);
//    
    //pr.getFormu().getChamp("restaurant").setDefaut((String)session.getAttribute("restaurant"));
    
    String apreswhere = "";
    if(request.getParameter("datecommande1") == null || request.getParameter("datecommande2") == null){
        apreswhere = "and datecommande = '"+utilitaire.Utilitaire.dateDuJour()+"'";
    }
    if(request.getParameter("restaurant") == null){
        apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
    }
    
    pr.setAWhere(apreswhere);
    pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("datecommande2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.setNpp(500);
    pr.setApres("commande/as-commande-analyse.jsp");
    pr.creerObjetPage();
%>
<script>
    function changerDesignation() {
        document.analyse.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Analyse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-commande-analyse.jsp" method="post" name="analyse" id="analyse">
            <%out.println(pr.getFormu().getHtmlEnsemble());%>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    table : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_client_details_rest") == 0) {%>    
                        <option value="vue_cmd_client_details_rest" selected>Tous</option>
                        <% } else { %>
                        <option value="vue_cmd_client_details_rest" >Tous</option>
                        <% } %>
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_clt_dtls_typeP_cloture") == 0) {%>    
                        <option value="vue_cmd_clt_dtls_typeP_cloture" selected>Clotur&eacute;</option>
                        <% } else { %>
                        <option value="vue_cmd_clt_dtls_typeP_cloture" >Clotur&eacute;</option>
                        <% } %>
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_clt_dtls_typeP_fait") == 0) {%>
                        <option value="vue_cmd_clt_dtls_typeP_fait" selected>Pr&ecirc;t &agrave; livrer</option>
                        <% } else { %>	
                        <option value="vue_cmd_clt_dtls_typeP_fait">Pr&ecirc;t &agrave; livrer</option>
                        <% } %>

                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_clt_dtls_typeP_valide") == 0) {%>
                        <option value="vue_cmd_clt_dtls_typeP_valide" selected>Valid&eacute;</option>
                        <% } else { %>
                        <option value="vue_cmd_clt_dtls_typeP_valide">Valid&eacute;</option>
                        <% } %>
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_clt_dtls_typeP_livre") == 0) {%>
                        <option value="vue_cmd_clt_dtls_typeP_livre" selected>Livr&eacute;</option>
                        <% } else { %>
                        <option value="vue_cmd_clt_dtls_typeP_livre">Livr&eacute;</option>
                        <% } %>
						
			<% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_clt_dtls_typeP_paye") == 0) {%>
                        <option value="vue_cmd_clt_dtls_typeP_paye" selected>Pay&eacute;</option>
                        <% } else { %>
                        <option value="vue_cmd_clt_dtls_typeP_paye">Pay&eacute;</option>
                        <% } %>

                    </select>
                </div>
                <div class="col-md-4"></div>
            </div>
        </form>
        <%
           out.println(pr.getTableauRecap().getHtml());%>
           <%
            String lienTableau[] = {pr.getLien() + "?but=commande/as-commande-liste-etat.jsp",pr.getLien() + "?but=commande/as-commande-liste-etat.jsp",pr.getLien() + "?but=commande/as-commande-liste-etat.jsp"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(somDefaut);%>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%
    }catch(Exception e){
        e.printStackTrace();
    }
%>