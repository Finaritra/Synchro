<%-- 
    Document   : as-paiement-liste
    Created on : 1 d�c. 2016, 15:42:37
    Author     : Joe
--%>

<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.PaiementRestaurant"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%@page import="lc.Direction"%>

<% 
    PaiementRestaurant lv = new PaiementRestaurant();
    //lv.setNomTable("AS_PAIEMENT_LIBELLE_TABLE");
    
    String listeCrt[] = {"daty", "observation", "modepaiement", "client", "caisse", "restaurant"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "observation", "montant", "numpiece", "modepaiement", "client", "caisse"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 4, libEntete, 8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    pr.getFormu().getChamp("daty1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("daty2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    
    String apreswhere = "";
    if(request.getParameter("daty1") == null || request.getParameter("daty2") == null){
        apreswhere = "and daty = '"+utilitaire.Utilitaire.dateDuJour()+"'";
    }
    if(request.getParameter("restaurant") == null){
        apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
    }
    
    pr.setAWhere(apreswhere);
    
    pr.setApres("commande/as-paiement-liste.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste paiement</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-paiement-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=commande/as-paiement-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "Observation", "Montant", "N� Pi�ce", "Mode de paiement", "R�f. client", "Caisse"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>