<%-- 
    Document   : as-livraison-fiche
    Created on : 1 d�c. 2016, 14:21:55
    Author     : Joe
--%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsLibelle"%>
<%@page import="mg.allosakafo.livraison.CommandeLivraisonLib"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.livraison.CommandeLivraison"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetails"%>
<%
    try {
        CommandeLivraison a = new CommandeLivraison();
        a.setNomTable("commandelivraisonlib");
        PageConsulte pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
        pc.getChampByName("idclient").setLibelle("Client");
        pc.getChampByName("idpoint").setLibelle("Point");
        pc.setTitre("Commande Livraison");
 

%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=commande/commande-livraison-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer" >
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annuler&classe=mg.allosakafo.livraison.CommandeLivraison&bute=commande/commande-livraison-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=cloturer&classe=mg.allosakafo.livraison.CommandeLivraison&bute=commande/commande-livraison-fiche.jsp&id=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Viser</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
</div>
<%} catch (Exception e) {
        e.printStackTrace();
    }%>