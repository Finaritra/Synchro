<%-- 
    Document   : as-commande-saisie
    Created on : 1 d�c. 2016, 09:51:55
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.commande.CommandeMobile"%>
<%@page import="mg.allosakafo.produits.ProduitsLibelle"%>
<%@page import="mg.allosakafo.tiers.Responsable"%>
<%@page import="mg.allosakafo.appel.Appel"%>
<%@page import="mg.allosakafo.secteur.Quartier"%>

<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    CommandeMobile da = new CommandeMobile();
    PageInsert pi = new PageInsert(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");


    affichage.Champ[] liste = new affichage.Champ[3];

    TypeObjet d = new TypeObjet();
    d.setNomTable("as_typecommande");
    liste[0] = new Liste("typecommande", d, "val", "id");

    Responsable dd = new Responsable();
    dd.setNomTable("as_responsable");
    liste[1] = new Liste("responsable", dd, "nom", "id");


    TypeObjet tt = new TypeObjet();
    tt.setNomTable("tables");
    liste[2] = new Liste("tables", tt, "val", "id");
        

    pi.getFormu().changerEnChamp(liste);

    pi.getFormu().getChamp("etat").setVisible(false);
    pi.getFormu().getChamp("datesaisie").setLibelle("date de saisie");
    pi.getFormu().getChamp("datesaisie").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("datecommande").setLibelle("Date de commande");
    pi.getFormu().getChamp("datecommande").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("responsable").setLibelle("R�sponsable");
    pi.getFormu().getChamp("numcommande").setLibelle("N� commande");
    pi.getFormu().getChamp("numcommande").setDefaut(Utilitaire.getMaxSeq("getSeqNumCommande")+"");
    pi.getFormu().getChamp("typecommande").setLibelle("Type commande");
    pi.getFormu().getChamp("remarque").setType("textarea");
    pi.getFormu().getChamp("observation").setType("textarea");
    
    String[] ord = {"datesaisie", "numcommande", "responsable", "tables", "typecommande", "datecommande"};
    pi.setOrdre(ord);
    
    pi.preparerDataFormu();
%>

<div class="content-wrapper">
    <h1 class="box-title">Enregistrer commande</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertCommande();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <div class="box table-responsive">
                        <div class="box-title with-border">
                            <h1 class="box-title">D&eacute;tails</h1>
                        </div>
                        <div class="box-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th style="background-color:#bed1dd">ID</th>
                                        <th style="background-color:#bed1dd">Article</th>
                                        <th style="background-color:#bed1dd">Quantit&eacute;</th>
                                        <th style="background-color:#bed1dd">Prix Unitaire</th>
                                        <th style="background-color:#bed1dd">Montant</th>
                                        <th style="background-color:#bed1dd">Remarque</th>
                                    </tr></thead>
                                <tbody>
                                    <%
                                        ProduitsLibelle p = new ProduitsLibelle();
                                        ProduitsLibelle[] listef = (ProduitsLibelle[]) CGenUtil.rechercher(p, null, null, " order by nom asc");

                                        for (int i = 0; i < listef.length; i++) {
                                    %>
                                    <input type="hidden" class="form form-control" name="nb" id="nb" value="<%=listef.length%>">
                                    <tr>
                                        <td><input type="hidden" class="form form-control" name="id<%=i%>" id="id<%=i%>" value="<%=listef[i].getId()%>"><%=listef[i].getId()%></td>
                                        <td><%=listef[i].getNom()%></td>
                                        <td><input type="text" class="form form-control" name="quantite<%=i%>" id="quantite<%=i%>" value="0" onblur="calculerMontant(<%=i%>)"></td>
                                        <td><input type="text" class="form form-control" name="pu<%=i%>" id="pu<%=i%>" value="<%=listef[i].getPu()%>" onblur="calculerMontant(<%=i%>)"></td>
                                        <td><input type="text" class="form form-control" name="montant<%=i%>" id="montant<%=i%>" value="0" readonly="readonly"></td>
                                        <td><input type="text" class="form form-control" name="remarque<%=i%>" id="remarque<%=i%>"></td>
                                    </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input name="acte" type="hidden" id="nature" value="insertcommandemobile">
        <input name="bute" type="hidden" id="bute" value="commande/as-commande-mobile-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.commande.CommandeMobile">
        <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;">Enregistrer</button>
        <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;">R&eacute;initialiser</button>
    </form>
</div>
<script language="JavaScript">

    var options = $('#typecommande option');
    for (var co = 0; co < options.size(); co++) {
        if (options[co].value === 'TPC00002') {
            $('#typecommande option')[co].selected = true;
        } else {
            $('#typecommande option')[co].selected = false;
        }

    }
    
    function calculerMontant(indice) {
        var quantite, pu, montant;
        quantite = parseFloat($('#quantite' + indice).val());
        pu = parseFloat($('#pu' + indice).val());
        if (!isNaN(quantite) && !isNaN(pu)) {
            montant = quantite * pu;
            $('#montant' + indice).val(montant.toFixed(2));
        } else {
            $('#montant' + indice).val('');
        }
    }

</script>