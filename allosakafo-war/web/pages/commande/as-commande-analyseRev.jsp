<%-- 
    Document   : as-commande-analyse
    Created on : 30 d�c. 2016, 04:57:15
    Author     : Joe
--%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsRestaurant"%>
<%@ page import="user.*" %>
<%@page import="affichage.Liste"%>
<%@ page import="bean.*" %>
<%@page import="bean.TypeObjet"%>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.commande.AnalyseCommande"%>
<%@ page import="affichage.*" %>
<%@page import="lc.Direction"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

<%
try{    
    CommandeClientDetailsRestaurant mvt = new CommandeClientDetailsRestaurant();
    
    String nomTable = "as_cmdDetIng";
    if(request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0){
        nomTable = request.getParameter("table");
    }
    mvt.setNomTable(nomTable);
    
    String listeCrt[] = {"datecommande","libelle","categorieIngredient", "nomTable", "produit","restaurant", "heureliv"};
    String listeInt[] = {"datecommande", "heureliv"};
    String[] pourcentage = {"nombrepargroupe"};
    String colDefaut[] = {"libelle"}; 
    String somDefaut[] = {"revient", "qteIng"};
    
    PageRechercheGroupe pr = new PageRechercheGroupe(mvt, request, listeCrt, listeInt, 3,colDefaut, somDefaut, pourcentage, 9, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    

    
    String apreswhere = "";
    if(request.getParameter("datecommande1") == null || request.getParameter("datecommande2") == null){
        apreswhere = "and datecommande = '"+utilitaire.Utilitaire.dateDuJour()+"'";
    }
    if(request.getParameter("restaurant") == null){
        apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
    }
    
    pr.setAWhere(apreswhere);
    pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("datecommande2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.setNpp(500);
    pr.setApres("commande/as-commande-analyseRev.jsp");
    pr.creerObjetPage();
%>
<script>
    function changerDesignation() {
        document.analyse.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Analyse REVIENT INGREDIENT</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-commande-analyseRev.jsp" method="post" name="analyse" id="analyse">
            <%out.println(pr.getFormu().getHtmlEnsemble());%>
			<div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    table : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_cmdDetIng") == 0) {%>    
                        <option value="as_cmdDetIng" selected>Tous</option>
                        <% } else { %>
                        <option value="as_cmdDetIng" >Tous</option>
                        <% } %>
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_cmdDetIngNonCloture") == 0) {%>
                        <option value="as_cmdDetIngNonCloture" selected>Non cloture</option>
                        <% } else { %>
                        <option value="as_cmdDetIngNonCloture">Non Cloture</option>
                        <% } %>
						
			<% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_clt_dtls_typeP_paye") == 0) {%>
                        <option value="as_cmdDetIngCloture" selected>Cloture</option>
                        <% } else { %>
                        <option value="as_cmdDetIngCloture">Cloture</option>
                        <% } %>

                    </select>
                </div>
                <div class="col-md-4"></div>
            </div>
        </form>
        <%
           out.println(pr.getTableauRecap().getHtml());%>
           <%
            String lienTableau[] = {pr.getLien() + "?but=commande/as-commande-liste-etat.jsp",pr.getLien() + "?but=commande/as-commande-liste-etat.jsp",pr.getLien() + "?but=commande/as-commande-liste-etat.jsp"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(somDefaut);%>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%
    }catch(Exception e){
        e.printStackTrace();
    }
%>