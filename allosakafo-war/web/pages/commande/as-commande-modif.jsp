<%-- 
    Document   : as-commande-modif.jsp
    Created on : 29 d?c. 2016, 19:50:47
    Author     : Joe
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="data.CommandeModifData"%>
<%@ page import="user.*"%>
<%@ page import="bean.*"%>
<%@ page import="utilitaire.*"%>
<%@ page import="affichage.*"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsLibelle"%>
<%@page import="mg.allosakafo.tiers.Responsable"%>
<%@page import="mg.allosakafo.appel.Appel"%>
<%
    try{
    String autreparsley = "data-parsley-range='[8, 40]' required";
    CommandeClient da = new CommandeClient();
    da.setNomTable("VUE_COMMANDE_CLIENT");
    PageUpdate pi = new PageUpdate(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    
    CommandeModifData cmd = CommandeModifData.getCommandeModifData(request.getParameter("id").trim());
    System.out.println("CommandeModifData : " + cmd);
    affichage.Champ[] liste = new affichage.Champ[3];

    Responsable dd = new Responsable();
    dd.setNomTable("as_responsable");
    liste[0] = new Liste("responsable", dd, "nom", "id");

    TypeObjet d1 = new TypeObjet();
    d1.setNomTable("as_secteur");
    liste[1] = new Liste("secteur", d1, "val", "id");
    System.out.println("CLIENT PRODUIT : "+cmd.getClientPoint().length);
//    TypeObjet client = new TypeObjet();
//    client.setNomTable("TABLECLIENTVUE2");
//    liste[2] = new Liste("client", client, "val", "id");
    liste[2] = new Liste("client",cmd.getClientPoint() , "val", "id");
            
    pi.getFormu().changerEnChamp(liste);

    pi.getFormu().getChamp("etat").setAutre("readonly=true");
    pi.getFormu().getChamp("datesaisie").setLibelle("date de saisie");
    pi.getFormu().getChamp("datecommande").setLibelle("Date de commande");
    pi.getFormu().getChamp("responsable").setLibelle("R�sponsable");
    pi.getFormu().getChamp("numcommande").setLibelle("N� commande");
    pi.getFormu().getChamp("typecommande").setLibelle("Type commande");
    pi.getFormu().getChamp("dateliv").setLibelle("Date de livraison");
    pi.getFormu().getChamp("adresseliv").setLibelle("Adresse de livraison");
    pi.getFormu().getChamp("heureliv").setLibelle("Heure de livraison");
    pi.getFormu().getChamp("distance").setLibelle("Distance");
    pi.getFormu().getChamp("remarque").setType("textarea");
    //pi.getFormu().getChamp("responsable").setVisible(false);
    pi.getFormu().getChamp("typecommande").setVisible(false);
    pi.getFormu().getChamp("dateliv").setVisible(false);
    pi.getFormu().getChamp("remarque").setVisible(false);
    pi.getFormu().getChamp("observation").setLibelle("Prenom Client");
    pi.getFormu().getChamp("quartier").setVisible(false);
    pi.getFormu().getChamp("vente").setVisible(false);
    pi.getFormu().getChamp("point").setVisible(false);
    pi.getFormu().getChamp("adresseliv").setVisible(false);
    pi.getFormu().getChamp("distance").setVisible(false);
    
    pi.preparerDataFormu();
    
    CommandeClient base = new CommandeClient();
    CommandeClient[] lda = cmd.getCommandeClient();
    
    String typecommande = lda[0].getTypecommande();
    String responsable = lda[0].getResponsable(); 
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="recettebordereau" id="recettebordereau">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="commande/as-commande-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.commande.CommandeClient">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                        <input name="nomtable" type="hidden" id="nomtable" value="as_commandeclient">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%}catch(Exception e){ e.printStackTrace(); }%>