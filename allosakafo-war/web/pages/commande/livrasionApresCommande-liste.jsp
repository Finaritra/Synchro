<%-- 
    Document   : as-produits-liste
    Created on : 1 d�c. 2016, 10:39:44
    Author     : Joe
--%>
<%@page import="mg.allosakafo.livraison.*"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.produits.ProduitsLibelle"%>
<%@page import="affichage.PageRecherche"%>

<%
    try {
        LivraisonApresCommandePoint lv = new LivraisonApresCommandePoint();
        String listeCrt[] = {"id", "daty","heure", "idcommnademere", "idpoint", "adresse", "idlivreur","client","point"};
        String listeInt[] = {"daty","heure"};
        String libEntete[] = {"id", "daty", "heure", "pointlib", "idcommnademere", "idpoint", "adresse", "idlivreur","client","montant"};
        
        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        System.out.println("LIENNN "+pr.getLien());
        pr.getFormu().getChamp("daty1").setLibelle("date de livraison min");
        pr.getFormu().getChamp("daty2").setLibelle("date de livraison max");
        pr.getFormu().getChamp("idpoint").setLibelle("payement");
        pr.getFormu().getChamp("idlivreur").setLibelle("livreur");
        pr.getFormu().getChamp("idcommnademere").setLibelle("id commande");
        pr.setApres("commande/LivraisonApresCommande-liste.jsp");
        
        pr.getFormu().getChamp("daty1").setDefaut(utilitaire.Utilitaire.dateDuJour());
        pr.getFormu().getChamp("daty2").setDefaut(utilitaire.Utilitaire.dateDuJour());
        
        affichage.Champ[] listePoint = new affichage.Champ[1];
        TypeObjet liste1 = new TypeObjet();
        liste1.setNomTable("point");
        listePoint[0] = new Liste("point", liste1, "val", "id");
        pr.getFormu().changerEnChamp(listePoint);

        pr.getFormu().getChamp("point").setDefaut((String)session.getAttribute("restaurant"));
        pr.getFormu().getChamp("point").setLibelle("restaurant");

        String apreswhere = "";
        if(request.getParameter("date1") == null && request.getParameter("date2") == null){
            apreswhere = "and daty = '"+utilitaire.Utilitaire.dateDuJour()+"'";
        }
        String[] colSomme = {"montant"};
        pr.setAWhere(apreswhere);
        pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste livraisons apres commande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/livrasionApresCommande-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=livraison/livraison-fiche.jsp",pr.getLien() + "?but=commande/as-commande-fiche.jsp"};
            String colonneLien[] = {"id","idcommnademere"};
            String varColonneLien[]={"id","id"};
            pr.getTableau().setVariableLien(varColonneLien);
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "Heure Livraison", "restaurant", "Commande m�re", "Payement", "Adresse", "Livreur","client","montant"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>

<%    } catch (Exception e) {
        e.printStackTrace();
    }
%>