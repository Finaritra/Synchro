<%-- 
    Document   : livrasionApresCommande-fiche
    Created on : 17 mars 2020, 15:06:03
    Author     : Maharo R.
--%>

<%@page import="mg.allosakafo.livraison.*"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.produits.ProduitsLibelle"%>
<%@page import="mg.allosakafo.reservation.ReservationLib"%>
<%@page import="mg.allosakafo.reservation.Reservation"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    try{
    LivraisonApresCommandeComplet a = new LivraisonApresCommandeComplet();
     //a.setNomTable("livraisonlib");
    
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    /*  private String id, source, heuresaisie, heuredebutreservation, heurefinreservation;
    private int nbpersonne;
    private Date datesaisie, datereservation;
    private String idClient,idCommande;
     private String telephone,tablenom;*/
    pc.getChampByName("heure").setLibelle("Heure");
    //pc.getChampByName("heuresaisie").setVisible(false);
  
    pc.getChampByName("idcommnademere").setLibelle("Commande m&egrave;re");
    pc.getChampByName("idpoint").setLibelle("Point relais");
    pc.getChampByName("adresse").setLibelle("Adresse");
    pc.getChampByName("idlivreur").setLibelle("Livreur");
    pc.getChampByName("daty").setLibelle("Date");
   
    pc.setTitre("Livraison Apres Commande Fiche");

%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=commande/livrasionApresCommande-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=annuler&bute=commande/livrasionApresCommande-fiche.jsp&classe=mg.allosakafo.livraison.LivraisonApresCommande" style="margin-right: 10px">Annuler</a>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=valider&bute=commande/livrasionApresCommande-fiche.jsp&classe=mg.allosakafo.livraison.LivraisonApresCommande" style="margin-right: 10px">Viser</a>
                            
                        
                        </div>
                        <br/>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                            <% } catch(Exception e){e.printStackTrace(); }%>