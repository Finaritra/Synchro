<%-- 
    Document   : as-commande-liste-etat.jsp
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="lc.Direction"%>

<%
    try{
    CommandeClientDetailsTypeP lv = new CommandeClientDetailsTypeP();
    String nomTable = "VUE_CMD_CLT_DETS_REST_PROD";
    if(request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0){
        nomTable = request.getParameter("table");
    }
    lv.setNomTable(nomTable);
	
    String listeCrt[] = {"id", "produit", "datecommande", "nomtable","responsable", "restaurant" ,"heureliv" };
    String listeInt[] = { "datecommande" ,"heureliv"};
    String libEntete[] = { "id","quantite" ,"produit", "acco_sauce", "nomtable","responsable", "heureliv",   "etat"};
        
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
//    affichage.Champ[] listeDir = new affichage.Champ[1];
//    Direction dir = new Direction();
//    dir.setNomTable("restaurant");
//    listeDir[0] = new Liste("restaurant",dir,"libelledir","iddir");
//    pr.getFormu().changerEnChamp(listeDir);
//        
    affichage.Champ[] listePoint = new affichage.Champ[1];
    TypeObjet liste1 = new TypeObjet();
    liste1.setNomTable("point");
    listePoint[0] = new Liste("restaurant", liste1, "val", "id");
    pr.getFormu().changerEnChamp(listePoint);
        

//    String apreswhere = "";
//    if(request.getParameter("datecommande1") == null || request.getParameter("datecommande2") == null){
//        apreswhere = "and datecommande = '"+utilitaire.Utilitaire.dateDuJour()+"'";
//    }
//    if(request.getParameter("restaurant") == null){
//        apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
//    }
//    pr.setAWhere(apreswhere);
    
    /*pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("datecommande2").setDefaut(utilitaire.Utilitaire.dateDuJour());*/
    pr.getFormu().getChamp("restaurant").setDefaut(session.getAttribute("restaurant").toString());
    
    pr.setApres("commande/as-commande-liste-etat.jsp");
    String[] colSomme = {"montant"};
    pr.setNpp(-1);
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-commande-liste-etat.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                         <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("VUE_CMD_CLT_DETS_REST_PROD") == 0) {%>    
                        <option value="VUE_CMD_CLT_DETS_REST_PROD" selected>Tous</option>
                        <% } else { %>
                        <option value="VUE_CMD_CLT_DETS_REST_PROD" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("VUE_CMD_CLT_DTLS_TYPEP_CREER") == 0) {%>    
                        <option value="VUE_CMD_CLT_DTLS_TYPEP_CREER" selected>Cr&eacute;e</option>
                        <% } else { %>
                        <option value="VUE_CMD_CLT_DTLS_TYPEP_CREER" >Cr&eacute;e</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_clt_dtls_typeP_cloture") == 0) {%>    
                        <option value="vue_cmd_clt_dtls_typeP_cloture" selected>Clotur&eacute;</option>
                        <% } else { %>
                        <option value="vue_cmd_clt_dtls_typeP_cloture" >Clotur&eacute;</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_clt_dtls_typeP_fait") == 0) {%>
                        <option value="vue_cmd_clt_dtls_typeP_fait" selected>Pr&ecirc;t &agrave; livrer</option>
                        <% } else { %>
                        <option value="vue_cmd_clt_dtls_typeP_fait">Pr&ecirc;t &agrave; livrer</option>
                        <% } %>

                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_clt_dtls_typeP_valide") == 0) {%>
                        <option value="vue_cmd_clt_dtls_typeP_valide" selected>Valid&eacute;</option>
                        <% } else { %>
                        <option value="vue_cmd_clt_dtls_typeP_valide">Valid&eacute;</option>
                        <% } %>
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("vue_cmd_clt_dtls_typeP_livre") == 0) {%>
                        <option value="vue_cmd_clt_dtls_typeP_livre" selected>Livr&eacute;</option>
                        <% } else { %>
                        <option value="vue_cmd_clt_dtls_typeP_livre">Livr&eacute;</option>
                        <% } %>
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("VUE_CMD_CLT_DTLS_TYPEP_PAYER") == 0) {%>
                        <option value="VUE_CMD_CLT_DTLS_TYPEP_PAYER" selected>Pay&eacute;</option>
                        <% } else { %>
                        <option value="VUE_CMD_CLT_DTLS_TYPEP_PAYER">Pay&eacute;</option>
                        <% } %>
				
                    </select>
                </div>
            </div>
        </form>
        <%  
            String lienTableau[] = {pr.getLien() + "?but=commande/as-commandedetail-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());
        %>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Quantite", "Produit", "Accompagnement sauce", "Table","Responsable","Heure", "Etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
	if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeclient_libfait") == 0) {    
        %>
		<form method="post" name="e" action="<%=pr.getLien()%>?but=commande/as-livraison-saisie.jsp">
		<%
				out.println(pr.getTableau().getHtmlWithCheckbox());
		%>
				<input type="hidden" name="acte" value="livraison-multiple">
		</form>
		<%
            } 
			else{
				out.println(pr.getTableau().getHtml());
				out.println(pr.getBasPage());
			}
        %>
    </section></div>
    <%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>