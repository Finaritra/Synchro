<%-- 
    Document   : as-commande-liste-details.jsp
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="lc.Direction"%>


<%
    try {

        CommandeClientDetailsTypeP2 p = new CommandeClientDetailsTypeP2();
        String nomTable = "Vue_cmd_dtls_clot_voaray";

        /*if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0) {
            nomTable = request.getParameter("table");
        }*/
        p.setNomTable(nomTable);

        String listeCrt[] = {"id", "produit", "typeproduit", "datecommande", "nomtable", "restaurant", "heureliv", "responsable"};
        String listeInt[] = {"datecommande", "heureliv"};
        String libEntete[] = {"id", "produit", "acco_sauce", "observation", "nomtable", "responsable",  "etat", "prioriter"};

        PageRecherche pr = new PageRecherche(p, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        
        affichage.Champ[] listePoint = new affichage.Champ[1];
        TypeObjet liste1 = new TypeObjet();
        liste1.setNomTable("point");
        listePoint[0] = new Liste("restaurant", liste1, "val", "id");
        pr.getFormu().changerEnChamp(listePoint);
        //pr.setAWhere(" order by id desc");
//        affichage.Champ[] listeDir = new affichage.Champ[1];
//        Direction dir = new Direction();
//        dir.setNomTable("restaurant");
//        listeDir[0] = new Liste("restaurant",dir,"libelledir","iddir");
//        pr.getFormu().changerEnChamp(listeDir);

        pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());
        pr.getFormu().getChamp("datecommande2").setDefaut(utilitaire.Utilitaire.dateDuJour());

        pr.getFormu().getChamp("heureliv2").setDefaut(utilitaire.Utilitaire.ajoutHeure(utilitaire.Utilitaire.heureCouranteHMS(), 0, utilitaire.Constante.attenteLivraison, 0));
        //pr.getFormu().getChamp("restaurant").setDefaut((String) session.getAttribute("restaurant"));
        pr.getFormu().getChamp("restaurant").setDefaut(session.getAttribute("restaurant").toString());
        
        String apreswhere = "";
        if (request.getParameter("datecommande1") == null || request.getParameter("datecommande2") == null) {
            apreswhere = "and datecommande = '" + utilitaire.Utilitaire.dateDuJour() + "'";
        }
        if (request.getParameter("heureliv2") == null) {
            //System.out.println("HEURE COURRANTE AVEC ADDITION "+utilitaire.Utilitaire.ajoutHeure(utilitaire.Utilitaire.heureCouranteHMS(),utilitaire.Constante.attenteLivraison));
            //System.out.println("HEURE COURRANTE AVEC ADDITION "+utilitaire.Utilitaire.ajoutHeure(utilitaire.Utilitaire.heureCouranteHMS(),0,utilitaire.Constante.attenteLivraison,0));

            apreswhere = apreswhere + " and heureliv <= '" + utilitaire.Utilitaire.ajoutHeure(utilitaire.Utilitaire.heureCouranteHMS(), 0, utilitaire.Constante.attenteLivraison, 0) + "'";
        }
//        if(request.getParameter("restaurant") == null){
//            apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
//        }
//        
        pr.setAWhere(apreswhere);

        pr.setApres("commande/as-commande-liste-details.jsp");
        String[] colSomme = {};
        pr.setNpp(500);
        pr.creerObjetPage(libEntete, colSomme);

        CommandeClientDetailsTypeP2[] liste = (CommandeClientDetailsTypeP2[]) pr.getTableau().getData();
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande d�tail</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-commande-liste-details.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <form action="<%=pr.getLien()%>?but=modifierEtatMultiple.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getTableauRecap().getHtml());%>
            <br>
            <%if (liste.length > 0) {%>
            <div class="box-footer">
                <input id="acte" type="hidden" name="acte" value=""> 
                <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;" onclick="document.getElementById('acte').value = 'fait'">Pr�t � livrer</button> 
                <button type="submit" name="Submit2" class="btn btn-warning pull-right" style="margin-right: 25px;" onclick="document.getElementById('acte').value = 'voaraycuisinier'">Voaray Cuisinier</button> 
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title" align="center">LISTE</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <div id="selectnonee">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                            <thead>
                                <tr class="head">
                                    <th align="center" valign="top" style="background-color:#bed1dd">
                                        <input onclick="CocheToutCheckbox(this, 'id')" type="checkbox">
                                    </th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Id</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Produit</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Sauce accompagnement</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Remarque</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Table</th>
                                    <th width="20%" align="center" valign="top" style="background-color:#bed1dd">Mere</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Heure</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Etat</th>
                                    

                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (int i = 0; i < liste.length; i++) {
                                %>
                                <tr style="<%if(liste[i].getEtat().contains("PREPA"))out.print("background-color : #5cffb5");%>">
                                    <td align="center">
                                        <input type="checkbox" value="<%=liste[i].getId()%>" name="id" id="checkbox0">
                                    </td>
                                    <td align="center">
                                        <a href="/phobo/pages/module.jsp?but=commande/as-commandedetail-fiche.jsp&id=<%=liste[i].getId()%>"><%=liste[i].getId()%></a>
                                    </td>
                                    <td align="center"><%=liste[i].getProduit()%> </td>
                                    <td align="center"><%=liste[i].getAcco_sauce()%></td>
                                    <td align="center"><%=Utilitaire.champNull(liste[i].getObservation())%></td>
                                    <td align="center"><%=liste[i].getNomtable()%></td>
                                    <td align="center"><a href="/phobo/pages/module.jsp?but=commande/as-commande-fiche.jsp&id=<%=liste[i].getResponsable()%>"><%=liste[i].getResponsable()%></a></td>
                                    <td align="center"><%=liste[i].getHeureCommande() %></td>
                                    <td align="center"><%=liste[i].getEtat()%></td>

                                    
                                </tr>
                                <%}%>
                            <input type="hidden" name="ids" value="null">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <input type="hidden" name="bute" value="commande/as-commande-liste-details.jsp"/>
            <%}%>
            <button type="submit" name="Submit2" class="btn btn-danger pull-right" style="margin-right: 25px;" onclick="document.getElementById('acte').value = 'annuler'">Annuler</button>
        </form>
        <% out.println(pr.getBasPage()); %>
    </section>
</div>

<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>

<script>
    function up(priorite1, priorite2, id) {

        var valeur = {
            'priorite1': priorite1,
            'priorite2': priorite2,
            'id': id
        };
        $.ajax({
            type: 'GET',
            url: '/phobo/UpDownSerlet',
            contentType: 'application/json',
            data: valeur,
            success: function (data) {
                var json = JSON.parse(data);
                console.log(json);

            },
            error: function (e) {
                console.log(e);
            }
        });
    }
    function down(priorite3, priorite4, id2) {
        var valeur = {
            'priorite1': priorite3,
            'priorite2': priorite4,
            'id': id2
        };
        $.ajax({
            type: 'GET',
            url: '/phobo/UpDownSerlet',
            contentType: 'application/json',
            data: valeur,
            success: function (data) {
                var json = JSON.parse(data);
                console.log(json);

            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    $(document).ready(function () {
        $(".up,.down").click(function () {
            var row = $(this).parents("tr:first");
            if ($(this).is(".up")) {

                row.insertBefore(row.prev());
            } else {
                row.insertAfter(row.next());
            }

        });
    });


</script>