<%-- 
    Document   : as-commande-liste-details-fait
    Created on : 24 sept. 2019, 14:40:48
    Author     : Notiavina
--%>

<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<%
    try {

        CommandeClientDetailsTypeP p = new CommandeClientDetailsTypeP();
        String nomTable = "vue_cmd_clt_dtls_typeP_fait";

        /*if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0) {
            nomTable = request.getParameter("table");
        }*/
        p.setNomTable(nomTable);

        String listeCrt[] = {"id", "produit","nomtable","TYPEPRODUIT"};
        String listeInt[] = {"quantite", "pu", "remise","etat", "datecommande"};
        String libEntete[] = {"id", "quantite","produit","acco_sauce","nomtable", "typeproduit","datecommande", "heureliv", "pu","montant",  "etat"};
        PageRecherche pr = new PageRecherche(p, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        //pr.setAWhere(" order by id desc");
        pr.setApres("commande/as-commande-liste-details-fait.jsp");
        String[] colSomme = {};
        pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande d�tail</h1>
    </section>
    <section class="content">
    <form action="<%=pr.getLien()%>?but=commande/as-commande-liste-details-fait.jsp" method="post" name="incident" id="incident">
        <%
            out.println(pr.getFormu().getHtmlEnsemble());
        %>
    </form>
    <form action="<%=pr.getLien()%>?but=modifierEtatMultiple.jsp" method="post" name="incident" id="incident">
        <%  
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Quantite", "Produit", "Sauce accompagnement", "Table","Type", "Date", "Heure",  "Prix unitaire","Montant", "Etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            pr.getTableau().setNameBoutton("Valider");
            pr.getTableau().setNameActe("valider");
            pr.getTableau().setNameBoutton2("Annuler");
            pr.getTableau().setNameActe2("cloturer");
            out.println(pr.getTableau().getHtmlWithCheckbox());
        %>
        <input type="hidden" name="bute" value="commande/as-commande-liste-details-fait.jsp"/>
    </form>
    <% out.println(pr.getBasPage()); %>
    </section>
</div>

<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>