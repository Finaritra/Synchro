<%-- 
    Document   : as-commande-liste
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.commande.LivraisonCommandeClientDetailsTypeP2"%>
<%@page import="bean.CGenUtil"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.livraison.CommandeLivraisonLib"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="mg.allosakafo.fin.Caisse"%>

<%
    try {
        String lien=(String)session.getValue("lien");
        LivraisonCommandeClientDetailsTypeP2 lv = new LivraisonCommandeClientDetailsTypeP2();
        String nomTable="VUE_CMD_LIV_DTLS_ACLOT";
        String name=request.getParameter("table");
        if(name!=null && name.compareTo("")!=0){
            nomTable=name;
        }
        lv.setNomTable(nomTable);
        
        String listeCrt[] = {"id", "idcl","idmere", "produit", "typeproduit", "datecommande", "telephone", "nomtable", "restaurant", "heureliv", "responsable"};
        String listeInt[] = {"datecommande","heureliv"};
        String libEntete[] = {"id","idmere", "idcl", "produit", "acco_sauce", "observation", "telephone", "nomtable", "responsable", "heureliv", "etat", "prioriter","montant"};


        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        
        affichage.Champ[] listePoint = new affichage.Champ[1];
        TypeObjet liste1 = new TypeObjet();
        liste1.setNomTable("point");
        listePoint[0] = new Liste("restaurant", liste1, "val", "id");
        pr.getFormu().changerEnChamp(listePoint);
        
        
        pr.getFormu().getChamp("restaurant").setDefaut(session.getAttribute("restaurant").toString());
        pr.getFormu().getChamp("nomtable").setLibelle("Nom client");
        pr.getFormu().getChamp("heureliv1").setLibelle("Heure min");
        pr.getFormu().getChamp("heureliv2").setLibelle("Heure max");
        pr.getFormu().getChamp("idmere").setLibelle("Commande m&egrave;re");
        pr.getFormu().getChamp("datecommande1").setLibelle("Date commande min");
        pr.getFormu().getChamp("datecommande2").setLibelle("Date commande max");
         
      
        pr.getFormu().getChamp("restaurant").setDefaut((String) session.getAttribute("restaurant"));

        String apreswhere = "";
        if (request.getParameter("datecommande1") == null || request.getParameter("datecommande2") == null) {
            apreswhere = "and datecommande >= '" + utilitaire.Utilitaire.dateDuJour() + "'";
        }
        pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());
        
        pr.setAWhere(apreswhere);
        
        pr.setApres("commande/commande-livraison-liste.jsp");

        String[] colSomme = {"montant"};
        pr.creerObjetPage(libEntete, colSomme);
        Caisse c=new Caisse();
        Caisse[] listeCaisse=c.getAllCaisse(null);
        LivraisonCommandeClientDetailsTypeP2[] liste = (LivraisonCommandeClientDetailsTypeP2[]) pr.getTableau().getData();
%>

<div class="content-wrapper">
    <section class="content">
        <div class="row" style="padding-top: 15px;">
            <h1>Liste commande livraison</h1>
        </div>
        <div class="row">
        <form action="<%=pr.getLien()%>?but=commande/commande-livraison-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
                
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                         <% if (nomTable != null && nomTable.compareToIgnoreCase("VUE_CMD_LIV_DTLS_TOUS") == 0) {%>    
                        <option value="VUE_CMD_LIV_DTLS_TOUS" selected>Tous</option>
                        <% } else { %>
                        <option value="VUE_CMD_LIV_DTLS_TOUS" >Tous</option>
                        <% } %>
                        <% if (nomTable != null && nomTable.compareToIgnoreCase("VUE_CMD_LIV_DTLS_ACLOT") == 0) {%>    
                        <option value="VUE_CMD_LIV_DTLS_ACLOT" selected>A cloturer</option>
                        <% } else { %>
                        <option value="VUE_CMD_LIV_DTLS_ACLOT" >A cloturer</option>
                        <% } %>
                        <% if (nomTable != null && nomTable.compareToIgnoreCase("VUE_CMD_LIV_DTLS_ANNULERPAY") == 0) {%>    
                        <option value="VUE_CMD_LIV_DTLS_ANNULERPAY" selected>Annuler payer</option>
                        <% } else { %>
                        <option value="VUE_CMD_LIV_DTLS_ANNULERPAY" >Annuler payer</option>
                        <% } %>
                        
				
                    </select>
                </div>
            </div>
            
        </form>
        </div>
        <div class="row">
        <%out.println(pr.getTableauRecap().getHtml());%>
        <form action="<%=pr.getLien()%>?but=modifierEtatMultiple.jsp" method="post" name="incident" id="incident">
            <% if(nomTable.compareTo("VUE_CMD_LIV_DTLS_ACLOT")==0){%>
            <div class="row">
                <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Caisse : 
                    <select name="caisse" class="champ" id="caisse" >
                        <% for(Caisse caisse:listeCaisse){%>
                        <option value="<%=caisse.getIdcaisse()%>" <%if (caisse.getDesccaisse().compareToIgnoreCase("-")==0) out.print("selected"); %>><%=caisse.getDesccaisse()%></option>
                        <% } %>
                    </select>
                </div>
            </div>
            </div>
            <%}%>
            <% if(liste.length>0){%>
                <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title" align="center">LISTE</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <div id="selectnonee">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                            <thead>
                                <tr class="head">
                                    <th width="5%" align="center" valign="top" style="background-color:#bed1dd">
                                        <input onclick="CocheToutCheckbox(this, 'id')" type="checkbox">
                                    </th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Id</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">M�re</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Date commande</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Produit</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Sauce accompagnement</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Remarque</th>
                                    <th width="5%" align="center" valign="top" style="background-color:#bed1dd">Telephone</th>                                    
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Client</th>
                                    <th width="5%" align="center" valign="top" style="background-color:#bed1dd">Heure</th>
                                    <th width="5%" align="center" valign="top" style="background-color:#bed1dd">montant</th>
                                    <th width="10%" align="center" valign="top" style="background-color:#bed1dd">Etat</th>

                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (int i = 0; i < liste.length; i++) {
                                %>
                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''" style="">
                                    <td align="" width="">
                                        <input type="checkbox" value="<%=liste[i].getId()%>" name="id" id="checkbox0">
                                    </td>
                                    <td width="10%" align=""><%=liste[i].getId()%></td>
                                    <td width="10%" align=""><a href="<%=lien%>?but=commande/as-commande-fiche.jsp?id=<%=liste[i].getIdmere()%>"><%=liste[i].getIdmere()%></a></td>
                                    <td width="10%" align=""><%=Utilitaire.formatterDaty(liste[i].getDatecommande())%></td>
                                    <td width="10%" align=""><%=liste[i].getProduit()%></td>
                                    <td width="10%" align=""><%=liste[i].getAcco_sauce()%></td>
                                    <td width="10%" align=""><%=Utilitaire.champNull(liste[i].getObservation())%></td>
                                    <td width="5%" align=""><%=liste[i].getTelephone()%></td>
                                    <td width="10%" align=""><%=liste[i].getNomtable() %></td>
                                    <td width="5%" align=""><%=liste[i].getHeureliv()%></td>
                                    <td width="10%" align=""><%=utilitaire.Utilitaire.formaterAr(liste[i].getMontant()) %></td>
                                    <td width="10%" align=""><%=liste[i].getEtat()%></td>
                                </tr>
                                <%}%>
                            <input type="hidden" name="ids" value="null">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <input type="hidden" name="bute" value="commande/commande-livraison-liste.jsp"/>
              <button class="btn btn-danger" type="submit" name="acte" id="acte" value="" onclick="conf()">Annuler</button>
            <%}%>
             <%if(nomTable.compareTo("VUE_CMD_LIV_DTLS_ACLOT")==0){%>
                <input type="hidden" name="bute" value="commande/commande-livraison-liste.jsp">
                <button class="btn btn-success" type="submit" id="acte" name="acte" value="cloturer">Cloturer</button>
              
            <% } %>
            <!-- ------------------------------------------------------------------------------------------------------------->
            
            <%if(nomTable.compareTo("commandelivraisonliblAcloturer")==0){%>
                <input type="hidden" name="bute" value="commande/commande-livraison-liste.jsp">
                <button class="btn btn-success" type="submit" name="acte" value="cloturer">Cloturer</button>
                <button class="btn btn-danger" type="submit" id="acte" name="acte" value="" onclick="conf()">Annuler</button>
            <% } %>
            <%
                out.println(pr.getBasPage());
            %>
        </form>
        </div>
    </section>    


</div>

<%    } catch (Exception e) {
        e.printStackTrace();
    }
%>
<script>
      function conf(){
            if (confirm("Voulez-vous vraiment annuler cette commande?")){
                document.getElementById('acte').value='annuler';
            }
        }
    function changerDesignation() {
        document.forms["incident"].submit();
    }
</script>
