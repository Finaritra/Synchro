<%-- 
    Document   : as-commandedetail-modif
    Created on : 30 sept. 2019, 14:59:49
    Author     : pro
--%>

<%@page import="mg.allosakafo.commande.CommandeClientDetails"%>
<%@ page import="user.*"%>
<%@ page import="bean.*"%>
<%@ page import="utilitaire.*"%>
<%@ page import="affichage.*"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsLibelle"%>
<%@page import="mg.allosakafo.tiers.Responsable"%>
<%@page import="mg.allosakafo.appel.Appel"%>

<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    CommandeClientDetails da = new CommandeClientDetails();
    PageUpdate pi = new PageUpdate(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    pi.getFormu().getChamp("idAccompagnementSauce").setPageAppel("choix/listeAccompagnementSauceChoix.jsp");
    pi.getFormu().getChamp("produit").setPageAppel("choix/listeProduitChoix.jsp");
    pi.preparerDataFormu();
    
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="recettebordereau" id="recettebordereau">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="commande/as-commandedetail-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.commande.CommandeClientDetails">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                        <input name="nomtable" type="hidden" id="nomtable" value="as_detailscommande">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>