<%-- 
    Document   : as-commande-liste-boisson
    Created on : 30 sept. 2019, 10:50:56
    Author     : pro
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="lc.Direction"%>

<%
    try {

        CommandeClientDetailsTypeP2 p = new CommandeClientDetailsTypeP2();
        String nomTable = "Vue_cmd_dtls_bois_rest_resp";

        /*if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0) {
            nomTable = request.getParameter("table");
        }*/
        p.setNomTable(nomTable);

        String listeCrt[] = {"id", "produit", "typeproduit" , "datecommande", "nomtable","responsable", "restaurant"};
        String listeInt[] = {"datecommande"};
        String libEntete[] = {"id", "quantite", "produit", "acco_sauce", "observation", "nomtable","responsable", "heureliv", "etat", "prioriter"};

        PageRecherche pr = new PageRecherche(p, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        
//        affichage.Champ[] listeDir = new affichage.Champ[1];
//        Direction dir = new Direction();
//        dir.setNomTable("restaurant");
//        listeDir[0] = new Liste("restaurant",dir,"libelledir","iddir");
//        pr.getFormu().changerEnChamp(listeDir);
        
        pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());
        pr.getFormu().getChamp("datecommande2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    
        String apreswhere = "";
        if(request.getParameter("datecommande1") == null || request.getParameter("datecommande2") == null){
            apreswhere = "and datecommande = '"+utilitaire.Utilitaire.dateDuJour()+"'";
        }
//        if(request.getParameter("restaurant") == null){
//            apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
//        }
        pr.setAWhere(apreswhere);
        
        affichage.Champ[] listePoint = new affichage.Champ[1];
        TypeObjet liste1 = new TypeObjet();
        liste1.setNomTable("point");
        listePoint[0] = new Liste("restaurant", liste1, "val", "id");
        pr.getFormu().changerEnChamp(listePoint);


        pr.getFormu().getChamp("restaurant").setDefaut(session.getAttribute("restaurant").toString());
            //pr.setAWhere(" order by id desc");
        pr.setApres("commande/as-commande-liste-boisson.jsp");
        String[] colSomme = {};
        pr.setNpp(100);
        pr.creerObjetPage(libEntete, colSomme);

        CommandeClientDetailsTypeP2[] liste = (CommandeClientDetailsTypeP2[]) pr.getTableau().getData();
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande d�tail</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-commande-liste-boisson.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <form action="<%=pr.getLien()%>?but=modifierEtatMultiple.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getTableauRecap().getHtml());%>
            <br>
            <%if (liste.length > 0) {%>
            <div class="box-footer">
                <input id="acte" type="hidden" name="acte" value=""> 
                <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;" onclick="document.getElementById('acte').value = 'fait_boisson'">Livrer</button> 
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title" align="center">LISTE</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <div id="selectnonee">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                            <thead>
                                <tr class="head">
                                    <th align="center" valign="top" style="background-color:#bed1dd">
                                        <input onclick="CocheToutCheckbox(this, 'id')" type="checkbox">
                                    </th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Id</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Quantite</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Produit</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Sauce accompagnement</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Remarque</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Table</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Responsable</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Heure</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Etat</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (int i = 0; i < liste.length; i++) {
                                %>

                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''" style="">
                                    <td align="center">
                                        <input type="checkbox" value="<%=liste[i].getId()%>" name="id" id="checkbox0">
                                    </td>
                                    <td width="14%" align="center">
                                        <%=liste[i].getId()%>
                                    </td>
                                    <td width="14%" align="center"><%=liste[i].getQuantite()%> </td>
                                    <td width="14%" align="center"><%=liste[i].getProduit()%> </td>
                                    <td width="14%" align="center"><%=liste[i].getAcco_sauce()%></td>
                                    <td width="14%" align="center"><%=Utilitaire.champNull(liste[i].getObservation())%></td>
                                    <td width="14%" align="center"><%=liste[i].getNomtable()%></td>
                                    <td width="14%" align="center"><a href="/phobo/pages/module.jsp?but=commande/as-commande-fiche.jsp&id=<%=liste[i].getResponsable()%>"><%=liste[i].getResponsable()%></a></td>
                                    <td width="14%" align="center"><%=liste[i].getHeureCommande()%></td>
                                    <td width="14%" align="center"><%=liste[i].getEtat()%></td>

                                    <td width="14%" align="center">
                                        <%if (i == 0) {%>
                                        
                                        

                                        <%} else if (i == 1) {%>
                                        <a href="#" class="up" onclick="up(<%=liste[i - 1].getPrioriter()%>, 0, '<%=liste[i].getId()%>')">
                                            <span class="glyphicon glyphicon-circle-arrow-up"></span>
                                        </a>&nbsp;&nbsp;
                                        
                                            <%} else {%>
                                        <a href="#" class="up" onclick="up(<%=liste[i - 2].getPrioriter()%>,<%=liste[i - 1].getPrioriter()%>, '<%=liste[i].getId()%>')">
                                            <span class="glyphicon glyphicon-circle-arrow-up"></span>
                                        </a>&nbsp;&nbsp;
                                        
                                        <%}%>
                                        
                                        <%if (i == liste.length - 1) {%>
                                        
                                        <%} else if (i == liste.length - 2) {%>
                                        <a href="#" class="down" onclick="up(<%=liste[i + 1].getPrioriter()%>, <%=liste[i + 1].getPrioriter() + 1000%>, '<%=liste[i].getId()%>')">
                                            <span class="glyphicon glyphicon-circle-arrow-down"></span>
                                        </a>
                                        <%} else {%>
                                        <a href="#" class="down" onclick="up(<%=liste[i + 2].getPrioriter()%>,<%=liste[i + 1].getPrioriter()%>, '<%=liste[i].getId()%>')">
                                            <span class="glyphicon glyphicon-circle-arrow-down"></span>
                                        </a>
                                        <%}%>
                                        
                                    </td>

                                    
                                            
                                </tr>
                                <%}%>
                                
                            <input type="hidden" name="ids" value="null">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                    <input type="hidden" name="bute" value="commande/as-commande-liste-boisson.jsp"/>
            <%}%>
            <button type="submit" name="Submit2" class="btn btn-danger pull-right" style="margin-right: 25px;" onclick="conf()">Annuler</button>
        </form>
        <% out.println(pr.getBasPage()); %>
    </section>
</div>
    <script> 
        function conf(){
            if (confirm("Voulez-vous vraiement annuler cette commande?")){
                document.getElementById('acte').value='annuler_boisson';
            }
        }
    </script>
<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>

<script>
    function up(priorite1, priorite2, id) {

        var valeur = {
            'priorite1': priorite1,
            'priorite2': priorite2,
            'id': id
        };
        $.ajax({
            type: 'GET',
            url: '/phobo/UpDownSerlet',
            contentType: 'application/json',
            data: valeur,
            success: function (data) {
                var json = JSON.parse(data);
                console.log(json);

            },
            error: function (e) {
                console.log(e);
            }
        });
    }
    function down(priorite3, priorite4, id2) {
        var valeur = {
            'priorite1': priorite3,
            'priorite2': priorite4,
            'id': id2
        };
        $.ajax({
            type: 'GET',
            url: '/phobo/UpDownSerlet',
            contentType: 'application/json',
            data: valeur,
            success: function (data) {
                var json = JSON.parse(data);
                console.log(json);

            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    $(document).ready(function () {
        $(".up,.down").click(function () {
            var row = $(this).parents("tr:first");
            if ($(this).is(".up")) {

                row.insertBefore(row.prev());
            } else {
                row.insertAfter(row.next());
            }

        });
    });


</script>