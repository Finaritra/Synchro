<%-- 
    Document   : as-commande-liste
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="lc.Direction"%>
<%@page import="service.AlloSakafoService"%>

<% 
    CommandeClientRestaurantEtat lv = new CommandeClientRestaurantEtat();
    
    String nomTable = "as_commandeclient_libelle2";
    if(request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0){
        nomTable = request.getParameter("table");
    }
    lv.setNomTable(nomTable);
	
    String listeCrt[] = {"id","datecommande", "heureliv", "client", "restaurant","prenom","coursier"};
    String listeInt[] = {"datecommande","heureliv"};
    String libEntete[] = {"id", "datecommande", "client", "prenom","adresseliv", "heureliv", "montant","revient","coursier", "etat"};
    
    
    user.UserEJB u=(user.UserEJB) session.getValue("u");
    if(u.getUser().getIdrole().compareToIgnoreCase("dg")!=0)
    {
        String[] libEnteteRempl = {"id", "datecommande", "client", "prenom","adresseliv", "heureliv", "coursier", "etat"};
        libEntete=libEnteteRempl;
    }
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    
    
    affichage.Champ[] listePoint = new affichage.Champ[1];
    TypeObjet liste1 = new TypeObjet();
    liste1.setNomTable("point");
    listePoint[0] = new Liste("restaurant", liste1, "val", "id");
    pr.getFormu().changerEnChamp(listePoint);


    pr.getFormu().getChamp("restaurant").setDefaut(session.getAttribute("restaurant").toString());
    
//    affichage.Champ[] liste = new affichage.Champ[1];
//    Direction dir = new Direction();
//    dir.setNomTable("restaurant");
//    liste[0] = new Liste("restaurant",dir,"libelledir","iddir");
//    pr.getFormu().changerEnChamp(liste);
//    
//    pr.getFormu().getChamp("restaurant").setDefaut(pr.getUtilisateur().getUser().getAdruser());
    
    pr.getFormu().getChamp("client").setLibelle("Numero de table");
    pr.getFormu().getChamp("prenom").setLibelle("Caisse");
    //pr.getFormu().getChamp("datecommande2").setDefaut(Utilitaire.dateDuJour());

    pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("datecommande2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    
    //pr.getFormu().getChamp("restaurant").setDefaut((String)session.getAttribute("restaurant"));
    
    String apreswhere = "";
    if(request.getParameter("datecommande1") == null && request.getParameter("datecommande2") == null){
        apreswhere = "and datecommande = '"+utilitaire.Utilitaire.dateDuJour()+"'";
    }
//    if(request.getParameter("restaurant") == null){
//        apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
//    }
    
    pr.setAWhere(apreswhere);
    pr.setNpp(100);
    pr.setApres("commande/as-commande-liste.jsp");
    String[] colSomme = {"montant","revient"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/as-commande-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeclient_libelle2") == 0) {%>    
                        <option value="as_commandeclient_libelle2" selected>Tous</option>
                        <% } else { %>
                        <option value="as_commandeclient_libelle2" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeClientLibLivraison") == 0) {%>    
                        <option value="as_commandeClientLibLivraison" selected>Livraison</option>
                        <% } else { %>
                        <option value="as_commandeClientLibLivraison" >Livraison</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeClientLibTable") == 0) {%>    
                        <option value="as_commandeClientLibTable" selected>Table</option>
                        <% } else { %>
                        <option value="as_commandeClientLibTable" >Table</option>
                        <% } %>
                        
				
                    </select>
                </div>
            </div>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=commande/as-commande-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            
            
			if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("as_commandeclient_libfait") == 0) {    
        %>
		<form method="post" name="e" action="<%=pr.getLien()%>?but=commande/as-livraison-saisie.jsp">
		<%
				out.println(pr.getTableau().getHtmlWithCheckbox());
		%>
				<input type="hidden" name="acte" value="livraison-multiple">
		</form>
		<%
            } 
			else{
				out.println(pr.getTableau().getHtml());
				out.println(pr.getBasPage());
			}
        %>
    </section>
</div>