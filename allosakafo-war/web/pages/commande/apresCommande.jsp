<%-- 
    Document   : apresCommande
    Created on : 2 d�c. 2016, 11:26:11
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*"%>
<%@ page import="bean.*" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.allosakafo.commande.*" %>

<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
        String nomtable;
        String typeBoutton;
    %>
    <%

        typeBoutton = request.getParameter("type");
        lien = (String) session.getValue("lien");
        u = (UserEJB) session.getAttribute("u");
        acte = request.getParameter("acte");
        bute = request.getParameter("bute");
        Object temp = null;
        String[] rajoutLien = null;
        String classe = request.getParameter("classe");
        ClassMAPTable t = null;
        String tempRajout = request.getParameter("rajoutLien");
        String idmere = request.getParameter("idmere");
        String val = "";
        try {
            String rajoutLie = "";
            if (tempRajout != null && tempRajout.compareToIgnoreCase("") != 0) {
                rajoutLien = utilitaire.Utilitaire.split(tempRajout, "-");
            }
            if (bute == null || bute.compareToIgnoreCase("") == 0) {
                bute = "pub/Pub.jsp";
            }

            if (classe == null || classe.compareToIgnoreCase("") == 0) {
                classe = "pub.Montant";
            }

            if (typeBoutton == null || typeBoutton.compareToIgnoreCase("") == 0) {
                typeBoutton = "3"; //par defaut modifier
            }

            int type = Utilitaire.stringToInt(typeBoutton);
            String id = request.getParameter("id");
            if (acte.compareToIgnoreCase("insert") == 0) {

                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
                ClassMAPTable o = (ClassMAPTable) u.createObject(f);
                temp = (Object) o;
                if (o != null) {
                    id = o.getTuppleID();
                }
            }
			
			if (acte.compareToIgnoreCase("insertLivraison") == 0) {

                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
				Livraison liv = (Livraison) f;
                                String table=request.getParameter("nomTableC");
                                
                u.createLivraison(liv,table);
            }
			
			if (acte.compareToIgnoreCase("insertPaiement") == 0) {

                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
				Paiement paie = (Paiement) f;
                                String nomtab=request.getParameter("nomtablec");
                u.createPaiement(paie,nomtab);
            }
			
            if (acte.compareToIgnoreCase("delete") == 0) {

                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
                temp = (Object) t;
                u.deleteObject(t);

            }
            if (acte.compareToIgnoreCase("deletefille") == 0) {

                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
                temp = (Object) t;
                u.deleteObjetFille(t);
            }

            if (acte.compareToIgnoreCase("update") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                Page p = new Page(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                temp = f;
                u.updateObject(f);
            }

            if (rajoutLien != null) {
                if (classe.compareToIgnoreCase("mg.allosakafo.commande.CommandeClientDetails") == 0) {
                    rajoutLie = "&id=" + idmere;

                }
            }
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&valeur=<%=val%>");</script>


    <%} catch (Exception ex) {
        ex.printStackTrace();
    %>
    <script>alert('<%=ex.getMessage()%>');
        history.back();</script>
        <%
            }
        %>
</html>
