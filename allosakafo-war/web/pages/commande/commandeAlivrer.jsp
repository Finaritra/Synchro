<%-- 
    Document   : as-commande-liste
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="mg.allosakafo.commande.LivraisonCommandeClientDetailsTypeP3"%>
<%@page import="bean.CGenUtil"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.livraison.CommandeLivraisonLib"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%@page import="lc.Direction"%>

<%
    try {

        LivraisonCommandeClientDetailsTypeP3 lv = new LivraisonCommandeClientDetailsTypeP3();
        String nomTable = "VUE_CMD_LIV_DTLS_ALIVRER_PT";
      

        if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0) {
            nomTable = request.getParameter("table");
        }
        lv.setNomTable(nomTable);

        String listeCrt[] = {"id", "idcl", "produit", "typeproduit", "datycl", "telephone", "nomtable", "restaurant", "heureliv", "responsable","adresse","idpoint"};
        String listeInt[] = {"datycl","heureliv"};
        String libEntete[] = {"id", "idcl", "produit", "acco_sauce", "observation", "telephone", "nomtable", "responsable", "heureliv", "etat", "prioriter","adresse","idpoint"};

        PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        
        pr.getFormu().getChamp("nomtable").setLibelle("Nom client");
        pr.getFormu().getChamp("heureliv1").setLibelle("Heure Min");
         pr.getFormu().getChamp("heureliv2").setLibelle("Heure Max");
        pr.getFormu().getChamp("datycl1").setLibelle("Date commande min");
        pr.getFormu().getChamp("datycl2").setLibelle("Date commande max");
        pr.getFormu().getChamp("idpoint").setLibelle("Point Relais");

        pr.getFormu().getChamp("datycl1").setDefaut(utilitaire.Utilitaire.dateDuJour());
        pr.getFormu().getChamp("datycl2").setDefaut(utilitaire.Utilitaire.dateDuJour());
        
        String apreswhere = "";
        if(request.getParameter("datycl1") == null && request.getParameter("datycl1") == null){
            apreswhere = "and datycl = '"+utilitaire.Utilitaire.dateDuJour()+"'";
        }
    //    if(request.getParameter("restaurant") == null){
    //        apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
    //    }
        apreswhere+=" order by datycl,heureliv desc";
        pr.setAWhere(apreswhere);
        
        affichage.Champ[] listePoint = new affichage.Champ[1];
        TypeObjet liste1 = new TypeObjet();
        liste1.setNomTable("point");
        listePoint[0] = new Liste("restaurant", liste1, "val", "id");
        pr.getFormu().changerEnChamp(listePoint);

        pr.getFormu().getChamp("restaurant").setDefaut(session.getAttribute("restaurant").toString());

        pr.setApres("commande/commandeAlivrer.jsp");
        pr.setNpp(200);
        
        String[] colSomme = {};
        pr.creerObjetPage(libEntete, colSomme);
        TypeObjet c = new TypeObjet();
        c.setNomTable("point");
         LivraisonCommandeClientDetailsTypeP3[] liste = (LivraisonCommandeClientDetailsTypeP3[]) pr.getTableau().getData();

%>
<script>
    function changerDesignation() {
        document.reservation.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Commande A Livrer</h1>
    </section>
    <section class="content">

          <form action="<%=pr.getLien()%>?but=commande/commandeAlivrer.jsp" method="post" name="reservation" id="reservation">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="col-md-4 col-md-offset-5">
                <div class="form-group">
                    <label>Etat : </label>
                    <select name="table" class="champ form-control" id="etatvue" onchange="changerDesignation()" style="display: inline-block; width: 250px;">
                         <option value="VUE_CMD_LIV_DTLS_ALIVRER_PT" <%if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("VUE_CMD_LIV_DTLS_ALIVRER_PT") == 0) {
                                out.print("selected");
                            }%>>Tous</option>
                         <option value="VUE_CMD_LIV_DTLS_ALIVRER_PTVID" <%if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("VUE_CMD_LIV_DTLS_ALIVRER_PTVID") == 0) {
                                out.print("selected");
                            }%>>Domicile</option>
                        <option value="VUE_CMD_LIV_DTLS_ALIVRER_PT_AD" <%if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("VUE_CMD_LIV_DTLS_ALIVRER_PT_AD") == 0) {
                                out.print("selected");
                            }%>>Point Relais</option>
                        
                        
                    </select>
                </div>
            </div>
        </form>
                            </br>   </br>   </br>
            <form action="<%=pr.getLien()%>?but=modifierEtatMultiple.jsp" method="post" name="formu" id="formu">

                <% if (liste.length > 0) {%>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" align="">LISTE</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="selectnonee">
                            <table width="100%" border="0" align="" cellpadding="3" cellspacing="3" class="table table-hover">
                                <thead>
                                    <tr class="head">
                                        <th align="" valign="top" style="background-color:#bed1dd">
                                            <input onclick="CocheToutCheckbox(this, 'id')" type="checkbox">
                                        </th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Id</th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Produit</th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Sauce accompagnement</th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Remarque</th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Telephone</th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Client</th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Commande</th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Heure</th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Adresse</th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Pt Relais</th>
                                        <th width="14%" align="" valign="top" style="background-color:#bed1dd">Etat</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        for (int i = 0; i < liste.length; i++) {
                                    %>
                                    <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''" style="">
                                        <td align="">
                                            <input type="checkbox" value="<%=liste[i].getId()%>" name="id" id="checkbox0">
                                        </td>

                                        <td width="14%" align=""><%=liste[i].getId()%></td>
                                         <td width="14%" align=""><%=liste[i].getProduit()%></td>
                                        <td width="14%" align=""><%=liste[i].getAcco_sauce()%></td>
                                        <td width="14%" align=""><%=Utilitaire.champNull(liste[i].getObservation())%></td>
                                        <td width="14%" align=""><%=liste[i].getTelephone()%></td>
                                        <td width="14%" align=""><%=liste[i].getNomtable()%></td>
                                        <td width="14%" align=""><a href="/phobo/pages/module.jsp?but=commande/as-commande-fiche.jsp&id=<%=liste[i].getResponsable()%>"><%=liste[i].getResponsable()%></a></td>
                                        <td width="14%" align=""><%=liste[i].getHeureliv()%></td>
                                         <td width="14%" align=""><%=liste[i].getAdresse()%></td>
                                          <td width="14%" align=""><%=Utilitaire.champNull(liste[i].getIdpoint())%></td>
                                        <td width="14%" align=""><%=liste[i].getEtat()%></td>

                                    </tr>
                                    <%}%>
                                <input type="hidden" name="ids" value="null">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="bute" value="commande/commandeAlivrer.jsp"/>
                <%}%>
                <input type="hidden" name="bute" value="commande/commande-livraison-liste.jsp">
                <button class="btn btn-success"onclick="changeraction()" type="submit" name="acte" value="livrer">livrer</button>
                <button class="btn btn-primary" type="submit" name="acte" value="revalider">revalider</button>
                <button class="btn btn-danger" type="submit" name="acte" value="rembourser">rembourser</button>
                <button class="btn btn-danger" type="submit" name="acte" id="acte" value="" onclick="conf()">Annuler</button>
                <%
                    out.println(pr.getBasPage());
                %>
            </form>
        </div>
    </section>  
<script text="JavaScript">
      function conf(){
            if (confirm("Voulez-vous vraiment annuler cette commande?")){
                document.getElementById('acte').value='annuler';
            }
        }
    function changeraction(){
        document.formu.action="<%=pr.getLien()%>?but=livraison/livraison-saisie.jsp";
    }
</script>


</div>

<%    } catch (Exception e) {
        e.printStackTrace();
    }
%>
