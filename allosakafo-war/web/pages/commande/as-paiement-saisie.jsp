<%-- 
    Document   : as-paiement-saisie
    Created on : 1 d�c. 2016, 15:42:08
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.commande.Paiement"%>
<%@page import="mg.allosakafo.commande.CommandeService"%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    Paiement da = new Paiement();
    PageInsert pi = new PageInsert(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("tva").setDefaut("0");
    pi.getFormu().getChamp("client").setAutre("readonly");
    pi.getFormu().getChamp("client").setPageAppel("choix/listeClientChoix.jsp");
    pi.getFormu().getChamp("idobjet").setLibelle("R�f. commande");
    pi.getFormu().getChamp("numpiece").setLibelle("N� Pi�ce");
    pi.getFormu().getChamp("modepaiement").setLibelle("Mode de paiement");
    pi.getFormu().getChamp("observation").setType("textarea");
	pi.getFormu().getChamp("caisse").setLibelle("Caisse");
   String nomtable="";
       if(request.getParameter("nomtable") != null && request.getParameter("nomtable").compareToIgnoreCase("")!=0)
    {
        nomtable=request.getParameter("nomtable");
    }
    affichage.Champ[] liste = new affichage.Champ[2];
    
    TypeObjet d = new TypeObjet();
    d.setNomTable("MODEPAIEMENT");
    liste[0] = new Liste("modepaiement", d, "val", "id");
	
	Caisse ou2 = new Caisse();
    ou2.setNomTable("caisse");
    liste[1] = new Liste("caisse", ou2, "desccaisse", "idcaisse");
	    
    pi.getFormu().changerEnChamp(liste);
    
    if(request.getParameter("idCommande") != null && request.getParameter("idCommande").compareToIgnoreCase("") != 0){
        pi.getFormu().getChamp("idobjet").setDefaut(request.getParameter("idCommande"));
        
        double montant = CommandeService.calculerMontantCommande(request.getParameter("idCommande"));
        CommandeClient cmc = CommandeService.getInfoCommandeClient(request.getParameter("idCommande"),nomtable);
        pi.getFormu().getChamp("client").setDefaut(cmc.getClient());
        pi.getFormu().getChamp("montant").setDefaut(Utilitaire.doubleWithoutExponential(montant));
    }
    pi.preparerDataFormu();
    
%>

<div class="content-wrapper">
    <h1 class="box-title">Enregistrer paiement</h1>
    <form action="<%=pi.getLien()%>?but=commande/apresCommande.jsp" method="post" name="appro" id="appro" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insertPaiement">
        <input name="bute" type="hidden" id="bute" value="commande/as-paiement-saisie.jsp">
        <input name="nomtablec" type="hidden" id="nomtablec" value="<%=nomtable%>">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.commande.Paiement">
    </form>
</div>
<script language="JavaScript">

    var options = $('#modepaiement option');
    for (var co = 0; co < options.size(); co++) {
        if (options[co].value === 'pay1') {
            $('#modepaiement option')[co].selected = true;
        } else {
            $('#modepaiement option')[co].selected = false;
        }

    }

</script>