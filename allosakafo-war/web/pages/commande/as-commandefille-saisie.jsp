<%-- 
    Document   : as-commandefille-saisie.jsp
    Created on : 1 d�c. 2016, 10:24:12
    Author     : Joe
--%>

<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetails"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    CommandeClientDetails da = new CommandeClientDetails();
    PageInsert pi = new PageInsert(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));

    String idBC = request.getParameter("id");
    pi.getFormu().getChamp("idmere").setDefaut(idBC);
    String lienfinaliser = "but=apresTarif.jsp&acte=finaliser&bute=stock/bc/stBonDeCommande-liste.jsp&classe=mg.cnaps.st.StBonDeCommande&id="+ idBC;
    
    pi.getFormu().getChamp("produit").setPageAppel("choix/listeArticleChoixMultiple.jsp", "produit;produitlibelle;pu");
    pi.getFormu().getChamp("observation").setType("textarea");
    pi.getFormu().getChamp("produit").setAutre("readonly='true'");
    

    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 align="center">Ajouter produit</h1>
    <form action="<%=pi.getLien()%>?but=commande/apresCommande.jsp" method="post" name="appro" id="appro" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlAddTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="commande/as-commandefille-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.commande.CommandeClientDetails">
        <input name="idmere" type ="hidden" id="idbc" value="<%=idBC%>">
        <input name="rajoutLien" type ="hidden" id="idbc" value="rajoutLien">
    </form>
    <%
        CommandeClientDetails p = new CommandeClientDetails();
        p.setNomTable("as_detailscommande_lib"); // vue
        CommandeClientDetails[] liste = (CommandeClientDetails[]) CGenUtil.rechercher(p, null, null, " and idmere = '" + request.getParameter("id") + "'");
        CommandeClient[] bc = (CommandeClient[])CGenUtil.rechercher(new CommandeClient(), null, null, " AND ID = '" + request.getParameter("id") + "'");
    %>
    <div id="selectnonee">
        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
            <thead>
                <tr class="head">
                    <th style="background-color:#bed1dd">Article</th>
                    <th style="background-color:#bed1dd">Quantit&eacute;</th>
                    <th style="background-color:#bed1dd">PU</th>
                    <th style="background-color:#bed1dd">Remise</th>
                    <th style="background-color:#bed1dd">Montant</th>
                    <th style="background-color:#bed1dd">Remarque</th>
                    <th style="background-color:#bed1dd">Action</th>
                </tr>
            </thead>
            <tbody>
                <%
                    double somme = 0;
                    double tva = 0;
                    for (int i = 0; i < liste.length; i++) {
                        double remise = ((liste[i].getQuantite() * liste[i].getPu()) * liste[i].getRemise()) / 100;
                %>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"><%=liste[i].getProduit()%> </td>
                    <td width="20%" align="center"><%=liste[i].getQuantite()%></td>
                    <td width="20%" align="center"><%=Utilitaire.formaterAr(liste[i].getPu()) %></td>
                    <td width="20%" align="center"><%=Utilitaire.formaterAr(remise)%></td>
                    <td width="20%" align="center"><%=Utilitaire.formaterAr((liste[i].getQuantite() * liste[i].getPu()) - remise)%></td>
                    
                    <td width="20%" align="center"><%=liste[i].getObservation()%></td>
                    <td width="20%" align="center"><a href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + liste[i].getTuppleID() + "&idbc=" + request.getParameter("id")%>&acte=delete&bute=stock/bc/stBonDeCommandeFille-saisie.jsp&classe=mg.cnaps.st.StBonDeCommandeFille&rajoutLien=id" style="margin-right: 10px">annuler</a></td>
                </tr>
                <%
                    somme += ((liste[i].getQuantite() * liste[i].getPu()) - remise);
                    }
                    
                %>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"><b>TOTAL</b></td>
                    <td width="20%" align="center"><b><%=Utilitaire.formaterAr(somme)%></b></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
