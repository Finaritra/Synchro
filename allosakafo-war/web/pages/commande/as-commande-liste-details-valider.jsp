<%-- 
    Document   : as-commande-liste-details-fait
    Created on : 24 sept. 2019, 14:40:48
    Author     : Notiavina
--%>

<%@page import="service.AlloSakafoService"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="lc.Direction"%>

<script language="javascript">
setTimeout(function(){
   window.location.reload(1);
}, 60000);
</script>

<%
    try {

        CommandeClientDetailsTypeP2 p = new CommandeClientDetailsTypeP2();
        String nomTable = "Vue_cmd_dtls_valide_rest_resp";

        /*if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0) {
            nomTable = request.getParameter("table");
        }*/
        p.setNomTable(nomTable);

        String listeCrt[] = {"id", "produit","nomtable","responsable","typeproduit", "datecommande","restaurant"};
        String listeInt[] = {"quantite", "pu", "remise","etat", "datecommande"};
        String libEntete[] = {"id","produit","acco_sauce", "nomtable","responsable", "observation", "typeproduit","datecommande", "heureliv", "pu","montant", "etat"};
        
        PageRecherche pr = new PageRecherche(p, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        //pr.setAWhere(" order by id desc");
//        affichage.Champ[] listeDir = new affichage.Champ[1];
//        Direction dir = new Direction();
//        dir.setNomTable("restaurant");
//        listeDir[0] = new Liste("restaurant",dir,"libelledir","iddir");
//        pr.getFormu().changerEnChamp(listeDir);
//        

        affichage.Champ[] listePoint = new affichage.Champ[1];
        TypeObjet liste1 = new TypeObjet();
        liste1.setNomTable("point");
        listePoint[0] = new Liste("restaurant", liste1, "val", "id");
        pr.getFormu().changerEnChamp(listePoint);
        
        pr.getFormu().getChamp("restaurant").setDefaut(session.getAttribute("restaurant").toString());
        
        pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());
        pr.getFormu().getChamp("datecommande2").setDefaut(utilitaire.Utilitaire.dateDuJour());
        pr.getFormu().getChamp("restaurant").setDefaut((String)session.getAttribute("restaurant"));
//        pr.getFormu().getChamp("restaurant").setDefaut(pr.getUtilisateur().getUser().getAdruser());
//
        String apreswhere = "";
        if(request.getParameter("datecommande1") == null || request.getParameter("datecommande2") == null){
            apreswhere = "and datecommande = '"+utilitaire.Utilitaire.dateDuJour()+"'";
        }
//        if(request.getParameter("restaurant") == null){
//            apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
//        }
        pr.setAWhere(apreswhere);
        
        pr.setApres("commande/as-commande-liste-details-valider.jsp");
        String[] colSomme = {};
        pr.setNpp(500);
        pr.creerObjetPage(libEntete, colSomme);
        CommandeClientDetailsTypeP2[] liste = (CommandeClientDetailsTypeP2[]) pr.getTableau().getData();
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande d�tail</h1>
    </section>
    <section class="content">
    <form action="<%=pr.getLien()%>?but=commande/as-commande-liste-details-valider.jsp" method="post" name="incident" id="incident">
        <%
            out.println(pr.getFormu().getHtmlEnsemble());
        %>
    </form>
    <form action="<%=pr.getLien()%>?but=modifierEtatMultiple.jsp" method="post" name="incident" id="incident">
        <%  
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <input type="hidden" name="acte" id="acte"/>
        <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;" onclick="document.getElementById('acte').value = 'livrer'">A livrer</button> 
        <div class="box-body table-responsive no-padding">
                    <div id="selectnonee">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
                            <thead>
                                <tr class="head">
                                    <th align="center" valign="top" style="background-color:#bed1dd">
                                        <input onclick="CocheToutCheckbox(this, 'id')" type="checkbox">
                                    </th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Id</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Produit</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Sauce accompagnement</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Remarque</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Table</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Mere</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Heure</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Etat</th>
                                    <th width="14%" align="center" valign="top" style="background-color:#bed1dd">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (int i = 0; i < liste.length; i++) {
                                %>
                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''" style="">
                                    <td align="center">
                                        <input type="checkbox" value="<%=liste[i].getId()%>" name="id" id="checkbox0">
                                    </td>
                                    <td width="14%" align="center">
                                        <a href="/phobo/pages/module.jsp?but=commande/as-commandedetail-fiche.jsp&id=<%=liste[i].getId()%>"><%=liste[i].getId()%></a>
                                    </td>
                                    <td width="14%" align="center"><%=liste[i].getProduit()%> </td>
                                    <td width="14%" align="center"><%=liste[i].getAcco_sauce()%></td>
                                    <td width="14%" align="center"><%=Utilitaire.champNull(liste[i].getObservation())%></td>
                                    <td width="14%" align="center"><%=liste[i].getNomtable()%></td>
                                    <td width="14%" align="center"><a href="/phobo/pages/module.jsp?but=commande/as-commande-fiche.jsp&id=<%=liste[i].getResponsable()%>"><%=liste[i].getResponsable()%></a></td>
                                    <td width="14%" align="center"><%=liste[i].getHeureliv()%></td>
                                    <td width="14%" align="center"><%=liste[i].getEtat()%></td>

                                    <td width="14%" align="center">
                                        <%if (i == 0) {%>
                                        <%} else if (i == 1) {%>
                                        <a href="#" class="up" onclick="up(<%=liste[i - 1].getPrioriter()%>, 0, '<%=liste[i].getId()%>')">
                                            <span class="glyphicon glyphicon-circle-arrow-up"></span>
                                        </a>&nbsp;&nbsp;
                                        <%} else {%>
                                        <a href="#" class="up" onclick="up(<%=liste[i - 2].getPrioriter()%>,<%=liste[i - 1].getPrioriter()%>, '<%=liste[i].getId()%>')">
                                            <span class="glyphicon glyphicon-circle-arrow-up"></span>
                                        </a>&nbsp;&nbsp;
                                        <%}%>
                                        <%if (i == liste.length - 1) {%>

                                        <%} else if (i == liste.length - 2) {%>
                                        <a href="#" class="down" onclick="up(<%=liste[i + 1].getPrioriter()%>, <%=liste[i + 1].getPrioriter() + 1000%>, '<%=liste[i].getId()%>')">
                                            <span class="glyphicon glyphicon-circle-arrow-down"></span>
                                        </a>
                                        <%} else {%>
                                        <a href="#" class="down" onclick="up(<%=liste[i + 2].getPrioriter()%>,<%=liste[i + 1].getPrioriter()%>, '<%=liste[i].getId()%>')">
                                            <span class="glyphicon glyphicon-circle-arrow-down"></span>
                                        </a>
                                        <%}%>
                                    </td>
                                </tr>
                                <%}%>
                            <input type="hidden" name="ids" value="null">
                            </tbody>
                        </table>
                    </div>
                </div>
        <input type="hidden" name="bute" value="commande/as-commande-liste-details-valider.jsp"/>
        
        <button type="submit" name="Submit2" class="btn btn-danger pull-right" style="margin-right: 25px;" onclick="conf()">Annuler</button>
        
    </form>
    <% out.println(pr.getBasPage()); %>
    </section>
</div>
    <script> 
        function conf(){
            if (confirm("Voulez-vous vraiement annuler cette commande?")){
                document.getElementById('acte').value='annuler_valider';
            }
        }
    </script> 
<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>