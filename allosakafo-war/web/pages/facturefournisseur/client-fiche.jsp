

<%@page import="affichage.PageConsulte"%>
<%@page import="mg.allosakafo.tiers.ClientAlloSakafo"%>
<%
    ClientAlloSakafo cl = new ClientAlloSakafo();
    cl.setNomTable("FOURNISSEURPRODUITS");

    PageConsulte pc = pc = new PageConsulte(cl, request, (user.UserEJB) session.getValue("u"));

    pc.getChampByName("nom").setLibelle("Nom");
    pc.getChampByName("prenom").setLibelle("Prenom");
    pc.getChampByName("sexe").setLibelle("Sexe");
    pc.getChampByName("datenaissance").setLibelle("Date de naissance");
    pc.getChampByName("adresse").setLibelle("Adresse");
    pc.getChampByName("telephone").setLibelle("Telephone");
    pc.getChampByName("fb").setLibelle("Facebook");

    pc.setTitre("Fiche client");

%>

<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=facturefournisseur/client-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=facturefournisseur/client-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>