<%@page import="affichage.PageUpdate"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.allosakafo.tiers.ClientAlloSakafo"%>

<%
    
    ClientAlloSakafo cl = new ClientAlloSakafo();
    cl.setNomTable("FOURNISSEURPRODUITS");
    
    PageUpdate pi = new PageUpdate( cl , request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    
    pi.preparerDataFormu();

%>

<div class="content-wrapper">
    <h1 class="box-title">Modification client</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro">
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="acte" value="update">
        <input name="bute" type="hidden" id="bute" value="facturefournisseur/client-modif.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.tiers.ClientAlloSakafo">
        <input name="nomtable" type="hidden" id="classe" value="fournisseurProduits">
    </form>
</div>