

<%@page import="mg.allosakafo.facturefournisseur.DetailsFactureFournisseur"%>
<%@page import="affichage.PageConsulte"%>
<%@page import="mg.allosakafo.tiers.ClientAlloSakafo"%>
<%
    DetailsFactureFournisseur cl = new DetailsFactureFournisseur();
    cl.setNomTable("detailsfflib");

    PageConsulte pc = pc = new PageConsulte(cl, request, (user.UserEJB) session.getValue("u"));

    pc.getChampByName("idingredient").setLibelle("Ingredient");
    pc.getChampByName("idmere").setLibelle("Facture fournisseur");
    pc.setTitre("Fiche details facture fournisseur");

%>

<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=facturefournisseur/detailsfacturefournisseur-fiche.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=facturefournisseur/detailsfacturefournisseur-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>