<%@page import="service.UploadService"%>
<%@page import="mg.allosakafo.AttacherFichier"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.facturefournisseur.*"%>
<%
    EtatFacture a = new EtatFacture();
    //a.setNomTable("EtatFacture");
    a.setNomTable("ETATFACTUREMONTANT");
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    
    pc.getChampByName("id").setLibelle("ID");
	pc.getChampByName("idFournisseur").setLibelle("Fournisseur");
	pc.getChampByName("idTVA").setLibelle("Montant TVA");
	pc.getChampByName("montantTTC").setLibelle("Montant TTC");
	pc.getChampByName("dateEmission").setVisible(false);
        pc.getChampByName("identite").setVisible(false);
	pc.getChampByName("datyEcheance").setLibelle("Echeance");
	pc.getChampByName("idDevise").setLibelle("Devise");
	pc.getChampByName("numFact").setLibelle("N? Facture");
        
	
    pc.setTitre("Consultation Facture Fournisseur");

	EtatFacture ff = (EtatFacture)pc.getBase();
        String designationOP="Payement facture num "+ff.getId() +" "+ Utilitaire.remplacerNull(ff.getNumFact()) +" de "+ff.getIdFournisseur();
           AttacherFichier[] fichiers = UploadService.getUploadFile(request.getParameter("id"));
           //AttacherFichier[] fichiers =null;
            configuration.CynthiaConf.load();
            String cdn = configuration.CynthiaConf.properties.getProperty("cdnReadUri");
            String dossier="ff";
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=facturefournisseur/facturefournisseur-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
					
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
			<div class="box-footer" >
                            <a class="btn btn-info pull-right"  href="<%=(String) session.getValue("lien") + "?but=pageupload.jsp&id=" + request.getParameter("id") + "&dossier="+dossier+"&nomtable=ATTACHER_FICHIER&procedure=GETSEQ_ATTACHER_FICHIER&bute=facturefournisseur/facturefournisseur-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Attacher Fichier</a> 
                            <a class="btn btn-primary pull-right"  href="<%=(String) session.getValue("lien") + "?but=facturefournisseur/detailsfacturefournisseur-saisie.jsp&idmere=" + request.getParameter("id")%>" style="margin-right: 10px">Saisir details facture fournisseur</a>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=livrerbl&bute=stock/bl/bondelivraison-fiche.jsp&idfacture=" + ff.getId() %>" style="margin-right: 10px">livrer</a>
                            
                            <% if (ff.getEtat().compareToIgnoreCase("CREER")==0 ){%>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&classe=mg.allosakafo.facturefournisseur.FactureFournisseur&bute=facturefournisseur/facturefournisseur-fiche.jsp&id=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Viser</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=facturefournisseur/facturefournisseur-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                        <% 
                            }
                            else if (ff.getEtat().compareToIgnoreCase("VALIDER")==0 ){
                            %>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annulerVisa&classe=mg.allosakafo.facturefournisseur.FactureFournisseur&bute=facturefournisseur/facturefournisseur-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=ded/ordonnerpayement-saisie.jsp&ded_Id=" + request.getParameter("id")%>&montant=<%=ff.getReste()%>&remarque=<%=designationOP%>" style="margin-right: 10px">Editer OP</a>
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">D&eacute;tails facture fournisseur</h1>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Ingredient</th>
                                    <th>Quantit&eacute;</th>
                                    <th>P.U</th>
                                    <th>Montant</th>
                                    <th>Compte</th>
                                    <th>Type</th>
                                    <th>Livr&eacute;</th>
                                    <th>Reste</th>
                                     <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                <%
                                    Detailsffbonlivraison[] liste = ff.getFilleBL("detailsffbonlivraisonlib",null);
                                    for (int i = 0; i < liste.length; i++) {%>
                                               <tr  onmouseout="this.style.backgroundColor = ''">
                                                <td  align="center"><a href="<%=(String) session.getValue("lien") + "?but=facturefournisseur/detailsfacturefournisseur-fiche.jsp&id=" + liste[i].getId()%>"><%=liste[i].getId()%></a></td>
                                                <td  align="center"><%=liste[i].getIdingredient()%></td>
                                                <td  align="center"><%=Utilitaire.formaterAr(liste[i].getQte())%></td>
                                                <td  align="right"><%=Utilitaire.formaterAr(liste[i].getPu())%></td>
                                                <td  align="right"><%=Utilitaire.formaterAr((liste[i].getQte()* liste[i].getPu()))%></td>
                                                <td  align="right"><%=liste[i].getCompte()%></td>    
                                                <td  align="right"><%=liste[i].getIdtypefacturelib()%></td>    
                                                <td  align="right"><a href="<%=(String) session.getValue("lien") + "?but=stock/bl/asBondeLivraisonFille-liste.jsp&iddetailsfacturefournisseur="+liste[i].getId()%>"><%=Utilitaire.formaterAr(liste[i].getLivre())%></a></td>
                                               <td  align="right"><%=Utilitaire.formaterAr(liste[i].getReste())%></td>
                                                <td  align="right">     <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=delete&classe=mg.allosakafo.facturefournisseur.DetailsFactureFournisseur&nomtable=detailsfacturefournisseur&bute=facturefournisseur/facturefournisseur-liste.jsp&id="+liste[i].getId()%>" style="margin-right: 10px">Supprimer</a>
                                            </td>
                                            </tr>
                                <% }%>
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
                            
           <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="box-title with-border">

                            <h2 class="box-title" style="margin-left: 10px;">Les fichiers attach�s</h2>
                        </div>
                        <div class="box-body">
			<table class="table table-striped table-bordered table-condensed table-responsive tree">
                                <thead>
                                    <tr>
                                        <th style="background-color:#bed1dd;"></th>
                                        <th style="background-color:#bed1dd;">Libell�</th>
                                        <th style="background-color:#bed1dd;">Fichier</th>
                                        <th style="background-color:#bed1dd;">Voir</th>
                                        <th style="background-color:#bed1dd;">#</th>
                                    </tr>
                                </thead>
                               <tbody>
                                   <%if(fichiers==null || fichiers.length==0){%>
                                   <tr>
                                        <td colspan="3" style="text-align: center;"><strong>Aucun fichier</strong></td>
                                    </tr>
                                  <%} else{
                                        for(AttacherFichier fichier : fichiers){%> 
                                    <tr class="treegrid-1 treegrid-expanded">
                                        <td><span class="treegrid-expander glyphicon glyphicon-minus"></span></td>
                                        <td><%=fichier.getChemin() %></td>
                                        <td><%=Utilitaire.champNull(fichier.getLibelle())%></td>
                                        <td><a href="#" class="btn btn-primary" onclick="javascript:pagePopUp('<%=cdn + dossier +"/"+ fichier.getChemin()%>')">Voir</a></td>
                                        <td></td>
                                    </tr>
                                    <%}}%>
                                    
					
                                    </tbody>
                        </table>
				</div>
				</div>
            </div>
    </div>
                            
                            
</div>