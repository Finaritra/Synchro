<%@page import="mg.allosakafo.facturefournisseur.FactureFournisseurLettre"%>
<%@page import="mg.allosakafo.stock.BonDeLivraison"%>
<%@page import="historique.MapUtilisateur"%>
<%@page import="mg.allosakafo.reservation.Reservation"%>
<%@page import="java.util.Vector"%>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="bean.*" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="affichage.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
        String nomtable = null;
        String typeBoutton;
    %>
    <%
        try {
            nomtable = request.getParameter("nomtable");
            typeBoutton = request.getParameter("type");
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            bute = request.getParameter("bute");
            Object temp = null;
            String[] rajoutLien = null;
            String classe = request.getParameter("classe");
            ClassMAPTable t = null;
            String tempRajout = request.getParameter("rajoutLien");
            String val = "";
            String id = request.getParameter("id");

            String rajoutLie = "";
            if (tempRajout != null && tempRajout.compareToIgnoreCase("") != 0) {
                rajoutLien = utilitaire.Utilitaire.split(tempRajout, "-");
            }
            if (bute == null || bute.compareToIgnoreCase("") == 0) {
                bute = "pub/Pub.jsp";
            }

            if (classe == null || classe.compareToIgnoreCase("") == 0) {
                classe = "pub.Montant";
            }

            if (typeBoutton == null || typeBoutton.compareToIgnoreCase("") == 0) {
                typeBoutton = "3"; //par defaut modifier
            }
            
            if(classe == null || classe.compareToIgnoreCase("mg.allosakafo.appel.Appel") == 0){
                if(request.getParameter("motif").compareToIgnoreCase("ABT00004") == 0){
                    bute = "commande/as-commande-saisie.jsp";
                }
            }
            
            if (rajoutLien != null) {

                for (int o = 0; o < rajoutLien.length; o++) {
                    String valeur = request.getParameter(rajoutLien[o]);
                    rajoutLie = rajoutLie + "&" + rajoutLien[o] + "=" + valeur;
                 
                }
    
            }

            int type = Utilitaire.stringToInt(typeBoutton);
            
            if (acte.compareToIgnoreCase("disponiple") == 0) {
                String idProduit = request.getParameter("id");
                String dispo = request.getParameter("isdispo");
                u.produitDisponible(idProduit, dispo);
                bute = "produits/as-produits-fiche.jsp&id="+idProduit;
            }
            if (acte.compareToIgnoreCase("updatecommande") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
                System.out.println("request.getParameter(dateliv)"+request.getParameter("dateliv")); 
                int nb = Utilitaire.stringToInt(request.getParameter("nb"));
                
                Vector produits = new Vector();
                Vector rmqs = new Vector();
                Vector qttes = new Vector();
                Vector pus = new Vector();
                
                int indice = 0;
                for(int i = 0; i < nb; i++){
                    if(request.getParameter("quantite"+i) != null){
                        produits.add(request.getParameter("id"+i));
                        rmqs.add(request.getParameter("remarque"+i));
                        qttes.add(request.getParameter("quantite"+i));
                        pus.add(request.getParameter("pu"+i));
                        indice++;
                    }
                }
                String[] produit = new String[produits.size()];
                String[] rmq = new String[produits.size()];
                String[] qtte = new String[produits.size()];
                String[] pu = new String[produits.size()];
                
                produits.copyInto(produit);
                rmqs.copyInto(rmq);
                qttes.copyInto(qtte);
                pus.copyInto(pu);
                
                ClassMAPTable o = (ClassMAPTable)u.updateCommande(f, produit, qtte, pu, rmq);
                temp = (Object) o;
                if (o != null) {
                    id = o.getTuppleID();
                }
            }
            
            if (acte.compareToIgnoreCase("insertcommande") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
                
                int nb = Utilitaire.stringToInt(request.getParameter("nb"));
                
                Vector produits = new Vector();
                Vector rmqs = new Vector();
                Vector qttes = new Vector();
                Vector pus = new Vector();
                
                int indice = 0;
                for(int i = 0; i < nb; i++){
                    if(request.getParameter("quantite"+i) != null && request.getParameter("quantite"+i).compareToIgnoreCase("0") != 0){
                        produits.add(request.getParameter("id"+i));
                        rmqs.add(request.getParameter("remarque"+i));
                        qttes.add(request.getParameter("quantite"+i));
                        pus.add(request.getParameter("pu"+i));
                        indice++;
                    }
                }
                String[] produit = new String[produits.size()];
                String[] rmq = new String[produits.size()];
                String[] qtte = new String[produits.size()];
                String[] pu = new String[produits.size()];
                
                produits.copyInto(produit);
                rmqs.copyInto(rmq);
                qttes.copyInto(qtte);
                pus.copyInto(pu);
                
                ClassMAPTable o = (ClassMAPTable)u.createCommande(f, produit, qtte, pu, rmq);
                temp = (Object) o;
                if (o != null) {
                    id = o.getTuppleID();
                }
            }
            
            if (acte.compareToIgnoreCase("insertcommandemobile") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
                int nb = Utilitaire.stringToInt(request.getParameter("nb"));
                
                Vector produits = new Vector();
                Vector rmqs = new Vector();
                Vector qttes = new Vector();
                Vector pus = new Vector();
                
                int indice = 0;
                for(int i = 0; i < nb; i++){
                    if(request.getParameter("quantite"+i) != null && request.getParameter("quantite"+i).compareToIgnoreCase("0") != 0){
                        produits.add(request.getParameter("id"+i));
                        rmqs.add(request.getParameter("remarque"+i));
                        qttes.add(request.getParameter("quantite"+i));
                        pus.add(request.getParameter("pu"+i));
                        indice++;
                    }
                }
                String[] produit = new String[produits.size()];
                String[] rmq = new String[produits.size()];
                String[] qtte = new String[produits.size()];
                String[] pu = new String[produits.size()];
                
                produits.copyInto(produit);
                rmqs.copyInto(rmq);
                qttes.copyInto(qtte);
                pus.copyInto(pu);
                
                ClassMAPTable o = (ClassMAPTable)u.createCommandeMobile(f, produit, qtte, pu, rmq);
                
                temp = (Object) o;
                if (o != null) {
                    id = o.getTuppleID();
                }
            }
            
            if (acte.compareToIgnoreCase("fait") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
                t.setNomTable(nomtable);
                temp = t;
                u.updateEtatFait(t);
                val = t.getTuppleID();
            }
            
            if (acte.compareToIgnoreCase("insert") == 0) {
                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                PageInsert p = new PageInsert(t, request);
                ClassMAPTable f = p.getObjectAvecValeur();
                f.setNomTable(nomtable);
                ClassMAPTable o = (ClassMAPTable) u.createObject(f);
                temp = (Object) o;
                if (o != null) {
                    id = o.getTuppleID();
                }
            }
            

            if (acte.compareToIgnoreCase("deleteFille") == 0) {

                t = (ClassMAPTable) (Class.forName(classe).newInstance());
                t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
                temp = (Object) t;
                u.deleteObjetFille(t);
            }
            /**
             * ********************************************
             */
             
            

        /**
         * ********************************************
         */

        
        if (acte.compareToIgnoreCase("delete") == 0) {
                        String error = ""; %>
    <%//if(request.getParameter("confirm") != null){
        try {
            //System.out.println("suppression : " + request.getParameter("confirm") + " nom table : " + nomtable);
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            if (nomtable != null && !nomtable.isEmpty()) {
                t.setNomTable(nomtable);
            }
                   u.deleteObject(t);
               } catch (Exception e) {%>
    <script language="JavaScript">alert('<%=e.getMessage()%>');history.back();</script>
    <%
        }
    
        }
        if (acte.compareToIgnoreCase("update") == 0) {
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            Page p = new Page(t, request);
            ClassMAPTable f = p.getObjectAvecValeur();
            temp = f;
			if(nomtable!=null){
				f.setNomTable(nomtable);
			}
			
			           
            u.updateObject(f);
        }
        if (acte.compareToIgnoreCase("dupliquer") == 0) {
            String classeFille = request.getParameter("nomClasseFille");
            String nomColonneMere = request.getParameter("nomColonneMere");
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            t.setNomTable(nomtable);
            temp = t;
            Object o = u.dupliquerObject(t, classeFille, nomColonneMere);
            val = o.toString();
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>&id=<%=val%>");</script>
    <%
        }
        
        if (acte.compareToIgnoreCase("annuler") == 0) {
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            t.setNomTable(nomtable);
            temp = t;
            u.annulerObject(t);
        }

        if (acte.compareToIgnoreCase("annulerVisa") == 0) {
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            temp = t;
            u.annulerVisa(t);
        }
        if (acte.compareToIgnoreCase("finaliser") == 0) {
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            temp = t;
            u.finaliser(t);
        }

        if (acte.compareToIgnoreCase("valider") == 0) {     // VISER
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            if(t instanceof ClassEtat)
            {
                ClassEtat ce=(ClassEtat)t;
                String et=request.getParameter("etat");
                int etat=Utilitaire.stringToInt(et);
                if (etat>0)ce.setEtat(etat);
            }
            t.setNomTable(nomtable);
            ClassMAPTable o = (ClassMAPTable) u.validerObject(t);
            temp = t;
            val = o.getTuppleID();
        }
        
        if(acte.compareToIgnoreCase("payer") == 0){
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            t.setNomTable(nomtable);
            /*ClassMAPTable o = (ClassMAPTable) u.updateEtatCommande(t,acte);
            temp = t;
            val = o.getTuppleID();*/
            ClassMAPTable o = (ClassMAPTable) u.payerCommande(id);
            temp = t;
            val = o.getTuppleID();
        }
        if(acte.compareToIgnoreCase("livrer") == 0){
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            t.setNomTable(nomtable);
            /*ClassMAPTable o = (ClassMAPTable) u.updateEtatCommande(t,acte);
            temp = t;
            val = o.getTuppleID();*/
            ClassMAPTable o = (ClassMAPTable) u.livrerCommande(id);
            temp = t;
            val = o.getTuppleID();
        }
        if (acte.compareToIgnoreCase("rejeter") == 0) {
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            PageInsert p = new PageInsert(t, request);
            ClassMAPTable f = p.getObjectAvecValeur();
            t.setNomTable(nomtable);
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            u.rejeterObject(t);
        }
        if (acte.compareToIgnoreCase("cloturer") == 0) {
            t = (ClassMAPTable) (Class.forName(classe).newInstance());
            t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
            u.cloturerObject(t);
        }

         if (acte.compareToIgnoreCase("effectuer") == 0) {     // VISER
            Reservation res = (Reservation) (Class.forName(classe).newInstance());
            res.setId(request.getParameter("id")) ;
            res.setNomTable(nomtable);
            res.effectuerReservation(u.getUser().getTuppleID(),null);
        }
        if (acte.compareToIgnoreCase("livrerbl") == 0) {     // VISER
            FactureFournisseurLettre res =new FactureFournisseurLettre();
            res.setId(request.getParameter("idfacture")) ;
            id=res.livrerBL(u.getUser().getTuppleID(),null);
        }


        
        
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&valeur=<%=val%>&id=<%=Utilitaire.champNull(id)%>");</script>
    <%

    } catch (Exception e) {
        e.printStackTrace();
    %>

    <script language="JavaScript"> alert('<%=e.getMessage()%>');
        history.back();</script>
        <%
                return;
            }
        %>
</html>



