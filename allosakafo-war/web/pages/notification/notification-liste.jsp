
<%@page import="historique.MapUtilisateur"%>
<%@page import="user.UserEJB"%>
<%@page import="bean.TypeObjet"%>
<%@page import="bean.CGenUtil"%>
<%@page import="mg.cnaps.notification.NotificationLibelle"%>
<%@page import="affichage.PageRecherche"%>

<% 
    UserEJB u = (UserEJB) session.getAttribute("u");
    MapUtilisateur mu = u.getUser();
    NotificationLibelle notif = new NotificationLibelle();
    
    if (request.getParameter("etat") != null && !request.getParameter("etat").equals("")) {
        notif.setNomTable(request.getParameter("etat"));
    } else {
        notif.setNomTable("NOTIFICATION_LIBELLE2");
    }
     
    UserEJB ue = (UserEJB) session.getValue("u");
    int inboxNotif = ue.getNotificationNonLus();         
            
    String listeCrt[] = {"id", "objet", "expediteur", "daty", "destinataire", "service", "direction"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "objet", "expediteur", "daty", "destinataire", "service", "direction"};
    PageRecherche pr = new PageRecherche(notif, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date max");
    pr.setApres("notification/notification-liste.jsp");
    String[] colSomme = null;
    
    pr.creerObjetPage(libEntete, colSomme);
%>

<% if(inboxNotif>0){%>
    <script>
        $( document ).ready(function() {
            $.notify("Vous avez <%=inboxNotif%> notification(s) non lue(s)", "success");
        });
    </script>
<% } %>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Notifications</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=notification/notification-liste.jsp" method="post" name="notification" id="notification">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row">           
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Etat : </label>
                        <select name="etat" class="champ form-control" id="etat" style="display: inline-block; width: 250px;">
                            <% if (request.getParameter("etat") == null || request.getParameter("etat").compareToIgnoreCase("") == 0) {%>
                            <option value="" selected>Tous </option>
                            <% } else { %>
                            <option value="">Tous</option>
                            <% } %>

                            <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("NOTIFICATION_LIBELLE_LU") == 0) {%>
                            <option value="NOTIFICATION_LIBELLE_LU" selected>Lu</option>
                            <% } else { %>
                            <option value="NOTIFICATION_LIBELLE_LU">Lu</option>
                            <% } %>

                            <% if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("NOTIFICATION_LIBELLE_NONLU") == 0) {%>
                            <option value="NOTIFICATION_LIBELLE_NONLU" selected>Non lu</option>
                            <% } else { %>
                            <option value="NOTIFICATION_LIBELLE_NONLU">Non lu</option>
                            <% } %>

                        </select>
                    </div>
                </div>
            </div>
		   
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=notification/notification-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"ID", "Objet", "Expediteur", "Date", "Destinataire", "Service Dest", "Direction Dest"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
