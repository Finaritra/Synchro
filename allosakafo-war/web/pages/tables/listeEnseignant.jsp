<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="matiere.*" %>
<%

Enseignant e=new Enseignant();
e.setNomTable("enseignantlib");

String listeCrt[]={"nom","prenom","dateNaissance","catDiplome","diplome"};
String listeInt[]=null;
String libEntete[]={"id","nom","prenom","catDiplome","diplome"};
PageRecherche pr=new PageRecherche(e,request,listeCrt,listeInt,2,libEntete,5);
pr.setUtilisateur((user.UserEJB)session.getValue("u"));
pr.setLien((String)session.getValue("lien"));
pr.setApres("tables/listeEnseignant.jsp");
affichage.Champ[] liste=new affichage.Champ[1];
TypeObjet cat=new TypeObjet();
cat.setNomTable("categoriediplome");
liste[0]=new Liste("catDiplome",cat,"val","val");
pr.getFormu().changerEnChamp(liste);
pr.getFormu().getChamp("dateNaissance").setLibelle("Date de naissance");

String[]colSomme=null;
pr.creerObjetPage(libEntete,colSomme);
%>
<div class="content-wrapper">
	<h1>Liste Enseignant</h1>
	<div class="row">
		<form action="<%=pr.getLien()%>?but=tables/listeEnseignant.jsp" method="post" name="listeenseignant" id="listeenseignant">
			<% out.println(pr.getFormu().getHtmlEnsemble());%>
		</form>
	</div>
	<div class="row">
		<%
		String lienTableau[]={pr.getLien()+"?but=tables/ficheEnseignant.jsp"};
		String colonneLien[]={"id"};
		pr.getTableau().setLien(lienTableau);
		pr.getTableau().setColonneLien(colonneLien);
		out.println(pr.getTableauRecap().getHtml());
		%>
	</div>
	<div class="row">
		<% out.println(pr.getTableau().getHtml()); %>
	</div>
	<div class="row">
		<% out.println(pr.getBasPage()); %>
	</div>
</div>
