<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="matiere.*" %>
<%!
Matiere p;
%>
<%
p=new Matiere();
//p.setNomTable("matierelib");
PageConsulte pc=new PageConsulte(p,request,(user.UserEJB)session.getValue("u"));//ou avec argument liste Libelle si besoin
%>
<div class="content-wrapper">
	<div class="row">
		<div class="col-md-6">
			<div class="box"> 
				<div class="box-title with-border">
					<h1 class="box-title">Fiche d'une mati&egrave;re</h1>
				</div>
				<div class="box-body">
					<%
					out.println(pc.getHtml());
					%>
				</div>
				<div class="box-footer">
					<p>
						<a href="<%=(String)session.getValue("lien")+"?but=tables/modifMatiere.jsp&id="+request.getParameter("id")%>"><button class="btn btn-warning">Modifier</button></a>
						<a href="<%=(String)session.getValue("lien")+"?but=apresTarif.jsp&id="+request.getParameter("id")%>&acte=delete&bute=tables/listeMatiere.jsp&classe=matiere.Matiere"><button class="btn btn-danger">Supprimer</button></a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
