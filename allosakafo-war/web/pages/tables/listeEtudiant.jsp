<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="etudiant.*" %>
<%@ page import="promotion.*" %>
<%

EtudiantPromo e=new EtudiantPromo();
String etat="ETUDIANTPROMOLIB2";
if(request.getParameter("etat")!=null && request.getParameter("etat").compareToIgnoreCase("")!=0)
  etat=request.getParameter("etat");
e.setNomTable(etat);
String listeCrt[]={"nom","prenom","datenaissance","sexe","ecole","nature","promotion","daty","numero","proffpere"};
String listeInt[]={"datenaissance"};
String libEntete[]={"id","numero","nom","prenom","datenaissance","sexe","promotion","daty"};

//PageRecherche pr=new PageRecherche(e,request,listeCrt,listeInt,3);
PageRecherche pr=new PageRecherche(e,request,listeCrt,listeInt,3,libEntete,8);
pr.setUtilisateur((user.UserEJB)session.getValue("u"));
pr.setLien((String)session.getValue("lien"));
pr.setApres("tables/listeEtudiant.jsp");
affichage.Champ[] liste=new affichage.Champ[7];

TypeObjet prof1=new TypeObjet();
prof1.setNomTable("TYPEPROF");
liste[0]=new Liste("proffpere",prof1,"val","val");

TypeObjet prof2=new TypeObjet();
prof2.setNomTable("TYPEPROF");
liste[1]=new Liste("profmere",prof2,"val","val");

TypeObjet sexe=new TypeObjet();
sexe.setNomTable("SEXE");
liste[2]=new Liste("sexe",sexe,"val","val");

Ecole ecole=new Ecole();
liste[3]=new Liste("ecole",ecole,"nom","nom");

TypeObjet nature=new TypeObjet();
nature.setNomTable("nature");
liste[4]=new Liste("nature",nature,"val","val");

TypeObjet pays=new TypeObjet();
pays.setNomTable("pays");
liste[5]=new Liste("pays",pays,"val","val");

Promotion promo=new Promotion();
liste[6]=new Liste("promotion",promo,"nom","nom");
pr.getFormu().changerEnChamp(liste);

pr.getFormu().getChamp("ecole").setLibelleAffiche("Provenance");
pr.getFormu().getChamp("daty").setLibelleAffiche("Daty entree univ");

String[]colSomme=null;
pr.creerObjetPage(libEntete,colSomme);
%>
<div class="content-wrapper">
	<div class="row">
		<form action="<%=pr.getLien()%>?but=tables/listeEtudiant.jsp" method="post" name="listeetudiant" id="listeetudiant">
			<% out.println(pr.getFormu().getHtmlEnsemble());%>
		</form>
	</div>
	<div class="row">
		<%
		String lienTableau[]={pr.getLien()+"?but=tables/ficheEtudiant.jsp"};
		String colonneLien[]={"id"};
		pr.getTableau().setLien(lienTableau);
		pr.getTableau().setColonneLien(colonneLien);
		out.println(pr.getTableauRecap().getHtml());
		%>
	</div>
	<div class="row">
		<% out.println(pr.getTableau().getHtml()); %>
	</div>
	<div class="row">
		<% out.println(pr.getBasPage()); %>
	</div>
</div>