<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="matiere.*" %>
<%

Matiere mat=new Matiere();

mat.setNomTable("matierelib");
String listeCrt[]={"code","intitule","typematiere","responsable","semestre","coeff"};
String listeInt[]=null;
String libEntete[]={"id","code","intitule","responsable","semestre","coeff"};
PageRecherche pr=new PageRecherche(mat,request,listeCrt,listeInt,3);
//PageRecherche pr=new PageRecherche(mat,request,listeCrt,listeInt,3,libEntete,4);
pr.setUtilisateur((user.UserEJB)session.getValue("u"));
pr.setLien((String)session.getValue("lien"));
pr.setApres("tables/listeMatiere.jsp");
affichage.Champ[] liste=new affichage.Champ[3];
TypeObjet semestre=new TypeObjet();
semestre.setNomTable("semestre");

liste[0]=new Liste("semestre",semestre,"desce","id");
TypeObjet type=new TypeObjet();
type.setNomTable("typematiere");
liste[1]=new Liste("typematiere",type,"val","id");
EnseignantSimple resp=new EnseignantSimple();
liste[2]=new Liste("responsable",resp,"prof","id");

pr.getFormu().changerEnChamp(liste);

String[]colSomme=null;
pr.creerObjetPage(libEntete,colSomme);
%>
<style>
h3 {
    color: #edb031;
}
</style>
<div class="content-wrapper">
<h1>Liste Mati&egrave;re</h1>
	<div class="row">
		<form action="<%=pr.getLien()%>?but=tables/listeMatiere.jsp" method="post" name="listematiere" id="listematiere">
			<% out.println(pr.getFormu().getHtmlEnsemble()); %>
		</form>
	</div>
	<div class="row">
		<%
		String lienTableau[]={pr.getLien()+"?but=tables/ficheMatiere.jsp"};
		String colonneLien[]={"id"};
		pr.getTableau().setLien(lienTableau);
		pr.getTableau().setColonneLien(colonneLien);
		out.println(pr.getTableauRecap().getHtml());
		%>
	</div>
	<div class="row">
		<% out.println(pr.getTableau().getHtml()); %>
	</div>
	<div class="row">
		<% out.println(pr.getBasPage()); %>
	</div>
</div>