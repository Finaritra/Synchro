<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>
<%
SigTravailleurs st = new SigTravailleurs();

String listeCrt[]={"id","code","travailleur_observations","travailleur_date_dern_maj"};
String listeInt[]={"travailleur_date_dern_maj"};
String libEntete[]={"id","code","travailleur_observations","travailleur_date_dern_maj"};
PageRecherche pr=new PageRecherche(st,request,listeCrt,listeInt,3,libEntete,19);
pr.setUtilisateur((user.UserEJB)session.getValue("u"));
pr.setLien((String)session.getValue("lien"));
pr.setApres("tables/travailleur-liste.jsp");
pr.getFormu().getChamp("travailleur_observations").setLibelle("Observation");
pr.getFormu().getChamp("travailleur_date_dern_maj").setLibelle("Date de derni�re mise � jour");


String[]colSomme=null;
pr.creerObjetPage(libEntete,colSomme);
%>
<div class="content-wrapper">
	<h1>Liste Travailleur</h1>
	<div class="row">
		<form action="<%=pr.getLien()%>?but=tables/travailleur-liste.jsp" method="post" name="travailleurliste" id="travailleurliste">
			<% out.println(pr.getFormu().getHtmlEnsemble());%>
		</form>
	</div>
	<div class="row">
		<%
		String lienTableau[]={pr.getLien()+"?but=tables/travailleur-liste.jsp"};
		String colonneLien[]={"id"};
		pr.getTableau().setLien(lienTableau);
		pr.getTableau().setColonneLien(colonneLien);
		out.println(pr.getTableauRecap().getHtml());
		%>
	</div>
	<div class="row">
		<% out.println(pr.getTableau().getHtml()); %>
	</div>
	<div class="row">
		<% out.println(pr.getBasPage()); %>
	</div>
</div>
