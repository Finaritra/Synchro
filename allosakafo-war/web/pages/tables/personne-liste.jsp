<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>
<%
SigPersonnes sp = new SigPersonnes();
sp.setNomTable("vuepersonne");

String listeCrt[]={"id","nom","prenom","sex","numero_cin", "date_cin", "date_duplicata", "lieu_cin", "acte_naissance", "date_naissance", "nationalite", "pers_lieu_naiss", "pere", "mere", "date_deces", "date_retraite", "pers_date_retraite", "lieu_naissance", "lieu_adresse", "andresse_lot"};
String listeInt[]={"date_cin", "date_duplicata", "date_naissance", "date_deces", "date_retraite"};
String libEntete[]={"id","nom","prenom","sex","numero_cin", "date_cin", "date_duplicata", "lieu_cin", "acte_naissance", "date_naissance", "nationalite", "pers_lieu_naiss", "pere", "mere", "date_deces", "date_retraite", "pers_date_retraite", "lieu_naissance", "lieu_adresse", "andresse_lot"};
PageRecherche pr=new PageRecherche(sp,request,listeCrt,listeInt,3,libEntete,19);
pr.setUtilisateur((user.UserEJB)session.getValue("u"));
pr.setLien((String)session.getValue("lien"));
pr.setApres("tables/personne-liste.jsp");
pr.getFormu().getChamp("nom").setLibelle("Nom");
pr.getFormu().getChamp("prenom").setLibelle("Prenom");
pr.getFormu().getChamp("sex").setLibelle("Sexe");
pr.getFormu().getChamp("numero_cin").setLibelle("Num�ro CIN");
pr.getFormu().getChamp("date_cin").setLibelle("Date d�livrance");
pr.getFormu().getChamp("date_duplicata").setLibelle("Date duplicata");
pr.getFormu().getChamp("lieu_cin").setLibelle("Lieu de d�livrance");
pr.getFormu().getChamp("acte_naissance").setLibelle("Acte de naissance");
pr.getFormu().getChamp("date_naissance").setLibelle("Date de naissance");
pr.getFormu().getChamp("lieu_naissance").setLibelle("Commune de naissance");
pr.getFormu().getChamp("nationalite").setLibelle("Nationalit�");
pr.getFormu().getChamp("pere").setLibelle("Nom du p�re");
pr.getFormu().getChamp("mere").setLibelle("Nom de la m�re");
pr.getFormu().getChamp("date_deces").setLibelle("Date de d�c�s");
pr.getFormu().getChamp("lieu_adresse").setLibelle("Date de retraite");
pr.getFormu().getChamp("andresse_lot").setLibelle("Adresse");

String[]colSomme=null;
pr.creerObjetPage(libEntete,colSomme);
%>
<div class="content-wrapper">
	<h1>Liste Personne</h1>
	<div class="row">
		<form action="<%=pr.getLien()%>?but=tables/personne-liste.jsp" method="post" name="personneliste" id="personneliste">
			<% out.println(pr.getFormu().getHtmlEnsemble());%>
		</form>
	</div>
	<div class="row">
		<%
		String lienTableau[]={pr.getLien()+"?but=tables/personne-fiche.jsp"};
		String colonneLien[]={"id"};
		pr.getTableau().setLien(lienTableau);
		pr.getTableau().setColonneLien(colonneLien);
		out.println(pr.getTableauRecap().getHtml());
		%>
	</div>
	<div class="row">
		<% out.println(pr.getTableau().getHtml()); %>
	</div>
	<div class="row">
		<% out.println(pr.getBasPage()); %>
	</div>
</div>
