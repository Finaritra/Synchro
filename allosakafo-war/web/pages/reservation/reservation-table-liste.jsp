<%-- 
    Document   : reservation-liste
    Created on : 8 oct. 2019, 15:19:50
    Author     : ionyr
--%>
<%@page import="mg.allosakafo.reservation.ReservationComplet"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="user.UserEJB"%>

<%
    ReservationComplet res = new ReservationComplet();
    //res.setNomTable("resa_table_libcomplet");
    String listeCrt[] = {"id", "idreservation", "idtable", "datereservation", "heuredebutreservation", "heurefinreservation"};
    String listeInt[] = {"datereservation", "nbpersonne"};
    String libEntete[] = {"id", "idreservation", "idtable", "datereservation", "heuredebutreservation", "heurefinreservation", "nbpersonne"};

    PageRecherche pr = new PageRecherche(res, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("idreservation").setLibelle("id Reservation");
    pr.getFormu().getChamp("idtable").setLibelle("id Table");
    pr.getFormu().getChamp("datereservation1").setLibelle("Date reservation min");
    pr.getFormu().getChamp("datereservation2").setLibelle("Date reservation max");
    pr.getFormu().getChamp("heuredebutreservation").setLibelle("Heure debut reservation ");
    pr.getFormu().getChamp("heurefinreservation").setLibelle("Heure fin reservation ");

    pr.setApres("reservation/reservation-table-liste.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste reservation table</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=reservation/reservation-table-liste.jsp" method="post" name="reservation" id="reservation">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Etat : </label>
                    <select name="etatvue" class="champ form-control" id="etatvue" onchange="changerDesignation()" style="display: inline-block; width: 250px;">
                         <option value="ReservationLibtous" <%if (request.getParameter("etatvue") != null && request.getParameter("etatvue").compareToIgnoreCase("ReservationLibtous") == 0) {
                                out.print("selected");
                            }%>>Tous</option>
                        <option value="ReservationLibcree" <%if (request.getParameter("etatvue") != null && request.getParameter("etatvue").compareToIgnoreCase("ReservationLibcree") == 0) {
                                out.print("selected");
                            }%>>Cr&eacute;e</option>
                        <option value="ReservationLibvalide" <%if (request.getParameter("etatvue") != null && request.getParameter("etatvue").compareToIgnoreCase("ReservationLibvalide") == 0) {
                                out.print("selected");
                            }%>>Valid&eacute;</option>
                        <option value="ReservationLibeffectue" <% if (request.getParameter("etatvue") == null || request.getParameter("etatvue").compareToIgnoreCase("ReservationLibeffectue") == 0) {
                                out.print(" selected");
                            }%>>Effectu&eacute;</option>
                    </select>
                </div>
            </div>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=reservation/reservation-table-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "id Reservation", "id Table", "date reservation", "heure debut reservation", "heure fin reservation", "nombre de personnes"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>