<%-- 
    Document   : reservation-saisie
    Created on : 8 oct. 2019, 11:01:17
    Author     : ionyr
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.facture.ClientPoint"%>
<%@page import="mg.allosakafo.reservation.ReservationTable"%>
<%@page import="user.*"%>
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<% try{
    String autreparsley = "data-parsley-range='[8, 40]' required";
    String idres=request.getParameter("idreservation");
    
    ReservationTable  a = new ReservationTable();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    pi.getFormu().getChamp("idReservation").setLibelle("Reservation");
    pi.getFormu().getChamp("idReservation").setPageAppel("choix/listeReservationChoix.jsp");
    pi.getFormu().getChamp("idReservation").setAutre("readonly");
    if(idres!=null && idres.compareTo("")!=0){
        pi.getFormu().getChamp("idReservation").setDefaut(idres);
    }
    
    pi.getFormu().getChamp("idtable").setLibelle("Table");
    pi.getFormu().getChamp("idtable").setPageAppel("choix/TablePointChoix.jsp");
    
    
//    
//    affichage.Champ[] liste = new affichage.Champ[1];
//    
//    ClientPoint op = new ClientPoint();
//    op.setNomTable("tableclientpointvue");
//    op.setPoint(AlloSakafoService.getIdRestau(null).getId());
//    System.out.println("Je suis ici");
//    liste[0] = new Liste("idtable", op, "VAL", "id");
//    
//    pi.getFormu().changerEnChamp(liste);
//    
    
    //pi.getFormu().getChamp("idtable").setPageAppel("choix/listeTableChoix.jsp");
    pi.getFormu().getChamp("idtable").setAutre("readonly");
    pi.getFormu().getChamp("etat").setVisible(false);
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Saisie Reservation table</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="reservation-saisie" id="reservation-saisie">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="reservation/reservation-fiche.jsp&id=<%=idres%>">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.reservation.ReservationTable">
    </form>
    
</div>
    <%} catch(Exception e){
        e.printStackTrace();
}%>

