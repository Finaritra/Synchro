<%-- 
    Document   : reservation-modif
    Created on : 8 oct. 2019, 15:22:40
    Author     : ionyr
--%>

<%@page import="mg.allosakafo.reservation.Reservation"%>
<%@ page import="user.*"%>
<%@ page import="bean.*"%>
<%@ page import="utilitaire.*"%>
<%@ page import="affichage.*"%>
<%@page import="mg.allosakafo.appel.Appel"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    Reservation da = new Reservation();
    PageUpdate pi = new PageUpdate(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
   
    pi.getFormu().getChamp("dateSaisie").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("heureSaisie").setDefaut(Utilitaire.heureCourante());
    pi.getFormu().getChamp("source").setLibelle("Source");
    pi.getFormu().getChamp("heureDebutReservation").setLibelle("Heure debut de reservation");
    pi.getFormu().getChamp("heureFinReservation").setLibelle("Heure fin de reservation"); 
    pi.getFormu().getChamp("dateReservation").setLibelle("Date de reservation");
    pi.getFormu().getChamp("nbPersonne").setLibelle("Nombre de personne");
    pi.getFormu().getChamp("dateSaisie").setVisible(false); 
    pi.getFormu().getChamp("heureSaisie").setVisible(false);
    pi.getFormu().getChamp("etat").setVisible(false);
    pi.getFormu().getChamp("idClient").setPageAppel("choix/listeClientChoix.jsp");
    pi.getFormu().getChamp("idClient").setAutre("readonly");
    pi.getFormu().getChamp("idClient").setLibelle("Client");
    pi.getFormu().getChamp("idCommande").setPageAppel("choix/listeCommandeChoix.jsp");
    pi.getFormu().getChamp("idCommande").setLibelle("Commande");
    pi.getFormu().getChamp("idCommande").setAutre("readonly");
    
    pi.getFormu().getChamp("source").setLibelle("Source");
    
    affichage.Champ[] liste = new affichage.Champ[2];
    TypeObjet op = new TypeObjet();
    op.setNomTable("source");
    liste[0] = new Liste("source", op, "VAL", "id");
     TypeObjet d1 = new TypeObjet();
    d1.setNomTable("point");
    liste[1] = new Liste("point", d1, "val", "id");
    pi.getFormu().changerEnChamp(liste);
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="recettebordereau" id="recettebordereau">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="reservation/reservation-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.reservation.Reservation">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                        <input name="nomtable" type="hidden" id="nomtable" value="as_reservation">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>