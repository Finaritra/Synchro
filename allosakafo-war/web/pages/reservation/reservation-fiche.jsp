<%-- 
    Document   : reservation-fiche
    Created on : 8 oct. 2019, 15:20:20
    Author     : ionyr
--%>

<%@page import="mg.allosakafo.reservation.ReservationLib"%>
<%@page import="mg.allosakafo.reservation.Reservation"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    try{
    ReservationLib a = new ReservationLib();
    
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    /*  private String id, source, heuresaisie, heuredebutreservation, heurefinreservation;
    private int nbpersonne;
    private Date datesaisie, datereservation;
    private String idClient,idCommande;
     private String telephone,tablenom;*/
    pc.getChampByName("source").setLibelle("Source");
    pc.getChampByName("heuresaisie").setVisible(false);
    pc.getChampByName("heurefinreservation").setVisible(false);
    pc.getChampByName("datesaisie").setVisible(false);
    pc.getChampByName("heuredebutreservation").setLibelle("Heure debut de reservation");
    pc.getChampByName("datereservation").setLibelle("Date de reservation");
    pc.getChampByName("nbpersonne").setLibelle("Nombre de personne");
    pc.getChampByName("idClient").setLibelle("Client");
    pc.getChampByName("etat").setLibelle("Etat");
    pc.getChampByName("point").setVisible(false);
    pc.getChampByName("idpointlibelle").setLibelle("Point");
    pc.getChampByName("tablenom").setLibelle("Nom du Table");
    pc.setTitre("Consultation Reservation");

%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=reservation/reservation-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-warning pull-left"  href="<%=(String) session.getValue("lien") + "?but=reservation/reservation-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=valider&bute=reservation/reservation-fiche.jsp&classe=mg.allosakafo.reservation.Reservation" style="margin-right: 10px">Viser</a>
                            <a class="btn btn-primary pull-left"  href="<%=(String) session.getValue("lien") + "?but=reservation/reservation-table-saisie.jsp&idreservation=" + request.getParameter("id")%>" style="margin-right: 10px">Attribution table</a>
                            <a class="btn btn-primary pull-left"   href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=effectuer&bute=reservation/reservation-fiche.jsp&classe=mg.allosakafo.reservation.Reservation" style="margin-right: 10px">Effectue</a>
                        
                        </div>
                        <br/>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                            <% } catch(Exception e){e.printStackTrace(); }%>