<%-- 
    Document   : reservation-table-fiche
    Created on : 3 f�vr. 2020, 11:52:01
    Author     : john
--%>

<%@page import="mg.allosakafo.reservation.ReservationComplet"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    ReservationComplet a = new ReservationComplet();
    PageConsulte pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.getChampByName("id").setLibelle("Id");
    pc.getChampByName("idreservation").setLibelle("R&eacute;servation");
    pc.getChampByName("idTable").setLibelle("Table"); 
    pc.getChampByName("etat").setLibelle("Etat");
    pc.getChampByName("heuredebutreservation").setLibelle("Heure debut r&eacute;servation");
    pc.getChampByName("heurefinreservation").setLibelle("Heure fin r&eacute;servation");
    pc.getChampByName("datereservation").setLibelle("Date r&eacute;servation");
    pc.getChampByName("nbpersonne").setLibelle("Nb personne");
    pc.setTitre("Consultation R&eacute;servation");

%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=reservation/reservation-table-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-warning pull-left"  href="<%=(String) session.getValue("lien") + "?but=reservation/reservation-table-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                        </div>
                        <br/>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
