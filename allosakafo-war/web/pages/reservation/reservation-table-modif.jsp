<%-- 
    Document   : reservation-modif
    Created on : 8 oct. 2019, 15:22:40
    Author     : ionyr
--%>

<%@page import="mg.allosakafo.reservation.ReservationTable"%>
<%@ page import="user.*"%>
<%@ page import="bean.*"%>
<%@ page import="utilitaire.*"%>
<%@ page import="affichage.*"%>
<%@page import="mg.allosakafo.appel.Appel"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    ReservationTable da = new ReservationTable();
    PageUpdate pi = new PageUpdate(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
   
//    pi.getFormu().getChamp("dateSaisie").setDefaut(Utilitaire.dateDuJour());
//    pi.getFormu().getChamp("heureSaisie").setDefaut(Utilitaire.heureCourante());
//    pi.getFormu().getChamp("source").setLibelle("Source");
//    pi.getFormu().getChamp("heureDebutReservation").setLibelle("Heure debut de reservation");
//    pi.getFormu().getChamp("heureFinReservation").setLibelle("Heure fin de reservation"); 
//    pi.getFormu().getChamp("dateReservation").setLibelle("Date de reservation");
//    pi.getFormu().getChamp("nbPersonne").setLibelle("Nombre de personne");
//    pi.getFormu().getChamp("dateSaisie").setVisible(false); 
//    pi.getFormu().getChamp("heureSaisie").setVisible(false);
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1>Modification</h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="reservationtable" id="reservationtable">
                        <%
                            out.println(pi.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="update">
                        <input name="bute" type="hidden" id="bute" value="reservation/reservation-table-modif.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.reservation.ReservationTable">
                        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-<%out.print(request.getParameter("id"));%>" >
                        <input name="nomtable" type="hidden" id="nomtable" value="resa_table">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>