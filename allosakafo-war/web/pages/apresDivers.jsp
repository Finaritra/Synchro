<%-- 
    Document   : apresDivers
    Created on : 18 sept. 2020, 16:43:28
    Author     : Axel
--%>

<%@page import="utilitaire.Utilitaire"%>
<%@page import="user.UserEJB"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <%!
        UserEJB u = null;
        String acte = null;
        String lien = null;
        String bute;
        String nomtable = null;
        String typeBoutton;
        String val = "";
        String rajoutLie = "";
    %>
    <%
        try {
            nomtable = request.getParameter("nomtable");
            typeBoutton = request.getParameter("type");
            lien = (String) session.getValue("lien");
            u = (UserEJB) session.getAttribute("u");
            acte = request.getParameter("acte");
            bute = request.getParameter("bute");
            String id = request.getParameter("id");

            if (acte.compareToIgnoreCase("ingredients_dispo") == 0) {
                bute = "produits/as-ingredients-fiche.jsp&id=" + id;
            }
            if (acte.compareToIgnoreCase("ingredients_indispo") == 0) {
                bute = "produits/as-ingredients-fiche.jsp&id=" + id;
            }

    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&valeur=<%=val%>&id=<%=Utilitaire.champNull(id)%>");</script>
    <%            
            
        } catch (Exception e) {
            e.printStackTrace();
    %>
    <script language="JavaScript"> alert('<%=e.getMessage()%>'); history.back();</script>
    <%
            return;
        }
    %>
</html>
