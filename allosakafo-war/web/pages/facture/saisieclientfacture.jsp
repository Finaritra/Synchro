<%@page import="mg.allosakafo.facture.FactureVue"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsTypeP"%>
<%@page import="mg.allosakafo.facture.Facture"%>
<%@page import="user.*"%>
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%
    try{
        String autreparsley = "data-parsley-range='[8, 40]' required";
        FactureVue  a = new FactureVue();
        PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));

        pi.setLien((String) session.getValue("lien"));    

        pi.getFormu().getChamp("adresseclient").setLibelle("Adresse Client");
        pi.getFormu().getChamp("modepaiement").setLibelle("Mode de Paiement");
        pi.getFormu().getChamp("datefacture").setLibelle("Date Facture");
        pi.getFormu().getChamp("detail").setLibelle("Detail Commande");
        pi.getFormu().getChamp("detail").setType("textarea");
        pi.getFormu().getChamp("detail").setDefaut(Utilitaire.tabToString(request.getParameterValues("id"), "", ";"));
        
        pi.getFormu().getChamp("datefacture").setDefaut(Utilitaire.dateDuJour());
        
        affichage.Champ[] liste = new affichage.Champ[1]; 
        TypeObjet mvt = new TypeObjet();
        mvt.setNomTable("modepaiement");
        liste[0] = new Liste("modepaiement", mvt, "VAL", "id");
        pi.getFormu().changerEnChamp(liste);
        pi.getFormu().getChamp("client").setAutre("Placeholder='T�l�phone-Nom'");
        pi.getFormu().getChamp("client").setValeur(request.getParameter("client"));
        pi.getFormu().getChamp("datesaisie").setVisible(false);
        pi.getFormu().getChamp("modepaiement").setDefaut("pay1");
        pi.getFormu().getChamp("montant").setVisible(false);
        pi.getFormu().getChamp("adresseclient").setVisible(false);
        
        
        pi.preparerDataFormu();
%>
<form action="<%=pi.getLien()%>?but=modifierEtatMultiple.jsp" method="post" name="factureclient" id="factureclient">
<div class="content-wrapper">
    <h1>Saisie Facture Client</h1>
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="facturer">
    <input name="bute" type="hidden" id="bute" value="commande/facture.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.facture.Facture">
</div>
</form>
<% }catch(Exception e){
    e.printStackTrace();
    System.out.println("Messs=="+e.getMessage());
}%>
