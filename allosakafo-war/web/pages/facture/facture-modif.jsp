<%-- 
    Document   : factureclient-modif
    Created on : 10 f�vr. 2020, 17:23:13
    Author     : Admin
--%>

<%@page import="affichage.PageUpdate"%>
<%@page import="user.UserEJB"%>
<%@page import="mg.allosakafo.facture.Facture"%>
<%@page import="affichage.*"%>
<%@ page import="bean.TypeObjet" %>
<%
    try{
        Facture facture = new Facture();
        
        PageUpdate pi = new PageUpdate(facture, request, (user.UserEJB) session.getValue("u"));
        pi.setLien((String) session.getValue("lien"));
        UserEJB u = (UserEJB) session.getAttribute("u");

        affichage.Champ[] liste = new affichage.Champ[1];
        
        TypeObjet op = new TypeObjet();
        op.setNomTable("modepaiement");
        liste[0] = new Liste("modepaiement", op, "VAL", "id");
        
        pi.getFormu().changerEnChamp(liste);
        
        pi.getFormu().getChamp("modepaiement").setLibelle("Mode de paiement");
        pi.getFormu().getChamp("adresseclient").setLibelle("Adresse du client");
        pi.getFormu().getChamp("modepaiement").setLibelle("Mode de paiement");
        pi.getFormu().getChamp("datefacture").setLibelle("Date facture");
        pi.getFormu().getChamp("datesaisie").setVisible(false);
        pi.getFormu().getChamp("etat").setVisible(false);
        
        pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 class="box-title">Modification facture</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro">
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="acte" value="update">
        <input name="bute" type="hidden" id="bute" value="facture/facture-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.facture.Facture">
        <input name="nomtable" type="hidden" id="classe" value="facture">
    </form>
</div>
<%
    }catch(Exception e){
        e.printStackTrace();
    }
%>