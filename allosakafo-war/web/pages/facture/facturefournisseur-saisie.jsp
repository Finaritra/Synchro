<%-- 
    Document   : facturefournisseur-saisie
    Created on : 8 ao�t 2016, 11:48:10
    Author     : Murielle
--%>

<%@page import="mg.cmcm.fin.FactureFournisseur"%>
<%@page import="mg.cmcm.budget.BudgetDepense"%>
<%@page import="user.*"%>
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    FactureFournisseur  a = new FactureFournisseur();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));

    pi.setLien((String) session.getValue("lien"));    
    affichage.Champ[] liste = new affichage.Champ[2];
    
    TypeObjet op = new TypeObjet();
    op.setNomTable("TYPEFACTURE");
    liste[0] = new Liste("typefacture", op, "VAL", "id");
    
    BudgetDepense service = new BudgetDepense();
    liste[1] = new Liste("idbudgetdepense", service, "projet", "id");
    
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("remarque").setType("textarea");
    pi.getFormu().getChamp("typefacture").setLibelle("Type facture");
    pi.getFormu().getChamp("idbudgetdepense").setLibelle("Budget D�pense");
    pi.getFormu().getChamp("beneficiaire").setPageAppel("choix/listeFournisseurChoix.jsp");
    pi.getFormu().getChamp("tva").setLibelle("TVA");
    pi.getFormu().getChamp("idobjet").setVisible(false);
    pi.getFormu().getChamp("etat").setVisible(false);
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Saisie Facture Fournisseur</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="facturefournisseur" id="facturefournisseur">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="fin/facture/facturefournisseurdetails-saisie.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.cmcm.fin.FactureFournisseur">
    <input name="nomtable" type="hidden" id="nomtable" value="FACTUREFOURNISSEUR">
    </form>
    
</div>
