<%-- 
    Document   : facturefournisseurdetails-saisie
    Created on : 8 ao�t 2016, 11:57:18
    Author     : Murielle
--%>

<%@page import="mg.cmcm.fin.*"%>
<%@page import="user.*"%>
<%@page import="bean.CGenUtil"%>
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*" %>
<%@ page import="utilitaire.Utilitaire" %>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    FactureFournisseurDetails  a = new FactureFournisseurDetails();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));   
    
    String idBC = request.getParameter("id");
    pi.getFormu().getChamp("idmere").setDefaut(idBC);
    String lienfinaliser = "but=apresTarif.jsp&acte=finaliser&bute=fin/facture/facturefournisseur-liste.jsp&classe=mg.cmcm.fin.FactureFournisseur&id="+ idBC;
    pi.getFormu().getChamp("idmere").setVisible(false);
    pi.getFormu().getChamp("pu").setDefaut("0");
    pi.getFormu().getChamp("pu").setLibelle("Prix unitaire");
    pi.getFormu().getChamp("tva").setLibelle("TVA");
    pi.getFormu().getChamp("quantite").setLibelle("Quantit�");
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Ajouter d�tail</h1>
    <form action="<%=pi.getLien()%>?but=fin/mvtFacture/apresMvtFacture.jsp" method="post" name="appro" id="appro" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlAddTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="fin/facture/facturefournisseurdetails-saisie.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cmcm.fin.FactureFournisseurDetails">
        <input name="rajoutLien" type="hidden" id="rajoutLien" value="id-">
        <input name="rajoutLien" type="hidden" id="rajoutLien" value="<%=idBC%>">
        <input name="idmere" type ="hidden" id="idmere" value="<%=idBC%>">
        <input name="id" type ="hidden" id="idmere" value="<%=idBC%>">
    </form>
    <%
        FactureFournisseurDetails p = new FactureFournisseurDetails();
        //p.setNomTable("BONDECOMMANDEFILLE_VUE"); // vue
        FactureFournisseurDetails[] liste = (FactureFournisseurDetails[]) CGenUtil.rechercher(p, null, null, " and idmere = '" + request.getParameter("id") + "'");
        FactureFournisseur[] bc = (FactureFournisseur[])CGenUtil.rechercher(new FactureFournisseur(), null, null, " AND ID = '" + request.getParameter("id") + "'");
    %>
    <div id="selectnonee">
        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
            <thead>
                <tr class="head">
                    <th style="background-color:#bed1dd">Designation</th>
                    <th style="background-color:#bed1dd">Quantit&eacute;</th>
                    <th style="background-color:#bed1dd">PU</th>
                    <th style="background-color:#bed1dd">Montant</th>
                    <th style="background-color:#bed1dd">Action</th>
                </tr>
            </thead>
            <tbody>
                <%
                    double somme = 0;
                    double tva = 0;
                    for (int i = 0; i < liste.length; i++) {
                %>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"><%=liste[i].getDesignation()%> </td>
                    <td width="20%" align="center"><%=liste[i].getQuantite()%></td>
                    <td width="20%" align="center"><%=Utilitaire.formaterAr(liste[i].getPu()) %></td>
                    <td width="20%" align="center"><%=Utilitaire.formaterAr((liste[i].getQuantite() * liste[i].getPu()))%></td>
                    <td width="20%" align="center"><a href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + liste[i].getTuppleID() + "&idmere=" + request.getParameter("id")%>&acte=delete&bute=fin/facture/facturefournisseurdetails-saisie.jsp&classe=mg.cmcm.fin.FactureFournisseurDetails&rajoutLien=id" style="margin-right: 10px">annuler</a></td>
                </tr>
                <%
                    somme += ((liste[i].getQuantite() * liste[i].getPu()));
                    }
                    
                    tva = (somme * bc[0].getTva()) / 100;
                %>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"><b>Montant TVA</b></td>
                    <td width="20%" align="center"><b><%=Utilitaire.formaterAr(tva)%></b></td>
                    <td width="20%" align="center"></td>
                </tr>
                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"></td>
                    <td width="20%" align="center"><b>TOTAL</b></td>
                    <td width="20%" align="center"><b><%=Utilitaire.formaterAr(somme + tva)%></b></td>
                    <td width="20%" align="center"></td>
                </tr>
            </tbody>
        </table>
        <center><a href="<%=pi.getLien()%>?<%=lienfinaliser%>" class="btn btn-success pull-center" tabindex="81">Finaliser</a></center>
    </div>
</div>

