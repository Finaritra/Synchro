<%-- 
    Document   : cvDiplomeChoixMultiple
    Created on : 7 déc. 2015, 16:25:23
    Author     : user
--%>
<%@page import="mg.cnaps.cv.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    CVcompetence competence = new CVcompetence();
    competence.setNomTable("CV_COMPETENCE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "domaine", "description"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "domaine", "description","remarque"};
    PageRechercheChoix pr = new PageRechercheChoix(competence, request, listeCrt, listeInt, 3, libEntete, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("cvCompetenceChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste competence</h1>
            </section>
            <section class="content">
                <form action="cvCompetenceChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="competence" id="competence">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/cv/competence-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] =  {"ID", "Domaine", "Description", "Remarque"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>