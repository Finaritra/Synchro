<%-- 
    Document   : courierChoix
    Created on : 27 nov. 2015, 09:55:35
    Author     : user
--%>

<%@page import="mg.cnaps.sig.SigFivondronana"%>
<%@page import="mg.cnaps.courier.CourCourierCodePostal"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mg.cnaps.courier.CourCourier"%>
<%@page import="mg.cnaps.log.LogPersonnel"%>
<%@page import="mg.cnaps.log.LogDeplacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    CourCourierCodePostal e = new CourCourierCodePostal();    
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "categorie", "designation", "personnel", "daty","code_postal","fivondronana","fokontany"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "categorie", "designation", "personnel", "daty", "heure","code_postal","fivondronana","fokontany"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 9);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choixMultiple/courierChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    affichage.Champ[] list = new affichage.Champ[1];
    SigFivondronana fiv = new SigFivondronana();
    fiv.setNomTable("SIG_FIV");
    //SigFivondronana[] listFiv = (SigFivondronana[]) CGenUtil.rechercher(fiv, null, null, "");
    Liste liste = new Liste("fivondronana",fiv,"val","val");
    list[0] = liste;
    pr.getFormu().changerEnChamp(list);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("categorie").setLibelleAffiche("Categorie");
    pr.getFormu().getChamp("Designation").setLibelleAffiche("Designation");
    pr.getFormu().getChamp("personnel").setLibelleAffiche("Personnel");
    pr.getFormu().getChamp("daty1").setLibelleAffiche("Date min");
    pr.getFormu().getChamp("daty2").setLibelleAffiche("Date max");
    pr.getFormu().getChamp("code_postal").setLibelleAffiche("Code postal");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    CourCourierCodePostal[] donnees = (CourCourierCodePostal[]) pr.getTableau().getData();
    String[] sessionChoix = null;
    if(session.getAttribute("checked") != null){
        ArrayList<String> sChoix = (ArrayList<String>) session.getAttribute("checked");
        sessionChoix = new String[sChoix.size()];
        for(int i = 0; i < sessionChoix.length; i++){
            sessionChoix[i] = sChoix.get(i);
            System.out.println("choix : "+sChoix.get(i));
        }
    }
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste courier</h1>
            </section>
            <section class="content">
                <form action="choixMultiple/courierChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="courier" id="courier">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=matieriel_roulant/chauffeur/personnel-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Categorie", "Désignation", "Personnel", "Date", "Heure","Code Postal","Fivondronana","Fokontany"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiplePag.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th>Option <input onclick="CocheTout(this, 'choix')" onchange="cocherBox()" type="checkbox"></th>
                            <th>Id</th>
                            <th>Categorie</th>
                            <th>D&eacute;signation</th>
                            <th>Personnel</th>
                            <th>Date</th>
                            <th>Heure</th>
                            <th>Code postal</th>
                            <th>Fivondronana</th>
                            <th>Fokontany</th>

                        </tr>
                        <%
                                for (int i = 0; i < donnees.length; i++) {
                        %>
                        <tr>
                            <%
                                if(sessionChoix != null){
                                    int tmp = 0;
                                    for(int j = 0; j < sessionChoix.length; j++){
                                        if(donnees[i].getId().compareToIgnoreCase(sessionChoix[j]) == 0){
                            %>
                            <td><input class="cocher" onchange="cocherBox()" type="checkbox" name="choix" value="<%=donnees[i].getId()%>" checked></td>
                            <%
                                            tmp = 1;
                                            break;
                                        }
                                    }
                                    if(tmp == 0){
                            %>
                            <td><input class="cocher" onchange="cocherBox()" type="checkbox" name="choix" value="<%=donnees[i].getId()%>"></td>
                            <%
                                    }
                                }else{
                            %>
                            <td><input class="cocher" onchange="cocherBox()" type="checkbox" name="choix" value="<%=donnees[i].getId()%>"></td>
                            <%
                                }
                            %>
                            <td align=center><%=donnees[i].getId()%></td>
                            <td align=center><%=donnees[i].getCategorie()%></td>
                            <td align=center><%=donnees[i].getDesignation()%></td>
                            <td align=center><%= utilitaire.Utilitaire.champNull(donnees[i].getPersonnel())%></td>
                            <td align=center><%=donnees[i].getDaty()%></td>
                            <td align=center><%=donnees[i].getHeure()%></td>
                            <td align=center><%=donnees[i].getCode_postal()%></td>
                            <td align=center><%=donnees[i].getFivondronana()%></td>
                            <td align=center><%=donnees[i].getFokontany()%></td>
                        </tr>
                        <%}%>
                    </table>
                    <input type="hidden" name="choixTotal" value="">
                    <input class="btn btn-success" onclick="" type="submit" value="Valider">
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
<script type="text/javascript">
    function CocheTout(ref, name) {
        var form = ref;

        while (form.parentNode && form.nodeName.toLowerCase() != 'form') {
            form = form.parentNode;
        }

        var elements = form.getElementsByTagName('input');

        for (var i = 0; i < elements.length; i++) {
            if (elements[i].type == 'checkbox' && elements[i].name == name) {
                elements[i].checked = ref.checked;
            }
        }
    }
    function cocherBox() {
        
        var nodesArray = [];
        $.each($("input[name='choix']:checked"), function(){            
            nodesArray.push($(this).val());
        });
        //alert("nodeArray : " + nodesArray.join(", "));
        var checkedBoxesToString = nodesArray.toString();
        
        $.ajax({
            type: 'GET',
            url: '${pageContext.request.contextPath}/CocherMultipleServlet',
            contentType: 'application/json',
            data: {'choix': checkedBoxesToString},
            success: function (ma) {
                if (ma != null) {
                    var data = JSON.parse(ma);
                    if (data.ok != null) {
                        //alert(data.ok);
                    }
                    if (data.erreur != null) {
                        alert(data.erreur);
                    }
                }

            },
            error: function (e) {
                //alert("Erreur Ajax");
            }

        });
    }
</script>