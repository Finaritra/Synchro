<%-- 
    Document   : appelDnChoixMulti
    Created on : 27 nov. 2015, 10:24:22
    Author     : Tafitasoa
--%>

<%@page import="mg.cnaps.recette.AppelDn"%>
<%@page import="mg.cnaps.log.LogPersonnel"%>
<%@page import="mg.cnaps.log.LogDeplacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    AppelDn lv = new AppelDn();
    String champReturn = request.getParameter("champReturn");
    lv.setNomTable("APPELDN_DISPO_LIBELLE");
    if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("") != 0) {
        lv.setNomTable(request.getParameter("etat"));
    }
    String listeCrt[] = {"id", "idtypeenvoi", "idemployeur", "periode", "resultat","adresse"};
    String listeInt[] = null;
    String libEntete[] = {"id", "idtypeenvoi", "idemployeur" ,"adresse", "periode", "resultat"};

    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("idtypeenvoi").setLibelle("Type envoie");
    pr.getFormu().getChamp("idemployeur").setLibelle("Employeur");
    pr.getFormu().getChamp("periode").setLibelle("Periode");
    pr.setApres("appelDnChoixMulti.jsp");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Appel DN</h1>
            </section>
            <section class="content">
                <form action="appelDnChoixMulti.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="appelDn" id="appelDn">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=recette/appeldn/appeldn-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Type d'envoie", "Employeur ","Adresse", "Periode", "Resultat"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>