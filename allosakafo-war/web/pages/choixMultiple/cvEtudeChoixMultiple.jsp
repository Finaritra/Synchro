<%-- 
    Document   : cvEtudeChoixMultiple
    Created on : 7 d�c. 2015, 16:22:52
    Author     : user
--%>

<%@page import="mg.cnaps.cv.CvEtude"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    CvEtude e = new CvEtude();
    e.setNomTable("CV_ETUDE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "date_debut", "date_fin"};
    String listeInt[] = {"date_debut", "date_fin"};
    String libEntete[] = {"id", "date_debut", "date_fin", "etablissement","remarque"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("cvEtudeChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("date_debut1").setLibelleAffiche("Date debut min");
    pr.getFormu().getChamp("date_fin1").setLibelleAffiche("Date fin min");
    pr.getFormu().getChamp("date_debut2").setLibelleAffiche("Date debut max");
    pr.getFormu().getChamp("date_fin2").setLibelleAffiche("Date fin max");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste experiences</h1>
            </section>
            <section class="content">
                <form action="cvEtudeChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="etude" id="etude">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/cv/etude-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] =  {"ID", "Date debut", "Date fin", "Etablissement","Remarque"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>