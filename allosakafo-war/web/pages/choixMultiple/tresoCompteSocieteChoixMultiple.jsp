<%-- 
    Document   : tresoCompteSocieteChoix
    Created on : 29 sept. 2015, 14:00:16
    Author     : user
--%>
<%@page import="mg.cnaps.treso.TresoCompteSociete"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>


<%
    String champReturn = request.getParameter("champReturn");
    TresoCompteSociete e = new TresoCompteSociete();
    e.setNomTable("TRESO_COMPTE_SOCIETE_VIEW");
    String listeCrt[]={"id", "compte_cle","compte_numero","agence_code"};
    String listeInt[]=null;
    String libEntete[]={"id", "compte_cle","compte_numero","agence_code","compte_bancaire"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("tresoCompteSocieteChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("compte_cle").setLibelleAffiche("Cle du compte");
    pr.getFormu().getChamp("compte_numero").setLibelleAffiche("Compte comptable");
    pr.getFormu().getChamp("agence_code").setLibelleAffiche("Agence");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    TresoCompteSociete[] listeP = (TresoCompteSociete[])pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste compte CNAPS</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="nature" id="nature">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th><b>#</b></th>
                            <th><b>Id</b></th>
                            <th><b>Cle du compte</b></th>
                            <th><b>Compte comptable</b></th>
                            <th><b>Agence</b></th>
                            <th><b>Numero de compte bancaire</b></th>
                        </tr>
                        <%
                            for (int i = 0; i < listeP.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getCompte_numero() %> <%=listeP[i].getAgence_code() %>" class="radio" /></td>
                            <td align=left><%=listeP[i].getId()%></td>
                            <td align=left><%=Utilitaire.champNull(listeP[i].getCompte_cle()) %></td>
                            <td align=left><%=Utilitaire.champNull(listeP[i].getCompte_numero()) %></td>
                            <td align=left><%=Utilitaire.champNull(listeP[i].getAgence_code()) %></td>
                            <td align=left><%=Utilitaire.champNull(listeP[i].getCompte_bancaire()) %></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
