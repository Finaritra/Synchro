<%-- 
    Document   : comptaCompteChoix
    Created on : 15 sept. 2015, 21:28:07
    Author     : user
--%>

<%@page import="mg.cnaps.archive.SigPieceRecue"%>
<%@page import="mg.cnaps.compta.ComptaCompte"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    TypeObjet e = new TypeObjet();
    e.setNomTable("vuepiecerectest");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id"};
    String listeInt[] = {};
    String libEntete[] = {"id"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 1, libEntete, 1);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("pieceRecueChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("Libelle");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste piece re�ues</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="cc" id="cc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% 
		    String libEnteteAffiche[] = {"Libelle"};
		    pr.getTableau().setLibelleAffiche(libEnteteAffiche);		    
		    out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>