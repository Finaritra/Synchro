<%-- 
    Document   : logPersonnelChoixMultiple
    Created on : 27 oct. 2015, 19:17:22
    Author     : user
--%>

<%@page import="mg.cnaps.recette.Talon"%>
<%@page import="mg.cnaps.log.LogDeplacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    Talon t = new Talon();
    t.setNomTable("talon_cie_view");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"periode","sig_dossiers_employeurs","nonplafonne","plafonne","cotiseemployeur","cotisetravailleur","majorationretard"};
    String listeInt[] = {"nonplafonne","plafonne","cotiseemployeur","cotisetravailleur","majorationretard","netapayer"};
    String libEntete[] = {"id","periode","nonplafonne","plafonne","cotiseemployeur","cotisetravailleur","majorationretard","netapayer"};
    PageRechercheChoix pr = new PageRechercheChoix(t, request, listeCrt, listeInt, 3, libEntete,7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("talonChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("periode").setLibelleAffiche("Periode");
    pr.getFormu().getChamp("sig_dossiers_employeurs").setLibelleAffiche("Dossiers des employeurs");
    String[] colSomme = {"nonplafonne","plafonne","cotiseemployeur","cotisetravailleur","majorationretard","netapayer"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Talon</h1>
            </section>
            <section class="content">
                <form action="talonChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="talon" id="talon">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=recette/declarationnominative/talon-fiche.jsp"};
                    String libelles[] = {"id","periode","non plafonne","plafonne","cotise employeur","cotise travailleur","majoration retard","net � payer"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>