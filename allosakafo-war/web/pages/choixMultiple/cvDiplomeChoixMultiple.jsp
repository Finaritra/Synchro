<%-- 
    Document   : cvDiplomeChoixMultiple
    Created on : 7 d�c. 2015, 16:25:23
    Author     : user
--%>
<%@page import="mg.cnaps.cv.CvDiplome"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    CvDiplome e = new CvDiplome();
    e.setNomTable("CV_DIPLOME");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "id_pays", "id_secteur"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "id_pays", "id_secteur", "annee_obtention","description", "niveau_bacc"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("cvEtudeChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("id_pays").setLibelleAffiche("Pays");
    pr.getFormu().getChamp("id_secteur").setLibelleAffiche("Secteur");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste experiences</h1>
            </section>
            <section class="content">
                <form action="cvEtudeChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="etude" id="etude">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/cv/etude-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] =  {"ID", "Pays", "Secteur", "Ann�e d'obtention","Description", "Niveau"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>