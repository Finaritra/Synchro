<%@page import="mg.cnaps.notification.NotificationMessage"%>
<%@page import="user.UserEJB"%>
<%@page import="user.*" %>
<%@page import="utilitaire.*" %>
<%@page import="bean.*" %>
<%@page import="java.sql.SQLException" %>
<%@page import="affichage.*" %>
<%
    String[] val = request.getParameterValues("id");  
    UserEJB u = (user.UserEJB) session.getValue("u");
    String lien = (String) session.getValue("lien");
    String bute = request.getParameter("bute");
    String acte = request.getParameter("acte");
    String classe = request.getParameter("classe");
    ClassEtat t = null;

    try {
        if (acte.compareToIgnoreCase("notifier") == 0) {	
            u.notifierObjectMultiple(val, classe);
        }
        if (acte.compareToIgnoreCase("notifierFiche") == 0) {	
            u.notifierObjectMultiple(val, classe);
            %>
            <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>&id=<%=request.getParameter("id")%>");</script>
            <%
        }
    } catch (Exception e) {
%>
<script>
    alert(<%=e.getMessage()%>);
</script>
<%}%>
<script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>");</script>