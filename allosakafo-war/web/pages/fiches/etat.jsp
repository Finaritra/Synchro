<script type = "text/javascript">
    function WebSocketTest() {

        if ("WebSocket" in window) {
            alert("WebSocket is supported by your Browser!");

            // Let us open a web socket
            var ws = new WebSocket("ws://localhost:8080/allosakafo-war/endpoint");

            ws.onopen = function () {
                // Web Socket is connected, send data using send()
                ws.send("Message to send");
                alert("Message is sent...");
            };

            ws.onmessage = function (evt) {
                var received_msg = evt.data;
                alert("Message is received..." , received_msg);
            };

            ws.onclose = function () {

                // websocket is closed.
                alert("Connection is closed...");
            };
        } else {

            // The browser doesn't support WebSocket
            alert("WebSocket NOT supported by your Browser!");
        }
    }
</script>



<div class="content-wrapper">

    <section class="content-header">
        <h1>Etat des commandes</h1>
    </section>

    <section class="content" >

        <div clas="row">

            <a href = "javascript:WebSocketTest()">Run WebSocket</a>
        </div>

        <div clas="row">
            <h2 align="left">Table 1</h2>
            <table class="table" style="background-color:white;">
                <thead></thead>
                <tbody>
                    <tr>
                        <td width="70%" >Poulet</td>
                        <td width="30%" align="left"> <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;">Livre</button> </td>
                    </tr>
                    <tr>
                        <td width="70%" >Jus</td>
                        <td width="30%" align="left"> <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;">Livre</button> </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div clas="row">
            <h2 align="left">Table 2</h2>
            <table class="table" style="background-color:white;" >
                <thead></thead>
                <tbody>
                    <tr>
                        <td width="70%" >Gateau</td>
                        <td width="30%" align="left"> <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 1%;">Preparer</button> </td>
                    </tr>
                    <tr>
                        <td width="70%" >Glace</td>
                        <td width="30%" align="left"> <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 1%">Preparer</button> </td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div clas="row">
            <h2 align="left">Table 3</h2>
            <table class="table" style="background-color:white;">
                <thead></thead>
                <tbody>
                    <tr>
                        <td width="70%" >Cake</td>
                        <td width="30%" align="left"> <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 1%;">Preparer</button> </td>
                    </tr>
                    <tr>
                        <td width="70%" >Glace</td>
                        <td width="30%" align="left"> <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 1%">Preparer</button> </td>
                    </tr>
                </tbody>
            </table>
        </div>


    </section>


</div>
