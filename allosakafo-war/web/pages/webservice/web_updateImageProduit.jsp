<%-- 
    Document   : web_updateImageProduit
    Created on : 5 mai 2017, 12:38:35
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    
    String idProduit = request.getParameter("idProduit");
    String image = request.getParameter("img");
    String resultatInsertion = new String();
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        resultatInsertion = ExecuteFunction.modifier_imageProduit(idProduit,image);
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>