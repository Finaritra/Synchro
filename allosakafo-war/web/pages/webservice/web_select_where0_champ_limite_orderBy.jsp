<%-- 
    Document   : web_select_where0_champ_limite_orderBY
    Created on : 25 avr. 2017, 11:42:03
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String table = request.getParameter("t");
    String champ_ordre = request.getParameter("co");
    String value_orderBy = request.getParameter("ob");
    int limite = Integer.parseInt(request.getParameter("l"));
    int debut = Integer.parseInt(request.getParameter("d"));
    JSONArray resultat_final = ExecuteFunction.select_listWhere0OrderByLimit(table,champ_ordre,value_orderBy,limite,debut);
%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultat_final);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>