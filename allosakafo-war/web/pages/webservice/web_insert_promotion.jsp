<%-- 
    Document   : web_inserer_promotion
    Created on : 4 mai 2017, 10:52:29
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String nomPromotion = request.getParameter("nomPromotion");
    String dateDebut = request.getParameter("dateDebut");
    String dateFin = request.getParameter("dateFin");
    String descriptionPromotion = request.getParameter("descriptionPromotion");
    String idSession = request.getParameter("idSession");
    String lienImage = request.getParameter("lienImage");
    String commentaire = request.getParameter("commentaire");
    System.out.println("Description :"+descriptionPromotion);
    String resultatInsertion = ExecuteFunction.inserer_promotion(id, nomPromotion, dateDebut, dateFin, descriptionPromotion, idSession, lienImage, commentaire);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>