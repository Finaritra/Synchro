<%-- 
    Document   : web_selectPromoDate_idAbt
    Created on : 4 mai 2017, 14:17:35
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String idAbonnement = request.getParameter("idAbonnement");
    int limite = Integer.parseInt(request.getParameter("limite"));
    int debut = Integer.parseInt(request.getParameter("debut"));
    String colonne = request.getParameter("colonne");
    String ordre = request.getParameter("ordre");
    String date = request.getParameter("date");
    JSONArray resultat_final = ExecuteFunction.selectPromo_date_idAbt(idAbonnement,debut,limite,date,colonne,ordre);
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultat_final);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>