<%-- 
    Document   : web_selectWhere_orderBy
    Created on : 4 mai 2017, 12:06:09
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String table = request.getParameter("table");
    String colonne = request.getParameter("colonne");
    String valeur = request.getParameter("valeur");
    String orderBy = request.getParameter("orderBy");
    String ordre = request.getParameter("ordre");
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    JSONArray resultat_final = ExecuteFunction.select_whereOrderBy(table,colonne,valeur,orderBy,ordre);
	try{
		out.println(resultat_final);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>