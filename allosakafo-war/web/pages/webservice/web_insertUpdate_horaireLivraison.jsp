<%-- 
    Document   : web_insertUpdate_horaireLivraison
    Created on : 15 mai 2017, 10:44:21
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String id = request.getParameter("id");
    String debutCommande = request.getParameter("debutCommande");
    String finCommande = request.getParameter("finCommande");
    String debutLivraison = request.getParameter("debutLivraison");
    String finLivraison = request.getParameter("finLivraison");
    String commentaire = request.getParameter("commentaire");
    String action = request.getParameter("action");
    String resultatInsertion = new String();
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        if(action.compareTo("insert")==0){
            //Insertion
            resultatInsertion = ExecuteFunction.inserer_horaireLivraison(id, debutCommande, finCommande, debutLivraison, finLivraison, commentaire);
        }else if(action.compareTo("update")==0){
            //Mise à jour
            resultatInsertion = ExecuteFunction.modifier_horaireLivraison(id, debutCommande, finCommande, debutLivraison, finLivraison, commentaire);
        }
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>