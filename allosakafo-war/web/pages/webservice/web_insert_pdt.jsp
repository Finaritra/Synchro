<%-- 
    Document   : web_insert_pdt
    Created on : 27 avr. 2017, 13:45:18
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String nom = request.getParameter("nom");
    String designation = request.getParameter("desc");
    String photo = request.getParameter("ph");
    String typeProduit = request.getParameter("tpe");
    String calorie = request.getParameter("cat");
    String poids = request.getParameter("pd");
    String pa = request.getParameter("pa");
    String afficher = request.getParameter("a");
    String commentaire = request.getParameter("c");
    String resultatInsertion = ExecuteFunction.inserer_produit(id, nom, designation, photo, typeProduit, calorie, poids, pa, afficher, commentaire);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>