<%-- 
    Document   : web_insert_fraisCommande
    Created on : 18 juil. 2017, 14:51:06
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String id = request.getParameter("id");
    String idCommande = request.getParameter("idCommande");
    String frais = request.getParameter("frais");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = null;
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        resultatInsertion = ExecuteFunction.inserer_fraisCommande(id, idCommande, frais, commentaire);
	out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>