<%-- 
    Document   : web_updateQuantitePanier
    Created on : 5 mai 2017, 09:42:41
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String table = request.getParameter("table");
    String nomChampQuantite = request.getParameter("nomChampQuantite");
    int quantite = Integer.parseInt(request.getParameter("quantite"));
    String nomChampCondition1 = request.getParameter("nomChampCondition1");
    String valeurChampCondition1 = request.getParameter("valeurChampCondition1");
    String nomChampCondition2 = request.getParameter("nomChampCondition2");
    String valeurChampCondition2 = request.getParameter("valeurChampCondition2");
    String resultatInsertion = null;
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        resultatInsertion = ExecuteFunction.update_quantiteCommande_2conditions(table,nomChampQuantite,quantite,nomChampCondition1,valeurChampCondition1,nomChampCondition2,valeurChampCondition2);
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
