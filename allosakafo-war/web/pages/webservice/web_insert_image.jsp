<%-- 
    Document   : web_insert_image
    Created on : 5 mai 2017, 10:28:58
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String id = request.getParameter("id");
    String idCategorieImage = request.getParameter("idCategorieImage");
    String nomImage = request.getParameter("nomImage");
    String lienImage = request.getParameter("lienImage");
    String description = request.getParameter("description");
    String alt = request.getParameter("alt");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = null;
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        resultatInsertion = ExecuteFunction.inserer_image(id, idCategorieImage, nomImage, lienImage, description, alt, commentaire);
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>