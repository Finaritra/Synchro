<%-- 
    Document   : web_insertUpdate_detailProduit
    Created on : 8 mai 2017, 13:16:05
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String id = request.getParameter("id");
    String idProduit = request.getParameter("idProduit");
    String commentaire = request.getParameter("commentaire");
    String energie = request.getParameter("energie");
    String a_energie = request.getParameter("a_energie");
    String proteine = request.getParameter("proteine");
    String a_proteine = request.getParameter("a_proteine");
    String carbohytrateGlycemique = request.getParameter("carbohytrateGlycemique");
    String a_carbohytrateGlycemique = request.getParameter("a_carbohytrateGlycemique");
    String totalSucre = request.getParameter("totalSucre");
    String a_totalSucre = request.getParameter("a_totalSucre");
    String totalSodium = request.getParameter("totalSodium");
    String a_totalSodium = request.getParameter("a_totalSodium");
    String cholesterole = request.getParameter("cholesterole");
    String a_cholesterole = request.getParameter("a_cholesterole");
    String totalMatiereGrasse = request.getParameter("totalMatiereGrasse");
    String a_totalMatiereGrasse = request.getParameter("a_totalMatiereGrasse");
    String totalMatiereGrasseMonoSaturee = request.getParameter("totalMatiereGrasseMonoSaturee");
    String a_totalMatiereGrasseMonoSaturee = request.getParameter("a_totalMatiereGrasseMonoSaturee");
    String totalMatiereGrassePolyinsaturee = request.getParameter("totalMatiereGrassePolyinsaturee");
    String a_totalMatiereGrassePolyinsaturee = request.getParameter("a_totalMatiereGrassePolyinsaturee");
    String totalKJ = request.getParameter("totalKJ");
    String a_totalKJ = request.getParameter("a_totalKJ");
    String fibreDietetique = request.getParameter("fibreDietetique");
    String a_fibreDietetique = request.getParameter("a_fibreDietetique");
    String totalCal = request.getParameter("totalCal");
    String a_totalCal = request.getParameter("a_totalCal");
    
    /*String v_energie = request.getParameter("energie");
    float energie = 0;
    if(v_energie!=null) System.out.println("different de null");
    if(v_energie.compareTo("")!=0) System.out.println("Est different de vide ");
    if(!v_energie.isEmpty()) System.out.println("N'est pas vide");
    
    if(v_energie!=null) energie = Float.parseFloat(v_energie);
    System.out.println(energie);
    
    String v_proteine = request.getParameter("proteine");
    float proteine = 0;
    if(v_proteine!=null && v_proteine.compareTo("")!=0 && !v_proteine.isEmpty()) proteine = Float.parseFloat(v_proteine);
    
    String v_carbohytrateGlycemique = request.getParameter("carbohytrateGlycemique");
    float carbohytrateGlycemique = 0;
    if(v_carbohytrateGlycemique!=null && v_carbohytrateGlycemique.compareTo("")!=0 && !v_carbohytrateGlycemique.isEmpty()) carbohytrateGlycemique = Float.parseFloat(v_carbohytrateGlycemique);
    
    String v_totalSucre = request.getParameter("totalSucre");
    float totalSucre = 0;
    if(v_totalSucre!=null && v_totalSucre.compareTo("")!=0 && !v_totalSucre.isEmpty()) totalSucre = Float.parseFloat(v_totalSucre);
    
    String v_totalSodium = request.getParameter("totalSodium");
    float totalSodium = 0;
    if(v_totalSodium!=null && v_totalSodium.compareTo("")!=0 && !v_totalSodium.isEmpty()) totalSodium = Float.parseFloat(v_totalSodium);
    
    String v_cholesterole = request.getParameter("cholesterole");
    float cholesterole = 0;
    if(v_cholesterole!=null && v_cholesterole.compareTo("")!=0 && !v_cholesterole.isEmpty()) cholesterole = Float.parseFloat(v_cholesterole);
    
    String v_totalMatiereGrasse = request.getParameter("totalMatiereGrasse");
    float totalMatiereGrasse = 0;
    if( v_totalMatiereGrasse!=null && v_totalMatiereGrasse.compareTo("")!=0 && !v_totalMatiereGrasse.isEmpty()) totalMatiereGrasse = Float.parseFloat(v_totalMatiereGrasse);
    
    String v_totalMatiereGrasseMonoSaturee = request.getParameter("totalMatiereGrasseMonoSaturee");
    float totalMatiereGrasseMonoSaturee = 0;
    if(v_totalMatiereGrasseMonoSaturee!=null && v_totalMatiereGrasseMonoSaturee.compareTo("")!=0 && !v_totalMatiereGrasseMonoSaturee.isEmpty()) totalMatiereGrasseMonoSaturee = Float.parseFloat(v_totalMatiereGrasseMonoSaturee);
    
    String v_totalMatiereGrassePolyinsaturee = request.getParameter("totalMatiereGrassePolyinsaturee");
    float totalMatiereGrassePolyinsaturee = 0;
    if(v_totalMatiereGrassePolyinsaturee!=null && v_totalMatiereGrassePolyinsaturee.compareTo("")!=0 && !v_totalMatiereGrassePolyinsaturee.isEmpty() ) totalMatiereGrassePolyinsaturee = Float.parseFloat(v_totalMatiereGrassePolyinsaturee);
    
    String v_fibreDietetique = request.getParameter("fibreDietetique");
    float fibreDietetique = 0;
    if( v_fibreDietetique!=null && v_fibreDietetique.compareTo("")!=0 && !v_fibreDietetique.isEmpty())fibreDietetique = Float.parseFloat(v_fibreDietetique);
    
    String v_totalKJ = request.getParameter("totalKJ");
    float totalKJ = 0;
    if(v_totalKJ!=null && v_totalKJ.compareTo("")!=0 && !v_totalKJ.isEmpty())totalKJ = Float.parseFloat(v_totalKJ);
    
    String v_totalCal = request.getParameter("totalCal");
    float totalCal = 0;
    if(v_totalCal!=null && v_totalCal.compareTo("")!=0 && !v_totalCal.isEmpty())totalCal = Float.parseFloat(v_totalCal);
    */
    
    String action = request.getParameter("action");
    String resultatInsertion = new String();
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        if(action.compareTo("insert")==0){
            //Insertion
            resultatInsertion = ExecuteFunction.inserer_detailProduit(id, idProduit, commentaire, energie, a_energie, proteine, a_proteine, carbohytrateGlycemique, a_carbohytrateGlycemique, totalSucre, a_totalSucre, totalSodium, a_totalSodium, cholesterole, a_cholesterole, totalMatiereGrasse, a_totalMatiereGrasse, totalMatiereGrasseMonoSaturee, a_totalMatiereGrasseMonoSaturee, totalMatiereGrassePolyinsaturee, a_totalMatiereGrassePolyinsaturee, fibreDietetique, a_fibreDietetique, totalKJ, a_totalKJ, totalCal, a_totalCal);
        }else if(action.compareTo("update")==0){
            //Mise à jour
            //resultatInsertion = ExecuteFunction.modifier_detailProduit(id, idProduit, commentaire, energie, proteine, carbohytrateGlycemique, totalSucre, totalSodium, cholesterole, totalMatiereGrasse, totalMatiereGrasseMonoSaturee, totalMatiereGrassePolyinsaturee, fibreDietetique, totalKJ, totalCal);
            resultatInsertion = ExecuteFunction.modifier_detailProduit(id, idProduit, commentaire, energie, a_energie, proteine, a_proteine, carbohytrateGlycemique, a_carbohytrateGlycemique, totalSucre, a_totalSucre, totalSodium, a_totalSodium, cholesterole, a_cholesterole, totalMatiereGrasse, a_totalMatiereGrasse, totalMatiereGrasseMonoSaturee, a_totalMatiereGrasseMonoSaturee, totalMatiereGrassePolyinsaturee, a_totalMatiereGrassePolyinsaturee, fibreDietetique, a_fibreDietetique, totalKJ, a_totalKJ, totalCal, a_totalCal);
        }
	out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
