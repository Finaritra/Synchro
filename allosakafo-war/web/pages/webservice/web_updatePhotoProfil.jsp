<%-- 
    Document   : web_updatePhotoProfil
    Created on : 5 mai 2017, 15:19:29
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    
    String idUtilisateur = request.getParameter("idUtilisateur");
    String image = request.getParameter("img");
    String resultatInsertion = new String();
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
    try{
        resultatInsertion = ExecuteFunction.modifier_photoProfilUtilisateur(idUtilisateur,image);
		out.println(resultatInsertion);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>