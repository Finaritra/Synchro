<%-- 
    Document   : web_insert_ingredient
    Created on : 27 avr. 2017, 13:45:18
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String libelle = request.getParameter("libelle");
    String seuil = request.getParameter("seuil");
    String unite = request.getParameter("unite");
    String quantiteParPack = request.getParameter("quantiteParPack");
    String pu = request.getParameter("pu");
    String actif = request.getParameter("actif");
    String resultatInsertion = ExecuteFunction.inserer_ingredient(id,libelle,seuil,unite,quantiteParPack,pu,actif);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>