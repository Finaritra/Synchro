<%-- 
    Document   : web_insert_detailCmd
    Created on : 28 avr. 2017, 14:22:43
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String idMere = request.getParameter("idMere");
    String produit = request.getParameter("pdt");
    String utilisateur = request.getParameter("util");
    String quantite = request.getParameter("qty");
    String pu = request.getParameter("pu");
    String remise = request.getParameter("remise");
    String observation = request.getParameter("obs");
    String resultatInsertion = ExecuteFunction.inserer_detailCommande(id, idMere, produit, quantite, pu, remise, observation);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>