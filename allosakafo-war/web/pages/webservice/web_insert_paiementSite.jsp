<%-- 
    Document   : web_insert_panierArticle
    Created on : 24 avr. 2017, 12:57:01
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String idTypePaiement = request.getParameter("idTypePaiement");
    String idUtilisateur = request.getParameter("idUtilisateur");
    String idCommandeMere = request.getParameter("idCommandeMere");
    String datePaiement = request.getParameter("datePaiement");
    String montant = request.getParameter("montant");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = ExecuteFunction.inserer_paiementSite(id, idTypePaiement, idUtilisateur, idCommandeMere, datePaiement, montant, commentaire);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>