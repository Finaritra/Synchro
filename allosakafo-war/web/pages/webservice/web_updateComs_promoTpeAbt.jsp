<%-- 
    Document   : web_updateImageProduit
    Created on : 5 mai 2017, 12:38:35
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String idTypeAbonnement = request.getParameter("idTypeAbonnement");
    String idPromotion = request.getParameter("idPromotion");
    String commentaire = request.getParameter("commentaire");
    String resultatModification = new String();
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setContentType("application/json");
    try{
        resultatModification = ExecuteFunction.updateComs_promoTpeAbt(idTypeAbonnement,idPromotion, commentaire);
	out.println(resultatModification);
    }catch(Exception e){
        resultatModification = e.getMessage();
    }
%>