<%-- 
    Document   : web_insert_produitFavori
    Created on : 3 mai 2017, 14:42:17
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String idProduit = request.getParameter("idProduit");
    String idUtilisateur = request.getParameter("idUtilisateur");
    String dateAjout = request.getParameter("dateAjout");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = ExecuteFunction.inserer_favori_produit(id, idProduit, idUtilisateur, dateAjout, commentaire);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>