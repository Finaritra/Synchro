<%-- 
    Document   : web_insert_promotionCause
    Created on : 4 mai 2017, 11:21:48
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String idPromotion = request.getParameter("idPromotion");
    String idProduit  = request.getParameter("idProduit");
    String nbrProduitOffert = request.getParameter("nbrProduitOffert");
    String remise = request.getParameter("remise");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = ExecuteFunction.inserer_promotionConsequence(id, idPromotion, idProduit, nbrProduitOffert, remise, commentaire);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
		out.println(resultatInsertion);
	}catch(Exception e){
		out.println("Exception!!!");
	}
%>