<%@page import="mg.allosakafo.ded.OrdonnerPayement"%>
<%@page import="user.*"%> 
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*"%>
<%@page import="utilitaire.*"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    OrdonnerPayement  a = new OrdonnerPayement();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));    
    
	pi.getFormu().getChamp("ded_Id").setLibelle("N� facture");
	pi.getFormu().getChamp("ded_Id").setPageAppel("choix/listeFFViseChoix.jsp");
	pi.getFormu().getChamp("ded_Id").setAutre("readonly='true'");
	
    pi.getFormu().getChamp("daty").setLibelle("Date");
	pi.getFormu().getChamp("montant").setLibelle("Montant");
	pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("remarque").setType("textarea");
	
	pi.getFormu().getChamp("idLigne").setVisible(false);
	pi.getFormu().getChamp("etat").setVisible(false);

	if (request.getParameter("ded_Id")!= null && request.getParameter("ded_Id").compareTo("") != 0) pi.getFormu().getChamp("ded_Id").setDefaut(request.getParameter("ded_Id"));
	if (request.getParameter("montant")!= null && request.getParameter("montant").compareTo("") != 0) pi.getFormu().getChamp("montant").setDefaut(request.getParameter("montant"));
	if (request.getParameter("remarque")!= null && request.getParameter("remarque").compareTo("") != 0) pi.getFormu().getChamp("remarque").setDefaut(request.getParameter("remarque"));
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Enregistrer Ordonner payement</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=ded/apresOrdonner.jsp" method="post" name="ded_op" id="ded_op">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insertByFacture">
    <input name="bute" type="hidden" id="bute" value="ded/ordonnerpayement-fiche.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.ded.OrdonnerPayement">
    </form>
</div>