<%-- 
    Document   : report-liste
    Created on : 11 jan. 2017, 15:42:37
    Author     : Narindra
--%>

<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.fin.Report"%>
<%@page import="affichage.PageRecherche"%>

<% 
    Report report = new Report();
    String table = "reportlib";
    if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0) {
        table=request.getParameter("table");
    }
    report.setNomTable(table);
    String listeCrt[] = {"daty", "montant", "devise", "caisse"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "montant", "devise", "caisse"};

    
    PageRecherche pr = new PageRecherche(report, request, listeCrt, listeInt, 4, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
  /*  affichage.Champ[] liste = new affichage.Champ[1];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("devise");
    liste[0] = new Liste("devise", ou, "VAL", "VAL");

    pr.getFormu().changerEnChamp(liste);
    */
    
    pr.setApres("ded/report-liste.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste report</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=ded/report-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
                       <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                       <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("report") == 0) {%>    
                        <option value="report" selected>Tous</option>
                        <% } else { %>
                        <option value="report" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("report_cree") == 0) {%>    
                        <option value="report_cree" selected>Cr&eacute;e</option>
                        <% } else { %>
                        <option value="report_cree" >Cr&eacute;e</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("report_valider") == 0) {%>    
                        <option value="report_valider" selected>Valid&eacute;e</option>
                        <% } else { %>
                        <option value="report_valider" >Valid&eacute;e</option>
                        <% } %>
                                                    
                    </select>
                </div>
            </div>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=ded/report-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "Montant", "Devise", "Caisse"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>