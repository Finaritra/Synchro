<%-- 
    Document   : as-livraison-fiche
    Created on : 1 d�c. 2016, 14:21:55
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.fin.*"%>
<%@page import="mg.allosakafo.ded.*"%>
<%
    EtatOP a = new EtatOP();
    //OrdonnerPayementLibelle a = new OrdonnerPayementLibelle();
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));

    pc.getChampByName("id").setLibelle("ID");
    pc.getChampByName("ded_Id").setLibelle("N� Facture");
    pc.getChampByName("montant").setLibelle("Montant");
    pc.getChampByName("remarque").setLibelle("Remarque");

    pc.getChampByName("daty").setLibelle("Date");

    pc.getChampByName("idLigne").setVisible(false);

    pc.setTitre("Consultation Ordonner Payement");
    a = (EtatOP)pc.getBase(); 
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=ded/ordonnerpayement-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>

                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer" >
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=ded/annulation-op-saisie.jsp&idobjet=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
                            <% if (a.getEtat() == ConstanteEtat.getEtatCreer()) {%>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=ded/apresOrdonner.jsp&acte=validerOP&classe=mg.allosakafo.ded.OrdonnerPayement&bute=ded/ordonnerpayement-fiche.jsp&id=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Viser</a>
                            
                            <%
                            } else if (a.getEtat() == ConstanteEtat.getEtatValider()) {
                            %>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=fin/sortiecaisse-saisie.jsp&idordre=" + request.getParameter("id") + "&remarque=" + a.getRemarque()%>&montant=<%=a.getReste() %>" style="margin-right: 10px">Payer</a>
                            <% } %>
                        </div>                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>
