<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.ded.OrdonnerPayementLibelle"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.allosakafo.fin.*"%>

<% 
    EtatOP lv = new EtatOP();
    String table="etatordonnerpayement";
    if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0) {
        table=request.getParameter("table");
    }
    lv.setNomTable(table);
    String listeCrt[] = {"id", "ded_Id", "daty","remarque"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "ded_Id", "daty", "montant", "remarque","etat","montantPaye","reste"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
      
    pr.getFormu().getChamp("ded_Id").setLibelleAffiche("N� Facture");

    pr.setApres("ded/ordonnerpayement-liste.jsp");
    String[] colSomme = {"montant","montantPaye","reste"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste ordonn� payement</h1>
    </section>
    <section class="content">
        
        <form action="<%=pr.getLien()%>?but=ded/ordonnerpayement-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("etatordonnerpayement") == 0) {%>    
                        <option value="etatordonnerpayement" selected>Tous</option>
                        <% } else { %>
                        <option value="etatordonnerpayement" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("etatordonnerpayementcreer") == 0) {%>    
                        <option value="etatordonnerpayementcreer" selected>Cr&eacute;e</option>
                        <% } else { %>
                        <option value="etatordonnerpayementcreer" >Cr&eacute;e</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("etatordonnerpayementvalider") == 0) {%>    
                        <option value="etatordonnerpayementvalider" selected>Valider</option>
                        <% } else { %>
                        <option value="etatordonnerpayementvalider" >Valider</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("etatordonnerpayementpayer") == 0) {%>    
                        <option value="etatordonnerpayementpayer" selected>Paye</option>
                        <% } else { %>
                        <option value="etatordonnerpayementpayer" >Paye</option>
                        <% } %>
                        
                      
                    </select>
                </div>
            </div>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=ded/ordonnerpayement-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "N� Facture", "Date", "Montant", "Remarque","etat","montantPaye","reste"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>