<%-- 
    Document   : attachementOrChoix
    Created on : 15 juin 2016, 14:10:31
    Author     : Joe
--%>
<%@page import="mg.cnaps.or.OrComplet"%>
<%@page import="affichage.PageRecherche"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    OrComplet orCmp = new OrComplet();
    orCmp.setNomTable("or_complet_nonattache");
    String listeCrt[] = {"id", "dossier", "compte", "date_op", "exercice"};
    String listeInt[] = {"date_op"};
    String libEntete[] = {"id", "dossier", "compte", "date_op", "exercice", "montant"};
    String champReturn = request.getParameter("champReturn");

    PageRechercheChoix pr = new PageRechercheChoix(orCmp, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("attachementOrChoix.jsp");
    pr.setChampReturn(champReturn);

    pr.getFormu().getChamp("date_op1").setLibelle("Date inf.");
    pr.getFormu().getChamp("date_op2").setLibelle("Date sup.");

    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste ordre de recette</h1>
            </section>
            <section class="content">
                <form action="attachementOrChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"Id", "Dossier", "Compte", "Date", "Exercice", "Montant"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixOr.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>