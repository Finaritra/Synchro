<%-- 
    Document   : apresChoixTravailleur
    Created on : 18 nov. 2015, 14:00:23
    Author     : Safidimahefa Romario
--%>
<%@page import="utilitaire.Utilitaire"%>
<html>
    <%
        String champRet =(String) request.getParameter("champReturn");
        String choix = (String) request.getParameter("choix");
        String[] champs = Utilitaire.split(champRet, ";");
        String[] lstChoix = Utilitaire.split(choix, ";");
    %>
    <script language="JavaScript">
        window.opener.document.all.<%=champs[0]%>.value = "<%=lstChoix[0]%>";
        window.opener.document.all.<%=champs[1]%>.value = "<%=lstChoix[1]%>";
        window.opener.document.all.<%=champs[2]%>.value = "<%=lstChoix[2]%>";
        window.opener.document.all.<%=champs[3]%>.value = "<%=lstChoix[3]%>";
        window.close();
    </script>
</html>
