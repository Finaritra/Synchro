<%-- 
    Document   : menuPereChoix
    Created on : 5 janv. 2016, 10:46:14
    Author     : user
--%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="mg.cnaps.accueil.Sig_beneficiaire"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    Menudynamique e = new Menudynamique();
    String champReturn = request.getParameter("champReturn");
    e.setNomTable("MENUDYNAMIQUE");
    String listeCrt[] = {"id", "libelle","rang", "niveau", "id_pere"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "libelle","icone", "href", "rang","niveau", "id_pere"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("menuPereChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id_pere").setLibelleAffiche("Menu Pere");
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste menu</h1>
            </section>
            <section class="content">
                <form action="menuPereChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="benef" id="benef">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"Id", "Libelle","Icone", "href", "Rang","Niveau", "Pere"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>