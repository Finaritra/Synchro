<%--
    Document   : apresChoixCodeAgence
    Created on : 15 sept. 2015, 11:46:56
    Author     : user
--%>
<%@page import="utilitaire.Utilitaire"%>
<html>
    <%
	String champRet = (String) request.getParameter("champReturn");
	String choix = (String) request.getParameter("choix");
	String[] champs = Utilitaire.split(champRet, ";");
	String[] lstChoix = Utilitaire.split(choix, ";");
    %>
    <script language="JavaScript">	        
        window.opener.document.getElementById("agence_code").value = "<%= lstChoix [0]%>"
        window.opener.document.getElementById("banque_code").value = "<%= lstChoix [1]%>"
        window.close();
    </script>
</html>