<%-- 
    Document   : choixOm
    Created on : 7 juin 2016, 11:57:46
    Author     : Ignafah
--%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.controle.ContOrdreMission"%>
<%
    String champReturn = request.getParameter("champReturn");
    ContOrdreMission lv = new ContOrdreMission();
     lv.setNomTable("CONT_ORDRE_MISSION_LIBELLE");
    String listeCrt[] = {"id", "sigdr", "idsecteur", "idequipe", "datedebut"};
    String listeInt[] = {"datedebut"};
    String libEntete[] = {"id", "sigdr", "idsecteur", "idequipe", "datedebut"};
    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choixOm.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("sigdr").setLibelle("Direction regionale");
    pr.getFormu().getChamp("idsecteur").setLibelle("Antenne");
    pr.getFormu().getChamp("idequipe").setLibelle("Siege");
    pr.getFormu().getChamp("datedebut1").setLibelle("date  min");
    pr.getFormu().getChamp("datedebut2").setLibelle("date  max");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);

%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Ordre De Mission</h1>
            </section>
            <section class="content">
                <form action="code_fkt-choix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=controle/om/ordredemission-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[]={"Id","DR","Secteur", "Equipe","Date debut"};
		    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
