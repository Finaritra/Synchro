<%-- 
    Document   : listeCategorieImmoChoix
    Created on : 30 sept. 2015, 09:53:42
    Author     : GILEADA
--%>

<%@page import="mg.cnaps.st.Immo_identification.java"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>


<%
    String champReturn = request.getParameter("champReturn");
    Immo_identification e = new Immo_identification();
    String listeCrt[] = {"id","type", "marque", "modele", "num_serie","fabricant"};
    String listeInt[] = null;
    String libEntete[]= {"id","type", "marque", "modele", "num_serie","fabricant"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeIdentificationImmoChoix.jsp");
    pr.setChampReturn(champReturn);
    
    pr.getFormu().getChamp("id").setLibelle("ID");
    pr.getFormu().getChamp("type").setLibelle("Type");
    pr.getFormu().getChamp("marque").setLibelle("Marque");
    pr.getFormu().getChamp("modele").setLibelle("Modele");
    pr.getFormu().getChamp("num_serie").setLibelle("Numero de serie");
    pr.getFormu().getChamp("fabricant").setLibelle("Fabricant");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Identification</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[]= {"Id","Type", "Marque", "Modele", "Numero de serie","Fabricant"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>