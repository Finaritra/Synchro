<%-- 
    Document   : cieMedChoix
    Created on : 18 nov. 2015, 14:59:29
    Author     : user
--%>
<%@page import="mg.cnaps.cie.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    CieMed e = new CieMed();
    e.setNomTable("cie_med_libelle"); //si_dossiers_travailleurs_lib
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "daty", "motif", "type"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "daty", "motif", "type"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("cieMedChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("daty").setLibelleAffiche("Daty");
    pr.getFormu().getChamp("motif").setLibelleAffiche("Motif");
    pr.getFormu().getChamp("type").setLibelleAffiche("Type");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Mise en Demeure</h1>
            </section>
            <section class="content">
                <form action="cieMedChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="med" id="med">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=cie/med-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Date", "Motif", "Type"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>