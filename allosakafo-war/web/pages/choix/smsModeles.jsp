<%@page import="mg.cnaps.sms.SmsModele"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    SmsModele mod = new SmsModele();
    mod.setNomTable("SMS_MODELE_SERVICE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "service", "libelle", "message"};
    String listeInt[] = null;
    String libEntete[] =  {"id", "service", "libelle", "message"};
    PageRechercheChoix pr = new PageRechercheChoix(mod, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sms/smsModeles.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    SmsModele[] liste = (SmsModele[])pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Modeles SMS</h1>
            </section>
            <section class="content">
                <form action="smsModeles.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="mod" id="mod">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th>#</th>
                            <th>Id</th>
                            <th>Service</th>
                            <th>Libelle</th>
                            <th>Message</th>

                        </tr>
                        <%
                        for (int i = 0; i < liste.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=liste[i].getId()%>;<%=liste[i].getLibelle() %>;<%=liste[i].getService() %>;<%=liste[i].getMessage() %>" class="radio" /></td>
                            <td align=center><%=liste[i].getId()%></td>
                            <td align=center><%=liste[i].getService() %></td>
                            <td align=center><%=liste[i].getLibelle() %></td>
                            <td align=center><%=liste[i].getMessage() %></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>