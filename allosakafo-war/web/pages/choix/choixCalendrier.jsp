<%@page import="mg.cnaps.log.LogPersonnel"%>
<%@page import="java.sql.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.Timestamp"%>
﻿<%@page import="mg.cnaps.accueil.Dossiers"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.securite.*" %>
<%
   
    SecPlanning[] listeplanning = (SecPlanning[]) CGenUtil.rechercher(new SecPlanning(), null, null, "");
%>

<link href="${pageContext.request.contextPath}/assets/calendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/calendar/bootstrapmodal.css" rel="stylesheet" type="text/css" />


<div class="content-wrapper">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-head">
                <h1 class="box-title">Calendrier Planning</h1>
            </div>
           
        </div>
    </div>
    
    <div class="col-md-12">
        <div id="calendar"></div>
        <div id="fullCalModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 id="modalTitle" class="modal-title"></h1>                        
                    </div>
                    <div id="modalBody" class="modal-body"></div>
                    <div id="modalInfo" class="modal-info"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/assets/calendar/moment.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/calendar/jquery.min.js"></script>        
    <script src="${pageContext.request.contextPath}/assets/calendar/fullcalendar.min.js"></script>       
    <script src="${pageContext.request.contextPath}/assets/calendar/fr.js"></script>       
    <script src="${pageContext.request.contextPath}/assets/calendar/bootstrapmodal.min.js"></script> 

    <script>
                $(document).ready(function() {
        $('#calendar').fullCalendar({
        header: {
        left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
        },
                defaultDate: '<%=Utilitaire.dateDuJourSql().toString()%>',
                editable: true,
                lang: 'fr',
                eventLimit: true,
                events: [
        <% 
            int taille = listeplanning.length;
            for (int i = 0; i < taille ; i++) {
                    Timestamp t1 = listeplanning[i].getHeuredebut();
                    
                    boolean existPasTimeStamp = t1 == null;
                    if (existPasTimeStamp)
                    {
                        t1 = new Timestamp(2000, 0, 0, 0, 0, 0, 0);
                    }
                    
                    
                    Timestamp t2 = listeplanning[i].getHeurefin();                 
                    existPasTimeStamp = t2 == null;
                    if (existPasTimeStamp)
                    {
                        t2 = new Timestamp(2000, 0, 0, 0, 0, 0, 0);
                    }
                    
                  
                    String temp1 = Utilitaire.getHeureFromTimestamp(t1);
                    String temp2 = Utilitaire.getHeureFromTimestamp(t2);
                    Date datedujour = Utilitaire.dateDuJourSql();
                    String time1 = listeplanning[i].getDatedebut().toString() + " " + temp1;
                    String time2 = listeplanning[i].getDatefin().toString() + " " + temp2;
            %>
                    {
                    title: '<%=listeplanning[i].getRemarque()%>',
                            description: '',
                            start: '<%=listeplanning[i].getDatedebut().toString()%>T<%=temp1%>',
                                                end: '<%=listeplanning[i].getDatefin().toString()%>T<%=temp2%>'
            <%
                if (datedujour.after(listeplanning[i].getDatefin())) {
            %>
                                                            , backgroundColor: '#ef1c1c'
            <%
                }
                if (datedujour.before(listeplanning[i].getDatedebut())) {
            %>
                                                            , backgroundColor: '#5af044'
            <%
                }
            %>


            }
        <%
                boolean pasfin = i < taille - 1;
                if (pasfin)
                {
                    %>
                                    ,
                    <%
                }
            }
        %>                                                    
            ],
            eventClick:  function(event, jsEvent, view) {
            $('#modalTitle').html(event.title);
                    $('#modalBody').html(event.description);
                    $('#eventUrl').attr('href', '#');
                    $('#fullCalModal').modal();
}
                                                                                        });
                                                                                        });
    </script>
</div>