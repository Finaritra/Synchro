<%@page import="mg.cnaps.rappro.RapproCompte"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

     RapproCompte rc = new RapproCompte();
     rc.setNomTable("RAPPRO_COMPTE_LIBELLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"datedebut","datefin","compte"};
    String listeInt[] = {""};
    String libEntete[] = {"id","compte", "datedebut", "datefin", "soldecompta", "soldebanque","datevalidation"};
    PageRechercheChoix pr = new PageRechercheChoix(rc, request, listeCrt, listeInt, 1, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("datedebut").setLibelle("date d&eacutebut");
    pr.getFormu().getChamp("datefin").setLibelle("date fin");
    pr.setApres("rapproCompteChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = {"soldecompta", "soldebanque"};
    pr.creerObjetPage(libEntete, colSomme);
    RapproCompte [] listeP = (RapproCompte[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste compte � rapprocher </h1>
            </section>
            <section class="content">
                <form action="rapproCompteChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="rapprocompte" id="rapprocompte">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    
                     String libEnteteAffiche[] = {"id", "Compte", "date d&eacutebut", "date fin", "solde compta","solde banque","valid&eacute le"};
                     pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th></th>
                                    <th>id</th>
                                    <th>Compte</th>
                                    <th>Date debut</th>
                                    <th>Date fin</th>
                                    <th>Solde compta</th>
                                    <th>Solde banque</th>
                                   

                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getCompte()%>;<%=listeP[i].getDatedebut()%>;<%=listeP[i].getDatefin()%>;<%=listeP[i].getSoldecompta()%>;<%=listeP[i].getSoldebanque()%>" class="radio" /></td>
                                    <td align=center><%=listeP[i].getId()%></td>
                                    <td align=center><%=listeP[i].getCompte()%></td>
                                    <td align=center><%=Utilitaire.formatterDaty(listeP[i].getDatedebut())%></td>
                                    <td align=center><%=Utilitaire.formatterDaty(listeP[i].getDatefin())%></td>
                                    <td align=center><%=Utilitaire.formaterAr(listeP[i].getSoldecompta())%></td>
                                    <td align=center><%=Utilitaire.formaterAr(listeP[i].getSoldebanque())%></td>
                                    
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>