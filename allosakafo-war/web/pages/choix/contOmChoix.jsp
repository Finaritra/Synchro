<%-- 
    Document   : contOmChoix
    Created on : 8 d�c. 2015, 10:49:15
    Author     : user
--%>

<%@page import="mg.cnaps.controle.ContOrdreMission"%>
<%@page import="mg.cnaps.controle.ContControle"%>
<%@page import="mg.cnaps.controle.ContRendezvous"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    ContOrdreMission e = new ContOrdreMission();
    e.setNomTable("CONT_ORDRE_MISSION_LIBELLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "sigdr", "idsecteur", "idequipe", "idlogdepl", "arrondissement", "datedebut", "datefin"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "sigdr", "idsecteur", "idequipe", "idlogdepl", "arrondissement", "datedebut", "datefin"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("contOmChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("sigdr").setLibelleAffiche("Direction r�gional");
    pr.getFormu().getChamp("idsecteur").setLibelleAffiche("Antenne/service");
    pr.getFormu().getChamp("idequipe").setLibelleAffiche("Siege");
    pr.getFormu().getChamp("idlogdepl").setLibelleAffiche("D�placement");
    pr.getFormu().getChamp("datedebut").setLibelleAffiche("Date d�but");
    pr.getFormu().getChamp("datefin").setLibelleAffiche("Date fin");
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des ordres de mission</h1>
            </section>
            <section class="content">
                <form action="contOmChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="om" id="om">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=controle/om/om-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] ={"id", "Dir�ction r�gionnal", "Secteur", "Equipe", "Deplacement", "Arrondissement", "Date debut", "Date fin"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>