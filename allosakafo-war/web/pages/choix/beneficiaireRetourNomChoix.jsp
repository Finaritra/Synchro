<%-- 
    Document   : beneficiaireRetourNomChoix
    Created on : 12 juil. 2016, 11:29:53
    Author     : Safidimahefa
--%>
<%@page import="mg.cnaps.accueil.Sig_beneficiaire"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    Sig_beneficiaire e = new Sig_beneficiaire();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "matricule", "type","nom","cin"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "matricule", "type","nom","cin"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("beneficiaireRetourNomChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("matricule").setLibelleAffiche("Matricule");
    pr.getFormu().getChamp("type").setLibelleAffiche("Type beneficiaire");
    pr.getFormu().getChamp("nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("cin").setLibelleAffiche("CIN");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    Sig_beneficiaire[] liste = (Sig_beneficiaire[])pr.getRs().getResultat();

%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste bénéficiaire</h1>
            </section>
            <section class="content">
                <form action="beneficiaireRetourNomChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="benef" id="benef">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "matricule", "type","nom","cin"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    //out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixRetourNomBeneficiaire.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <p align="center"><strong><u>LISTE</u></strong></p>
                    <div id="divchck">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table" table-hover="">
                            <tbody>
                                <tr class="head">
                                    <td align="center" valign="top"></td>
                                    <td width="33%" align="center" valign="top">Id</td>
                                    <td width="33%" align="center" valign="top">Matricule</td>
                                    <td width="33%" align="center" valign="top">Type</td>
                                    <td width="33%" align="center" valign="top">Nom</td>
                                    <td width="33%" align="center" valign="top">Cin</td>
                                </tr>
                                <%for(int i = 0 ; i<liste.length ; i++){%>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
                                        <td align="center">
                                        <input type="radio" value="<%=liste[i].getId()%>;<%=liste[i].getNom()%>;<%=pr.getChampReturn()%>" name="choix" onmousedown="getChoix()" id="choix" class="radio"></td>
                                        <td width="33%" align="center"><%=liste[i].getId() %></td>
                                        <td width="33%" align="center"><%=liste[i].getMatricule() %></td>
                                        <td width="33%" align="center"><%=liste[i].getType()%></td>
                                        <td width="33%" align="center"><%=liste[i].getNom() %></td>
                                        <td width="33%" align="center"><%=liste[i].getCin() %></td>
                                    </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>        
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
