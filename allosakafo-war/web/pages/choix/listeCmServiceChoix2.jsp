<%-- 
    Document   : listeServiceChoix
    Created on : 20 janv. 2016, 17:34:58
    Author     : ITU
--%>


<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.immo.ImmoLogService"%>

<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.immo.Immo_nature"%>
<%

    String champReturn = request.getParameter("champReturn");
    ImmoLogService e = new ImmoLogService();
    String listeCrt[] = {"id", "libelle", "code_dr", "dr_rattache", "code_service"};

    String listeInt[] = null;
    String libEntete[] = {"id", "libelle", "code_dr", "dr_rattache", "code_service"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 5, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.getFormu().getChamp("id").setLibelle("ID");
    pr.getFormu().getChamp("libelle").setLibelle("Libelle");
    
    pr.getFormu().getChamp("code_service").setLibelle("Code Service");
    pr.getFormu().getChamp("dr_rattache").setLibelle("DR Rattach�");
    pr.getFormu().getChamp("code_dr").setLibelle("Code DR");
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeServiceChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    ImmoLogService[] listeP=(ImmoLogService[])pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste services</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[]= {"id", "Libelle", "Code DR", "DR rattach�", "Code service"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixCm.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <input type="hidden" name="personnel-id" value="personnel"/>
					<input type="hidden" name="detenteur-id" value="service"/>
					
				   <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Service</th>
                                    <th>N�Direction</th>
                                    <th>Direction</th>
                                    <th>Code service</th>
                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                    <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>; " class="radio" /></td>
                                    <td align=left><%=listeP[i].getId()%></td>
                                    <td align=left><%=listeP[i].getLibelle()%></td>
                                    <td align=left><%=listeP[i].getCode_dr()%></td>
                                    <td align=left><%=listeP[i].getDr_rattache()%></td>
                                    <td align=left><%=listeP[i].getCode_service()%></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

