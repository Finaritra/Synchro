<%-- 
    Document   : archEtagereChoix
    Created on : 29 d�c. 2015, 14:16:37
    Author     : user
--%>
<%@page import="mg.cnaps.accueil.SousPrestations"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.cnaps.accueil.TypesPieces"%>
<%
    SousPrestations lv = new SousPrestations();
    //lv.setNomTable("sig_pieces_recues_libelle2");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "sous_prestation_libelle", "prestation_code","sous_prestation_abreviation","code_imputation"};
    String listeInt[] = null;
    String libEntete[] = {"id", "sous_prestation_libelle", "prestation_code","sous_prestation_abreviation","code_imputation"};
    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sigsousprestationsChoix.jsp");
    pr.setChampReturn(champReturn);
    
    
    pr.getFormu().getChamp("sous_prestation_libelle").setLibelleAffiche("Sous prestation");
    pr.getFormu().getChamp("prestation_code").setLibelleAffiche("Prestation code");
    pr.getFormu().getChamp("sous_prestation_abreviation").setLibelleAffiche("Sous prestation Abr�viation");
    pr.getFormu().getChamp("code_imputation").setLibelleAffiche("Imputation");
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des sous prestations </h1>
            </section>
            <section class="content">
                <form action="sigsousprestationsChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    
                    String libelles[] =  {"id", "sous_prestation_libelle", "prestation_code","sous_prestation_abreviation","code_imputation"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>