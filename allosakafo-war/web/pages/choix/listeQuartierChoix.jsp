<%-- 
    Document   : listeClientChoix.jsp
    Created on : 2 d�c. 2016, 13:46:50
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@page import="mg.allosakafo.secteur.Quartier"%>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>

<%
    String champReturn = request.getParameter("champReturn");
    Quartier e = new Quartier();
	e.setNomTable("AS_QUARTIER_LIBELLE");
    //e.setNomTable("");
    String listeCrt[] = {"ID","NOM", "SECTEUR"};
    String listeInt[] = null;
    String libEntete[] = {"ID","NOM", "SECTEUR"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeQuartierChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Allo Sakafo 1.0</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Quartier</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Quartier</th>
                                    <th>Secteur</th>
                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {                                
                            %>
                            <tr>
                                    <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getVal()%>;<%=listeP[i].getDesce()%>%>" class="radio" /></td>
                                    <td align=left><%=listeP[i].getId()%></td>
                                    <td align=left><%=listeP[i].getVal()%></td>
                                    <td align=left><%=listeP[i].getDesce()%></td>
									
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>