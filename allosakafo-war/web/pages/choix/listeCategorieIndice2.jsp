<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.paie.*" %>


<%
    PaieIndiceLibelle e = new PaieIndiceLibelle();
    e.setNomTable("paie_indicelibelle");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "categorie", "classee", "echelon", "indice"};
    String listeInt[] = null;
    String libEntete[]= {"id", "categorie", "classee", "echelon", "indice"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeCategorieIndice2.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("classee").setLibelleAffiche("Classe");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    PaieIndiceLibelle[] listeP = (PaieIndiceLibelle[]) pr.getRs().getResultat();
    //"matrcnaps;raisonsocial;nif;stat;adresse;taxe;codeposte"
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des categories</h1>
            </section>
            <section class="content">
                <form action="listeCategorieIndice2.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    //String lienTableau[] = {pr.getLien() + "?but=employeur/employeur-fiche.jsp"};
                    //String colonneLien[] = {"id"};
                    String libelles[] = {"Id","Nom", "T�l�phone", "Direction r�gionale"};
		    //pr.getTableau().setLien(lienTableau);
                    //pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                    //
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultipleCtg.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id categorie</th>
                                    <th>Categorie</th>
                                    <th>Classe</th>
                                    <th>Echelon</th>
                                    <th>Indice</th>

                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getIndice()%>;<%=listeP[i].getIndice()%>;<%=listeP[i].getCategorie()%>" class="radio" /></td>
                                    <td align=center><%=listeP[i].getId()%></td>
                                    <td align=center><%=listeP[i].getCategorie()%></td>
                                    <td align=center><%=listeP[i].getClassee()%></td>
                                    <td align=center><%=listeP[i].getEchelon()%></td>
                                    <td align=center><%=listeP[i].getIndice()%></td>
                                    
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

