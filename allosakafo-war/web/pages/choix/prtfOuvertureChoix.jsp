<%-- 
    Document   : prtfOuvertureChoix
    Created on : 13 nov. 2015, 16:28:23
    Author     : user
--%>
<%@page import="mg.cnaps.prtf.PrtfOuverture"%>
<%@page import="mg.cnaps.pret.PretEmprunt"%>
<%@page import="mg.cnaps.st.StArticle"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>


<%
    String champReturn = request.getParameter("champReturn");
    PrtfOuverture e = new PrtfOuverture();
    e.setNomTable("PRTF_OUVERTURE");
    String listeCrt[] = {"id", "numero", "ref_dossier", "id_type_placement", "date_ouverture","date_constatation","date_echeance"};
    String listeInt[] = null;
    String libEntete[] = {"id", "numero", "ref_dossier", "id_type_placement", "montant_adjuge", "montant_propose", "date_ouverture","date_constatation","date_echeance", "duree"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 10);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("prtfOuvertureChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("ref_dossier").setLibelleAffiche("Dossier");
    pr.getFormu().getChamp("id_type_placement").setLibelleAffiche("Type Placement");
//    pr.getFormu().getChamp("date_ouverture").setLibelleAffiche("Date ouverture");
//    pr.getFormu().getChamp("date_constatation").setLibelleAffiche("Date Constatation");
//    pr.getFormu().getChamp("montant_propose").setLibelleAffiche("Montant propos�");
//    pr.getFormu().getChamp("montant_adjuge").setLibelleAffiche("Montant adjug�");
//    pr.getFormu().getChamp("date_echeance").setLibelleAffiche("Date �ch�ance");
//    pr.getFormu().getChamp("duree").setLibelleAffiche("Dur�e");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste ouverture</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="ouverture" id="ouverture">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[]= {"ID", "Numero", "Dossier", "Type placement", "Montant adjug�", "Montant propos�", "Date d'ouverture","Date constatation","Date �cheance", "Dur�e"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

