<%@page import="mg.cnaps.commun.Sig_commune"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    Sig_commune e = new Sig_commune();
    e.setNomTable("sig_commune_libelle");
    String listeCrt[] = {"id", "val", "desce", "code_region"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "val", "desce", "code_region"};
    PageRecherche pr = new PageRecherche(e, request, listeCrt, listeInt, 2, libEntete, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("code_communeChoix.jsp");

    pr.getFormu().getChamp("id").setLibelleAffiche("Code commune");
    pr.getFormu().getChamp("val").setLibelleAffiche("Nom commune");
    pr.getFormu().getChamp("desce").setLibelleAffiche("Description");
    pr.getFormu().getChamp("code_region").setLibelleAffiche("Code region");

    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    Sig_commune[] listeP = (Sig_commune[]) pr.getRs().getResultat();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste communes</h1>
            </section>
            <section class="content">
                <form action="code_communeChoix.jsp?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="code_communeChoix" id="code_communeChoix">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=configuration/fiche_idvaldesce.jsp"};
                    String colonneLien[] = {"id"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>