<%-- 
    Document   : archEtagereChoix
    Created on : 29 d�c. 2015, 14:16:37
    Author     : user
--%>
<%@ page import="mg.cnaps.archive.ArchPaquet"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    ArchPaquet lv = new ArchPaquet();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"ID", "IDSERVICE", "idarchive","TYPE_PIECE"};
    String listeInt[] = null;
    String libEntete[] = {"ID", "IDSERVICE", "idarchive","DESCRIPTION","TYPE_PIECE"};
    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("archPaquetChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("ID").setLibelleAffiche("ID");
    pr.getFormu().getChamp("IDSERVICE").setLibelleAffiche("Service");
    pr.getFormu().getChamp("idarchive").setLibelleAffiche("Bordereaux");
    pr.getFormu().getChamp("TYPE_PIECE").setLibelleAffiche("Type pi�ce");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des paquets </h1>
            </section>
            <section class="content">
                <form action="archPaquetChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    
                    String libelles[] =  {"Id", "Service", "Bodereaux", "Detail", "Type de pi�ce"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
