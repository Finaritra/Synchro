<%@page import="mg.cnaps.compta.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
	ComptaCompte lv = new ComptaCompte();
    lv.setNomTable("COMPTA_COMPTE_CHOIX");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"compte", "libelle", "typeCompte", "classe"};
    String listeInt[] = null;
    String libEntete[] = {"id", "compte", "libelle", "typeCompte", "classe"};
    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("libelle").setLibelle("Libell�");
    pr.getFormu().getChamp("typeCompte").setLibelle("Type de compte");
    pr.getFormu().getChamp("compte").setLibelle("Compte");

    pr.setApres("numCompteGeneralRetourLibelleChoix.jsp");
    pr.setChampReturn(champReturn);

    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    ComptaCompte[] liste = (ComptaCompte[])pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste de num�ro de comptes g�n�ral</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
					out.println(pr.getTableauRecap().getHtml());

                    String libelles[] = {"Id", "Compte", "Libell�", "Type de Compte", "classe"};
                    pr.getTableau().setLibelleAffiche(libelles);
				%>
				
				
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixIdvaldesceRetourLibelle.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <input type="hidden" name="suffixe" value="compte_numero">
					
					
                    <p align="center"><strong><u>LISTE</u></strong></p>
                    <div id="divchck">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table" table-hover="">
                            <thead>
									<tr class="head">
										<th align="center" valign="top"></th>
										<th width="33%" align="center" valign="top">Compte</thth>
										<th width="33%" align="center" valign="top">Libelle</th>
										<th width="33%" align="center" valign="top">Type�Compte</th>
										<th width="33%" align="center" valign="top">Classe</th>
									 </tr>
								 </thead>
							<tbody>
								
                                <%for(int i = 0 ; i<liste.length ; i++){%>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
                                        <td align="center">
                                        <input type="radio" value="<%=liste[i].getId()%>;<%=liste[i].getLibelle() %>" name="choix" onmousedown="getChoix()" id="choix" class="radio"></td>
                                        <td width="33%" align="center"><%= liste[i].getCompte()%></td>
                                        <td width="33%" align="center"><%=liste[i].getLibelle() %></td>
                                        <td width="33%" align="center"><%=liste[i].getTypeCompte() %></td>
                                       <td width="33%" align="center"><%=liste[i].getClasse() %></td>
                                     </tr>
                                <%}%>
                            </tbody>
                        </table>
                </form>
				
				
				
				
				
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
