<%-- 
    Document   : listeProduitChoix
    Created on : 30 sept. 2019, 16:24:02
    Author     : pro
--%>

<%@page import="mg.allosakafo.produits.Produits"%>
<%@page import="mg.allosakafo.produits.AccompagnementSauce"%>
<%@ page import="user.*" %>
<%@page import="mg.allosakafo.tiers.ClientAlloSakafo"%>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>

<%
    String champReturn = request.getParameter("champReturn");
     TypeObjet to=new TypeObjet(); 
    to.setNomTable("tableclientvue2");
    String listeCrt[] = {"id","val"};
    String listeInt[] = null;
    String libEntete[] = {"id", "val", "desce"};
    PageRechercheChoix pr = new PageRechercheChoix(to, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choix/listeTableChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Allo Sakafo 1.0</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des Tables</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getLien()%>?but=<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
