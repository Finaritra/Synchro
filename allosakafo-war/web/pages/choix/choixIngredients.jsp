<%-- 
    Document   : listeClientChoix.jsp
    Created on : 2 d�c. 2016, 13:46:50
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@page import="mg.allosakafo.produits.Ingredients"%>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>

<%
    String champReturn = request.getParameter("champReturn");
    Ingredients lv = new Ingredients();
    lv.setNomTable("AS_INGREDIENTS_LIBELLE2");
    
    String listeCrt[] = {"id","libelle", "unite"};
    String listeInt[] = null;
    String libEntete[] = {"id", "libelle", "unite", "pu", "quantiteparpack", "seuil"};
    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choixIngredients.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
	
	Ingredients[] listeP = (Ingredients[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Allo Sakafo 1.0</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Ingredients</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th><b>#</b></th>
                            <th><b>Id</b></th>
                            <th><b>Libelle</b></th>
                            <th><b>Unit&eacute;</b></th>
                            <th><b>Pu</b></th>
                            <th><b>Quantit&eacute; par pack</b></th>
                            <th><b>Seuil</b></th>
                        </tr>
                        <%
                            for (int i = 0; i < listeP.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getLibelle()%>;<%=listeP[i].getUnite()%>" class="radio" /></td>
                            <td align=left><%=listeP[i].getId()%></td>
                            <td align=left><%=listeP[i].getLibelle()%></td>
                            <td align=left><%=listeP[i].getUnite()%></td>
                            <td align=left><%=Utilitaire.formaterAr(listeP[i].getPu())%></td>
                            <td align=left><%=Utilitaire.formaterAr(listeP[i].getQuantiteparpack())%></td>
                            <td align=right><%=Utilitaire.formaterAr(listeP[i].getSeuil()) %></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>