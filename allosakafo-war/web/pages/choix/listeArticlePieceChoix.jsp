
<%@page import="mg.cnaps.log.LogMaintenanceFilleType"%>
<%@page import="mg.cnaps.st.StArticle"%>
<%@page import="mg.cnaps.st.StMagasin"%>
<%@page import="user.*"%>
<%@page import="bean.*"%>
<%@page import="utilitaire.*"%>
<%@page import="affichage.*"%>
<%@page import="mg.cnaps.sig.*"%>
<%@page import="mg.cnaps.commun.Constante" %>


<%
    String champReturn = request.getParameter("champReturn");
    LogMaintenanceFilleType e = new LogMaintenanceFilleType();
    e.setNomTable("ST_ARTICLE_PU_QTT_TYPE");

    String listeCrt[] = {"id", "designation", "pu", "quantite"};
    String listeInt[] = null;
    String libEntete[] = {"id", "designation", "pu", "quantite"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setAWhere(" and TYPE in (" + Constante.listeIdArticleVehicule + ")");
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeArticlePieceChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Article</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
		    String libelles[] = {"ID", "Designation", "PU Actuelle", "Quantit� Dispo."};
		    pr.getTableau().setLibelleAffiche(libelles);
		    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixId.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
