<%@page import="mg.cnaps.medecine.MedBeneficiaireComplet"%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="user.UserEJB"%>
<%
    try{
    UserEJB u = (UserEJB) session.getAttribute("u");
    String listeCrt[] = {"matriculeagent", "nomagent", "prenomagent"};
    String listeInt[] = {};
    String libEntete[] = {"id", "nom", "prenom", "type", "agepers"};
    MedBeneficiaireComplet cons = new MedBeneficiaireComplet();
    PageRechercheChoix pr = new PageRechercheChoix(cons, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setChampReturn(request.getParameter("champReturn"));
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choixPatientMultiple.jsp");
    pr.getFormu().getChamp("matriculeagent").setLibelle("matricule agent");
    pr.getFormu().getChamp("nomagent").setLibelle("Nom agent");
    pr.getFormu().getChamp("prenomagent").setLibelle("Prenom agent");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
%>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste patients</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="cc" id="cc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%

                    String libelles[] = {"Id", "Nom", "Prenom", "Genre", "Age"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
<% }catch(Exception ex){
    ex.printStackTrace();
}%>
