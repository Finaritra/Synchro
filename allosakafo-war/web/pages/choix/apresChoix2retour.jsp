<%-- 
    Document   : apresChoix2retour
    Created on : 17 mars 2016, 09:38:41
    Author     : Tafitasoa
--%>
<%@page import="utilitaire.Utilitaire"%>
<html>
    <%
        String champRet =(String) request.getParameter("champReturn");
        String choix = (String) request.getParameter("choix");
        String[] champs = Utilitaire.split(champRet, ";");
        String[] lstChoix = Utilitaire.split(choix, ";");
    %>
    <script language="JavaScript">
        window.opener.document.all.<%=champs[0]%>.value = "<%=lstChoix[0]%>";
        window.opener.document.all.<%=champs[1]%>.value = "<%=lstChoix[1]%>";
        window.close();
    </script>
</html>
