<%-- 
    Document   : apresChoixSaisieTef
    Created on : 7 juin 2016, 14:37:49
    Author     : Ignafah
--%>

<%@page import="mg.cnaps.paie.PaieInfoPersonnel"%>
<%@page import="mg.cnaps.sig.SigEmployeursComptes"%>
<%@page import="bean.TypeObjet"%>
<%@page import="bean.CGenUtil"%>
<%@page import="utilitaire.Utilitaire"%>
<html>
    <%
        String champRet =(String) request.getParameter("champReturn");
        String choix = (String) request.getParameter("choix");
        String cible = (String) request.getParameter("cible");
        String[] champs = Utilitaire.split(champRet, ";");
        String[] lstChoix = Utilitaire.split(choix, ";");
        System.out.println("LIST CHOIX: "+lstChoix[0]);
        if(cible!=null && cible.compareToIgnoreCase("beneficiaire")==0){
            System.out.println("ID RECU: "+lstChoix[0]);
            for(int i=0; i<champs.length;i++){            
                TypeObjet e = new TypeObjet();
                e.setNomTable("trs_banques");
                TypeObjet[] trsBank = (TypeObjet[]) CGenUtil.rechercher(e, null, null, " AND ID ='"+lstChoix[3]+"'");
                %>
                    <script language="JavaScript">
                        window.opener.document.all.<%=champs[0]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[0])%>";
                        window.opener.document.all.<%=champs[1]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[1])%>";
                        window.opener.document.all.<%=champs[2]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[2])%>";  
                        window.opener.document.all.<%=champs[4]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[4])%>"; 
                        window.opener.document.all.<%=champs[5]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[5])%>"; 
                    </script>
                <%
                if(trsBank!=null && trsBank.length > 0){
                %>
                <script language="JavaScript">                    
                    window.opener.document.all.<%=champs[3]%>.value = "<%=utilitaire.Utilitaire.champNull(trsBank[0].getId())%>";                    
                </script>
                <%
                
                }
            }        
        }
        if(cible!=null && cible.compareToIgnoreCase("travailleur")==0){
            %>
            <script language="JavaScript">
                window.opener.document.all.<%=champs[0]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[0])%>";
                window.opener.document.all.<%=champs[1]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[1])+" "+utilitaire.Utilitaire.champNull(lstChoix[2])%>";
            </script>
            <%
        
        }
        if(cible!=null && cible.compareToIgnoreCase("employeur")==0){
            %>
            <script language="JavaScript">
                        window.opener.document.all.<%=champs[0]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[0])%>";
                        window.opener.document.all.<%=champs[1]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[1])%>";
            </script>
            <%            
                SigEmployeursComptes[] sigemp = (SigEmployeursComptes[]) CGenUtil.rechercher(new SigEmployeursComptes(), null, null, " AND MERE ='"+lstChoix[0]+"'");
                if(sigemp!=null && sigemp.length > 0){
                %>
                <script language="JavaScript">
                    window.opener.document.all.<%=champs[2]%>.value = "<%=utilitaire.Utilitaire.champNull(sigemp[0].getAgence_code())%>";
                    window.opener.document.all.<%=champs[3]%>.value = "<%=utilitaire.Utilitaire.champNull(sigemp[0].getBanque_code())%>";
                    window.opener.document.all.<%=champs[4]%>.value = "<%=utilitaire.Utilitaire.champNull(sigemp[0].getCompte_numero())%>";
                    window.opener.document.all.<%=champs[5]%>.value = "<%=utilitaire.Utilitaire.champNull(sigemp[0].getCompte_cle())%>";
                 </script>
            
            <%
                }
        }
        if(cible!=null && cible.compareToIgnoreCase("fournisseur")==0){
            %>
            <script language="JavaScript">
                window.opener.document.all.<%=champs[0]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[0])%>";
                window.opener.document.all.<%=champs[1]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[1])%>";
            </script>
            <%   
                SigEmployeursComptes sec = new SigEmployeursComptes();
                sec.setNomTable("SIG_EMPLOYEURS_COMPTES");
                SigEmployeursComptes[] sigemp = (SigEmployeursComptes[]) CGenUtil.rechercher(sec, null, null, " AND MERE ='"+lstChoix[2]+"'");
                if(sigemp!=null && sigemp.length > 0){
            %>
                <script language="JavaScript">
                    window.opener.document.all.<%=champs[2]%>.value = "<%=utilitaire.Utilitaire.champNull(sigemp[0].getAgence_code())%>";
                    window.opener.document.all.<%=champs[3]%>.value = "<%=utilitaire.Utilitaire.champNull(sigemp[0].getBanque_code())%>";
                    window.opener.document.all.<%=champs[4]%>.value = "<%=utilitaire.Utilitaire.champNull(sigemp[0].getCompte_numero())%>";
                    window.opener.document.all.<%=champs[5]%>.value = "<%=utilitaire.Utilitaire.champNull(sigemp[0].getCompte_cle())%>";
                    window.opener.document.all.<%=champs[6]%>.value = "<%=utilitaire.Utilitaire.champNull(sigemp[0].getCompte_date().toString())%>";
                 </script>
            
            <%
            }
        }
        if(cible!=null && cible.compareToIgnoreCase("personnel")==0){
            %>
            <script language="JavaScript">
                window.opener.document.all.<%=champs[0]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[0])%>";
                window.opener.document.all.<%=champs[1]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[1])+" "+utilitaire.Utilitaire.champNull(lstChoix[2])%>";
            </script>
            <%
                PaieInfoPersonnel[] pip = (PaieInfoPersonnel[]) CGenUtil.rechercher(new PaieInfoPersonnel(), null, null, " AND ID ='"+lstChoix[0]+"'");
                if(pip!=null && pip.length > 0){
                    TypeObjet b = new TypeObjet();
                    b.setNomTable("TRS_BANQUES_AGENCES");
                    %>
                        <script language="JavaScript">
                            window.opener.document.all.<%=champs[2]%>.value = "<%=utilitaire.Utilitaire.champNull(pip[0].getCode_agence_banque())%>";                            
                            window.opener.document.all.<%=champs[4]%>.value = "<%=utilitaire.Utilitaire.champNull(pip[0].getBanque_numero_compte() )%>";
                            window.opener.document.all.<%=champs[5]%>.value = "<%=utilitaire.Utilitaire.champNull(pip[0].getBanque_compte_cle())%>";
                         </script>            
                    <%                    
                    TypeObjet[] banque = (TypeObjet[]) CGenUtil.rechercher(b, null, null, "");
                    if(banque!=null && banque.length > 0){
                    %>
                    <script language="JavaScript">
                        window.opener.document.all.<%=champs[3]%>.value = "<%=utilitaire.Utilitaire.champNull(banque[0].getVal())%>";
                     </script>            
                    <%
                    }
            }
        }
    %>
<script language="JavaScript"> 
    window.close();
</script>
</html>
