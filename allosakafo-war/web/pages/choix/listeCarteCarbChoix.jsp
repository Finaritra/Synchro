<%--
    Document   : stTiersChoix
    Created on : 29 oct. 2015, 16:11:35
    Author     : user
--%>
<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="mg.cnaps.log.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    LogCarteCarbDemRecTEF e = new LogCarteCarbDemRecTEF();
    e.setNomTable("Log_Carte_Carb_Dem_Rec_TEF");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "idtef", "idcarte", "montant", "datedemande", "montant_accorde"};
    String listeInt[] = {"montant", "montant_accorde"};
    String libEntete[] = {"id", "idtef", "idcarte", "montant", "datedemande", "montant_accorde"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeCarteCarbChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("idtef").setLibelleAffiche("TEF");
    pr.getFormu().getChamp("idcarte").setLibelleAffiche("Numero carte");
    pr.getFormu().getChamp("datedemande").setLibelleAffiche("Date demande");
//    pr.getFormu().getChamp("montant1").setLibelleAffiche("Montant demand� min");
//    pr.getFormu().getChamp("montant2").setLibelleAffiche("Montant demand� max");
//    pr.getFormu().getChamp("montant_accorde1").setLibelleAffiche("Montant accord� min");
//    pr.getFormu().getChamp("montant_accorde2").setLibelleAffiche("Montant accord� max");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste tiers</h1>
            </section>
            <section class="content">
                <form action="stTiersChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="tiers" id="tiers">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
		    String libelles[] = {"Id", "TEF", "Numero Carte", "Montant Demand�", "Date Demande", "Montant Accord�"};
		    pr.getTableau().setLibelleAffiche(libelles);
		    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>