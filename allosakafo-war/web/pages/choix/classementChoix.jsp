<%-- 
    Document   : classementChoixMultiple
    Created on : 2 juin 2016, 10:47:50
    Author     : RAMARONERA
--%>

<%@page import="mg.cnaps.archive.ArchClassementView"%>
<%@page import="affichage.PageRechercheChoix"%>
<%

    ArchClassementView lv = new ArchClassementView();
   String listeCrt[] = {"id","nature_dossier","matricule","periode"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "nature_dossier","matricule","periode"};
    String champReturn = request.getParameter("champReturn");
    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("classementChoix.jsp");
    pr.getFormu().getChamp("nature_dossier").setLibelle("Nature Dossier");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);%>
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste dossiers </h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="cc" id="cc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultipleClassement.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                   <input type="hidden" name="lien" value="<%=pr.getLien() + "?but=archive/sigPieceRecueListeAjax.jsp"%>">
                    <% 
		    String libEnteteAffiche[] = {"Id", "Nature dossier","Matricule","Periode"};
		    pr.getTableau().setLibelleAffiche(libEnteteAffiche);		    
		    out.println(pr.getTableau().getHtmlWithRadioButton()); 
                    %>
                </form>
                <% out.println(pr.getBasPage());%>
            
			<div id="container-piece-liste" class="row">
			</div>
			</section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

