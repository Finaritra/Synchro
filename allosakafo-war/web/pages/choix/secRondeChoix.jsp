<%-- 
    Document   : secRondeChoix
    Created on : 18 nov. 2015, 16:31:51
    Author     : user
--%>
<%@page import="mg.cnaps.securite.SecRonde"%>
<%@page import="mg.cnaps.securite.SecRapport"%>
<%@page import="mg.cnaps.cie.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    SecRonde e = new SecRonde();
    e.setNomTable("SEC_RONDE_LIBELLE"); //si_dossiers_travailleurs_lib
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "idsite", "debut", "fin"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "idsite", "debut", "fin", "remarque"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("secRondeChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("idsite").setLibelleAffiche("Site");
    pr.getFormu().getChamp("debut").setLibelleAffiche("Debut");
    pr.getFormu().getChamp("fin").setLibelleAffiche("Fin");
//    pr.getFormu().getChamp("remarque").setLibelleAffiche("Remarque");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Ronde</h1>
            </section>
            <section class="content">
                <form action="secRondeChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="ronde" id="ronde">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/securite/ronde-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Personnel", "D�but", "Fin", "Remarque"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>