<%-- 
    Document   : listeNatureImmoChoix
    Created on : 29 d�c. 2015, 14:16:37
    Author     : andry
--%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.immo.Immo_nature"%>
<%@page import="mg.cnaps.immo.ImmoCategorie"%>
<%@page import="bean.CGenUtil"%>


<%
    Immo_nature lv = new Immo_nature();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "cle_nature", "nature", "libelle", "classee"};
    String listeInt[] = null;
    String libEntete[] = {"id", "cle_nature", "nature", "libelle", "classee"};
    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeNatureImmoChoix.jsp");
    pr.setChampReturn(champReturn);

    String catego = request.getParameter("categorie");
    String id = request.getParameter("categorie");
    boolean categoryFiltreExist = false;
    if (id != null) {
        categoryFiltreExist = id.equals("") == false;
    }
    if (categoryFiltreExist) {
        ImmoCategorie c = new ImmoCategorie();
        c.setId(id);
        ImmoCategorie[] listecompte = null;
        listecompte = (ImmoCategorie[]) CGenUtil.rechercher(c, null, null, "");

        ImmoCategorie categoTrouve = listecompte[0];
        boolean categoExist = listecompte.length > 0;
        if (categoExist) {
            catego = categoTrouve.getCategorie();
            System.out.print("cateGoExist = " + catego);
        }

        String aWhere = " and classee='" + catego + "'";
        System.out.println(aWhere);
        pr.setAWhere(aWhere);
    }

    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des �tag�res </h1>
            </section>
            <section class="content">
                <form action="listeNatureImmoChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%

                    String libelles[] = {"Id", "Service", "Dossier", "Description", "Type de pi�ce"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
