<%-- 
    Document   : medecinChoix
    Created on : 26 oct. 2015, 09:17:31
    Author     : matiasy
--%>
<%@page import="mg.cnaps.accident.SigAtmpRente"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    SigAtmpRente s = new SigAtmpRente();
    s.setNomTable("SIG_ATMP_RENTE");
    String champReturn = request.getParameter("champReturn");
   /* String aWhere = request.getParameter("type");*/
   /* if(aWhere == null) aWhere = "";
    else aWhere = " and type='"+aWhere+"'";*/
    String listeCrt[] = {"id", "typerente"};
  
    String libEntete[] =  {"id", "typerente"};
    PageRechercheChoix pr = new PageRechercheChoix(s, request, listeCrt, null, 2, libEntete, 2);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
 //  pr.getC
   // pr.setApres("medecinChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = {};

  //  pr.setAWhere(aWhere);
    pr.creerObjetPage(libEntete, colSomme);
    SigAtmpRente[] liste = (SigAtmpRente[])pr.getRs().getResultat();
   
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Rente</h1>
            </section>
            <section class="content">
                <form action="atmp_rente-choix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="atmp_rente" id="atmp_rente">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "type rente"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>