<%-- 
    Document   : stTiersChoix
    Created on : 29 oct. 2015, 16:11:35
    Author     : user
--%>
<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="mg.cnaps.log.LogPersonnel"%>
<%@page import="mg.cnaps.log.LogDeplacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    StTiers e = new StTiers();
    e.setNomTable("ST_TIERS_INFO");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "raisonsocial", "adresse","typetiers"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "raisonsocial", "adresse","typetiers"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("stTiersChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("raisonsocial").setLibelleAffiche("Raison social");
    pr.getFormu().getChamp("adresse").setLibelleAffiche("Adresse");
    pr.getFormu().getChamp("typetiers").setLibelleAffiche("Type tiers");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste tiers</h1>
            </section>
            <section class="content">
                <form action="stTiersChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="tiers" id="tiers">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=stock/tiers/tiers-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "raisonsocial", "adresse","typetiers"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>