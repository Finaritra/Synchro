<%-- 
    Document   : medDemandeChoix
    Created on : 26 oct. 2015, 09:07:02
    Author     : user
--%>
<%@page import="mg.cnaps.compta.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    ComptaCompte lv = new ComptaCompte();
    lv.setNomTable("COMPTA_COMPTE_CHOIX");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"compte", "libelle", "typeCompte", "classe"};
    String listeInt[] = null;
    String libEntete[] = {"id", "compte", "libelle", "typeCompte", "classe"};
    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("libelle").setLibelle("Libell�");
    pr.getFormu().getChamp("typeCompte").setLibelle("Type de compte");
    pr.getFormu().getChamp("compte").setLibelle("Compte");

    pr.setApres("numCompteGeneralChoix.jsp");
    pr.setChampReturn(champReturn);

    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    ComptaCompte[] listeP = (ComptaCompte[])pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste de num�ro de comptes g�n�ral</h1>
            </section>
            <section class="content">
                <form action="numCompteGeneralChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="demande" id="demande">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());

                    String libelles[] = {"Id", "Compte", "Libell�", "Type de Compte", "classe"};
                    pr.getTableau().setLibelleAffiche(libelles);
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th>#</th>
                            <th>Id</th>
                            <th>Compte</th>
                            <th>Libell�</th>
                            <th>Type de Compte</th>
                            <th>Classe</th>

                        </tr>
                        <%
                            for (int i = 0; i < listeP.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getCompte() %>" class="radio" /></td>
                            <td align=center><%=listeP[i].getId()%></td>
                            <td align=center><%=Utilitaire.champNull(listeP[i].getCompte())%></td>
                            <td align=center><%=Utilitaire.champNull(listeP[i].getLibelle())%></td>
                            <td align=center><%=Utilitaire.champNull(listeP[i].getTypeCompte())%></td>

                            <td align=center><%=Utilitaire.champNull(listeP[i].getClasse())%></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>