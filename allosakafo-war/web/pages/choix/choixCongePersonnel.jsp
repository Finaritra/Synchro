<%@page import="mg.cnaps.paie.CongePersonnelLibelle"%>
<%@page import="mg.cnaps.accueil.Dossiers"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    CongePersonnelLibelle e = new CongePersonnelLibelle();
    e.setNomTable("CONGE_PERSONNEL_LIBELLE");
  //  e.setNomTable("sig_dossiers_lib");//sig_dossiers_travailleurs_lib
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "nom", "prenom", "libelle"   
    };
    
    
    
    String listeInt[] = {""};
    String libEntete[] = {"id", "nom", "prenom", "libelle" };
    
    
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choixCongePersonnel.jsp");
    pr.setChampReturn(champReturn);
    
    
//    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
//    pr.getFormu().getChamp("categorie_val").setLibelleAffiche("Cat�gorie");
//    pr.getFormu().getChamp("idconge_demande").setLibelleAffiche("Demande cong�");
//    pr.getFormu().getChamp("heure_depart").setLibelleAffiche("Heure de d�part");
//    pr.getFormu().getChamp("heure_retour").setLibelleAffiche("Heure de retour");
//    pr.getFormu().getChamp("duree_delairoute").setLibelleAffiche("Dur�e d�lairoute");
//    pr.getFormu().getChamp("lieu_de_jouissance").setLibelleAffiche("Lieu");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Conge Personnel</h1>
            </section>
            <section class="content">
                <form action="choixCongePersonnel.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=dossier/dossier-employeur.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "nom", "prenom", "libelle"    };
    
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>