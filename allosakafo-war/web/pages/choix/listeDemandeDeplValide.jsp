<%@page import="mg.cnaps.log.LogDemandeDepl"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    LogDemandeDepl livre = new LogDemandeDepl();
    livre.setNomTable("LOG_DEM_DPL_INFO");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "motif", "type_deplacement", "date_depart", "date_arrivee", "kilometrique_depart", "chauffeur"};
    String listeInt[] = {};
    String libEntete[] = {"id", "motif", "type_deplacement", "date_depart", "date_arrivee", "kilometrique_depart", "chauffeur"};
    PageRechercheChoix pr = new PageRechercheChoix(livre, request, listeCrt, listeInt, 4, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeDemandeDeplValide.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("type_deplacement").setLibelleAffiche("D�placement");
    pr.getFormu().getChamp("date_depart").setLibelleAffiche("Date de d�part");
    pr.getFormu().getChamp("date_arrivee").setLibelleAffiche("Date d'arriv�");
    pr.getFormu().getChamp("kilometrique_depart").setLibelleAffiche("Kilometrage de d�part");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);

%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Livre</h1>
            </section>
            <section class="content">
                <form action="bibltLivreChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="livre" id="livre">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"Id", "Motif", "D�placement", "Date de d�part", "Date d'arriv�", "Kilom�trage de d�part", "Chauffeur"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
