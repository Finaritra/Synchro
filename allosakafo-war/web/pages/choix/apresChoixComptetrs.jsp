<%-- 
    Document   : apresChoixComptetrs
    Created on : 31 mai 2016, 09:26:04
    Author     : Joe
--%>
<%@page import="utilitaire.Utilitaire"%>
<html>
    <%
        String champRet =(String) request.getParameter("champReturn");
        String choix =(String) request.getParameter("choix");
        String[] champs = Utilitaire.split(champRet, ";");
        String[] lstChoix = Utilitaire.split(choix, ";");
        String cible =(String) request.getParameter("cible");
    %>
    <%
     if(cible!=null && cible.compareToIgnoreCase("personnel")==0){
    %>
    <script language="JavaScript">
        window.opener.document.all.<%=champs[0]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[0])%>";
        window.opener.document.all.<%=champs[1]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[1])%>";
        window.opener.document.all.<%=champs[2]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[2])%>";
        window.opener.document.all.<%=champs[3]%>.value = "<%=utilitaire.Utilitaire.champNull(lstChoix[3])+" "+utilitaire.Utilitaire.champNull(lstChoix[4])%>";
    </script>
    <%
     } else{
    %>
    <script language="JavaScript">
        window.opener.document.all.<%=champs[0]%>.value = "<%=lstChoix[0]%>";
        window.opener.document.all.<%=champs[1]%>.value = "<%=lstChoix[1]%>";
        window.opener.document.all.<%=champs[2]%>.value = "<%=lstChoix[2]%>";
        window.opener.document.all.<%=champs[3]%>.value = "<%=lstChoix[3]%>";
        window.opener.document.all.<%=champs[4]%>.value = "<%=lstChoix[4]%>";
        window.opener.document.all.<%=champs[5]%>.value = "<%=lstChoix[5]%>";        
    </script>
    <%}%>
     <script language="JavaScript">
        window.close();        
    </script>
</html>