<%-- 
    Document   : apresChoixOr
    Created on : 15 juin 2016, 14:30:32
    Author     : Joe
--%>
<%@page import="utilitaire.Utilitaire"%>
<%@ page import="mg.cnaps.recette.Ordr"%>
<html>
    <%
        try {
            String iddr = request.getParameter("champReturn");
            String idor = request.getParameter("choix");
            
            Ordr ord = new Ordr();
            ord.setIdor(idor);
            ord.setIddr(iddr);
            int maxSeq = Utilitaire.getMaxSeq("GETSEQORDR");
            String nombre = Utilitaire.completerInt(6, maxSeq);
            String id = String.valueOf("ORDR") + String.valueOf(nombre);
            ord.setId(id);
            ord.insertToTable();
            
            %>
            <script language="JavaScript">
                confirm('Attachement termin� avec succees');
                window.close();
            </script>
            <%
        } catch (Exception ex) {
            ex.printStackTrace();
                        %>
            <script language="JavaScript">
                alert('<%=ex.getMessage()%>');
            </script>
            <%
        }
    %>
</html>