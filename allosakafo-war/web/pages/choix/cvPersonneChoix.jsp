<%-- 
    Document   : cvPersonneChoix
    Created on : 7 d�c. 2015, 16:36:01
    Author     : user
--%>
<%@page import="mg.cnaps.cv.CvPersonne"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    CvPersonne e = new CvPersonne();
    e.setNomTable("cv_personne_sans_cv");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "nom", "prenom", "date_naissance","numero_cin", "adresse", "telephone", "mail"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "nom", "prenom", "date_naissance","numero_cin", "adresse", "telephone", "mail"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("cvPersonneChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("numero_cin").setLibelleAffiche("Numero CIN");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des personnes </h1>
            </section>
            <section class="content">
                <form action="cvPersonneChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/cv/personnecv-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Nom", "Prenom", "Date de naissance","Numero CIN", "Adresse", "Telephone", "Mail"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>