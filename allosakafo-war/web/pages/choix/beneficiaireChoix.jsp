<%-- 
    Document   : beneficiaireChoix
    Created on : 14 sept. 2015, 17:35:26
    Author     : Tafitasoa
--%>
<%@page import="mg.cnaps.accueil.Sig_beneficiaire"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    Sig_beneficiaire e = new Sig_beneficiaire();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "matricule", "type","nom","cin"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "matricule", "type","nom","cin"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("beneficiaireChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("matricule").setLibelleAffiche("Matricule");
    pr.getFormu().getChamp("type").setLibelleAffiche("Type beneficiaire");
    pr.getFormu().getChamp("nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("cin").setLibelleAffiche("CIN");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste bénéficiaire</h1>
            </section>
            <section class="content">
                <form action="beneficiaireChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="benef" id="benef">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "matricule", "type","nom","cin"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>