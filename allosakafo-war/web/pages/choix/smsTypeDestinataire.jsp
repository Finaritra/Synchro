<%@page import="mg.cnaps.sms.SmsTypeDestinataire"%>
<%@page import="mg.cnaps.sms.SmsModele"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    SmsTypeDestinataire mod = new SmsTypeDestinataire();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "type_dest", "libelle"};
    String listeInt[] = null;
    String libEntete[] =  {"id", "type_dest", "libelle"};
    PageRechercheChoix pr = new PageRechercheChoix(mod, request, listeCrt, listeInt, 2, libEntete, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sms/smsTypeDestinataire.jsp");
    pr.getFormu().getChamp("type_dest").setLibelle("Type destinataire");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    SmsTypeDestinataire[] liste = (SmsTypeDestinataire[])pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Type Destinataire</h1>
            </section>
            <section class="content">
                <form action="smsTypeDestinataire.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="mod" id="mod">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th>#</th>
                            <th>Id</th>
                            <th>Type destinataire</th>
                            <th>Libelle</th>

                        </tr>
                        <%
                        for (int i = 0; i < liste.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=liste[i].getId()%>;<%=liste[i].getLibelle() %>" class="radio" /></td>
                            <td align=center><%=liste[i].getId()%></td>
                            <td align=center><%=liste[i].getType_dest() %></td>
                            <td align=center><%=liste[i].getLibelle() %></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>