<%@page import="mg.cnaps.accident.SigAtmpMedecin"%>
<%@page import="mg.cnaps.st.StTiers"%>
<%@page import="mg.cnaps.st.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    SigAtmpMedecin e = new SigAtmpMedecin();
    e.setNomTable("SIG_ATMP_MEDECIN");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "nom", "prenom","date_naissance", "numero_telephone", "email","numero_onm"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "nom", "prenom","date_naissance", "numero_telephone", "email","numero_onm"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
   // pr.setAWhere(" AND typetiers = 'TRS000001'");
    pr.setApres("sigAtmpMedecinChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("date_naissance").setLibelleAffiche("Naissance");
    pr.getFormu().getChamp("numero_telephone").setLibelleAffiche("T�l�phone");
    pr.getFormu().getChamp("numero_onm").setLibelleAffiche("ONM");
    pr.getFormu().getChamp("email").setLibelleAffiche("�mail");
    
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>


<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste M�decin</h1>
            </section>
            <section class="content">
                <form action="sigAtmpMedecinChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    
                    String libelles[] =  {"id", "nom", "prenom","Naissance", "T�l�phone", "�mail","ONM"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>