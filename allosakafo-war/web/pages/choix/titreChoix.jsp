<%@page import="mg.cnaps.treso.TresoTitre"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    TresoTitre t = new TresoTitre();
    String listeCrt[] = {"id", "numerotation", "daty", "remarque", "montant"};
    String listeInt[] = {"daty", "montant"};
    String libEntete[] = {"id", "numerotation", "daty", "remarque", "montant"};
    PageRechercheChoix pr = new PageRechercheChoix(t, request, listeCrt, listeInt,3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("titreChoix.jsp");
    
    pr.getFormu().getChamp("id").setLibelleAffiche("Id");
    pr.getFormu().getChamp("numerotation").setLibelleAffiche("Numérotation");
    pr.getFormu().getChamp("daty1").setLibelleAffiche("Date min");
    pr.getFormu().getChamp("daty2").setLibelleAffiche("Date max");
    pr.getFormu().getChamp("remarque").setLibelleAffiche("Remarque");
    pr.getFormu().getChamp("montant1").setLibelleAffiche("Montant min");
    pr.getFormu().getChamp("montant2").setLibelleAffiche("Montant max");
    
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
    TresoTitre[] listeD = (TresoTitre[]) pr.getRs().getResultat();
%>
  <!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste titre</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="titre" id="titre">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=tresorerie/titre/titre-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[]={"id", "Numérotation", "Date", "Remarque", "Montant"};
		    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
