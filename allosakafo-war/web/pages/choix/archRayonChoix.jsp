<%-- 
    Document   : archRayonChoix
    Created on : 29 d�c. 2015, 14:55:13
    Author     : user
--%>
<%@page import="mg.cnaps.archive.ArchRayons"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    ArchRayons e = new ArchRayons();
    e.setNomTable("ARCH_RAYONS_LIBELLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "idsalle", "libelle", "position"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "idsalle", "libelle", "position"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("archRayonChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("idsalle").setLibelleAffiche("Salle");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des rayons</h1>
            </section>
            <section class="content">
                <form action="archRayonChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="rayon" id="rayon">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=archive/rayon-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] =  {"Id", "Salle", "Libelle", "Position"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>