
<%@page import="mg.cnaps.pointage.PointAutorisation"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>


<%
    PointAutorisation e = new PointAutorisation();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "id_personnel", "date_debut", "date_fin"};
    String listeInt[] = null;
    String libEntete[]= {"id", "id_personnel", "date_debut", "date_fin"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("pointautorisationchoix.jsp");
    pr.setAWhere(" AND IS_DEDUIRE_CONGE = 'OUI'");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("id_personnel").setLibelleAffiche("Personnel");
    pr.getFormu().getChamp("date_debut").setLibelleAffiche("Date d&eacute;but");
    pr.getFormu().getChamp("date_fin").setLibelleAffiche("Date fin");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    PointAutorisation[] listeP = (PointAutorisation[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste autorisation</h1>
            </section>
            <section class="content">
                <form action="pointautorisationchoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="autorisation" id="autorisation">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"Id","Personnel", "Date &eacute;but", "Date fin"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixAutorisation.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                <th>#</th>
                                <th>Id</th>
                                <th>Personnel</th>
                                <th>Date d&eacute;but</th>
                                <th>Date fin</th>
                            </tr>
                            <%
                                for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getId_personnel()%>" class="radio" /></td>
                                    
                                <td align=center><%=listeP[i].getId()%></td>
                                <td align=center><%=listeP[i].getId_personnel()%></td>
                                <td align=center><%=listeP[i].getDate_debut()%></td>
                                <td align=center><%=listeP[i].getDate_fin()%></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

