
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.sig.SigActivites"%>
<% 
    SigActivites e = new SigActivites();
    String nomTable = request.getParameter("nomTable");
    e.setNomTable(nomTable);
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id","activite_libelle","ventil_code", "code"};
    String listeInt[] = {""};
    String libEntete[] = {"id","activite_libelle","ventil_code", "code"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete,4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sigActiviteChoix2.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("activite_libelle").setLibelleAffiche("activit&eacute;");
    pr.getFormu().getChamp("ventil_code").setLibelleAffiche("Code ventil");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    SigActivites[] listeP = (SigActivites[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste agences</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="agencecode" id="agencecode">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
		    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixCodeAgence.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>id</th>
                                    <th>activit&eacute;</th>
                                    <th>Code ventil</th>
				    <th>Code</th>
                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getActivite_libelle()%>" class="radio" /></td>
                                    <td align=center><%=listeP[i].getId()%></td>
                                    <td align=center><%=listeP[i].getActivite_libelle()%></td>
                                    <td align=center><%=listeP[i].getVentil_code()%></td>
				    <td align=center><%=listeP[i].getCode()%></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>