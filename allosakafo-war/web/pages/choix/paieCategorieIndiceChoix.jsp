<%@page import="mg.cnaps.paie.PaieCategorieIndiceDetails"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.commun.*" %>


<%
    String champReturn = request.getParameter("champReturn");
    PaieCategorieIndiceDetails e = new PaieCategorieIndiceDetails();
    e.setNomTable("PAIECATEGORIEINDICE_DETAILS");
    String listeCrt[] = {"id","categorie","categorieval", "indice"};
    String listeInt[] = {""};
    String libEntete[] = {"id","categorie","categorieval", "indice"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("paieCategorieIndiceChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("Identifiant");
    pr.getFormu().getChamp("categorie").setLibelleAffiche("Numero cat&eacute;gorie");
    pr.getFormu().getChamp("categorieval").setLibelleAffiche("Cat&eacute;gorie");
    pr.getFormu().getChamp("indice").setLibelleAffiche("Indice");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste cat&eacute;gorie-indice</h1>
            </section>
            <section class="content">
                <form action="paieCategorieIndiceChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[]={"Id","Numero cat&eacute;gorie","Cat&eacute;gorie", "Indice"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>