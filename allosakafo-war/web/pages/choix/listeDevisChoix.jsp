<%--
    Document   : listeServiceChoix
    Created on : 20 janv. 2016, 17:34:58
    Author     : ITU
--%>


<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.immo.ImmoErDevis"%>

<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.immo.ImmoErDevis"%>
<%

    String champReturn = request.getParameter("champReturn");
    ImmoErDevis e = new ImmoErDevis();
    e.setNomTable("immo_er_devis_libelle2");
    String listeCrt[] = {"id", "id_nature", "id_immo", "daty"};

    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "id_nature", "id_immo", "daty"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 4, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeDevisChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Nature immo</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[]= {"id", "Nature", "Immo", "Date"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixService.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
