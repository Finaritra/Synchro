<%@page import="mg.cnaps.cie.RecRelance"%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.sig.SigActivites"%>

<%
    String champReturn = request.getParameter("champReturn");
    
    RecRelance e = new RecRelance();
    String listeCrt[] = {"id","employeur","iddn", "date_relance","remarque","montant"};
    String listeInt[] = {"date_relance","montant"};
    String libEntete[] = {"id","employeur","iddn", "date_relance","remarque","montant"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("recRelanceChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("iddn").setLibelleAffiche("Id DN");
    pr.getFormu().getChamp("date_relance1").setLibelleAffiche("Date Relance min");
    pr.getFormu().getChamp("date_relance2").setLibelleAffiche("Date Relance max");
    pr.getFormu().getChamp("montant1").setLibelleAffiche("Montant min");
    pr.getFormu().getChamp("montant2").setLibelleAffiche("Montant max");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Relance</h1>
            </section>
            <section class="content">
                <form action="recRelanceChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="relanceliste" id="relanceliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[]={"Id","Employeur","Id DN", "Date relance","remarque","montant"};
		    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>