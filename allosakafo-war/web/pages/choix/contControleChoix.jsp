<%-- 
    Document   : contControleChoix
    Created on : 8 d�c. 2015, 10:27:38
    Author     : user
--%>

<%@page import="mg.cnaps.controle.ContControle"%>
<%@page import="mg.cnaps.controle.ContRendezvous"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    ContControle e = new ContControle();
    e.setNomTable("CONT_CONTROLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "idom", "datecontrole", "entitecontrole", "nomentreprise", "activiteentreprise", "personnerencontre", "fonctionpersrencontre"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "idom", "datecontrole", "entitecontrole", "nomentreprise", "activiteentreprise", "personnerencontre", "fonctionpersrencontre"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("contControleChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("idom").setLibelleAffiche("Ordre de mission");
    pr.getFormu().getChamp("datecontrole").setLibelleAffiche("Date de controle");
    pr.getFormu().getChamp("entitecontrole").setLibelleAffiche("Entite controle");
    pr.getFormu().getChamp("nomentreprise").setLibelleAffiche("Nom entreprise");
    pr.getFormu().getChamp("activiteentreprise").setLibelleAffiche("Activit� entreprise");
    pr.getFormu().getChamp("personnerencontre").setLibelleAffiche("Personne rencontr�");
    pr.getFormu().getChamp("fonctionpersrencontre").setLibelleAffiche("Fonction de personne rencontr�");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des controles</h1>
            </section>
            <section class="content">
                <form action="contControleChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="controle" id="controle">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=controle/consultation/controle-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] ={"ID", "Ordre de mission", "Date de controle", "Entite controle", "Nom de l'entreprise", "Activite de l'entreprise", "Personne rencontr�", "Fonction de la personne rencontr�e"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>