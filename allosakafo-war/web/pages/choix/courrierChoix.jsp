<%-- 
    Document   : courrierChoix
    Created on : 17 nov. 2015, 19:12:24
    Author     : user
--%>
<%@page import="mg.cnaps.courier.CourierContenuCourier"%>
<%@page import="mg.cnaps.accueil.Dossiers"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    CourierContenuCourier e = new CourierContenuCourier();
    e.setNomTable("COUR_CONTENU_COURIER"); //si_dossiers_travailleurs_lib
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "iddossier", "idlog_service", "contenu"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "iddossier", "idlog_service", "contenu"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("courrierChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("iddossier").setLibelleAffiche("Dossier");
    pr.getFormu().getChamp("idlog_service").setLibelleAffiche("Service");
    pr.getFormu().getChamp("contenu").setLibelleAffiche("Contenu");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Courrier</h1>
            </section>
            <section class="content">
                <form action="courrierChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="courrier" id="courrier">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/courrier/courrier-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Dossier", "Service", "Contenu"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>