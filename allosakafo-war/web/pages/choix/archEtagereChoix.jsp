<%-- 
    Document   : archEtagereChoix
    Created on : 29 d�c. 2015, 14:16:37
    Author     : user
--%>
<%@page import="mg.cnaps.archive.ArchEtagere"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    ArchEtagere e = new ArchEtagere();
    e.setNomTable("ARCH_ETAGERE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"ID", "NUMERO", "DESCRIPTION", "IDSALLE"};
    String listeInt[] = {""};
    String libEntete[] = {"ID", "NUMERO", "DESCRIPTION", "IDSALLE"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("archEtagereChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("ID").setLibelleAffiche("ID");
    pr.getFormu().getChamp("IDSALLE").setLibelleAffiche("Salle");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des �tag�res </h1>
            </section>
            <section class="content">
                <form action="archEtagereChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] =  {"id", "numero", "description", "Salle"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>