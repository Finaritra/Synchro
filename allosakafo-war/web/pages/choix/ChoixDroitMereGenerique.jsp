<%-- 
    Document   : ChoixDroitMereGenerique
    Created on : 12 juil. 2016, 09:51:14
    Author     : hp
--%>

<%@page import="mg.cnaps.pension.DroitMere"%>
<%@page import="java.util.concurrent.ConcurrentHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="mg.cnaps.budget.BudgetDepenseLibelle"%>
<%@page import="mg.cnaps.budget.BudgetDepense"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    String champReturn = request.getParameter("champReturn");
    String [] champReturnList = champReturn.split(";");
    
    
    String table = null;
    boolean tableExistPas = champReturnList.length < 3 ;
    if (tableExistPas)
    {
        table = "DROIT_MERE";
    }else
    {
        table = champReturnList [2];
    }
    
    
    Map<String, String> nomVue = new ConcurrentHashMap<String, String>();
    nomVue.put("DROIT_MERE", "DROIT_MERE_LIBELLE");
    nomVue.put("DROIT_MERE_AT", "DROIT_MERE_AT_LIBELLE");
    nomVue.put("DROIT_MERE_AF", "DROIT_MERE_AF_LIBELLE");
    
    Map<String, String> titre = new ConcurrentHashMap<String, String>();
    titre.put("DROIT_MERE", "liste droit m�re");
    titre.put("DROIT_MERE_AT", "liste droit m�re AT");
    titre.put("DROIT_MERE_AF", "liste droit m�re AF");
    
    Map<String, String> seq = new ConcurrentHashMap<String, String>();
    seq.put("DROIT_MERE", "getseqdroitmere");
    seq.put("DROIT_MERE_AT", "getseqdroitmereat");
    seq.put("DROIT_MERE_AF", "getseqdroitmereaf");
    
    Map<String, String> nomTableDetails = new ConcurrentHashMap<String, String>();
    nomTableDetails.put("DROIT_MERE", "droit_details");
    nomTableDetails.put("DROIT_MERE_AT", "droit_details_at");
    nomTableDetails.put("DROIT_MERE_AF", "droit_details_af");
  
    Map<String, String> titreDetail = new ConcurrentHashMap<String, String>();
    titreDetail.put("DROIT_MERE", "liste droit m�re d�tail");
    titreDetail.put("DROIT_MERE_AT", "liste droit m�re d�tail at");
    titreDetail.put("DROIT_MERE_AF", "liste droit m�re d�tail af");
   
    
    
    
    DroitMere e = new DroitMere();
    e.setNomTable(nomVue.get(table));
    String listeCrt[] = {"id", "idtrav", "pension_num","dr","periode","echeance","type_pension","mois","annee","montant_debit","montant_credit"};
    String listeInt[] = {"montant_debit","montant_credit","mois"};
    String libEntete[] = {"id", "idtrav", "pension_num","dr","periode","echeance","type_pension","mois","annee","montant_debit","montant_credit"};
    
    
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("ChoixDroitMereGenerique.jsp");
    pr.setChampReturn(champReturn);
   
    
    
    pr.getFormu().getChamp("idtrav").setLibelle("Travailleur");    
    pr.getFormu().getChamp("type_pension").setLibelle("Type Pension");    
    affichage.Champ[] liste = new affichage.Champ[2];
    liste[0] = new Liste("mois1");
    ((Liste) (liste[0])).makeListeMois();
    liste[1] = new Liste("mois2");
    ((Liste) (liste[1])).makeListeMois();
    pr.getFormu().changerEnChamp(liste);
    pr.getFormu().getChamp("mois1").setLibelle("Mois min");
  
    pr.getFormu().getChamp("mois2").setLibelle("Mois max");
    pr.getFormu().getChamp("pension_num").setLibelle("N� Pension");
    pr.getFormu().getChamp("montant_debit1").setLibelle("debit min");
    pr.getFormu().getChamp("montant_debit2").setLibelle("debit max");
    pr.getFormu().getChamp("montant_credit1").setLibelle("cr&eacute;dit min");
    pr.getFormu().getChamp("montant_credit2").setLibelle("cr&eacute;dit max");
    pr.getFormu().getChamp("echeance").setLibelle("Ech�ance");
    pr.getFormu().getChamp("dr").setLibelle("DR");
   
    
    String[] colSomme = {"montant_debit","montant_credit"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1> <%= titre.get(table) %></h1>
            </section>
            <section class="content">
                <form action="ChoixDroitMereGenerique.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bdDep" id="bdDep">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=pension/droitmere-fiche.jsp&table=" + table};
                    String colonneLien[] = {"id"};
                    String libelles[] =  {"id", "Travailleur", "Pension N�", "dr","Periode","Echeance","Type pension","Mois","Ann�e","Montant d�bit","Montant Cr�dit"};
           
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresTarifChoixDroiMereGenerique.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>