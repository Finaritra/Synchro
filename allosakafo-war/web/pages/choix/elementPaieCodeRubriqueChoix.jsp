<%@page import="mg.cnaps.paie.PaieRubrique"%>
<%@page import="mg.cnaps.commun.Sig_commune"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    PaieRubrique e = new PaieRubrique();
    e.setNomTable("paie_rubrique");
    String listeCrt[] = {"id", "code", "desce"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "code", "desce"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("elementPaieCodeRubriqueChoix.jsp");

    pr.getFormu().getChamp("id").setLibelleAffiche("Identifiant");
    pr.getFormu().getChamp("code").setLibelleAffiche("Code");
    pr.getFormu().getChamp("desce").setLibelleAffiche("Description");

    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    PaieRubrique[] listeP = (PaieRubrique[]) pr.getRs().getResultat();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des &eacute;l&eacute;ments de paies</h1>
            </section>
            <section class="content">
                <form action="elementPaieCodeRubriqueChoix.jsp?champReturn=<%=request.getParameter("champReturn")%>" method="post" name="elementpaiechoix" id="elementpaiechoix">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/element/rubrique-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixCoderRubrique.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=request.getParameter("champReturn")%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th>#</th>
                            <th>ID</th>
                            <th>Code</th>
                            <th>D&eacute;scription</th>
                            <th>Remarque</th>

                        </tr>
                        <%
                            for (int i = 0; i < listeP.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<% String val = listeP[i].getFormule() + ";" + listeP[i].getTyperubrique() + ";" + listeP[i].getId();
                                                        out.print(val);%>" class="radio" /></td>
                            <td align=center><%=listeP[i].getId()%></td>
                            <td align=center><%=listeP[i].getCode()%></td>
                            <td align=center><%=listeP[i].getDesce()%></td>
                            <td align=center><%=listeP[i].getRang()%></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <%
                    out.println(pr.getBasPage());
                %>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>