<%-- 
    Document   :sigDossiersalonChoix
    Created on : 18 nov. 2015, 13:35:44
    Author     : Safidimahefa Romario
--%>


<%@page import="mg.cnaps.sig.SigDossierEmployeursInfo"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    SigDossierEmployeursInfo d = new SigDossierEmployeursInfo();
    String champReturn = request.getParameter("champReturn");
    champReturn ="sig_dossiers_employeurs;idemployeur";
    String listeCrt[] = {"id", "id_deposant","deposant", "dossier_matricule", "sous_prestation_code", "dossier_num_bordereau"};
    String listeInt[] = {""};
    String libEntete[] =  {"id", "id_deposant","deposant", "dossier_matricule", "sous_prestation_code", "dossier_num_bordereau"};
    PageRechercheChoix pr = new PageRechercheChoix(d, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sigDossiersEmployeursChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("dossier_matricule").setLibelleAffiche("Dossier Matricule");
    pr.getFormu().getChamp("id_deposant").setLibelleAffiche("Id Employeur");
     pr.getFormu().getChamp("deposant").setLibelleAffiche("Deposant");
    pr.getFormu().getChamp("sous_prestation_code").setLibelleAffiche("Sous prestation code");
    pr.getFormu().getChamp("dossier_num_bordereau").setLibelleAffiche("Num Borderau");

    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    SigDossierEmployeursInfo[] listeP = (SigDossierEmployeursInfo[]) pr.getRs().getResultat();
    
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Dossier</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="magasin" id="magasin">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=dossier/dossier-liste.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"ID",  "Dossier Matricule","ID Employeur","Employeur", "Sous prestation code", "N� Borderau"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixCodeAgence.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>id</th>
                                    <th>Dossier Matricule</th>
                                    <th>ID Employeur</th>
                                    <th>Employeur</th>
                                    <th>Sous prestation code</th>
                                    <th>N� Borderau</th>

                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getId_deposant()%>" class="radio" /></td>
                                    <td align=center><%=listeP[i].getId()%></td>
                                    <td align=center><%=listeP[i].getDossier_matricule() %></td>
                                    <td align=center><%=listeP[i].getId_deposant() %></td>
                                    <td align=center><%=listeP[i].getDeposant() %></td>
                                    <td align=center><%=listeP[i].getSous_prestation_code()%></td>
                                    <td align=center><%=listeP[i].getDossier_num_bordereau() %></td>
                                    
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
