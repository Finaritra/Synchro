<%--
    Document   : bibltLivreChoix
    Created on : 7 d�c. 2015, 14:42:44
    Author     : user
--%>

<%@page import="mg.cnaps.bibliotheque.BibltLivreInfo"%>
<%@page import="mg.cnaps.bibliotheque.BibltLivre"%>
<%@page import="mg.cnaps.budget.BudgetActivites"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    BibltLivreInfo livre = new BibltLivreInfo();
    livre.setNomTable("BIBLT_LIVRE_DISPO_INFO");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "id_domaine", "titre", "date_edition", "quantite", "code"};
    String listeInt[] = {"date_edition"};
    String libEntete[] = {"id", "id_domaine", "titre" , "date_edition", "quantite","qtrestante", "code"};
    PageRechercheChoix pr = new PageRechercheChoix(livre, request, listeCrt, listeInt, 2, libEntete, 7);
    //pr.setAWhere(" AND QTRESTANTE > 0 ");
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("bibltLivreChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("id_domaine").setLibelleAffiche("Theme");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);

%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Livre</h1>
            </section>
            <section class="content">
                <form action="bibltLivreChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="livre" id="livre">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/bibliotheque/livre-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Theme", "Titre", "Date d'&eacute;dition", "Quantit&eacute;","Quantit&eacute; disponible", "Code"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
