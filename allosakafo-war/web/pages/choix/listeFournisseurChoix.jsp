<%-- 
    Document   : listeClientChoix.jsp
    Created on : 2 d�c. 2016, 13:46:50
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@page import="mg.allosakafo.facture.Client"%>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>

<%
    String champReturn = request.getParameter("champReturn");
    Client e = new Client();
    e.setNomTable("fournisseurProduits");
    String listeCrt[] = {"id","nom", "adresse", "telephone"};
    String listeInt[] = null;
    String libEntete[] = {"id","nom", "adresse", "telephone"};
    System.out.println("-------------"+e.getNomTable());
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeFournisseurChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    
    Client[] listeP = (Client[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Allo Sakafo 1.0</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste fournisseur</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libEnteteAffiche[] = {"id","nom", "adresse", "telephone"};
                    pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                        <table width=100% border=0 align=center cellpadding=3 cellspacing=3 class=monographe>
                            <tr class=head>
                                <td align="center" valign="top">#</td>
                                <td align="center" valign="top">ID</td>
                                <td align="center" valign="top">nom</td>
                                <td align="center" valign="top">adresse</td>
                                <td align="center" valign="top">telephone</td>
                            </tr>

                            <% //out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                            <%
                            for (int i = 0; i < listeP.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()+";"+listeP[i].getNom() %>" class="radio" /></td>
                            <td align=center><%=listeP[i].getId()%></td>
                            <td align=center><%=listeP[i].getNom()%></td>
                            <td align=center><%=listeP[i].getAdresse()%></td>
                            <td align=center><%=listeP[i].getTelephone()%></td>
                        </tr>
                <%}%>
                 </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>