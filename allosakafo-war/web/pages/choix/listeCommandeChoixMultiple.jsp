<%-- 
    Document   : choixFactureMultiple
    Created on : 9 juin 2016, 20:03:54
    Author     : Joe
--%>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="user.UserEJB"%>
<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    
    CommandeClient e = new CommandeClient();
    e.setNomTable("as_commandeclient_libelle");
    String listeCrt[] = {"datecommande", "client", "responsable", "typecommande"};
    String listeInt[] = {"datecommande"};
    String libEntete[] = {"id", "datecommande", "client", "typecommande", "responsable", "remarque"};

    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setChampReturn(request.getParameter("champReturn"));
    pr.setApres("listeCommandeChoixMultiple.jsp");
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
%>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Pho 1.0</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste facture</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="cc" id="cc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%

                    String libEnteteAffiche[] = {"Id", "Date Commande", "Client", "Type Commande", "Responsable", "Remarque"};
                    pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <%=pr.getTableau().getHtmlWithMultipleCheckbox()%>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
