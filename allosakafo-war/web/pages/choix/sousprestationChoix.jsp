<%-- 
    Document   : sousprestationChoix
    Created on : 21 sept. 2015, 16:04:35
    Author     : Jetta
--%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.accueil.SousPrestations"%>
<%

    SousPrestations e = new SousPrestations();
    e.setNomTable("SIG_SOUS_PRESTATIONS_INFO");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "sous_prestation_libelle", "prestation_code","sous_prestation_abreviation","code_imputation","compte_a_debit","compte_a_credit"};
    String listeInt[] = null;
    String libEntete[] = {"id", "sous_prestation_libelle", "prestation_code","sous_prestation_abreviation","code_imputation","compte_a_debit","compte_a_credit"};
   PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sousprestationChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("sous_prestation_libelle").setLibelleAffiche("Sous Presatation libelle");
    pr.getFormu().getChamp("code_imputation").setLibelleAffiche("Imputation");
   pr.getFormu().getChamp("sous_prestation_abreviation").setLibelleAffiche("Abreviation");
   pr.getFormu().getChamp("prestation_code").setLibelleAffiche("Prestation");
   pr.getFormu().getChamp("compte_a_debit").setLibelleAffiche("Compte a debit");
   pr.getFormu().getChamp("compte_a_credit").setLibelleAffiche("Compte a credit");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste soupresatation</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="magasin" id="magasin">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=stock/magasin/magasin-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"id", "Libelle", "Prestation","Abreviation","Imputation","compte a Debit","Compte a Credit"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>