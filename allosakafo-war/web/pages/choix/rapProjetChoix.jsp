<%-- 
    Document   : rapProjetChoix
    Created on : 7 d�c. 2015, 22:00:18
    Author     : user
--%>
<%@page import="mg.cnaps.rapport.RapProjetLibelle"%>
<%@page import="mg.cnaps.rapport.RapProjet"%>
<%@page import="mg.cnaps.medecine.MedMedecin"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    RapProjetLibelle e = new RapProjetLibelle();
    e.setNomTable("RAP_PROJET_LIBELLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "date_debut", "date_fin", "idbudgetprojet", "volume_horaire", "realisation_jour"};
    String listeInt[] = {"date_debut", "date_fin"};
    String libEntete[] = {"id",  "idbudgetprojet", "date_debut", "date_fin", "volume_horaire", "realisation_jour","id_activite","intitule_activite"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("rapProjetChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");

    pr.getFormu().getChamp("idbudgetprojet").setLibelleAffiche("projet");
    pr.getFormu().getChamp("Volume_horaire").setLibelleAffiche("Volume horaire");
    pr.getFormu().getChamp("realisation_jour").setLibelleAffiche("Nombre de jour de realisation");
    pr.getFormu().getChamp("date_debut1").setLibelleAffiche("Date debut min");
    pr.getFormu().getChamp("date_debut2").setLibelleAffiche("date debut max");
    pr.getFormu().getChamp("date_fin1").setLibelleAffiche("date fin min ");
    pr.getFormu().getChamp("date_fin2").setLibelleAffiche("Date fin max");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    RapProjetLibelle[] liste = (RapProjetLibelle[])pr.getListe();
    
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Projet</h1>
            </section>
            <section class="content">
                <form action="rapProjetChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="medecin" id="medecin">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=cms/configuration/medecin-fiche.jsp"};
                    String colonneLien[] = {"id"};
                   // String libelles[] = {"Id", "Budget projet", "Date debut", "Date fin", "Volume horaire", "Nombre de jour de r�alisation","Intitul� Activit�"};
                    //pr.getTableau().setLien(lienTableau);
                    //pr.getTableau().setColonneLien(colonneLien);
                   // pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                 <form action="../<%=pr.getLien()%>?but=choix/apresChoixCodeAgence.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                <th>#</th>
                                    <th>ID</th>
                                    <th>Budget Projet</th>
                                    <th>Date D�but</th>
                                    <th>Date Fin</th>
                                    <th>Nombre de jour de R�alisation</th>
                                    <th>Intitul� Activit�</th>

                            </tr>
                            <%
                                    for (int i = 0; i < liste.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=liste[i].getId()%>;<%=liste[i].getId_activite()%>" class="radio" /></td>
                                    <td align=center><%=liste[i].getId()%></td>
                                    <td align=center><%=liste[i].getIdbudgetprojet()%></td>
                                    <td align=center><%=liste[i].getDate_debut()%></td>
                                    <td align=center><%=liste[i].getDate_fin()%></td>
                                     <td align=center><%=liste[i].getRealisation_jour()%></td>
                                      <td align=center><%=liste[i].getIntitule_activite()%></td>
                                    
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>