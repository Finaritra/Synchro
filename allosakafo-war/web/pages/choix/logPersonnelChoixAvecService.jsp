<%-- 
    Document   : logPersonnelChoixAvecService
    Created on : 10 mai 2016, 15:18:35
    Author     : ITU
--%>

<%@page import="mg.cnaps.immo.ListeChoixPersonnelAvecService"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    ListeChoixPersonnelAvecService e = new ListeChoixPersonnelAvecService();
    e.setNomTable("LOG_PERSONNEL_LISTE_CHOIX");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "nom_et_prenom","id_service","log_service", "id_direction", "log_direction"};
    String listeInt[] = null;
    String libEntete[] = {"id", "nom_et_prenom","id_service","log_service", "id_direction", "log_direction"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("logPersonnelChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("nom_et_prenom").setLibelleAffiche("Nom et pr�nom");
    pr.getFormu().getChamp("id_direction").setLibelleAffiche("Id direction");
    pr.getFormu().getChamp("lib_direction").setLibelleAffiche("Direction");
    pr.getFormu().getChamp("id_service").setLibelleAffiche("Id service");
    pr.getFormu().getChamp("lib_service").setLibelleAffiche("Service");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des personnels</h1>
            </section>
            <section class="content">
                <form action="logPersonnelChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="chauffeur" id="chauffeur">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "nom_et_prenom","id_service","log_service", "id_direction", "log_direction"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>"/>
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>