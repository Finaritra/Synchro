<%-- 
    Document   : courEnvoiChoix
    Created on : 27 nov. 2015, 11:04:23
    Author     : user
--%>

<%@page import="mg.cnaps.courier.CourEnvoiRecept"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    CourEnvoiRecept e = new CourEnvoiRecept();
//    e.setNomTable("cie_med_libelle"); 
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id","prestataire","type", "pays", "daty"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "daty", "heure", "prestataire", "tarif", "reception", "type", "pays"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("courEnvoiChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("daty1").setLibelleAffiche("Date min");
    pr.getFormu().getChamp("daty2").setLibelleAffiche("Date max");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Envoie - Recept</h1>
            </section>
            <section class="content">
                <form action="courEnvoiChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="envoi" id="envoi">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=cie/med-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"Id", "Date", "Heure", "Prestataire", "Tarif", "Reception", "Type", "Pays"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>