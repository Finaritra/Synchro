<%--
    Document   : listeVehiculeChoix
    Created on : 8 sept. 2015, 15:04:58
    Author     : user
--%>

<%@page import="mg.cnaps.log.LogCarteCarburant"%>
<%@page import="mg.cnaps.log.LogVehicule"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet listeTypeCarte = new TypeObjet();
    listeTypeCarte.setNomTable("LOG_TYPE_CARTE");
    liste[0] = new Liste("idtype_carte", listeTypeCarte, "val", "val");

    LogCarteCarburant e = new LogCarteCarburant();
    e.setNomTable("LOG_CARTE_CARBURANT_SOLDE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "numero", "montant", "date_expiration", "iddetenteur", "idtype_carte"};
    String listeInt[] = {"date_expiration"};
    String libEntete[] = {"id", "numero", "montant", "date_expiration", "iddetenteur", "idtype_carte"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 6);
    pr.getFormu().changerEnChamp(liste);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("carteChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelle("Id");
    pr.getFormu().getChamp("numero").setLibelle("Numero");
    pr.getFormu().getChamp("montant").setLibelle("Montant");
    pr.getFormu().getChamp("date_expiration1").setLibelle("Date d'expiration min");
    pr.getFormu().getChamp("date_expiration2").setLibelle("Date d'expiration max");
    pr.getFormu().getChamp("iddetenteur").setLibelle("Détenteur");
    pr.getFormu().getChamp("idtype_carte").setLibelle("Type carte");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste vehicule</h1>
            </section>
            <section class="content">
                <form action="carteChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="vehicule" id="vehicule">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
		    String lienTableau[] = {pr.getLien() + "?but=materiel_roulant/vehicule/carburant-saisie.jsp"};
		    String colonneLien[] = {"id"};
		    String libelles[] = {"Id", "Numero", "Solde Actuel", "Date d'expiration", "Détenteur", "Type Carte"};
		    pr.getTableau().setLien(lienTableau);
		    pr.getTableau().setColonneLien(colonneLien);
		    pr.getTableau().setLibelleAffiche(libelles);
		    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>