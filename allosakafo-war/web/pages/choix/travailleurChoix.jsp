<%-- 
    Document   : travailleurChoix
    Created on : 16 mars 2016, 09:41:59
    Author     : Tafitasoa
--%>

<%@page import="mg.cnaps.sig.Sig_travailleurs_info_complet"%>
<%@page import="mg.cnaps.commun.Sig_commune"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>


<%
    String champReturn = request.getParameter("champReturn");
    Sig_travailleurs_info_complet e = new Sig_travailleurs_info_complet();
    e.setNomTable("SIG_TRAVAILLEUR_INFO_COMPLET");
    String listeCrt[] = {"id_travailleurs","travailleur_matricule","pers_nom","pers_prenom","employeur_matricule","pers_numero_cin","pers_nom_mere"};
    String listeInt[] = {""};
    String libEntete[] = {"id_travailleurs","travailleur_matricule","pers_nom","pers_prenom","employeur_matricule","pers_numero_cin","pers_nom_mere"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setChampReturn(champReturn);
    pr.setApres("travailleurChoix.jsp");
    pr.getFormu().getChamp("id_travailleurs").setLibelleAffiche("ID");
    pr.getFormu().getChamp("travailleur_matricule").setLibelleAffiche("Matricule");
    pr.getFormu().getChamp("pers_nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("pers_prenom").setLibelleAffiche("Prenom");
    pr.getFormu().getChamp("employeur_matricule").setLibelleAffiche("ID Employeur");
    pr.getFormu().getChamp("pers_numero_cin").setLibelleAffiche("CIN");
    pr.getFormu().getChamp("pers_nom_mere").setLibelleAffiche("Nom de la mere");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    Sig_travailleurs_info_complet[] trv = (Sig_travailleurs_info_complet[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste travailleurs</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=employeur/employeur-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[]={"Id","Matricule","Nom","Prenom","Id Employeur","Code Direction Regional"};
		    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
<form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Matricule</th>
                                    <th>Nom</th>
                                    <th>Prenom</th>
                                    <th>CIN</th>
                                    <th>Nom mere</th>
                                     <th>Employeur</th>

                            </tr>
                            <%
                                    for (int i = 0; i < trv.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=trv[i].getId_travailleurs()%>;<%=trv[i].getId_travailleurs()%>" class="radio" /></td>
                                    <td align=center><%=trv[i].getId_travailleurs()%></td>
                                    <td align=center><%=trv[i].getTravailleur_matricule()%></td>
                                    <td align=center><%=trv[i].getPers_nom()%></td>
                                    <td align=center><%=trv[i].getPers_prenom()%></td>
                                    <td align=center><%=trv[i].getPers_numero_cin()%></td>
                                    <td align=center><%=trv[i].getPers_nom_mere()%></td>   
                                      <td align=center><%=trv[i].getEmpl_matr()%></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
