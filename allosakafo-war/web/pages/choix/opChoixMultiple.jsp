<%-- 
    Document   : opChoixMultiple
    Created on : 6 janv. 2016, 14:43:47
    Author     : user
--%>
<%@page import="mg.cnaps.treso.OpTefProjet"%>
<%@page import="mg.cnaps.medecine.MedDemConsultationComplet"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    OpTefProjet demconsulcomp = new OpTefProjet();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "op_num", "date_op", "montant_op", "beneficiaire_op"};
    String listeInt[] = {"date_op"};
    String libEntete[] = {"id", "op_num", "date_op", "montant_op", "beneficiaire_op"};
    PageRechercheChoix pr = new PageRechercheChoix(demconsulcomp, request, listeCrt, listeInt, 2, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("opChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    OpTefProjet[] listeP = (OpTefProjet[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Ordre de Paiement</h1>
            </section>
            <section class="content">
                <form action="opChoixMultiple.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="op" id="op">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "Numero", "Date", "Montant", "Bénéficiaire"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixOp.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                        <tr class=info>
                            <th>#</th>
                            <th>Id</th>
                            <th>Numero OP</th>
                            <th>Date OP</th>
                            <th>Montant OP</th>

                            <th>Bénéficiaire OP</th>
                            <th>Code TEF</th>
                            <th>Libelle TEF</th>
                            <th>Date TEF</th>
                            <th>Code Projet</th>
                            <th>Libelle projet</th>

                        </tr>
                        <%
                            for (int i = 0; i < listeP.length; i++) {
                        %>
                        <tr>
                            <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getDate_op()%>;<%=listeP[i].getMontant_op()%>;<%=listeP[i].getOp_num()%>;<%=listeP[i].getLibelle_tef()%>;<%=listeP[i].getId()%>;<%=listeP[i].getBeneficiaire_op()%>;<%=listeP[i].getCode_tef()%>;<%=listeP[i].getDate_tef()%>;<%=listeP[i].getCode_projet()%>;<%=listeP[i].getLibelle_projet()%>;<%=listeP[i].getReference() %>" class="radio" /></td>
                            <td align=center><%=listeP[i].getId()%></td>
                            <td align=center><%=Utilitaire.champNull(listeP[i].getOp_num())%></td>
                            <td align=center><%=Utilitaire.champNull(Utilitaire.formatterDaty(listeP[i].getDate_op()))%></td>
                            <td align=center><%=Utilitaire.champNull(Utilitaire.formaterAr(listeP[i].getMontant_op()))%></td>

                            <td align=center><%=Utilitaire.champNull(listeP[i].getBeneficiaire_op())%></td>
                            <td align=center><%=Utilitaire.champNull(listeP[i].getCode_tef())%></td>
                            <td align=center><%=Utilitaire.champNull(listeP[i].getLibelle_tef())%></td>
                            <td align=center><%=Utilitaire.champNull(Utilitaire.formatterDaty(listeP[i].getDate_tef()))%></td>
                            <td align=center><%=Utilitaire.champNull(listeP[i].getCode_projet())%></td>
                            <td align=center><%=Utilitaire.champNull(listeP[i].getLibelle_projet())%></td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
