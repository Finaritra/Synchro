<%@page import="java.lang.reflect.Field"%>
<%@page import="mg.cnaps.log.LogPersonnel"%>
<%@page import="java.sql.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.Timestamp"%>
﻿<%@page import="mg.cnaps.accueil.Dossiers"%>
<%@page import="mg.cnaps.commun.Sig_region"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.securite.*" %>
<%
   String table = request.getParameter("table");
   String nomClasse = request.getParameter("nomclasse");
   if((table == null || "".equals(table)))
       throw new Exception("table vide");
   if((nomClasse == null || "".equals(nomClasse)))    
       throw new Exception("nomclasse vide");
   
   ClassMAPTable t = (ClassMAPTable) (Class.forName(nomClasse).newInstance()); 
   ClassMAPTable[] liste =(ClassMAPTable[]) CGenUtil.rechercher(t, null, null, "");
   
   

%>
<link href="${pageContext.request.contextPath}/assets/calendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/calendar/bootstrapmodal.css" rel="stylesheet" type="text/css" />

<div class="content-wrapper">
    <div class="col-md-12">
        <div id="calendar"></div>
        <div id="fullCalModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 id="modalTitle" class="modal-title"></h1>                        
                    </div>
                    <div id="modalBody" class="modal-body"></div>
                    <div id="modalInfo" class="modal-info"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary"><a id="eventUrl" target="_blank">Fiche du planning</a></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/assets/calendar/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/calendar/jquery.min.js"></script>        
<script src="${pageContext.request.contextPath}/assets/calendar/fullcalendar.min.js"></script>       
<script src="${pageContext.request.contextPath}/assets/calendar/fr.js"></script>       
<script src="${pageContext.request.contextPath}/assets/calendar/bootstrapmodal.min.js"></script> 

<script>
            $(document).ready(function() {
    $('#calendar').fullCalendar({
    header: {
    left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
    },
            defaultDate: '<%=Utilitaire.dateDuJourSql().toString()%>',
            editable: true,
            lang: 'fr',
            eventLimit: true,
            events: [
    <% for (int i = 0; i < liste.length - 1; i++) {
            Field[] listefield = liste[i].getFieldList();
            
            Timestamp t1 = null ;
            String temp1;
            Timestamp t2 = null;
            String temp2;
            Date datedujour = Utilitaire.dateDuJourSql();
            Date datedebut = null;
            Date datefin = null ;
            String remarque = liste[i].getValInsert("remarque");
            String description = liste[i].getValInsert("description");
            for(Field e : listefield){
                if(e.getName().equals("heuredebut"))
                {
                    t1 =(Timestamp) (liste[i].getValField(e));
                }
                if(e.getName().equals("heurefin")){
                    t2 =(Timestamp) (liste[i].getValField(e));
                }
                if(e.getName().equals("datedebut")){
                    datedebut =(Date) (liste[i].getValField(e));
                }
                if(e.getName().equals("datefin")){
                    datefin =(Date) (liste[i].getValField(e));
                }
            }
            temp1 = Utilitaire.getHeureFromTimestamp(t1);
            temp2 = Utilitaire.getHeureFromTimestamp(t2);
            String time1 = datedebut.toString() + " " + temp1;
            String time2 = datefin.toString() + " " + temp2;
    %>
            {
                    title: '<%=remarque%>',
                    description: '<%=SecService.makeDescriptionHtml(description, time1, time2)%>',
                    start: '<%=datedebut.toString()%>T<%=temp1%>',
                    end: '<%=datefin.toString()%>T<%=temp2%>'
    <%
        if (datedujour.after(datefin)) {
    %>
                   , backgroundColor: '#ef1c1c'
    <%
        }
        if (datedujour.before(datedebut)) {
    %>
                    , backgroundColor: '#5af044'
    <%
        }
    %>


            },
    <%
        }
            Field[] listefield = liste[liste.length-1].getFieldList();
            
            Timestamp t1 = null ;
            String temp1;
            Timestamp t2 = null;
            String temp2;
            Date datedujour = Utilitaire.dateDuJourSql();
            Date datedebut = null;
            Date datefin = null ;
            String remarque = liste[liste.length-1].getValInsert("remarque");
            String description = liste[liste.length-1].getValInsert("description");
            for(Field e : listefield){
                if(e.getName().equals("heuredebut"))
                {
                    t1 =(Timestamp) (liste[liste.length-1].getValField(e));
                }
                if(e.getName().equals("heurefin")){
                    t2 =(Timestamp) (liste[liste.length-1].getValField(e));
                }
                if(e.getName().equals("datedebut")){
                    datedebut =(Date) (liste[liste.length-1].getValField(e));
                }
                if(e.getName().equals("datefin")){
                    datefin =(Date) (liste[liste.length-1].getValField(e));
                }
            }
            temp1 = Utilitaire.getHeureFromTimestamp(t1);
            temp2 = Utilitaire.getHeureFromTimestamp(t2);
            String time1 = datedebut.toString() + " " + temp1;
            String time2 = datefin.toString() + " " + temp2;
        
    %>
            {   
                     title: '<%=remarque%>',
                    description: '<%=SecService.makeDescriptionHtml(description, time1, time2)%>',
                    start: '<%=datedebut.toString()%>T<%=temp1%>',
                    end: '<%=datefin.toString()%>T<%=temp2%>' 
    <%
        if (datedujour.after(datefin)) {
    %>
                    , backgroundColor: '#ef1c1c'
    <%
        }
        if (datedujour.before(datedebut)) {
    %>
                    , backgroundColor: '#5af044'
    <%
        }
    %>
            }
                    ],
        eventClick:  function(event, jsEvent, view) {
                    $('#modalTitle').html(event.title);
                    $('#modalBody').html(event.description);
                    $('#eventUrl').attr('href', '#');
                    $('#fullCalModal').modal();
        }
        });
});

</script>