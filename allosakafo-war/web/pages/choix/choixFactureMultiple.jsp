<%-- 
    Document   : choixFactureMultiple
    Created on : 9 juin 2016, 20:03:54
    Author     : Joe
--%>
<%@page import="mg.cnaps.st.StFactureAppro"%>
<%@page import="mg.cnaps.medecine.MedBeneficiaireComplet"%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="user.UserEJB"%>
<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    
    StFactureAppro fact = new StFactureAppro();

    String listeCrt[] = {"id", "daty", "facture", "commande"};
    String listeInt[] = {"daty",};
    String libEntete[] = {"id", "daty", "facture", "commande","idobjet", "montant"};

    PageRechercheChoix pr = new PageRechercheChoix(fact, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date max");
    pr.setChampReturn(request.getParameter("champReturn"));
    pr.setApres("choixFactureMultiple.jsp");
    
    pr.setAWhere(" AND IDOBJET IS NULL");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
%>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste facture</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="cc" id="cc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%

                    String libEnteteAffiche[] = {"Id", "Date", "Facture", "Commande", "IdObjet", "Montant"};
                    pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixFactureMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <%=pr.getTableau().getHtmlWithMultipleCheckbox()%>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
