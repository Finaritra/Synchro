<%-- 
    Document   : beneficiaireChoixSaisieTef
    Created on : 7 juin 2016, 14:42:54
    Author     : Ignafah
--%>

<%@page import="mg.cnaps.accueil.Sig_beneficiaire"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    Sig_beneficiaire e = new Sig_beneficiaire();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "matricule", "type","nom","cin"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "matricule", "type","nom","cin"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("beneficiaireChoixSaisieTef.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("matricule").setLibelleAffiche("Matricule");
    pr.getFormu().getChamp("type").setLibelleAffiche("Type beneficiaire");
    pr.getFormu().getChamp("nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("cin").setLibelleAffiche("CIN");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste bénéficiaire</h1>
            </section>
            <section class="content">
                <form action="beneficiaireChoixSaisieTef.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="benef" id="benef">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "matricule", "type","nom","cin"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                    Sig_beneficiaire[] sb = ( Sig_beneficiaire[])pr.getRs().getResultat();                    
                %>
                <!-- <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx"> -->
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixSaisieTef.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <input type="hidden" name="cible" value="beneficiaire">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Matricule</th>
                                    <th>Type</th>
                                    <th>Nom</th>
                                    <th>CIN</th>
                            </tr>
                            <% for(int i=0; i<sb.length; i++){                                
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=sb[i].getId()%>;<%=sb[i].getNom()%>;<%=sb[i].getP_id_agence()%>;<%=sb[i].getP_id_banque()%>;<%=sb[i].getP_numero_compte()%>;<%=sb[i].getP_cle()%>" class="radio"></td>
                                <td align=center><%=sb[i].getId() %></td>
                                <td align=center><%=sb[i].getMatricule() %></td>
                                <td align=center><%=sb[i].getType() %></td>
                                <td align=center><%=sb[i].getNom() %></td>
                                <td align=center><%=sb[i].getCin() %></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>