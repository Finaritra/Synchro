<%@page import="mg.cnaps.log.LogPersonnelValide"%>
<%@page import="mg.cnaps.archive.ArchClassementView"%>
<%@page import="affichage.PageRechercheChoix"%>
<%

    LogPersonnelValide e = new LogPersonnelValide();
    e.setNomTable("LOG_PERSONNEL_VALIDE_VIEW");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "MATRICULE", "nom", "prenom", "direction", "service"};
    String listeInt[] = null;
    String libEntete[] = {"id", "MATRICULE", "nom", "prenom", "direction", "service"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("logPersonnelChoixSaisieCourrier.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("prenom").setLibelleAffiche("Pr�nom");
    pr.getFormu().getChamp("direction").setLibelleAffiche("Direction");
    pr.getFormu().getChamp("service").setLibelleAffiche("Service");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    LogPersonnelValide[] lpv = (LogPersonnelValide[]) pr.getListe();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des personnels</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="cc" id="cc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixSaisieCourrier.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <input type="hidden" name="cible" value="personnel"/>
                    <% for (int i = 0; i < lpv.length; i++) {%>
                    <input type="hidden" name="choix" value="<%=lpv[i].getId()%>;<%=lpv[i].getAdresse()%>">
                    <%}%>
                    <%
                        String libelles[] = {"id", "Matricule", "nom", "prenom", "direction", "service"};
                        pr.getTableau().setLibelleAffiche(libelles);
                        out.println(pr.getTableau().getHtmlWithRadioButton());
                    %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>

