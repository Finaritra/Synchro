<%-- 
    Document   : agenceCodeChoix
    Created on : 15 sept. 2015, 11:40:26
    Author     : user
--%>



<%@page import="mg.cnaps.accueil.TrsBanquesCodeLibelle"%>
<%@page import="mg.cnaps.compta.ComptaTiersView"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<% 
    TrsBanquesCodeLibelle e = new TrsBanquesCodeLibelle();
    e.setNomTable("TRS_BANQUES_CODE_LIBELLE");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "val", "desce", "code_banque", "code_agence"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "val", "desce", "code_banque", "code_agence"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete,4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("agenceBanqueRetourCode.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("val").setLibelleAffiche("Valeur");
    pr.getFormu().getChamp("desce").setLibelleAffiche("Description");
    pr.getFormu().getChamp("code_banque").setLibelleAffiche("Code banque");
   pr.getFormu().getChamp("code_agence").setLibelleAffiche("Code agence");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    TrsBanquesCodeLibelle[] listeP = (TrsBanquesCodeLibelle[]) pr.getRs().getResultat();
%>


<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste agences</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="agencecode" id="agencecode">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "val", "desce", "code agence", "code banque"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixCodeAgence4.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Banque</th>
                                    <th>Code Banque</th>
                                    <th>Agence</th>
                                    <th> Code Agence</th>
                                    

                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getCode_agence()%>;<%= listeP[i].getCode_banque()%>" class="radio" /></td>
                                    <td align=center><%=listeP[i].getVal_banque()%></td>
                                    <td align=center><%=listeP[i].getCode_banque()%></td>

                                    <td align=center><%=listeP[i].getDesce()%></td>
                                   <td align=center><%=listeP[i].getCode_agence()%></td>
                                    
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>