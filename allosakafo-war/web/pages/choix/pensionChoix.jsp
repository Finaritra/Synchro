<%-- 
    Document   : pensionChoix
    Created on : 5 juil. 2016, 11:01:45
    Author     : Ignafah
--%>

<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.accueil.Pension_droit"%>
<%
    Pension_droit pd = new Pension_droit();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"idtravailleur", "id_pension_type", "num_pension","echeance_droit"};
    String listeInt[] = null;
    String libEntete[] = {"idtravailleur", "id_pension_type", "num_pension","echeance_droit"};
    PageRechercheChoix pr = new PageRechercheChoix(pd, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("pensionChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("idtravailleur").setLibelleAffiche("Travailleur");
    pr.getFormu().getChamp("id_pension_type").setLibelleAffiche("Type pension");
    pr.getFormu().getChamp("num_pension").setLibelleAffiche("N&ordm; pension");
    pr.getFormu().getChamp("echeance_droit").setLibelleAffiche("Ech�ance");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste pension</h1>
            </section>
            <section class="content">
                <form action="pensionChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="benef" id="benef">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%                    
                    Pension_droit[] sb = (Pension_droit[])pr.getRs().getResultat();                    
                %>
                <!-- <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx"> -->
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <input type="hidden" name="cible" value="droitdetail">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Travailleur</th>
                                    <th>Type pension</th>
                                    <th>N� Pension</th>
                                    <th>Echeance</th>
                            </tr>
                            <% for(int i=0; i<sb.length; i++){                                
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=sb[i].getIdtravailleur()%>;<%=sb[i].getId_pension_type() %>;<%=sb[i].getNum_pension() %>;<%=sb[i].getEcheance_droit()%>;<%= sb[i].getNum_pension() %>;<%=sb[i].getIdtravailleur()%>" class="radio"></td>
                                <td align=center><%=sb[i].getIdtravailleur() %></td>
                                <td align=center><%=sb[i].getId_pension_type() %></td>
                                <td align=center><%=sb[i].getNum_pension() %></td>
                                <td align=center><%=sb[i].getEcheance_droit() %></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
