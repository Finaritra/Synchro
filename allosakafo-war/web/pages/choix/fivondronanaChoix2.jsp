<%@page import="mg.cnaps.sig.*"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    SigFivondronana livre = new SigFivondronana();
    livre.setNomTable("SIG_FIV");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "val", "desce", "code_region"};
    String listeInt[] = {};
    String libEntete[] = {"id", "val", "desce", "code_region"};
    PageRechercheChoix pr = new PageRechercheChoix(livre, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("fivondronanaChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("val").setLibelleAffiche("Valeur");
    pr.getFormu().getChamp("desce").setLibelleAffiche("Code postal");
    pr.getFormu().getChamp("code_region").setLibelleAffiche("Code r�gion");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);

%>
<html> 
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Livre</h1>
            </section>
            <section class="content">
                <form action="fivondronanaChoix2.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="livre" id="livre">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
