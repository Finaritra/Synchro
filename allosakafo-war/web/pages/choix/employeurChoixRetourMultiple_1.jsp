<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.sig.*" %>
<%
    String champReturn = request.getParameter("champReturn");
    VueSigEmployeur e = new VueSigEmployeur();
    e.setNomTable("vuesigemployeurs");
    String listeCrt[]={"id","nom","employeur_telephone", "code_dr","emp_matricule","employeur_mail","employeur_adresse_lot"};
    String listeInt[]=null;
    String libEntete[]={"id","nom","employeur_telephone", "code_dr","emp_matricule","employeur_mail","employeur_adresse_lot"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("employeurChoixRetourMultiple_1.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("nom").setLibelleAffiche("Nom");
    pr.getFormu().getChamp("employeur_telephone").setLibelleAffiche("T�l�phone");
    pr.getFormu().getChamp("code_dr").setLibelleAffiche("Direction r�gionale");
    pr.getFormu().getChamp("emp_matricule").setLibelleAffiche("Matricule");
    pr.getFormu().getChamp("employeur_mail").setLibelleAffiche("E-mail");
    pr.getFormu().getChamp("employeur_adresse_lot").setLibelleAffiche("Adresse");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    VueSigEmployeur[] liste = (VueSigEmployeur[])pr.getListe();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Employeurs</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="agencecode" id="agencecode">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id","nom","Telephone","Direction","Matricule","E-mail","Adresse"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../choix/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>id</th>
                                    <th>nom</th>
                                    <th>Telephone</th>
				    <th>Direction</th>
				    <th>Matricule</th>
				    <th>E-mail</th>
				    <th>Adresse</th>

                            </tr>
                            <%
                                    for (int i = 0; i < liste.length; i++) {
                            %>
                            <tr>
                                <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=liste[i].getEmp_matricule()%>;<%=liste[i].getNom()%>" class="radio" /></td>
                                    <td align=center><%=liste[i].getId()%></td>
                                    <td align=center><%=liste[i].getNom()%></td>
                                    <td align=center><%=liste[i].getEmployeur_telephone()%></td>
                                    <td align=center><%=liste[i].getCode_dr()%></td>
                                    <td align=center><%=liste[i].getEmp_matricule()%></td>
                                    <td align=center><%=liste[i].getEmployeur_mail()%></td>
                                    <td align=center><%=liste[i].getEmployeur_adresse_lot()%></td>
                                    
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>