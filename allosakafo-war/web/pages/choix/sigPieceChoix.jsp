<%-- 
    Document   : archEtagereChoix
    Created on : 29 d�c. 2015, 14:16:37
    Author     : user
--%>
<%@page import="mg.cnaps.accueil.PiecesRecues"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    PiecesRecues lv = new PiecesRecues();
    lv.setNomTable("sig_pieces_recues_libelle2");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "PIECES_NOM_FICHIER", "TYPE_PIECE_CODE","PIECE_RECUE_NB","DOSSIER_NUMERO"};
    String listeInt[] = null;
    String libEntete[] = {"id", "PIECES_NOM_FICHIER", "TYPE_PIECE_CODE","PIECE_RECUE_NB","DOSSIER_NUMERO"};
    PageRechercheChoix pr = new PageRechercheChoix(lv, request, listeCrt, listeInt, 3, libEntete, 5);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("sigPieceChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("PIECES_NOM_FICHIER").setLibelleAffiche("Nom de pi�ce");
    pr.getFormu().getChamp("TYPE_PIECE_CODE").setLibelleAffiche("type de pi�ce");
    pr.getFormu().getChamp("PIECE_RECUE_NB").setLibelleAffiche("Nombre de pi�ce");
    pr.getFormu().getChamp("DOSSIER_NUMERO").setLibelleAffiche("Numero de dossier");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des �tag�res </h1>
            </section>
            <section class="content">
                <form action="apresChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    
                    String libelles[] =  {"Id", "Nom de pi�ce", "Type de pi�ce", "Nombre re�u", "Numero dossier"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>