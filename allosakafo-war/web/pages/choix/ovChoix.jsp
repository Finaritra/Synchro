<%-- 
    Document   : ovChoix
    Created on : 15 sept. 2015, 15:33:06
    Author     : user
--%>
<%@page import="mg.cnaps.st.StDemandeAppro"%>
<%@page import="mg.cnaps.treso.TresoOv"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    TresoOv t = new TresoOv();
    String listeCrt[] = {"id", "libelle", "modedepayement", "comptesociete", "daty", "montant", "etat"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "libelle", "modedepayement", "comptesociete", "daty", "montant", "etat"};
    PageRechercheChoix pr = new PageRechercheChoix(t, request, listeCrt, listeInt,3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("ovChoix.jsp");
    
    pr.getFormu().getChamp("id").setLibelleAffiche("Id");
    pr.getFormu().getChamp("libelle").setLibelleAffiche("Libelle");
    pr.getFormu().getChamp("modedepayement").setLibelleAffiche("Mode de payement");
    pr.getFormu().getChamp("comptesociete").setLibelleAffiche("Compte societe");
    pr.getFormu().getChamp("daty").setLibelleAffiche("Date");
    pr.getFormu().getChamp("montant").setLibelleAffiche("Montant");
    pr.getFormu().getChamp("etat").setLibelleAffiche("Etat");
    
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    TresoOv[] listeD = (TresoOv[]) pr.getRs().getResultat();
%>
  <!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste ordre de virement</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="ov" id="ov">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=tresorerie/OV/ov-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[]={"id", "libelle", "modedepayement", "comptesociete", "daty", "montant", "etat"};
		    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
