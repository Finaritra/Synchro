<%-- 
    Document   : deplacementChoix
    Created on : 8 sept. 2015, 20:58:05
    Author     : user
--%>
<%@page import="mg.cnaps.log.LogPersonnelLibelle"%>
<%@page import="mg.cnaps.log.LogDeplacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    LogPersonnelLibelle e = new LogPersonnelLibelle();
    e.setNomTable("log_personnel_libelle");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "nom" };
    String listeInt[] = null;
    String libEntete[] = {"id", "nom"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 2);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("logPersonnelLibelleChoix.jsp");
    pr.setChampReturn(champReturn);
 /*   pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("motif").setLibelleAffiche("Motif");
    pr.getFormu().getChamp("type_deplacement").setLibelleAffiche("Type de déplacement");
    pr.getFormu().getChamp("date_depart").setLibelleAffiche("Date de départ");
    pr.getFormu().getChamp("antenne_depart").setLibelleAffiche("Antenne de départ");
    pr.getFormu().getChamp("chauffeur").setLibelleAffiche("Chauffeur");
    pr.getFormu().getChamp("vehicule").setLibelleAffiche("Vehicule");*/
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste déplacement</h1>
            </section>
            <section class="content">
                <form action="logPersonnelLibelleChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="deplacement" id="deplacement">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=materiel_roulant/panne-mt/incidentDepl-saisie.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[] = {"ID", "nom"};
                    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>