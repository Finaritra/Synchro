<%-- 
    Document   : deplacementChoix
    Created on : 8 sept. 2015, 21:26:25
    Author     : user
--%>
<%@page import="mg.cnaps.securite.SecPersonnel"%>
<%@page import="mg.cnaps.log.PersFonctionDirectService"%>
<%@page import="mg.cnaps.log.LogPersonnel"%>
<%@page import="mg.cnaps.log.LogDeplacement"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    SecPersonnel p = new SecPersonnel();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "nom", "prenom", "adresse","type"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "nom", "prenom", "adresse","type"};
    
    
    
    PageRechercheChoix pr = new PageRechercheChoix(p, request, listeCrt, listeInt, 3, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("employe-planning-choix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    SecPersonnel [] liste = (SecPersonnel[]) pr.getRs().getResultat();
    
    
    
    String index = request.getParameter("index");
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste personnels s�curit�</h1>
            </section>
            
                <form action="employe-planning-choix.jsp?champReturn=<%=pr.getChampReturn()%>&index=<%= index %>" method="post" name="chauffeur" id="chauffeur">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=paie/employe/personnel-fiche.jsp"};
                    String colonneLien[] = {"id"};
                   // pr.getTableau().setLien(lienTableau);
                   // pr.getTableau().setColonneLien(colonneLien);
                    //out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apres-employe-planning-choix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <p align="center"><strong><u>LISTE</u></strong></p>
                    <div id="divchck">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table" table-hover="">
                            <tbody>
                                <tr class="head">
                                    <td align="center" valign="top"></td>
                                    <td width="33%" align="center" valign="top">Id</td>
                                    <td width="33%" align="center" valign="top">Nom</td>
                                    <td width="33%" align="center" valign="top">Prenom</td>
                                    <td width="33%" align="center" valign="top">Adresse</td>
                                    <td width="33%" align="center" valign="top">Type</td>
                                </tr>



                                <%
                                    
                                        SecPersonnel emp = null;
                                        for (int i = 0; i < liste.length; i++) 
                                        {
                                            emp = liste [i];                                        
                                       
                                        %>
                                            <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
                                                <td align="center">
                                                    <input type="checkbox" value="<%=emp.getId()%>;<%=emp.getNom() + " " + emp.getPrenom()%>" name="choix"  id="choix" class="radio">
                                                </td>
                                                <td width="33%" align="center"><a href="<%=pr.getLien()%>?but=paie/employe/personnel-fiche.jsp&id=<%=liste[i].getId()%>"><%=liste[i].getId()%></a></td>
                                                <td width="33%" align="center"><%=emp.getNom()%></td>
                                                <td width="33%" align="center"><%=emp.getPrenom()%></td>
                                                <td width="33%" align="center"><%=emp.getAdresse()%></td>
                                                <td width="33%" align="center"><%=emp.getType()%></td>
                                            </tr>
                                        <%}%>
                            
                            
                            
                            </tbody>
                        </table>
                                
                
                <% out.println(pr.getBasPage());%>
            
        </div>
        <div class="row">
            <div class="col-xs-5"></div>
            <div class="col-xs-7"><input type="submit" name="Submit" value="Ajouter" class="btn btn-success" >
            </div>
        </div>
                    <input type="hidden" name="index" value="<%= index %>">
                    </form>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>