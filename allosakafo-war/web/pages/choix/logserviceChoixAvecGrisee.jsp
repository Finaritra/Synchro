<%-- 
    Document   : serviceChoix
    Created on : 30 sept. 2015, 17:41:03
    Author     : user
--%>
<%@page import="mg.cnaps.log.LogService"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%

    LogService e = new LogService();
    e.setNomTable("log_service_info");//si_dossiers_travailleurs_lib
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "libelle", "code_dr", "dr_rattache"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "libelle", "code_dr", "dr_rattache"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 4);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("logserviceChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("libelle").setLibelleAffiche("Libelle");
    pr.getFormu().getChamp("code_dr").setLibelleAffiche("Direction regionale");
    pr.getFormu().getChamp("dr_rattache").setLibelleAffiche("DR rattache");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
	LogService[] liste = (LogService[])pr.getRs().getResultat();

%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Service</h1>
            </section>
            <section class="content">
                <form action="logserviceChoixAvecGrisee.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
//                    String lienTableau[] = {pr.getLien() + "?but=dossier/dossier-travailleur.jsp"};
//                    String colonneLien[] = {"id"};
                    String libelles[] = {"ID", "Libelle", "Direction regionale", "DR rattache"};
//                    pr.getTableau().setLien(lienTableau);
//                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixCm.jsp" method="post" name="frmchx" id="frmchx">
					<input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <p align="center"><strong><u>LISTE</u></strong></p>
                    <div id="divchck">
                        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table" table-hover="">
                            <tbody>
                                <tr class="head">
                                    <td align="center" valign="top"></td>
                                    <td width="33%" align="center" valign="top">Id</td>
                                    <td width="33%" align="center" valign="top">Libelle</td>
                                    <td width="33%" align="center" valign="top">Direction régional</td>
                                    <td width="33%" align="center" valign="top">Dr rattache</td>
                                </tr>
                                <%for(int i = 0 ; i<liste.length ; i++){%>
                                    <tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
                                        <td align="center">
                                        <input type="radio" value="<%=liste[i].getId()%>;<%=liste[i].getId()%>" name="choix" onmousedown="getChoix()" id="choix" class="radio"></td>
                                        <td width="33%" align="center"><%=liste[i].getId() %></td>
                                        <td width="33%" align="center"><%=liste[i].getLibelle() %></td>
                                        <td width="33%" align="center"><%=liste[i].getCode_dr() %></td>
                                        <td width="33%" align="center"><%=liste[i].getDr_rattache() %></td>

                                    </tr>
                                <%}%>
                            </tbody>
                        </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>