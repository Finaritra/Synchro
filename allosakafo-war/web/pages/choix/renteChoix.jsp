<%@page import="mg.cnaps.accident.SigAtmpRenteLibelle"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.commun.*" %>


<%
    String champReturn = request.getParameter("champReturn");
    SigAtmpRenteLibelle e = new SigAtmpRenteLibelle();
    String listeCrt[] = {"id","iddossier","dossier_matricule", "dossier_date_reception","travailleur_matricule", "pers_nom","pers_prenom"};
    String listeInt[] = {""};
    String libEntete[] = {"id","iddossier","dossier_matricule", "dossier_date_reception","travailleur_matricule", "pers_nom","pers_prenom"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("renteChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("Numero rente");
    pr.getFormu().getChamp("iddossier").setLibelleAffiche("ID. dossier");
    pr.getFormu().getChamp("dossier_matricule").setLibelleAffiche("Matricule dossier");
    pr.getFormu().getChamp("dossier_date_reception").setLibelleAffiche("Date de reception dossier");
    pr.getFormu().getChamp("travailleur_matricule").setLibelleAffiche("Matricule travailleur");
    pr.getFormu().getChamp("pers_nom").setLibelleAffiche("Travailleur nom");
    pr.getFormu().getChamp("pers_prenom").setLibelleAffiche("Travailleur prenom");
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste fokotany</h1>
            </section>
            <section class="content">
                <form action="renteChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=employeur/employeur-fiche.jsp"};
                    String colonneLien[] = {"id"};
                    String libelles[]={"ID Dossier","ID. Dossier","Dossier matricule", "Dossier date reception","Matricule travailleur", "Travailleur nom","Travailleur prenom"};
		    pr.getTableau().setLien(lienTableau);
                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>