<%@page import="mg.allosakafo.fin.*"%>
<%@page import="mg.allosakafo.ded.*"%>
<%@page import="user.*"%> 
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*"%>
<%@page import="utilitaire.*"%>
<%@page import="bean.*"%>

<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    MvtCaisse  a = new MvtCaisse();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));    
    
	pi.getFormu().getChamp("idordre").setLibelle("Reference");
	pi.getFormu().getChamp("idordre").setPageAppel("choix/listeOPViseChoix.jsp");
	
    pi.getFormu().getChamp("daty").setLibelle("Date");
	pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
	pi.getFormu().getChamp("datyvaleur").setLibelle("Date valeur");
	
	pi.getFormu().getChamp("debit").setLibelle("Montant");
	pi.getFormu().getChamp("designation").setLibelle("Designation");
	pi.getFormu().getChamp("numpiece").setLibelle("N� piece");
	pi.getFormu().getChamp("numcheque").setLibelle("N� cheque");
	pi.getFormu().getChamp("typemvt").setLibelle("Type mouvement");
	pi.getFormu().getChamp("remarque").setLibelle("Remarque");
	pi.getFormu().getChamp("remarque").setType("textarea");
	
	/* ??? */
	pi.getFormu().getChamp("tiers").setLibelle("Tiers");
		
	affichage.Champ[] liste = new affichage.Champ[4];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("devise");
    liste[0] = new Liste("iddevise", ou, "VAL", "id");
	
	TypeObjet ou1 = new TypeObjet();
    ou1.setNomTable("modepaiement");
    liste[1] = new Liste("idmode", ou1, "VAL", "id");
	
	Caisse ou2 = new Caisse();
    ou2.setNomTable("caisse");
    liste[2] = new Liste("idcaisse", ou2, "desccaisse", "idcaisse");
    pi.getFormu().getChamp("idcaisse").setDefaut(session.getAttribute("caisse").toString());

	TypeObjet ou3 = new TypeObjet();
    ou3.setNomTable("typemvt");
    liste[3] = new Liste("typemvt", ou3, "VAL", "id");
	
    pi.getFormu().changerEnChamp(liste);

	pi.getFormu().getChamp("iddevise").setLibelle("Devise");
	pi.getFormu().getChamp("idmode").setLibelle("Mode de paiement");
	pi.getFormu().getChamp("idcaisse").setLibelle("Caisse");
	pi.getFormu().getChamp("credit").setVisible(false);
	pi.getFormu().getChamp("etat").setVisible(false);
        
        pi.getFormu().getChamp("idMode").setDefaut("pay1");
        pi.getFormu().getChamp("typemvt").setDefaut("1");

	/*if (request.getParameter("idordre")!= null && request.getParameter("idordre").compareTo("") != 0){
		pi.getFormu().getChamp("idordre").setDefaut(request.getParameter("idordre"));
		SituationOp[] sop = (SituationOp[])CGenUtil.rechercher(new SituationOp(), null, null, " AND ID = '"+request.getParameter("idordre")+"'");
                if(sop.length > 0){
                    pi.getFormu().getChamp("debit").setDefaut(Utilitaire.doubleWithoutExponential(sop[0].getReste()));
                }else{
                    OrdonnerPayement opg = new OrdonnerPayement();
                    OrdonnerPayement op = ((OrdonnerPayement[]) CGenUtil.rechercher(opg, "select * from OrdonnerPayement where id='"+request.getParameter("idordre")+"'"))[0];
                    pi.getFormu().getChamp("debit").setDefaut(Utilitaire.doubleWithoutExponential(op.getMontant()));
                }
	}*/
	if (request.getParameter("remarque")!= null && request.getParameter("remarque").compareTo("") != 0) pi.getFormu().getChamp("designation").setDefaut(request.getParameter("remarque"));
	if (request.getParameter("montant")!= null && request.getParameter("montant").compareTo("") != 0) pi.getFormu().getChamp("debit").setDefaut(request.getParameter("montant"));
        if (request.getParameter("idordre")!= null && request.getParameter("idordre").compareTo("") != 0) pi.getFormu().getChamp("idordre").setDefaut(request.getParameter("idordre"));
        if (request.getParameter("typemvt")!= null && request.getParameter("typemvt").compareTo("") != 0) pi.getFormu().getChamp("typemvt").setDefaut(request.getParameter("typemvt"));
        pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Enregistrer Sortie Caisse</h1>
    <form action="<%=pi.getLien()%>?but=fin/apresMvt.jsp" method="post" name="ded_op" id="ded_op">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="fin/mvtcaisse-fiche.jsp">
    
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.fin.MvtCaisse">
    </form>
</div>