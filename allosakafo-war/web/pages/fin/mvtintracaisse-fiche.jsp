
<%@page import="mg.allosakafo.fin.MvtIntraCaisse"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
try{
    MvtIntraCaisse a = new MvtIntraCaisse();
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));
    a = (MvtIntraCaisse)pc.getBase();
    pc.getChampByName("id").setLibelle("Id");
    pc.getChampByName("daty").setLibelle("Date");
    
    pc.setTitre("Fiche mouvement intra caisse");
    MvtIntraCaisse donne=(MvtIntraCaisse)pc.getBase();
    MvtIntraCaisse[] remboursement=donne.getContre(null, "MvtIntraCaisseLibSource");
	double montantT = 0;
	if(remboursement != null)
		montantT =  AdminGen.calculSommeDouble(remboursement, "montant");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=fin/mvtintracaisse-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>

                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                            
                        %>
                        <br/>
                        <div class="box-footer" >
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&classe=mg.allosakafo.fin.MvtIntraCaisse&bute=fin/mvtintracaisse-fiche.jsp&id=" + request.getParameter("id")+"&etat="+a.getEtat() %>" target="_blank" style="margin-right: 10px">Viser</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annulerVisa&classe=mg.allosakafo.fin.MvtIntraCaisse&bute=fin/mvtintracaisse-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=fin/mvtintracaisse-saisie.jsp&caissedepart="+donne.getCaissearrive()+"&caissearrive="+donne.getCaissedepart()+"&source="+donne.getId() %>" target="_blank" style="margin-right: 10px">Regulariser credit</a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
     <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <h3> Total : <%=Utilitaire.formaterAr(montantT) %></h3>
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">Liste des payements</h1>
                    </div>
                        <div class="box-body table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        
                                        <th>Daty</th>
                                        <th>Montant</th>
                                        <th>Remarque</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    <%
										if(remboursement != null){
											for (int i = 0; i < remboursement.length; i++) {
                                    %>
												<tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
													
													
													
													<td  align="center"><%=utilitaire.Utilitaire.formatterDaty(remboursement[i].getDaty()) %></td>
													 <td  align="right"><%=remboursement[i].getMontant() %></td>
													<td  align="right"><%=remboursement[i].getRemarque() %></td>

												</tr>
                                    <%
											}
										}
                                    %>

                                </tbody>
                            </table>

                        </div>
                    
                </div>
            </div>
        </div>
</div>
<%}catch(Exception ex) {
	ex.printStackTrace();
}%>
