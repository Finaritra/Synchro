<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="bean.AdminGen"%>
<%@page import="user.UserEJB"%>
<%@page import="utilitaire.Utilitaire"%>
<%@page import="mg.allosakafo.fin.EtatdeCaisseDate"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%@page import="affichage.PageRecherche"%>

<% 
    EtatdeCaisseDate lv = new EtatdeCaisseDate();
    
    String listeCrt[] = {"daty", "caisse", "devise"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"caisse","devise","report", "debit", "credit", "disponible"};
    
	UserEJB u = (user.UserEJB) session.getValue("u");
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 3);
    pr.setUtilisateur(u);
    pr.setLien((String) session.getValue("lien"));
        
    affichage.Champ[] liste = new affichage.Champ[2];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("Devise");
    liste[0] = new Liste("devise", ou, "VAL", "id");

	Caisse ou2 = new Caisse();
    ou2.setNomTable("caisse");
    liste[1] = new Liste("caisse", ou2, "desccaisse", "idcaisse");
	
    pr.getFormu().changerEnChamp(liste);
    
    pr.getFormu().getChamp("caisse").setLibelleAffiche("Caisse");
	pr.getFormu().getChamp("daty1").setLibelleAffiche("Date debut");
	pr.getFormu().getChamp("daty2").setLibelleAffiche("Date fin");
    pr.getFormu().getChamp("devise").setLibelleAffiche("Devise");
    
    pr.setApres("fin/etatcaisse-liste.jsp");
    String[] colSomme = {"report", "debit", "credit", "disponible"};
    pr.creerObjetPage(libEntete, colSomme);
	
	String[] debutFin=Utilitaire.getDebutFinAnnee();
	
	String idCaisse=request.getParameter("caisse");
	if ((idCaisse==null)||idCaisse.compareTo("")==0) idCaisse="%";
	String devise = request.getParameter("devise");
	if ((devise==null)||devise.compareTo("")==0) devise="%";
	String dateDebut=request.getParameter("daty1");
	if ((dateDebut==null)||dateDebut.compareTo("")==0) dateDebut=debutFin[0];
	String dateFin=request.getParameter("daty2");
	if ((dateFin==null)||dateFin.compareTo("")==0) dateFin=debutFin[1];
	
	EtatdeCaisseDate[] listeT = u.traiteEtatCaisse(dateDebut,dateFin,idCaisse, devise) ;
	
	String[][] datadirect = pr.getTableauRecap().getDataDirecte();
	datadirect[0][2] = Utilitaire.formaterAr(AdminGen.calculSommeDouble(listeT,7)); 
	datadirect[0][3] = Utilitaire.formaterAr(AdminGen.calculSommeDouble(listeT,4));
	datadirect[0][4] = Utilitaire.formaterAr(AdminGen.calculSommeDouble(listeT,5)); 
	datadirect[0][5] = Utilitaire.formaterAr(AdminGen.calculSommeDouble(listeT,6));

	pr.getTableauRecap().setDataDirecte(datadirect);
%>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Etat de caisse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=fin/etatcaisse-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <div class="row">
			<div class="row col-md-12">
				<div class="box box-primary">
					<div class="box-header"><h3 class="box-title" align="center">LISTE</h3></div>
					<div class="box-body table-responsive no-padding">
						<div id="selectnonee">
							<table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="table table-hover">
								<thead>
									<tr class="head">
										<th width="15%" align="center" valign="top" style="background-color:#bed1dd">Caisse</th>
										<th width="5%" align="center" valign="top" style="background-color:#bed1dd">Devise</th>
										<th width="20%" align="center" valign="top" style="background-color:#bed1dd">Report initial</th>
										<th width="20%" align="center" valign="top" style="background-color:#bed1dd">Montant debit</th>
										<th width="20%" align="center" valign="top" style="background-color:#bed1dd">Montant credit</th>
										<th width="20%" align="center" valign="top" style="background-color:#bed1dd">Montant disponible</th>
									</tr>
								</thead>
								<tbody>
								<% for(int i=0;i<listeT.length;i++) { %>
									<tr onmouseover="this.style.backgroundColor='#EAEAEA'" onmouseout="this.style.backgroundColor=''">
										<td width="15%" align="left" ><%=listeT[i].getCaisse() %></td>
										<td width="5%" align="left" ><%=listeT[i].getDevise() %></td>
										<td width="20%" align="right" ><%=Utilitaire.formaterAr(listeT[i].getReport()) %></td>
										<td width="20%" align="right" ><%=Utilitaire.formaterAr(listeT[i].getDebit()) %></td>
										<td width="20%" align="right" ><%=Utilitaire.formaterAr(listeT[i].getCredit()) %></td>
										<td width="20%" align="right" ><%=Utilitaire.formaterAr(listeT[i].getDisponible()) %></td>
									</tr>
								<%}%>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<%
            
            out.println(pr.getBasPage());

        %>
    </section>
</div>