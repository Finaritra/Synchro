<%@page import="mg.cnaps.st.StMvtStockFille"%>
<%@page import="mg.cnaps.budget.BudgetTef"%>
<%@ page import="user.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="bean.*" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="affichage.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <%!
	UserEJB u = null;
	String acte = null;
	String lien = null;
	String bute;
	String nomtable;
	String typeBoutton;
	String cms = null;
    %>
    <%
	try {
	    nomtable = request.getParameter("nomtable");
	    cms = request.getParameter("cms");
	    typeBoutton = request.getParameter("type");
	    lien = (String) session.getValue("lien");
	    u = (UserEJB) session.getAttribute("u");
	    acte = request.getParameter("acte");
	    bute = request.getParameter("bute");
	    Object temp = null;
	    String[] rajoutLien = null;
	    String classe = request.getParameter("classe");
	    ClassMAPTable t = null;
	    String tempRajout = request.getParameter("rajoutLien");
	    String val = "";
	    String idTef = "";

	    String rajoutLie = "";
	    if (tempRajout != null && tempRajout.compareToIgnoreCase("") != 0) {
		rajoutLien = utilitaire.Utilitaire.split(tempRajout, "-");
	    }
	    if (bute == null || bute.compareToIgnoreCase("") == 0) {
		bute = "pub/Pub.jsp";
	    }

	    if (classe == null || classe.compareToIgnoreCase("") == 0) {
		classe = "pub.Montant";
	    }

	    if (typeBoutton == null || typeBoutton.compareToIgnoreCase("") == 0) {
		typeBoutton = "3"; //par defaut modifier
	    }

	    int type = Utilitaire.stringToInt(typeBoutton);
	    if (acte.compareToIgnoreCase("insert") == 0) {
		t = (ClassMAPTable) (Class.forName(classe).newInstance());
		PageInsert p = new PageInsert(t, request);
		ClassMAPTable f = p.getObjectAvecValeur();
		f.setNomTable(nomtable);
                String classeretour = request.getParameter("classeretour");
		String idobjet = request.getParameter("idobjet");
		temp = u.creerTefAvecAttachement(f, idobjet, classeretour);
		if (temp != null) {
		    idTef = ((BudgetTef) temp).getId();
		    bute += "&id=" + idTef;
		    if (cms != null) {
			bute += "&cms=" + cms;
		    }
		    val = temp.toString();
		}
	    }
	    if (acte.compareToIgnoreCase("insertTefIndemnite") == 0) {
		String[] pers_mission = (String[]) session.getAttribute("pers_mission");
		String[] montant = (String[]) session.getAttribute("montant");
		t = (ClassMAPTable) (Class.forName(classe).newInstance());
		PageInsert p = new PageInsert(t, request);
		ClassMAPTable f = p.getObjectAvecValeur();
		f.setNomTable(nomtable);
		temp = u.createObject(f);
		if (temp != null) {
		    idTef = ((BudgetTef) temp).getId();
		    bute += "&id=" + idTef;
		    val = temp.toString();
		}
		u.insertPaiementIndemnite(pers_mission, montant, idTef);
	    }
	    if (acte.compareToIgnoreCase("delete") == 0) {
		String error = "";
		try {
		    if (request.getParameter("confirm") != null) {
			System.out.println("suppression : " + request.getParameter("confirm"));
			t = (ClassMAPTable) (Class.forName(classe).newInstance());
			t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
			t.setNomTable(nomtable);
			u.deleteObject(t);
		    } else {
    %>
    <script language="JavaScript">
	if (confirm("Voulez-vous vraiment supprimer ?")) { // Clic sur OK
	    var url = window.location.href;
	    url = url + "&confirm=oui";
	    window.location.replace(url);
	    //alert("url : "+url);
	}
    </script>
    <%
		}
	    } catch (Exception e) {
		out.println("<script language=\"JavaScript\">alert(" + e.getMessage() + ")</script>");
		System.out.println("erreur suppression" + e.getMessage());
	    }
	}
//            if (acte.compareToIgnoreCase("delete") == 0) {
//                String error = "";
//                try {
//                    t = (ClassMAPTable) (Class.forName(classe).newInstance());
//                    t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
//                    t.setNomTable(nomtable);
//                    u.deleteObject(t);
//
//                } catch (Exception e) {
//                    out.println("<script language=\"JavaScript\">alert(" + e.getMessage() + ")</script>");
//                    System.out.println("erreur suppression" + e.getMessage());
//                }
//            }
	if (acte.compareToIgnoreCase("update") == 0) {
	    t = (ClassMAPTable) (Class.forName(classe).newInstance());
	    Page p = new Page(t, request);
	    ClassMAPTable f = p.getObjectAvecValeur();
	    f.setNomTable(nomtable);
	    u.updateObject(f);
	}
	if (acte.compareToIgnoreCase("annuler") == 0) {
	    t = (ClassMAPTable) (Class.forName(classe).newInstance());
	    t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
	    u.annulerObject(t);
	}
	if (acte.compareToIgnoreCase("valider") == 0) {
	    t = (ClassMAPTable) (Class.forName(classe).newInstance());
	    t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
	    u.validerObject(t);
	}
	if (acte.compareToIgnoreCase("rejeter") == 0) {
	    t = (ClassMAPTable) (Class.forName(classe).newInstance());
	    PageInsert p = new PageInsert(t, request);
	    ClassMAPTable f = p.getObjectAvecValeur();
	    t.setNomTable(nomtable);
	    t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
	    u.rejeterObject(t);
	}
	if (acte.compareToIgnoreCase("cloturer") == 0) {
	    t = (ClassMAPTable) (Class.forName(classe).newInstance());
	    t.setValChamp(t.getAttributIDName(), request.getParameter("id"));
	    u.cloturerObject(t);
	}
    %>
    <script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute + rajoutLie%>&valeur=<%=val%>");</script>
    <%

    } catch (Exception e) {
	e.printStackTrace();
    %>

    <script language="JavaScript"> alert('<%=e.getMessage()%>');
	history.back();</script>
        <%
		return;
	    }
        %>
</html>



