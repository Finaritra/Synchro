<%-- 
    Document   : reservation-fiche
    Created on : 8 oct. 2019, 15:20:20
    Author     : ionyr
--%>

<%@ page import="mg.allosakafo.livraison.*" %>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    try{
    Livreur a = new Livreur();
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));
   
    pc.setTitre("Consultation Livreur");

%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=livraison/livreur-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-warning pull-left"  href="<%=(String) session.getValue("lien") + "?but=livraison/livreur-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>     
                     </div>
                        <br/>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                            <% } catch(Exception e){e.printStackTrace(); }%>