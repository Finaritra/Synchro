<%-- 
    Document   : reservation-fiche
    Created on : 8 oct. 2019, 15:20:20
    Author     : ionyr
--%>

<%@ page import="mg.allosakafo.livraison.*" %>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    try{
    LivraisonApresCommandeComplet a = new LivraisonApresCommandeComplet();
    a.setNomTable("LIVRAISONLIBCOMPLET");
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));
    pc.getChampByName("idcommnademere").setLibelle("Commande m&egrave;re");
    pc.getChampByName("idpoint").setLibelle("Point");
    pc.getChampByName("idlivreur").setLibelle("Livreur");
    
    pc.setTitre("Consultation Livraison");

%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=reservation/reservation-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-warning pull-left"  href="<%=(String) session.getValue("lien") + "?but=livraison/livraison-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=valider&bute=livraison/livraison-fiche.jsp&classe=mg.allosakafo.livraison.LivraisonApresCommande" style="margin-right: 10px">Viser</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=annuler&bute=livraison/livraison-fiche.jsp&classe=mg.allosakafo.livraison.LivraisonApresCommande" style="margin-right: 10px">Annuler</a>
                            
                        </div>
                        <br/>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                            <% } catch(Exception e){e.printStackTrace(); }%>