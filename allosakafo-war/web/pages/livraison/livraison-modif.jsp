<%-- 
    Document   : as-produits-modif
    Created on : 2 oct. 2019, 09:47:29
    Author     : Notiavina
--%>
<%@page import="mg.allosakafo.livraison.Livreur"%>
<%@page import="mg.allosakafo.livraison.LivraisonApresCommande"%>
<%@page import="mg.allosakafo.produits.ProduitsPrix"%>
<%@page import="mg.allosakafo.produits.Produits"%>
<%@ page import="user.*"%>
<%@ page import="utilitaire.*"%>
<%@ page import="bean.*" %>
<%@ page import="affichage.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%
    try{
        LivraisonApresCommande  a = new LivraisonApresCommande();

        PageUpdate pi = new PageUpdate(a, request, (user.UserEJB) session.getValue("u"));
        pi.setLien((String) session.getValue("lien"));
     
        affichage.Champ[] liste = new affichage.Champ[2];
        TypeObjet op = new TypeObjet();
        op.setNomTable("point");
        liste[0] = new Liste("idpoint", op, "VAL", "id");

        Livreur liv = new Livreur();
        liste[1] = new Liste("idlivreur", liv, "nom", "id");
        pi.getFormu().getChamp("idcommnademere").setLibelle("id commande");
        pi.getFormu().getChamp("idcommnademere").setAutre("readonly");
        pi.getFormu().getChamp("etat").setVisible(false);
        pi.getFormu().changerEnChamp(liste);
        pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 class="box-title">Modification produits</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro">
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
         <input name="acte" type="hidden" id="nature" value="update">
        <input name="bute" type="hidden" id="bute" value="livraison/livraison-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.livraison.LivraisonApresCommande">
    </form>
</div>
<%} catch(Exception ex){
    ex.printStackTrace();
}%>
