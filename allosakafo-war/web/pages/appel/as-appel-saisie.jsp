<%-- 
    Document   : as-appel-saisie
    Created on : 30 nov. 2016, 15:29:42
    Author     : Joe
--%>
<%@page import="mg.allosakafo.appel.Appel"%>
<%@page import="user.*"%> 
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*"%>
<%@page import="utilitaire.*"%>
<script>

</script>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    Appel  a = new Appel();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));    
    
    affichage.Liste[] liste = new affichage.Liste[2];
    
    TypeObjet op = new TypeObjet();
    //op.setNomTable("AS_MOTIFAPPEL");
    op.setNomTable("point");
    liste[0] = new Liste("motif", op, "VAL", "id");
    
    TypeObjet ab = new TypeObjet();
    //ab.setNomTable("AS_ABOUTISSEMENT");
    ab.setNomTable("place");
    liste[1] = new Liste("aboutissement", ab, "VAL", "id");
    
    liste[0].setDeroulanteDependante(liste[1],"desce","onblur");
    
    /*TypeObjet or = new TypeObjet();
    or.setNomTable("AS_OPERATEUR");
    liste[2] = new Liste("numeroappele", or, "VAL", "id");*/
    
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("numeroappele").setVisible(false);
    pi.getFormu().getChamp("numeroappelant").setLibelle("Numero appelant");
    pi.getFormu().getChamp("observation").setType("textarea");
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("heure").setDefaut(Utilitaire.heureCouranteHM());
    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Enregistrer appel</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="starticle" id="starticle">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="appel/as-appel-saisie.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.appel.Appel">
    </form>
</div>
<script language="JavaScript">

    var options = $('#motif option');
    for (var co = 0; co < options.size(); co++) {
        if (options[co].value === 'ABT00004') {
            $('#motif option')[co].selected = true;
        } else {
            $('#motif option')[co].selected = false;
        }
    }
    
    var aboutiss = $('#aboutissement option');
    for (var co = 0; co < aboutiss.size(); co++) {
        if (aboutiss[co].value === 'ABT00001') {
            $('#aboutissement option')[co].selected = true;
        } else {
            $('#aboutissement option')[co].selected = false;
        }
    }
</script>