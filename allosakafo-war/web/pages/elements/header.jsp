<%@page import="bean.CGenUtil"%>
<%@page import="mg.cnaps.messagecommunication.MessageLibelle"%>
<%@page import="historique.MapUtilisateur"%>

<%@page import="user.UserEJB"%>
<%
    String lien = (String) session.getValue("lien");
    UserEJB ue = (UserEJB) session.getValue("u");
    MapUtilisateur map = ue.getUser();
    String nomRestau = (String) session.getValue("nomRestau");
%>

<style>
    nav { 
        display: flex;
        flex-direction: row;
        flex: 1 1 100%;
        justify-content: space-between;
        align-items: center;
    }

    nav:before{
      content : none !important;
    }
    nav:after{
      content : none !important;
    }

</style>

<script>
    function verifEditerTef(et,name){
    if(et<11){
            alert('Impossible d\'editer Tef. '+name+' non vis� ');
    }else{
        document.tef.submit();

        }
    }
    function verifLivraisonBC(et){
    if(et<11){
            alert('Impossible d effectuer la livraison du bon de commande);
    }else{
        document.tef.submit();

        }
    }
    function CocherToutCheckBox(ref, name) {
        var form = ref;

        while (form.parentNode && form.nodeName.toLowerCase() != 'form') {
            form = form.parentNode;
        }

        var elements = form.getElementsByTagName('input');

        for (var i = 0; i < elements.length; i++) {
            if (elements[i].type == 'checkbox' && elements[i].name == name) {
                elements[i].checked = ref.checked;
            }
        }
    }

</script>
<header class="main-header" style="position: fixed; left: 0; right: 0;">
    <!-- Logo -->
    <a href="<%=lien%>?but=travailleur/travailleur-liste.jsp" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">CC</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>P</b>h&ocirc; <b>R</b>ESTO</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div style="font-size: 25px;color: white;"><%=nomRestau%></div>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="loadMessageHeader()">
                        <i class="fa fa-envelope-o"></i>
                        <%if (0 == 0) {%>
                        <span class="label label-success" id="nb-inbox"><%=0%></span>
                        <%} else {%>
                        <span class="label label-success" id="nb-inbox"></span>
                        <%}%>

                    </a>
                    <ul class="dropdown-menu" id="message-listcontent-header">
                        <li class="header">Vous avez <span id="inbox"><%=0%></span> message(s)
                            <span class="btn btn-default pull-right" style="position: absolute; top: 0; right: 5px;" data-toggle="modal" data-target="#modalSendMessageTo"><i class="fa fa-plus"></i></span></li>
                        <!--                        <a title="Nouveau message">
                                                    <i class="fa fa-plus-square-o"></i>
                                                </a>-->
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu" id="message-list-header">

                            </ul>
                        </li>
                        <li class="footer"><a href="#">Tous les Messages</a></li>
                    </ul>
                </li>
                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu">
                    <a href="<%= lien %>?but=notification/notification-liste.jsp" class="dropdown-toggle">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-danger"><% if(0 >0 ) { %> <%=0%> <% }%></span>
                    </a>
                    <!--<ul class="dropdown-menu">
                        <li class="header">Vous avez 2 notifications</li>   
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> 2 nouveaux dossiers
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> 1 dossier termin&eacute;
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">Tout voir</a></li>
                    </ul>-->
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><%=map.getLoginuser()%></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="${pageContext.request.contextPath}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                            <p>
                                <%=map.getLoginuser() + "-" + map.getIdrole()%>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<%=lien%>?but=utilisateur/utilisateur-modif.jsp&id=<%=map.getRefuser()%>" class="btn btn-default btn-flat">Modifier Profil</a>
                            </div>
                            <div class="pull-right">
                                <a href="deconnexion.jsp" class="btn btn-default btn-flat">D&eacute;connexion</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="modal fade" id="modalSendMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="message-chat-title"></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="message-chat-content clearfix" id="message-chat-content">
                    
                </div>
                <br/>
                <form>
                    <textarea id="messagefrom" onkeypress="keypressedsendMessage(this, 1)" class="form-control" rows="3" placeholder="Votre message ici" ></textarea>
                    <br/><br/>
                    <input type="button" class="btn btn-primary pull-right" style="margin-left: 5px;" onclick="keypressedsendMessage(this, 2)" value="Envoyer"/>
                    <input type="reset" class="btn btn-danger pull-right" value="Annuler"/>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalSendMessageTo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <%
                    MapUtilisateur[] utilisateurs = (MapUtilisateur[]) CGenUtil.rechercher(new MapUtilisateur(), null, null, " AND REFUSER <> '" + map.getRefuser() + "'");
                    for (MapUtilisateur utilisateur : utilisateurs) {%>
                <div class="radio">
                    <label>
                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="<%=utilisateur.getRefuser()%>">
                        <%=utilisateur.getNomuser()%>
                    </label>
                </div>
                <%}
                %>

            </div>
            <div class="modal-body clearfix">
                <form>
                    <textarea id="msgelement" class="form-control" rows="3" placeholder="Votre message ici" ></textarea>
                    <br/><br/>
                    <input type="button" class="btn btn-primary pull-right" style="margin-left: 5px;" onclick="keypressedsendMessage(this, 3)" value="Envoyer"/>
                    <input type="reset" class="btn btn-danger pull-right" value="Annuler"/>
                </form>
            </div>
        </div>
    </div>
</div>