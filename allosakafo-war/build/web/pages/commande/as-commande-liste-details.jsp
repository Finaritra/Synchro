<%-- 
    Document   : as-commande-liste
    Created on : 1 d�c. 2016, 09:52:16
    Author     : Joe
--%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>

<%
    try {

        CommandeClientDetailsLibelleEtat p = new CommandeClientDetailsLibelleEtat();
        String nomTable = "AS_DETAILS_COMMANDE_DESC_ETAT";

        if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0) {
            nomTable = request.getParameter("table");
        }
        p.setNomTable(nomTable);

        String listeCrt[] = {"id", "idmere", "produit", "observation", "idproduit" };
        String listeInt[] = {"quantite", "pu", "remise","etat"};
        String libEntete[] = {"id", "idmere", "produit", "observation", "idproduit", "quantite", "pu", "remise","statue"};
        
        PageRecherche pr = new PageRecherche(p, request, listeCrt, listeInt, 2, libEntete, 9);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        //pr.getChampByName("status").setLibelle("Etat");
        pr.setApres("commande/as-commande-liste-details.jsp");
        String[] colSomme = {};
        pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande d�tail</h1>
    </section>
    <section class="content">
    <form action="<%=pr.getLien()%>?but=commande/as-commande-liste-details.jsp" method="post" name="incident" id="incident">
        <%
            out.println(pr.getFormu().getHtmlEnsemble());
        %>
    </form>
    <form action="<%=pr.getLien()%>?but=apresCommandeFait.jsp" method="post" name="incident" id="incident">
        <%  
            String lienTableau[] = {pr.getLien() + "?but=commande/as-commande-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id", "idmere", "produit", "observation", "idproduit", "Quantite", "Prix unitaire", "Remise","Etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            pr.getTableau().setNameBoutton("Fait");
            out.println(pr.getTableau().getHtmlWithCheckbox());
        %>
    </form>
    <% out.println(pr.getBasPage()); %>
    </section>
</div>

<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>