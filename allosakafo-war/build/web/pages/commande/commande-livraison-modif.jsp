
<%@page import="mg.allosakafo.livraison.CommandeLivraison"%>
<%@page import="mg.allosakafo.livraison.Livreur"%>
<%@page import="user.*"%>
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="mg.allosakafo.commande.*" %>
<% try{
     CommandeLivraison commande=new CommandeLivraison();
    commande.setIdclient(request.getParameter("idclient"));
    PageUpdate pi = new PageUpdate(commande, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    
    affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet op = new TypeObjet();
    op.setNomTable("pointtrie");
    liste[0] = new Liste("idpoint", op, "VAL", "id");
    pi.getFormu().changerEnChamp(liste);
    pi.getFormu().getChamp("idclient").setLibelle("Commande m&egrave;re");
     pi.getFormu().getChamp("idclient").setAutre("readonly");
    pi.getFormu().getChamp("idpoint").setLibelle("Point");
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("etat").setVisible(false);

    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Modifier commande Livraison</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="reservation-saisie" id="reservation-saisie">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="update">
    <input name="bute" type="hidden" id="bute" value="commande/commande-livraison-fiche.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.livraison.CommandeLivraison">
    </form>
    
</div>
    <%} catch(Exception e){
        e.printStackTrace();
}%>

