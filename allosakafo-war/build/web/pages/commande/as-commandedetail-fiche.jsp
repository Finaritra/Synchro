<%-- 
    Document   : as-commandedetail-fiche
    Created on : 30 sept. 2019, 14:57:14
    Author     : pro
--%>

<%@page import="mg.allosakafo.commande.CommandeClientDetailsTypeP"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.commande.Paiement"%>
<%
    CommandeClientDetailsTypeP lv = new CommandeClientDetailsTypeP();
    lv.setNomTable("vue_cmd_client_details_typeP");
    PageConsulte pc = pc = new PageConsulte(lv, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setTitre("Commande detail");

%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=commande/as-commande-liste-etat.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=commande/as-commandedetail-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>
