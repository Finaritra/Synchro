
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.ded.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="lc.Direction"%>
<%@page import="service.AlloSakafoService"%>

<%      
    try {
    /*private String id , idobjet,motif ;
    private double montant;
    private Date dateliv;
    private int etat;*/
    Annulation annulation = new Annulation();
    if(request.getParameter("nomtable")!=null){
        annulation.setNomTable(request.getParameter("nomtable"));
    }
    String listeCrt[] = {"id","idobjet","motif", "daty","montant" , "etat"};
    String listeInt[] = {"daty" ,"montant"};
    String libEntete[] = {"id","idobjet","motif", "daty","montant" , "etat"};

    
    PageRecherche pr = new PageRecherche(annulation, request, listeCrt, listeInt, 2, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));

    pr.setApres("commande/annulation-op-liste.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/annulation-op-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
         <%  
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id","Idobjet","Motif", "Date","Montant" , "Etat"};
            String lienTableau[] = {pr.getLien() + "?but=ded/annution-op-fiche.jsp&nomtable="+annulation.getNomTable() };
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
        
    </section>
</div>
        
<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>        