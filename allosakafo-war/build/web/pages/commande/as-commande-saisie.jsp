<%-- 
    Document   : as-commande-saisie
    Created on : 1 d�c. 2016, 09:51:55
    Author     : Joe
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.produits.ProduitsLibelle"%>
<%@page import="mg.allosakafo.tiers.Responsable"%>
<%@page import="mg.allosakafo.appel.Appel"%>
<%@page import="mg.allosakafo.secteur.Quartier"%>

<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    CommandeClient da = new CommandeClient();
    PageInsert pi = new PageInsert(da, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");

    affichage.Champ[] liste = new affichage.Champ[5];

    TypeObjet d = new TypeObjet();
    d.setNomTable("as_typecommande");
    liste[0] = new Liste("typecommande", d, "val", "id");

    Responsable dd = new Responsable();
    dd.setNomTable("as_responsable");
    liste[1] = new Liste("responsable", dd, "nom", "id");

	TypeObjet d1 = new TypeObjet();
    d1.setNomTable("as_secteur");
    liste[2] = new Liste("secteur", d1, "val", "id");
    
    TypeObjet c= new TypeObjet();
    c.setNomTable("point");
    liste[3] = new Liste("point", c, "val", "id");
    
    TypeObjet v= new TypeObjet();
    v.setNomTable("vente");
    liste[4] = new Liste("vente", v, "val", "id");
	
    pi.getFormu().changerEnChamp(liste);

    pi.getFormu().getChamp("etat").setVisible(false);
    pi.getFormu().getChamp("datesaisie").setLibelle("date de saisie");
    pi.getFormu().getChamp("datesaisie").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("datecommande").setLibelle("Date de commande");
    pi.getFormu().getChamp("datecommande").setDefaut(Utilitaire.dateDuJour());
	pi.getFormu().getChamp("secteur").setLibelle("Secteur");
	pi.getFormu().getChamp("secteur").setAutre("readonly='true'");
    pi.getFormu().getChamp("responsable").setLibelle("R�sponsable");
    pi.getFormu().getChamp("numcommande").setLibelle("N� commande");
    pi.getFormu().getChamp("numcommande").setDefaut(Utilitaire.getMaxSeq("getSeqNumCommande")+"");
    pi.getFormu().getChamp("typecommande").setLibelle("Type commande");
    pi.getFormu().getChamp("dateliv").setLibelle("Date de livraison");
    pi.getFormu().getChamp("dateliv").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("adresseliv").setLibelle("Adresse de livraison");
    pi.getFormu().getChamp("heureliv").setLibelle("Heure de livraison");
    pi.getFormu().getChamp("heureliv").setDefaut(Utilitaire.heureCouranteHM(1));
    pi.getFormu().getChamp("distance").setLibelle("Distance");
    
	pi.getFormu().getChamp("remarque").setType("textarea");
	
	pi.getFormu().getChamp("observation").setType("textarea");
	
	pi.getFormu().getChamp("client").setAutre(" onblur='searchClient()'");
	
	pi.getFormu().getChamp("adresseliv").setAutre(" onchange='searchLieu()'");
	
	pi.getFormu().getChamp("quartier").setPageAppel("choix/listeQuartierPrixChoix.jsp", "quartier;quartierlibelle;secteur;distance");
	pi.getFormu().getChamp("quartier").setAutre(" readonly='true'");
	
    if (request.getParameter("id") != null) {
        Appel ap = new Appel();
        Appel[] lst = (Appel[]) CGenUtil.rechercher(ap, null, null, " and ID = '" + request.getParameter("id").trim() + "'");
        if (lst != null && lst.length != 0) {
            pi.getFormu().getChamp("client").setDefaut(lst[0].getNumeroappelant());
        }
    }
    String[] ord = {"datesaisie", "numcommande", "responsable", "client", "typecommande", "datecommande"};
    pi.setOrdre(ord);
   
    pi.preparerDataFormu();
%>
<script>
	function searchClient(){
		var num = document.getElementById("client").value;
		$.ajax({
			url : 'http://localhost:8080/allosakafo-war/pages/commande/ajaxCommande.jsp?acte=searchClient&numero='+num,
			type : 'GET',
			dataType : 'html',
			success : function(response){
				var douteux = response.split("<p id='douteux'>")[1].split("</p>")[0];
				if (douteux == 'true'){
					$("#id01").modal();
				}
				var text = response.split("<p id='client'>")[1].split("</p>")[0];
				var adresseL = response.split("<p id='adresseliv'>")[1].split("</p>")[0];
				document.getElementById("remarque").value = text; 
				document.getElementById("adresseliv").value = adresseL; 
				searchLieu();
			}
		});
		
	}
	function searchLieu(){
		var adresse = document.getElementById("adresseliv").value;
		$.ajax({
			url : 'http://localhost:8080/allosakafo-war/pages/commande/ajaxCommande.jsp?acte=searchLieu&adresse='+adresse,
			type : 'GET',
			dataType : 'html',
			success : function(response){
				var quartier = response.split("<p id='quartier'>")[1].split("</p>")[0];
				var secteur = response.split("<p id='secteur'>")[1].split("</p>")[0];
				var montant = response.split("<p id='montant'>")[1].split("</p>")[0];
				document.getElementById("secteur").value = secteur;
				document.getElementById("distance").value = montant;
				document.getElementById("quartierlibelle").value = quartier;				
			}
		});
	}
	
</script>

<div class="content-wrapper">
	<div id="id01" class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
		
		  <!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Alerte</h4>
				</div>
			<div class="modal-body">
				<p>Client douteux</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		  </div>
		</div>
	</div>
    <h1 class="box-title">Enregistrer commande</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertCommande();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <div class="box">
                        <div class="box-title with-border">
                            <h1 class="box-title">D&eacute;tails</h1>
                        </div>
                        <div class="box-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th style="background-color:#bed1dd">ID</th>
                                        <th style="background-color:#bed1dd">Article</th>
                                        <th style="background-color:#bed1dd">Quantit&eacute;</th>
                                        <th style="background-color:#bed1dd">Prix Unitaire</th>
                                        <th style="background-color:#bed1dd">Montant</th>
                                        <th style="background-color:#bed1dd">Remarque</th>
                                    </tr></thead>
                                <tbody>
                                    <%
                                        ProduitsLibelle p = new ProduitsLibelle();
                                        ProduitsLibelle[] listef = (ProduitsLibelle[]) CGenUtil.rechercher(p, null, null, " order by nom asc");

                                        for (int i = 0; i < listef.length; i++) {
                                    %>
                                    <input type="hidden" class="form form-control" name="nb" id="nb" value="<%=listef.length%>">
                                    <tr>
                                        <td><input type="hidden" class="form form-control" name="id<%=i%>" id="id<%=i%>" value="<%=listef[i].getId()%>"><%=listef[i].getId()%></td>
                                        <td><%=listef[i].getNom()%></td>
                                        <td><input type="text" class="form form-control" name="quantite<%=i%>" id="quantite<%=i%>" value="0" onblur="calculerMontant(<%=i%>)"></td>
                                        <td><input type="text" class="form form-control" name="pu<%=i%>" id="pu<%=i%>" value="<%=listef[i].getPu()%>" onblur="calculerMontant(<%=i%>)"></td>
                                        <td><input type="text" class="form form-control" name="montant<%=i%>" id="montant<%=i%>" value="0" readonly="readonly"></td>
                                        <td><input type="text" class="form form-control" name="remarque<%=i%>" id="remarque<%=i%>"></td>
                                    </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input name="acte" type="hidden" id="nature" value="insertcommande">
        <input name="bute" type="hidden" id="bute" value="commande/as-commande-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.commande.CommandeClient">
        <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;">Enregistrer</button>
        <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;">R&eacute;initialiser</button>
    </form>
</div>
<script language="JavaScript">

    var options = $('#typecommande option');
    for (var co = 0; co < options.size(); co++) {
        if (options[co].value === 'TPC00002') {
            $('#typecommande option')[co].selected = true;
        } else {
            $('#typecommande option')[co].selected = false;
        }

    }
    
    function calculerMontant(indice) {
        var quantite, pu, montant;
        quantite = parseFloat($('#quantite' + indice).val());
        pu = parseFloat($('#pu' + indice).val());
        if (!isNaN(quantite) && !isNaN(pu)) {
            montant = quantite * pu;
            $('#montant' + indice).val(montant.toFixed(2));
        } else {
            $('#montant' + indice).val('');
        }
    }

</script>