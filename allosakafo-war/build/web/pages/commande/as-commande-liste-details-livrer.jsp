<%-- 
    Document   : as-commande-liste-details-fait
    Created on : 24 sept. 2019, 14:40:48
    Author     : Notiavina
--%>

<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="lc.Direction"%>


<%
    

        CommandeClientDetailsTypeP p = new CommandeClientDetailsTypeP();
        String nomTable = "VUE_CMD_CLT_TYPEP_REST_NOT";

        if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0) {
            nomTable = request.getParameter("table");
        }
        p.setNomTable(nomTable);

        String listeCrt[] = {"id", "produit","nomtable", "datecommande", "restaurant"};
        String listeInt[] = {"quantite", "pu", "remise","etat", "datecommande"};
        String libEntete[] = {"id", "quantite","produit","acco_sauce","nomtable", "typeproduit","datecommande", "heureliv", "pu","montant",  "etat"};
        
        PageRecherche pr = new PageRecherche(p, request, listeCrt, listeInt, 2, libEntete, libEntete.length);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));
        
//        affichage.Champ[] listeDir = new affichage.Champ[1];
//        Direction dir = new Direction();
//        dir.setNomTable("restaurant");
//        listeDir[0] = new Liste("restaurant",dir,"libelledir","iddir");
//        pr.getFormu().changerEnChamp(listeDir);
        
        pr.getFormu().getChamp("datecommande1").setDefaut(utilitaire.Utilitaire.dateDuJour());
        pr.getFormu().getChamp("datecommande2").setDefaut(utilitaire.Utilitaire.dateDuJour());
        pr.getFormu().getChamp("restaurant").setDefaut((String)session.getAttribute("restaurant"));

        String apreswhere = "";
        if(request.getParameter("datecommande1") == null || request.getParameter("datecommande2") == null){
            apreswhere = "and datecommande = '"+utilitaire.Utilitaire.dateDuJour()+"'";
        }
//        if(request.getParameter("restaurant") == null){
//            apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
//        }
        pr.setAWhere(apreswhere);
    
        //pr.setAWhere(" order by id desc");
        pr.setApres("commande/as-commande-liste-details-livrer.jsp");
        String[] colSomme = {"montant"};
        pr.setNpp(500);
        pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande d�tail</h1>
    </section>
    <section class="content">
    <form action="<%=pr.getLien()%>?but=commande/as-commande-liste-details-livrer.jsp" method="post" name="incident" id="incident">
        <%
            out.println(pr.getFormu().getHtmlEnsemble());
        %>  
        <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("VUE_CMD_CLT_TYPEP_REST_NOT") == 0) {%>
                        <option value="VUE_CMD_CLT_TYPEP_REST_NOT" selected>Livr&eacute;</option>
                        <% } else { %>
                        <option value="VUE_CMD_CLT_TYPEP_REST_NOT">Livr&eacute;</option>
                        <% } %>
                        
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("VUE_CMD_CLT_DTLS_TYPEP_PAYER") == 0) {%>
                        <option value="VUE_CMD_CLT_DTLS_TYPEP_PAYER" selected>Pay&eacute;</option>
                        <% } else { %>
                        <option value="VUE_CMD_CLT_DTLS_TYPEP_PAYER">Pay&eacute;</option>
                        <% } %>
				
                    </select>
                </div>
            </div>
    </form>
    <form action="<%=pr.getLien()%>?but=modifierEtatMultiple.jsp" method="post" name="formu" id="formu">
        <%  
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
         <input type="hidden" name="acte" id="acte"/>
        <%
            String libEnteteAffiche[] = {"Id", "Quantite", "Produit", "Sauce accompagnement", "Table","Type", "Date", "Heure",  "Prix unitaire","Montant", "Etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            pr.getTableau().setNameBoutton("Payer");
            pr.getTableau().setNameActe("payer");
            pr.getTableau().setNameBoutton2("Annuler");
            pr.getTableau().setNameActe2("annuler");
            out.println(pr.getTableau().getHtmlWithCheckbox());
        %>
        <button onclick="changeraction()" class="btn btn-success pull-right" style="margin-right: 25px;" tabindex="71">Facturer</button>
        <input type="hidden" name="bute" value="commande/as-commande-liste-details-livrer.jsp"/>
    </form>
    <% out.println(pr.getBasPage()); %>
    </section>
</div>
<script text="JavaScript">
    function changeraction(){
        document.formu.action="<%=pr.getLien()%>?but=facture/saisieclientfacture.jsp";
    }
</script>
