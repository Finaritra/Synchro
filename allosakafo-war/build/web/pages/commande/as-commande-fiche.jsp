<%-- 
    Document   : as-commande-fiche
    Created on : 1 d�c. 2016, 09:52:34
    Author     : Joe
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.livraison.CommandeLivraison"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="mg.allosakafo.commande.CommandeClient"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetails"%>
<%@page import="mg.allosakafo.commande.CommandeClientDetailsLibelle"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%
    CommandeClient dma = new CommandeClient();
    double total = 0.0;
    dma.setNomTable("as_commandeclient_libelle");
    PageConsulte pc = new PageConsulte(dma, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin

	pc.getChampByName("numcommande").setLibelle("N� commande");
	pc.getChampByName("datesaisie").setVisible(false);
	
        pc.getChampByName("prenom").setLibelle("Caisse");
	pc.getChampByName("typecommande").setLibelle("Type de commande");
	pc.getChampByName("adresseliv").setLibelle("Adresse de livraison");
	pc.getChampByName("heureliv").setLibelle("Heure de livraison");
        pc.getChampByName("responsable").setLibelle("Contact");
	pc.getChampByName("dateliv").setVisible(false);
	pc.getChampByName("remarque").setVisible(false);
	pc.getChampByName("vente").setVisible(false);
	pc.getChampByName("distance").setVisible(false);
	pc.getChampByName("adresseliv").setVisible(false);
	pc.getChampByName("typecommande").setVisible(false);
	pc.getChampByName("numcommande").setVisible(false);
        pc.getChampByName("etat").setVisible(false);
        
	
    pc.setTitre("Fiche commande");
    

    CommandeClient bondecommande = (CommandeClient) pc.getBase();
    String infosClient = bondecommande.getResponsable()+"-"+bondecommande.getClient();
    int[] tabEtat = new int[5];
    tabEtat[0] = ConstanteEtat.getEtatCloture();
    tabEtat[1] = ConstanteEtat.getEtatFait();
    tabEtat[2] = ConstanteEtat.getEtatValider();
    tabEtat[3] = ConstanteEtat.getEtatLivraison();
    tabEtat[4] = ConstanteEtat.getEtatPaye();
    CommandeLivraison cl=bondecommande.getCl(null);
    Caisse c=new Caisse();
    Caisse[] listeCaisse=c.getAllCaisse(null);
    CommandeClient commande=(CommandeClient)pc.getBase();
%>

<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=commande/as-commande-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        Date de livraison : <B> <%=Utilitaire.format(cl.getDaty())%> </B> Lieu de livraison <B><%=cl.getAdresse()+ "</B> --- point : "+cl.getIdpoint() %>
                        <div class="box-footer">
                            <!--<% /*if (bondecommande.getEtat() > ConstanteEtat.getEtatAnnuler() && bondecommande.getEtat() < ConstanteEtat.getEtatPaye()){*/ %>
                            <a class="btn btn-primary pull-right"  href="#" onclick="pagePopUp('commande/fiche-commande-chef.jsp?id=<%/*=request.getParameter("id")*/%>')" style="margin-right: 10px">Afficher</a>
                            <% //} %>
                            <% //if (bondecommande.getEtat() == ConstanteEtat.getEtatCreer()){ %>
                                    <a class="btn btn-success pull-right"  href="<%//=(String) session.getValue("lien") + "?but=commande/as-commandefille-saisie.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Ajouter produits</a>
                                    <a class="btn btn-warning pull-right"  href="<%//=(String) session.getValue("lien") + "?but=commande/as-commande-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                                    <a class="btn btn-danger pull-right"  href="<%//=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annuler&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
                                    <a class="btn btn-primary pull-right"  href="<%//=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=fait&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Pret a livrer</a>
                            <% //} 
                                    //else if (bondecommande.getEtat() == ConstanteEtat.getEtatFait()){ %>
                                    <a class="btn btn-warning pull-right"  href="<%//=(String) session.getValue("lien") + "?but=commande/as-commande-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                                    <a class="btn btn-danger pull-right"  href="<%//=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annuler&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>
                                    <a class="btn btn-danger pull-right"  href="<%//=(String) session.getValue("lien") + "?but=commande/as-livraison-saisie.jsp&idCommande=" + request.getParameter("id")%>" style="margin-right: 10px">Livrer</a>
                            <% //} 
                            //else if (bondecommande.getEtat() == ConstanteEtat.getEtatLivraison()){ %>
                                    <a class="btn btn-success pull-right"  href="<%//=(String) session.getValue("lien") + "?but=commande/as-paiement-saisie.jsp&idCommande=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Payer</a>
                                    <a class="btn btn-success pull-right"  href="<%//=(String) session.getValue("lien") + "?but=facture/factureclient-saisie.jsp&idCommande=" + request.getParameter("id")%>" target="_blank" style="margin-right: 10px">Editer Facture Client</a>
                                    <a class="btn btn-danger pull-right"  href="<%//=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annuler&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>

                            <% //} %>-->
                            <a href="<%=(String) session.getValue("lien")%>?but=commande/as-commande-modif.jsp&id=<%=request.getParameter("id")%>"class="btn btn-primary pull-left">Modifier</a>
                             <a href="<%=(String) session.getValue("lien")%>?but=commande/as-commande-modif-date-heure.jsp&id=<%=request.getParameter("id")%>" class="btn btn-warning pull-left">Modifier date et heure</a>                            
                    <form action="<%=(String) session.getValue("lien")%>?but=commande/apresPayer.jsp&id=<%=request.getParameter("id")%>"  method="post" > 
                             Caisse : 
                            <select name="idCaisse" class="champ" id="caisse" >
                            <% for(Caisse caisse:listeCaisse){%>
                               <option value="<%=caisse.getIdcaisse()%>" <%if (caisse.getDesccaisse().compareToIgnoreCase(AlloSakafoService.getCaisseDefaut())==0) out.print("selected"); %>><%=caisse.getDesccaisse()%></option>
                            <% } %>
                             </select>
			 <button class="btn btn-success" type="submit">Payer</button>
                     </form>  
                             <a href="<%=(String) session.getValue("lien")%>?but=commande/commande-livraison-modif.jsp&idclient=<%=request.getParameter("id")%>" class="btn btn-warning pull-right">Modifier Infos livraison</a>
                        
                             <%--  <%    
                                switch(bondecommande.getEtat()){
                                    case 9 : %>
                                         <a class="btn btn-warning pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=payer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Payer</a>
                                        <a class="btn btn-primary pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=livrer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Livrer</a>
                                        <a class="btn btn-success pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Valider</a>
                                        <a class="btn btn-primary pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=fait&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Pret a livrer</a>
                                        <a class="btn btn-warning pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=cloturer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Cloturer</a>
                                <%  break;
                                    case 10 : %>
                                        <a class="btn btn-warning pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=payer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Payer</a>
                                        <a class="btn btn-primary pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=livrer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Livrer</a>
                                        <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Valider</a>
                                        <a class="btn btn-primary pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=fait&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Pret a livrer</a>
                                        <a class="btn btn-warning pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=cloturer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Cloturer</a>
                                <%  break;
                                    case 11 : %>
                                        <a class="btn btn-warning pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=payer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Payer</a>
                                        <a class="btn btn-primary pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=livrer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Livrer</a>
                                        <a class="btn btn-success pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Valider</a>
                                        <a class="btn btn-primary pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=fait&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Pret a livrer</a>
                                        <a class="btn btn-warning pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=cloturer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Cloturer</a>
                                <%  break;
                                    case 20 : %>
                                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=payer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Payer</a>
                                        <a class="btn btn-primary pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=livrer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Livrer</a>
                                        <a class="btn btn-success pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Valider</a>
                                        <a class="btn btn-primary pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=fait&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Pret a livrer</a>
                                        <a class="btn btn-warning pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=cloturer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Cloturer</a>
                                <%  break;
                                    default : %>
                                        <a class="btn btn-warning pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=payer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Payer</a>
                                        <a class="btn btn-primary pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=livrer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Livrer</a>
                                        <a class="btn btn-success pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Valider</a>
                                        <a class="btn btn-primary pull-right" disabled href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=fait&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Pret a livrer</a>
                                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=cloturer&bute=commande/as-commande-fiche.jsp&classe=mg.allosakafo.commande.CommandeClient&id=" + request.getParameter("id")%>" style="margin-right: 10px">Cloturer</a>
                                <%  break;
                                }
                            %>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <% /*out.println(pc.getBasPage());*/ %>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">D&eacute;tails commande</h1>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Produits</th>
                                    <th>Quantit&eacute;</th>
                                    <th>P.U</th>
                                    <th>Remise</th>
                                    <th>Montant</th>
                                    <th>Revient</th>
                                    <th>Observation</th>
                                </tr>
                            </thead>

                            <tbody>
                                <%
                                    CommandeClientDetailsLibelle p = new CommandeClientDetailsLibelle();
                                    //p.setNomTable("as_details_commande");
                                    p.setNomTable("AS_DETAILS_COMMANDE_ETAT_TPPRD");
                                    CommandeClientDetailsLibelle[] liste = (CommandeClientDetailsLibelle[]) CGenUtil.rechercher(p, null, null, " and idmere = '" + request.getParameter("id") + "' and etat >0");

                                    //private String id, idmere, produit, observation; private double quantite, pu, remise;
                                    String style="";
                                    for (int i = 0; i < liste.length; i++) {
                                        style="";
                                        double remise = ((liste[i].getQuantite() * liste[i].getPu()) * liste[i].getRemise()) / 100;
                                        if(liste[i].estBoisson()){
                                            style="color:red";
                                        }
                                %>
                                              <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" style="<%=style%>" onmouseout="this.style.backgroundColor = ''">
                                                <td  align="center"><%=liste[i].getId()%></td>
                                                <td  align="center"><a href="<%=(String) session.getValue("lien")%>?but=produits/as-produits-fiche.jsp&id=<%=liste[i].getIdproduit() %>"><%=liste[i].getProduit()%></a></td>
                                                <td  align="center"><%=Utilitaire.formaterAr(liste[i].getQuantite())%></td>
                                                <td  align="right"><%=Utilitaire.formaterAr(liste[i].getPu())%></td>
                                                <td  align="right"><%=Utilitaire.formaterAr(remise)%></td>
                                                <td  align="right"><%=Utilitaire.formaterAr((liste[i].getQuantite() * liste[i].getPu()) - remise)%></td>
                                                <td  align="right"><%=Utilitaire.formaterAr(liste[i].getRevient(pc.getUtilisateur().getUser().getIdrole())) %></td>
                                                <td  align="center"><%=Utilitaire.champNull(ConstanteEtat.etatToChaineLivraison(liste[i].getEtatChiffre()) )%></td>
                                            </tr>
                                        <% 
                                        total += ((liste[i].getQuantite() * liste[i].getPu()) - remise);
                                        }
                                    %>

                                <tr onmouseover="this.style.backgroundColor = '#EAEAEA'" onmouseout="this.style.backgroundColor = ''">
                                    <td  align="center"></td>
                                    <td  align="center"></td>
                                    <td  align="right"></td>
                                    <td  align="center"></td>
                                    <td  align="center">Total</td>
                                    <td  align="right"><%=Utilitaire.formaterAr(total)%></td>
                                    <td  align="right"><%=Utilitaire.formaterAr(AdminGen.calculSommeDouble(liste, "revient")) %></td>
                                    <td  align="center"></td>
                                </tr>
                            </tbody>
                        </table>
                            <form action="<%=(String) session.getValue("lien") + "?but=livraison/livraison-saisie.jsp"%>" method="post">  
                                <input type="hidden"  name="idDetail" id="idDetail" value="<%= Utilitaire.tabToString(commande.getIdDetailsCommandeSansLivraison() , "", ";") %>">
                                <input type="submit" class="btn btn-warning pull-right"style="margin-right: 10px" value="Livrer">
                            </form>        
                            <form action="<%=(String) session.getValue("lien") + "?but=facture/saisieclientfacture.jsp"%>" method="post">  
                                Nom du client : 
                                <input type="text"  name="client" id="client" value="<%=infosClient  %>">
                                <input type="hidden"  name="id" id="id" value="<%=request.getParameter("id")%>">
                                <input type="submit" class="btn btn-warning pull-right"style="margin-right: 10px" value="Facture">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
