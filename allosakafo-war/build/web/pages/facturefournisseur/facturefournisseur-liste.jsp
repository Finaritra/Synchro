<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.facturefournisseur.*"%>
<%@page import="affichage.PageRecherche"%>

<% 
    try{
    EtatFacture lv = new EtatFacture();
    //lv.setNomTable("EtatFacture");
     lv.setNomTable("ETATFACTUREMONTANT");
    if (request.getParameter("table") != null ){
        lv.setNomTable(request.getParameter("table"));
    }
    
    String listeCrt[] = {"id", "daty", "idFournisseur","idDevise","datyecheance","identite"};
    String listeInt[] = {"daty", "datyecheance"};
    String libEntete[] = {"id", "daty", "idFournisseur", "idDevise", "datyecheance", "montantTTC","montantPaye","reste", "idTVA","entite","etat"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setDefaut(Utilitaire.dateDuJour());
    pr.getFormu().getChamp("daty2").setDefaut(Utilitaire.dateDuJour());
      
    affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet typeObjet = new TypeObjet();
    typeObjet.setNomTable("point");
    liste[0] = new Liste("identite",typeObjet, "val", "id");
    pr.getFormu().changerEnChamp(liste);
    
    pr.getFormu().getChamp("identite").setDefaut(session.getAttribute("restaurant").toString());    
    pr.getFormu().getChamp("identite").setLibelle("Entite");

   /* affichage.Champ[] liste = new affichage.Champ[1];
	
    TypeObjet ou = new TypeObjet();
    ou.setNomTable("Devise");
    liste[0] = new Liste("idDevise", ou, "VAL", "VAL");

    pr.getFormu().changerEnChamp(liste);
    */
    /*
    pr.getFormu().getChamp("groupee").setLibelleAffiche("Groupe");
    pr.getFormu().getChamp("typearticle").setLibelleAffiche("Type d'article");
    */
    pr.setApres("facturefournisseur/facturefournisseur-liste.jsp");
    String[] colSomme = {"montantTTC", "montantPaye","reste","idTVA"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste Facture Fournisseur</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=facturefournisseur/facturefournisseur-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("EtatFacture") == 0) {%>    
                        <option value="ETATFACTUREMONTANT" selected>Tous</option>
                        <% } else { %>
                        <option value="ETATFACTUREMONTANT" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("EtatFactureCreer") == 0) {%>    
                        <option value="EtatFactureCreerMONTANT" selected>Cr&eacute;e</option>
                        <% } else { %>
                        <option value="EtatFactureCreerMONTANT" >Cr&eacute;e</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("EtatFactureValider") == 0) {%>    
                        <option value="EtatFactureValiderMONTANT" selected>Valider</option>
                        <% } else { %>
                        <option value="EtatFactureValiderMONTANT" >Valider</option>
                        <% } %>
                      
                    </select>
                </div>
            </div>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=facturefournisseur/facturefournisseur-fiche.jsp", pr.getLien()};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Date", "Fournisseur", "Devise", "Echeance", "Montant TTC","montantPaye","reste", "Montant TVA","Entite","etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
    <%}catch(Exception e){e.printStackTrace();}%>