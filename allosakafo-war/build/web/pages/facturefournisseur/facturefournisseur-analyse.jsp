<%@page import="mg.allosakafo.facturefournisseur.*"%>
<%@ page import="affichage.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="user.*" %>


<%
    try {

        EtatFacture lv = new EtatFacture();
        //lv.setNomTable("EtatFacture");
        lv.setNomTable("ETATFACTUREMONTANT");
        if (request.getParameter("table") != null ){
            lv.setNomTable(request.getParameter("table"));
        }

        String listeCrt[] = {"id", "daty", "idFournisseur", "idDevise", "datyecheance"};
        String listeInt[] = {"daty", "datyecheance"};
        String libEntete[] = {"id", "daty", "idFournisseur", "idDevise", "datyecheance", "montantTTC", "montantPaye", "reste", "idTVA", "etat"};

        String[] pourcentage = {};
        String somDefaut[] = {"montantTTC", "montantPaye"};
        String colDefaut[] = {"id", "daty", "idFournisseur", "idDevise", "reste","datyecheance"};

        PageRechercheGroupe pr = new PageRechercheGroupe(lv, request, listeCrt, listeInt, 3, colDefaut, somDefaut, pourcentage, libEntete.length, 2);
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));

        pr.setApres("facturefournisseur/facturefournisseur-analyse.jsp");
        pr.creerObjetPagePourc();
%>
<script>
    function changerDesignation() {
        document.analyse.submit();
    }
</script>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Analyse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=facturefournisseur/facturefournisseur-analyse.jsp" method="post" name="analyse" id="analyse">
            <%out.println(pr.getFormu().getHtmlEnsemble());%>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("ETATFACTUREMONTANT") == 0) {%>    
                            <option value="ETATFACTUREMONTANT" selected>Tous</option>
                        <% } else { %>
                        <option value="ETATFACTUREMONTANT" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("ETATFACTUREMONTANTCREE") == 0) {%>    
                        <option value="ETATFACTUREMONTANTCREE" selected>Cr&eacute;e</option>
                        <% } else { %>
                        <option value="ETATFACTUREMONTANTCREE" >Cr&eacute;e</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("ETATFACTUREMONTANTVALIDE") == 0) {%>    
                        <option value="ETATFACTUREMONTANTVALIDE" selected>Valider</option>
                        <% } else { %>
                        <option value="ETATFACTUREMONTANTVALIDE" >Valider</option>
                        <% } %>
                      
                    </select>
                </div>
            </div>

        </form>
        <%
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>

<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>