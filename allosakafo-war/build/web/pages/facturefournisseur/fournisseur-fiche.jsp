<%-- 
    Document   : fournisseur-fiche
    Created on : 8 juin 2020, 15:14:30
    Author     : Sandratra
--%>

<%@page import="mg.allosakafo.facture.Client"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>

<%
try{
    Client cl = new Client();
    cl.setNomTable("fournisseurProduits");

    PageConsulte pc = pc = new PageConsulte(cl, request, (user.UserEJB) session.getValue("u"));

    pc.getChampByName("prenom").setLibelle("Compte");
     pc.setTitre("Consultation Fournisseur");

%>

<div class="content-wrapper">
 <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=facturefournisseur/fournisseur-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
					
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
			<div class="box-footer" >
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=facturefournisseur/fournisseur-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</br>
<% }
catch(Exception e){
  e.printStackTrace();
%>
<script language="JavaScript"> alert('<%=e.getMessage()%>');history.back(); </script>
<%
  return;
}
%>
</DIV>
