<%@page import="mg.allosakafo.tiers.ClientAlloSakafo"%>
<%@page import="affichage.PageRecherche"%>
<%
    try {

        ClientAlloSakafo cl = new ClientAlloSakafo();
        cl.setNomTable("FOURNISSEURPRODUITS");
        
        String listeCrt[] = {"id", "nom", "prenom", "datenaissance", "adresse", "telephone", "fb"};
        String listeInt[] = {"datenaissance"};
        String libEntete[] = {"id", "nom", "prenom", "datenaissance", "adresse", "telephone", "fb"};

        PageRecherche pr = new PageRecherche(cl, request, listeCrt, listeInt, 3, libEntete, libEntete.length );
        pr.setUtilisateur((user.UserEJB) session.getValue("u"));
        pr.setLien((String) session.getValue("lien"));

        pr.setApres("facturefournisseur/client-liste.jsp");

        pr.creerObjetPage(libEntete);

        String libEnteteAffiche[] = {"Id", "Nom", "Prenom", "Date de naissance", "Adresse", "Telephone", "Fb"};
        pr.getTableau().setLibelleAffiche(libEnteteAffiche);


%>


<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste client</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=facturefournisseur/client-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%
            String lienTableau[] = {pr.getLien() + "?but=facturefournisseur/client-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>

<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>    