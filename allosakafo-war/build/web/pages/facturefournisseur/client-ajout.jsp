<%@page import="mg.allosakafo.tiers.ClientAlloSakafo"%>
<%@page import="user.*"%> 
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*"%>
<%@page import="utilitaire.*"%>
<%
    try {

        ClientAlloSakafo a = new ClientAlloSakafo();
        
        a.setNomTable("FOURNISSEURPRODUITS");
        PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
        pi.setLien((String) session.getValue("lien"));


        pi.getFormu().getChamp("nom").setLibelle("Nom");
        pi.getFormu().getChamp("prenom").setLibelle("Prenom");
        pi.getFormu().getChamp("sexe").setLibelle("Sexe");
        pi.getFormu().getChamp("datenaissance").setLibelle("Date de naissance");
        pi.getFormu().getChamp("adresse").setLibelle("Adresse");
        pi.getFormu().getChamp("telephone").setLibelle("Telephone");
        pi.getFormu().getChamp("fb").setLibelle("Facebook");
        
        
        pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Saisie client</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="starticle" id="starticle">
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="facturefournisseur/client-ajout.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.tiers.ClientAlloSakafo">
        <input name="nomtable" type="hidden" id="classe" value="fournisseurProduits">
    </form>
</div>

<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>

