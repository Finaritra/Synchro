<%-- 
    Document   : as-commande-analyse
    Created on : 30 d�c. 2016, 04:57:15
    Author     : Joe
--%>

<%@ page import="user.*" %>
<%@page import="affichage.Liste"%>
<%@ page import="bean.*" %>
<%@page import="bean.TypeObjet"%>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.commande.AnalyseCommande"%>
<%@ page import="affichage.*" %>
<%@page import="lc.Direction"%>
<%@ page import="mg.allosakafo.facturefournisseur.EtatFacture" %>

<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

<%
try{    
    EtatFacture mvt = new EtatFacture();
    
   // String nomTable = "etatfacture";
    String nomTable = "ETATFACTUREMONTANT";
    if(request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0){
        nomTable = request.getParameter("table");
    }
    mvt.setNomTable(nomTable);
    
    String listeCrt[] = {"id", "numFact", "daty","idFournisseur", "idDevise"};
    String libEntete[] = {"Id", "numFact", "daty","idFournisseur", "idDevise"};
    String listeInt[] = {"daty","datyecheance"};
    String[] pourcentage = {"nombrepargroupe"};
    String colDefaut[] = {"id", "numFact", "daty","idFournisseur", "idDevise", "dateEmission","datyecheance"}; 
    String somDefaut[] = {"montantTTC", "montantPaye","reste"};
    
    PageRechercheGroupe pr = new PageRechercheGroupe(mvt, request, listeCrt, listeInt, 3,colDefaut, somDefaut, pourcentage,libEntete.length , 2);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
//    affichage.Champ[] liste = new affichage.Champ[2];
//    
//    Direction dir = new Direction();
//    dir.setNomTable("restaurant");
//    liste[0] = new Liste("restaurant",dir,"libelledir","iddir");
//    
//    TypeObjet ou1 = new TypeObjet();
//    ou1.setNomTable("as_typeproduit");
//    liste[1] = new Liste("typeproduit", ou1, "VAL", "VAL");
//    
//    pr.getFormu().changerEnChamp(liste);
//    
    //pr.getFormu().getChamp("restaurant").setDefaut((String)session.getAttribute("restaurant"));
    
    /*String apreswhere = "";
    if(request.getParameter("datecommande1") == null || request.getParameter("datecommande2") == null){
        apreswhere = "and datecommande = '"+utilitaire.Utilitaire.dateDuJour()+"'";
    }
    if(request.getParameter("restaurant") == null){
        apreswhere += " and restaurant = '"+pr.getUtilisateur().getUser().getAdruser()+"'";
    }
    
    pr.setAWhere(apreswhere);*/
   
    pr.setNpp(500);
    pr.setApres("facturefournisseur/analyse-facture-fournisseur.jsp");
    pr.creerObjetPagePourc();
%>
<script>
    function changerDesignation() {
        document.analyse.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Analyse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=facturefournisseur/analyse-facture-fournisseur.jsp" method="post" name="analyse" id="analyse">
            <%out.println(pr.getFormu().getHtmlEnsemble());%>
			<div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("EtatFacture") == 0) {%>    
                        <option value="EtatFacture" selected>Tous</option>
                        <% } else { %>
                        <option value="EtatFacture" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("EtatFactureCreer") == 0) {%>    
                        <option value="EtatFactureCreer" selected>Cr&eacute;e</option>
                        <% } else { %>
                        <option value="EtatFactureCreer" >Cr&eacute;e</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("EtatFactureValider") == 0) {%>    
                        <option value="EtatFactureValider" selected>Valider</option>
                        <% } else { %>
                        <option value="EtatFactureValider" >Valider</option>
                        <% } %>
                      
                    </select>
                </div>
            </div>
        </form>
        <%
           out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%
    }catch(Exception e){
        e.printStackTrace();
    }
%>