<%@page import="mg.allosakafo.facturefournisseur.FactureFournisseur"%>
<%@page import="user.*"%> 
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*"%>
<%@page import="utilitaire.*"%>
<%
    String autreparsley = "data-parsley-range='[8, 40]' required";
    FactureFournisseur  a = new FactureFournisseur();
    PageUpdate pi = new PageUpdate(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));    
    
    affichage.Champ[] liste = new affichage.Champ[2];
    
    TypeObjet op = new TypeObjet();
    op.setNomTable("devise");
    liste[0] = new Liste("idDevise", op, "desce", "id");
    
    TypeObjet depo = new TypeObjet();
    depo.setNomTable("point");
    liste[1] = new Liste("entite", depo, "val", "id");
    
    pi.getFormu().changerEnChamp(liste);
    
	pi.getFormu().getChamp("numFact").setLibelle("N� facture");
    pi.getFormu().getChamp("daty").setLibelle("Date");
	pi.getFormu().getChamp("idDevise").setLibelle("Devise");
	pi.getFormu().getChamp("idTVA").setLibelle("Montant TVA");
	pi.getFormu().getChamp("montantTTC").setLibelle("Montant TTC");
	pi.getFormu().getChamp("idFournisseur").setLibelle("Fournisseur");
	pi.getFormu().getChamp("datyecheance").setLibelle("Date d'echeance");
		
	pi.getFormu().getChamp("resp").setVisible(false);
	pi.getFormu().getChamp("etat").setVisible(false);
	pi.getFormu().getChamp("dateEmission").setVisible(false);
		
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("remarque").setType("textarea");
	pi.getFormu().getChamp("idFournisseur").setPageAppel("choix/listeFournisseurChoix.jsp");
	
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Enregistrer facture fournisseur</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="starticle" id="starticle">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="update">
    <input name="bute" type="hidden" id="bute" value="facturefournisseur/facturefournisseur-fiche.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.facturefournisseur.FactureFournisseur">
    </form>
</div>