<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%!
    ClassMAPTable objet;
%>
<%
    String classe = request.getParameter("classe");
    objet = (ClassMAPTable) (Class.forName(classe).newInstance());
    PageConsulte pc = new PageConsulte(objet, request, (user.UserEJB) session.getValue("u"));

%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">Details</h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
