<%-- 
    Document   : livraison-liste
    Created on : 12 mars 2020, 14:23:39
    Author     : Maharo R.
--%>

<%@page import="mg.allosakafo.livraison.Livreur"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="user.UserEJB"%>

<%  try{
    Livreur lv = new Livreur();
    
    String listeCrt[] = {"id","nom","contact"};
    String listeInt[] = {};
    String libEntete[] = {"id","nom","contact"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
  
    pr.setApres("livraison/livreur-liste.jsp");
    String[] colSomme = {};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste livraison</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=livraison/livreur-liste.jsp" method="post" name="incident" id="incident">
            <%
                 String lienTableau[] = {pr.getLien() + "?but=livraison/livreur-fiche.jsp"};
                String colonneLien[] = {"id"};
                pr.getTableau().setLien(lienTableau);
                pr.getTableau().setColonneLien(colonneLien);
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%  
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] ={"id","nom","contact"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
    <%

    } catch (Exception e) {
        e.printStackTrace();
}%>
