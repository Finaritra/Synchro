<%-- 
    Document   : reservation-saisie
    Created on : 8 oct. 2019, 11:01:17
    Author     : ionyr
--%>

<%@page import="bean.AdminGen"%>
<%@page import="bean.CGenUtil"%>
<%@page import="mg.allosakafo.livraison.Livreur"%>
<%@page import="mg.allosakafo.livraison.LivraisonApresCommande"%>
<%@page import="user.*"%>
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="mg.allosakafo.commande.*" %>
<% try{
    String autreparsley = "data-parsley-range='[8, 40]' required";
    
    String[] ids=request.getParameterValues("id");
    String idDetail=request.getParameter("idDetail");
    if(idDetail!=null&&idDetail.compareToIgnoreCase("")!=0)
    {
        ids=Utilitaire.stringToTab(idDetail, ";");
    }
    LivraisonCommandeClientDetailsTypeP3 commande=new LivraisonCommandeClientDetailsTypeP3();
    String listeAdresse="";
    if(ids==null||ids.length==0)throw new Exception("Comande non livrable");
    
        //commande.setId(ids[0]);
    commande.setNomTable("VUE_CMD_LIV_DTLS_ALIVRER_PT_ID");
    LivraisonCommandeClientDetailsTypeP3[] listeCommande=(LivraisonCommandeClientDetailsTypeP3[])CGenUtil.rechercher(commande,null,null," and id in ("+Utilitaire.tabToString(ids, "'", ",")+")");
    commande=null;
    if(listeCommande.length>0)
    {
        listeAdresse=Utilitaire.tabToString(AdminGen.grouperDistinctString(listeCommande,"adresse") , "", ";");
        commande=listeCommande[0];
    }
    
    
    LivraisonApresCommande  a = new LivraisonApresCommande();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    
    affichage.Champ[] liste = new affichage.Champ[2];
    TypeObjet op = new TypeObjet();
    op.setNomTable("pointtrie");
    liste[0] = new Liste("idpoint", op, "VAL", "id");
    
    Livreur liv = new Livreur();
    liste[1] = new Liste("idlivreur", liv, "nom", "id");
    
    pi.getFormu().changerEnChamp(liste);
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("idcommnademere").setLibelle("Commande m&egrave;re");
    if(commande!=null)
    {
        pi.getFormu().getChamp("adresse").setDefaut(listeAdresse);
        pi.getFormu().getChamp("idpoint").setDefaut(commande.getIdpoint());
        pi.getFormu().getChamp("daty").setDefaut(commande.getDatecommande().toString());
    }
    if(ids!=null && ids.length>0){
        pi.getFormu().getChamp("idcommnademere").setDefaut(Utilitaire.tabToString(ids, "", ";"));
    }else{
        pi.getFormu().getChamp("idcommnademere").setPageAppel("choix/listeDetailsCommandeChoixMultiple.jsp");
    }
   // pi.getFormu().getChamp("idcommnademere").setAutre("readonly");
    pi.getFormu().getChamp("idpoint").setLibelle("Point");
    pi.getFormu().getChamp("idlivreur").setLibelle("Livreur");
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("etat").setVisible(false);
    pi.getFormu().getChamp("heure").setDefaut(Utilitaire.heureCouranteHMS());
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Saisie Livraison</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="reservation-saisie" id="reservation-saisie">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="livraison/livraison-fiche.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.livraison.LivraisonApresCommande">
    </form>
    
</div>
    <%} catch(Exception e){
        e.printStackTrace();
        throw e;
}%>

