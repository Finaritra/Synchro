<%-- 
    Document   : livraison-liste
    Created on : 12 mars 2020, 14:23:39
    Author     : Maharo R.
--%>

<%@page import="mg.allosakafo.commande.Livraison"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="user.UserEJB"%>

<%  try{
    Livraison lv = new Livraison();
    String nomTable = "LIVRAISONLIB";
     if(request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("") != 0){
        nomTable = request.getParameter("table");
    }
    lv.setNomTable(nomTable);
    
    String listeCrt[] = {"id","heure","idcommnademere","idpoint","adresse","idlivreur","daty"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id","heure","idcommnademere","idpoint","adresse","idlivreur","daty"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    pr.getFormu().getChamp("heure").setLibelle("Heure");
    pr.getFormu().getChamp("idcommnademere").setLibelle(" Commande Mere");
    pr.getFormu().getChamp("idpoint").setLibelle("Point Relais");
    pr.getFormu().getChamp("adresse").setLibelle(" Adresse");
    pr.getFormu().getChamp("idlivreur").setLibelle("Livreur");
    pr.getFormu().getChamp("daty1").setLibelle("Daty Min");
    pr.getFormu().getChamp("daty2").setLibelle("Daty Max");
    
    pr.setApres("livraison/livraison-liste.jsp");
    String[] colSomme = {};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste livraison</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=livraison/livraison-liste.jsp" method="post" name="incident" id="incident">
            <%
                 String lienTableau[] = {pr.getLien() + "?but=livraison/livraison-fiche.jsp"};
                String colonneLien[] = {"id"};
                pr.getTableau().setLien(lienTableau);
                pr.getTableau().setColonneLien(colonneLien);
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
              <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("LIVRAISONLIB") == 0) {%>    
                        <option value="LIVRAISONLIB" selected>Tous</option>
                        <% } else { %>
                        <option value="LIVRAISONLIB" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("LIVRAISONLIB_CREE") == 0) {%>    
                        <option value="LIVRAISONLIB_CREE" selected>Cr&eacute;e</option>
                        <% } else { %>
                        <option value="LIVRAISONLIB_CREE" >Cr&eacute;e</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("LIVRAISONLIB_LIVREE") == 0) {%>    
                        <option value="LIVRAISONLIB_LIVREE" selected>Livre&eacute;e</option>
                        <% } else { %>
                        <option value="LIVRAISONLIB_LIVREE" >Livre&eacute;e</option>
                        <% } %>
                        
				
                    </select>
                </div>
            </div>

        </form>
        <%  
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id","Heure","Commande Mere","Point Livraison","Adresse","Livreur","Date"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
    <%

    } catch (Exception e) {
        e.printStackTrace();
}%>
