
<%@page import="mg.cnaps.prevision.PrevisionBudgetVue"%>
<%@page import="utilitaire.*"%>
<%@page import="java.sql.Connection"%>
<%@page import="user.UserEJB"%>
<%@page import="bean.CGenUtil"%>
<%@page import="bean.TypeObjet"%>
<%!
    UserEJB u = null;
    String lien = "";
    TypeObjet[] liste_mois = null;
    PrevisionBudgetVue[] liste_prevision;
    double[] somme_prevision;
    String daty_fin = null;
    String annee = null;
    String annee2=null;
    String mois1 = null;
    String mois2 = null;
    String mois1Lib = null;
    String mois2Lib = null;
    TypeObjet[] liste = null;
%>
<%
    try {
        u = (UserEJB) session.getAttribute("u");
        lien = (String) session.getValue("lien");
        TypeObjet d = new TypeObjet();
        d.setNomTable("MOIS");
        mois1 = request.getParameter("mois1");
        if (mois1 == null || mois1.compareTo("") == 0) {
            mois1 = "1";
        }
        mois2 = request.getParameter("mois2");
        if (mois2 == null || mois2.compareTo("") == 0) {
            mois2 = "12";
        }
        annee = request.getParameter("annee");
        if (annee == null || annee.compareTo("") == 0) {
            annee = "2015";
        }
        annee2 = request.getParameter("annee2");
        if (annee2 == null || annee2.compareTo("") == 0) {
            annee2 = "2015";
        }
        PrevisionBudgetVue v = new PrevisionBudgetVue();
        liste_prevision = (PrevisionBudgetVue[]) u.getData(v, null, null, null, " and mois >= " + mois1 + " and mois <= " + mois2 + " and annee between " + annee + " and "+ annee2 + " order by mois asc");
        somme_prevision = v.getSommeMontant(liste_prevision);
         liste_mois = (TypeObjet[]) u.getData(d, null, null, null, " ");
        liste = (TypeObjet[]) u.getData(d, null, null, null, " and id >= " + mois1 + " and id <=" + mois2);

%>

<div class="content-wrapper">
    <section class="content-header">
        <div>
            <div class="col-sm-6">
                <h4>Pr&eacute;vision</h4>
            </div>
        </div>
    </section>
    <section class="content">
        <form action="<%=lien%>?but=prevision/prevision.jsp" method="post" name="prestation" id="prestation">
            <div class="row">
                <div class="row col-md-12">
                    <div class="box box-primary box-solid collapsed">
                        <div class="box-header with-border">
                            <h3 class="box-title" color="#edb031">
                                <span color="#edb031">Recherche avanc�e</span>
                            </h3>
                            <div class="box-tools pull-right">
                                <button data-original-title="Collapse" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="">
                                    <i class="fa fa-plus"></i>
                                </button> 
                            </div>
                        </div>
                        <div class="box-body" id="pagerecherche">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label for="mois1">Mois Min</label>
                                    <select name="mois1" class="form-control">
                                        <%for(int i =0 ; i<liste_mois.length ; i++){%>
                                        <option class="form-control" value="<%=liste_mois[i].getId()%>" <%if(request.getParameter("mois1")!=null && request.getParameter("mois1").compareTo(liste_mois[i].getId())==0)out.print(" selected");%>><%=liste_mois[i].getVal()%></option>>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="mois2">Mois Max</label>
                                     <select name="mois2" class="form-control">
                                        <%for(int i =0 ; i<liste_mois.length ; i++){%>
                                        <option class="form-control" value="<%=liste_mois[i].getId()%>" <%if(request.getParameter("mois2")!=null && request.getParameter("mois2").compareTo(liste_mois[i].getId())==0)out.print(" selected");%>><%=liste_mois[i].getVal()%></option>>
                                        <%}%>
                                    </select>
                                </div>
                                
                                
                                 <div class="col-md-2">
                                    <label for="annee">Annee</label>
                                    <input name="annee" type="textbox" class="form-control" id="annee" value="<%=annee%>" />
                                    
                                </div>
                                    
                                <div class="col-md-2">
                                    <label for="annee">�</label>
                                    <input name="annee2" type="textbox" class="form-control" id="annee" value="<%=annee%>" />
                                </div>
                                
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row col-md-12">
                                <div class="col-xs-6" align="right">
                                    <button type="submit" class="btn btn-primary" id="btnListe">Afficher</button>
                                </div>
                                <div class="col-xs-6" align="left">
                                    <button type="reset" class="btn btn-default">R�initialiser</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    Solde anterieur: <input type="text" name="solde_anterieur" id="solde_anterieur"  onblur="calculAvecSoldeAnterieur()">
            
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">PREVISION</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table width="100%" class="table table-bordered">
                            <tr>
                                <td><b><u>Mois</u></b></td>
                                <td><b><u>Ann�e</u></b></td>
                                <td><b><u>Recette Pr�vue </u></b></td>
                                <td><b><u>D&eacute;pense Prevue </u></b></td>
                                <td><b><u>Solde Pr&eacute;vu</u></b></td>
                                <td><b><u>Recette R&eacute;alis&eacute;e </u></b></td>
                                <td><b><u>D&eacute;pense R&eacute;alis&eacute;e </u></b></td>
                                <td><b><u>Solde R&eacute;alis&eacute;</u></b></td>
                            </tr>
                            <%if (liste_prevision.length > 0) {
                                    double soldeP = 0, soldeR=0;
                                    for (int i = 0; i < liste_prevision.length; i++) {
                                        daty_fin = Utilitaire.convertDatyFormtoRealDatyFormat(Utilitaire.getLastDayOfDate("01/" + liste_prevision[i].getMois() + "/" + annee));soldeP = soldeP + liste_prevision[i].getSoldeprevu();soldeR = soldeR+liste_prevision[i].getSolderealise();%>
                            <tr>
                                <td><%= liste_prevision[i].getMois_valeur() %></td>
                                <td><%= liste_prevision[i].getAnnee() %></td>
                                <td><a href="<%=lien + "?but=budget/recette/recette-liste-mois.jsp&mois=" + liste_prevision[i].getMois() + "&exercice=" + annee%>"><%=Utilitaire.formaterAr(liste_prevision[i].getPrevurecette())%></a></td>
                                <td><a href="<%=lien + "?but=budget/depense/depense-liste-mois.jsp&mois=" + liste_prevision[i].getMois() + "&exercice=" + annee%>"><%=Utilitaire.formaterAr(liste_prevision[i].getPrevudepense()) %></a></td>
                                <td><input type="text" name="soldePrevu_<%=i%>" id="soldePrevu_<%=i%>" value="<%out.print(Utilitaire.formaterAr(soldeP)); %>" readonly/></td>
                               
                                <td><a href="<%=lien + "?but=recette/declaration/declaration-liste.jsp&daty1=01/" + liste_prevision[i].getMois() + "/" + annee + "&daty2=" + daty_fin%>&etat=RECETTE_DEC_LIBELLE_VISER" ><%=Utilitaire.formaterAr(liste_prevision[i].getRealiserecette())%></a></td>          
                                <td><a href="<%=lien + "?but=tresorerie/OP/op-liste.jsp&date_op1=01/" + liste_prevision[i].getMois() + "/" + annee + "&date_op2= " + daty_fin%>&etat=op_complet_vise"><%=Utilitaire.formaterAr(liste_prevision[i].getRealisedepense()) %></a></td>
                                <td><input type="text" name="soldeRealise_<%=i%>" id="soldeRealise_<%=i%>" value="<%out.print(Utilitaire.formaterAr(soldeR));  %>" readonly/></td>
                           
                            </tr>
                            <%}%>
                            <tr>
                                <td>Total:</td>
                                <td><b></b></td>
                                <td><b><%=Utilitaire.formaterAr(somme_prevision[1])%></b></td>
                                <td><b><%=Utilitaire.formaterAr(somme_prevision[0])%></b></td>
                                <td></td>
                                <td><b><%=Utilitaire.formaterAr(somme_prevision[3])%></b></td>
                                <td><b><%=Utilitaire.formaterAr(somme_prevision[2])%></b></td>
                                        
                            </tr>
                            <%}%>
                        </table>
                    </div>
                </div>  
            </div>
        </div>
        <!--<div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table width="100%" class="table table-bordered">
                            <tr>
                                <td><b><u>Mois</u></b></td>
                                <td><b><u>Solde pr&eacute;vu</u></b></td>
                                <td><b><u>Solde r&eacute;alis&eacute;</u></b></td>
                            </tr>
                            <%if (liste_prevision.length > 0) {
                                    for (int i = 0; i < liste_mois.length; i++) {
                                        daty_fin = Utilitaire.convertDatyFormtoRealDatyFormat(Utilitaire.getLastDayOfDate("01/" + liste_mois[i].getId() + "/" + annee));%>
                            <tr>
                                <td><%= liste_mois[i].getVal()%></td>
                            </tr>
                            <%}%>
                            <%}%>
                        </table>
                    </div>
                </div>  
            </div>
        </div>-->
    </section>
</div>
<%  } catch (Exception e) {
        e.printStackTrace();
        throw e;
    }
%>
<script type="text/javascript">
    function calculerPrevision(indice){ 
      var valeurIndice = parseFloat(formatEspace(document.getElementById("montantPrevu_"+indice).value));
      var temp = valeurIndice;
      for(i=parseInt(indice)+1;i<12;i++){
          var precVal = parseFloat(formatEspace(document.getElementById("montantPrevu_"+i).value));
          temp = temp + precVal;
          document.getElementById("montantPrevu_"+i).value = temp;
          //$("#"+predicat+"_"+i).val(Math.round(newValFinal*100)/100);
      }
    }
    function calculerRealiser(indice){ 
      var valeurIndice = parseFloat(formatEspace(document.getElementById("montantRealise_"+indice).value));
      var temp = valeurIndice;
      for(i=parseInt(indice)+1;i<12;i++){
          var precVal = parseFloat(formatEspace(document.getElementById("montantRealise_"+i).value));
          temp = temp + precVal;
          document.getElementById("montantRealise_"+i).value = temp;
          //$("#"+predicat+"_"+i).val(Math.round(newValFinal*100)/100);
      }
    }
    function formatEspace(montantBase) {
        var str = montantBase.replace(/\s/g, "");//remplacer espace
        var ret = str.replace(",",".");
        return ret;
    }
    function calculAvecSoldeAnterieur(){
        var solde = parseFloat(formatEspace(document.getElementById("solde_anterieur").value));
        for(i=0;i<12;i++){
            solde = solde + parseFloat(formatEspace(document.getElementById("soldePrevu_"+i).value));
            var val_format = format(solde,2," ");
            var ret = val_format.replace(".",",");    
            document.getElementById("soldePrevu_"+i).value = ret;
        }
    }
    function format(valeur,decimal,separateur) {
	var deci=Math.round( Math.pow(10,decimal)*(Math.abs(valeur)-Math.floor(Math.abs(valeur)))) ; 
	var val=Math.floor(Math.abs(valeur));
	if ((decimal===0)||(deci===Math.pow(10,decimal))) {val=Math.floor(Math.abs(valeur)); deci=0;}
	var val_format=val+"";
	var nb=val_format.length;
	for (var i=1;i<4;i++) {
		if (val>=Math.pow(10,(3*i))) {
			val_format=val_format.substring(0,nb-(3*i))+separateur+val_format.substring(nb-(3*i));
		}
	}
	if (decimal>0) {
		var decim=""; 
		for (var j=0;j<(decimal-deci.toString().length);j++) {decim+="0";}
		deci=decim+deci.toString();
		val_format=val_format+"."+deci;
	}
	if (parseFloat(valeur)<0) {val_format="-"+val_format;}
	return val_format;
    }
</script>