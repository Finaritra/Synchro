<%-- 
    Document   : web_selectById
    Created on : 18 avr. 2017, 11:59:45
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String url = request.getParameter("url");
    String table = request.getParameter("table");
    JSONArray resultat_url = ExecuteFunction.resultJSON(url);
    JSONArray resultat_table = ExecuteFunction.resultJSONChamp(table);
%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setContentType("application/json");
	try{
            out.println("{\"champ\":" + resultat_table);
            out.println(",\"list\":" + resultat_url);
            out.println(",'query':[{query:\"" + url + "\"}]");
        }
		
                
	catch(Exception e){
		out.println("Exception!!!");
	}
%>