
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.commande.*"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="utilitaire.*"%>
<%@page import="lc.Direction"%>
<%@page import="service.AlloSakafoService"%>

<%      
    try {
    
    Annulation annulation = new Annulation();
	
    String listeCrt[] = {"id","idobjet","motif", "daty","montant" , "etat"};
    String listeInt[] = {"daty" ,"montant"};
    String libEntete[] = {"id","idobjet","motif", "daty","montant" , "etat"};

    
    PageRecherche pr = new PageRecherche(annulation, request, listeCrt, listeInt, 2, libEntete, 6);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));

    pr.setApres("commande/annulation-op-liste.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste commande</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=commande/annulation-op-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%  
            out.println(pr.getTableauRecap().getHtml());
        %>
        <br>
    </section>
</div>
        
<%
    } catch (Exception e) {
        e.printStackTrace();
    }
%>        