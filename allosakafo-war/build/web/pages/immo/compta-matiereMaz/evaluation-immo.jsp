
<%@page import="utilitaire.Utilitaire"%>
<%@page import="mg.cnaps.immo.ImmoEvaluationMaz"%>
<%@page import="affichage.PageUpdate"%>
<%@page import="user.UserEJB"%>
<%
    UserEJB u;
    ImmoEvaluationMaz ieval;
%>
<%
    ieval = new ImmoEvaluationMaz();
    u = (user.UserEJB) session.getValue("u");
    PageUpdate pu = new PageUpdate(ieval, request, (user.UserEJB) session.getValue("u"));
	
    pu.setLien((String) session.getValue("lien"));
   
    pu.getFormu().getChamp("nomenclature").setLibelle("Nomenclature");
    pu.getFormu().getChamp("nomenclature").setAutre("readonly");
    pu.getFormu().getChamp("designation").setLibelle("D�signation mat�riel");
    pu.getFormu().getChamp("designation").setAutre("readonly");
    pu.getFormu().getChamp("unite").setAutre("readonly");
    pu.getFormu().getChamp("unite").setLibelle("Unit�");
    pu.getFormu().getChamp("unite").setAutre("readonly");
    pu.getFormu().getChamp("prix_u").setLibelle("Prix unitaire");    
    pu.getFormu().getChamp("quantite").setLibelle("Quantit�");
    pu.getFormu().getChamp("quantite").setAutre("readonly");
    //pu.getFormu().getChamp("quantite").setDefaut("1");
    pu.getFormu().getChamp("observation").setLibelle("Observation");
    pu.getFormu().getChamp("observation").setAutre("readonly");
    pu.getFormu().getChamp("service").setLibelle("Service");
    pu.getFormu().getChamp("service").setPageAppel("immo/compta-matiere/log-service-liste.jsp");
    pu.getFormu().getChamp("service").setAutre("readonly");
    pu.getFormu().getChamp("annee").setLibelle("Ann�e");
    pu.getFormu().getChamp("annee").setAutre("readonly");
    pu.getFormu().getChamp("montant").setVisible(false);
    pu.getFormu().getChamp("montant").setAutre("readonly");
    pu.getFormu().getChamp("evalue").setDefaut("1");
    pu.getFormu().getChamp("evalue").setVisible(false);
    //pu.getFormu().getChamp("").setPageAppel("choix/immobilisationChoix.jsp");
  
    pu.preparerDataFormu();
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <h1><a href="<%=(String) session.getValue("lien")%>?but=immo/compta-matiereMaz/immo-a-evalue-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a>Evaluation </h1>
                    <form action="<%=(String) session.getValue("lien")%>?but=immo/compta-matiereMaz/apres-modif-maz.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="evaluation" id="evaluation">
                        <%
                            out.println(pu.getFormu().getHtmlInsert());
                        %>
                        <div class="row">
                            <div class="col-md-11">
                                <button class="btn btn-primary pull-right" name="Submit2" type="submit">Valider</button>
                            </div>
                            <br><br> 
                        </div>
                        <input name="acte" type="hidden" id="acte" value="updateEv">
                        <input name="bute" type="hidden" id="bute" value="immo/compta-matiereMaz/immo-a-evalue-fiche.jsp">
                        <input name="classe" type="hidden" id="classe" value="mg.cnaps.immo.ImmoEvaluationMaz">
                        <input name="nomtable" type="hidden" id="nomtable" value="IMMO_A_EVALUE">

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

</script>