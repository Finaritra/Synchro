
<%@page import="mg.cnaps.immo.Immobilisation"%>
<%@page import="affichage.PageRecherche"%>
<% Immobilisation dr = new Immobilisation();
    dr.setNomTable("IMMO_LIBELLE");
    if (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("") != 0) {
        System.out.println(request.getParameter("etat"));
        dr.setNomTable(request.getParameter("etat"));
    }
    String listeCrt[] = {"id","codification", "nature_immo", "designation","numero_bl", "daty"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id","codification", "designation","nature_immo",  "daty", "valeur_acquisition","amortissable","numero_bl"};
    PageRecherche pr = new PageRecherche(dr, request, listeCrt, listeInt,3, libEntete, 8);
    pr.setUtilisateur((user.UserEJB) session.getValue("u")); 
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("nature_immo").setLibelle("Nature");
    pr.getFormu().getChamp("daty1").setLibelle("Date min");
    pr.getFormu().getChamp("daty2").setLibelle("Date max");
    pr.getFormu().getChamp("numero_bl").setLibelle("TEF");
    pr.setApres("immo/immobilisation-liste.jsp");
    String[] colSomme = {"valeur_acquisition"};
    pr.creerObjetPage(libEntete, colSomme);
%>

<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste des immobilisations</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=immo/immobilisation-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="etat" class="form-control" id="etat" onchange="changerDesignation()" >
                        <option value="IMMO_LIBELLE" <%= (request.getParameter("etat") == null || request.getParameter("etat").compareToIgnoreCase("") == 0)?"selected":""%> >Tous </option>
                        <option value="IMMO_LIBELLE_VALIDER" <%= (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("IMMO_LIBELLE_VALIDER")==0)?"selected":""%> >Vis&eacute;e </option>
                        <option value="IMMO_LIBELLE_NON_VALIDE" <%= (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("IMMO_LIBELLE_NON_VALIDE") == 0)?"selected":""%> >Non Vis&eacute;e </option>
                        <option value="IMMO_LIBELLE_ATTRIBUE" <%= (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("IMMO_LIBELLE_ATTRIBUE") == 0)?"selected":""%> >Attribu&eacute;e </option>
                        <option value="IMMO_LIBELLE_NON_ATTRIBUE" <%= (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("IMMO_LIBELLE_NON_ATTRIBUE") == 0)?"selected":""%> >Non Attribu&eacute;e </option>
                        <option value="IMMO_LIBELLE_CONDAMNE" <%= (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("IMMO_LIBELLE_CONDAMNE") == 0)?"selected":""%> >Condamn�e </option>
                        <option value="IMMO_LIBELLE_CEDE" <%= (request.getParameter("etat") != null && request.getParameter("etat").compareToIgnoreCase("IMMO_LIBELLE_CEDE") == 0)?"selected":""%> >Ced�e </option>
                    </select></br></br>
                </div>
                <div class="col-md-4"></div>
            </div><br/><br/>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=immo/immobilisation-fiche.jsp",pr.getLien() + "?but=stock/bc/tef-fiche.jsp"};
            String colonneLien[] = {"id","numero_bl"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"id","Codification", "D&eacute;signation", "Nature", "Acquisition", "Valeur","Amortissable","Origine"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
