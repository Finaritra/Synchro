<%-- 
    Document   : TablePointChoix
    Created on : 23 f�vr. 2021, 19:11:32
    Author     : ACER
--%>

<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.facture.ClientPoint"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    ClientPoint e = new ClientPoint();
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "val","pointlib"};
    String listeInt[] = {""};
    String libEntete[] = {"id", "val","pointLib"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 2, libEntete, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("TableClientChoix.jsp");
    pr.setChampReturn(champReturn);
    pr.getFormu().getChamp("id").setLibelleAffiche("ID");
    pr.getFormu().getChamp("val").setLibelleAffiche("Nom de la table");
    pr.getFormu().getChamp("pointlib").setLibelleAffiche("Point");
    String aWhere = "or point is null";
    
    if(request.getParameter("pointlib") == null){
       aWhere = "and (point like '"+AlloSakafoService.getNomRestau()+"' or point is null)";
    }    
    
    pr.setAWhere(aWhere);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste des tables </h1>
            </section>
            <section class="content">
                <form action="TablePointChoix.jsp?champReturn=<%=pr.getChampReturn()%>" method="post" name="bpc" id="bpc">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String lienTableau[] = {pr.getLien() + "?but=budget/section/section-fiche.jsp"};
                    String libelles[] =  {"id", "Nom de la table", "Point"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
