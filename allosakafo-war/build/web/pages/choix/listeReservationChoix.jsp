<%-- 
    Document   : listeReservationChoix
    Created on : 3 f�vr. 2020, 14:01:54
    Author     : rajao
--%>


<%@page import="mg.allosakafo.reservation.Reservation"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<% 
    try{
    Reservation reservation=new Reservation();
    //reservation.setNomTable("reservation_lib");
    String champReturn = request.getParameter("champReturn");
    String listeCrt[] = {"id", "source","datesaisie", "datereservation","idClient"};
    String listeInt[] = {"datesaisie","datereservation"};
    String libEntete[] = {"id", "source", "heuresaisie", "heuredebutreservation", "heurefinreservation","nbpersonne","datesaisie", "datereservation","idClient"};
    PageRechercheChoix pr = new PageRechercheChoix(reservation, request, listeCrt, listeInt, 2, libEntete,libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("choix/listeReservationChoix.jsp");
    pr.setChampReturn(champReturn);
    /*pr.getFormu().getChamp("datesaisie1").setLibelle("Date de saisie Min");
    pr.getFormu().getChamp("datesaisie2").setLibelle("Date de saisie Max");
    pr.getFormu().getChamp("datereservation1").setLibelle("Date de reservation Min");
    pr.getFormu().getChamp("datereservation2").setLibelle("Date de reservation Max");*/
    

    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    Reservation[] listeP = (Reservation[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Site pho</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Reservation</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getLien()%>?but=<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="agencecode" id="agencecode">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id", "source", "heuresaisie", "Heure debut reservation", "Heure fin reservation","Nombre personne","Date de saisie", "Date de reservation","Client"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                 <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
<% } catch(Exception e){e.printStackTrace();} %>
