<%-- 
    Document   : listeMvtCaisse
    Created on : 25 f�vr. 2020, 10:59:11
    Author     :Maharo
--%>

<%@ page import="user.*" %>
<%@page import="mg.allosakafo.fin.MvtCaisse"%>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>

<%
    try{
    String champReturn = request.getParameter("champReturn");
    MvtCaisse e = new MvtCaisse();
    //e.setNomTable("");
    String listeCrt[] = {"id","designation", "numpiece","numcheque","typemvt","daty"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id","designation", "tiers", "numpiece","numcheque","typemvt","daty"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeMvtCaisseChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme =null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Popup MVT CAISSE</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste MVT</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                       String libelles[]={"Id","Designation", "Tiers", "Numpiece","Numcheque","Type Mouvement","Daty","Debit","Credit","Reste"};
//		    pr.getTableau().setLien(lienTableau);
//                    pr.getTableau().setColonneLien(colonneLien);
                    pr.getTableau().setLibelleAffiche(libelles);

                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoix.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithRadioButton()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>
<% } catch(Exception e){e.printStackTrace();} %>