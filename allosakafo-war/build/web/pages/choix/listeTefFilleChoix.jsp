<%-- 
    Document   : listeTefFilleChoix
    Created on : 1 oct. 2015, 11:10:06
    Author     : GILEADA
--%>

<%@page import="utilitaire.Utilitaire"%>
<%@page import="affichage.PageRechercheChoix"%>
<%@page import="mg.cnaps.budget.BudgetTefMereFille"%>
<%
    String champReturn = request.getParameter("champReturn");
    BudgetTefMereFille e = new BudgetTefMereFille();
    e.setNomTable("BUDGET_TEF_MERE_FILLE_LIB2");
    String listeCrt[]={"id","code", "daty","designation","quantite","pu","libelle","nombeneficiaire","tef"};
    String listeInt[]={"daty"};
    String libEntete[]={"id","code", "daty","designation","quantite","pu","libelle","nombeneficiaire","tef"};
    PageRechercheChoix pr = new PageRechercheChoix(e, request, listeCrt, listeInt, 3, libEntete, 9);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.setApres("listeTefFilleChoix.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
    BudgetTefMereFille[] listeP = (BudgetTefMereFille[]) pr.getRs().getResultat();
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cynthia 2.0 | CNaPS</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Liste Tef Choix</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="comptaTiers" id="comptaTiers">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libelles[] = {"id","code", "daty","designation","quantite","pu","libelle","nombeneficiaire","tef"};
                    pr.getTableau().setLibelleAffiche(libelles);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choix/apresChoixComptee.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <table class="table table-bordered">
                            <tr class=info>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Code</th>
                                    <th>Date</th>
                                    <th>Designation</th>
                                    <th>Quantite</th>
                                    <th>PU</th>
                                    <th>Libelle</th>
                                    <th>Beneficiaire</th>
                                    <th>Tef</th>
                            </tr>
                            <%
                                    for (int i = 0; i < listeP.length; i++) {
                            %>
                            <tr>
                                    <td><input type="radio" name="choix" onMouseDown="getChoix()" value="<%=listeP[i].getId()%>;<%=listeP[i].getQuantite()%>;<%=listeP[i].getPu()%>;<%=listeP[i].getFrais()%>;<%=listeP[i].getDesignation()%>" class="radio" /></td>
                                    <td align=left><%=listeP[i].getId()%></td>
                                    <td align=left><%=listeP[i].getCode()%></td>
                                    <td align=center><%=Utilitaire.convertDatyFormtoRealDatyFormat(listeP[i].getDaty()+"")%></td>
                                    <td align=left><%=listeP[i].getDesignation()%></td>
                                    <td align=right><%=Utilitaire.formaterAr(listeP[i].getQuantite())%></td>
                                    <td align=right><%=Utilitaire.formaterAr(listeP[i].getPu())%></td>
                                    <td align=left><%=listeP[i].getLibelle()%></td>
                                    <td align=left><%=listeP[i].getNombeneficiaire()%></td>
                                    <td align=left><%=listeP[i].getTef()%></td>
                            </tr>
                            <%}%>
                    </table>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>