<%-- 
    Document   : listeMenuDynamiqueChoixMultiple
    Created on : 30 nov. 2019, 09:12:16
    Author     : rajao
--%>
<%@page import="utilisateur.Utilisateur"%>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="user.*" %>
<%@ page import="mg.allosakafo.commande.CommandeClientDetails" %>
<%
    String champReturn = request.getParameter("champReturn");
    CommandeClientDetails menudynamique=new CommandeClientDetails();
    menudynamique.setNomTable("AS_DETAILSCOMMANDELIB");
    String listeCtr[]={"id", "idmere", "produit","prioriter","pu"};
    String listeInt[]={"pu"};
    String libEntete[]={"id", "idmere", "produit","idAccompagnementSauce","quantite","pu","prioriter"};
    PageRechercheChoix pr=new PageRechercheChoix(menudynamique,request,listeCtr,listeInt,3,libEntete,libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    pr.getFormu().getChamp("idmere").setLibelle("Commande m&egrave;re");
    pr.getFormu().getChamp("prioriter").setLibelle("Priorit&eacute;");
    pr.getFormu().getChamp("pu1").setLibelle("PU min");
    pr.getFormu().getChamp("pu2").setLibelle("PU max");
    
    pr.setApres("listeDetailsCommandeChoixMultiple.jsp");
    pr.setChampReturn(champReturn);
    String[] colSomme = null;
    pr.creerObjetPage(libEntete, colSomme);
%>
<html>
    <head>
        <meta charset="UTF-8">
        <title>SPAT</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <jsp:include page='../elements/css.jsp'/>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <section class="content-header">
                <h1>Choix D�tails Commande</h1>
            </section>
            <section class="content">
                <form action="<%=pr.getApres()%>?champReturn=<%=pr.getChampReturn()%>" method="post" name="personneliste" id="personneliste">
                    <% out.println(pr.getFormu().getHtmlEnsemble());%>
                </form>
                <%
                    String libEnteteAffiche[] = {"id", "Commande m&egrave;re", "Produit","Accompagnement Sauce","Quantit&eacute;","PU","Priorit&eacute;"};
                    pr.getTableau().setLibelleAffiche(libEnteteAffiche);
                    out.println(pr.getTableauRecap().getHtml());
                %>
                <form action="../<%=pr.getLien()%>?but=choixMultiple/apresChoixMultiple.jsp" method="post" name="frmchx" id="frmchx">
                    <input type="hidden" name="champReturn" value="<%=pr.getChampReturn()%>">
                    <% out.println(pr.getTableau().getHtmlWithMultipleCheckbox()); %>
                </form>
                <% out.println(pr.getBasPage());%>
            </section>
        </div>
        <jsp:include page='../elements/js.jsp'/>
    </body>
</html>