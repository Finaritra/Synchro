<%--
    Document   : test-fiche
    Created on : 13 janv. 2016, 11:03:23
    Author     : Admin
--%>

<%--
    Document   : courier-fiche
    Created on : 14 d�c. 2015, 14:24:32
    Author     : Jetta
--%>

<%@page import="test.Test1"%>
<%@page import="affichage.PageConsulte"%>
<%@page import="user.UserEJB"%>
<%
    Test1 ad;
    UserEJB u;
%>
<%
    u = (UserEJB) session.getAttribute("u");
    ad = new Test1();
    //ad.setNomTable("COUR_COURIER_LIBELLE");
    String[] libelleLettrageFiche = {"id", "Chemin", "Libelle", "Mere"};
    PageConsulte pc = new PageConsulte(ad, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    /*POUR ACTIVER L'ATTACHEMENT DE FICHIER GENERIQUE*/


    /*END*/
    pc.setLibAffichage(libelleLettrageFiche);
    pc.setTitre("Fiche  test");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=test/test-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=test/test-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=rest/test-liste.jsp&classe=test.Test1" style="margin-right: 10px">Supprimer</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>