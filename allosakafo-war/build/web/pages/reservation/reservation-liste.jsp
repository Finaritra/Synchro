<%-- 
    Document   : reservation-liste
    Created on : 8 oct. 2019, 15:19:50
    Author     : Antsa
--%>
<%@page import="mg.allosakafo.reservation.ReservationLib"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="user.UserEJB"%>

<%
    try{
    ReservationLib res = new ReservationLib();
    res.setNomTable("ReservationLibtous");
     if (request.getParameter("etatvue") != null && !request.getParameter("etatvue").equals("")) {
        res.setNomTable(request.getParameter("etatvue"));
    }
    
    String listeCrt[] = {"id", "telephone","tablenom","source", "idClient", "heuredebutreservation", "datereservation","remarque","idpointlibelle"};
    String listeInt[] = {"datereservation"};
    String libEntete[] = {"id","telephone","idClient","tablenom","nbpersonne","source", "heuredebutreservation", "datereservation","remarque","idpointlibelle","etat"};

    PageRecherche pr = new PageRecherche(res, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("idClient").setLibelle("Num�ro client");
    pr.setApres("reservation/reservation-liste.jsp");
    String[] colSomme = null;
    
    
    affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet liste1 = new TypeObjet();
    liste1.setNomTable("point");
    liste[0] = new Liste("idpointlibelle", liste1, "val", "val");
    pr.getFormu().changerEnChamp(liste);
    
    pr.getFormu().getChamp("datereservation1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("idpointlibelle").setDefaut(service.AlloSakafoService.getRestau(null).getVal());
    pr.getFormu().getChamp("idpointlibelle").setLibelle("Point");
    
    
    //pr.getFormu().getChamp("restaurant").setDefaut((String)session.getAttribute("restaurant"));
    
    String apreswhere = "";
    if(request.getParameter("datereservation1") == null){
        apreswhere = "and datereservation = '"+utilitaire.Utilitaire.dateDuJour()+"'";
    }
    pr.setAWhere(apreswhere);
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.reservation.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste reservation</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=reservation/reservation-liste.jsp" method="post" name="reservation" id="reservation">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="col-md-4 col-md-offset-5">
                <div class="form-group">
                    <label>Etat : </label>
                    <select name="etatvue" class="champ form-control" id="etatvue" onchange="changerDesignation()" style="display: inline-block; width: 250px;">
                         <option value="ReservationLibtous" <%if (request.getParameter("etatvue") == null || request.getParameter("etatvue").compareToIgnoreCase("ReservationLibtous") == 0) {
                                out.print("selected");
                            }%>>Tous</option>
                        <option value="ReservationLibcree" <%if (request.getParameter("etatvue") != null && request.getParameter("etatvue").compareToIgnoreCase("ReservationLibcree") == 0) {
                                out.print("selected");
                            }%>>Cr&eacute;e</option>
                        <option value="ReservationLibvalide" <%if (request.getParameter("etatvue") != null && request.getParameter("etatvue").compareToIgnoreCase("ReservationLibvalide") == 0) {
                                out.print("selected");
                            }%>>Valid&eacute;</option>
                        <option value="RESERVATIONLIBEFFECTUE" <% if (request.getParameter("etatvue") != null && request.getParameter("etatvue").compareToIgnoreCase("ReservationLibeffectue") == 0) {
                                out.print(" selected");
                            }%>>Effectu&eacute;</option>
                    </select>
                </div>
            </div>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=reservation/reservation-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            
            String libEnteteAffiche[] = {"Id","Telephone","Client","Nom Table", "Nombre personne","Source", "Heure Debut Reservation", "Date reservation","Remarques","Point","etat"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>
    <% }catch(Exception e){e.printStackTrace();} %>