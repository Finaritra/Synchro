<%-- 
    Document   : reservation-saisie
    Created on : 8 oct. 2019, 11:01:17
    Author     : ionyr
--%>
<%@page import="mg.allosakafo.reservation.Reservation"%>
<%@page import="user.*"%>
<%@ page import="bean.TypeObjet" %>
<%@page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="service.AlloSakafoService" %>
<% try{
    String autreparsley = "data-parsley-range='[8, 40]' required";
    Reservation  a = new Reservation();
    PageInsert pi = new PageInsert(a, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    pi.getFormu().getChamp("datereservation").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("heuredebutreservation").setDefaut(Utilitaire.heureCouranteHMS());
    pi.getFormu().getChamp("source").setLibelle("Source");
    
    affichage.Champ[] liste = new affichage.Champ[2];
    TypeObjet op = new TypeObjet();
    op.setNomTable("source");
    liste[0] = new Liste("source", op, "VAL", "id");
    TypeObjet point = new TypeObjet();
    point.setNomTable("point");
    liste[1] = new Liste("point", point, "VAL", "id");
    pi.getFormu().changerEnChamp(liste);
    
    pi.getFormu().getChamp("heuredebutreservation").setLibelle("Heure debut de reservation");
    pi.getFormu().getChamp("datereservation").setLibelle("Date de reservation");
    pi.getFormu().getChamp("nbpersonne").setLibelle("Nombre de personne");
    pi.getFormu().getChamp("heurefinreservation").setVisible(false); 
    pi.getFormu().getChamp("datesaisie").setDefaut(Utilitaire.dateDuJour()); 
    pi.getFormu().getChamp("datesaisie").setAutre("readonly");
    pi.getFormu().getChamp("datesaisie").setLibelle("Date saisie");
    pi.getFormu().getChamp("heuresaisie").setVisible(false);
    pi.getFormu().getChamp("idCommande").setPageAppel("choix/listeCommandeChoixMultiple.jsp");
    pi.getFormu().getChamp("idCommande").setAutre("readonly");
    pi.getFormu().getChamp("idCommande").setLibelle("Commande");
    pi.getFormu().getChamp("idClient").setLibelle("Client");
   pi.getFormu().getChamp("idClient").setAutre("Placeholder='T�l�phone-Nom'");
    pi.getFormu().getChamp("etat").setVisible(false);
    pi.getFormu().getChamp("source").setDefaut("source1");
    pi.getFormu().getChamp("point").setDefaut(AlloSakafoService.getNomRestau());

    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Saisie Reservation</h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="reservation-saisie" id="reservation-saisie">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="reservation/reservation-fiche.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.reservation.Reservation">
    </form>
    
</div>
    <%} catch(Exception e){
        e.printStackTrace();
}%>

