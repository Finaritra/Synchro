<%-- 
    Document   : projet-fiche
    Created on : 8 d�c. 2015, 14:51:59
    Author     : Jetta
--%>


<%@page import="mg.cnaps.rapport.RapProjet"%>
<%@page import="mg.cnaps.rapport.RapOperationnelInfo"%>
<%@page import="affichage.PageConsulte"%>

<%
    RapProjet virement;
%>
<%
    virement = new RapProjet();
   virement.setNomTable("RAP_PROJET_LIBELLE");
    String[] libelleVirementFiche = {"id", "Sommaire", "Date debut", "date fin", "Budget Projet","Volume horaire","Jour realisation"};
    PageConsulte pc = new PageConsulte(virement, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setLibAffichage(libelleVirementFiche);
    pc.setTitre("Fiche Projet");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/rapport/projet/projet-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                           <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/rapport/projet/projet-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/rapport/projet/projet-liste.jsp&classe=mg.cnaps.rapport.RapProjet" style="margin-right: 10px">Supprimer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%=pc.getBasPage()%>
</div>

