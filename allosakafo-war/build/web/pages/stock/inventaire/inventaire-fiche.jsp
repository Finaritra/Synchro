<%@page import="java.sql.Date"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%@page import="mg.cnaps.st.StInventaire"%>
<%
    try {
        StInventaire inventaire = new StInventaire();
        inventaire.setNomTable("ST_INVENTAIRE_LIBELLE");
        String[] libelleInventaireFiche = {"ID", "Date", "Designation", "Magasin", "Remarque", "Etat", "Iduser","Type inventaire"};
        PageConsulte pc = new PageConsulte(inventaire, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
        pc.setLibAffichage(libelleInventaireFiche);
        pc.setTitre("Fiche Inventaire");
        StInventaireFille inventairefille = new StInventaireFille();
        inventairefille.setNomTable("ST_INVENTAIRE_FILLE_LIB");
        String inventaireId = " AND INVENTAIRE = '" + request.getParameter("id") + "'";
        StInventaire invt = new StInventaire();
        StInventaire[] base = (StInventaire[]) CGenUtil.rechercher(invt, null, null, " and ID = '" + request.getParameter("id") + "'");

        StInventaireFille[] inventairefilles = (StInventaireFille[]) CGenUtil.rechercher(inventairefille, null, null, inventaireId);
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="javascript:history.go(-1)"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                        <!-- <h1 class="box-title"><a href="<%/*=(String) session.getValue("lien")*/%>?but=stock/inventaire/inventaire-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%/*=pc.getTitre()*/%></h1>-->
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a onclick="imprimer()" class="btn btn-primary pull-right">Imprimer</a>
                            <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/inventaire/inventaire-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/inventaire/inventaire-liste.jsp&classe=mg.cnaps.st.StInventaire" style="margin-right: 10px">Supprimer</a>
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=valider&bute=stock/inventaire/inventaire-liste.jsp&classe=mg.cnaps.st.StInventaire" style="margin-right: 10px">Viser</a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title with-border">
                    <h1 class="box-title">D�tails</h1>
                </div>
                <div class="box-body no padding">
                    <table class="table table-bordered">
                        <tr>
                            <th>ID</th>
                            <th>CODE</th>
                            <th>ARTICLE</th>
                            <th>TYPE</th>
                            <th>Quantit� Th�orique</th>
                            <th>Quantit� Inventori�e</th>
                        </tr>

                        <%
                            StInventaireLibelleEtat inv = new StInventaireLibelleEtat();
                            inv.setNomTable("ST_INVENTAIRE_FILLE_ETAT");
                            String inventId = request.getParameter("id");
                            StInventaireLibelleEtat[] inventair = (StInventaireLibelleEtat[]) CGenUtil.rechercher(inv, null, null, " AND IDINVENTAIRE = '" + inventId + "'");
                            //StArticle[] listeInv = u.getArticleInventaire(inventair[0].getDaty(), inventair[0].getMagasin());
                            String magasin = "%";
                            Date date = Utilitaire.dateDuJourSql();
                            if (base != null && base.length > 0) {
                                magasin = base[0].getMagasin();
                                date = base[0].getDaty();
                            }
                            StockService.setQuantiteTheoriqueInventaire(inventair, date, magasin);
                            boolean inventaireExist = inventair != null;

                            if (inventaireExist) {
                                int taille = inventair.length;
                                for (int i = 0; i < taille; i++) {%>
                        <tr>
                            <td><%=inventair[i].getId()%></td>
                            <td><%=inventair[i].getCode()%></td>
                            <td><%=inventair[i].getArticle()%></td>
                            <td><%=inventair[i].getChapitre()%></td>
                            <td><%=inventair[i].getReste()%></td>
                            <td>
                                <a href="<%=(String) session.getValue("lien") + "?but=stock/inventaire/inventaire-fille-modif.jsp&id=" + inventair[i].getId()%>" title="Modifier"><%=inventair[i].getPump()%></a>
                            </td>
                        </tr>
                        <% }
                        } else {
                        %> <tr>
                            <td colspan="6">Aucun R�sulat<td>
                        </tr>
                        <%}%>
                    </table>
                </div>


            </div> 
        </div> 

    </div> 
    <script language ='JavaScript'>
        function imprimer() {
        <%
            if (inventairefilles.length > 0) {%>
            document.location.replace('<%=(String) session.getValue("lien")%>/../../EtatStockServlet?action=inventaire&id=<%=request.getParameter("id")%>');
        <% } else { %>
                    alert('Aucun article � inventorier');
        <%
            }
        %>
                }
    </script>
</div>
<% } catch (Exception ex) {
        ex.printStackTrace();
        throw new Exception(ex.getMessage());
    }%>