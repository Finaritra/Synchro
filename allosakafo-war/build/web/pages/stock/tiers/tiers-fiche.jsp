<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.st.*" %>
<%
    StTiers tiers = new StTiers();
    tiers.setNomTable("ST_TIERS_INFO");
    //String[] libelleTiersFiche = {"Id", "Raison Social", "Numero Identification Fiscal", "Statistique", "Adresse", "Code Poste", "Taxe", "Rubrique", "Contact", "Matricule CNaPS", "Remarque", "TypeTiers", "Id Comptabilite", "Compte General", "Numero Tiers"};
    PageConsulte pc = new PageConsulte(tiers, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    //pc.setLibAffichage(libelleTiersFiche);
    pc.setTitre("Fiche Tiers");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/tiers/tiers-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                    <div class="box-footer">
                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=stock/tiers/tiers-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                        <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&id=" + request.getParameter("id")%>&acte=delete&bute=stock/tiers/tiers-liste.jsp&classe=mg.cnaps.st.StTiers" style="margin-right: 10px">Supprimer</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>
