<%-- 
    Document   : as-commande-analyse
    Created on : 30 d�c. 2016, 04:57:15
    Author     : Joe
--%>
<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.stock.MvtStockDec"%>

<%@ page import="user.*" %>
<%@page import="affichage.Liste"%>
<%@ page import="bean.*" %>
<%@page import="bean.TypeObjet"%>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@page import="lc.Direction"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

<%
try{    
    MvtStockDec mvt = new MvtStockDec();
    
    
    
    String listeCrt[] = {"ingredients","unite","idingredients","point","daty","categorieIngredient"};
    String listeInt[] = {"daty"};
    String[] pourcentage = {"nombrepargroupe"};
    String colDefaut[] = {"ingredients","unite","categorieIngredient","point"}; 
    String somDefaut[] = {"entree", "sortie","ecart"};
    
    PageRechercheGroupe pr = new PageRechercheGroupe(mvt, request, listeCrt, listeInt, 3,colDefaut, somDefaut, pourcentage, 5, 3);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    

    
    String apreswhere = "";
    if(request.getParameter("daty1") == null || request.getParameter("daty2") == null){
        apreswhere = "and daty = '"+utilitaire.Utilitaire.dateDuJour()+"'";
    }
    
    
    pr.setAWhere(apreswhere);
    pr.getFormu().getChamp("daty1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("daty2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("point").setDefaut(AlloSakafoService.getNomRestau());
    pr.setNpp(500);
    pr.setApres("stock/mvtStock/analyseStDecompose.jsp");
    pr.creerObjetPage();
%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Analyse STOCK DECOMPOSE</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/analyseStDecompose.jsp" method="post" name="analyse" id="analyse">
            <%out.println(pr.getFormu().getHtmlEnsemble());%>
        </form>
        
        <%
           out.println(pr.getTableauRecap().getHtml());%>
           
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%
    }catch(Exception e){
        e.printStackTrace();
    }
%>