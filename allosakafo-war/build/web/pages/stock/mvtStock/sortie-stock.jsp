<%@page import="service.AlloSakafoService"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.stock.*"%>
<%@page import="mg.allosakafo.produits.Produits"%>
<%@page import="mg.allosakafo.produits.Ingredients"%>



<%
    String idcategorie = request.getParameter("idcategorieingredient");
    String autreparsley = "data-parsley-range='[8, 40]' required";
    MvtStock cmd = new MvtStock();
    PageInsert pi = new PageInsert(cmd, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));
    UserEJB u = (UserEJB) session.getAttribute("u");
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("daty").setLibelle("Date");
   
    pi.getFormu().getChamp("typemvt").setVisible(false);
    pi.getFormu().getChamp("depot").setLibelle("Point");
    affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet c= new TypeObjet();
    c.setNomTable("point");
    liste[0] = new Liste("depot", c, "val", "id");
    liste[0].setDefaut(AlloSakafoService.getIdRestau(null).getId());
	
    pi.getFormu().changerEnChamp(liste);
	pi.getFormu().getChamp("typebesoin").setVisible(false);
	pi.getFormu().getChamp("etat").setVisible(false);
	pi.getFormu().getChamp("fournisseur").setVisible(false);
	pi.getFormu().getChamp("beneficiaire").setVisible(false);
	pi.getFormu().getChamp("numbc").setVisible(false);
	pi.getFormu().getChamp("numbs").setVisible(false);
    if(request.getParameter("daty")!=null&&request.getParameter("daty").compareToIgnoreCase("")!=0)pi.getFormu().getChamp("daty").setDefaut(request.getParameter("daty"));
    pi.getFormu().getChamp("designation").setDefaut(request.getParameter("designation"));
    pi.getFormu().getChamp("observation").setDefaut(request.getParameter("observation"));
    if(request.getParameter("depot")!=null&&request.getParameter("depot").compareToIgnoreCase("")!=0)pi.getFormu().getChamp("depot").setDefaut(request.getParameter("depot"));
    pi.preparerDataFormu();
    TypeObjet crtIng = new TypeObjet();
    crtIng.setNomTable("categorieingredient");
    TypeObjet[]listeIng=(TypeObjet[])CGenUtil.rechercher(crtIng, null, null, "");
%>
<script  language="JavaScript">
	function calculerTotal(indice) {
        var quantite, pu, montant;
        quantite = parseFloat($('#quantite' + indice).val());
        pu = parseFloat($('#quantiteparpack' + indice).val());
        if (!isNaN(quantite) && !isNaN(pu)) {
            montant = quantite * pu;
            $('#total' + indice).val(montant.toFixed(2));
        } else {
            $('#total' + indice).val('');
        }
    }
</script>
<div class="content-wrapper">
    <h1 class="box-title">Enregistrer Sortie stock</h1>
    <form action="<%=pi.getLien()%>?but=stock/mvtStock/apresMvt.jsp" method="post" name="inventaire" id="inventaire" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertCommande();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <button type="button" onclick="afficher()" class="btn btn-primary col-md-offset-6">Afficher</button>
        <div>
        Categorie : 
                    <select name="idcategorieingredient" class="champ" id="idcategorieingredient">
                        <%
                        for(TypeObjet ingr:listeIng){
                        %>
                        <% if (request.getParameter("idcategorieingredient") != null && request.getParameter("idcategorieingredient").compareToIgnoreCase(ingr.getId()) == 0) {%>    
                        <option value="<%=ingr.getId() %>" selected><%=ingr.getVal() %></option>
                        <% } else { %>
                        <option value="<%=ingr.getId() %>" ><%=ingr.getVal() %></option>
                        <% }} %>
                    </select>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <div class="box">
                        <div class="box-title with-border">
                            <h1 class="box-title">D&eacute;tails</h1>
                        </div>
                        <div class="box-body table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th style="background-color:#bed1dd">ID</th>
                                        <th style="background-color:#bed1dd">Libelle</th>
                                        <th style="background-color:#bed1dd">Unite</th>
                                       <!-- <th style="background-color:#bed1dd">Quantite par pack</th>-->
                                        <th style="background-color:#bed1dd">Quantite</th>
									<!--	<th style="background-color:#bed1dd">Total</th>-->
                                    </tr></thead>
                                <tbody>
                                    <%
                                        Ingredients p = new Ingredients();
                                        p.setNomTable("AS_INGREDIENTS_LIB_SAISIE");
                                        if(idcategorie != null&&!idcategorie.isEmpty())p.setCategorieingredient(idcategorie);
                                        Ingredients[] listef = (Ingredients[]) CGenUtil.rechercher(p, null, null, " order by id asc");
                                         for (int i = 0; i < listef.length; i++) {
                                    %>
                                <input type="hidden" class="form form-control" name="nb" id="nb" value="<%=listef.length%>">
                                <tr>
                                    <td><input type="hidden" class="form form-control" name="ids" id="id<%=i%>" value="<%=listef[i].getId()%>"><%=listef[i].getId()%></td>
                                    <td><%=listef[i].getLibelle()%></td>
                                    <td><%=listef[i].getUnite()%></td>
                                   <!-- <td><input type="text" class="form form-control" name="quantiteparpack" id="quantiteparpack<%=i%>" value="<%=listef[i].getQuantiteparpack()%>" onblur="calculerTotal(<%=i%>)"></td>-->
                                    <td><input type="text" class="form form-control" name="quantite" id="quantite<%=i%>" value="0" onblur="calculerTotal(<%=i%>)"></td>
								<!--	<td><input type="text" class="form form-control" name="total" id="total<%=i%>" readonly="true"></td>-->
                                </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
                                
        <input name="acte" type="hidden" id="nature" value="insertMvt">
		<input name="type" type="hidden" id="type" value="1">
        <input name="bute" type="hidden" id="bute" value="stock/mvtStock/asMvtStock-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.stock.MvtStock">
        <button type="submit" name="Submit2" class="btn btn-success pull-right" style="margin-right: 25px;">Enregistrer</button>
        <button type="reset" name="Submit2" class="btn btn-default pull-right" style="margin-right: 15px;">R&eacute;initialiser</button>
    </form>
    <script>
        function afficher() {
            window.location.href = "/phobo/pages/module.jsp?but=stock/mvtStock/sortie-stock.jsp&observation="+document.getElementById("observation").value+"&depot="+document.getElementById("depot").value+"&designation="+document.getElementById("designation").value+"&daty="+document.getElementById("daty").value+"&idcategorieingredient="+document.getElementById("idcategorieingredient").value;
        }
    </script>
</div>