<%-- 
    Document   : entree-stock-multiple
    Created on : 16 f�vr. 2021, 10:41:09
    Author     : Sanda
--%>

<%@page import="service.AlloSakafoService"%>
<%@page import="affichage.Liste"%> 
<%@page import="bean.TypeObjet"%> 
<%@page import="user.UserEJB"%> 
<%@page import="utilitaire.Utilitaire"%> 
<%@page import="affichage.PageInsertMultiple"%>
<%@page import="mg.allosakafo.stock.MvtStock"%>
<%@page import="mg.allosakafo.stock.MvtStockFille"%>

<%
    try {
        String autreparsley = "data-parsley-range='[8, 40]' required";
        UserEJB u = u = (UserEJB) session.getValue("u");
        String classeMere = "mg.allosakafo.stock.MvtStock",
               classeFille = "mg.allosakafo.stock.MvtStockFille",
               titre = "Enregistrer entr�e stock",
			   redirection = "stock/mvtStock/asMvtStock-fiche.jsp",
			   colonneMere = "idmvtstock",
               nomSaisie = "entreeStock";
        int taille = 10;
        
        MvtStock mere = new MvtStock();
        MvtStockFille fille = new MvtStockFille();
        PageInsertMultiple pi = new PageInsertMultiple(mere, fille, request, taille, u);
        pi.setLien((String) session.getValue("lien"));

        pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
        pi.getFormu().getChamp("daty").setLibelle("Date");
        pi.getFormu().getChamp("typemvt").setVisible(false);
        pi.getFormu().getChamp("depot").setLibelle("Point");
        affichage.Champ[] liste = new affichage.Champ[1];
        TypeObjet c= new TypeObjet();
        c.setNomTable("point");
        liste[0] = new Liste("depot", c, "val", "id");
        liste[0].setDefaut(AlloSakafoService.getIdRestau(null).getId());	
        pi.getFormu().changerEnChamp(liste);
        pi.getFormu().getChamp("typebesoin").setVisible(false);
        pi.getFormu().getChamp("etat").setVisible(false);
        pi.getFormu().getChamp("fournisseur").setVisible(false);
        pi.getFormu().getChamp("beneficiaire").setVisible(false);
        pi.getFormu().getChamp("numbc").setVisible(false);
        pi.getFormu().getChamp("numbs").setVisible(false);
        if(request.getParameter("daty")!=null&&request.getParameter("daty").compareToIgnoreCase("")!=0)pi.getFormu().getChamp("daty").setDefaut(request.getParameter("daty"));
        pi.getFormu().getChamp("designation").setDefaut(request.getParameter("designation"));
        pi.getFormu().getChamp("observation").setDefaut(request.getParameter("observation"));
        if(request.getParameter("depot")!=null&&request.getParameter("depot").compareToIgnoreCase("")!=0)pi.getFormu().getChamp("depot").setDefaut(request.getParameter("depot"));

        for( int i = 0 ; i < taille ; i++ ){
            pi.getFormufle().getChamp("Idunite_"+i).setAutre("readonly");
            pi.getFormufle().getChamp("Ingredients_"+i).setAutre("readonly");
            pi.getFormufle().getChamp("Ingredients_"+i).setPageAppel("choix/listeIngredientChoix.jsp","ingredients_"+i+";ingredientslibelle_"+i+";idunite_"+i);
        }
        pi.getFormufle().getChamp("Entree_0").setLibelle("Quantite");
        pi.getFormufle().getChamp("Ingredients_0").setLibelle("Ingr�dient");
        pi.getFormufle().getChamp("Idunite_0").setLibelle("Unite");
        affichage.Champ.setVisible(pi.getFormufle().getChampFille("Id"), false);
        affichage.Champ.setVisible(pi.getFormufle().getChampFille("Idmvtstock"), false);
        affichage.Champ.setVisible(pi.getFormufle().getChampFille("Sortie"), false);
        affichage.Champ.setVisible(pi.getFormufle().getChampFille("Solde"), false);
        affichage.Champ.setVisible(pi.getFormufle().getChampFille("Pu"), false);
        affichage.Champ.setVisible(pi.getFormufle().getChampFille("Idmvt"), false);
        affichage.Champ.setVisible(pi.getFormufle().getChampFille("Stock_init"), false);

        pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1><%=titre%></h1>
    <form class='container' action="<%=pi.getLien()%>?but=apresMultiple.jsp" method="post" name="<%=nomSaisie%>" id="<%=nomSaisie%>">
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            pi.getFormufle().makeHtmlInsertTableauIndex();
            out.println(pi.getFormu().getHtmlInsert());
            out.println(pi.getFormufle().getHtmlTableauInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="<%=redirection%>">
        <input name="classe" type="hidden" id="classe" value="<%=classeMere%>">
        <input name="classefille" type="hidden" id="classefille" value="<%=classeFille%>">
        <input name="nombreLigne" type="hidden" id="nombreLigne" value="<%=pi.getNombreLigne()%>">
        <input name="colonneMere" type="hidden" id="colonneMere" value="<%=colonneMere%>">
    </form>
</div>

<%
	} catch (Exception e) {
		e.printStackTrace();
%>
    <script language="JavaScript">
        alert('<%=e.getMessage()%>');
        history.back();
    </script>
<% }%>
