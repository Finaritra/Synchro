<%@page import="mg.allosakafo.stock.*"%>
<%@page import="affichage.PageRecherche"%>
<% 
    MvtStockDetailComplet lv = new MvtStockDetailComplet();

    lv.setNomTable("MvtStockDetLibComplet");
    if(request.getParameter("table")!=null&&request.getParameter("table").compareTo("")!=0)lv.setNomTable(request.getParameter("table"));
    String listeCrt[] = {"id", "ingredients", "entree", "sortie","daty","depot","numbs","numbc"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id","idmvtstock","numbs","daty", "ingredients", "entree", "sortie","pu","depot","numbc"};

    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("numbs").setLibelle("plat");
    pr.getFormu().getChamp("numbc").setLibelle("origine");
    pr.getFormu().getChamp("daty1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("daty2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    
    String apreswhere = "";
    if(request.getParameter("daty1") == null && request.getParameter("daty2") == null){
        apreswhere = "and daty = '"+utilitaire.Utilitaire.dateDuJour()+"'";
    }
    pr.setAWhere(apreswhere);
    pr.setApres("stock/mvtStock/mvtStockFilleComplet-liste.jsp");
    String[] colSomme = {"entree","sortie"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste mouvement stock Fille</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=stock/mvtStock/mvtStockFilleComplet-liste.jsp" method="post" name="incident" id="incident">
            <%

                out.println(pr.getFormu().getHtmlEnsemble());
            %>
            <div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("MvtStockDetLibComplet") == 0) {%>    
                        <option value="MvtStockDetLibComplet" selected>Tous</option>
                        <% } else { %>
                        <option value="MvtStockDetLibComplet" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("MVTSTOCKDETLIBCOMPLETvalide") == 0) {%>    
                        <option value="MVTSTOCKDETLIBCOMPLETvalide" selected>valide</option>
                        <% } else { %>
                        <option value="MVTSTOCKDETLIBCOMPLETvalide" >valide</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("MVTSTOCKDETLIBCOMPLETcree") == 0) {%>    
                        <option value="MVTSTOCKDETLIBCOMPLETcree" selected>cree</option>
                        <% } else { %>
                        <option value="MVTSTOCKDETLIBCOMPLETcree" >cree</option>
                        <% } %>
                        
				
                    </select>
                </div>
            </div>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=stock/mvtStock/mvtStockFille-fiche.jsp",pr.getLien() + "?but=stock/mvtStock/asMvtStock-fiche.jsp"};
            String colonneLien[] = {"id","idmvtstock"};
            String varColonneLien[]={"id","id"};
            pr.getTableau().setVariableLien(varColonneLien);
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
