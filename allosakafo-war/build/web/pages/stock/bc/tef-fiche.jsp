<%@ page import="user.*" %>
<%@ page import="mg.cnaps.treso.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="mg.cnaps.budget.*" %>

<%
    BudgetTef tef;
    UserEJB u = (UserEJB) session.getAttribute("u");
    String idtef = request.getParameter("id");
    String tefId = " AND TEF = '" + idtef + "'";
    BudgetTefFille teffille = new BudgetTefFille();
    BudgetTefFille[] teffilleliste = (BudgetTefFille[]) u.getData(teffille, null, null, null, tefId);
    String id = request.getParameter("id");
    tef = new BudgetTef();
    tef.setNomTable("STBUDGET_TEF_LIBELLE");
    String[] libelleTefFiche = {"Id", "Code", "Date", "Libell�", "Montant", "Id B�n�ficiaire", "Type B�n�ficiaire", "Nom B�n�ficiaire", "CIN B�n�ficiaire", "Code Banque Succursale", "Code Banque", "Code Agence", "Num�ro Compte", "Cl�", "Date", "Remarque", "Date CIN B�n�ficiaire", "Type TEF", "Etat", "Budget", "TEF li�", "Frais", "Utilisateur"};
    PageConsulte pc = new PageConsulte(tef, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    pc.setLibAffichage(libelleTefFiche);
    pc.setTitre("Fiche Tef");
    pc.setCanUploadFile(true);
    pc.setFichier_table_cible("BUDGET_PJ_TEF");
    pc.setFichier_table_procedure("getSeqBUDGETPJTEF");
    pc.getChampByName("iduser").setVisible(false);
    BudgetTef tefObject = (BudgetTef) pc.getBase();
    pc.setLien("personne/personne-pdf.jsp");
    pc.setDonnee(pc.getHtml());

    double val = 0;
    if (tefObject.getEtat() <= 9 && tefObject.getEtat() >= 1) {
        val = tefObject.getMontant();
    }

    BudgetTef ttef = new BudgetTef();
    ttef.setNomTable("BUDGET_TEF_LIBELLE_FICHE");
    ttef.setId(idtef);
    BudgetTef btef = (BudgetTef) CGenUtil.rechercher(ttef, null, null, "")[0];

    LcTef union = new LcTef();
    union.setId(idtef);
    LcTef[] rstBDV = (LcTef[]) CGenUtil.rechercher(union, null, null, "");

    TresoOpTef[] tots = (TresoOpTef[]) u.getData(new TresoOpTef(), null, null, null, " AND TEF = '"+idtef+"'");
    BudgetDepense dep = new BudgetDepense();
    dep.setNomTable("budget_depense_projetLib");
    dep.setId(tefObject.getBudget());
    BudgetDepense[] depl = (BudgetDepense[]) CGenUtil.rechercher(dep, null, null, "");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=stock/bc/tef-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td class="left">Id</td>
                                        <td><%=btef.getId()%></td>
                                    </tr>
                                    <tr>
                                        <td class="left">Code</td>
                                        <td><%=btef.getCode()%></td>
                                    </tr>
                                    <tr>
                                        <td class="left">Date</td>
                                        <td><%=Utilitaire.formatterDaty(btef.getDaty())%></td>
                                    </tr>
                                    <tr>
                                        <td class="left">Libell�</td>
                                        <td><%=btef.getLibelle()%></td>
                                    </tr>
                                    <tr>
                                        <td class="left">Montant</td>
                                        <td><%=Utilitaire.formaterAr(btef.getMontant())%></td>
                                    </tr>
                                    <tr>
                                        <td class="left">Id Beneficiaire</td>
                                        <td><%=btef.getIdbeneficiaire()%></td>
                                    </tr>
                                    <tr>
                                        <td class="left">Type Beneficiaire</td>
                                        <td><%=btef.getTypebeneficiaire() %></td>
                                    </tr>
                                    <tr>
                                        <td class="left">Nom Beneficiaire</td>
                                        <td><%=btef.getNombeneficiaire()%></td>
                                    </tr>
                                    <tr>
                                        <td class="left">CIN Beneficiaire</td>
                                        <td><%=btef.getCinbeneficiaire()%></td>
                                    </tr>
                                    <tr>
                                        <td class="left">Code Banque Succursale</td>
                                        <td><%=btef.getBanquesuccursalecode()%></td>
                                    </tr>
                                    <tr>
                                        <td class="left">Code Banque</td>
                                        <td><%=btef.getBanquecode()%></td>
                                    </tr>
                                    <tr>
                                        <td class="left">Code Agence</td>
                                        <td><%=btef.getBanqueagencecode()%></td>
                                    </tr><tr>
                                        <td class="left">Numero Compte</td>
                                        <td><%=btef.getBanquecomptenumero()%></td>
                                    </tr><tr>
                                        <td class="left">Cle</td>
                                        <td><%=btef.getBanquecomptecle()%></td>
                                    </tr><tr>
                                        <td class="left">Date CIN Beneficiaire</td>
                                        <td><%=Utilitaire.champNull(Utilitaire.formatterDaty(btef.getDatecinbeneficiaire()))%></td>
                                    </tr><tr>
                                        <td class="left">Remarque</td>
                                        <td><%=btef.getRemarque()%></td>
                                    </tr><tr>
                                        <td class="left">Type TEF</td>
                                        <td><%=btef.getTypetef()%></td>
                                    </tr><tr>
                                        <td class="left">Etat</td>
                                        <td><%=tef.getEtatText(btef.getEtat())%></td>
                                    </tr><tr>
                                        <td class="left">Budget</td>
                                        <td><%=btef.getBudget()%></td>
                                    </tr><tr>
                                        <td class="left">TEF lie</td>
                                        <td><%=btef.getTefliee()%></td>
                                    </tr><tr>
                                        <td class="left">Frais</td>
                                        <td><%=Utilitaire.formaterAr(btef.getFrais())%></td>
                                    </tr><tr>
                                        <td class="left">TVA</td>
                                        <td><%=Utilitaire.formaterAr(btef.getMontant_tva())%></td>
                                    </tr>
                                <input type="hidden" name="id" value="<%=btef.getId()%>">
                                </tbody>
                            </table>

                            <br/>
                            <div class="box-footer">
                                <a class="btn btn-warning pull-right" href="paie.jsp?but=stock/bc/tef-update-budget.jsp&id=<%=btef.getId()%>" style="margin-right: 10px" >Modifier</a>
                                <a class="btn btn-danger pull-right" href="paie.jsp?but=apresTarif.jsp&id=<%=btef.getId()%>&acte=delete&bute=stock/bc/tef-liste.jsp&classe=mg.cnaps.budget.BudgetTef" style="margin-right: 10px">Supprimer</a>
                                <a class="btn btn-warning pull-right" href="paie.jsp?but=apresTarif.jsp&id=<%=btef.getId()%>&acte=annuler&bute=stock/bc/tef-liste.jsp&classe=mg.cnaps.budget.BudgetTef" style="margin-right: 10px">Annuler</a>
                                <a class="btn btn-info pull-right" href="paie.jsp?but=apresTarif.jsp&id=<%=btef.getId()%>&acte=cloturer&bute=stock/bc/tef-liste.jsp&classe=mg.cnaps.budget.BudgetTef" style="margin-right: 10px">Cloturer</a>

                                <a class="btn btn-default pull-right" href="paie.jsp?but=apresTarif.jsp&id=<%=btef.getId()%>&acte=dupliquer&nomColonneMere=TEF&nomClasseFille=mg.cnaps.budget.BudgetTefFille&bute=stock/bc/tef-liste.jsp&classe=mg.cnaps.budget.BudgetTef" style="margin-right: 10px">Extourner</a>

                                <a class="btn btn-success pull-right" href="paie.jsp?but=apresTarif.jsp&id=<%=btef.getId()%>&acte=valider&bute=stock/bc/tef-liste.jsp&classe=mg.cnaps.budget.BudgetTef" style="margin-right: 10px">Viser</a>
				<%if(btef.getEtat()>1 && tots.length!=0){%>
				<a class="btn btn-default pull-right" href="paie.jsp?but=tresorerie/OP/op-fiche.jsp&id=<%=tots[0].getOp()%>" style="margin-right: 10px">Voir OP lI&Eacute;</a>
				<%}%>
                                <a class="btn btn-danger pull-right" style="margin-right: 10px" onclick="verifTef'(<%=btef.getEtat()%>)'">Rejeter</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title">D�tails</h1>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>D�signation</th>
                                <th>Quantit�</th>
                                <th>PU</th>
                            </tr>
                            <%for (int i = 0; i < teffilleliste.length; i++) {%>
                            <tr>
                                <td><%=teffilleliste[i].getId()%></td>
                                <td><%=teffilleliste[i].getCode()%></td>
                                <td><%=teffilleliste[i].getDesignation()%></td>
                                <td><%=Utilitaire.formaterAr(teffilleliste[i].getQuantite())%></td>
                                <td align="right"><%=Utilitaire.formaterAr(teffilleliste[i].getPu())%></td>
                            </tr>
                            <% }%>
                        </table>
                    </div>
                </div>
            </div>

            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h2 class="box-title">Ligne budg&eacute;taire</h2>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <% if (rstBDV.length == 0) {
                                    
                                    if (depl.length > 0) {
                            
                            %>
                            <tr>
                                <th>Projet : </th>
                                <td><%=utilitaire.Utilitaire.champNull(depl[0].getProjet())%></td>
                            </tr>
                            <tr>
                                <th>Credit : </th>
                                <td><%=utilitaire.Utilitaire.formaterAr(depl[0].getMontant_credit())%></td>
                            </tr>
                            <tr>
                                <th>Engag�s : </th>
                                <td><%=utilitaire.Utilitaire.formaterAr(depl[0].getEngage())%></td>
                            </tr>
                            <tr>
                                <th>Disponible : </th>
                                <td><%=utilitaire.Utilitaire.formaterAr(depl[0].getMontant_credit() - dep.getEngage())%></td>
                            </tr>
                            <tr>
                                <th>Montant demand� : </th>
                                <td><%=utilitaire.Utilitaire.formaterAr(tefObject.getMontant())%></td>
                            </tr>
                            <tr>
                                <th>Dispo. apr�s TEF : </th>
                                <td><%=utilitaire.Utilitaire.formaterAr((depl[0].getMontant_credit() - depl[0].getEngage()) - val)%></td>
                            </tr>
                            <% }
                            } else {

                            %>
                            <tr>
                                <th>Code projet</th>
                                <th>Intitul�</th>
                                <th>Compte</th>
                                <th>Credit</th>
                                <th>Engag�s</th>
                                <th>Disponible</th>
                                <th>Montant demand�</th>
                                <th>Dispo. apr�s TEF</th>
                            </tr>
                            <% for (int i = 0; i < rstBDV.length; i++) {%>
                            <tr>
                                <td><%=rstBDV[i].getCode()%></td>
                                <td><%=rstBDV[i].getIntitule()%></td>
                                <td><%=rstBDV[i].getCompte()%></td>
                                <td><%=utilitaire.Utilitaire.formaterAr(rstBDV[i].getMontant_credit())%></td>
                                <td><%=utilitaire.Utilitaire.formaterAr(rstBDV[i].getEngage())%></td>
                                <td><%=utilitaire.Utilitaire.formaterAr(rstBDV[i].getMontant_credit() - rstBDV[i].getEngage())%></td>
                                <td><%=utilitaire.Utilitaire.formaterAr(rstBDV[i].getMontantmere())%></td>
                                <td><%=utilitaire.Utilitaire.formaterAr((rstBDV[i].getMontant_credit() - rstBDV[i].getEngage()) - rstBDV[i].getMontantmere())%></td>
                            </tr>
                            <% }
                                }%>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%
        pc.setNomColonneMere("tef");
        pc.setNomClasseFilleADuplique("mg.cnaps.budget.BudgetTefFille");
        out.println(pc.getBasPage());
    %>
</div>
<script>
    function verifTef(et) {
        if (et >= 11) {
            alert('Impossible de rejeter Tef. TEF d�ja vis� ');
        } else {
            document.location.href = 'href="paie.jsp?but=budget/tef/rejettef-saisie.jsp&id=<%=btef.getId()%>" ';
        }
    }

</script>