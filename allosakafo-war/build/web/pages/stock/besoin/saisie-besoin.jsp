
<%@page import="affichage.Liste"%> 
<%@page import="affichage.PageInsert"%> 
<%@page import="bean.TypeObjet"%> 
<%@page import="mg.allosakafo.achat.Besoin"%>
<%@page import="user.UserEJB"%> 

<%
    try{
    Besoin pj = new Besoin();
    PageInsert pi = new PageInsert(pj, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));

    affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet liste1 = new TypeObjet();
    liste1.setNomTable("point");
    liste[0] = new Liste("remarque", liste1, "val", "id");
    pi.getFormu().changerEnChamp(liste);
     
    String pointDefaut = service.AlloSakafoService.getNomRestau();
    pi.getFormu().getChamp("remarque").setDefaut(pointDefaut);
	
    pi.getFormu().getChamp("idproduit").setPageAppel("choix/ChoixProduitEtIngredients.jsp");
    
    pi.getFormu().getChamp("idproduit").setLibelle("Designation Plat");
    pi.getFormu().getChamp("quantite").setLibelle("Quantite");
    pi.getFormu().getChamp("remarque").setLibelle("Point");   
    pi.getFormu().getChamp("idproduit").setAutre("readonly");

    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Saisie Besoin </h1>
    <!--  -->
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="sbesoin" id="sbesoin">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>

    <input name="acte" type="hidden" id="nature" value="insert">
    <input name="bute" type="hidden" id="bute" value="stock/besoin/saisie-besoin.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.achat.Besoin">
    <input name="nomtable" type="hidden" id="nomtable" value="besoin">
</form>
</div>
<%
} catch (Exception e) {
    e.printStackTrace();
%>
<script language="JavaScript"> 
	alert('<%=e.getMessage()%>');
    history.back();</script>
<% }%>
