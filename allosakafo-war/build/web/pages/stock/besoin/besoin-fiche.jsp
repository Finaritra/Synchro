
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@page import="affichage.PageConsulte"%>
<%@page import="mg.allosakafo.achat.Besoin"%>
<%
    Besoin besoin = new Besoin();
    besoin.setNomTable("BESOINLIBPOINT");
    PageConsulte pc = pc = new PageConsulte(besoin, request, (user.UserEJB) session.getValue("u"));//ou avec argument liste Libelle si besoin
    
    /*pc.getChampByName("id").setLibelle("ID");
    pc.getChampByName("unite").setLibelle("Unit?");
    pc.getChampByName("typearticle").setLibelle("Type");
    pc.getChampByName("groupee").setLibelle("Groupe");
    pc.getChampByName("sousgroupe").setLibelle("Sous groupe");
    */
    
    pc.getChampByName("idproduit").setLibelle("Produit");
    pc.getChampByName("remarque").setLibelle("Point");
    pc.setTitre("Besoin");


%>
<div class="content-wrapper">
      <h1 class="box-title">Besoin fiche</h1>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><!--<a href="<%//=(String) session.getValue("lien")%>?but=appel/as-appel-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%//=pc.getTitre()%>--></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <div class="box-footer">
                            <a class="btn btn-warning pull-left"  href="<%=(String) session.getValue("lien") + "?but=stock/besoin/modif-besoin.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <% //out.println(pc.getBasPage());%>
</div>
