<%-- 
    Document   : modif-besoin
    Created on : 5 mai 2020, 12:07:26
    Author     : Sandratra
--%>

<%@page import="service.AlloSakafoService"%>
<%@page import="mg.allosakafo.achat.Besoin"%>
<%@page import="user.*"%> 
<%@page import="bean.TypeObjet" %>
<%@page import="affichage.*"%>

<!--FICHIER TOJO And-->
<%
    try{
    String autreparsley = "data-parsley-range='[8, 40]' required";
    Besoin  bs = new Besoin();
    PageUpdate pi = new PageUpdate(bs, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));   
    
    pi.getFormu().getChamp("idproduit").setPageAppel("choix/ChoixProduitEtIngredients.jsp");
    
    pi.getFormu().getChamp("idproduit").setLibelle("Designation Plat");
    pi.getFormu().getChamp("quantite").setLibelle("Quantite");
    
    affichage.Champ[] liste = new affichage.Champ[1];
    TypeObjet d1 = new TypeObjet();
    d1.setNomTable("point");
    liste[0] = new Liste("remarque", d1, "val", "id");
    pi.getFormu().changerEnChamp(liste);
//    pi.getFormu().getChamp("remarque").setDefaut(AlloSakafoService.getIdRestau(null).getId());
    pi.getFormu().getChamp("remarque").setLibelle("Point");
    pi.getFormu().getChamp("idproduit").setAutre("readonly");

    
    pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1>Modification besoin</h1>
    <!--  -->
    <form action="<%=(String) session.getValue("lien")%>?but=apresTarif.jsp&id=<%out.print(request.getParameter("id"));%>" method="post" name="sbesoin" id="sbesoin">
    <%
        pi.getFormu().makeHtmlInsertTabIndex();
        out.println(pi.getFormu().getHtmlInsert());
    %>
    <input name="acte" type="hidden" id="nature" value="update">
    <input name="bute" type="hidden" id="bute" value="stock/besoin/besoin-fiche.jsp">
    <input name="classe" type="hidden" id="classe" value="mg.allosakafo.achat.Besoin">
    </form>
</div>
<%
    }catch(Exception e){
e.printStackTrace();
}
%>