<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.fin.MvtCaisse"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%@page import="affichage.PageRecherche"%>

<% 
    String nomTable="MVTCAISSELETTREGROUPEMERE";
    MvtCaisse lv = new MvtCaisse();
    if(request.getParameter("table")!=null && request.getParameter("table").compareToIgnoreCase("")!=0) nomTable=request.getParameter("table");
    
    lv.setNomTable(nomTable);
	
    String listeCrt[] = {"numPiece", "idcaisse", "daty"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"daty", "numpiece", "idcaisse","tiers", "debit", "credit"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
        
      
       
    pr.getFormu().getChamp("daty1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("daty2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    
    pr.setApres("fin/mvtcaisse-listegroupe.jsp");
    String[] colSomme = {"debit", "credit"};
    pr.setNpp(150);
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste mouvement de caisse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=fin/mvtcaisse-listegroupe.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=fin/mvtcaisse-fiche.jsp", pr.getLien()};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            //String libEnteteAffiche[] = {"Id", "Designation", "Devise", "Caisse", "N� piece", "N� cheque", "Date", "Date valeur", "Montant debit", "Montant credit","Montant reste"};
            //pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            pr.rajoutLienBasPage("table",nomTable);
            out.println(pr.getBasPage());

        %>
    </section>
</div>