<%@page import="mg.allosakafo.fin.MvtIntraCaisse"%>
<%@ page import="user.*"%>
<%@ page import="utilitaire.*"%>
<%@ page import="bean.*" %>
<%@ page import="affichage.*"%>
<%
    try {
        MvtIntraCaisse a = new MvtIntraCaisse();
        PageUpdate pi = new PageUpdate(a, request, (user.UserEJB) session.getValue("u"));
        pi.setLien((String) session.getValue("lien"));
        UserEJB u = (UserEJB) session.getAttribute("u");


        pi.preparerDataFormu();
%>
<div class="content-wrapper">
    <h1 class="box-title">Modification mouvement caisse interne</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="appro" id="appro">
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="acte" value="update">
        <input name="bute" type="hidden" id="bute" value="fin/mvtintracaisse-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.fin.MvtIntraCaisse">
    </form>
</div>

<%} catch (Exception ex) {
        ex.printStackTrace();
    }%>