<%-- 
    Document   : mvt-intra-caisse-liste
    Created on : 20 nov. 2019, 09:54:51
    Author     : john
--%>

<%@page import="mg.allosakafo.fin.MvtIntraCaisse"%>
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%@page import="affichage.PageRecherche"%>

<% 
    String nomTable="MVTINTRACAISSELIB";
    MvtIntraCaisse lv = new MvtIntraCaisse();
    if(request.getParameter("table")!=null && request.getParameter("table").compareToIgnoreCase("")!=0) nomTable=request.getParameter("table");
    
    lv.setNomTable(nomTable);
	
    String listeCrt[] = {"id", "caissedepart","caissearrive","remarque","daty"};
    String listeInt[] = {"daty"};
    String libEntete[] = {"id", "caissedepart","caissearrive","remarque","daty","montant"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, libEntete.length);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
        
      
    pr.getFormu().getChamp("caissedepart").setLibelleAffiche("Caisse d�part");
    pr.getFormu().getChamp("caissearrive").setLibelleAffiche("Caisse arriv�e");
    
    /*pr.getFormu().getChamp("daty1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("daty2").setDefaut(utilitaire.Utilitaire.dateDuJour());*/
    
    pr.setApres("fin/mvt-intra-caisse-liste.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.mvtintra.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste des mouvements intra caisse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=fin/mvtintracaisse-liste.jsp" method="post" name="mvtintra" id="mvtintra">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
<div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                         <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("MVTINTRACAISSELIB") == 0) {%>    
                        <option value="MVTINTRACAISSELIB" selected>Tous</option>
                        <% } else { %>
                        <option value="MVTINTRACAISSELIB" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("MVTINTRACAISSELIBCREE") == 0) {%>    
                        <option value="MVTINTRACAISSELIBCREE" selected>Cr&eacute;e</option>
                        <% } else { %>
                        <option value="MVTINTRACAISSELIBCREE" >Cr&eacute;e</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("MVTINTRACAISSELIBVALIDE") == 0) {%>    
                        <option value="MVTINTRACAISSELIBVALIDE" selected>Valid&eacute;e</option>
                        <% } else { %>
                        <option value="MVTINTRACAISSELIBVALIDE" >Valid&eacute;e</option>
                        <% } %>
                        
				
                    </select>
                </div>
            </div>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=fin/mvtintracaisse-fiche.jsp", pr.getLien()};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Caisse depart","Caisse arrivee","Remarque","Date","Montant"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());

        %>
    </section>
</div>