<%-- 
    Document   : as-paiement-saisie
    Created on : 1 d�c. 2016, 15:42:08
    Author     : N
--%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%@ page import="utilitaire.*" %>
<%@page import="mg.allosakafo.ded.Report"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%@page import="mg.allosakafo.fin.MvtIntraCaisse"%>


<%
    try{
    String autreparsley = "data-parsley-range='[8, 40]' required";
    MvtIntraCaisse  r = new MvtIntraCaisse();
    PageInsert pi = new PageInsert(r, request, (user.UserEJB) session.getValue("u"));
    pi.setLien((String) session.getValue("lien"));    
    pi.getFormu().getChamp("daty").setDefaut(Utilitaire.dateDuJour());
    pi.getFormu().getChamp("daty").setLibelle("Date");
    pi.getFormu().getChamp("caissedepart").setLibelle("Caisse d�part");
    pi.getFormu().getChamp("caissearrive").setLibelle("Caisse arriv�e");
    
    pi.getFormu().getChamp("etat").setVisible(false);
    
    
    affichage.Champ[] liste = new affichage.Champ[2];
    
    Caisse depart = new Caisse();
    liste[0] = new Liste("caissedepart", depart, "desccaisse", "idcaisse");
    
    Caisse arrivee = new Caisse();
    liste[1] = new Liste("caissearrive", arrivee, "desccaisse", "idcaisse");
    
    pi.getFormu().changerEnChamp(liste);
    pi.preparerDataFormu();
    
%>
<div class="content-wrapper">
    <h1 class="box-title">Enregistrer mouvement intra-caisse</h1>
    <form action="<%=pi.getLien()%>?but=apresTarif.jsp" method="post" name="mvtintracaisse" id="mvtintracaisse" data-parsley-validate>
        <%
            pi.getFormu().makeHtmlInsertTabIndex();
            out.println(pi.getFormu().getHtmlInsert());
        %>
        <input name="acte" type="hidden" id="nature" value="insert">
        <input name="bute" type="hidden" id="bute" value="fin/mvtintracaisse-fiche.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.allosakafo.fin.MvtIntraCaisse">
    </form>
</div>
<%}catch(Exception e){
    e.printStackTrace();
    throw e;
}%>
<script language="JavaScript">

</script>