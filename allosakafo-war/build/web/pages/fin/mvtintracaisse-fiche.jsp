
<%@page import="mg.allosakafo.fin.MvtIntraCaisse"%>
<%@ page import="user.*" %>
<%@ page import="bean.*" %>
<%@ page import="utilitaire.*" %>
<%@ page import="affichage.*" %>
<%
    MvtIntraCaisse a = new MvtIntraCaisse();
    PageConsulte pc = pc = new PageConsulte(a, request, (user.UserEJB) session.getValue("u"));
    a = (MvtIntraCaisse)pc.getBase();
    pc.getChampByName("id").setLibelle("Id");
    pc.getChampByName("daty").setLibelle("Date");
    
    pc.setTitre("Fiche mouvement intra caisse");
%>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=fin/mvtintracaisse-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>

                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                            
                        %>
                        <br/>
                        <div class="box-footer" >
                            <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=valider&classe=mg.allosakafo.fin.MvtIntraCaisse&bute=fin/mvtintracaisse-fiche.jsp&id=" + request.getParameter("id")+"&etat="+a.getEtat() %>" target="_blank" style="margin-right: 10px">Viser</a>
                            <a class="btn btn-danger pull-right"  href="<%=(String) session.getValue("lien") + "?but=apresTarif.jsp&acte=annulerVisa&classe=mg.allosakafo.fin.MvtIntraCaisse&bute=fin/mvtintracaisse-fiche.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Annuler</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%out.println(pc.getBasPage());%>
</div>