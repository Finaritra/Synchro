<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="mg.allosakafo.fin.MvtCaisse"%>
<%@page import="mg.allosakafo.fin.Caisse"%>
<%@page import="affichage.PageRecherche"%>

<% 
    String nomTable="MVTCAISSELETTREReste";
    MvtCaisse lv = new MvtCaisse();
    if(request.getParameter("table")!=null && request.getParameter("table").compareToIgnoreCase("")!=0) nomTable=request.getParameter("table");
    
    lv.setNomTable(nomTable);
	
    String listeCrt[] = {"id", "designation", "iddevise","idcaisse","numpiece", "numcheque", "daty", "datyvaleur","vnumPiece","vNumCheque"};
    String listeInt[] = {"daty", "datyvaleur"};
    String libEntete[] = {"id", "designation", "iddevise","idcaisse","numpiece", "numcheque", "daty", "datyvaleur", "debit", "credit","reste","vnumPiece","vNumCheque"};
    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
        
      
    pr.getFormu().getChamp("idcaisse").setLibelleAffiche("Caisse");
    pr.getFormu().getChamp("iddevise").setLibelleAffiche("Devise");
    
    pr.getFormu().getChamp("vnumPiece").setLibelleAffiche("num Piece");
    pr.getFormu().getChamp("vnumcheque").setLibelleAffiche("Numero de cheque");
    
     pr.getFormu().getChamp("numpiece").setLibelleAffiche("livreur");
    pr.getFormu().getChamp("numcheque").setLibelleAffiche("Comande");
    
    pr.getFormu().getChamp("daty1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("daty2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    
    pr.setApres("fin/mvtcaisse-liste.jsp");
    String[] colSomme = {"debit", "credit","reste"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste mouvement de caisse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=fin/mvtcaisse-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>
<div class="row col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    Etat : 
                    <select name="table" class="champ" id="table" onchange="changerDesignation()" >
                         <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("MVTCAISSELETTREReste") == 0) {%>    
                        <option value="MVTCAISSELETTREReste" selected>Tous</option>
                        <% } else { %>
                        <option value="MVTCAISSELETTREReste" >Tous</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("MVTCAISSELETTREResteCree") == 0) {%>    
                        <option value="MVTCAISSELETTREResteCree" selected>Cr&eacute;e</option>
                        <% } else { %>
                        <option value="MVTCAISSELETTREResteCree" >Cr&eacute;e</option>
                        <% } %>
                        <% if (request.getParameter("table") != null && request.getParameter("table").compareToIgnoreCase("MVTCAISSELETTREResteValide") == 0) {%>    
                        <option value="MVTCAISSELETTREResteValide" selected>Valide&eacute;e</option>
                        <% } else { %>
                        <option value="mvtCaisseLettreResteValide" >Valid&eacute;e</option>
                        <% } %>
                        
				
                    </select>
                </div>
            </div>
        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=fin/mvtcaisse-fiche.jsp", pr.getLien()};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id", "Designation", "Devise", "Caisse", "Livreur", "Comande", "Date", "Date valeur", "Montant debit", "Montant credit","Montant reste","num Piece","Num Cheque"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            pr.rajoutLienBasPage("table",nomTable);
            out.println(pr.getBasPage());

        %>
    </section>
</div>