<%@page import="utilitaire.Utilitaire"%>
<%@page import="mg.cnaps.rappro.RapproService"%>
<%@page import="bean.AdminGen"%>
<%@page import="mg.cnaps.rappro.RapproCompte"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.cnaps.rappro.RapprochementGrandLivre"%>
<%@page import="mg.cnaps.compta.ComptaEtatGrandLivre"%>
<%@page import="java.sql.Date"%>
<%@page import="mg.cnaps.rappro.RapproReleve"%>
<%@page import="bean.CGenUtil"%>
<%@page import="mg.cnaps.compta.ComptaGrandLivre"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
    // Date releve=new Date("");
    RapprochementGrandLivre gl = new RapprochementGrandLivre();
    RapproReleve rr = new RapproReleve();
    rr.setNomTable("RAPPRO_RELEVE_LIBELLE");
    String lien=(String) session.getValue("lien");
    String listeCrt[] = {"daty","ref","libelle","debit","credit"};
    String listeInt[] = {"debit","credit"};
    String libEntetegl[] = {"id","compte", "daty","libelle", "ref", "debit","credit"};
    PageRecherche pr = new PageRecherche(gl, request, listeCrt, listeInt, 3, libEntetegl, 7);
    String listeCrtrr[] = {"datereleve","libelle","reference","debit","credit","numero"};
    String listeIntrr[] = {"debit","credit"};
    String libEnteterr[] = {"id","numero", "datereleve", "libelle", "reference", "debit","credit","solde","compte"};
    PageRecherche prrr = new PageRecherche(rr, request, listeCrtrr, listeIntrr, 3, libEnteterr, 9);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien(lien);
    pr.setApres("rappro/lettrage/rapprochement.jsp");
    prrr.setUtilisateur((user.UserEJB) session.getValue("u"));
    prrr.setLien((String) session.getValue("lien"));
    prrr.setApres("rappro/lettrage/rapprochement.jsp");
    String[] colSommegl = {"debit", "credit"};
    String[] colsommerr = {"debit", "credit","solde"};
    String  compte=request.getParameter("compte");
    RapproCompte c=new RapproCompte();
    c.setNomTable("RAPPRO_COMPTE_LIBELLE");
    RapproCompte[] listecompte=null;
    RapproCompte [] compt=null;
    int idc=0;
    double soldeInitialgl=0;
    double soldeInitialrl=0;
    RapproReleve [] rel=null;
    RapprochementGrandLivre [] g=null;
    listecompte =(RapproCompte[]) CGenUtil.rechercher(c, null, null, " order by id");
    int lc=listecompte.length;
    idc=lc-1;
    String [] att={"compte"};
    String [] valAtt={""+listecompte[idc].getCompte()+""};
    System.out.println("compte1="+listecompte[0].getCompte());
    System.out.println("compte2="+listecompte[1].getCompte());
    RapproCompte [] listec= (RapproCompte [])AdminGen.findAvecOrder(listecompte, att, valAtt);
    int l=listec.length;
    double soldefinalRl=0;
    double soldefinalgl=0;
    if(compte!=null && compte.compareToIgnoreCase("")!=0 )
    { 
      prrr.setAWhere("and compte='"+compte+"' and id not in (select idreleve from rappro_sous_releve)");
      pr.setAWhere("and compte='"+compte+"' and id not in (select idsousecriture from rappro_sous_ecriture)");
      if(l>1)
      {
        
        Date datedebut=listec[l-1].getDatedebut();
        Date dateAvant=Utilitaire.getDateAvant(datedebut, -1);
        String [] attribut={"datefin"};
        String [] val={""+dateAvant+""};
        System.out.println("dateAvant="+dateAvant);
        compt=(RapproCompte[])AdminGen.findAvecOrder(listecompte, attribut, val);
        soldeInitialrl=compt[0].getSoldebanque();
        soldeInitialgl=compt[0].getSoldecompta();
        soldefinalRl=listec[l-1].getSoldebanque();
        soldefinalgl=listec[l-1].getSoldecompta();
      }
      else if(l==1)
      {
        soldeInitialrl=0;
        soldeInitialgl=0;
        soldefinalRl=listec[l-1].getSoldebanque();
        soldefinalgl=listec[l-1].getSoldecompta();
      }
    }
    else
    {
        if(l>1)
        {
            prrr.setAWhere("and compte='"+listec[l-1].getCompte()+"' and id not in (select idreleve from rappro_sous_releve)");
            pr.setAWhere("and compte ='"+listec[l-1].getCompte()+"' and id not in (select idsousecriture from rappro_sous_ecriture)");
            Date datedebut=listec[l-1].getDatedebut();
            Date dateAvant=Utilitaire.getDateAvant(datedebut, -1);
            String [] attribut={"datefin"};
            String [] val={""+dateAvant+""};
            System.out.println("dateAvant="+dateAvant);
            compt=(RapproCompte[])AdminGen.findAvecOrder(listecompte, attribut, val);
            soldeInitialrl=compt[0].getSoldebanque();
            soldeInitialgl=compt[0].getSoldecompta();
            soldefinalRl=listec[l-1].getSoldebanque();
            soldefinalgl=listec[l-1].getSoldecompta();
        }
        else if(l==1)
        {
              prrr.setAWhere("and compte='"+listec[l-1].getCompte()+"' and id not in (select idreleve from rappro_sous_releve)");
              pr.setAWhere("and compte='"+listec[l-1].getCompte()+"' and id not in (select idsousecriture from rappro_sous_ecriture)");
              soldeInitialrl= 0;
              soldeInitialgl= 0;
              soldefinalRl=listec[l-1].getSoldebanque();
              soldefinalgl=listec[l-1].getSoldecompta();
        }
    }
    prrr.creerObjetPage(libEnteterr, colsommerr);
    RapproReleve[] lrr = (RapproReleve[])prrr.getTableau().getData();
    pr.creerObjetPage(libEntetegl, colSommegl);
    RapprochementGrandLivre[] lgrl = (RapprochementGrandLivre[]) pr.getTableau().getData();
 
%>
<div class="content-wrapper">
    </br>
    <div class="row">
            <form action="<%=pr.getLien()%>?but=rappro/lettrage/rapprochement.jsp" method="post" name="gl" id="gl">
                <div class="col-md-6">
                    <%out.println(pr.getFormu().getHtmlEnsemble());%>
                </div>
            </form>
            <form action="<%=prrr.getLien()%>?but=rappro/lettrage/rapprochement.jsp" method="post" name="releve" id="releve">
                <div class="col-md-6">
                    <%out.println(prrr.getFormu().getHtmlEnsemble());%>
                </div>
            </form>
        </div>
    
<form id="formLettrage" action="<%=lien%>?but=rappro/lettrage/apresTarifLettrage.jsp" method="post">
    <section class="content-header"> 
        <div class="row">

            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="form-group">
                            <label>Compte</label>
                            <div class="input-group">                              
                                <input class="form-control" id="compte" name="compte" type="text" value="<%=listecompte[idc].getCompte()%>">
                                <input class="form-control" id="idcompte" name="idcompte" type="hidden">
                               
                                <div class="input-group-btn">
                                    <button class="btn btn-default btn-sm" type="button" onclick="pagePopUp('choix/rapproCompteChoix.jsp?champReturn=compte;datedebut;id;datefin')">...</button>
                                   
                                    <button class="btn btn-default btn-sm" type="button" onclick="getdonneeRapprocher()">filtrer</button>
                                </div>
                            </div>
                </div>
            
            </div>
        </div>
    </br>
    </section>
    <section class="content">
        
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text">Montant Grand Livre</span>
                          <span class="info-box-number" id="montantGL">0</span>
                          <!-- /.info-box-content -->
                    </div>
                  <!-- /.info-box -->
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-print"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Montant Relev�s</span>
                  <span class="info-box-number" id="montantR">0</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-red"><i class="fa fa-balance-scale"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Diff�rences</span>
                    <span class="info-box-number" id="montantDIF">0</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
      
        <div class="row" ng-app="myApp" ng-controller="myCtrl"> 
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" style="text-align: center;">comptabilit�</h3>
                        </br>
                        <p class="box-title" style="text-align: left;color: #0075b0">Solde Initial:<%=Utilitaire.formaterAr(soldeInitialgl)%></p>
                    </div>
                    <div class="box-body table-responsive no-padding">                                           
                        <input type="hidden" id="creditTotal1" value="0" />
                        <input type="hidden" id="debitTotal1" value="0" />
                        <input type="hidden" id="montantTotal1" value="0" />
                        <table class="table table-hover">
                            <thead>
                            <th style='background-color:#bed1dd'>Num</th>
                            <th style='background-color:#bed1dd'>Date</th>
                            <th style='background-color:#bed1dd'>Libelle</th>
                            <th style='background-color:#bed1dd'>Ref</th>
                            <th style='background-color:#bed1dd'>Debit</th>
                            <th style='background-color:#bed1dd'>Credit</th>
                            <th style='background-color:#bed1dd'>Solde</th>
                            <th style='background-color:#bed1dd'></th>
                            </thead>
                            <tbody>
                                <%for (int i = 0; i < lgrl.length; i++) {
                                     
                                %>  

                                <tr>   
                                    <td> <%out.print(i + 1);%></td>
                                    <td><% out.print(Utilitaire.formatterDaty(lgrl[i].getDaty()));%></td>
                                    <td><%out.print(lgrl[i].getLibelle());%></td>
                                    <td><%out.print(lgrl[i].getRef());%></td>
                                    <td id="debit-<%out.print(i+1);%>"><%out.print(Utilitaire.formaterAr(lgrl[i].getDebit())); %></td>
                                    <td id="credit-<%out.print(i+1);%>"><%out.print(Utilitaire.formaterAr(lgrl[i].getCredit()));%></td>
                                    <td><%out.print(Utilitaire.formaterAr(lgrl[i].getSolde()));%></td>
                                    <td><input type="checkbox" name="manuelg" class="check-input" id="check-<%out.print(i+1);%>" value="<%out.print(lgrl[i].getId());%>"></td>
                                </tr>

                                <%/*System.out.println(lgrl[i].getId());*/}%>                      
                            </tbody>
                        </table>
                            <div class="box-footer">
                        <p class="box-title" style="text-align: left ;color: #0075b0">Solde final :<%=Utilitaire.formaterAr(soldefinalgl)%></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" style="text-align: center;">Banques</h3>
                        <p class="box-title" style="text-align: left;color: #0075b0">Solde Initial:<%=Utilitaire.formaterAr(soldeInitialrl)%></p>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <input type="hidden" id="creditTotal2" value="0" />
                        <input type="hidden" id="debitTotal2" value="0" />
                        <input type="hidden" id="montantTotal2" value="0" />
                        <table class="table table-hover">
                            <thead>
                            <th style='background-color:#bed1dd'>code DR</th>
                            <th style='background-color:#bed1dd'>Date</th>
                            <th style='background-color:#bed1dd'>Libelle</th>
                            <th style='background-color:#bed1dd'>Ref</th>
                            <th style='background-color:#bed1dd'>Debit</th>
                            <th style='background-color:#bed1dd'>Credit</th>
                            <th style='background-color:#bed1dd'>Solde</th>
                            <th style='background-color:#bed1dd'></th>
                            </thead>
                            <tbody>

                                <% 
                                for (int i = 0; i < lrr.length; i++) {
                                %>

                                <tr> 
                                    <td> <%out.print(lrr[i].getNumero());%></td>
                                    <td> <%out.print(Utilitaire.formatterDaty(lrr[i].getDatereleve()));%></td>
                                    <td> <%out.print(lrr[i].getLibelle());%></td>
                                    <td> <%out.print(lrr[i].getReference());%></td>
                                    <td id="debit2-<%out.print(i+1);%>"> <%out.print(Utilitaire.formaterAr(lrr[i].getDebit()));%></td>
                                    <td id="credit2-<%out.print(i+1);%>"> <%out.print(Utilitaire.formaterAr(lrr[i].getCredit()));%></td>
                                    <td> <%out.print(Utilitaire.formaterAr(lrr[i].getSolde()));%></td>
                                    <td><input type="checkbox" class="check-input-2" id="check-<%out.print(i+1);%>" name="manuelr" value="<%out.print(lrr[i].getId());%>"> </td>
                                </tr>
                                <% 
                                    ;}
                                     
                                %>

                            </tbody>
                        </table>
                        <div class="box-footer">
                            <p class="box-title" style="text-align: left; color: #0075b0" > Solde final :<%=Utilitaire.formaterAr(soldefinalRl)%></p>
                            
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <input name="acte" type="hidden" id="acte" value="">
        <input name="bute" type="hidden" id="bute" value="rappro/lettrage/rapprochement.jsp">
        <input name="classe" type="hidden" id="classe" value="mg.cnaps.rappro.RapproReleve">
        
         <div class="box-footer">
                    <a class="btn btn-default pull-right" onclick="rapprocher()" href="#" style="margin-center:10px">rapprocher</a>
                   <a class="btn btn-default pull-right" onclick="genererEcriture()" href="#" style="margin-center:10px">G�n�rer �criture</a>
                   <a class="btn btn-default pull-right" onclick="saisieReleve()" href="#" style="margin-center:10px">Saisie R�lev�</a>
         </div>
      
          <% 
            out.println(prrr.getBasPage());
        %>
    </section>
    </form>
</div>
                
                                                                              
<script>
    var montantR = 0;
    var montantGL = 0;
    $('.check-input').change(function(){
        var ischecked= $(this).is(':checked');
        var inputId = this.id;
        var debitId = inputId.replace('check','#debit');
        var creditId = inputId.replace('check','#credit');
        
        var debitMontant = parseInt( $(debitId).html().toString().replace("&nbsp;", "").replace("&nbsp;", "") );
        var creditMontant = parseInt( $(creditId).html().toString().replace("&nbsp;", "").replace("&nbsp;", ""));
        
        var creditTotal = parseInt( $('#creditTotal1').val() );
        var debitTotal = parseInt( $('#debitTotal1').val() );
        var montantTotal = parseInt( $('#montantGL').html().toString().replace("&nbsp;", "").replace("&nbsp;", "") );
        
        if(ischecked){
            creditTotal += creditMontant;
            debitTotal += debitMontant;
            montantTotal = creditTotal - debitTotal;
        }
        else{
            creditTotal -= creditMontant;
            debitTotal -= debitMontant;
            montantTotal = creditTotal - debitTotal;
        }
        
        $('#creditTotal1').val(creditTotal);
        $('#debitTotal1').val(debitTotal);
        $('#montantGL').html(montantTotal.toLocaleString());
        montantR = parseInt( $('#montantR').html().toString().replace("&nbsp;", "").replace("&nbsp;", ""));
        $('#montantDIF').html((montantTotal + montantR).toLocaleString()).trigger('contentChange');
    });
    
    $('.check-input-2').change(function(){
        var ischecked= $(this).is(':checked');
        var inputId = this.id;
        var debitId = inputId.replace('check','#debit2');
        var creditId = inputId.replace('check','#credit2');
        
        var debitMontant = parseInt( $(debitId).html().toString().replace("&nbsp;", "").replace("&nbsp;", "") );
        var creditMontant = parseInt( $(creditId).html().toString().replace("&nbsp;", "").replace("&nbsp;", "") );
        
        var creditTotal = parseInt( $('#creditTotal2').val() );
        var debitTotal = parseInt( $('#debitTotal2').val() );
        var montantTotal = parseInt( $('#montantR').html().toString().replace("&nbsp;", "").replace("&nbsp;", "") );
        
        if(ischecked){
            creditTotal += creditMontant;
            debitTotal += debitMontant;
            montantTotal = creditTotal - debitTotal;
        }
        else{
            creditTotal -= creditMontant;
            debitTotal -= debitMontant;
            montantTotal = creditTotal - debitTotal;
        }
        
        $('#creditTotal2').val(creditTotal);
        $('#debitTotal2').val(debitTotal);
        $('#montantR').html(montantTotal.toLocaleString());
        montantGL = parseInt( $('#montantGL').html().toString().replace("&nbsp;", "").replace("&nbsp;", "")  );
        $('#montantDIF').html((montantTotal + montantGL).toLocaleString()).trigger('contentChange');
        
    });
    $('#montantDIF').bind('contentChange', function(){
        var valeur = parseInt( $('#montantDIF').html().toString().replace("&nbsp;", "").replace("&nbsp;", "") );
        if(valeur === 0 && montanGL != 0){
            rapprocher();
        }
       /* else if(valeur != 0 && montantR!=0)
        {
            genererEcriture();
        }*/
    });  
    function genererEcriture() {
          $("#acte").val("generer");
            document.forms["formLettrage"].submit();
    }
    function rapprocher() {
        $("#acte").val("rapprocher");
            document.forms["formLettrage"].submit();
        
    }
    function saisieReleve()
    {
        $("#acte").val("saisieReleve");
            document.forms["formLettrage"].submit();
        
    }
    function getdonneeRapprocher()
    {      
           $("#acte").val("filtrer");
           document.forms["formLettrage"].submit();
           //document.location.reload();
    }
</script>
                                                                                                                                                                                         

                                                                                                                                                                                                        </body>
                                                                                                                                                                                                        </html>
