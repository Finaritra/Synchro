
<%@page import="affichage.Liste"%>
<%@page import="bean.TypeObjet"%>
<%@page import="affichage.PageRecherche"%>
<%@page import="mg.allosakafo.facture.Facture"%>

<% 
    Facture lv = new Facture();
    lv.setNomTable("facture_lib");
    
    String listeCrt[] = {"id","client","modepaiement","datefacture","datesaisie","montant"};
    String listeInt[] = {"datefacture","datesaisie","montant"};
    String libEntete[] = {"id","client","modepaiement","adresseclient","datefacture","datesaisie","montant"};

    
    PageRecherche pr = new PageRecherche(lv, request, listeCrt, listeInt, 3, libEntete, 7);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    
    pr.setApres("fature/facture-liste.jsp");
    String[] colSomme = {"montant"};
    pr.creerObjetPage(libEntete, colSomme);
%>
<script>
    function changerDesignation() {
        document.incident.submit();
    }
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Liste paiement</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=facture/facture-liste.jsp" method="post" name="incident" id="incident">
            <%
                out.println(pr.getFormu().getHtmlEnsemble());
            %>

        </form>
        <%  String lienTableau[] = {pr.getLien() + "?but=facture/facture-fiche.jsp"};
            String colonneLien[] = {"id"};
            pr.getTableau().setLien(lienTableau);
            pr.getTableau().setColonneLien(colonneLien);
            out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            String libEnteteAffiche[] = {"Id","Client","Mode de paiement","Adresse du client","Date de facture","Date de saisie","Montant"};
            pr.getTableau().setLibelleAffiche(libEnteteAffiche);
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>