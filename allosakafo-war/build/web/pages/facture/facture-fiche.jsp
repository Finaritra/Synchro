<%-- 
    Document   : facture-fiche
    Created on : 10 f�vr. 2020, 17:30:57
    Author     : john
--%>

<%@page import="mg.allosakafo.facture.Facture"%>
<%@page import="affichage.PageConsulte"%>
<%
    try {
        Facture cl = new Facture();
        cl.setNomTable("facture_lib");

        PageConsulte pc = pc = new PageConsulte(cl, request, (user.UserEJB) session.getValue("u"));
        pc.getChampByName("modepaiement").setLibelle("Mode de paiement");
        pc.getChampByName("adresseclient").setLibelle("Adresse du client");
        pc.getChampByName("datefacture").setLibelle("Date de facture");
        pc.getChampByName("datesaisie").setLibelle("Date de saisie");
        /*pc.getChampByName("adresse").setLibelle("Adresse");
    pc.getChampByName("telephone").setLibelle("Telephone");
    pc.getChampByName("fb").setLibelle("Facebook");*/

        pc.setTitre("Fiche facture");

%>

<div class="content-wrapper">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-fiche">
                <div class="box">
                    <div class="box-title with-border">
                        <h1 class="box-title"><a href="<%=(String) session.getValue("lien")%>?but=facture/facture-liste.jsp"><i class="fa fa-arrow-circle-left"></i></a><%=pc.getTitre()%></h1>
                    </div>
                    <div class="box-body">
                        <%
                            out.println(pc.getHtml());
                        %>
                        <br/>
                        <a class="btn btn-warning pull-right"  href="<%=(String) session.getValue("lien") + "?but=facture/facture-modif.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Modifier</a>
                        <a class="btn btn-primary pull-right"  href="<%=(String) session.getValue("lien") + "?but=facture/detailsfacture-liste.jsp&idfacture=" + request.getParameter("id")%>" style="margin-right: 10px">Voir d�tails facture</a>
                        <a class="btn btn-success pull-right"  href="<%=(String) session.getValue("lien") + "?but=commande/facture.jsp&id=" + request.getParameter("id")%>" style="margin-right: 10px">Voir apper�u facture</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%} catch (Exception e) {
        e.printStackTrace();
    }%>
