

<%@page import="mg.allosakafo.fin.OrdreDePaiement"%>
<%@page import="affichage.PageRechercheGroupe"%>
<%@page import="mg.allosakafo.fin.EtatOP"%>
<%
    try{
    OrdreDePaiement lv = new OrdreDePaiement();

    String listeCrt[] = {"daty", "ded_id", "modepaiement", "remarque"};
    String listeInt[] = {"daty"};
    String[] pourcentage = {"nombrepargroupe"};
    String colDefaut[] = {"id", "daty", "ded_id", "modepaiement", "remarque"}; 
    String somDefaut[] = {"montant"};
    
    PageRechercheGroupe pr = new PageRechercheGroupe(lv, request, listeCrt, listeInt, 3,colDefaut, somDefaut, pourcentage, 6, 2);
    pr.setUtilisateur((user.UserEJB) session.getValue("u"));
    pr.setLien((String) session.getValue("lien"));
    pr.getFormu().getChamp("daty1").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.getFormu().getChamp("daty2").setDefaut(utilitaire.Utilitaire.dateDuJour());
    pr.setNpp(500);
    pr.setApres("facture/analyse-op.jsp");
    pr.creerObjetPagePourc();

%>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Analyse</h1>
    </section>
    <section class="content">
        <form action="<%=pr.getLien()%>?but=facture/analyse-op.jsp" method="post" name="analyse" id="analyse">
            <%out.println(pr.getFormu().getHtmlEnsemble());%>
        </form>
        <%
           out.println(pr.getTableauRecap().getHtml());%>
        <br>
        <%
            out.println(pr.getTableau().getHtml());
            out.println(pr.getBasPage());
        %>
    </section>
</div>
<%
    }catch(Exception e){
        e.printStackTrace();
    }
%>