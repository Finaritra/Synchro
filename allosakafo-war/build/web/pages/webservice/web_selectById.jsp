<%-- 
    Document   : web_selectById
    Created on : 18 avr. 2017, 11:59:45
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String table = request.getParameter("t");
    String champ = request.getParameter("c");
    String parametre = request.getParameter("p");
    JSONArray resultat_final = ExecuteFunction.select_action_tableFieldParameter(table,champ,parametre);
%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultat_final);
%>