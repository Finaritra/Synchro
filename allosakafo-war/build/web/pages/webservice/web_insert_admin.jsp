<%-- 
    Document   : web_insert_admin
    Created on : 25 avr. 2017, 09:51:45
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("c1");
    String nom = request.getParameter("c2");
    String prenom = request.getParameter("c3");
    String identifiant = request.getParameter("c4");
    String mdp = request.getParameter("c5");
    String email = request.getParameter("c6");
    int statutAdmin = Integer.parseInt(request.getParameter("c7"));
    String commentaire = request.getParameter("c8");
    String resultatInsertion = null;
    try{
        resultatInsertion = ExecuteFunction.inserer_admin_article(id, nom, prenom, identifiant, mdp, email, statutAdmin, commentaire);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultatInsertion);
%>