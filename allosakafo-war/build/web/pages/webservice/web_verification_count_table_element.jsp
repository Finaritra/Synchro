<%-- 
    Document   : web_verification_count_table_element
    Created on : 25 avr. 2017, 11:03:21
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String table = request.getParameter("t");
    String champ1 = request.getParameter("c");
    String valeurChamp1 = request.getParameter("v");
    JSONArray resultat_final = ExecuteFunction.select_count_table(table,champ1,valeurChamp1);
%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultat_final);
%>
