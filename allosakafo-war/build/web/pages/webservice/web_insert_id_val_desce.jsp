<%-- 
    Document   : web_insert_typeadresse
    Created on : 26 avr. 2017, 13:40:47
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String tab = request.getParameter("tab");
    String id = request.getParameter("id");
    String val = request.getParameter("val");
    String desce = request.getParameter("desce");
    String resultatInsertion = null;
    try{
        resultatInsertion = ExecuteFunction.inserer_id_val_desce(tab, id, val, desce);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultatInsertion);
%>