<%-- 
    Document   : web_listeOrderBy
    Created on : 13 avr. 2017, 16:38:22
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String table = request.getParameter("t");
    String champ = request.getParameter("c");
    String ordre = request.getParameter("o");
    JSONArray resultat_final = ExecuteFunction.select_list_order_by(table,champ,ordre);
%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultat_final);
%>
