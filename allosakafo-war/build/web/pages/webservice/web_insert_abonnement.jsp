<%-- 
    Document   : web_insert_abonnement
    Created on : 2 mai 2017, 16:30:22
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String typeAbonnement = request.getParameter("idTypeAbonnement");
    String debut = request.getParameter("dateDebut");
    String fin = request.getParameter("dateFin");
    String commentaire = request.getParameter("commentaire");
    String resultatInsertion = new String();
    try{
        resultatInsertion = ExecuteFunction.inserer_abonnement(id, typeAbonnement,debut,fin, commentaire);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultatInsertion);
%>
