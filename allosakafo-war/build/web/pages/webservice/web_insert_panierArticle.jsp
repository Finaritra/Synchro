<%-- 
    Document   : web_insert_panierArticle
    Created on : 24 avr. 2017, 12:57:01
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String idProduit = request.getParameter("pdt");
    String idUtilisateur = request.getParameter("util");
    int quantite = Integer.parseInt(request.getParameter("qty"));
    float pu = Float.parseFloat(request.getParameter("pu"));
    float remise = Float.parseFloat(request.getParameter("rms"));
    int statut = Integer.parseInt(request.getParameter("stat"));
    String observation = request.getParameter("obs");
    String resultatInsertion = ExecuteFunction.inserer_panier_article(id, idProduit, idUtilisateur, quantite, pu, remise, statut, observation);
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultatInsertion);
%>