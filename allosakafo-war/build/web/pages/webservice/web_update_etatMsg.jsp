<%-- 
    Document   : web_update_etatMsg
    Created on : 27 avr. 2017, 22:48:43
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String etat = request.getParameter("etat");
    String resultatInsertion = null;
    try{
        resultatInsertion = ExecuteFunction.update_statut_message(id, etat);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultatInsertion);
%>