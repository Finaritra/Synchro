<%-- 
    Document   : web_insert_trfLiv
    Created on : 28 avr. 2017, 00:58:43
    Author     : Mahefa
--%>
<%@page import="connexionBase.*"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.sql.*"%>
<%
    String id = request.getParameter("id");
    String quartier = request.getParameter("q");
    String montant = request.getParameter("m");
    String dateEffectivite = request.getParameter("d");
    String observation = request.getParameter("obs");
    String resultatInsertion = null;
    try{
        resultatInsertion = ExecuteFunction.inserer_TarifLivraison(id, quartier, montant, dateEffectivite, observation);
    }catch(Exception e){
        resultatInsertion = e.getMessage();
    }
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/json");
    out.println(resultatInsertion);
%>