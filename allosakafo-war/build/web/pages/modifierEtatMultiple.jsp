<%-- 
    Document   : apresCommandeFait
    Created on : 16 sept. 2019, 09:31:13
    Author     : pro
--%>

<%@page import="mg.allosakafo.commande.CommandeClientDetails"%>
<%@page import="mg.allosakafo.commande.DetailCommandeClient"%>
<%@page import="mg.allosakafo.stock.EtatdeStockDate"%>
<%@page import="mg.allosakafo.produits.Recette"%>
<%@page import="utilitaire.ConstanteEtat"%>
<%@page import="mg.allosakafo.livraison.CommandeLivraison"%>
<%@page import="mg.allosakafo.facture.Facture"%>
<%@page import="user.UserEJB"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="mg.allosakafo.commande.CommandeService"%>

<%
    UserEJB u = (UserEJB) session.getAttribute("u");
    String[] id = request.getParameterValues("id");
    String[] remarque = request.getParameterValues("remarque");
    String lien = (String) session.getValue("lien");
    String acte = request.getParameter("acte");
    String bute = request.getParameter("bute");
    String caisse=request.getParameter("caisse");
    CommandeService cs = new CommandeService();
    System.out.println("ACTE: "+acte);
    if(acte != null && !"".equals(acte) && acte.equals("annuler")){
        u.annulerDetailsCommande(id);
    }
    if(acte != null && !"".equals(acte) && acte.equals("fait")){
        cs.validerDetailsCommande2(id, null);
    }  
    if(acte != null && !acte.isEmpty() && acte.equals("facturer")){
            Facture f = new Facture(request.getParameter("client"),request.getParameter("modepaiement"),request.getParameter("adresseclient"),utilitaire.Utilitaire.stringDate(request.getParameter("datefacture")));
            String[] ids = request.getParameter("detail").split(";");
            f.insertFacture(ids,""+u.getUser().getRefuser(), null);
            bute="commande/facture.jsp&id="+f.getId();
    } 
    if(acte != null && !"".equals(acte) && acte.equals("fait_boisson")){
        cs.validerDetailsCommande2(id, null);
        u.livrerDetailsCommandeLivraison(id);
    }
    if(acte != null && !"".equals(acte) && acte.equals("cloturer")){
        u.cloturerDetailsCommandeWithUser(id,caisse,null);
    }
        
    if(acte != null && !"".equals(acte) && acte.equals("valider"))
        cs.validerDetailsCommande2(id, null);
    if(acte != null && !"".equals(acte) && acte.equals("payer")){
            u.payerDetailsCommande(id, null);
    }
    if(acte != null && !"".equals(acte) && acte.equals("livrer")){
        u.livrerDetailsCommandeLivraison(id);
    }
    if(acte != null && !"".equals(acte) && acte.equals("annuler_valider")){
         cs.annulerValiderDetailsCommande(id,""+u.getUser().getRefuser() , null);
    }
     if(acte != null && !"".equals(acte) && acte.equals("facturer")){
            
    }
     if(acte != null && !"".equals(acte) && acte.equals("revalider")){
          CommandeLivraison cmd=new CommandeLivraison();
          cmd.setEtat(ConstanteEtat.getEtatCloture());
          cmd.updateEtatDetailsLivraison(request.getParameterValues("id"),null);
    }
       if(acte != null && !"".equals(acte) && acte.equals("rembourser")){
          CommandeLivraison cmd=new CommandeLivraison();
          cmd.setEtat(ConstanteEtat.getEtatRembourser());
          cmd.updateEtatDetailsLivraison(request.getParameterValues("id"),null);
    }
        if(acte != null && !"".equals(acte) && acte.equals("modifier_recette")){
          Recette rec=new Recette();
          rec.modifQte(id,request.getParameterValues("quantite") ,null);
    }
        if(acte != null && !"".equals(acte) && acte.equals("supprimer_recette")){
          Recette rec=new Recette();
          rec.suppressionMultiple(id,""+u.getUser().getRefuser() , null);
    }
         if(acte != null && !"".equals(acte) && acte.equals("achat")){
             
             String[] aacheter = request.getParameterValues("aacheter");
             String[] pu = request.getParameterValues("pu");
             String[] idfournisseur=request.getParameterValues("idFournisseur");
            
             EtatdeStockDate.insertAchat(id,idfournisseur,pu,aacheter,u.getUser().getTuppleID(),null);
    }
           if(acte != null && !"".equals(acte) && acte.equals("voaraycuisinier")){
            CommandeClientDetails.commandeVoarayCuisinierMultiple(request.getParameterValues("id"),null,u);
            bute="commande/as-commande-liste-details.jsp";
    }
         
         
      
   
       
    if(acte != null && !"".equals(acte) && acte.equals("annuler_boisson")) u.annulerDetailsCommande(id);
        //cs.annulerBoisson(id, null);
    if(acte != null && !"".equals(acte) && acte.equals("modifier"))
        cs.modifRemarque(id, remarque, null);
%>
<script language="JavaScript"> document.location.replace("<%=lien%>?but=<%=bute%>");</script>