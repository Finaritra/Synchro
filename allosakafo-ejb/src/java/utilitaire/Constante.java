package utilitaire;

import java.io.File;

public class Constante {

    public static final String fichierRestau="D:/env/wildfly-10.0.0.Final/standalone/deployments/pho.war/pages/fichierRestau.txt";
    public static final String caisseDefaut="D:/env/wildfly-10.0.0.Final/standalone/deployments/pho.war/pages/fichierCaisse.txt";
    
    private static String idTypeLCRecette = "TLC00001";
    private static String idTypeLCDepense = "TLC00002";
    public static String idRoleDirecteur = "directeur";
    public static String idRoleUtilisateur = "utilisateur";
    public static String idRoleAdmin = "admin";
    public static String idRoleAdminFacture = "adminFacture";
    public static String idRoleAdminCompta = "compta";
    public static String idRoleDg = "dg";
    public static String idRoleSaisie = "saisie";
    public static String tableOpFfLc = "OpFfLc";
    private static String mois[] = {"Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre"};
    private static String moisRang[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    private static String objetDepense = "depense";
    private static String objetRecette = "recette";
    private static String objetFacture = "facture";
    private static String objetFactureF = "factureF";
    static private String tableRecetteLC = "recettesLC";
    static private String tableFactureLC = "factureLigneCredit";
    static private String tableFactureFLC = "FactureFournisseurLC";
    static private String tableDepenseLC = "sortieFF";
    private static String idFournAuto = "clt1";
    static private String objFactureProf = "FactureProfFournisseur";
    static private String objetBc = "BC";
    static private String objetCDetailFactProf = "detFactProf";
    static private String objetDetailBC = "detBC";
    static private String objetDetailFactFourn = "detFactF";
    static private String dedFactProfFourn = "DedFACTUREProfFOURNISSEUR";
    static private String DetailLcCree = "0";
    static private String DetailLcValide = "1";
    static private String objetDedBc = "DedBc";
    static private String objetLcDetMvt = "LcDetailMvtCaisse";
    static private String objetLcDetDed = "LcDetailDed";
    static private String objetLcDetOp = "LcDetailop";
    static private String objetOpFf = "OPFACTUREFOURNISSEUR";
    static private String objetDed = "ded";
    static private String objetOp = "ORDONNERPAYEMENT";
    static private String objetLcDetail = "LcDetails";
    static private String tableDetailBC = "detailBC";
    private static String objetMvtCaisse = "mvtCaisse";
    static private String objectDetailFPF = "detailFPF";
    public static String objetFactureFournisseur = "FactureFournisseur";
    static private String opFactureFournisseur = "OPFACTUREFOURNISSEUR";
    static private String objetLigneCredit = "LigneCredit";
    static private String objetLigneCreditRecette = "LigneCreditRecette";
    static private String visaOp = "VISAORDREPAYEMENT";
    static private String visaOr = "VISAOR";
    static public String typeOpFacture = "facture";
    static public String typeOpNormale = "normale";
    static public String tableVisaOp = "VISAORDREPAYEMENT";
    static public String objetFactureClient = "FACTURECLIENT";
    public static String tableOrFcLc = "ORFCLC";
    static public String tableFactureCLC = "FactureClientLC";
    static public String factureClient = "FACTURECLIENT";
    public static final String constanteTiersStock = "TIERS1";
    public static final String constantePeriode = "T";
    public static final int attenteLivraison=75;
    public static final double fraisLivraison=mg.cnaps.commun.Constante.getFraisLivraison();
    
    public static final String constanteEntreeStock = "MVT000002";
    public static final String constanteSortieStock = "MVT000003";
    public static final String constanteReintegrStock = "MVT000001";
    public static final String conge = "CONGE";
    public static final String mouvement = "MOUVEMENT";
    public static final String depart = "DEPART";
    public static final String deplacement = "DEPLACEMENT";
    public static final String deces = "DECES";
    public static final String financeTypeAnnulation="3";
    
    public static final String devise="2";
    
    public static final String typeEspece="pay1";
    public static final String nif="5000075593";
    public static final String stat="18111 11 2008 0 10015";
    public static final String rcs="2008B00015";
    public static final String tel="034 99 300 10 - 032 03 300 10 - 033 64 300 10";
    
    public static final String idBarquette="PRDTBKTP000039";
    
    public static final String idCaisseApayer="CE000606";
    public static final String idTypeBoisson="TPD00001";
    public static final String idTypeComandeResa="TPC00003";
    public static final String idTypeComandeLivr="TPC00001";
    public static final String idTypeComandeNormal="TPC00002";
    public static final String entreeStock="0";
    public static final String marquageTableClient= "CASM";


    public static String getIdTypeLCDepense() {
        return idTypeLCDepense;
    }

    public static String getDetailLcCree() {
        return DetailLcCree;
    }

    public static String getDetailLcValide() {
        return DetailLcValide;
    }

    public static String getIdTypeLCRecette() {
        return idTypeLCRecette;
    }

    public static String getIdRoleDirecteur() {
        return idRoleDirecteur;
    }

    public static String[] getMois() {
        return mois;
    }

    public static String[] getMoisRang() {
        return moisRang;
    }

    public static String getObjetDepense() {
        return objetDepense;
    }

    public static String getObjetRecette() {
        return objetRecette;
    }

    public static String getObjetFacture() {
        return objetFacture;
    }

    public static String getObjetFactureF() {
        return objetFactureF;
    }

    public static String getTableRecetteLC() {
        return tableRecetteLC;
    }

    public static String getTableFactureLC() {
        return tableFactureLC;
    }

    public static String getTableFactureFLC() {
        return tableFactureFLC;
    }

    public static String getTableDepenseLC() {
        return tableDepenseLC;
    }

    public static String getIdFournAuto() {
        return idFournAuto;
    }

    public static String getObjFactureProf() {
        return objFactureProf;
    }

    public static String getObjetBc() {
        return objetBc;
    }

    public static String getObjetCDetailFactProf() {
        return objetCDetailFactProf;
    }

    public static String getObjetDetailBC() {
        return objetDetailBC;
    }

    public static String getObjetDetailFactFourn() {
        return objetDetailFactFourn;
    }

    public static String getDedFactProfFourn() {
        return dedFactProfFourn;
    }

    public static void setObjetDedBc(String objetDedBce) {
        objetDedBc = objetDedBce;
    }

    public static String getObjetDedBc() {
        return objetDedBc;
    }

    public static void setObjetLcDetMvt(String objetLcDetMvte) {
        objetLcDetMvt = objetLcDetMvte;
    }

    public static String getObjetLcDetMvt() {
        return objetLcDetMvt;
    }

    public static String getObjetLcDetDed() {
        return objetLcDetDed;
    }

    public static String getObjetLcDetOp() {
        return objetLcDetOp;
    }

    public static String getObjetOpFf() {
        return objetOpFf;
    }

    public static String getObjetDed() {
        return objetDed;
    }

    public static String getObjetOp() {
        return objetOp;
    }

    public static String getObjetLcDetail() {
        return objetLcDetail;
    }

    public static String getTableDetailBC() {
        return tableDetailBC;
    }

    public static String getObjetMvtCaisse() {
        return objetMvtCaisse;
    }

    public static String getObjectDetailFPF() {
        return objectDetailFPF;
    }

    public static String getObjetFactureFournisseur() {
        return objetFactureFournisseur;
    }

    public static String getOpFactureFournisseur() {
        return opFactureFournisseur;
    }

    public static String getObjetLigneCredit() {
        return objetLigneCredit;
    }

    public static String getObjetLigneCreditRecette() {
        return objetLigneCreditRecette;
    }

    public static String getVisaOp() {
        return visaOp;
    }

    public static String getVisaOr() {
        return visaOr;
    }

}
