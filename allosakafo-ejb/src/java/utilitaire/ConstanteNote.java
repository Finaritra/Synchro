package utilitaire;

/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public final class  ConstanteNote {
  static final private String colorInitial="WHITE";
  static final private String colorAj="#A9A9A9"; //gris clair
  static final private String noMention="--";
  static final private String aj="Aj";
  static final private String[] admission={"Ajour.","Admis"};
  static final private int nbreElimMin=0;
  static final private int nbrematAjMin=2;
  static final private double moyenne=10;
  static final private int valMin=6;
  static final private int[] nombreMatiere={5,6,5};//nombre matieres s4,s5 et s6
  static final private String[] semSansOption={"S1","S2","S3"};
  static final private String[] semAvecOption={"S4","S5","S6"};
  static final private String objetTable="NOMBREMATIEREAJOURNEE ";
  static final private String objetNote="NOTEFINALECOMPLET";

  public ConstanteNote() {
  }
  public static final String getColorInitial() {
   return colorInitial;
  }
  public static final String getColorAj() {
   return colorAj;
  }
  public static final String getAj() {
    return aj;
  }
  public static final int getValMin() {
   return valMin;
  }
  public static final String[] getAdmission() {
    return admission;
  }
  public static final int getNbreElimMin() {
    return nbreElimMin;
  }
  public static final int getNbrematAjMin() {
    return nbrematAjMin;
  }
  public static final double getMoyenne() {
    return moyenne;
  }
  public static final String getNoMention() {
   return noMention;
  }
  public static final String[] getSemSansOption() {
   return semSansOption;
  }
  public static final String[] getSemAvecOption() {
   return semAvecOption;
  }
  public static final int[] getNombreMatiere() {
   return nombreMatiere;
  }
}