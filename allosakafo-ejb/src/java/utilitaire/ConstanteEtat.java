/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitaire;

public class ConstanteEtat {

    private static final int etatAnnuler = 0;
    private static final int etatEnAttente = 7;
    private static final int etatFait = 10;
    private static final int etatCreer = 1;
    private static final int etatLivraison = 20;
    private static final int etatCloture = 9;
    private static final int etatValider = 11;
    private static final int etatPaye = 40;
    private static final int etatEncoursPayement = 30;
    private static final int cloture = 9;
    private static final int fait = 10;
    private static final int valider = 11;
    private static final int payer = 40;
    private static final int livrer = 20;
    private static final int effectuer = 12;
    private static final int etatRembourser = 5;
    private static final int etatAnnulerPayer = 6;
    private static final int etatACloturer = 8;
    private static final int etatALivrer = 12;
    public static final String etatreport="5";
    private static final int etatVoarayCuisinier=10;
    private static final int ingdisponible=11;
    private static final int ingindisponible=1;

    public static int getIngdisponible() {
        return ingdisponible;
    }

    public static int getIngindisponible() {
        return ingindisponible;
    }

    

    public static int getEtatVoarayCuisinier() {
        return etatVoarayCuisinier;
    }

    public static int getEtatRembourser() {
        return etatRembourser;
    }

    public static int getEtatAnnulerPayer() {
        return etatAnnulerPayer;
    }

    public static int getEtatACloturer() {
        return etatACloturer;
    }

    public static int getEtatALivrer() {
        return etatALivrer;
    }

    public static String getEtatreport() {
        return etatreport;
    }

    public static int getEffectuer() {
        return effectuer;
    }
    

    public static int getCloture() {
        return cloture;
    }

    public static int getFait() {
        return fait;
    }

    public static int getValider() {
        return valider;
    }

    public static int getPayer() {
        return payer;
    }

    public static int getLivrer() {
        return livrer;
    }

    public static int getEtatFait() {
        return etatFait;
    }

    
    public static int getEtatLivraison() {
        return etatLivraison;
    }

    public static int getEtatPaye() {
        return etatPaye;
    }

    public static int getEtatCreer() {
        return etatCreer;
    }

    public static int getEtatAnnuler() {
        return etatAnnuler;
    }

    public static int getEtatCloture() {
        return etatCloture;
    }

    public static int getEtatValider() {
        return etatValider;
    }
	
	public static int getEtatEncoursPayement(){
		return etatEncoursPayement;
	}
	

    public static String etatToChaine(String valeur) {
        int val = Utilitaire.stringToInt(valeur);
	if(val == ConstanteEtat.getEtatCreer()) return "<b style='color:lightskyblue'>CR&Eacute;&Eacute;(E)</b>";
        if(val == ConstanteEtat.getEtatValider()) return "<b style='color:green'>VIS&Eacute;(E)</b>";
        if(val == ConstanteEtat.getEtatAnnuler()) return "<b style='color:orange'>ANNUL&Eacute;(E)</b>";
        if(val == ConstanteEtat.getEtatCloture()) return "<b style='color:orange'>CLOTUR&Eacute;(E)</b>";
        if(val == ConstanteEtat.getEtatLivraison()) return "<b style='color:green'>LIVR&Eacute;(E)</b>";
        if(val == ConstanteEtat.getEtatPaye()) return "<b style='color:green'>PAY&Eacute;</b>";
        if(val == ConstanteEtat.getEtatFait()) return "<b style='color:green'>EN COURS DE PREPA</b>";
        if(val == ConstanteEtat.getEtatRembourser()) return "<b style='color:green'>REMBOURS&Eacute;(E)</b>";
        if(val == ConstanteEtat.getEtatACloturer()) return "<b style='color:orange'>A CLOTURER</b>";
        if(val == ConstanteEtat.getEtatALivrer()) return "<b style='color:green'>A LIVRER</b>";
		if(val == ConstanteEtat.getEtatEncoursPayement()) return "<b style='color:yellow'>EN COURS DE PAIEMENT</b>";
        return "<b>AUTRE</b>";
    }
    public static String etatToChaineLivraison(int valeur)
    {
        return etatToChaineLivraison(String.valueOf(valeur));
    }
    public static String etatToChaineLivraison(String valeur) {
        int val = Utilitaire.stringToInt(valeur);
	if(val == ConstanteEtat.getEtatCreer()) return "Cree";
        if(val == ConstanteEtat.getEtatValider()) return "Valide";
        if(val == ConstanteEtat.getEtatAnnuler()) return "Annule";
        if(val == ConstanteEtat.getEtatCloture()) return "Cloture";
        if((val == ConstanteEtat.getEtatLivraison())||val == ConstanteEtat.getEtatPaye()) return "Livree";
        //if(val == ConstanteEtat.getEtatPaye()) return "Paye";
        if(val == ConstanteEtat.getEtatFait()) return "Fait";
        if(val == ConstanteEtat.getEtatRembourser()) return "rembourse";
        if(val == ConstanteEtat.getEtatACloturer()) return "A Cloturer";
        if(val == ConstanteEtat.getEtatALivrer()) return "A Livrer";
	if(val == ConstanteEtat.getEtatEncoursPayement()) return "<b style='color:yellow'>EN COURS DE PAIEMENT</b>";
        return "AUTRE";
    }
    public static int chaineToEtat(String chaine)
    {
        if(chaine.compareToIgnoreCase("cree")==0 ) return ConstanteEtat.getEtatCreer();
        if(chaine.compareToIgnoreCase("valide")==0 || chaine.compareToIgnoreCase("cloture")==0) return ConstanteEtat.getEtatValider();
        int val=Utilitaire.stringToInt(chaine);
        if(val>0)return val;
        return 0;
    }

    public static int getEtatEnAttente() {
        return etatEnAttente;
    }

}
