package quitte;
import bean.ClassMAPTable;
import java.sql.Connection;
/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class QuitteComplet extends ClassMAPTable{

  private String id;
  private java.sql.Date daty;
  private String etudiant;
  private String motif;
  private String categoriemotif;
  public QuitteComplet() {
    super.setNomTable("quittecomplet");
  }
  public String getTuppleID(){
return id;
}
public String getAttributIDName(){
return "id";
 }
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public void setDaty(java.sql.Date daty) {
    this.daty = daty;
  }
  public java.sql.Date getDaty() {
    return daty;
  }
  public void setMotif(String motif) {
    this.motif = motif;
  }
  public String getMotif() {
    return motif;
  }
  public void setCategoriemotif(String categoriemotif) {
    this.categoriemotif = categoriemotif;
  }
  public String getCategoriemotif() {
    return categoriemotif;
  }
  public void setEtudiant(String etudiant) throws Exception{
   // if(etudiant.compareToIgnoreCase("")==0) throw new Exception("Etudiant vide");
    this.etudiant = etudiant;
  }
  public String getEtudiant() {
    return etudiant;
  }
}