package quitte;
import bean.ClassMAPTable;
import java.sql.Connection;

/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class Quitte extends ClassMAPTable{

  private String id;
  private java.sql.Date daty;
  private String motif;
  private String categorieMotif;
  private String inscription;
  public Quitte() {
    super.setNomTable("quitte");
  }
  public String getTuppleID(){
  return id;
}
public String getAttributIDName(){
  return "id";
}
public void construirePK(Connection c) throws Exception{
 super.setNomTable("quitte");
 this.preparePk("QUI","getSeqQuitte");
 this.setId(makePK(c));
  }
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public void setDaty(java.sql.Date daty) {
    this.daty = daty;
  }
  public java.sql.Date getDaty() {
    return daty;
  }
  public void setMotif(String motif) {
    this.motif = motif;
  }
  public String getMotif() {
    return motif;
  }
  public void setCategorieMotif(String categorieMotif) {
    this.categorieMotif = categorieMotif;
  }
  public String getCategorieMotif() {
    return categorieMotif;
  }
  public void setInscription(String inscription) {
    this.inscription = inscription;
  }
  public String getInscription() {
    return inscription;
  }
}