/**
 * 25 ao�t 2015
 *
 * @author Manda
 */

package reporting;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.ReportExportConfiguration;
import net.sf.jasperreports.export.SimpleDocxReportConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOdtReportConfiguration;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import net.sf.jasperreports.export.SimplePptxReportConfiguration;

public class ReportingUtils {

    public ReportingUtils() {
    }

    private JasperPrint jasperPrint = null;

    public void PDF(String reportPath, List dataSource, Map param, OutputStream out) throws JRException, IOException {
        jasperPrint = fillReport(reportPath, dataSource, param);
        exportReport(jasperPrint, ReportType.PDF, out);
    }

    public void DOCX(String reportPath, List dataSource, Map param, OutputStream out) throws JRException, IOException {
        jasperPrint = fillReport(reportPath, dataSource, param);
        exportReport(jasperPrint, ReportType.DOCX, out);
    }

    public void XLSX(String reportPath, List dataSource, Map param, OutputStream out) throws JRException, IOException {
        jasperPrint = fillReport(reportPath, dataSource, param);
        exportReport(jasperPrint, ReportType.XLSX, out);
    }

    public void ODT(String reportPath, List dataSource, Map param, OutputStream out) throws JRException, IOException {
        jasperPrint = fillReport(reportPath, dataSource, param);
        exportReport(jasperPrint, ReportType.ODT, out);
    }

    public void PPT(String reportPath, List dataSource, Map param, OutputStream out) throws JRException, IOException {
        jasperPrint = fillReport(reportPath, dataSource, param);
        exportReport(jasperPrint, ReportType.PPT, out);
    }

    public void export(String reportPath, List dataSource, Map param, OutputStream out, ReportType reportType) throws JRException {
        jasperPrint = fillReport(reportPath, dataSource, param);
        exportReport(jasperPrint, reportType, out);
    }

    public JasperPrint fillReport(String reportPath, List dataSource, Map param) throws JRException {
        if (dataSource == null || dataSource.isEmpty()) {
            return JasperFillManager.fillReport(reportPath, param, new JREmptyDataSource());
        } else {
            JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(dataSource);
            return JasperFillManager.fillReport(reportPath, param, beanCollectionDataSource);
        }
    }

    public JasperPrint multipageLinking(JasperPrint page1, JasperPrint page2) {  
        List<JRPrintPage> pages = page2.getPages();  
        for (int count = 0; count<pages.size(); count++) {  
            page1.addPage(pages.get(count));  
        }
        return page1;  
    }  
    
    public void exportReport(JasperPrint jasperPrint, ReportType reporType, OutputStream out) throws JRException {
        Exporter jRExporter = null;
        ReportExportConfiguration configuration = null;
        switch (reporType) {
            case PDF:
                jRExporter = new JRPdfExporter();
                configuration = new SimplePdfReportConfiguration();
                System.out.println("==========> PDF  JRPdfExporter");
                break;

            case DOCX:
                jRExporter = new JRDocxExporter();
                configuration = new SimpleDocxReportConfiguration();
                break;

            case XLSX:
                jRExporter = new JRXlsxExporter();
                configuration = new SimplePptxReportConfiguration();
                break;

            case PPT:
                jRExporter = new JRPptxExporter();
                configuration = new SimplePptxReportConfiguration();
                break;

            case ODT:
                jRExporter = new JROdtExporter();
                configuration = new SimpleOdtReportConfiguration();
                break;

        }
        if (jRExporter != null) {
            System.out.println("jRExporter begin");
            jRExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            jRExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
            jRExporter.setConfiguration(configuration);
            jRExporter.exportReport();
            System.out.println("jRExporter end");
        } else {
            throw new JRException("Une erreur est survenue lors de la generation de l'etat.");
        }
    }

    public enum ReportType {

        PDF, DOCX, XLSX, ODT, PPT
    }

    private String setName(ReportType reportType) {

        return null;

    }
}
