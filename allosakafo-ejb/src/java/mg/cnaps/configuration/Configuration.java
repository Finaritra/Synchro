package mg.cnaps.configuration;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.Utilitaire;

public class Configuration extends ClassMAPTable{
    String id;
    String valmin;
    String valmax;
    String desce;
    String remarque;
    Date daty;
    String typeconfig;
    String compte_credit;

    public String getCompte_credit() {
        return compte_credit;
    }

    public void setCompte_credit(String compte_credit) {
        this.compte_credit = compte_credit;
    }

    public Configuration() {
        super.setNomTable("configuration");
    }

    public Configuration(String valmin, String valmax, String desce, String remarque, Date daty, String typeconfig) {
        super.setNomTable("configuration");
        setValmin(valmin);
        setValmax(valmax);
        setDesce(desce);
        setRemarque(remarque);
        setDaty(daty);
        setTypeconfig(typeconfig);
    }
    
    public void construirePK(Connection c) throws Exception{
        this.preparePk("CONF","getSeqConfiguration");
        this.setId(makePK(c));
    }
    
    public String getTuppleID() {
        return getId();
    }

    public String getAttributIDName() {
       return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValmin() {
        return Utilitaire.champNull(valmin);
    }

    public void setValmin(String valmin) {
        this.valmin = valmin;
    }

    public String getValmax() {
        return Utilitaire.champNull(valmax);
    }

    public void setValmax(String valmax) {
        this.valmax = valmax;
    }
    
    

    public String getDesce() {
        return Utilitaire.champNull(desce);
    }

    public void setDesce(String desce) {
        this.desce = desce;
    }

    public String getRemarque() {
        return Utilitaire.champNull(remarque);
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public String getTypeconfig() {
        return Utilitaire.champNull(typeconfig);
    }

    public void setTypeconfig(String typeconfig) {
        this.typeconfig = typeconfig;
    }
    
    
    
}
