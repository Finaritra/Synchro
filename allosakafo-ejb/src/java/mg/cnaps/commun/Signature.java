/**
 * 13 oct. 2015
 *
 * @author Manda
 */
package mg.cnaps.commun;

import bean.ClassMAPTable;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

public class Signature extends ClassMAPTable {

    private String id;
    private String idcnapsuser;
    private InputStream signature;

    public void construirePK(Connection c) throws Exception {
        this.preparePk("SIG", "getSeqSignature");
        this.setId(makePK(c));
    }

    @Override
    public String getTuppleID() {
        return this.getId();
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdcnapsuser() {
        return idcnapsuser;
    }

    public void setIdcnapsuser(String idcnapsuser) {
        this.idcnapsuser = idcnapsuser;
    }

    public InputStream getSignature() {
        return signature;
    }

    public void setSignature(InputStream signature) {
        this.signature = signature;
    }

    public Signature() {
        super.setNomTable("cnapsuser_signature");
    }

    public Signature(String idcnapsuser, InputStream signature) {
        setIdcnapsuser(idcnapsuser);
        setSignature(signature);
    }

    public InputStream getStreamSignature() throws SQLException{
        //if(signature != null)
          //  return signature.getBinaryStream();
        //else return null;
        return null;
    }
    
}
