/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.cnaps.commun;

import bean.ClassUser;
import java.sql.Date;
import java.sql.Connection;

public class VisaObject extends ClassUser{
    String id;
    String remarque;
    String objet;
    String refobjet;
    java.sql.Date daty;

    public String getId() {
        return id;
    }

    public VisaObject() {
    }
    
    public VisaObject(String remarque, String objet, String refobjet, Date daty) {
        this.setRemarque(remarque);
        this.setObjet(objet);
        this.setRefobjet(refobjet);
        this.setDaty(daty);
    }
    
    
    public void construirePK(Connection c) throws Exception{
        this.preparePk("VIS", "GETSEQVISAOBJECT");
        this.setId(makePK(c));
    }
    
    public void setId(String id) {
        this.id = id;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getRefobjet() {
        return refobjet;
    }

    public void setRefobjet(String refobjet) {
        this.refobjet = refobjet;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
    
}
