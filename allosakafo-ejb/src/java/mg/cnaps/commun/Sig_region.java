/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.cnaps.commun;

import bean.ClassMAPTable;
import java.sql.Connection;
import utilitaire.Utilitaire;

/**
 *
 * @author Tafitasoa
 */
public class Sig_region extends ClassMAPTable {
    
    private String id;
    private String val;
    private String desce;
    private String code_faritany;
    
    public Sig_region(){
        super.setNomTable("sig_region");
    }

    public Sig_region(String val, String desce, String code_faritany) throws Exception {
        setVal(val);
        setDesce(desce);
        setCode_faritany(code_faritany);
    }
    
    public void construirePK(Connection c) throws Exception{
        this.preparePk("REG","getSeqsigregion");
        this.setId(makePK(c));
    }

    @Override
    public String getTuppleID() {
        return this.getId();
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the val
     */
    public String getVal() {
        return Utilitaire.champNull(val);
    }

    /**
     * @param val the val to set
     */
    public void setVal(String val) {
        this.val = val;
    }

    /**
     * @return the desce
     */
    public String getDesce() {
        return Utilitaire.champNull(desce);
    }

    /**
     * @param desce the desce to set
     */
    public void setDesce(String desce) {
        this.desce = desce;
    }

    /**
     * @return the code_faritany
     */
    public String getCode_faritany() {
        return Utilitaire.champNull(code_faritany);
    }

    /**
     * @param code_faritany the code_faritany to set
     */
    public void setCode_faritany(String code_faritany) throws Exception {
        if(getMode().compareTo("modif")!=0)
        {
            this.code_faritany = code_faritany;
            return;
        }
        if(code_faritany=="") throw new Exception("Champ code faritany manquant");
        this.code_faritany = code_faritany;
    }
    
}
