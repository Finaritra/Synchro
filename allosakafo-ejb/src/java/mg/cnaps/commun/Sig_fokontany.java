/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.cnaps.commun;

import bean.ClassMAPTable;
import java.sql.Connection;
import utilitaire.Utilitaire;

/**
 *
 * @author Tafitasoa
 */
public class Sig_fokontany extends ClassMAPTable {

    private String id;
    private String val;
    private String desce;
    private String code_commune;
    
    public Sig_fokontany(){
        super.setNomTable("sig_fokontany");
    }

    public Sig_fokontany(String val, String desce, String code_commune) throws Exception {
        setVal(val);
        setDesce(desce);
        setCode_commune(code_commune);
    }
    
    public void construirePK(Connection c) throws Exception{
        this.preparePk("FOK","getSeqsigfokontany");
        this.setId(makePK(c));
    }

    @Override
    public String getTuppleID() {
        return this.getId();
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the val
     */
    public String getVal() {
        return Utilitaire.champNull(val);
    }

    /**
     * @param val the val to set
     */
    public void setVal(String val) throws Exception {
        if(getMode().compareTo("modif")!=0)
        {
            this.val = val;
            return;
        }
        if(val=="") throw new Exception("Champ Valeur manquant");
        this.val = val;
    }

    /**
     * @return the desce
     */
    public String getDesce() {
        return Utilitaire.champNull(desce);
    }

    /**
     * @param desce the desce to set
     */
    public void setDesce(String desce) {
        this.desce = desce;
    }

    /**
     * @return the code_commune
     */
    public String getCode_commune() {
        return Utilitaire.champNull(code_commune);
    }

    /**
     * @param code_commune the code_commune to set
     */
    public void setCode_commune(String code_commune) throws Exception {
        if(getMode().compareTo("modif")!=0)
        {
            this.code_commune = code_commune;
            return;
        }
        if(code_commune=="") throw new Exception("Champ Code_Commune manquant");
        this.code_commune = code_commune;
    }

}
