/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.cnaps.commun;

public class Constante {

    private static final String dr_siege = "DR00011";
    private static final String dr_abrev = "42";
    private static final String id_classe = "CLS0016"; // cab
    public static final String idtype_carte_prepaye = "1";
    public static final String idtype_carte_postpaye = "2";
    public static final String idtype_carte_fille = "3";
    public static final String idtype_carte_mere = "4";
    public static final String ovModePaiementCheque = "MP12";
    public static final String ovModePaiementEspece = "MP1";
    public static String idJovenna = "1";
    public static final String idLiquidite = "1";
    public static final String idBesoinFormation = "FTD2";
    public static final String listeIdArticleVehicule = "'TP000056','TP000024','TP000034','TP000044'";
    public static final String idEntretienVehiculeInterne = "0";
    public static final String libelleEntretienVehiculeInterne = "Interne";
    public static final String idEntretienVehiculeExterne = "1";
    public static final String libelleEntretienVehiculeExterne = "Externe";
    public static final double fraisLivraison = 3000;
    public static final int dureeLivraisonDefaut= utilitaire.Constante.attenteLivraison ;
    public static String getId_classe() {
	return id_classe;
    }

    public static String getDr_abrev() {
	return dr_abrev;
    }

    public static String getDr_siege() {
	return dr_siege;
    }

    /**
     * @return the idJovenna
     */
    public static String getIdJovenna() {
	return idJovenna;
    }

    /**
     * @param aIdJovenna the idJovenna to set
     */
    public static void setIdJovenna(String aIdJovenna) {
	idJovenna = aIdJovenna;
    }

    public static double getFraisLivraison() {
        return fraisLivraison;
    }
    

}
