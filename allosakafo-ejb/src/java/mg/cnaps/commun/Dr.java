/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.cnaps.commun;

import bean.ClassMAPTable;
import java.sql.Connection;
import utilitaire.Utilitaire;

/**
 *
 * @author Anthony
 */
public class Dr extends ClassMAPTable{
    private String id;
    private String val;
    private String desce;
    private String dr_abreviation;
    private String numero_fixe;
    private String numero_mobile;
    private String dr_sigle;
    
    public Dr() {
        super.setNomTable("sig_dr");
    }

    public Dr(String val, String desce, String dr_abreviation, String numero_fixe, String numero_mobile, String dr_sigle) {
        setVal(val);
        setDesce(desce);
        setDr_abreviation(dr_abreviation);
        setNumero_fixe(numero_fixe);
        setNumero_mobile(numero_mobile);
        setDr_sigle(dr_sigle);
    }
    
    public String getTuppleID(){
        return id;
    }
    public String getAttributIDName(){
        return "id";
    }
    public void construirePK(Connection c) throws Exception{
            this.preparePk("DR","getSeqsigdr");
            this.setId(makePK(c));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVal() {
        return Utilitaire.champNull(val);
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getDesce() {
        return Utilitaire.champNull(desce);
    }

    public void setDesce(String desce) {
        this.desce = desce;
    }

    public String getDr_abreviation() {
        return Utilitaire.champNull(dr_abreviation);
    }

    public void setDr_abreviation(String dr_abreviation) {
        this.dr_abreviation = dr_abreviation;
    }

    public String getNumero_fixe() {
        return Utilitaire.champNull(numero_fixe);
    }

    public void setNumero_fixe(String numero_fixe) {
        this.numero_fixe = numero_fixe;
    }

    public String getNumero_mobile() {
        return Utilitaire.champNull(numero_mobile);
    }

    public void setNumero_mobile(String numero_mobile) {
        this.numero_mobile = numero_mobile;
    }

    public String getDr_sigle() {
        return Utilitaire.champNull(dr_sigle);
    }

    public void setDr_sigle(String dr_sigle) {
        this.dr_sigle = dr_sigle;
    }
    public String getValColLibelle(){
        return val;
    }
    
}
