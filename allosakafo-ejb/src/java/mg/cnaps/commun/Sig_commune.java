/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.cnaps.commun;

import bean.ClassMAPTable;
import java.sql.Connection;
import utilitaire.Utilitaire;

/**
 *
 * @author Tafitasoa
 */
public class Sig_commune extends ClassMAPTable {
    
    private String id;
    private String val;
    private String desce;
    private String code_region;
    
    public Sig_commune(){
        super.setNomTable("sig_commune");
    }

    public Sig_commune(String val, String desce, String code_region) throws Exception {
        setVal(val);
        setDesce(desce);
        setCode_region(code_region);
    }
    
    public void construirePK(Connection c) throws Exception{
        this.preparePk("COMM","getSeqsigcommune");
        this.setId(makePK(c));
    }

    @Override
    public String getTuppleID() {
        return this.getId();
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the val
     */
    public String getVal() {
        return Utilitaire.champNull(val);
    }

    /**
     * @param val the val to set
     */
    public void setVal(String val) {
        this.val = val;
    }

    /**
     * @return the desce
     */
    public String getDesce() {
        return Utilitaire.champNull(desce);
    }

    /**
     * @param desce the desce to set
     */
    public void setDesce(String desce) {
        this.desce = desce;
    }

    /**
     * @return the code_region
     */
    public String getCode_region() {
        return Utilitaire.champNull(code_region);
    }

    /**
     * @param code_region the code_region to set
     */
    public void setCode_region(String code_region) throws Exception {
        if(getMode().compareTo("modif")!=0)
        {
            this.code_region = code_region;
            return;
        }
        if(code_region=="") throw new Exception("Champ code region manquant");
        this.code_region = code_region;
    }
    
}
