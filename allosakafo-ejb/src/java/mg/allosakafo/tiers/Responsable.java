/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.tiers;

import bean.ClassMAPTable;
import java.sql.Connection;

/**
 *
 * @author Joe
 */
public class Responsable extends ClassMAPTable{
    
    private String id, nom, prenom, type, contact;
    
    public Responsable() {
        this.setNomTable("as_responsable");
    }
    
    public String getValColLibelle(){
        return nom + " " + prenom;
    }
    
    public String getTuppleID() {
        return id;
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("ASR", "getSeqASResponsable");
        this.setId(makePK(c));
    }
    
    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (nom != null && nom.compareToIgnoreCase("") != 0) {
                this.nom = nom;
                return;
            } else {
                throw new Exception("Nom du responsable invalide");
            }
        }
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
    
    
}
