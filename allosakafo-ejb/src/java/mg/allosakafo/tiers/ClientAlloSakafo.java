/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.tiers;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.Utilitaire;
import bean.CGenUtil;

/**
 *
 * @author Joe
 */
public class ClientAlloSakafo extends ClassMAPTable {

    private String id, nom, prenom, sexe, adresse, telephone, fb;
    private Date datenaissance;

    public ClientAlloSakafo() {
        this.setNomTable("as_client");
    }
	
	public ClientAlloSakafo(String nom, String telephone) throws Exception{
        this.setNomTable("as_client");
		this.setNom(nom);
		this.setTelephone(telephone);
    }
	
	public ClientAlloSakafo(String nom, String telephone, String adresse) throws Exception{
        this.setNomTable("as_client");
		this.setNom(nom);
		this.setTelephone(telephone);
		this.setAdresse(adresse);
    }
    
    public String getValColLibelle(){
        return nom + " " + prenom;
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("CAS", "getSeqASClient");
        this.setId(makePK(c));
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (nom != null && nom.compareToIgnoreCase("") != 0) {
                this.nom = nom;
                return;
            } else {
                throw new Exception("Nom du clients invalide");
            }
        }
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) 
        {
            if (telephone == null || telephone.compareTo("") == 0) {
                    throw new Exception("Champ telephone manquant");
            }
            else
            {
                String[] numeros = Utilitaire.split(telephone, ";");
                for (int i=0; i<numeros.length; i++)
                {
                    if (!numeros[i].startsWith("0") && !numeros[i].startsWith("261")) throw new Exception("Numero invalide");
                    else if (numeros[i].startsWith("0") && numeros[i].length() != 10) throw new Exception("Numero invalide");
                    else if (numeros[i].startsWith("261") && numeros[i].length() != 12) throw new Exception("Numero invalide");
                }
                this.telephone = telephone;
            }
        }
        else this.telephone = telephone;
    }

    public String getFb() {
        return fb;
    }

    public void setFb(String fb) {
        this.fb = fb;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }
    
	public boolean estDouteux() throws Exception{
		ClientDouteux[] results = (ClientDouteux[])CGenUtil.rechercher(new ClientDouteux(), "select * from as_clientdouteux where idclient='"+this.getId()+"'");
		if (results.length > 0) return true;
		return false;
	}
    
}
