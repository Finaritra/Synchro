/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.tiers;

import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
/**
 *
 * @author Joe
 */
public class Vehicule extends ClassEtat{
    
    private String id, nom, typevehicule;
    private Date dateacquisition;
    
    public Vehicule() {
        this.setNomTable("as_vehicule");
    }
    
    public String getValColLibelle(){
        return nom;
    }
    
    public void construirePK(Connection c) throws Exception {
        this.preparePk("ASV", "getSeqASVehicule");
        this.setId(makePK(c));
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (nom != null && nom.compareToIgnoreCase("") != 0) {
                this.nom = nom;
                return;
            } else {
                throw new Exception("Nom vehicule invalide");
            }
        }
        this.nom = nom;
    }

    public String getTypevehicule() {
        return typevehicule;
    }

    public void setTypevehicule(String type) {
        this.typevehicule = type;
    }

    public Date getDateacquisition() {
        return dateacquisition;
    }

    public void setDateacquisition(Date dateacquisition) {
        this.dateacquisition = dateacquisition;
    }
    
    
}
