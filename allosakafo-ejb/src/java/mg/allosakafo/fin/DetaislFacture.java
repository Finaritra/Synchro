/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.fin;

import bean.ClassMAPTable;

/**
 *
 * @author John
 */
public class DetaislFacture extends ClassMAPTable {
    
    private String id, idfacture, idpproduit;
    
    private double qte, pu, montant;
    
    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the idfacture
     */
    public String getIdfacture() {
        return idfacture;
    }

    /**
     * @param idfacture the idfacture to set
     */
    public void setIdfacture(String idfacture) {
        this.idfacture = idfacture;
    }

    /**
     * @return the idpproduit
     */
    public String getIdpproduit() {
        return idpproduit;
    }

    /**
     * @param idpproduit the idpproduit to set
     */
    public void setIdpproduit(String idpproduit) {
        this.idpproduit = idpproduit;
    }

    /**
     * @return the qte
     */
    public double getQte() {
        return qte;
    }

    /**
     * @param qte the qte to set
     * @throws java.lang.Exception
     */
    public void setQte(double qte) throws Exception {
        if(montant < 0) throw new Exception("Quantit� invalide.");
        this.qte = qte;
    }

    /**
     * @return the pu
     */
    public double getPu() {
        return pu;
    }

    /**
     * @param pu the pu to set
     * @throws java.lang.Exception
     */
    public void setPu(double pu) throws Exception {
        if(montant < 0) throw new Exception("Prix unitaire invalide.");
        this.pu = pu;
    }

    /**
     * @return the montant
     */
    public double getMontant() {
        return montant;
    }

    /**
     * @param montant the montant to set
     * @throws java.lang.Exception
     */
    public void setMontant(double montant) throws Exception {
        if(montant < 0) throw new Exception("Montant invalide.");
        this.montant = montant;
    }
    
}
