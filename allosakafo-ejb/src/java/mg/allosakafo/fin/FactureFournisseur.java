/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.fin;

import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author Joe
 */
public class FactureFournisseur extends ClassEtat{
    
    private String id, remarque, designation, typefacture, idobjet, idbudgetdepense;
    private double tva;
    private Date daty;
    private String beneficiaire;
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("FF", "get_seq_facturefournisseur");
        this.setId(makePK(c));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getTypefacture() {
        return typefacture;
    }

    public void setTypefacture(String typefacture) {
        this.typefacture = typefacture;
    }

    public String getIdobjet() {
        return idobjet;
    }

    public void setIdobjet(String idobjet) {
        this.idobjet = idobjet;
    }

    public String getIdbudgetdepense() {
        return idbudgetdepense;
    }

    public void setIdbudgetdepense(String idbudgetrecette) {
        this.idbudgetdepense = idbudgetrecette;
    }

    public double getTva() {
        return tva;
    }

    public void setTva(double tva) {
        this.tva = tva;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }
    
    public FactureFournisseur(){
        super.setNomTable("FACTUREFOURNISSEUR");
    }
    
    public String getBeneficiaire(){
        return this.beneficiaire;
    }
    
    public void setBeneficiaire(String _beneficiaire){
        this.beneficiaire = _beneficiaire;
    }
}
