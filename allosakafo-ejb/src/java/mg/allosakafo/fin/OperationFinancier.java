/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mg.allosakafo.fin;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.Utilitaire;

public class OperationFinancier extends ClassMAPTable{
    private String id, idcategorieoperation, idtypeoperation, idservice;
    private double prixunitaire, entree, sortie;
    private Date dateoperation;

    public OperationFinancier() {
        setNomTable("operationfinancier");
    }

    public OperationFinancier(String id, String idcategorieoperation, String idtypeoperation, String idservice, double prixunitaire, double entree, double sortie, Date dateoperation) throws Exception {
        setNomTable("operationfinancier");
        setId(id);
        setIdcategorieoperation(idcategorieoperation);
        setIdtypeoperation(idtypeoperation);
        setIdservice(idservice);
        setPrixunitaire(prixunitaire);
        setEntree(entree);
        setSortie(sortie);
        setDateoperation(dateoperation);
    }

    public String getId() {
        return id;
    }

    public String getIdcategorieoperation() {
        return idcategorieoperation;
    }

    public String getIdtypeoperation() {
        return idtypeoperation;
    }

    public String getIdservice() {
        return idservice;
    }

    public double getPrixunitaire() {
        return prixunitaire;
    }

    public double getEntree() {
        return entree;
    }

    public double getSortie() {
        return sortie;
    }

    public Date getDateoperation() {
        return dateoperation;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIdcategorieoperation(String idcategorieoperation) {
        this.idcategorieoperation = idcategorieoperation;
    }

    public void setIdtypeoperation(String idtypeoperation) {
        this.idtypeoperation = idtypeoperation;
    }

    public void setIdservice(String idservice) {
        this.idservice = idservice;
    }

    public void setPrixunitaire(double prixunitaire) throws Exception {
        if (prixunitaire < 0)
            throw new Exception("Prix unitaire invalide.");
        this.prixunitaire = prixunitaire;
    }

    public void setEntree(double entree) throws Exception {
        if (entree < 0)
            throw new Exception("Entrée invalide.");
        this.entree = entree;
    }

    public void setSortie(double sortie) throws Exception{
        if (sortie < 0)
            throw new Exception("Sortie invalide.");
        this.sortie = sortie;
    }

    public void setDateoperation(Date dateoperation) throws Exception{
        if (dateoperation.compareTo(Utilitaire.dateDuJourSql()) > 0)
            throw new Exception("Date d'opération invalide.");
        this.dateoperation = dateoperation;
    }
    
    
    
    public void construirePK(Connection c) throws Exception {

        this.preparePk("OPFI", "getseqoperationfinancier");
        this.setId(makePK(c));
    }
    
    public String getTuppleID() {
        return getId();
    }

    
    public String getAttributIDName() {
        return "id";
    }
    
    
}
