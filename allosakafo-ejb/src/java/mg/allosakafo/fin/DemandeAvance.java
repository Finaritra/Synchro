package mg.allosakafo.fin;

import bean.ClassMAPTable;
import java.sql.Connection;
import utilitaire.*;
import bean.*;
import java.sql.Date;

public class DemandeAvance extends ClassEtat {

    private String id;
    private Date daty;
    private String motif;
    private String catMotif;
    private String avancement;
    private int nbrRemboursement, mois_premier_remboursement, annee_premier_remboursement;
    private double montant;
    private String remarque;

    public int getMois_premier_remboursement() {
        return mois_premier_remboursement;
    }

    public void setMois_premier_remboursement(int mois_premier_remboursement) {
        this.mois_premier_remboursement = mois_premier_remboursement;
    }

    public int getAnnee_premier_remboursement() {
        return annee_premier_remboursement;
    }

    public void setAnnee_premier_remboursement(int annee_premier_remboursement) {
        this.annee_premier_remboursement = annee_premier_remboursement;
    }
    
    public DemandeAvance() {
        super.setNomTable("demandeavance");
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public void construirePK(Connection c) throws Exception {
        super.setNomTable("demandeavance");
        this.preparePk("DEA", "get_Seq_demandeavance");
        this.setId(makePK(c));
    }

    //-----getter

    public Date getDaty() {
        return this.daty;
    }

    public String getMotif() {
        return this.motif;
    }

    public String getCatMotif() {
        return this.catMotif;
    }

    public String getAvancement() {
        return this.avancement;
    }

    public int getNbrRemboursement() {
        return this.nbrRemboursement;
    }

    public double getMontant() {
        return this.montant;
    }

    //----setter---

    public void setDaty(Date d) {
        /*java.sql.Date today=Utilitaire.dateDuJourSql();
         if(Utilitaire.compareDaty(today,d)>0)throw new Exception("Date invalide!");*/
        this.daty = d;
    }

    public void setMotif(String m) throws Exception {
        if (m.compareToIgnoreCase("") == 0) {
            throw new Exception("Motif ne doit pas etre vide");
        }
        this.motif = m;
    }

    public void setCatMotif(String c) throws Exception {
        if (c.compareToIgnoreCase("") == 0) {
            throw new Exception("CatMotif ne doit pas etre vide");
        }
        this.catMotif = c;
    }

    public void setAvancement(String av) throws Exception {
        if (av.compareToIgnoreCase("") == 0) {
            throw new Exception("avancement ne doit pas etre vide");
        }
        this.avancement = av;
    }

    public void setNbrRemboursement(int n) throws Exception {
        if (n >= 800) {
            throw new Exception("Nombre de remboursement non valide!Trop grand");
        }
        this.nbrRemboursement = n;
    }

    public void setMontant(double m) throws Exception {
        if (m <= 0) {
            throw new Exception("Montant invalide!doit �tre sup�rieur � 0");
        }
        this.montant = m;
    }

    public DemandeAvance(String id, java.sql.Date d, String motif, String cat, String avancement, int nb, double mont, int etat) throws Exception {
        super.setNomTable("DemandeAvance");

        this.setId(id);
        this.setDaty(d);
        this.setMotif(motif);
        this.setCatMotif(cat);
        this.setAvancement(avancement);
        this.setNbrRemboursement(nb);
        this.setMontant(mont);
        this.setEtat(etat);
    }

    public String getRemarque() {
        return this.remarque;
    }

    public void setRemarque(String r) {
        this.remarque = r;
    }

    public DemandeAvance(String id, java.sql.Date daty, String motif, String cat, String avancement, int nb, double mont) throws Exception {
        super.setNomTable("DemandeAvance");
        this.setId(id);
        this.setDaty(daty);
        this.setMotif(motif);
        this.setCatMotif(cat);
        this.setAvancement(avancement);
        this.setNbrRemboursement(nb);
        this.setMontant(mont);
    }
    
}
