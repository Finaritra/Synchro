/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.fin;

import bean.CGenUtil;
import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import mg.allosakafo.ded.OrdonnerPayement;
import service.AlloSakafoService;
import utilitaire.Constante;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Joe
 */
public class MvtCaisse extends ClassEtat{
    
    private String id, designation, iddevise, idmode, idcaisse, remarque, tiers, numpiece, numcheque, typemvt, idordre,vnumPiece,vnumCheque;

    public String getVnumPiece() {
        return vnumPiece;
    }

    public void setVnumPiece(String vnumPiece) {
        this.vnumPiece = vnumPiece;
    }

    public String getVnumCheque() {
        return vnumCheque;
    }

    public void setVnumCheque(String vnumCheque) {
        this.vnumCheque = vnumCheque;
    }
    private double debit, credit, reste;
    private Date daty, datyvaleur;
    
	public MvtCaisse (){
        super.setNomTable("mvtcaisse");
    }
	
	public MvtCaisse (String designation, String iddevise, String idmode, String idcaisse, String remarque, String tiers, String idordre, double credit, double debit, Date daty, String typemvt) throws Exception{
        super.setNomTable("mvtcaisse");
		this.setDesignation(designation);
		this.setIddevise(iddevise);
		this.setIdmode(idmode);
		this.setIdcaisse(idcaisse);
		this.setRemarque(remarque);
		this.setTiers(tiers);
		this.setIdordre(idordre);
		this.setCredit(credit);
		this.setDebit(debit);
		this.setDaty(daty);
		this.setTypemvt(typemvt);
    }
    public MvtCaisse (String designation, String iddevise, String idmode, String idcaisse, double credit,Date daty, String typemvt) throws Exception{
        super.setNomTable("mvtcaisse");
		this.setDesignation(designation);
		this.setIddevise(iddevise);
		this.setIdmode(idmode);
		this.setIdcaisse(idcaisse);
		this.setCredit(credit);
		this.setDaty(daty);
		this.setTypemvt(typemvt);
    }
        
        public MvtCaisse (String designation, String iddevise, String idmode, String idcaisse, String remarque, String tiers, String idordre, double credit, double debit, Date daty, String typemvt,int etat) throws Exception{
        super.setNomTable("mvtcaisse");
		this.setDesignation(designation);
		this.setIddevise(iddevise);
		this.setIdmode(idmode);
		this.setIdcaisse(idcaisse);
		this.setRemarque(remarque);
		this.setTiers(tiers);
		this.setIdordre(idordre);
		this.setCredit(credit);
		this.setDebit(debit);
		this.setDaty(daty);
		this.setTypemvt(typemvt);
                this.setEtat(etat);
    }
	
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("MVT", "getseqmvtcaisse");
        this.setId(makePK(c));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getIddevise() {
        return iddevise;
    }

    public void setIddevise(String iddevise) {
        this.iddevise = iddevise;
    }

    public String getIdmode() {
        return idmode;
    }

    public void setIdmode(String idmode) {
        this.idmode = idmode;
    }

    public String getIdcaisse() {
        return idcaisse;
    }

    public void setIdcaisse(String idcaisse) throws Exception {
        if (idcaisse.compareToIgnoreCase("-")==0 && this.getMode().compareToIgnoreCase("modif")==0) throw new Exception ("caisse vide");
        this.idcaisse = idcaisse;
        
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getTiers() {
        return tiers;
    }

    public void setTiers(String tiers) {
        this.tiers = tiers;
    }

    public String getNumpiece() {
        return numpiece;
    }

    public void setNumpiece(String numpiece) {
        this.numpiece = numpiece;
    }

    public String getNumcheque() {
        return numcheque;
    }

    public void setNumcheque(String numcheque) {
        this.numcheque = numcheque;
    }

    public String getTypemvt() {
        return typemvt;
    }

    public void setTypemvt(String typemvt) {
        this.typemvt = typemvt;
    }

    public String getIdordre() {
        return idordre;
    }

    public void setIdordre(String idordre) {
        this.idordre = idordre;
    }

    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) throws Exception {
        if(this.getMode().compareToIgnoreCase("select")==0)
        {
            this.debit = debit;
        }
        if(debit<0)throw new Exception("montant debit negatif");
        this.debit = debit;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) throws Exception {
        if(this.getMode().compareToIgnoreCase("select")==0)
        {
            this.credit = credit;
        }
        if(credit<0)throw new Exception("montant credit negatif");
        this.credit = credit;
    }
    
    public double getReste() {
        return reste;
    }

    public void setReste(double reste){
         this.reste = reste;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public Date getDatyvaleur() {
        return datyvaleur;
    }

    public void setDatyvaleur(Date datyvaleur) {
        this.datyvaleur = datyvaleur;
    }
    
    public void controler(java.sql.Connection c)throws Exception
    {
        Report report = new Report();
       // Date dateavant = Utilitaire.ajoutJourDate(this.getDaty(), 1);
        Report[] repts = (Report[])CGenUtil.rechercher(report, null, null, c, " and caisse like '"+this.getIdcaisse()+"' and daty>='"+Utilitaire.datetostring(this.getDaty())+"' and etat='"+ConstanteEtat.getEtatValider() +"'");
        if (repts.length >0) throw new Exception("Impossible d effectuer ce mouvement, la caisse est deja cloturee pour cette date");
        if(this.getTypemvt()!=null && this.getTypemvt().compareToIgnoreCase(Constante.financeTypeAnnulation)==0)return;
        EtatOP[] sop = (EtatOP[])CGenUtil.rechercher(new EtatOP(), null, null, " AND ID = '"+this.getIdordre()+"'");
        if(sop.length>0){
            if(sop[0].getReste() == 0) throw new Exception("Mvt Caisse DEJA PAYE");
            if(this.getDebit() > sop[0].getReste()) throw new Exception("Montant superieur reste a payer");
        }
    }
    public OrdonnerPayement getOP(Connection c) throws Exception
    {
        OrdonnerPayement crtOrd=new OrdonnerPayement();
        crtOrd.setId(this.getIdordre());
        OrdonnerPayement ret[]=(OrdonnerPayement[])CGenUtil.rechercher(crtOrd, null,null,c,"");
        if(ret.length==0)return null;
        return ret[0];
    }
    public void verifCaisse()throws Exception{
        /*if(this.getIdcaisse().compareToIgnoreCase(AlloSakafoService.getCaisseDefaut())!=0){
            throw new Exception ("Action non permise");
        }*/
		
    }
    /*@Override
    public int annulerVisa(String user,Connection c) throws Exception
    {
        if(getEtat()>ConstanteEtat.getEtatValider()) throw new Exception("Impossible � annuler, superieur valide");
        OrdonnerPayement op=getOP(c);
        setMode("modif");
        op.annulerVisa(user, c);
        return this.updateEtat(ConstanteEtat.getEtatCreer(), this.getTuppleID(), c);
    }*/
}
