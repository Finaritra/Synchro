/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.fin;

import bean.ClassMAPTable;
import java.sql.Connection;

/**
 *
 * @author tahina
 */
public class LiaisonMvtIntraCaisse extends ClassMAPTable {
    String id,source,contre,remarque;
    java.sql.Date daty;

    public LiaisonMvtIntraCaisse()
    {
        this.setNomTable("LiaisonMvtIntraCaisse");
        this.setNomSequenceDirecte("seqLiaisonIntraCaisse");
        this.setINDICE_PK("LIC");
        
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public java.sql.Date getDaty() {
        return daty;
    }

    public void setDaty(java.sql.Date daty) {
        this.daty = daty;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getContre() {
        return contre;
    }

    public void setContre(String contre) {
        this.contre = contre;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
    
}
