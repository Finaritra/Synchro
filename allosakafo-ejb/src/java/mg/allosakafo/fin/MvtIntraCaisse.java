/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.fin;

import bean.CGenUtil;
import bean.ClassEtat;
import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import service.AlloSakafoService;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;

/**
 *
 * @author rajao
 */
public class MvtIntraCaisse extends ClassEtat{
    private String id;
    private Date daty;
    private String caissedepart,caissearrive,source;
    private double montant,rembourse,reste;
    
    //Tsy maintsy ny restauDefaut no d�part na arriv�e
    @Override
    public void controler(Connection c) throws Exception {
        /*String maCaisse = AlloSakafoService.getCaisseDefaut();
        if(maCaisse.compareToIgnoreCase(this.getCaissedepart())!=0 || maCaisse.compareToIgnoreCase(this.getCaissearrive())!=0){
            throw new Exception("Action non permise. Votre caisse doit figurer au d�part ou � l'arriv�e");
        }*/
    }
    
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    

    public double getRembourse() {
        return rembourse;
    }

    public void setRembourse(double rembourse) {
        this.rembourse = rembourse;
    }

    public double getReste() {
        return reste;
    }

    public void setReste(double reste) {
        this.reste = reste;
    }
    private String remarque;
    
    public MvtIntraCaisse()
    {
        super.setNomTable("mvtintracaisse");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        
        this.id = id;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public String getCaissedepart() {
        return caissedepart;
    }

    public void setCaissedepart(String caissedepart) throws Exception {
        if(this.getMode().compareToIgnoreCase("modif")!=0)this.caissedepart = caissedepart;
        if(getCaissearrive()!=null && caissedepart.compareToIgnoreCase(getCaissearrive())==0)throw new Exception("caisse depart et arrivee identique");
        this.caissedepart = caissedepart;
        
    }

    public String getCaissearrive() {
        return caissearrive;
    }

    public void setCaissearrive(String caissearrive) throws Exception {
        if(this.getMode().compareToIgnoreCase("modif")!=0)this.caissearrive = caissearrive;
        if(getCaissedepart()!=null && caissearrive.compareToIgnoreCase(getCaissedepart())==0)throw new Exception("caisse depart et arrivee identique");
        this.caissearrive = caissearrive;
    }

    public double getMontant() {
        
        return montant;
    }

    public void setMontant(double montant)throws Exception {
        if(this.getMode().compareToIgnoreCase("modif")==0 && montant<=0){
            throw new Exception("La valeur du montant est n�gative ou 0");
        }
        this.montant = montant;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    /*public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }*/
    
     public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("MVT", "getseqmvtintracaisse");
        this.setId(makePK(c));
    }
    public MvtIntraCaisse[] getContre(Connection c,String nomTable)throws Exception
    {
        if(this.estCredit()==false)return null;
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            MvtIntraCaisse crt=new MvtIntraCaisse();
            crt.setNomTable("MvtIntraCaisseSource");
            if(nomTable!=null&&nomTable.compareToIgnoreCase("")!=0)crt.setNomTable(nomTable);
            crt.setSource(this.getId());

            return (MvtIntraCaisse[])CGenUtil.rechercher(crt, null, null, c, "") ;
        }
        catch(Exception e)
        {
            if(estOuvert==true)c.rollback();
            throw e;
        }
        finally
        {
            if(estOuvert==true)c.close();
        }
    }
    public MvtIntraCaisse getSource(Connection c)throws Exception
    {
        MvtIntraCaisse crt=new MvtIntraCaisse();
        crt.setNomTable("MvtIntraCaisse_SumSource");
        LiaisonMvtIntraCaisse liaison=getLiaison(c);
        if(liaison!=null)crt.setId(liaison.getSource());
        else return null;
        MvtIntraCaisse[] valiny=(MvtIntraCaisse[])CGenUtil.rechercher(crt, null, null, c, "") ;
        if(valiny.length==0)return null;
        return valiny[0];
    }
    public LiaisonMvtIntraCaisse getLiaison(Connection c)throws Exception
    {
        LiaisonMvtIntraCaisse crt=new LiaisonMvtIntraCaisse();
        crt.setContre(this.getId());
        LiaisonMvtIntraCaisse[] ret=(LiaisonMvtIntraCaisse[])CGenUtil.rechercher(crt, null, null, c,"");
        if(ret.length>0)return ret[0];
        return null;
    }
    public boolean estCredit() throws Exception
    {
        if(this.getCaissearrive().compareToIgnoreCase("CRD")==0) return true;
        return false;
    }
    public boolean estContreCredit() throws Exception
    {
        if(this.getCaissedepart().compareToIgnoreCase("CRD")==0) return true;
        return false;
    }
    
    public Object validerObject(String u, Connection c) throws Exception
    {
        if(this.estContreCredit()==true)
        {
            MvtIntraCaisse sourceComplet=this.getSource(c);
            if(sourceComplet!=null&&sourceComplet.getReste()<this.getMontant()) throw new Exception("Montant superieur au reste cr�dit");
        }
        return super.validerObject(u, c);
    }
    public ClassMAPTable createObject(String u,Connection c)throws Exception
    {
        ClassMAPTable ret= super.createObject(u, c);
        if(this.estContreCredit()==true&&this.getSource()!=null&&this.getSource().compareToIgnoreCase("")!=0)
        {
            LiaisonMvtIntraCaisse li=new LiaisonMvtIntraCaisse();
            li.setDaty(this.getDaty());        
            li.setContre(ret.getTuppleID());
            li.setSource(this.getSource());
            li.insertToTableWithHisto(u, c);
        }
        return ret;
    }
    
}
