/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.fin;

import bean.ClassMAPTable;
import java.sql.Date;

/**
 *
 * @author Joe
 */
public class OrdreDePaiementGroupe extends ClassMAPTable{
    
    /*
    vue: ny mois sy année alaina amin'ny date ihany 
    table oredre de paiement
    liste + tableau recap fotsiny fa tsy misy saisie
    
    
    manao vue ray mitovy amin'io ihany ho an'ny ordre de recette
    */
    private Date daty;
    private String idordre, description, modepaiement;
    private double montant;
    private int mois, annee;

    public OrdreDePaiementGroupe() {
        this.setNomTable("ordredepaiementgroupe");
    }
    
    @Override
    public String getTuppleID() {
        return null;
    }

    @Override
    public String getAttributIDName() {
        return "";
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public String getIdordre() {
        return idordre;
    }

    public void setIdordre(String idordre) {
        this.idordre = idordre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModepaiement() {
        return modepaiement;
    }

    public void setModepaiement(String modepaiement) {
        this.modepaiement = modepaiement;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public int getMois() {
        return mois;
    }

    public void setMois(int mois) {
        this.mois = mois;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }
    
    
}
