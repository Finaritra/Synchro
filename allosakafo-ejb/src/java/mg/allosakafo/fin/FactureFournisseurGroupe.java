/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.fin;

import bean.ClassMAPTable;
import java.sql.Date;

/**
 *
 * @author Joe
 */
public class FactureFournisseurGroupe extends ClassMAPTable{
    
    /*
    vue: jointure amin'ny table ordre de paiement (table union_op_ff no ifandraisany)
    montant: montant ny facture
    paye: somme ny montant ny OP ka ny etat = 51 (etat paye)
    reste: reste a payer (montant - paye)
    
    manambotra vue ray koa mitovy amin'io ihany ny structure fa ny table ampiasaina no miova: facture client, ordre derectte, union_or_fc
    */
    
    private Date daty;
    private String idfacture, designation;
    private int mois;
    private int annee;
    private double montant, paye, reste;

    public FactureFournisseurGroupe() {
        this.setNomTable("");
    }
    
    
    
    @Override
    public String getTuppleID() {
        return null;
    }

    @Override
    public String getAttributIDName() {
        return "";
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public String getIdfacture() {
        return idfacture;
    }

    public void setIdfacture(String idfacture) {
        this.idfacture = idfacture;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getMois() {
        return mois;
    }

    public void setMois(int mois) {
        this.mois = mois;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public double getPaye() {
        return paye;
    }

    public void setPaye(double paye) {
        this.paye = paye;
    }

    public double getReste() {
        return reste;
    }

    public void setReste(double reste) {
        this.reste = reste;
    }
    
    
}
