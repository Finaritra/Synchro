package mg.allosakafo.fin;

import bean.ClassMAPTable;
import java.sql.Connection;
import utilitaire.*;
import java.util.Vector;

/**
 * <p>
 * Title: Categorie </p>
 * <p>
 * Description: </p>
 * <p>
 * Copyright: Copyright (c) 2012</p>
 * <p>
 * Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class PlanRemboursement extends ClassMAPTable {

    private String id;
    private String iddemandeavance;
    private int mois;
    private int annee;
    private double montant;

    public PlanRemboursement() {
        super.setNomTable("planremboursement");
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public PlanRemboursement(String id,String iddemandeavance, int mois, int annee, double montant) throws Exception{
        this.setId(id);
        this.setIddemandeavance(iddemandeavance);
        this.setMois(mois);
        this.setAnnee(annee);
        this.setMontant(montant);
        this.setNomTable("PAIE_PLANREMBOURSEMENT");
    }
    public PlanRemboursement(String iddemandeavance, int mois, int annee, double montant) throws Exception{
        this.setIddemandeavance(iddemandeavance);
        this.setMois(mois);
        this.setAnnee(annee);
        this.setMontant(montant);
        this.setNomTable("PAIE_PLANREMBOURSEMENT");
    }
    
    public void construirePK(Connection c) throws Exception {
        super.setNomTable("planremboursement");
        this.preparePk("PLR", "get_Seq_planremboursement");
        this.setId(makePK(c));
    }

    public int getMois() {
        return this.mois;
    }

    public int getAnnee() {
        return this.annee;
    }

    public double getMontant() {
        return this.montant;
    }

    public String getIddemandeavance() {
        return this.iddemandeavance;
    }

    public void setMois(int m) {
        this.mois = m;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public void setMontant(double mon) throws Exception {
        if (mon <= 0) {
            throw new Exception("Montant invalide!doit �tre sup�rieur � 0");
        }
        this.montant = mon;
    }

    public void setIddemandeavance(String dem) throws Exception {
        if (dem.compareToIgnoreCase("") == 0) {
            throw new Exception("demandeAvance ne doit pas etre vide");
        }
        this.iddemandeavance = dem;
    }

    public double sommeMontantPL(PlanRemboursement[] liste) {
        double m = 0;
        for (int i = 0; i < liste.length; i++) {
            m = m + liste[i].getMontant();
        }
        return m;
    }

    
}
