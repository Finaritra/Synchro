package mg.allosakafo.fin;

import bean.CGenUtil;
import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.UtilDB;

public class Caisse extends ClassMAPTable{
    
    private String idcaisse, desccaisse, respcaisse, idetatcaisse;
    
	
    public Caisse (){
        super.setNomTable("CAISSE");
    }
	
    public String getTuppleID() {
        return idcaisse;
    }

    public String getAttributIDName() {
        return "idcaisse";
    }

    public String getIdcaisse() {
        return idcaisse;
    }

    public void setIdcaisse(String idcaisse) {
        this.idcaisse = idcaisse;
    }

    public String getDesccaisse() {
        return desccaisse;
    }

    public void setDesccaisse(String desccaisse) {
        this.desccaisse = desccaisse;
    }

    public String getRespcaisse() {
        return respcaisse;
    }

    public void setRespcaisse(String respcaisse) {
        this.respcaisse = respcaisse;
    }

    public String getIdetatcaisse() {
        return idetatcaisse;
    }

    public void setIdetatcaisse(String idetatcaisse) {
        this.idetatcaisse = idetatcaisse;
    }
    public Caisse[] getAllCaisse(Connection c) throws Exception{
        int verif=0;
        Caisse[] retour=null;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                verif=1;
            }
            retour=(Caisse[]) CGenUtil.rechercher(new Caisse(), null, null, c, "");
        }catch(Exception e){
            throw e;
        }finally{
            if(c!=null && verif==1)c.close();
        }
        return retour;
    }
}
