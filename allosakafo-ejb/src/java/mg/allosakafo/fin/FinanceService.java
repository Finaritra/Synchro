/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.fin;

import java.sql.Connection;
import utilitaire.*;
import bean.CGenUtil;
/**
 *
 * @author Joe
 */
public class FinanceService {
    
    public static OrdreDePaiement payerOp(OrdreDePaiement op, Connection connection) throws Exception{
        try{
            
        } catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return null;
    }
    /*
    public static OrdreDePaiement findDetailsOp(String idop)throws Exception{
        Connection conn = null;
        try{
            conn = (new UtilDB()).GetConn();
            OrdreDePaiement op = new OrdreDePaiement();
            op.setNomTable("union_op_ff_vue");
            OrdreDePaiement[] listop = (OrdreDePaiement[])CGenUtil.rechercher(op, null, null, conn, " AND ID = '"+idop+"'");
            if(listop != null && listop.length > 0){
                return listop[0];
            } else {
                throw new Exception("Objet introuvable");
            }
        } catch(Exception ex){
            ex.printStackTrace();
        } finally{
            if(conn != null){
                conn.close();
            }
        }
        return null;
    }

    public static void updateMontantBudgetLiquide(double montantDebit, double montantCredit, String iduser, Connection connection)throws Exception{
        try{
            if(montantDebit > 0 && montantCredit == 0){ // mise a jour budget depense
                BudgetDepense bd = new BudgetDepense();
                BudgetDepense[] lbd = (BudgetDepense[])CGenUtil.rechercher(bd, null, null, connection, " AND ANNEE = " + Utilitaire.getAnneeEnCours());
                if(lbd != null && lbd.length > 0){
                    double liquide = lbd[0].getLiquide() + montantDebit;
                    lbd[0].setLiquide(liquide);
                    lbd[0].updateToTableWithHisto(iduser, connection);
                } else {
                    throw new Exception("erreur mise a jour budget");
                }
            } else { // mise a jour budget recette
                BudgetRecette bd = new BudgetRecette();
                BudgetRecette[] lbd = (BudgetRecette[])CGenUtil.rechercher(bd, null, null, connection, " AND ANNEE = " + Utilitaire.getAnneeEnCours());
                if(lbd != null && lbd.length > 0){
                    double liquide = lbd[0].getLiquide() + montantCredit;
                    lbd[0].setLiquide(liquide);
                    lbd[0].updateToTableWithHisto(iduser, connection);
                } else {
                    throw new Exception("erreur mise a jour budget");
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public static OrdreDeRecette findDetailsOr(String idop)throws Exception{
        Connection conn = null;
        try{
            conn = (new UtilDB()).GetConn();
            OrdreDeRecette op = new OrdreDeRecette();
            op.setNomTable("union_or_fc_vue");
            OrdreDeRecette[] listop = (OrdreDeRecette[])CGenUtil.rechercher(op, null, null, conn, " AND ID = '"+idop+"'");
            if(listop != null && listop.length > 0){
                return listop[0];
            } else {
                throw new Exception("Objet introuvable");
            }
        } catch(Exception ex){
            ex.printStackTrace();
        } finally{
            if(conn != null){
                conn.close();
            }
        }
        return null;
    }
    
    public static void annulerMontantBudger(double montantDebit, double montantCredit, String idUser, Connection connection) throws Exception{
        try{
            if(montantDebit > 0 && montantCredit == 0){ // mise a jour budget depense
                BudgetDepense bd = new BudgetDepense();
                BudgetDepense[] lbd = (BudgetDepense[])CGenUtil.rechercher(bd, null, null, connection, " AND ANNEE = " + Utilitaire.getAnneeEnCours());
                if(lbd != null && lbd.length > 0){
                    double engage = lbd[0].getEngage() - montantDebit;
                    lbd[0].setEngage(engage);
                    lbd[0].updateToTableWithHisto(idUser, connection);
                } else {
                    throw new Exception("erreur mise a jour budget");
                }
            } else { // mise a jour budget recette
                BudgetRecette bd = new BudgetRecette();
                BudgetRecette[] lbd = (BudgetRecette[])CGenUtil.rechercher(bd, null, null, connection, " AND ANNEE = " + Utilitaire.getAnneeEnCours());
                if(lbd != null && lbd.length > 0){
                    double engage = lbd[0].getEngage() - montantCredit;
                    lbd[0].setEngage(engage);
                    lbd[0].updateToTableWithHisto(idUser, connection);
                } else {
                    throw new Exception("erreur mise a jour budget");
                }
            }
        } catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }*/
}
