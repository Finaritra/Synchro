package mg.allosakafo.fin;

import bean.ClassMAPTable;
import java.sql.Date;

public class EtatdeCaisseDate extends ClassMAPTable{
    
    /*
    vue: somme ny debit sy credit ao amin'ny table mouvement de caisse
    rehefa any amin'ny affichage dia mbola miampy ny montant ny dernier report amin'ny caisse tsirairay (possible date samihafa)
    */
    
    private Date daty;
    private String caisse, devise;
    private double report, debit, credit, disponible;

    public EtatdeCaisseDate() {
        this.setNomTable("etatcaissepardate");
    }
    
    public EtatdeCaisseDate(Date _date, String _caisse, String _devise, double _report, double _credit, double _debit){
		this.setNomTable("etatcaissepardate");
		this.setDaty(_date);
		this.setCaisse(_caisse);
		this.setDevise(_devise);
		this.setReport(_report);
		this.setCredit(_credit);
		this.setDebit(_debit);
		this.calculerDisponible();
	}
    
    @Override
    public String getTuppleID() {
        return null;
    }

    @Override
    public String getAttributIDName() {
        return "";
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public String getCaisse() {
        return caisse;
    }

    public void setCaisse(String caisse) {
        this.caisse = caisse;
    }


    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

	public double getReport() {
        return report;
    }

    public void setReport(double report) {
        this.report = report;
    }
	
    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) {
        this.debit = debit;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public double getDisponible() {
        return disponible;
    }

    public void setDisponible(double disponible) {
        this.disponible = disponible;
    }
	
	public void calculerDisponible (){
		this.setDisponible (this.getReport()-this.getDebit()+this.getCredit());
	}
}
