/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.fin;

import bean.CGenUtil;
import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;

/**
 *
 * @author Joe
 */
public class FactureClient extends ClassEtat{
    
    private String id, remarque, designation, idobjet;
    private double tva;
    private Date daty, dateemission, dateecheance;
    private String client;

    public Date getDateemission() {
        return dateemission;
    }

    public void setDateemission(Date dateemission) {
        this.dateemission = dateemission;
    }

    public Date getDateecheance() {
        return dateecheance;
    }

    public void setDateecheance(Date dateecheance) {
        this.dateecheance = dateecheance;
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("FCL", "getSeqFCClient");
        this.setId(makePK(c));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    

    public String getIdobjet() {
        return idobjet;
    }

    public void setIdobjet(String idobjet) {
        this.idobjet = idobjet;
    }

    

    public double getTva() {
        return tva;
    }

    public void setTva(double tva) {
        this.tva = tva;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }
    
    public FactureClient (){
        super.setNomTable("as_factureclient");
    }
    
    public String getClient(){
        return this.client;
    }
    
    public void setClient(String _client){
        this.client = _client;
    }
    
    public OrdreDePaiement[] getOr(Connection c) throws Exception
    {
        OrdreDePaiement op = new OrdreDePaiement();
        op.setDed_id(getId());
        return (OrdreDePaiement[])CGenUtil.rechercher(op, null,null, c, "");
    }
    public MvtCaisse[] getMvtCaisse(Connection c) throws Exception
    {
        MvtCaisse mvt = new MvtCaisse();
        mvt.setIdordre(this.getIdobjet());
        return (MvtCaisse[])CGenUtil.rechercher(mvt, null,null, c, ""); 
    }
    
    public void annulerFacture(String refuser,Connection c)throws Exception{
        int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                verif=1;
            }
            OrdreDePaiement[] listeOR=this.getOr(c);
            for(int i=0;i<listeOR.length;i++)
            {
                listeOR[i].annuler(refuser,c);
            }
            MvtCaisse[] lmvt=getMvtCaisse(c);
            for(int k=0;k<lmvt.length;k++)
            {
                lmvt[k].annuler(refuser,c);
            }
            this.annuler(refuser, c);
            if(verif==1)c.commit();
        }
        catch(Exception ex){
            c.rollback();
            ex.printStackTrace();
            throw ex;
        }
        finally{
            if(c!=null && verif==1)c.close();
        }
    }
}
