package mg.allosakafo.stock;

import bean.ClassMAPTable;
import java.sql.Connection;

/**
 *
 * @author BICI
 */
public class BonDeLivraisonFille extends ClassMAPTable {

    private String id, produit, numbl,iddetailsfacturefournisseur;
    private double quantite, quantiteparpack;

    public String getIddetailsfacturefournisseur() {
        return iddetailsfacturefournisseur;
    }

    public void setIddetailsfacturefournisseur(String iddetailsfacturefournisseur) {
        this.iddetailsfacturefournisseur = iddetailsfacturefournisseur;
    }

    public BonDeLivraisonFille() {
        this.setNomTable("AS_BONDELIVRAISON_FILLE");
    }
	public BonDeLivraisonFille(String produit, String numbl, double quantiteparpack, double quantite) {
        this.setNomTable("AS_BONDELIVRAISON_FILLE");
		this.setProduit(produit);
		this.setNumbl(numbl);
		this.setQuantiteparpack(quantiteparpack);
		this.setQuantite(quantite);
    }
        public BonDeLivraisonFille(String produit, String numbl, double quantiteparpack, double quantite,String iddetailsfacturefournisseur) {
        this.setNomTable("AS_BONDELIVRAISON_FILLE");
		this.setProduit(produit);
		this.setNumbl(numbl);
		this.setQuantiteparpack(quantiteparpack);
		this.setQuantite(quantite);
                this.setIddetailsfacturefournisseur(iddetailsfacturefournisseur);
    }
	
    public void construirePK(Connection c) throws Exception {
        this.preparePk("BLF", "getSeqBondeLivraisonFille");
        this.setId(makePK(c));
    }


    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

	public String getNumbl() {
        return numbl;
    }

    public void setNumbl(String numbl) {
        this.numbl = numbl;
    }
	
    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }
	
	public double getQuantiteparpack() {
        return quantiteparpack;
    }

    public void setQuantiteparpack(double quantiteparpack) {
        this.quantiteparpack = quantiteparpack;
    }
	

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public double getTotal(){
		return this.getQuantiteparpack() * this.getQuantite();
	}
}
