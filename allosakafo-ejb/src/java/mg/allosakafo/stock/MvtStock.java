package mg.allosakafo.stock;

import bean.CGenUtil;
import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author BICI
 */
public class MvtStock extends ClassEtat {

    private String id, typemvt, designation, observation, depot, typebesoin, fournisseur, beneficiaire, numbc, numbs;
    private Date daty;
    MvtStockFille[]listeFille;

    public MvtStockFille[] getListeFille() {
        return listeFille;
    }

    public void setListeFille(MvtStockFille[] listeFille) {
        this.listeFille = listeFille;
    }
    
    public boolean isSynchro()
    {
        return true;
    }

    
    public MvtStockFille[] getFille(Connection c,String nomTable)throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            if(nomTable==null||nomTable.compareToIgnoreCase("")==0) nomTable=new MvtStockFille().getNomTable();
            MvtStockFille crt=new MvtStockFille();
            crt.setNomTable(nomTable);
            crt.setIdmvt(this.getId());
            MvtStockFille[] listeF=(MvtStockFille[])CGenUtil.rechercher(crt, null,null, c, "");
            this.setListeFille(listeF);
            return listeF;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(c!=null&&estOuvert==true)c.close();
        }
    }
    public MvtStock contrer(Connection c) throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(getListeFille()==null)getFille(null,null);
            MvtStock contre = new MvtStock("0", "Annulation "+this.getId(), "", "", this.getId(), Utilitaire.dateDuJourSql(), this.getDepot());
            contre.construirePK(c);
            MvtStockFille[] ancienFille=getListeFille();
            MvtStockFille[] lf=new MvtStockFille[ancienFille.length];
            for(int i=0;i<ancienFille.length;i++)
            {
                lf[i] = new MvtStockFille(contre.getId(), ancienFille[i].getIngredients() , ancienFille[i].getSortie(), ancienFille[i].getEntree());
                lf[i].construirePK(c);
            }
            contre.setListeFille(lf);
            return contre;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(c!=null&&estOuvert==true)c.close();
        }
    }
    public void insertAvecFille(String u,Connection c) throws Exception
    {
        if(this.getId()==null||this.getId().compareToIgnoreCase("")==0)this.makePK(c);
        this.insertToTableWithHisto(u, c);
        if(this.getListeFille()==null)return;
        MvtStockFille[]lf=this.getListeFille();
        for(int i=0;i<lf.length;i++)
        {
            lf[i].insertToTableWithHisto(u, c);
        }
    }
    public MvtStock() {
        this.setNomTable("AS_MVT_STOCK");
    }
	
    public MvtStock(String typemvt, String designation, String observation, String fournisseur, String numbc, Date daty) {
        this.setNomTable("AS_MVT_STOCK");
		this.setTypemvt(typemvt);
		this.setDesignation(designation);
		this.setObservation(observation);
		this.setFournisseur(fournisseur);
		this.setNumbc(numbc);
		this.setDaty(daty);
    }
    public MvtStock(String typemvt, String designation, String observation, String fournisseur, String numbc, Date daty,String depot) {
        this.setNomTable("AS_MVT_STOCK");
        this.setTypemvt(typemvt);
        this.setDesignation(designation);
        this.setObservation(observation);
        this.setFournisseur(fournisseur);
        this.setNumbc(numbc);
        this.setDaty(daty);
        this.setDepot(depot);
    }
      public MvtStock(String designation, String observation,String numbc, Date daty,String depot) {
        this.setNomTable("AS_MVT_STOCK");
        this.setDesignation(designation);
        this.setObservation(observation);
        this.setNumbc(numbc);
        this.setDaty(daty);
        this.setDepot(depot);
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("MVTS", "getSeqMvtStock");
        this.setId(makePK(c));
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypemvt() {
        return typemvt;
    }

    public void setTypemvt(String typemvt) {
        this.typemvt = typemvt;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getDepot() {
        return depot;
    }

    public void setDepot(String depot) {
        this.depot = depot;
    }

    public String getTypebesoin() {
        return typebesoin;
    }

    public void setTypebesoin(String typebesoin) {
        this.typebesoin = typebesoin;
    }

    public String getBeneficiaire() {
        return beneficiaire;
    }

    public void setBeneficiaire(String beneficiaire) {
        this.beneficiaire = beneficiaire;
    }

    public String getNumbc() {
        return numbc;
    }

    public void setNumbc(String numbc) {
        this.numbc = numbc;
    }

    public String getNumbs() {
        return numbs;
    }

    public void setNumbs(String numbs) {
        this.numbs = numbs;
    }

}
