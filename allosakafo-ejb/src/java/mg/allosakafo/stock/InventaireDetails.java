/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.UtilDB;

/**
 *
 * @author Axel
 */
public class InventaireDetails extends ClassEtat  {

    private String id , idmere , idingredient , explication ,idcategorieingredient,idIng,unite;

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public String getIdIng() {
        return idIng;
    }

    public void setIdIng(String idIngr) {
        this.idIng = idIngr;
    }
    private double quantiter,qtetheorique ;
    private Date daty;

    public double getQtetheorique() {
        return qtetheorique;
    }
    
    public double getQuantiterEcart() {
        return qtetheorique-quantiter;
    }

    public void setQtetheorique(double qtetheorique) {
        this.qtetheorique = qtetheorique;
    }
    
      public void setQtetheorique(String quantiter) throws Exception {
          try{
               setQtetheorique(utilitaire.Utilitaire.stringToDouble(quantiter));
          }catch(Exception ex){
                throw new Exception("quantite theorique invalide");
          }
    }
    
    public InventaireDetails(){
        setNomTable("inventairedetails");
    }

    public InventaireDetails(String idmere, String idingredient, String explication, double quantiter, String idCategorie, Date daty) {
        setNomTable("inventairedetails");
        this.setIdmere(idmere);
        this.setIdingredient(idingredient);
        this.setExplication(explication);
        this.setQuantiter(quantiter);
        this.setIdcategorieingredient(idCategorie);
        this.setDaty(daty);
    }
    public InventaireDetails(String idmere, String idingredient, String explication, String quantiter, String idCategorie, Date daty) throws Exception{
        setNomTable("inventairedetails");
        this.setIdmere(idmere);
        this.setIdingredient(idingredient);
        this.setExplication(explication);
        this.setQuantiter(quantiter);
        this.setIdcategorieingredient(idCategorie);
        this.setDaty(daty);
    }
    
    public InventaireDetails(String idmere, String idingredient, String explication, String quantiter,String qtetheorique, String idCategorie, Date daty) throws Exception{
        setNomTable("inventairedetails");
        this.setIdmere(idmere);
        this.setIdingredient(idingredient);
        this.setExplication(explication);
        this.setQuantiter(quantiter);
        this.setQtetheorique(qtetheorique);
        this.setIdcategorieingredient(idCategorie);
        this.setDaty(daty);
    }
    
    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
         return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdmere() {
        return idmere;
    }

    public void setIdmere(String idmere) {
        this.idmere = idmere;
    }

    public String getIdingredient() {
        return idingredient;
    }

    public void setIdingredient(String idingredient) {
        this.idingredient = idingredient;
    }

    public String getExplication() {
        return explication;
    }

    public void setExplication(String explication) {
        this.explication = explication;
    }

    public double getQuantiter() {
        return quantiter;
    }

    public void setQuantiter(double quantiter) {
        this.quantiter = quantiter;
    }
    public void setQuantiter(String quantiter) throws Exception {
        try{
               setQuantiter(utilitaire.Utilitaire.stringToDouble(quantiter));
          }catch(Exception ex){
                throw new Exception("quantite  invalide");
          }
    }
    
     public void construirePK(Connection c) throws Exception {
        this.preparePk("IVD", "GETinventairedetails");
        this.setId(makePK(c));
    }

    public String getIdcategorieingredient() {
        return idcategorieingredient;
    }

    public void setIdcategorieingredient(String idcategorieingredient) {
        this.idcategorieingredient = idcategorieingredient;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }
   
}
