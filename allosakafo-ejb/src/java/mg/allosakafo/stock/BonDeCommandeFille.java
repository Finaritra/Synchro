package mg.allosakafo.stock;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author BICI
 */
public class BonDeCommandeFille extends ClassMAPTable {

    private String id, produit,idbc , remarque;
    private double quantiteparpack,quantite;
    private double pu;

    public BonDeCommandeFille() {
        this.setNomTable("AS_BONDECOMMANDE_FILLE");
    }

	public BonDeCommandeFille(String produit, String idbc, String remarque, double prixu, double qtepack, double qte){
		this.setNomTable("AS_BONDECOMMANDE_FILLE");
		this.setProduit(produit);
		this.setIdbc(idbc);
		this.setRemarque(remarque);
		this.setPu (prixu);
		this.setQuantiteparpack(qtepack);
		this.setQuantite(qte);
	}
	
    public void construirePK(Connection c) throws Exception {
        this.preparePk("BCF", "getSeqBondeCommandeFille");
        this.setId(makePK(c));
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public String getIdbc() {
        return idbc;
    }

    public void setIdbc(String idbc) {
        this.idbc = idbc;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    public double getQuantiteparpack() {
        return quantiteparpack;
    }

    public void setQuantiteparpack(double quantiteparpack) {
        this.quantiteparpack = quantiteparpack;
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
