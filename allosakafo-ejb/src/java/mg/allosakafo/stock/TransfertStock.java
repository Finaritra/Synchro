/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import java.sql.Date;

/**
 *
 * @author Maharo R.
 */
public class TransfertStock extends MvtStock{
    
    private String depotfinal;

    public TransfertStock() {
         this.setNomTable("transfertstock");
    }

    public TransfertStock(String typemvt, String designation, String observation, String fournisseur, String numbc, Date daty, String depot,String depotfinal) {
        
        super(typemvt, designation, observation, fournisseur, numbc, daty, depot);
         this.setNomTable("transfertstock");
        this.setDepotfinal(depotfinal);
    }

  
    
    

    public String getDepotfinal() {
        return depotfinal;
    }

    public void setDepotfinal(String depotfinal) {
        this.depotfinal = depotfinal;
    }
    
    
    
}
