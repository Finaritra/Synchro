/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

/**
 *
 * @author rajao
 */
public class BonDeLivraisonDateFournisseur extends BonDeLivraisonDate{
    public String idfournisseur,nomfournisseur;
    public BonDeLivraisonDateFournisseur(){
        super();
        this.setNomTable("as_bondelivrasion_fille_frs");
    }

    public String getIdfournisseur() {
        return idfournisseur;
    }

    public void setIdfournisseur(String idfournisseur) {
        this.idfournisseur = idfournisseur;
    }

    public String getNomfournisseur() {
        return nomfournisseur;
    }

    public void setNomfournisseur(String nomfournisseur) {
        this.nomfournisseur = nomfournisseur;
    }
    
    
}
