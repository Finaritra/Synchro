/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.Utilitaire;
import utilitaire.ConstanteEtat;
/**
 *
 * @author BICI
 */
public class BonDeLivraisonEtat extends ClassMAPTable {

    private String id, remarque, idbc;
    private Date daty;
	private int etat;
	
    public BonDeLivraisonEtat() {
        this.setNomTable("AS_BONDELIVRAISON");
    }


    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdbc() {
        return idbc;
    }

    public void setIdbc(String idbc) {
        this.idbc = idbc;
    }

	public void setEtat(int etat){
		this.etat = etat;
	}
	
	public String getEtat(){
		return ConstanteEtat.etatToChaine(String.valueOf(etat));
	}	
}
