/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import bean.ClassMAPTable;

/**
 *
 * @author John
 */
public class ReporterStock extends ClassMAPTable {

    private String id, idmagasin, depot, type, observation;
    private double theorique;
    private double inventaire;
    
    public ReporterStock() {
        super.setNomTable("vue_etat_de_stock_report");
    }
    
    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "ID";
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the idmagasin
     */
    public String getIdmagasin() {
        return idmagasin;
    }

    /**
     * @param idmagasin the idmagasin to set
     */
    public void setIdmagasin(String idmagasin) {
        this.idmagasin = idmagasin;
    }

    /**
     * @return the depot
     */
    public String getDepot() {
        return depot;
    }

    /**
     * @param depot the depot to set
     */
    public void setDepot(String depot) {
        this.depot = depot;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the observation
     */
    public String getObservation() {
        return observation;
    }

    /**
     * @param observation the observation to set
     */
    public void setObservation(String observation) {
        this.observation = observation;
    }

    /**
     * @return the theorique
     */
    public double getTheorique() {
        return theorique;
    }

    /**
     * @param theorique the theorique to set
     */
    public void setTheorique(double theorique) {
        this.theorique = theorique;
    }

    /**
     * @return the inventaire
     */
    public double getInventaire() {
        return inventaire;
    }

    /**
     * @param inventaire the inventaire to set
     */
    public void setInventaire(double inventaire) {
        this.inventaire = inventaire;
    }
    
}
