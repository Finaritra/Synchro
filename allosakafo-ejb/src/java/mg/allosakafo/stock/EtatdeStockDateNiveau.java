/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import bean.CGenUtil;
import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mg.allosakafo.facturefournisseur.DetailsFactureFournisseur;
import mg.allosakafo.facturefournisseur.FactureFournisseur;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author ACER
 */
public class EtatdeStockDateNiveau extends ClassMAPTable{
        
    /*
    vue: somme ny debit sy credit ao amin'ny table mouvement de caisse
    rehefa any amin'ny affichage dia mbola miampy ny montant ny dernier report amin'ny caisse tsirairay (possible date samihafa)
    */
    
    private Date daty;
    private String ingredients, unite,point,idCategorieIngredient,idfournisseur,idingredient,fournisseur,niveau;
    private double pu,montant,montantbesoin,montantStock;
    private double report, entree, sortie, reste,besoin,aacheter,reportSuiv;

    public double getReportSuiv() {
        return reportSuiv;
    }

    public void setReportSuiv(double reportSuiv) {
        this.reportSuiv = reportSuiv;
    }

    public double getMontantStock() {
        return montantStock;
    }

    public void setMontantStock(double montantStock) {
        this.montantStock = montantStock;
    }

    public String getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }

    public String getIdingredient() {
        return idingredient;
    }

    public void setIdingredient(String idingredient) {
        this.idingredient = idingredient;
    }

    public String getIdfournisseur() {
        return idfournisseur;
    }

    public void setIdfournisseur(String idfournisseur) {
        this.idfournisseur = idfournisseur;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    public double getMontant() {
        return (this.getPu() * this.getAacheter());
    }
    
     public double getMontantbesoin() {
        return (this.getPu() * this.getBesoin());
    }

    

    public void setMontant(double montant) {
        this.montant = montant;
    }

    
    
    public String getIdCategorieIngredient() {
        return idCategorieIngredient;
    }

    public void setIdCategorieIngredient(String idCategorieIngredient) {
        this.idCategorieIngredient = idCategorieIngredient;
    }
    

    public double getBesoin() {
        return besoin;
    }

    public void setBesoin(double besoin) {
        this.besoin = besoin;
    }

    public EtatdeStockDateNiveau() {
        this.setNomTable("as_etatstock1");
    }
    
    public EtatdeStockDateNiveau(Date _date, String _ingredients, String _unite, double _report, double _entree, double _sortie){
		this.setNomTable("as_etatstock1");
		this.setDaty(_date);
		this.setIngredients(_ingredients);
		this.setUnite(_unite);
		this.setEntree(_entree);
		this.setReport(_report);
		this.setSortie(_sortie);
		//this.calculerDisponible();
    }
    
    @Override
    public String getTuppleID() {
        return null;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }


    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

	public double getReport() {
        return report;
    }

    public void setReport(double report) {
        this.report = report;
    }
	
    public double getEntree() {
        return entree;
    }

    public void setEntree(double entree) {
        this.entree = entree;
    }

    public double getSortie() {
        return sortie;
    }

    public void setSortie(double sortie) {
        this.sortie = sortie;
    }

    public double getReste() {
        return this.getReport()-this.getSortie()+this.getEntree();
    }

    public void setReste(double reste) {
        this.reste = reste;
    }
    
    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }
	
    public double calculerDisponible (){

        if(this.getReport()==this.getEntree())
        {
            return (-this.getSortie()+this.getEntree());
        }
        else
        {
            double ret=this.getReport()-this.getSortie()+this.getEntree();
            return ret;
        }
    }
    public double getAacheter(){
        double achat = this.getBesoin()-Math.max(0,this.getReste());
        if(achat<0)achat=0;
        return achat;
    }
    /**
     * @return the point
     */
    public String getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(String point) {
        this.point = point;
    }
    
    public static EtatdeStockDateNiveau[] caculEtatStock(String datyInf,String datySup, String ingr,String depot,String categorie,String idIngredient, Connection c) throws Exception {
        EtatdeStockDateNiveau[] ret = null; 
        boolean newCon = false;
        try {
            if(c==null) {
                c = new UtilDB().GetConn();
                newCon=true;
            }
            // Find last inventaire a partir daty
//            InventaireMere im = new InventaireMere();
//            im.setIdcategorieingredient(idcategorie);
//            im.setNomTable("inventairemere");
//            InventaireMere[] inventaires = (InventaireMere[]) CGenUtil.rechercher(im, null, null, c, " and daty <= to_date('" + daty + "' , 'YYYY-MM-DD') and id in (select idmere from inventairedetails) order by daty desc");
//            if(inventaires.length<0) {
//                throw new Exception("Inventaire inexistant");
//            }
            if(datyInf==null||datyInf.compareToIgnoreCase("")==0)datyInf=datySup;
            if(depot==null||depot.compareToIgnoreCase("")==0)depot="defaut";
            InventaireDetails det = new InventaireDetails();
            det.setNomTable("INVENTAIREDETAILSLIBINVENT");
            String filtreIngr="";
            if(ingr!=null&&ingr.compareToIgnoreCase("")!=0) filtreIngr=" and idingredient like '%"+ingr+"%'";
            if(idIngredient!=null&&idIngredient.compareToIgnoreCase("")!=0) filtreIngr=" and iding like '%"+idIngredient+"%'";
            if(categorie!=null&&categorie.compareToIgnoreCase("")!=0) filtreIngr+=" and upper(idcategorieIngredient) like upper('%"+categorie+"%')";
            
            String req="select * from "+det.getNomTable()+" inv where inv.daty=(select max(inv2.daty) from "+det.getNomTable()+" inv2 where inv2.IDING=inv.idIng)";
            String aWhere=" and inv.daty <= '"+datyInf+"' and inv.explication='"+depot+"' and inv.etat="+ ConstanteEtat.getEtatValider() +"  order by inv.daty desc";
            InventaireDetails[] details = (InventaireDetails[]) CGenUtil.rechercher(det, req+filtreIngr+aWhere, c );
            String[] idingredients = new String[details.length];
            for(int i=0; i<details.length; i++) {
                idingredients[i] = details[i].getIdIng();
            }
            
            
            String iding = "";
            
            if(idingredients.length!=0) {
                iding=Utilitaire.tabToString(idingredients, "'", ",");
            }
            
            iding = iding.isEmpty() ? "''" : iding;
            
            // Find mouvement stock a partir daty
            MvtStockDetail md = new MvtStockDetail();
            md.setNomTable("AS_MVTSTOCK_FDTLIBUNITE");
            String apresWhere=" and daty > '"+datyInf+"' and ingredients in ("+iding+")";
            if(details.length>0)
                apresWhere="and point='"+depot+"' and daty > '"+Utilitaire.datetostring(details[0].getDaty())+"' and daty<='"+datySup+"' and idingredients in ("+iding+")";
            //MvtStockDetail[] mvtdetails = (MvtStockDetail[]) CGenUtil.rechercher(md, null, null, c, apresWhere);
            String requete="select '-',ingredients,sum(entree) as entree,sum(sortie) as sortie,max(daty) as daty,unite from " +md.getNomTable()+" where 1<2 ";
            String group=" group by ingredients,unite";
            /*
            select nvl(inv.daty,'') as daty,mvtdet.ingredients, mvdet.unite,mvt.entree,mvt.sortie,(select nvl(inv.quantiter,0) from inventairedetails inv 
            where inv.idingredients=mvdet.idingredients and inv.daty=(select max(inv2.daty) from inventairedetails inv2 where inv2.idingredients=inv.idingredients
            and inv2.daty<='06/04/2020')) as report,0 as reste,mvt.point as point from AS_MVTSTOCK_FDTLIBUNITE mvt
            */
            requete=requete+apresWhere+group;
            
            System.out.println("requ***************"+requete);
            MvtStockDetail[] mvtdetails=(MvtStockDetail[])(CGenUtil.rechercher(md, requete, c));
            // Calcul mouvement stock
            Map mvt = new HashMap();
            for(InventaireDetails inventaire: details) {
                EtatdeStockDateNiveau stock = new EtatdeStockDateNiveau(inventaire.getDaty() , inventaire.getIdingredient(), inventaire.getUnite(), inventaire.getQuantiter(), 0, 0);   
                stock.setIdCategorieIngredient(inventaire.getIdcategorieingredient());
                mvt.put(inventaire.getIdingredient(), stock);
            }
            
            for(MvtStockDetail mvtdetail: mvtdetails) {
                EtatdeStockDateNiveau stock = (EtatdeStockDateNiveau) mvt.get(mvtdetail.getIngredients());
                stock.setSortie(mvtdetail.getSortie());
                stock.setEntree(mvtdetail.getEntree());
                mvt.replace(mvtdetail.getIngredients(), stock);
            }
            
            ret = new EtatdeStockDateNiveau[mvt.size()];
//            ret = (EtatdeStockDateNiveau[]) mvt.entrySet().toArray();
            int i = 0;
            for(Object e : mvt.keySet()) {
                //System.out.println("e = " + e + " " + mvt.get(e));
                ret[i] = (EtatdeStockDateNiveau) mvt.get(e);
//                System.out.println("ret[i] = " + ret[i].getIngredients() + " " + ret[i].getReport());
                i++;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(newCon && c!=null)
            {
                c.close();
            }
        }
        return ret;
    }
    
     public static EtatdeStockDateNiveau[] caculEtatStock(String datySup, String ingr,String depot,String categorie,String idIngredient, Connection c , String colonne , String valorder,String datyInf,String niveau) throws Exception {
          if(depot==null||depot.compareToIgnoreCase("")==0)depot="defaut";
          if(niveau==null||depot.compareToIgnoreCase("")==0)niveau="1";
             String req = "select inv.idcategorieIngredient,inv.idcatIng,inv.iding as idingredient,inv.daty,inv.unite, nvl(mvt.entree,0) as entree,nvl(mvt.sortie,0) as sortie, (inv.QUANTITER-nvl(mvt.sortie,0)+nvl(mvt.entree,0)) as reste,inv.idingredient as ingredients,inv.quantiter as report,(inv.QUANTITER-nvl(mvt.sortie,0)+nvl(mvt.entree,0))*facmax.pu as montantStock "
                     +",nvl((select inv3.quantiter from INVENTAIREDETAILSLIBINVENT2 inv3 where INV3.DATY='"+datySup+"' and INV3.ETAT>=11 and INV3.IDING=inv.iding and INV3.EXPLICATION like '"+depot+"'),0) as reportSuiv"
                     + " from INVENTAIREDETAILSLIBINVENT2 inv,as_ingredients_ff_lib facmax,(select sum(mvt.entree) as entree,sum(mvt.sortie) as sortie,mvt.idingredients,mvt.point,mvt.unite, mvt.ingredients"
                     + " from AS_MVTSTOCK_FDTLIBUNITE mvt where mvt.point like '"+depot+"' and "
                     + "mvt.daty>(select max(inv.daty) from INVENTAIREDETAILSLIBINVENT2 inv where inv.iding=mvt.idingredients and inv.daty<'"+datyInf+"' and inv.EXPLICATION like '"+depot+"') and mvt.daty<='"+datySup+"'"
                     + " group by mvt.idingredients,mvt.point,mvt.unite, mvt.ingredients) mvt "
                     + "  where inv.daty=(select max(inv2.daty) "
                     + " from  INVENTAIREDETAILSLIBINVENT2 inv2 where inv.niveau = '"+niveau+"' and inv2.IDING=inv.idIng and inv2.daty<'"+datyInf+"' and inv2.EXPLICATION like '"+depot+"')"
                     + " and inv.IDING=mvt.IDINGREDIENTS(+) and facmax.id=inv.iding and inv.EXPLICATION=mvt.point(+) and inv.EXPLICATION like '"+depot+"'";
             return caculEtatStock(ingr,categorie,idIngredient, c,req,colonne,valorder);
         }
    
   
    
     public static EtatdeStockDateNiveau[] caculEtatStockCompose(String datySup, String ingr,String depot,String categorie,String idIngredient, Connection c, String colonne , String valorder,String datyInf) throws Exception {
          if(depot==null||depot.compareToIgnoreCase("")==0)depot="defaut";
             String req = "select inv.idcategorieIngredient, inv.daty,inv.unite, nvl(mvt.entree,0) as entree,nvl(mvt.sortie,0) as sortie, (inv.QUANTITER-nvl(mvt.sortie,0)+nvl(mvt.entree,0)) as reste,inv.idingredient as ingredients,inv.quantiter as report "
                     +",nvl((select inv3.quantiter from INVENTAIREDETAILSLIBINVENTCPS inv3 where INV3.DATY='"+datySup+"' and INV3.ETAT>=11 and INV3.IDING=inv.iding and INV3.EXPLICATION like '"+depot+"'),0) as reportSuiv"
                     + " from INVENTAIREDETAILSLIBINVENTCPS inv,(select sum(mvt.entree) as entree,sum(mvt.sortie) as sortie,mvt.idingredients,mvt.point,mvt.unite, mvt.ingredients"
                     + " from AS_MVTSTOCK_FDTLIBUNITECPS mvt where mvt.point like '"+depot+"' and "
                     + "mvt.daty>(select max(inv.daty) from INVENTAIREDETAILSLIBINVENTCPS inv where inv.iding=mvt.idingredients and inv.daty<'"+datyInf+"' and inv.EXPLICATION like '"+depot+"') and mvt.daty<='"+datySup+"'"
                     + " group by mvt.idingredients,mvt.point,mvt.unite, mvt.ingredients) mvt "
                     + "  where inv.daty=(select max(inv2.daty) "
                     + " from  INVENTAIREDETAILSLIBINVENTCPS inv2 where inv2.IDING=inv.idIng and inv2.daty<'"+datyInf+"' and inv2.EXPLICATION like '"+depot+"')"
                     + " and inv.IDING=mvt.IDINGREDIENTS(+) and inv.EXPLICATION=mvt.point(+) and inv.EXPLICATION like '"+depot+"'";
             return caculEtatStock(ingr,categorie,idIngredient, c,req,colonne,valorder);
         }
     
     
   
    
    public static EtatdeStockDateNiveau[] caculEtatStockBesoin(String datySup, String ingr,String depot,String categorie,String idIngredient, Connection c, String colonne , String valorder) throws Exception {
          if(depot==null||depot.compareToIgnoreCase("")==0)depot="defaut";
           
             String req = "select inv.iding as idingredient,inv.daty,inv.unite, nvl(mvt.entree,0) as entree,nvl(mvt.sortie,0) as sortie, (inv.QUANTITER-nvl(mvt.sortie,0)+nvl(mvt.entree,0)) as reste,inv.idingredient||' ('||inv.unite||')' as ingredients,inv.quantiter as report,nvl(b.besoin,0) as besoin,facmax.idfournisseur as idfournisseur,facmax.pu as pu ,facmax.nom as fournisseur "
                     + " from INVENTAIREDETAILSLIBINVENT inv, besoinrecette b,as_ingredients_ff_lib facmax,(select sum(mvt.entree) as entree,sum(mvt.sortie) as sortie,mvt.idingredients,mvt.point,mvt.unite, mvt.ingredients"
                     + " from AS_MVTSTOCK_FDTLIBUNITE mvt where "
                     + "mvt.daty>(select max(inv.daty) from INVENTAIREDETAILSLIBINVENT inv where inv.iding=mvt.idingredients and inv.daty<='"+datySup+"') and mvt.daty<='"+datySup+"'"
                     + " group by mvt.idingredients,mvt.point,mvt.unite, mvt.ingredients) mvt "
                     +   " where  b.idingredients = inv.iding(+) and facmax.id=inv.iding "
                     + "  and inv.daty=(select max(inv2.daty) "
                     + " from  INVENTAIREDETAILSLIBINVENT inv2 where inv2.IDING=inv.idIng and inv2.daty<='"+datySup+"')"
                     + " and inv.IDING=mvt.IDINGREDIENTS(+) and inv.EXPLICATION=mvt.point(+) and inv.EXPLICATION like '"+depot+"'";
               
               //System.out.println("reqqq: "+req);
             return caculEtatStock(ingr,categorie,idIngredient, c,req,colonne,valorder);
         }
        public static EtatdeStockDateNiveau[] caculEtatStockBesoinCompose(String datySup, String ingr,String depot,String categorie,String idIngredient, Connection c, String colonne , String valorder) throws Exception {
          if(depot==null||depot.compareToIgnoreCase("")==0)depot="defaut";           
          
          String req = "select inv.iding as idingredient,inv.daty,inv.unite, nvl(mvt.entree,0) as entree,nvl(mvt.sortie,0) as sortie, (inv.QUANTITER-nvl(mvt.sortie,0)+nvl(mvt.entree,0)) as reste,inv.idingredient||' ('||inv.unite||')' as ingredients,inv.quantiter as report,nvl(b.besoin,0) as besoin "
                     +",nvl((select inv3.quantiter from INVENTAIREDETAILSLIBINVENTCPS inv3 where INV3.DATY='"+datySup+"' and INV3.ETAT>=11 and INV3.IDING=inv.iding ),0) as reportSuiv"
                     + " from INVENTAIREDETAILSLIBINVENTCPS inv, besoinetrecette b,(select sum(mvt.entree) as entree,sum(mvt.sortie) as sortie,mvt.idingredients,mvt.point,mvt.unite, mvt.ingredients"
                     + " from AS_MVTSTOCK_FDTLIBUNITECPS mvt where "
                     + "mvt.daty>(select max(inv.daty) from INVENTAIREDETAILSLIBINVENTCPS inv where inv.iding=mvt.idingredients and inv.daty<'"+datySup+"') and mvt.daty<='"+datySup+"'"
                     + " group by mvt.idingredients,mvt.point,mvt.unite, mvt.ingredients) mvt "
                     +   " where  b.idingredients = inv.iding(+)"
                     + "  and inv.daty=(select max(inv2.daty) "
                     + " from  INVENTAIREDETAILSLIBINVENTCPS inv2 where inv2.IDING=inv.idIng and inv2.daty<'"+datySup+"')"
                     + " and inv.IDING=mvt.IDINGREDIENTS(+) and inv.EXPLICATION=mvt.point(+) and inv.EXPLICATION like '"+depot+"'";

               
               
             return caculEtatStock(ingr,categorie,idIngredient, c,req,colonne,valorder);
         }

    
    
      public static EtatdeStockDateNiveau[] caculEtatStock(String ingr,String categorie,String idIngredient, Connection c,String req, String colonne , String valorder) throws Exception {
         EtatdeStockDateNiveau[] ret = null; 
           boolean newCon = false;
           try{
             if(c==null) {
                c = new UtilDB().GetConn();
                newCon=true;
            }
             if(ingr!=null&&ingr.compareToIgnoreCase("")!=0) req+=" and upper(inv.idingredient) like upper('%"+ingr+"%')";
             if(categorie!=null&&categorie.compareToIgnoreCase("")!=0) req+=" and (upper(inv.idcategorieIngredient) like upper('%"+categorie+"%') or inv.idcatIng='"+categorie+"')";
             if(idIngredient!=null&&idIngredient.compareToIgnoreCase("")!=0) req+=" and inv.IDING like '%"+idIngredient+"%'";
             if(req.contains("besoinrecette")==true)req+=" order by facmax.nom asc";
             if(colonne!=null && valorder!=null && valorder.compareToIgnoreCase("-")!=0){
                   req="Select * from ("+req+") order by "+colonne+" "+valorder;
               }
             EtatdeStockDateNiveau stock = new EtatdeStockDateNiveau();
             stock.setNomTable("AS_ETATSTOCKVIDENIVEAU");
             System.out.println("Req => "+req);
            ret = (EtatdeStockDateNiveau[]) CGenUtil.rechercher(stock, req, c);
       }
      catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(newCon && c!=null)
            {
                c.close();
            }
        }
        return ret;
       }
      
      public static void insertAchat(String[] id,String[]idfournisseur,String[]pu,String[] qte,String user,Connection c)throws Exception{
          int verif=0;
          try{
              if(c==null){
                  c=new UtilDB().GetConn();
                  verif=1;
              }
              if(id==null || id.length==0)throw new Exception("aucun element selectionn&eacute;");
              String []listeIndice=new String[id.length];
                for(int j=0;j<id.length;j++){
                    String[] id_indice = Utilitaire.split(id[j], "-");
                    id[j] = id_indice[0];
                    listeIndice[j]=id_indice[1];
                 }
              FactureFournisseur ff=new FactureFournisseur(); 
              
              for(int i=0;i<id.length;i++){
                  int indRemarque=Integer.parseInt(listeIndice[i]);
                  double quantite=Utilitaire.stringToDouble(qte[indRemarque]);
                  double prix=Utilitaire.stringToDouble(pu[indRemarque]);
                  if(quantite>0){
                    if(ff.getIdFournisseur()==null || ff.getIdFournisseur().compareToIgnoreCase(idfournisseur[indRemarque])!=0){
                        
                         ff=new FactureFournisseur("proposition achat",Utilitaire.dateDuJourSql(),Utilitaire.dateDuJourSql(),idfournisseur[indRemarque],"","Ar"); 
                         ff.construirePK(c);
                         ff.insertToTableWithHisto(user, c);

                    }
                      DetailsFactureFournisseur dts=new DetailsFactureFournisseur(id[i],ff.getId(),quantite,prix);
                      ff.setMontantTTC(ff.getMontantTTC()+(quantite*prix));
                      dts.construirePK(c);
                      dts.insertToTableWithHisto(user, c);
                      if(ff.getMontantTTC()>0){
                          ff.updateToTableWithHisto(user, c);
                      }
                  }
                    
              }
              if(verif==1)c.commit();
          }
          catch(Exception ex){
              if(c!=null)c.rollback();
              ex.printStackTrace();
              throw ex;
          }
          finally{
              if(verif==1 && c!=null)c.close();
          }
      }
      
      
    public String getJsonAchatBesoin(EtatdeStockDateNiveau[] listeachat,double montantInf){
        String json = "[ [ \"Idingredient\",\"Ingredient\",\"Reste\",\"Besoin\",\"AFabriquer\" ]";
        for(int i = 0,l =  listeachat.length; i < l ; i++) {
            if(listeachat[i].getAacheter()>=montantInf){
                json +=",";
                json +="[\"" +listeachat[i].getIdingredient() + "\",\"" ;
                json += listeachat[i].getIngredients() + "\",\"" ;
                json += listeachat[i].getReste() + "\",\"" ;
                json += listeachat[i].getBesoin() + "\",\"" ;
                json += listeachat[i].getAacheter() +"\" ] "  ;
            }
        }
        json+="]";
        return json;
    }
    
    public String getJsonAchat(EtatdeStockDateNiveau[] listeachat,double montantInf){
        String json = "[ [\"Idingredient\",\"Ingredient\",\"Reste\",\"Besoin\",\"Aacheter\",\"Pu\",\"MontantBesoin\",\"Montant\",\"Fournisseur\"]";
        for(int i = 0,l =  listeachat.length; i < l ; i++) {
            if(listeachat[i].getAacheter()>=montantInf){
                json +=",";
                json +="[\"" +listeachat[i].getIdingredient() + "\",\"" ;
                json += listeachat[i].getIngredients() + "\",\"" ;
                json += listeachat[i].getReste() + "\",\"" ;
                json += listeachat[i].getBesoin() + "\",\"" ;
                json += listeachat[i].getAacheter() +"\",\""  ;
                json += listeachat[i].getPu() + "\",\"" ;
                json += listeachat[i].getMontantbesoin()+ "\",\"" ;
                json += listeachat[i].getFournisseur() + "\",\"" ;
                json += listeachat[i].getMontant()+  "\"]"  ;                  
            }
        }
        json+="]";
        return json;
    }
    
    public EtatdeStockDateNiveau[] getMontantInf( EtatdeStockDateNiveau[] listeachat,double montantInf ) throws Exception{
        EtatdeStockDateNiveau[] result = null ;
        List<EtatdeStockDateNiveau> ls = new ArrayList<EtatdeStockDateNiveau>()  ;
        for(int i = 0,l =  listeachat.length; i < l ; i++) {
            if(listeachat[i].getAacheter()>=montantInf){
                ls.add( listeachat[i] );
            }
        }
        result = ls.toArray(new EtatdeStockDateNiveau[ ls.size() ]);
        return result;
    }
    
}
