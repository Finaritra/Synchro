/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import bean.CGenUtil;
import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import mg.allosakafo.facturefournisseur.Detailsffbonlivraison;
import service.AlloSakafoService;
import user.UserEJB;
import utilitaire.UtilDB;

/**
 *
 * @author BICI
 */
public class BonDeLivraison extends ClassEtat {

    private String id, remarque, idbc,point;

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }
    private Date daty;

    public BonDeLivraison() {
        this.setNomTable("AS_BONDELIVRAISON");
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("BL", "getSeqBondeLivraison");
        this.setId(makePK(c));
    }

    public BonDeLivraison(String remarque, String idbc, Date daty) {
        this.setNomTable("AS_BONDELIVRAISON");
        this.setRemarque(remarque);
        this.setIdbc(idbc);
        this.setDaty(daty);
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdbc() {
        return idbc;
    }

    public void setIdbc(String idbc) {
        this.idbc = idbc;
    }
   public void controlerVisa(Connection c)throws Exception{
       int verif=0;
        String res = "";
       try{
          if(c==null){
              c=new UtilDB().GetConn();
              verif=1;
            } 
         Detailsffbonlivraison dts= new Detailsffbonlivraison();
         dts.setNomTable("detailsffbonlivraisonid");
         Detailsffbonlivraison[] retour=(Detailsffbonlivraison[]) CGenUtil.rechercher(dts, null, null, c, " and idblmere='"+this.getId()+"'");
          if(retour==null || retour.length==0)throw new Exception("bon de livraison fille vide");
          //maka ny reste  : if qte -(sum(qte blfille valider) - qte ao am detailsfacturefournisseur ) raha negative dia mamoka exception
          for(int i=0;i<retour.length;i++){
              if(retour[i].getRestelivre()<0)throw new Exception("l ingredient "+retour[i].getIdingredient()+" n a plus de reste � livrer");
          }
          //maka ny qte ao sisa :
       }
       catch(Exception ex){
           ex.printStackTrace();
           throw ex;
       }
       finally{
       if(c!=null && verif==1)c.close();
        }
   }
   public BonDeLivraisonFille[] getFille(Connection c)throws Exception{
       int verif=0;
       try{
          if(c==null){
              c=new UtilDB().GetConn();
              verif=1;
            } 
          return (BonDeLivraisonFille[]) CGenUtil.rechercher(new BonDeLivraisonFille(), null, null, c, " and numbl='"+this.getId()+"'");
        }
       catch(Exception ex){
          ex.printStackTrace();
           throw ex;
       }
       finally{
       if(c!=null && verif==1)c.close();
        }
   }
   
   public void saveMvtStock(UserEJB u,Connection c)throws Exception{
       int verif=0;
       try{
           if(c==null){
               c= new UtilDB().GetConn();
                verif=1;
           }
            MvtStock mvt = new MvtStock("bon de livraison","",this.getId(),this.getDaty(), AlloSakafoService.getNomRestau());
            mvt.setTypemvt("0");
            mvt.setDepot(this.getPoint());
            BonDeLivraisonFille[] lsfille= this.getFille(c);
             
            if(lsfille!=null){
                String[] ids = new String[lsfille.length];
                String[] quantites = new String[lsfille.length];
                for(int i=0;i<lsfille.length;i++){
                    ids[i]=lsfille[i].getProduit();
                    quantites[i]=""+lsfille[i].getQuantite();
                }
                //mvt, id,quantite,typemvt,c
                 u.saveInventaire(mvt, ids,quantites, "0",c);
                 u.validerObject(mvt,c);
            }
            
           
           
       }
       catch(Exception ex){
           ex.printStackTrace();
           throw ex;
       }
       finally{
          if(c!=null && verif==1)c.close();
       }
   }

}
