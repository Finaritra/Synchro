/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import java.sql.Date;

/**
 *
 * @author tahina
 */
public class MvtStockDetailComplet extends MvtStockFille {
    private String typemvt, designation, observation, depot, typebesoin, fournisseur, beneficiaire, numbc, numbs;

    
    private Date daty;
    int etat;
    
    public MvtStockDetailComplet()
    {
        setNomTable("MvtStockDetLibComplet");
    }

    

    public String getTypemvt() {
        return typemvt;
    }

    public void setTypemvt(String typemvt) {
        this.typemvt = typemvt;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getDepot() {
        return depot;
    }

    public void setDepot(String depot) {
        this.depot = depot;
    }

    public String getTypebesoin() {
        return typebesoin;
    }

    public void setTypebesoin(String typebesoin) {
        this.typebesoin = typebesoin;
    }

    public String getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }

    public String getBeneficiaire() {
        return beneficiaire;
    }

    public void setBeneficiaire(String beneficiaire) {
        this.beneficiaire = beneficiaire;
    }

    public String getNumbc() {
        return numbc;
    }

    public void setNumbc(String numbc) {
        this.numbc = numbc;
    }

    public String getNumbs() {
        return numbs;
    }

    public void setNumbs(String numbs) {
        this.numbs = numbs;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    
    
}
