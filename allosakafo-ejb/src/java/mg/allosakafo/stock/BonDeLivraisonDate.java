/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import java.sql.Date;

/**
 *
 * @author Axel
 */
public class BonDeLivraisonDate extends BonDeLivraisonFille {
    private Date daty;
    public BonDeLivraisonDate(){
        setNomTable("as_bondelivraison_fille_date");
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }
    
}
