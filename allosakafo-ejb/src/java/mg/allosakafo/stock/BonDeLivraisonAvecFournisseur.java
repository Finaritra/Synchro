/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

/**
 *
 * @author rajao
 */
public class BonDeLivraisonAvecFournisseur extends BonDeLivraison {
    private String idfournisseur,libellefournisseur;
    public BonDeLivraisonAvecFournisseur(){
        super();
        this.setNomTable("bondelivraisonavecfournisseur");
    }

    public String getIdfournisseur() {
        return idfournisseur;
    }

    public void setIdfournisseur(String idfournisseur) {
        this.idfournisseur = idfournisseur;
    }

    public String getLibellefournisseur() {
        return libellefournisseur;
    }

    public void setLibellefournisseur(String libellefournisseur) {
        this.libellefournisseur = libellefournisseur;
    }
    
}
