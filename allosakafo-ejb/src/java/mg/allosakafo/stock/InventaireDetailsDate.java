/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import java.sql.Date;

/**
 *
 * @author MEVA
 */
public class InventaireDetailsDate extends InventaireDetails{
    private Date daty;
    private double pu;

    public InventaireDetailsDate() {
        this.setNomTable("inventairemerelibcomplet");
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double prixunitaire) {
        this.pu = prixunitaire;
    }
}
