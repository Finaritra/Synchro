/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import java.sql.Date;

/**
 *
 * @author MEVA
 */
public class InventaireDetailsComplet extends InventaireDetails{
    private Date daty;
    private String depot, type, observation, etatfille,libelleingredient,unite,photo,categorieingredient,libelledepot;
    private double seuil,quantiteparpack,pu;
    private int compose,etat;

    public String getLibelledepot() {
        return libelledepot;
    }

    public void setLibelledepot(String libelledepot) {
        this.libelledepot = libelledepot;
    }

    public String getLibelleingredient() {
        return libelleingredient;
    }

    public void setLibelleingredient(String libelleingredient) {
        this.libelleingredient = libelleingredient;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCategorieingredient() {
        return categorieingredient;
    }

    public void setCategorieingredient(String categorieingredient) {
        this.categorieingredient = categorieingredient;
    }

    public double getSeuil() {
        return seuil;
    }

    public void setSeuil(double seuil) {
        this.seuil = seuil;
    }

    public double getQuantiteparpack() {
        return quantiteparpack;
    }

    public void setQuantiteparpack(double quantiteparpack) {
        this.quantiteparpack = quantiteparpack;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    public int getCompose() {
        return compose;
    }

    public void setCompose(int compose) {
        this.compose = compose;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    

    public InventaireDetailsComplet() {
        this.setNomTable("inventairemerelibcomplet");
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public String getDepot() {
        return depot;
    }

    public void setDepot(String depot) {
        this.depot = depot;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getEtatfille() {
        return etatfille;
    }

    public void setEtatfille(String etatfille) {
        this.etatfille = etatfille;
    }

}
