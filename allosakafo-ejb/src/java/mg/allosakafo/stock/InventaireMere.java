/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;
import bean.CGenUtil;
import bean.ClassEtat;
import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import java.util.HashMap;
import mg.allosakafo.produits.Ingredients;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Axel
 */
public class InventaireMere  extends ClassEtat {
    
    private String id ,  depot , type , observation , Idcategorieingredient;
    private Date daty ;  
    private int nbincident;
    InventaireDetails[] listeDetails;

    public InventaireDetails[] getListeDetails() {
        return listeDetails;
    }

    public void setListeDetails(InventaireDetails[] listeDetails) {
        this.listeDetails = listeDetails;
    }

    public int getNbincident() {
        return nbincident;
    }

    public void setNbincident(int nbincident) {
        this.nbincident = nbincident;
    }

   
    public InventaireMere(){
        setNomTable("inventairemere");
    }

    public String getIdcategorieingredient() {
        return Idcategorieingredient;
    }

    public void setIdcategorieingredient(String Idcategorieingredient) {
        this.Idcategorieingredient = Idcategorieingredient;
    }
    
    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
         return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepot() {
        return depot;
    }

    public void setDepot(String depot) {
        this.depot = depot;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("IM", "GETinventairemere");
        this.setId(makePK(c));
    }
     public void insertInventaireFille(String[] idingredients, String[] quantite,String[] qtetheorique, String user,Connection c) throws Exception {
        int verif = 0;
        try {
            if(c==null){
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            verif =1;
            }
            
            for(int i=0;i<idingredients.length;i++){
                if(quantite[i]!=null&&quantite[i].compareToIgnoreCase("")!=0)
                {
                    InventaireDetails details= new InventaireDetails(this.getId(),idingredients[i],"",quantite[i],qtetheorique[i], this.getIdcategorieingredient(), this.getDaty());
                    if(qtetheorique!=null){
                        details.setQtetheorique(qtetheorique[i]);
                    }
                    details.construirePK(c);
                    details.insertToTableWithHisto(user, c);
                }
                    
            }
            if (verif==1)c.commit();
        } 
        catch (Exception e) 
        {
                c.rollback();
                throw e;
        } 
        finally 
        {
                if (c != null && verif==1) {
                    c.close();
            }
        }
    }
    

    public void insertInventaireFille(String[] idingredients, String[] quantite, String user,Connection c) throws Exception {
        int verif = 0;
        try {
            if(c==null){
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            verif =1;
            }
            this.insertInventaireFille(idingredients,  quantite,null,  user, c);
           
            if (verif==1)c.commit();
        } 
        catch (Exception e) 
        {
                c.rollback();
                throw e;
        } 
        finally 
        {
                if (c != null && verif==1) {
                    c.close();
            }
        }
    }
    public InventaireDetails[] getFille(String nomTable,Connection c) throws Exception
    {
        boolean estOuvert=false;
        try{            
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            InventaireDetails crt=new InventaireDetails();
            if(nomTable!=null)crt.setNomTable(nomTable);
            crt.setIdmere(this.getId());
            return (InventaireDetails[])CGenUtil.rechercher(crt, null,null, c,"");
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            if(c!=null&&estOuvert==true)c.close();
        }
    }
    public void controlerUpdate(Connection c) throws Exception {
        InventaireMere[] aControler=(InventaireMere [])CGenUtil.rechercher(new InventaireMere(), null,null, c, " and id='"+this.getId()+"'");
        if(aControler.length==0)throw new Exception("Inventaire non existante");
        /*InventaireMere contr=new InventaireMere();
        String[]valDaty={Utilitaire.datetostring(aControler[0].getDaty()),""};
        String []crtDaty={"daty"};
        contr.setIdcategorieingredient(aControler[0].getIdcategorieingredient());
        InventaireMere []linv=(InventaireMere [])CGenUtil.rechercher(contr, crtDaty,valDaty, c, " and etat>=11");
        if(linv.length>0)throw new Exception("Inventaire existante dans la meme categorie");*/
    }  
    public void controler(Connection c)throws Exception
    {
        /*InventaireMere contr=new InventaireMere();
        String[]valDaty={Utilitaire.datetostring(this.getDaty()),""};
        String []crtDaty={"daty"};
        contr.setIdcategorieingredient(this.getIdcategorieingredient());
        InventaireMere []linv=(InventaireMere [])CGenUtil.rechercher(contr, crtDaty,valDaty, c, " and etat>=11");
        if(linv.length>0)throw new Exception("Inventaire existante dans la meme categorie");*/
    }
         
    public void insertInventaireFille(Ingredients[]listeing,HashMap<String,Double> getquantite,String user,Connection c)throws Exception{
       int verif = 0;
       try {
           if(c==null){
           c = new UtilDB().GetConn();
           c.setAutoCommit(false);
           verif =1;
           }
           if(listeing!=null && listeing.length>0){
               for(int i=0;i<listeing.length;i++){
                    InventaireDetails details= new InventaireDetails(this.getId(),listeing[i].getId(),"",getquantite.get(listeing[i].getId()), listeing[i].getCategorieingredient(), this.getDaty());
                    details.construirePK(c);
                    details.insertToTableWithHisto(user, c);
                    /*else{
                       //saisie inventairecompose
                        details.setNomTable("inventairecompose");
                        details.construirePK(c);
                        details.insertToTableWithHisto(user, c);

                        Ingredients[] listecompose=listeing[i].decomposer(c);
                        this.insertInventaireFille(listecompose, getquantite, user, c);
                    }*/
               }
            }

           if (verif==1)c.commit();
           } catch (Exception e) {
                if (c != null && verif==1) {
                    c.rollback();
                }
               throw e;
            } finally {
                if (c != null && verif==1) {
                   c.close();
            }
        }
    }
   
    
     public InventaireDetailsComplet[] getFilleComplet(String nomTable,Connection c) throws Exception
    {
        boolean estOuvert=false;
        try{            
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            InventaireDetailsComplet crt=new InventaireDetailsComplet();
            if(nomTable!=null)crt.setNomTable(nomTable);
            crt.setIdmere(this.getId());
            return (InventaireDetailsComplet[])CGenUtil.rechercher(crt, null,null, c,"");
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            if(c!=null&&estOuvert==true)c.close();
        }
    }
    public void controlerDoublon(Connection c)throws Exception{
         boolean estOuvert=false;
        try{
              if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            InventaireDetailsComplet[] ls= this.getFilleComplet("inventairedetailslib",c);
            String req =null;
            if(ls!=null && ls.length>0){
                req="'"+ls[0].getExplication()+"'";
                for(int i=1;i<ls.length;i++){
                    
                   req+=",'"+ls[i].getExplication()+"'";
                  
                }
                 InventaireDetailsComplet inv = new InventaireDetailsComplet();
                inv.setNomTable("inventairedetailslib");
                 InventaireDetailsComplet[] lsd = (InventaireDetailsComplet[])CGenUtil.rechercher(inv,null,null,c," and depot='"+this.getDepot()+"' and explication in ("+req+") and daty='"+Utilitaire.datetostring(ls[0].getDaty())+"' and etat=11" );
                 if(lsd!=null && lsd.length>0)throw new Exception("il existe deja un inventaire sur cette date");
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw ex;
        }
         finally
        {
            if(c!=null&&estOuvert==true)c.close();
        }
    }
    public ClassMAPTable createObject(String u,Connection c)throws Exception
    {
        ClassMAPTable retour=super.createObject(u, c);
        if(getListeDetails()!=null&&getListeDetails().length>0)
        {
            getListeDetails()[0].createObject(u, c);
        }
        return retour;
    }
       
}
