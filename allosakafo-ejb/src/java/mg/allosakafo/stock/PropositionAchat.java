/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.stock;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author MEVA
 */
public class PropositionAchat {

    private String id, idingredient,ingredient, fournisseur,idunite,unite;
    private Date daty;
    private double quantite, pu, montant;

    public PropositionAchat(String id,String idingredient,String ingredient, String idunite,String unite, Date daty, double quantite,Connection c) throws Exception {
        this.setId(id);
        this.setIngredient(ingredient);
        this.setIdingredient(idingredient);
        this.setUnite(unite);
        this.setIdunite(idunite);
        this.setDaty(daty);
        this.setQuantite(quantite);
    }
    
    public PropositionAchat(String id,String idingredient,String ingredient, String idunite,String unite, Date daty, double quantite, double pu,Connection c) throws Exception {
        this.setId(id);
        this.setIngredient(ingredient);
        this.setIdingredient(idingredient);
        this.setUnite(unite);
        this.setIdunite(idunite);
        this.setDaty(daty);
        this.setQuantite(quantite);
        this.setPu(pu);
    }

    public String getIdingredient() {
        return idingredient;
    }

    public void setIdingredient(String idingredient) {
        this.idingredient = idingredient;
    }

    public String getIdunite() {
        return idunite;
    }

    public void setIdunite(String idunite) {
        this.idunite = idunite;
    }

    public PropositionAchat() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) throws Exception {
        if( quantite<0) throw new Exception("Quantit� invalide");
        this.quantite = quantite;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    
    
}
