package mg.allosakafo.stock;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author BICI
 */
public class MvtStockFille extends ClassMAPTable {

    private String id, idmvtstock, ingredients, idmvt,idunite;
    private double solde, pu, entree, sortie,stock_init;

    public String getIdunite() {
        return idunite;
    }

    public void setIdunite(String idunite) {
        this.idunite = idunite;
    }

    public boolean isSynchro()
    {
        return true;
    }

    public MvtStockFille() {
        this.setNomTable("AS_MVT_STOCK_FILLE");
    }
	
	public MvtStockFille(String idmvtstock, String ingredients, double entree, double sortie) {
        this.setNomTable("AS_MVT_STOCK_FILLE");
		this.setIdmvtstock(idmvtstock);
		this.setIngredients(ingredients);
		this.setEntree(entree);
		this.setSortie(sortie);
    }
        
        public MvtStockFille(String idmvtstock, String ingredients, double entree, double sortie,String unite) {
        this.setNomTable("AS_MVT_STOCK_FILLE");
		this.setIdmvtstock(idmvtstock);
		this.setIngredients(ingredients);
		this.setEntree(entree);
		this.setSortie(sortie);
                this.setIdunite(unite);
    }
    public void construirePK(Connection c) throws Exception {
        this.preparePk("MVTSF", "getSeqMvtStockFille");
        this.setId(makePK(c));
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdmvtstock() {
        return idmvtstock;
    }

    public void setIdmvtstock(String idmvtstock) {
        this.idmvtstock = idmvtstock;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public double getEntree() {
        return entree;
    }

    public void setEntree(double entree) {
        this.entree = entree;
    }

    public double getSortie() {
        return sortie;
    }

    public void setSortie(double sortie) {
        this.sortie = sortie;
    }

    public String getIdmvt() {
        return idmvt;
    }

    public void setIdmvt(String idmvt) {
        this.idmvt = idmvt;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }
     public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.solde = pu;
    }

    /**
     * @return the stock_init
     */
    public double getStock_init() {
        return stock_init;
    }

    /**
     * @param stock_init the stock_init to set
     */
    public void setStock_init(double stock_init) {
        this.stock_init = stock_init;
    }
}
