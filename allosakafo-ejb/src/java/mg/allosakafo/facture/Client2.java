/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.facture;

import java.sql.Connection;

/**
 *
 * @author MEVA
 */
public class Client2 extends Client{
    private String id,prenom;

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        if(prenom == null)
            this.prenom = "-";
        else
            this.prenom = prenom;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getAttributIDName(){
        return "id";
    }
     public String getTuppleID()
    {
        return String.valueOf(getId());
    }
     
     public void construirePK(Connection c) throws Exception {
        this.preparePk("CLT", "getSeqASClient");
        this.setIdClient(makePK(c));
        this.setId(idClient);
    }

}
