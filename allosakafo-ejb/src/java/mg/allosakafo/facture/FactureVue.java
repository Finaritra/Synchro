/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.facture;

import bean.ClassMAPTable;
import java.sql.Date;

/**
 *
 * @author HP
 */
public class FactureVue extends ClassMAPTable {

    private String id, client, modepaiement, adresseclient,detail;
    private Date datefacture, datesaisie;
    private double montant;
    
    
    @Override
    public String getTuppleID() {
        return getId();
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
    
    public FactureVue(){
        super.setNomTable("facturevue");
    }

    public String getId() {
        return id;
    }

    public String getClient() {
        return client;
    }

    public String getModepaiement() {
        return modepaiement;
    }

    public String getAdresseclient() {
        return adresseclient;
    }

    public Date getDatefacture() {
        return datefacture;
    }

    public Date getDatesaisie() {
        return datesaisie;
    }

    public double getMontant() {
        return montant;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public void setModepaiement(String modepaiement) {
        this.modepaiement = modepaiement;
    }

    public void setAdresseclient(String adresseclient) {
        this.adresseclient = adresseclient;
    }

    public void setDatefacture(Date datefacture) {
        this.datefacture = datefacture;
    }

    public void setDatesaisie(Date datesaisie) {
        this.datesaisie = datesaisie;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
    
}
