/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.facture;

import bean.ClassMAPTable;
import java.sql.Connection;

/**
 *
 * @author Andy Rand
 */
public class DetailsFacture extends ClassMAPTable{
    
    
     private String id,idfacture,idproduit;
     private double qte,pu,montant;

    public DetailsFacture() {
         this.setNomTable("DETAILSFACTURE");
    }

    public DetailsFacture(String idfacture, String idproduit, double qte, double pu, double montant) throws Exception {
        this.setNomTable("DETAILSFACTURE");
        this.setIdfacture(idfacture);
        this.setIdproduit(idproduit);
        this.setQte(qte);
        this.setPu(pu);
        this.setMontant(montant);
    }

    public DetailsFacture(String id, String idfacture, String idproduit, double qte, double pu, double montant) throws Exception {
        this.setNomTable("DETAILSFACTURE");
        this.setId(id);
        this.setIdfacture(idfacture);
        this.setIdproduit(idproduit);
        this.setQte(qte);
        this.setPu(pu);
        this.setMontant(montant);
    }
     


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdfacture() {
        return idfacture;
    }

    public void setIdfacture(String idfacture) {
        this.idfacture = idfacture;
    }

    public String getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(String idproduit) {
        this.idproduit = idproduit;
    }

    public double getQte() {
        return qte;
    }

    public void setQte(double qte) throws Exception {
         if (getMode().compareToIgnoreCase("modif") == 0) {
            if (qte < 0) {
               throw new Exception("qte invalide");
            } 
        }
        this.qte = qte;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) throws Exception {
         if (getMode().compareToIgnoreCase("modif") == 0) {
            if (pu < 0) {
               throw new Exception("pu invalide");
            } 
        }
       
        
        this.pu = pu;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) throws Exception {
          if (getMode().compareToIgnoreCase("modif") == 0) {
            if (montant < 0) {
               throw new Exception("montant invalide");
            } 
        }
        this.montant = montant;
    }
     
     

     @Override
    public String getTuppleID() {
     return this.id;
    }

    @Override
    public String getAttributIDName() {
    return "id";
    }
    public void construirePK(Connection c) throws Exception {
         this.preparePk("DET", "GETSEQDetailsfacture");
        this.setId(makePK(c));
        
    }
    
}
