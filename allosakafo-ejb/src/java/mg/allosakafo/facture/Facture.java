/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.facture;

import bean.AdminGen;
import bean.CGenUtil;
import bean.ClassEtat;
import bean.TypeObjet;
import java.sql.Connection;
import java.sql.Date;
import mg.allosakafo.commande.CommandeClientDetails;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Admin
 */
public class Facture extends ClassEtat{
    private String id, client, modepaiement, adresseclient;
    private Date datefacture, datesaisie;
    private double montant;

    public Facture() {
        super.setNomTable("facture");
    }
    
    public Facture(String client,String modepaiement,String adresseclient,Date datefacture) {
        super.setNomTable("facture");
        setClient(client);
        setModepaiement(modepaiement);
        setAdresseclient(adresseclient);
        setDatefacture(datefacture);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getModepaiement() {
        return modepaiement;
    }

    public void setModepaiement(String modepaiement) {
        this.modepaiement = modepaiement;
    }

    public String getAdresseclient() {
        return adresseclient;
    }

    public void setAdresseclient(String adresseclient) {
        this.adresseclient = adresseclient;
    }

    public Date getDatefacture() {
        return datefacture;
    }

    public void setDatefacture(Date datefacture) {
        this.datefacture = datefacture;
    }

    public Date getDatesaisie() {
        return datesaisie;
    }

    public void setDatesaisie(Date datesaisie) {
        this.datesaisie = datesaisie;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
    
    public void construirePK(Connection c) throws Exception {
        this.preparePk("FACT", "getSeqfacture");
        //this.setIdClient(makePK(c));
        this.setId(makePK(c));
    }

    public void gererClient(String refuser,Connection c)throws Exception{
        int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }
            String[] client=this.getClient().split("-");
            if(client.length!=2) throw new Exception("T�l�phone et nom obligatoires");
            Client cl=new Client();
            cl.setNomTable("client");
            Client[] liste=(Client[]) CGenUtil.rechercher(cl, null, null, c, " and telephone like '%"+client[0]+"%'");
            /*TypeObjet to=new TypeObjet();
            to.setNomTable("regime");
            TypeObjet[] lto=(TypeObjet[])CGenUtil.rechercher(to, null, null, c, " and val like '%"+client[2]+"%'");
            if(lto.length==0) throw new Exception("R�gime introuvable");*/
            if(liste.length==0){
                cl.setNom(client[1]);
                cl.setIdRegime("reg1");
                cl.setTelephone(client[0]);
                cl.construirePK(c);
                cl.setIdClient(cl.getId());
                cl.insertToTableWithHisto(refuser, c);
            }else{
                //System.out.println("mg.allosakafo.facture.Facture.gererClient() ELSE");
                cl=liste[0];
                cl.setId(cl.getIdClient());
            }
            //System.out.println("mg.allosakafo.facture.Facture.gererClient() " + cl.getTelephone() + "  " + cl.getNom() + "  " + cl.getIdClient());
            this.setClient(cl.getId());
            if(c!=null && verif==1)c.commit();
        }catch(Exception e){
            if(c!=null && verif==1)c.rollback();
            throw e;
        }finally{
            if(c!=null && verif==1)c.close();
        }
        
    }
    
    public void insertFacture(String[] listeDetailsCommande,String refuser,Connection c) throws Exception{
        int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }
            String id = Utilitaire.tabToString(listeDetailsCommande, "'",",");
            System.out.println("id ----------------- "+id +"  refuser "+ refuser);
            CommandeClientDetails[] listeDetails=(CommandeClientDetails[]) CGenUtil.rechercher(new CommandeClientDetails(), null, null, c, " and etat>0 and ( id in ("+id+") OR idmere IN ("+id+"))  ");
            if(listeDetails.length==0) throw new Exception("Pas de d�tails commande");
            this.gererClient(refuser, c);
            double totalFacture=AdminGen.calculSommeDouble(listeDetails, "montant");
             System.out.println("mg.allosakafo.facture.Facture.insertFacture() " + totalFacture);
            this.setMontant(totalFacture);
            this.construirePK(c);
            
            this.insertToTableWithHisto(refuser, c);
            
            DetailsFacture df=null;
            for(int j=0;j<listeDetails.length;j++){
                df=new DetailsFacture(this.getId(), listeDetails[j].getProduit(), listeDetails[j].getQuantite(), listeDetails[j].getPu(), listeDetails[j].getMontant());
                df.construirePK(c);
                df.insertToTableWithHisto(refuser, c);
            }
            
            if(verif==1 && c!=null)c.commit();
        }catch(Exception e){
            e.printStackTrace();
            if(verif==1 && c!=null)c.rollback();
            throw e;
        }finally{
            if(verif==1 && c!=null)c.close();
        }
    }
}
