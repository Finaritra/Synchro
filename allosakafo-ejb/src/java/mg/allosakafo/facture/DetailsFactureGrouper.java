/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.facture;

import java.sql.Date;

/**
 *
 * @author JESSI
 */
public class DetailsFactureGrouper extends Facture {
     private String idproduit;
     private double qte,pu;

    public DetailsFactureGrouper() {
           this.setNomTable("DETAILSFACTURE_GROP");
 
    }

    public String getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(String idproduit) {
        this.idproduit = idproduit;
    }

    public double getQte() {
        return qte;
    }

    public void setQte(double qte) {
        this.qte = qte;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

   
}
