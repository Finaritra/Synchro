/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.facture;

import bean.ClassMAPTable;
import bean.TypeObjet;

/**
 *
 * @author nyamp
 */
public class ClientPoint extends TypeObjet{
    private String  point;
    private String  pointlib;
    
    public ClientPoint(){
        this.setNomTable("TableClientPointVue");
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getPointlib() {
        return pointlib;
    }

    public void setPointlib(String pointlib) {
        this.pointlib = pointlib;
    }

    @Override
    public String getValColLibelle() {
        return this.getVal();
    }
}
