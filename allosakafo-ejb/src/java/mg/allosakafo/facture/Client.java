// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   Client.java

package mg.allosakafo.facture;

import bean.*;
import java.sql.Connection;
import utilitaire.Utilitaire;


public class Client extends ClassMAPTable
{

    public Client(String i, String no, String reg, String adr, String tel, String fa, String sta,
            String ni, String r, String tp, String quit) throws Exception
    {
        super.setNomTable("as_client");
        setIdClient(i);
        setNom(no);
        setNif(ni);
        setAdresse(adr);
        setTelephone(tel);
        setFax(fa);
        setIdRegime(reg);
        setRc(r);
        setQuittance(quit);
        setNumstat(sta);
        setTp(tp);
    }

    public Client(String no, String reg, String tel, String fa, String adr, String sta, String ni,
            String r, String tp, String quit) throws Exception
    {
        super.setNomTable("as_client");
        if(no.compareTo("") == 0 || no == null)
            setNom("-");
        else
            setNom(no);
        if(ni.compareTo("") == 0 || ni == null)
            setNif("-");
        else
            setNif(ni);
        if(adr.compareTo("") == 0 || adr == null)
            setAdresse("-");
        else
            setAdresse(adr);
        setTelephone(tel);
        if(fa.compareTo("") == 0 || fa == null)
            setFax("-");
        else
            setFax(fa);
        if(reg.compareTo("") == 0 || reg == null)
            setIdRegime("-");
        else
            setIdRegime(reg);
        if(r.compareTo("") == 0 || r == null)
            setRc("-");
        else
            setRc(r);
        if(quit.compareTo("") == 0 || quit == null)
            setQuittance("-");
        else
            setQuittance(quit);
        if(sta.compareTo("") == 0 || sta == null)
            setNumstat("-");
        else
            setNumstat(sta);
        if(tp.compareTo("") == 0 || tp == null)
            setTp("-");
        else
            setTp(tp);
        setIndicePk("clt");
        setNomProcedureSequence("getSeqClient");
        setIdClient(makePK());
    }
    public Client()
    {
      super.setNomTable("as_client");
    }
	
	public Client(String _nom, String _telephone) throws Exception
    {
      super.setNomTable("as_client");
	  this.setNom(_nom);
	  this.setTelephone(_telephone);
    }
	
    public String getTuppleID()
    {
        return id;
    }

    public String getAttributIDName()
    {
        return "id";
    }

    public void construirePK(Connection c) throws Exception {
            
    if( this.getNomTable().compareToIgnoreCase("fournisseurProduits") == 0 ){        
        this.preparePk("F", "getSeqASClient");
    }else{
        this.preparePk("CLT", "getSeqASClient");
    }
        //this.setIdClient(makePK(c));
        this.setId(makePK(c));
    }
	
    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        if(nom == null)
            this.nom = "";
        else
            this.nom = nom;
    }

    public void setNif(String nif)
    {
        this.nif = nif;
    }

    public String getNif()
    {
        return nif;
    }

    public void setAdresse(String adresse)
    {
        if(adresse == null)
            this.adresse = "";
        else
            this.adresse = adresse;
    }

    public String getAdresse()
    {
        return adresse;
    }

    public void setTelephone(String telephone) throws Exception
    {
        if(this.getMode().compareToIgnoreCase("select")==0)
        {
            this.telephone = telephone;
            return;
        }
            
        if (telephone == null || telephone.compareTo("") == 0) {
            throw new Exception("Champ telephone manquant");
        }
        else
        {
            String[] numeros = Utilitaire.split(telephone, ";");
            for (int i=0; i<numeros.length; i++)
            {
                    if (!numeros[i].startsWith("0") && !numeros[i].startsWith("261")) throw new Exception("Numero invalide");
                    else if (numeros[i].startsWith("0") && numeros[i].length() != 10) throw new Exception("Numero invalide");
                    else if (numeros[i].startsWith("261") && numeros[i].length() != 12) throw new Exception("Numero invalide");
            }
            this.telephone = telephone;
        }
    }

    public String getTelephone()
    {
        return telephone;
    }

    public void setFax(String fax)
    {
        if(fax == null)
            this.fax = "";
        else
            this.fax = fax;
    }

    public String getFax()
    {
        return fax;
    }

    public void setIdClient(String idClient)
    {
        this.idClient = idClient;
    }

    public String getIdClient()
    {
        return idClient;
    }

    public void setRc(String rc)
    {
        if(rc == null)
            this.rc = "";
        else
            this.rc = rc;
    }

    public String getRc()
    {
        return rc;
    }

    public void setTp(String tp)
    {
        if(tp == null)
            this.tp = "";
        else
            this.tp = tp;
    }

    public String getTp()
    {
        return tp;
    }

    public void setQuittance(String quittance)
    {
        if(quittance == null)
            this.quittance = "";
        else
            this.quittance = quittance;
    }

    public String getQuittance()
    {
        return quittance;
    }

    public void setIdRegime(String idRegime)
    {
        if(idRegime == null)
            this.idRegime = "-";
        else
            this.idRegime = idRegime;
    }

    public String getIdRegime()
    {
        return idRegime;
    }
    public static String getIdDyn(String idClient)throws Exception
    {
      Connection c=null;
      try {
        c=new utilitaire.UtilDB().GetConn();
        return getIdDyn(idClient,c);
      }
      catch (Exception ex) {
        throw ex;
      }
      finally
      {
        if(c!=null)c.close();
      }
    }
    public static String getIdDyn(String idClient,Connection c) throws Exception
    {
      char[] idc=idClient.toCharArray();
      Client crtc=new Client();
      Client[]lc=null;
      if(idc.length==9&&idc[0]=='c'&&idc[1]=='l'&&idc[2]=='t')
      {
        crtc.setIdClient(idClient);
        lc=(Client[])CGenUtil.rechercher(crtc,null,null,c,"");
        if(lc.length>0) return (idClient);
        else
        {
          crtc.setIdClient("");
          crtc.setNom(idClient);
          lc=(Client[])CGenUtil.rechercher(crtc,null,null,c,"");
          if(lc.length==0)throw new Exception("Pas de client");
          return (lc[0].getIdClient());
        }
      }
      else
      {
        crtc.setNom(idClient);
        lc=(Client[])CGenUtil.rechercher(crtc,null,null,c,"");
        if(lc.length==0)throw new Exception("Pas de client");
        return(lc[0].getIdClient());
      }
    }
    public void setNumstat(String numstat)
    {
        if(numstat == null)
            this.numstat = "-";
        else
            this.numstat = numstat;
    }

    public String getNumstat()
    {
        return numstat;
    }

    public TypeObjet getInfoComptable()
    {
        TypeObjetUtil tu = new TypeObjetUtil();
        tu.setNomTable("compte");
        return (TypeObjet)tu.rechercher(3, getTuppleID())[0];
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void controler(Connection c) throws Exception {
       if(this.getNomTable().compareToIgnoreCase("fournisseurProduits")==0){
                Client cl = new Client();
                cl.setNomTable("fournisseurProduits");
                Client[] ls = (Client[])CGenUtil.rechercher(cl, null, null, c, " and replace(telephone, ' ', '') like replace ('%"+this.getTelephone()+"%',' ','')");
                if(ls!=null && ls.length>0)throw new Exception("le numero telephone existe deja");
       }
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getEstlounge() {
        return estlounge;
    }

    public void setEstlounge(int estlounge) {
        this.estlounge = estlounge;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }
    
    
  

    public String nom,prenom;
    public String nif;
    public String adresse;
    public String telephone;
    public String fax;
    public String rc;
    public String tp;
    public String quittance;
    public String idRegime;
    public String idClient;
    public String numstat;
    public String id;
    public int estlounge;
    public String point;
}
