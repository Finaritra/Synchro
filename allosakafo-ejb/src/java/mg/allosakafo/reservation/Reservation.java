/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.reservation;

import bean.CGenUtil;
import bean.ClassEtat;
import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Time;
import mg.allosakafo.commande.CommandeClient;
import mg.allosakafo.commande.CommandeClientDetails;
import mg.allosakafo.facture.Client;
import service.AlloSakafoService;
import user.UserEJB;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Notiavina
 */
public class Reservation extends ClassEtat {
    private String id, source, heuresaisie,  heurefinreservation;
    private int nbpersonne;
    private Date datesaisie, datereservation;
    private String heuredebutreservation,idClient,idCommande,remarque,point;

    public String getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(String idCommande) {
        this.idCommande = idCommande;
    }
    
    public boolean isSynchro()
    {
        return true;
    }

    
    public Reservation(){
        super.setNomTable("as_reservation");
    }
    public Reservation(String id,String source,String heuresaisie,String heuredebutreservation,String heurefinreservation,int nbpersonne,Date datesaisie, Date datereservation,String remarque)throws Exception{
        super.setNomTable("as_reservation");
        setId(id);
        setSource(source);
        setHeuresaisie(heuresaisie);
        setHeuredebutreservation(heuredebutreservation);
        setHeurefinreservation(heurefinreservation);
        setNbpersonne(nbpersonne);
        setDatesaisie(datesaisie);
        setDatereservation(datereservation);
    }
    public Reservation(String idCl,String source,String heuredebutreservation,String nbpersonne,String datereservation,String remarque)throws Exception{
        super.setNomTable("as_reservation");
        this.setIdClient(idCl);
        setSource(source);
        setHeuresaisie(Utilitaire.heureCouranteHMS());
        setHeuredebutreservation(heuredebutreservation);
        setNbpersonne(Utilitaire.stringToInt(nbpersonne));
        setDatesaisie(Utilitaire.dateDuJourSql());
        setDatereservation(Utilitaire.stringDate(datereservation));
        this.setRemarque(remarque);
    }
    public Reservation(String idCl,String source,String heuredebutreservation,String nbpersonne,String datereservation,String remarque,String point)throws Exception{
        super.setNomTable("as_reservation");
        this.setIdClient(idCl);
        setSource(source);
        setHeuresaisie(Utilitaire.heureCouranteHMS());
        setHeuredebutreservation(heuredebutreservation);
        setNbpersonne(Utilitaire.stringToInt(nbpersonne));
        setDatesaisie(Utilitaire.dateDuJourSql());
        setDatereservation(Utilitaire.stringDate(datereservation));
        this.setRemarque(remarque);
        this.setPoint(point);
    }
    public String[] getCommandes() throws Exception{
        String[] retour=null;
        if(this.getIdCommande()!=null && this.getIdCommande().compareTo("")!=0){
            retour=this.getIdCommande().split(";");
        }
        return retour;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getHeuresaisie() {
        return heuresaisie;
    }

    public void setHeuresaisie(String heureSaisie) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            this.heuresaisie=Utilitaire.getCurrentHeure();
            return;
        }
        this.heuresaisie = heureSaisie;
    }

    public String getHeuredebutreservation() {
        return heuredebutreservation;
    }
    
    public void setHeuredebutreservation(String heureDebutReservation) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (heureDebutReservation != null && heureDebutReservation.compareToIgnoreCase("") != 0) {
                this.heuredebutreservation=AlloSakafoService.formaterHeure(heureDebutReservation);
                return;
            } else {
                throw new Exception("Heure debut reservation invalide");
            }
        }
        this.heuredebutreservation = heureDebutReservation;
    }

    public String getHeurefinreservation() {
        return heurefinreservation;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }
    
    

    public void setHeurefinreservation(String heureFinReservation) throws Exception {
        String debut=getHeuredebutreservation();
        if (getMode().compareToIgnoreCase("modif") == 0) {
            long time=0;
            if(debut!=null && debut.compareToIgnoreCase("")!=0){
               debut=AlloSakafoService.formaterHeure(debut);
                time=Time.valueOf(debut).getTime();
            }
            this.heurefinreservation=new Time(time+(30*60000)).toString();
            return;
        }
        this.heurefinreservation=heureFinReservation;       
    }

    public int getNbpersonne() {
        return nbpersonne;
    }

    public void setNbpersonne(int nbpersonne) throws Exception {
        if(getMode().equals("modif") && nbpersonne<1){
            throw new Exception("Nombre personne invalide");
        }
        this.nbpersonne = nbpersonne;
    }

    public Date getDatesaisie() {
        return datesaisie;
    }

    public void setDatesaisie(Date dateSaisie) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            this.datesaisie=Utilitaire.dateDuJourSql();
            return;
        }
        this.datesaisie = dateSaisie;
    }

    public Date getDatereservation() {
        return datereservation;
    }

    public void setDatereservation(Date datereservation) {
        this.datereservation = datereservation;
    }
    
    
    public void construirePK(Connection c) throws Exception {
        this.preparePk("RES", "getseqreservation");
        this.setId(makePK(c));
    }

    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }
    
    

    public  void gererClient(String refuser,Connection c)throws Exception{
        int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }
            Client[] liste=(Client[]) CGenUtil.rechercher(new Client(), null, null, c, " and id ='"+getIdClient()+"'");
            if(liste.length>0)
            {
                return;
            }
            
            String[] client=this.getIdClient().split("-");
            if(client.length==0||getIdClient().compareToIgnoreCase("")==0) throw new Exception("T�l�phone et nom obligatoires");
            if(Utilitaire.estTelephoneValide(client[0])==false) throw new Exception("Telephone non valide");
            Client cl=new Client();
            liste=(Client[]) CGenUtil.rechercher(cl, null, null, c, " and telephone like '%"+client[0]+"%'");
            if(liste.length==0||client[0].compareToIgnoreCase("")==0){
                if(client.length>1)cl.setNom(client[1]);
                cl.setTelephone(client[0]);
                cl.construirePK(c);
                cl.insertToTableWithHisto(refuser, c);
            }else{
                cl=liste[0];
            }
            this.setIdClient(cl.getId());
            if(c!=null && verif==1)c.commit();
        }catch(Exception e){
            if(c!=null && verif==1)c.rollback();
            throw e;
        }finally{
            if(c!=null && verif==1)c.close();
        }
        
    }
    public CommandeClient[] getCommandeClient(Connection c)throws Exception
    {
        CommandeClient crt=new CommandeClient();
        crt.setClient(this.getIdClient());
        return null;
    }
    
    public void effectuerReservation(String refuser,Connection c)throws Exception{
        int verif=0;
        try
        {
            if(c == null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            Reservation res=(Reservation)CGenUtil.rechercher(this, null, null, c, "")[0];
            if(res.getEtat()<ConstanteEtat.getEtatCreer() )throw new Exception("objet annule");
            res.setEtat(ConstanteEtat.getEffectuer());
           res.updateToTableWithHisto(refuser, c);
           if(verif==1)c.commit();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            if( c != null && verif == 1 ){c.rollback();}
            throw e;
        }
        finally
        {
            if(c!=null && verif==1)
            {
                c.close();
            }
        }
    }
    public CommandeClient[] getCommande(String nomTable,Connection c) throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c == null){
                c = new UtilDB().GetConn();
                estOuvert = true;
            }
            CommandeClient crt=new CommandeClient();
            if(nomTable!=null&&nomTable.compareToIgnoreCase("")!=0)crt.setNomTable(nomTable);
            crt.setTypecommande(this.getId());
            return (CommandeClient[])CGenUtil.rechercher(crt, null, null, c, "");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(estOuvert==true)c.close();
        }
    }
    public CommandeClientDetails[] getDetailComande(String nomTable,Connection c) throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c == null){
                c = new UtilDB().GetConn();
                estOuvert = true;
            }
            CommandeClientDetails crt=new CommandeClientDetails();
            if(nomTable!=null&&nomTable.compareToIgnoreCase("")!=0)crt.setNomTable(nomTable);
            crt.setTypeCommande(this.getId());
            return (CommandeClientDetails[])CGenUtil.rechercher(crt, null, null, c, "");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(estOuvert==true)c.close();
        }
    }
    public void changerTable(String nomClient,Connection c)throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c == null){
                c = new UtilDB().GetConn();
                estOuvert = true;
            }
            CommandeClient[]listeC=this.getCommande(null,c);
            for(CommandeClient com:listeC)
            {
                com.changerTable(nomClient, c);
            }
            if(c!=null)c.commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            c.rollback();
            throw e;
        }
        finally
        {
            if(estOuvert==true)c.close();
        }
    }
    public Reservation getResaMemeClientEnCours(Connection c) throws Exception
    {
        Reservation crt=new Reservation();
        crt.setIdClient(this.getIdClient());
        crt.setNomTable("as_reservationSansTable");
        Reservation[] valiny=(Reservation[])CGenUtil.rechercher(crt, null, null, c, " order by datereservation desc, id desc");
        if (valiny.length==0)return null;
        return valiny[0];
    }
    public ClassMAPTable createObject(String u,Connection c) throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c == null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                estOuvert = true;
            }
            this.gererClient(u, c);
            //String[] listeCommande=this.getCommandes();
            Reservation enCours=getResaMemeClientEnCours(c);
            if(enCours!=null&&Utilitaire.diffJourDaty(enCours.getDatereservation(), this.getDatereservation())==0) return enCours;
            this.construirePK(c);
            insertToTableWithHisto(u, c);
            
            if(estOuvert==true)c.commit();
            return this;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            c.rollback();
            throw e;
        }
        finally
        {
            if(estOuvert==true)c.close();
        }
    }

}
