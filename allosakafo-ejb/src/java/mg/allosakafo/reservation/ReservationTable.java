/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.reservation;

import bean.CGenUtil;
import bean.ClassEtat;
import bean.ClassMAPTable;
import java.sql.Connection;
import mg.allosakafo.commande.CommandeClient;
import service.AlloSakafoService;
import utilitaire.UtilDB;

/**
 *
 * @author Asus
 */
public class ReservationTable extends ClassEtat{
    private String id,idReservation,idtable;

    public ReservationTable() {
        this.setNomTable("RESA_TABLE");
    }

    public boolean isSynchro()
    {
        return true;
    }

    @Override
    public void controler(Connection c) throws Exception {
       String aWhere = " and id = '"+this.getIdReservation()+"'";
       Reservation[] reservations = (Reservation[])CGenUtil.rechercher(new Reservation(), null, null, c, aWhere);
       if(reservations != null && reservations.length > 0){
           if(reservations[0].getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0){
               throw new Exception("ACTION NON PERMISE");
           }
       }
    }

    public ReservationTable(String id, String idReservation, String idtable) {
        this.setNomTable("RESA_TABLE");
        this.setId(id);
        this.setIdReservation(idReservation);
        this.setIdtable(idtable);
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(String idReservation) {
        this.idReservation = idReservation;
    }

    public String getIdtable() {
        return idtable;
    }

    public void setIdtable(String idtable) {
        this.idtable = idtable;
    }
    
    
    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
    
     public void construirePK(Connection c) throws Exception {
        this.preparePk("RT", "getseqresa_table");
        this.setId(makePK(c));
    }
     
     
    public Reservation getReservation (String nT,Connection c)throws Exception
    {
        Reservation crt=new Reservation();
        if(nT!=null)crt.setNomTable(nT);
        crt.setId(this.getIdReservation());
        Reservation[] valiny=(Reservation[])CGenUtil.rechercher(crt, null, null, c, "");
        if(valiny.length==0)return null;
        return valiny[0];
    }
    public ClassMAPTable createObject(String u,Connection c)throws Exception
    {
        Reservation res=this.getReservation(null, c);
        if(res==null)throw new Exception("Reservation non existante");
        CommandeClient[] listeCom=res.getCommande(null, c);
        
        for(CommandeClient cmd:listeCom)
        {
            cmd.changerClient(this.getIdtable(), c);
        }
        ReservationTable crtRes=new ReservationTable();
        crtRes.setIdReservation(res.getId());
        ReservationTable[] listRes=(ReservationTable[])CGenUtil.rechercher(crtRes, null,null, c, "");
        if(listRes==null||listRes.length==0)
        return super.createObject(u, c);
        listRes[0].setIdtable(this.getIdtable());
        listRes[0].updateToTableWithHisto(u, c);
        return listRes[0];
    }

}
