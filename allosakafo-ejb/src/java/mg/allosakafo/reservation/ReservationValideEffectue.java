/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.reservation;

import bean.CGenUtil;
import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Time;
import mg.allosakafo.facture.Client;
import service.AlloSakafoService;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Maharo
 */
public class ReservationValideEffectue extends ClassEtat{
    private String id, source, heuresaisie, heuredebutreservation, heurefinreservation,idClient,tablenom;
    private int nbpersonne;
    private Date datesaisie, datereservation;

    public ReservationValideEffectue() {
    }

    public ReservationValideEffectue(String id, String source, String heuresaisie, String heuredebutreservation, String heurefinreservation, String idClient, String tablenom, int nbpersonne, Date datesaisie, Date datereservation) throws Exception {
         super.setNomTable("RESERVATIONLIBVALIDE");
        this.setIdClient(idClient);
        this.setTablenom(tablenom);
      
      
        
        
        setId(id);
        setSource(source);
        setHeuresaisie(heuresaisie);
        setHeuredebutreservation(heuredebutreservation);
        setHeurefinreservation(heurefinreservation);
        setNbpersonne(nbpersonne);
        setDatesaisie(datesaisie);
        setDatereservation(datereservation);
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

   public void setHeuresaisie(String heureSaisie) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            this.heuresaisie=Utilitaire.getCurrentHeure();
            return;
        }
        this.heuresaisie = heureSaisie;
    }

    public String getHeuresaisie() {
        return heuresaisie;
    }

    public String getHeuredebutreservation() {
        return heuredebutreservation;
    }
    
    public void setHeuredebutreservation(String heureDebutReservation) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (heureDebutReservation != null && heureDebutReservation.compareToIgnoreCase("") != 0) {
                this.heuredebutreservation=AlloSakafoService.formaterHeure(heureDebutReservation);
                return;
            } else {
                throw new Exception("Heure debut reservation invalide");
            }
        }
        this.heuredebutreservation = heureDebutReservation;
    }

  
    public String getHeurefinreservation() {
        return heurefinreservation;
    }

   public void setHeurefinreservation(String heureFinReservation) throws Exception {
        String debut=getHeuredebutreservation();
        if (getMode().compareToIgnoreCase("modif") == 0) {
            debut=AlloSakafoService.formaterHeure(debut);
            this.heurefinreservation=new Time(Time.valueOf(debut).getTime()+(30*60000)).toString();
            return;
        }
        this.heurefinreservation=heureFinReservation;       
    }


    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getTablenom() {
        return tablenom;
    }

    public void setTablenom(String tablenom) {
        this.tablenom = tablenom;
    }

    public int getNbpersonne() {
        return nbpersonne;
    }

    public void setNbpersonne(int nbpersonne) throws Exception {
        if(getMode().equals("modif") && nbpersonne<1){
            throw new Exception("Nombre personne invalide");
        }
        this.nbpersonne = nbpersonne;
    }

    public Date getDatesaisie() {
        return datesaisie;
    }

    public void setDatesaisie(Date dateSaisie) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            this.datesaisie=Utilitaire.dateDuJourSql();
            return;
        }
        this.datesaisie = dateSaisie;
    }

    public Date getDatereservation() {
        return datereservation;
    }

    public void setDatereservation(Date datereservation) {
        this.datereservation = datereservation;
    }
  
    
    
  
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }
    
    public void gererClient(String refuser,Connection c)throws Exception{
        int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }
            Client[] liste=(Client[]) CGenUtil.rechercher(new Client(), null, null, c, " and id ='"+getIdClient()+"'");
            if(liste.length>0)
            {
                return;
            }
            
            String[] client=this.getIdClient().split("-");
            if(client.length==0||getIdClient().compareToIgnoreCase("")==0) throw new Exception("T�l�phone et nom obligatoires");
            if(Utilitaire.estTelephoneValide(client[0])==false) throw new Exception("Telephone non valide");
            Client cl=new Client();
            liste=(Client[]) CGenUtil.rechercher(cl, null, null, c, " and telephone like '%"+client[0]+"%'");
            if(liste.length==0||client[0].compareToIgnoreCase("")==0){
                if(client.length>1)cl.setNom(client[1]);
                cl.setTelephone(client[0]);
                cl.construirePK(c);
                cl.insertToTableWithHisto(refuser, c);
            }else{
                cl=liste[0];
            }
            this.setIdClient(cl.getId());
            if(c!=null && verif==1)c.commit();
        }catch(Exception e){
            if(c!=null && verif==1)c.rollback();
            throw e;
        }finally{
            if(c!=null && verif==1)c.close();
        }
        
    }

    
    public void effectuerReservation(String refuser,Connection c)throws Exception{
        int verif=0;
        try
        {
            if(c == null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            Reservation res=(Reservation)CGenUtil.rechercher(this, null, null, c, "")[0];
            if(res.getEtat()<ConstanteEtat.getEtatValider())throw new Exception("objet non vis�");
            res.setEtat(ConstanteEtat.getEffectuer());
           res.updateToTableWithHisto(refuser, c);
           if(verif==1)c.commit();
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            if(c!=null && verif==1)
            {
                c.close();
            }
        }
    }
    
}
