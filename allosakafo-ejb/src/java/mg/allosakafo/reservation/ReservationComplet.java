/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.reservation;

import bean.ClassEtat;
import java.sql.Date;

/**
 *
 * @author Antsa
 */
public class ReservationComplet  extends ClassEtat{
    
    private String id,idreservation , heuredebutreservation,heurefinreservation,idtable;
    private Date datereservation;
    private int nbpersonne;

    public ReservationComplet() {
          this.setNomTable("RESA_TABLE_LIBCOMPLET");
    }

    public ReservationComplet(String id, String idreservation, String heuredebutreservation, String heurefinreservation, String idtable, Date datereservation, int nbpersonne) {
         this.setNomTable("RESA_TABLE_LIBCOMPLET");
        this.setId(id);
        this.setIdreservation(idreservation);
        this.setHeuredebutreservation(heuredebutreservation);
        this.setHeurefinreservation(heurefinreservation);
        this.setIdtable(idtable);
        this.setDatereservation(datereservation);
        this.setNbpersonne(nbpersonne);
    }
    
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdreservation() {
        return idreservation;
    }

    public void setIdreservation(String idreservation) {
        this.idreservation = idreservation;
    }

    public String getHeuredebutreservation() {
        return heuredebutreservation;
    }

    public void setHeuredebutreservation(String heuredebutreservation) {
        this.heuredebutreservation = heuredebutreservation;
    }

    public String getHeurefinreservation() {
        return heurefinreservation;
    }

    public void setHeurefinreservation(String heurefinreservation) {
        this.heurefinreservation = heurefinreservation;
    }

    public String getIdtable() {
        return idtable;
    }

    public void setIdtable(String idtable) {
        this.idtable = idtable;
    }

    public Date getDatereservation() {
        return datereservation;
    }

    public void setDatereservation(Date datereservation) {
        this.datereservation = datereservation;
    }

    public int getNbpersonne() {
        return nbpersonne;
    }

    public void setNbpersonne(int nbpersonne) {
        this.nbpersonne = nbpersonne;
    }
    
    
    
    @Override
    public String getTuppleID() {
      return id;
    }

    @Override
    public String getAttributIDName() {
      return "id";
    }
    
}
