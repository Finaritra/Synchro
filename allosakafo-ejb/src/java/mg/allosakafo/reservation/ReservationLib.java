/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.reservation;

/**
 *
 * @author rajao
 */
public class ReservationLib extends Reservation{
    private String telephone,tablenom,idpointlibelle;;
    
    public ReservationLib()
    {
        this.setNomTable("ReservationLibTous");
    }
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTablenom() {
        return tablenom;
    }

    public void setTablenom(String tablenom) {
        this.tablenom = tablenom;
    }
    
    public String getIdpointlibelle() {
        return idpointlibelle;
    }

    public void setIdpointlibelle(String idpointlibelle) {
        this.idpointlibelle = idpointlibelle;
    }
}
