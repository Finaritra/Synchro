/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author Joe
 */
public class Livraison extends ClassEtat{
    
    private String id, commande, adresse, secteur, responsable, cuisinier, description;
    private String heure;
    private double distance, carburant;
    private Date daty;

	
    public Livraison() {
        this.setNomTable("as_livraison");
    }
    
    public boolean isSynchro()
    {
        return true;
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("ASL", "getSeqASLivraison");
        this.setId(makePK(c));
    }
    
	public double getCarburant() {
        return carburant;
    }

    public void setCarburant(double carburant) {
        this.carburant = carburant;
    }
	
    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (heure != null && heure.compareToIgnoreCase("") != 0) {
                this.heure = heure;
                return;
            } else {
                throw new Exception("Heure de livraison manquant");
            }
        }
        this.heure = heure;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommande() {
        return commande;
    }

    public void setCommande(String commande) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (commande != null && commande.compareToIgnoreCase("") != 0) {
                this.commande = commande;
                return;
            } else {
                throw new Exception("Numero commande manquant");
            }
        }
        this.commande = commande;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (adresse != null && adresse.compareToIgnoreCase("") != 0) {
                this.adresse = adresse;
                return;
            } else {
                throw new Exception("Adresse de livraison manquant");
            }
        }
        this.adresse = adresse;
    }

    public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (responsable != null && responsable.compareToIgnoreCase("") != 0) {
                this.responsable = responsable;
                return;
            } else {
                throw new Exception("Champ responsable manquant");
            }
        }
        this.responsable = responsable;
    }

    public String getCuisinier() {
        return cuisinier;
    }

    public void setCuisinier(String cuisinier) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (cuisinier != null && cuisinier.compareToIgnoreCase("") != 0) {
                this.cuisinier = cuisinier;
                return;
            } else {
                throw new Exception("Champ coursier manquant");
            }
        }
        this.cuisinier = cuisinier;
    }
	//description
	public String getDescription() {
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }
}
