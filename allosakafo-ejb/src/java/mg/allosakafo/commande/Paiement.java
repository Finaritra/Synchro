/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author Joe
 */
public class Paiement extends ClassMAPTable{
    
    private String id, modepaiement, idobjet, observation, client, numpiece, caisse;
    private Date daty;
    private double montant, tva;

    public String getClient() {
        return client;
    }

    public String getNumpiece() {
        return numpiece;
    }

    public void setNumpiece(String numpiece) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (numpiece != null && numpiece.compareToIgnoreCase("") != 0) {
                this.numpiece = numpiece;
                return;
            } else {
                throw new Exception("Pi�ce de paiement obligatoire");
            }
        }
        this.numpiece = numpiece;
    }

    public void setClient(String client) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (client != null && client.compareToIgnoreCase("") != 0) {
                this.client = client;
                return;
            } else {
                throw new Exception("Champ client manquant");
            }
        }
        this.client = client;
    }

    public double getTva() {
        return tva;
    }

    public void setTva(double tva) {
        this.tva = tva;
    }

    public Paiement() {
        this.setNomTable("as_paiement");
    }
    
    public void construirePK(Connection c) throws Exception {
        this.preparePk("PAS", "getSeqASPaiement");
        this.setId(makePK(c));
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModepaiement() {
        return modepaiement;
    }

    public void setModepaiement(String modepaiement) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (modepaiement != null && modepaiement.compareToIgnoreCase("") != 0) {
                this.modepaiement = modepaiement;
                return;
            } else {
                throw new Exception("Champ mode de paiement manquant");
            }
        }
        this.modepaiement = modepaiement;
    }

    public String getIdobjet() {
        return idobjet;
    }

    public void setIdobjet(String idobjet) {
        this.idobjet = idobjet;
    }

	public String getCaisse() {
        return caisse;
    }

    public void setCaisse(String caisse) {
        this.caisse = caisse;
    }
	
    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (montant > 0) {
                this.montant = montant;
                return;
            } else {
                throw new Exception("Montant n�gatif ou null");
            }
        }
        this.montant = montant;
    }
    
    
}
