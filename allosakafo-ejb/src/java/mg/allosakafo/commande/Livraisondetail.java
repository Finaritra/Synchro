/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassMAPTable;
import java.sql.Date;
import utilitaire.Constante;

/**
 *
 * @author pro
 */
public class Livraisondetail extends ClassMAPTable{

    private String id, nomtable, produit, restaurant,restaurantlib, sauceAcoompagnement, heureCommande, heureLivraison,heureCommandeReel;
    private Date dateCommande, dateLivraison;
    private double difference;

    public Livraisondetail() {
        super.setNomTable("livraisondetail");
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String getSauceAcoompagnement() {
        return sauceAcoompagnement;
    }

    public void setSauceAcoompagnement(String sauceAcoompagnement) {
        this.sauceAcoompagnement = sauceAcoompagnement;
    }

    public String getHeureCommande() {
        //return utilitaire.Utilitaire.ajoutHeure(heureCommande, 0, -Constante.attenteLivraison, 0);
        return heureCommande;
    }
    public String getHeureCommandeReel() throws Exception
    {
        //System.out.println("Attente livraison "+Constante.attenteLivraison);
        //return utilitaire.Utilitaire.ajoutHeure(getHeureCommande(), 0, -Constante.attenteLivraison, 0);
        return heureCommandeReel;
    }
    public void setHeureCommandeReel(String g) throws Exception
    {
        heureCommandeReel=g;
    }

    public void setHeureCommande(String heureCommande) {
        this.heureCommande = heureCommande;
    }

    public String getNomtable() {
        return nomtable;
    }

    public void setNomtable(String nomtable) {
        this.nomtable = nomtable;
    }

    public String getHeureLivraison() {
        return heureLivraison;
    }

    public void setHeureLivraison(String heureLivraison) {
        this.heureLivraison = heureLivraison;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public Date getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(Date dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public String getRestaurantlib() {
        return restaurantlib;
    }

    public void setRestaurantlib(String restaurantlib) {
        this.restaurantlib = restaurantlib;
    }
    
    public double getDifference() {
        return difference+Constante.attenteLivraison ;
    }

    public void setDifference(double difference) {
        this.difference = difference;
    }
    
    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
    
}
