/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassMAPTable;
import java.sql.Connection;
import com.google.gson.annotations.Expose;

/**
 *
 * @author Joe
 */
public class CommandeMobileDetails extends ClassMAPTable{
    @Expose
    private String id, idmere, produit, observation;
    @Expose
    private double quantite, pu, remise;

    public CommandeMobileDetails() {
        this.setNomTable("AS_DETAILSCOMMANDEMOBILE");
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("DCMM", "GETSEQ_as_detailscmdmobile");
        this.setId(makePK(c));
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdmere() {
        return idmere;
    }

    public void setIdmere(String idmere) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (idmere != null && idmere.compareToIgnoreCase("") != 0) {
                this.idmere = idmere;
                return;
            } else {
                throw new Exception("Commande inexistant");
            }
        }
        this.idmere = idmere;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (produit != null && produit.compareToIgnoreCase("") != 0) {
                this.produit = produit;
                return;
            } else {
                throw new Exception("Produit introuvable");
            }
        }
        this.produit = produit;
    }

    

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public double getQuantite() {
        return quantite;
    } 

    public void setQuantite(double quantite) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (quantite >= 0) {
                this.quantite = quantite;
                return;
            } else {
                throw new Exception("Quantit� n�gatif ou null");
            }
        }
        this.quantite = quantite;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (pu >= 0) {
                this.pu = pu;
                return;
            } else {
                throw new Exception("PU n�gatif ou null");
            }
        }
        this.pu = pu;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }
    
	public double getMontant(){
		double montantRemise = ((getQuantite() * getPu()) * getRemise()) / 100;
		return (getQuantite() * getPu()) - montantRemise ; 
	}
    
	public Object clone(){
		CommandeMobileDetails det = new CommandeMobileDetails();
		try{
			det.setId(this.getId());
			det.setIdmere(this.getIdmere());
			det.setProduit(this.getProduit());
			det.setObservation(this.getObservation());
			det.setQuantite(this.getQuantite());
		}
		catch(Exception ex){
		
		}
		return det;	
	}
}
