/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.AdminGen;
import bean.CGenUtil;
import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.Utilitaire;

import com.google.gson.annotations.Expose;
import mg.allosakafo.fin.MvtCaisse;
import mg.allosakafo.livraison.CommandeLivraison;
import mg.allosakafo.reservation.Reservation;
import service.AlloSakafoService;
import utilitaire.Constante;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;

/**
 *
 * @author Joe
 */
public class CommandeClient extends ClassEtat {

    @Expose
    private String id, client, responsable, typecommande, remarque, numcommande, secteur, observation, quartier, prenom;
    @Expose
    private Date datesaisie, datecommande;

    private String adresseliv;
    private String heureliv;
    private double distance;
    private Date dateliv;
    private String vente;
    private String point;

    public CommandeClient(String id, String client, String responsable, String typecommande, String remarque, String numcommande, String secteur, String observation, String quartier, String prenom, Date datesaisie, Date datecommande, String adresseliv, String heureliv, double distance, Date dateliv, String vente, String point) {
        this.id = id;
        this.client = client;
        this.responsable = responsable;
        this.typecommande = typecommande;
        this.remarque = remarque;
        this.numcommande = numcommande;
        this.secteur = secteur;
        this.observation = observation;
        this.quartier = quartier;
        this.prenom = prenom;
        this.datesaisie = datesaisie;
        this.datecommande = datecommande;
        this.adresseliv = adresseliv;
        this.heureliv = heureliv;
        this.distance = distance;
        this.dateliv = dateliv;
        this.vente = vente;
        this.point = point;
    }

    public CommandeClient(String id, String client, String responsable, String typecommande, String remarque, String numcommande, String secteur, String observation, String quartier, String prenom, Date datesaisie, Date datecommande, String adresseliv, String heureliv, double distance, Date dateliv, String vente, String point, int etat) {
        this.id = id;
        this.client = client;
        this.responsable = responsable;
        this.typecommande = typecommande;
        this.remarque = remarque;
        this.numcommande = numcommande;
        this.secteur = secteur;
        this.observation = observation;
        this.quartier = quartier;
        this.prenom = prenom;
        this.datesaisie = datesaisie;
        this.datecommande = datecommande;
        this.adresseliv = adresseliv;
        this.heureliv = heureliv;
        this.distance = distance;
        this.dateliv = dateliv;
        this.vente = vente;
        this.point = point;
        this.setEtat(etat);
    }

    
    public boolean isSynchro()
    {
        return true;
    }

    @Override
    public Object cloturerObject(String u, Connection c) throws Exception {
        CommandeClient[] commande;
        commande = (CommandeClient[]) CGenUtil.rechercher(this, null, null, c, " AND ID = '" + this.getId() + "'");

        CommandeClientDetails[] listeDetails = (CommandeClientDetails[]) CGenUtil.rechercher(new CommandeClientDetails(), null, null, c, " AND idMere = '" + commande[0].getId() + "'");
        for (int i = 0; i < listeDetails.length; i++) {
            listeDetails[i].setEtat(ConstanteEtat.getEtatCloture());
            listeDetails[i].updateToTableWithHisto(u, c);
        }
        commande[0].setEtat(ConstanteEtat.getEtatCloture());
        commande[0].updateToTableWithHisto(u, c);
        
        return commande[0];
    }

    public void verifPoint()throws Exception{
        if(this.getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0){
            throw new Exception("Action non permise");
        }
    }
    
    public void controlerUpdate(Connection c) throws Exception {
        if(this.getEtat() >= ConstanteEtat.getEtatACloturer()){
            throw new Exception("Les commandes cl�tur�es ne peuvent plus �tre modifi�es");
        }
        
        if(this.getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0){
            throw new Exception("Action non permise");
        }
    }

    public boolean estLivraison(Connection c)throws Exception
    {
        CommandeLivraison cl=new CommandeLivraison();
        cl.setIdclient(this.getId());
        CommandeLivraison[] listeLivr=(CommandeLivraison[])CGenUtil.rechercher(cl, null, null, c, "");
        if(listeLivr.length>0)return true;
        return false;
    }
    public Reservation getReservation(String nomT,Connection c) throws Exception
    {
        Reservation crt=new Reservation();
        if(nomT!=null&&nomT.compareToIgnoreCase("")!=0)crt.setNomTable(nomT);
        crt.setId(this.getTypecommande());
        Reservation[] valiny=(Reservation[])CGenUtil.rechercher(crt, null, null, c, "");
        if (valiny.length==0) return null;
        return valiny[0];
    }
    public boolean estReservation()throws Exception
    {
        if(this.getTypecommande().startsWith("RES")) return true;
        return false;
    }
    public void changerTable(String table, Connection c) throws Exception
    {
        
    }
    public String getNumcommande() {
        return numcommande;
    }

    public void setNumcommande(String numcommande) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (numcommande != null && numcommande.compareToIgnoreCase("") != 0) {
                this.numcommande = numcommande;
                return;
            } else {
                //throw new Exception("Numero commande manquant");
            }
        }
        this.numcommande = numcommande;
    }

    public CommandeClient() {
        this.setNomTable("as_commandeclient");
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("CMD", "getSeqASCommande");
        this.setId(makePK(c));
    }

    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getAdresseliv() {
        return adresseliv;
    }
    public boolean decalerLivraison(String heureEnCours,int dureeMin) throws Exception
    {
        String heureFin=Utilitaire.ajoutHeure(heureEnCours, 0, dureeMin, 0);
        if(Utilitaire.comparerHeure(this.getHeureliv(), heureFin)>0&&Utilitaire.diffJourDaty(this.getDatecommande(), Utilitaire.dateDuJourSql())==0)
        {
            this.setHeureliv(heureFin);
            return true;
        }
        return false;
    }

    public void setAdresseliv(String adresseliv) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (adresseliv != null && adresseliv.compareToIgnoreCase("") != 0) {
                this.adresseliv = adresseliv;
                return;
            } else {
                //throw new Exception("Adresse de livraison manquant");
            }
        }
        this.adresseliv = adresseliv;
    }

    public String getHeureliv() {
        return heureliv;
    }

    public void setHeureliv(String heureliv) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (heureliv != null && heureliv.compareToIgnoreCase("") != 0) {
                this.heureliv = heureliv;
                return;
            } else {
                //throw new Exception("Heure de livraison manquant");
            }
        }
        this.heureliv = heureliv;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Date getDateliv() {
        return dateliv;
    }

    public void setDateliv(Date dateliv) throws Exception{
        if (getMode().compareTo("modif") == 0) {
            if (dateliv == null) {
                //throw new Exception("Date de livraison obligatoire");
            } else {
                this.dateliv = dateliv;
            }
            return;
        }
        this.dateliv = dateliv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClient() {	
        return client;
    }

    public void setClient(String client) throws Exception{
        if(!this.getMode().equals("select")){
            if (client == null || client.compareToIgnoreCase("") == 0) {
                throw new Exception("Champ client manquant");
            }
        }
        this.client = client;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (responsable != null && responsable.compareToIgnoreCase("") != 0) {
                this.responsable = responsable;
                return;
            } else {
               // throw new Exception("Responsable manquant");
            } 
        }
        this.responsable = responsable;
    }

    public String getTypecommande() {
        return typecommande;
    }

    public void setTypecommandeAuto(String typecommande) {
        if(typecommande!=null&&typecommande.startsWith("RES"))
        {
            this.typecommande=typecommande;
            return;
        }
        if(getClient()!=null&&getClient().startsWith(Constante.marquageTableClient)==false)
        {
            this.typecommande=Constante.idTypeComandeLivr;
            return;
        }
        this.typecommande = Constante.idTypeComandeNormal;
    }
    public void setTypecommande(String typecommande) {
        this.typecommande = typecommande;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

	public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }
	
    public Date getDatesaisie() {
        return datesaisie;
    }

    public void setDatesaisie(Date datesaisie) throws Exception{
        if (getMode().compareTo("modif") == 0) {
            if (datesaisie == null) {
                throw new Exception("Date de saisie obligatoire");
            } else {
                this.datesaisie = datesaisie;
            }
            return;
        }
        this.datesaisie = datesaisie;
    }

    public Date getDatecommande() {
        return datecommande;
    }

    public void setDatecommande(Date datecommande) throws Exception{
        if (getMode().compareTo("modif") == 0) {
            if (datecommande == null||Utilitaire.diffJourDaty(Utilitaire.dateDuJourSql(), datecommande)>0) {
                this.datecommande=Utilitaire.dateDuJourSql();
            } 
            else {
                this.datecommande = datecommande;
            }
            return;
        }
        this.datecommande = datecommande;
    }
	
	public String getHeureCuisine() throws Exception{
        // -30min
		String[] str_heure = Utilitaire.split(this.getHeureliv(), ":");
		int heure = Integer.parseInt(str_heure[0]);
		int minutes = Integer.parseInt(str_heure[1]);
		
		if (minutes >= 30) minutes -= 30;
		else{
			minutes = 30 + minutes ;
			heure -= 1;
		}
		
		if (heure < 0){
			heure += 24;
		}
		
		return Utilitaire.transformeHeure(heure+":"+minutes);
    }
	
	 public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
	 public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) throws Exception{
        this.quartier = quartier;
    }

    /**
     * @return the vente
     */
    public String getVente() {
        return vente;
    }

    /**
     * @param vente the vente to set
     */
    public void setVente(String vente) {
        this.vente = vente;
    }

    /**
     * @return the point
     */
    public String getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(String point) {
        this.point = point;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public CommandeLivraison getCl(Connection c)throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                estOuvert=true;
                c=new UtilDB().GetConn();
            }
            CommandeLivraison crt=new CommandeLivraison();
            crt.setNomTable("COMMANDELIVRAISONLIB");
            crt.setIdclient(this.getId());
            CommandeLivraison[] liste=(CommandeLivraison[]) CGenUtil.rechercher(crt, null, null, c, "");
            if(liste.length==0) 
            {
                CommandeLivraison cmd=new CommandeLivraison("", "", "", "12:00", Utilitaire.dateDuJour() , "0");
                return cmd; 
            }
            return liste[0];
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            if(estOuvert==true&&c!=null)c.close();
        }
    }
    public MvtCaisse[]getMvtCaisse(Connection c, String nomTable)throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                estOuvert=true;
                c=new UtilDB().GetConn();
            }
            if(nomTable==null || nomTable.compareToIgnoreCase("")==0)nomTable="mvtCaisseLettreComandeMere";
            MvtCaisse crt=new MvtCaisse();
            crt.setNomTable(nomTable);
            crt.setNumpiece(this.getId());
            MvtCaisse[] listeMvt=(MvtCaisse[])CGenUtil.rechercher(crt, null, null,c, "");
            if(listeMvt.length==0)return null;
            if(estOuvert==true)c.commit();
            return listeMvt;
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            if(estOuvert==true&&c!=null)c.close();
        }
    }
    public int payerApayer(String idCaisse,String user,Connection c) throws Exception
    {
        MvtCaisse[]liste=getMvtCaisse(c,null);
        if(liste==null)return 0;
        if(idCaisse==null||idCaisse.compareToIgnoreCase("")==0)idCaisse=Constante.caisseDefaut;
        for(MvtCaisse mvt:liste)
        {
            if(mvt.getIdcaisse()!=null&&mvt.getIdcaisse().compareToIgnoreCase(Constante.idCaisseApayer)!=0)throw new Exception("Payement deja effectue");
            mvt.setMode("modif");
            mvt.setIdcaisse(idCaisse);
            mvt.setDaty(Utilitaire.dateDuJourSql());
            mvt.setDatyvaleur(Utilitaire.dateDuJourSql());
            mvt.setNomTable("mvtCaisse");
            mvt.updateToTableWithHisto(user,c);
        }
        return liste.length;
     
    }
    public String[]getIdDetailsCommandeSansLivraison() throws Exception
    {
        CommandeClientDetails[] valiny=getDetailsCommandeClientSansLivraison(null);
        return AdminGen.grouperDistinctString(valiny, "id") ;
    }
    
    public CommandeClientDetails[] getDetailsCommandeClient(String nomTable) throws Exception
    {
        if(nomTable==null || nomTable.compareToIgnoreCase("")==0)nomTable="as_detailscommande";
        CommandeClientDetails crt=new CommandeClientDetails();
        crt.setNomTable(nomTable);
        crt.setIdmere(this.getId());
        CommandeClientDetails[] retour=(CommandeClientDetails[])CGenUtil.rechercher(crt, null,null,"") ;
        
        return retour;
    }
    public CommandeClientDetails[] getDetailsCommandeClient(String nomTable,Connection c) throws Exception
    {
        if(nomTable==null || nomTable.compareToIgnoreCase("")==0)nomTable="as_detailscommande";
        CommandeClientDetails crt=new CommandeClientDetails();
        crt.setNomTable(nomTable);
        crt.setIdmere(this.getId());
        CommandeClientDetails[] retour=(CommandeClientDetails[])CGenUtil.rechercher(crt, null,null,c,"") ;        
        return retour;
    }
    public CommandeClientDetails[] getDetailsCommandeClientSansLivraison(String nomTable) throws Exception
    {
        if(nomTable==null || nomTable.compareToIgnoreCase("")==0)nomTable="as_detailscommande";
        CommandeClientDetails crt=new CommandeClientDetails();
        crt.setNomTable(nomTable);
        crt.setIdmere(this.getId());
        CommandeClientDetails[] retour=(CommandeClientDetails[])CGenUtil.rechercher(crt, null,null," and (etat="+ConstanteEtat.getEtatALivrer()+" or (produit like 'LIVR%' and etat<"+ConstanteEtat.getLivrer() +" and etat>"+ConstanteEtat.getEtatACloturer()+") )") ;
        return retour;
    }
    public void changerClient(String nT,Connection c) throws Exception
    {
        try
        {
            this.setClient(nT);
            this.updateToTable(c);
        }
        catch(Exception e)
        {
            c.rollback();
        }
    }
    public static CommandeClientDetails[] getCommandeEnCours(String idTableClient,Connection c)throws Exception
    {
        return null;
    }
}
