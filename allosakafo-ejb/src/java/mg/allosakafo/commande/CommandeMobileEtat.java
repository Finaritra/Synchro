/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassMAPTable;
import java.sql.Date;
import utilitaire.ConstanteEtat;
import utilitaire.Utilitaire;

/**
 *
 * @author Tsiry
 */
public class CommandeMobileEtat extends ClassMAPTable {

    private String id, tables, responsable, typecommande, remarque, numcommande;
    private Date datesaisie, datecommande,dateliv;
    int etat;

    
    
    
    public String getNumcommande() {
        return numcommande;
    }

    public void setNumcommande(String numcommande) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (numcommande != null && numcommande.compareToIgnoreCase("") != 0) {
                this.numcommande = numcommande;
                return;
            } else {
                throw new Exception("Numero commande manquant");
            }
        }
        this.numcommande = numcommande;
    }

    public CommandeMobileEtat() {
        this.setNomTable("AS_COMMANDEMOBILE_LIBELLE2");
    }

    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public Date getDateliv() {
        return dateliv;
    }

    public void setDateliv(Date dateliv) throws Exception {
        if (getMode().compareTo("modif") == 0) {
            if (dateliv == null) {
                throw new Exception("Date de livraison obligatoire");
            } else {
                this.dateliv = dateliv;
            }
            return;
        }
        this.dateliv = dateliv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTables() {
        return tables;
    }

    public void setTables(String tables) throws Exception {
        if( tables == null || tables.compareToIgnoreCase("") == 0  ){
            throw new Exception("Champ tables manquant");
        }else{
            this.tables = tables;
        }
    }    

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (responsable != null && responsable.compareToIgnoreCase("") != 0) {
                this.responsable = responsable;
                return;
            } else {
                throw new Exception("Responsable manquant");
            }
        }
        this.responsable = responsable;
    }

    public String getTypecommande() {
        return typecommande;
    }

    public void setTypecommande(String typecommande) {
        this.typecommande = typecommande;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }


    public Date getDatesaisie() {
        return datesaisie;
    }

    public void setDatesaisie(Date datesaisie) throws Exception {
        if (getMode().compareTo("modif") == 0) {
            if (datesaisie == null) {
                throw new Exception("Date de saisie obligatoire");
            } else {
                this.datesaisie = datesaisie;
            }
            return;
        }
        this.datesaisie = datesaisie;
    }

    public Date getDatecommande() {
        return datecommande;
    }

    public void setDatecommande(Date datecommande) throws Exception {
        if (getMode().compareTo("modif") == 0) {
            if (datecommande == null) {
                throw new Exception("Date commande obligatoire");
            } else {
                this.datecommande = datecommande;
            }
            return;
        }
        this.datecommande = datecommande;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getEtat() {
        return ConstanteEtat.etatToChaine(String.valueOf(etat));
    }


}
