/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

/**
 *
 * @author Jessi
 */
public class CommandeClientDetailsLibelleEtat extends CommandeClientDetailsLibelle {
    private String statue;

    public String getStatue() {
        return statue;
    }

    public void setStatue(String status) {
        this.statue = status;
    }

    public CommandeClientDetailsLibelleEtat() {
        this.setNomTable("AS_DETAILS_COMMANDE_DESC_ETAT");
    }
    
}
