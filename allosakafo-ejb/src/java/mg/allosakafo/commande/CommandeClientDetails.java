/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.CGenUtil;
import bean.ClassMAPTable;
import java.sql.Connection;
import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import mg.allosakafo.fin.FactureClient;
import mg.allosakafo.fin.MvtCaisse;
import mg.allosakafo.produits.Produits;
import mg.allosakafo.produits.ProduitsTypeLibelle;
import mg.allosakafo.produits.Recette;
import service.AlloSakafoService;
import user.UserEJB;
import utilitaire.Constante;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Joe
 */
public class CommandeClientDetails extends ClassMAPTable{
    @Expose
    private String id, idmere, produit, observation,idAccompagnementSauce,typeCommande;

    @Expose
    private double quantite, pu, remise,revient;
    @Expose
    private int etat, prioriter;

    public String getTypeCommande() {
        return typeCommande;
    }

    public void setTypeCommande(String typeCommande) {
        this.typeCommande = typeCommande;
    }
    
    public double getRevient() {
        return revient;
    }

    public void setRevient(double revient) {
        this.revient = revient;
    }

    public String getIdAccompagnementSauce() {
        return idAccompagnementSauce;
    }

    public void setIdAccompagnementSauce(String idAccompagnementSauce) {
        this.idAccompagnementSauce = idAccompagnementSauce;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public boolean isSynchro()
    {
        return true;
    }

    public CommandeClientDetails() {
        this.setNomTable("as_detailscommande");
    }
     public CommandeClientDetails(String id, String idmere, String produit, String observation, String idAccompagnementSauce, double quantite, double pu, double remise, int etat, int prioriter,double revient) throws Exception {  
        this.setNomTable("as_detailscommande");
        this.setId(id);
        this.setIdmere(idmere);
        this.setProduit(produit);
        this.setObservation(observation);
        this.setIdAccompagnementSauce(idAccompagnementSauce);
        this.setQuantite(quantite);
        this.setPu(pu);
        this.setRemise(remise);
        this.setEtat(etat);
        this.setPrioriter(prioriter);
        this.setRevient(revient);
    }
    public CommandeClientDetails(String id, String idmere, String produit, String observation, String idAccompagnementSauce, double quantite, double pu, double remise, int etat, int prioriter) throws Exception {
        this.setNomTable("as_detailscommande");
        this.setId(id);
        this.setIdmere(idmere);
        this.setProduit(produit);
        this.setObservation(observation);
        this.setIdAccompagnementSauce(idAccompagnementSauce);
        this.setQuantite(quantite);
        this.setPu(pu);
        this.setRemise(remise);
        this.setEtat(etat);
        this.setPrioriter(prioriter);
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("DCM", "getSeqASDetailsCommande");
        this.setId(makePK(c));
    }

    @Override
    public void controlerUpdate(Connection c) throws Exception {
        CommandeClient cc = new CommandeClient();
        String aWhere = " and id like '"+this.getIdmere()+"'";
        CommandeClient[] commande = (CommandeClient[])CGenUtil.rechercher(new CommandeClient(), null, null, c,aWhere);
        if(commande.length > 0){
            if(commande[0].getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0){
                throw new Exception("ACTION NON PERMISE");
            }
        }
        
        if(this.getEtat() >= ConstanteEtat.getCloture()){
            throw new Exception("Modification non autoris�e");
        }
    }
    
    
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdmere() {
        return idmere;
    }

    public void setIdmere(String idmere) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (idmere != null && idmere.compareToIgnoreCase("") != 0) {
                this.idmere = idmere;
                return;
            } else {
                throw new Exception("Commande inexistant");
            }
        }
        this.idmere = idmere;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (produit != null && produit.compareToIgnoreCase("") != 0) {
                this.produit = produit;
                return;
            } else {
                throw new Exception("Produit introuvable");
            }
        }
        this.produit = produit;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public double getQuantite() {
        return quantite;
    } 

    public void setQuantite(double quantite) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (quantite >= 0) {
                this.quantite = quantite;
                return;
            } else {
                throw new Exception("Quantit� n�gatif ou null");
            }
        }
        this.quantite = quantite;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (pu >= 0) {
                this.pu = pu;
                return;
            } else {
                throw new Exception("PU n�gatif ou null");
            }
        }
        this.pu = pu;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }
    
    public double getMontant(){
            double montantRemise = ((getQuantite() * getPu()) * getRemise()) / 100;
            return (getQuantite() * getPu()) ; 
    }

    public Object clone(){
            CommandeClientDetails det = new CommandeClientDetails();
            try{
                    det.setId(this.getId());
                    det.setIdmere(this.getIdmere());
                    det.setProduit(this.getProduit());
                    det.setObservation(this.getObservation());
                    det.setQuantite(this.getQuantite());
            }
            catch(Exception ex){

            }
            return det;	
    }

    public int getPrioriter() {
        return prioriter;
    }

    public void setPrioriter(int prioriter) {
        this.prioriter = prioriter;
    }
    public FactureClient[] getFactureClient(Connection c) throws Exception
    {
        FactureClient fact = new FactureClient();
        fact.setIdobjet(getId());
        return (FactureClient[])CGenUtil.rechercher(fact, null, null,c, "");
    }
    public static ProduitsTypeLibelle getBarquette(Connection c) throws Exception
    {
        ProduitsTypeLibelle crt=new ProduitsTypeLibelle();
        crt.setId(Constante.idBarquette);
        return ((ProduitsTypeLibelle[]) CGenUtil.rechercher(crt,null, null, c, ""))[0];
    }
    
    public CommandeClientDetails creerBarquette(String choixresto,Connection c) throws Exception
    {
        CommandeClientDetails p=new CommandeClientDetails();
        p.construirePK(c);
        p.setIdmere(this.getIdmere());
        p.setProduit(Constante.idBarquette);
        ProduitsTypeLibelle barq=getBarquette(c);
        double prix=barq.getPu();
        if(choixresto!=null && choixresto.contains("louunge"))prix=barq.getPrixl();
        p.setPu(prix);
        p.setQuantite(1);
        p.setObservation(this.getObservation());
        //p.setIdAccompagnementSauce(nu);
//                p.setObservation("");
        p.setEtat(1);
        //int priorite = Utilitaire.stringToInt(type_p.getDesce()) * 1000;
        //int priorite = prd.getPriorite() * 1000;

        //p.setPrioriter(priorite);
        return p;
    }
    public CommandeClient getMere(Connection c, String nomTable)throws Exception
    {
        if(nomTable==null||nomTable.compareToIgnoreCase("")==0)nomTable=new CommandeClient().getNomTable();
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            CommandeClient crt=new CommandeClient();
            crt.setNomTable(nomTable);
            crt.setId(this.getIdmere());
            CommandeClient[]ret=(CommandeClient[])CGenUtil.rechercher(crt, null, null, c, "");
            if(ret.length==0)throw new Exception("Commande mere non Existante");
            return ret[0];
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(c!=null&&estOuvert==true)c.close();
        }
    }
    public Recette[]getRecette(Connection c,String nT)throws Exception
    {
        if(nT==null||nT.compareToIgnoreCase("")==0)nT=new Recette().getNomTable();
        Recette p = new Recette();
        p.setNomTable(nT);
        return (Recette[]) CGenUtil.rechercher(p, null, null, c, " and idproduits = '" + this.getProduit() + "'");
    }
            
    public CommandeClientDetails creerBarquette(Connection c) throws Exception
    {
        return creerBarquette(null,null);
    }
    public MvtCaisse getMvtCaisse(Connection c) throws Exception
    {
        MvtCaisse crt=new MvtCaisse();
        crt.setIdordre(this.getId());
        MvtCaisse[] retour=(MvtCaisse[])CGenUtil.rechercher(crt, null, null, c, "");
        if(retour.length>0)return retour[0];
        return null;
    }
    public static void commandeVoarayCuisinierMultiple(String[] id, Connection c, UserEJB u) throws Exception{
        boolean estOuvert=false;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                c.setAutoCommit(false);
                estOuvert=true;
            }
            if(id == null){
                throw new Exception("Aucune commande selectionnee");
            }
            
            ArrayList<Object> mere = new CommandeService().setEtatDetailCommande(id,ConstanteEtat.getEtatVoarayCuisinier(),"Commande d�j� en cours", c);
            
            for(int i=0;i<mere.size();i++){
                CommandeClient clTemp = (CommandeClient) mere.get(i);
                if(clTemp.getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0) {
                    throw new Exception("Action non permise");
                }
            }

//            String id_str = Utilitaire.tabToString(id, "'", ",");
//            CommandeClientDetails[] liste=(CommandeClientDetails[]) CGenUtil.rechercher(new CommandeClientDetails(), null, null, c, " and id in (" + id_str + ")");
//            if(liste.length == 0){
//                throw new Exception("Commande detail introuvable");
//            }
//            for(int i=0; i<id.length; i++){
//                CommandeClientDetails dts = liste[i];
//                dts.setEtat(ConstanteEtat.getEtatVoarayCuisinier());
//                u.updateObject(dts, c);
//            }
            if(estOuvert)c.commit();
        }catch(Exception e){
            c.rollback();
            throw e;
        }finally{
            if(c!=null && estOuvert) c.close();
        }
    }
}
