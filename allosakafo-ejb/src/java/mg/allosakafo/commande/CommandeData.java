/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassMAPTable;
import com.google.gson.annotations.Expose;

/**
 *
 * @author HP
 */
public class CommandeData extends ClassMAPTable{
    @Expose
    private String id, tables,idcommande,produit;
    @Expose
    private int quantite;

    public CommandeData() {
        this.setNomTable("AS_COMMANDEDATA");
    }
    
    public String getTuppleID() {
        return getId();
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public String getTables() {
        return tables;
    }

    public String getIdcommande() {
        return idcommande;
    }

    public String getProduit() {
        return produit;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTables(String tables) {
        this.tables = tables;
    }

    public void setIdcommande(String idcommande) {
        this.idcommande = idcommande;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
}
