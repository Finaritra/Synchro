/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author Joe
 */
public class CommandeClientLibelle extends CommandeClient {

    private String coursier;
	private double montant;

    public CommandeClientLibelle() {
        this.setNomTable("as_commandeclient_libelle2");
    }

    public String getTuppleID() {
        return this.getId();
    }

    public String getAttributIDName() {
        return "id";
    }  
	
    public String getCoursier() {
        return coursier;
    }

    public void setCoursier(String coursier) {
        this.coursier = coursier;
    }

	public double getMontant(){
		return this.montant;
	}
	public void setMontant(double montant){
		this.montant = montant;
	}
}
