/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassMAPTable;
import java.sql.Date;
import utilitaire.ConstanteEtat;

/**
 *
 * @author Notiavina
 */
public class CommandeClientRestaurantEtat extends ClassMAPTable {
    private String id, client, responsable, typecommande, remarque, numcommande, secteur, restaurant, prenom;
    private Date datesaisie, datecommande;
    private String adresseliv;
    private String heureliv;
    private double distance,revient;
    String idClient;

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public double getRevient() {
        return revient;
    }

    public void setRevient(double revient) {
        this.revient = revient;
    }
    private Date dateliv;
	
    private int etat;
    private String coursier;
    private String vente;
    private double montant;
    
    public CommandeClientRestaurantEtat() {
        this.setNomTable("as_commandeclient_libelle2");
    }

    public String getId() {
        return id;
    }

    public String getClient() {
        return client;
    }

    public String getResponsable() {
        return responsable;
    }

    public String getTypecommande() {
        return typecommande;
    }

    public String getRemarque() {
        return remarque;
    }

    public String getNumcommande() {
        return numcommande;
    }

    public String getSecteur() {
        return secteur;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public Date getDatesaisie() {
        return datesaisie;
    }

    public Date getDatecommande() {
        return datecommande;
    }

    public String getAdresseliv() {
        return adresseliv;
    }

    public String getHeureliv() {
        return heureliv;
    }

    public double getDistance() {
        return distance;
    }

    public Date getDateliv() {
        return dateliv;
    }

    public String getEtat() {
        return ConstanteEtat.etatToChaine(String.valueOf(etat));
        //return etat;
    }

    public String getCoursier() {
        return coursier;
    }

    public String getVente() {
        return vente;
    }

    public double getMontant() {
        return montant;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public void setTypecommande(String typecommande) {
        this.typecommande = typecommande;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public void setNumcommande(String numcommande) {
        this.numcommande = numcommande;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public void setDatesaisie(Date datesaisie) {
        this.datesaisie = datesaisie;
    }

    public void setDatecommande(Date datecommande) {
        this.datecommande = datecommande;
    }

    public void setAdresseliv(String adresseliv) {
        this.adresseliv = adresseliv;
    }

    public void setHeureliv(String heureliv) {
        this.heureliv = heureliv;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setDateliv(Date dateliv) {
        this.dateliv = dateliv;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public void setCoursier(String coursier) {
        this.coursier = coursier;
    }

    public void setVente(String vente) {
        this.vente = vente;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
}
