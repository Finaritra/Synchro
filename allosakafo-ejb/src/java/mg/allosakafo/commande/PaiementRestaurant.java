/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

/**
 *
 * @author Notiavina
 */
public class PaiementRestaurant extends Paiement{
    private String restaurant;

    public PaiementRestaurant(){
        super.setNomTable("as_paiement_libelle_restaurant");
    }
    
    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }
}
