/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

/**
 *
 * @author pro
 */
public class CommandeClientDetailsTypePPriorite extends CommandeClientDetailsTypeP{
    
    private int prioriter;

    public int getPrioriter() {
        return prioriter;
    }

    public void setPrioriter(int prioriter) {
        this.prioriter = prioriter;
    }
    
    public CommandeClientDetailsTypePPriorite() {
        super.setNomTable("VUE_CMD_CLT_DTLS_TYPEP_CLO_PRT");
    }
    
}
