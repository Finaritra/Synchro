/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.Utilitaire;
import com.google.gson.annotations.Expose;

/**
 *
 * @author Tsiry
 */
public class CommandeMobile extends ClassEtat{
    @Expose
    private String id, tables, responsable, typecommande, remarque, numcommande , observation;
    @Expose
    private Date datesaisie, datecommande , dateliv;

    public String getNumcommande() {
        return numcommande;
    }
    public void setObservation(String observation) {
        this.observation = observation;
    }

    
    

    public void setNumcommande(String numcommande) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (numcommande != null && numcommande.compareToIgnoreCase("") != 0) {
                this.numcommande = numcommande;
                return;
            } else {
                throw new Exception("Numero commande manquant");
            }
        }
        this.numcommande = numcommande;
    }

    public CommandeMobile() {
        this.setNomTable("as_commandemobile");
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("CMDM", "GETSEQ_as_commandemobile");
        this.setId(makePK(c));
    }

    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTables() {	
        return tables;
    }

    public void setTables(String tables) throws Exception{
        this.tables = tables ;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (responsable != null && responsable.compareToIgnoreCase("") != 0) {
                this.responsable = responsable;
                return;
            } else {
                throw new Exception("Responsable manquant");
            } 
        }
        this.responsable = responsable;
    }

    public String getTypecommande() {
        return typecommande;
    }

    public void setTypecommande(String typecommande) {
        this.typecommande = typecommande;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }


    public Date getDatesaisie() {
        return datesaisie;
    }

    public void setDatesaisie(Date datesaisie) throws Exception{
        if (getMode().compareTo("modif") == 0) {
            if (datesaisie == null) {
                throw new Exception("Date de saisie obligatoire");
            } else {
                this.datesaisie = datesaisie;
            }
            return;
        }
        this.datesaisie = datesaisie;
    }

    public Date getDatecommande() {
        return datecommande;
    }

    public void setDatecommande(Date datecommande) throws Exception{
        if (getMode().compareTo("modif") == 0) {
            if (datecommande == null) {
                throw new Exception("Date commande obligatoire");
            } else {
                this.datecommande = datecommande;
            }
            return;
        }
        this.datecommande = datecommande;
    }
	
    public Date getDateliv() {
        return dateliv;
    }

    public void setDateliv(Date dateliv) throws Exception{
        this.dateliv = dateliv;
    }
	
	 public String getObservation() {
        return observation;
    }

 
         
}
