/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import java.sql.Date;

/**
 *
 * @author JESSI
 */
public class LivraisonCommandeClientDetailsTypeP2 extends CommandeClientDetailsTypeP2 {
    
    private String idcl;
    private Date datycl;
    private String telephone;
    private String heurecl;
    private String idmere;

    public String getIdmere() {
        return idmere;
    }

    public void setIdmere(String idmere) {
        this.idmere = idmere;
    }

    public LivraisonCommandeClientDetailsTypeP2() {
        super.setNomTable("VUE_CMD_LIV_DTLS_ACLOT");
    }

    public String getIdcl() {
        return idcl;
    }

    public void setIdcl(String idcl) {
        this.idcl = idcl;
    }

    public Date getDatycl() {
        return datycl;
    }

    public void setDatycl(Date datycl) {
        this.datycl = datycl;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getHeurecl() {
        return heurecl;
    }

    public void setHeurecl(String heurecl) {
        this.heurecl = heurecl;
    }
    
}
