/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import utilitaire.Utilitaire;

/**
 *
 * @author Jessi
 */
public class DetailsCommandePhotos extends CommandeClientDetails{
    private String photo;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
     public String[] getListePhoto(){
        String[]retour={""};
        if(this.photo==null)return retour;
        return Utilitaire.split(this.photo, ";");
    }
}
