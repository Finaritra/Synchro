/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassMAPTable;
import java.sql.Date;
import utilitaire.ConstanteEtat;

/**
 *
 * @author Joe
 */
public class AnalyseCommande extends ClassMAPTable {

    private String client, produit, remarque, heureliv, numcommande, adresseliv, etat, typeproduit, secteur;

    private Date dateliv, datecommande;
    private double quantite, pu, montant;

    public AnalyseCommande() {
        this.setNomTable("as_analysecommande_details");
    }

    public String getNumcommande() {
        return numcommande;
    }

    public void setNumcommande(String numcommande) {
        this.numcommande = numcommande;
    }

    public String getAdresseliv() {
        return adresseliv;
    }

    public void setAdresseliv(String adresseliv) {
        this.adresseliv = adresseliv;
    }

    public String getTuppleID() {
        return null;
    }

    public String getAttributIDName() {
        return "";
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getHeureliv() {
        return heureliv;
    }

    public void setHeureliv(String heureliv) {
        this.heureliv = heureliv;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public Date getDateliv() {
        return dateliv;
    }

    public void setDateliv(Date dateliv) {
        this.dateliv = dateliv;
    }

    public Date getDatecommande() {
        return datecommande;
    }

    public void setDatecommande(Date datecommande) {
        this.datecommande = datecommande;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public String getEtat() {
       return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getTypeproduit() {
        return typeproduit;
    }

    public void setTypeproduit(String typeproduit) {
        this.typeproduit = typeproduit;
    }

    public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }
}
