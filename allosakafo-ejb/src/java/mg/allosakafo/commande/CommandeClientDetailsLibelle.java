/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassMAPTable;
import java.util.Hashtable;
import utilitaire.ConstanteEtat;

/**
 *
 * @author Joe
 */
public class CommandeClientDetailsLibelle extends ClassMAPTable{
    private String id, idmere, produit, observation, idproduit,typeproduit;
    private double quantite, pu, remise,revient;

    public double getRevient() {
        return revient;
    }
    public double getRevient(String profil) {
        if(profil.compareToIgnoreCase("dg")!=0) return 0;
        return revient;
    }

    public void setRevient(double revient) {
        this.revient = revient;
    }
    private int etat;

    public String getTypeproduit() {
        return typeproduit;
    }

    public void setTypeproduit(String typeproduit) {
        this.typeproduit = typeproduit;
    }

    public CommandeClientDetailsLibelle() {
        this.setNomTable("as_details_commande");
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdmere() {
        return idmere;
    }

    public void setIdmere(String idmere) {
        this.idmere = idmere;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(String idproduit) {
        this.idproduit = idproduit;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public String getEtat() {
        return ConstanteEtat.etatToChaine(String.valueOf(etat));
    }
    public int getEtatChiffre()
    {
        return etat;
    }
    public void setEtat(int etat) {
        this.etat = etat;
    }
    public boolean estBoisson(){
        boolean retour=false;
        if(this.getTypeproduit().equals("TPD00001") && !this.getIdproduit().startsWith("PRDTBKTP") && !this.getIdproduit().startsWith("LIVR")){
            retour=true;
        }
        return retour;
    }
    
}
