/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.CGenUtil;
import java.sql.Connection;
import mg.allosakafo.produits.Recette;
import mg.allosakafo.stock.MvtStock;
import mg.allosakafo.stock.MvtStockFille;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author MEVA
 */
public class CommandeClientDetailsWithPoint extends CommandeClientDetails{
    private String client, point;

    public CommandeClientDetailsWithPoint() {
        this.setNomTable("CommandeClientDetailsWithPoint");
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }
    public MvtStock creerMvtStock(Connection c) throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            MvtStock mvtstocktemp = new MvtStock("0", "Sortie en stock commande", "", "", this.getId(), this.getMere(c, "").getDatecommande(), this.getPoint());
            mvtstocktemp.construirePK(c);
            Recette[] liste = this.getRecette(c, null);
            MvtStockFille[]lf=new MvtStockFille[liste.length];
            for (int i = 0; i < liste.length; i++) {
                lf[i] = new MvtStockFille(mvtstocktemp.getId(), liste[i].getIdingredients(), 0, liste[i].getQuantite() * this.getQuantite());
                lf[i].construirePK(c);
            }
            mvtstocktemp.setListeFille(lf);
            return mvtstocktemp;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(c!=null&&estOuvert==true)c.close();
        }
    }
    
    
    
}
