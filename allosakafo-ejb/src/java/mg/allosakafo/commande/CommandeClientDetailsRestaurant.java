/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

/**
 *
 * @author Notiavina
 */
public class CommandeClientDetailsRestaurant extends CommandeClientDetailsTypeP{
    private String restaurant,libelle,categorieIngredient;

    public String getCategorieIngredient() {
        return categorieIngredient;
    }

    public void setCategorieIngredient(String categorieIngredient) {
        this.categorieIngredient = categorieIngredient;
    }
    double revient,qteing;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getRevient() {
        return revient;
    }

    public void setRevient(double revient) {
        this.revient = revient;
    }

    public double getQteing() {
        return qteing;
    }

    public void setQteing(double qteing) {
        this.qteing = qteing;
    }
    
    

    public CommandeClientDetailsRestaurant(){
        super.setNomTable("vue_cmd_client_details_rest");
    }
    
    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }
}
