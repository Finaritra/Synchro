/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import com.google.gson.annotations.Expose;

/**
 *
 * @author Jessi
 */
public class CommandeClientTotal extends CommandeClient {
    @Expose
    private double total=0;

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public CommandeClientTotal() {
        this.setNomTable("as_commandeclient_total");
    }
    
}
