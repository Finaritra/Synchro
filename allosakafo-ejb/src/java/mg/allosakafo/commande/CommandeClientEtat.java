/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import utilitaire.Utilitaire;
import utilitaire.ConstanteEtat;

/**
 *
 * @author Joe
 */
public class CommandeClientEtat extends ClassMAPTable {

    private String id, client, responsable, typecommande, remarque, numcommande, secteur;
    private Date datesaisie, datecommande;
    private String adresseliv;
    private String heureliv;
    private double distance;
    private Date dateliv;
	
    private int etat;
    private String coursier;
    private String vente;
    private double montant;

    public String getNumcommande() {
        return numcommande;
    }

    public void setNumcommande(String numcommande) throws Exception {
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (numcommande != null && numcommande.compareToIgnoreCase("") != 0) {
                this.numcommande = numcommande;
                return;
            } else {
                throw new Exception("Numero commande manquant");
            }
        }
        this.numcommande = numcommande;
    }

    public CommandeClientEtat() {
        this.setNomTable("as_commandeclient_libelle2");
    }

    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getAdresseliv() {
        return adresseliv;
    }

    public void setAdresseliv(String adresseliv) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (adresseliv != null && adresseliv.compareToIgnoreCase("") != 0) {
                this.adresseliv = adresseliv;
                return;
            } else {
                throw new Exception("Adresse de livraison manquant");
            }
        }
        this.adresseliv = adresseliv;
    }

    public String getHeureliv() {
        return heureliv;
    }

    public void setHeureliv(String heureliv) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (heureliv != null && heureliv.compareToIgnoreCase("") != 0) {
                this.heureliv = heureliv;
                return;
            } else {
                throw new Exception("Heure de livraison manquant");
            }
        }
        this.heureliv = heureliv;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Date getDateliv() {
        return dateliv;
    }

    public void setDateliv(Date dateliv) throws Exception{
        if (getMode().compareTo("modif") == 0) {
            if (dateliv == null) {
                throw new Exception("Date de livraison obligatoire");
            } else {
                this.dateliv = dateliv;
            }
            return;
        }
        this.dateliv = dateliv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClient() {	
        return client;
    }

    public void setClient(String client) throws Exception{
        if(!this.getMode().equals("select")){
            if (client == null || client.compareToIgnoreCase("") == 0) {
                throw new Exception("Champ client manquant");
            }
        }
        this.client = client;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (responsable != null && responsable.compareToIgnoreCase("") != 0) {
                this.responsable = responsable;
                return;
            } else {
                throw new Exception("Responsable manquant");
            } 
        }
        this.responsable = responsable;
    }

    public String getTypecommande() {
        return typecommande;
    }

    public void setTypecommande(String typecommande) {
        this.typecommande = typecommande;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

	public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }
	
    public Date getDatesaisie() {
        return datesaisie;
    }

    public void setDatesaisie(Date datesaisie) throws Exception{
        if (getMode().compareTo("modif") == 0) {
            if (datesaisie == null) {
                throw new Exception("Date de saisie obligatoire");
            } else {
                this.datesaisie = datesaisie;
            }
            return;
        }
        this.datesaisie = datesaisie;
    }

    public Date getDatecommande() {
        return datecommande;
    }

    public void setDatecommande(Date datecommande) throws Exception{
        if (getMode().compareTo("modif") == 0) {
            if (datecommande == null) {
                throw new Exception("Date commande obligatoire");
            } else {
                this.datecommande = datecommande;
            }
            return;
        }
        this.datecommande = datecommande;
    }
	
	public String getHeureCuisine() throws Exception{
        // -30min
		String[] str_heure = Utilitaire.split(this.getHeureliv(), ":");
		int heure = Integer.parseInt(str_heure[0]);
		int minutes = Integer.parseInt(str_heure[1]);
		
		if (minutes >= 30) minutes -= 30;
		else{
			minutes = 30 + minutes ;
			heure -= 1;
		}
		
		if (heure < 0){
			heure += 24;
		}
		
		return Utilitaire.transformeHeure(heure+":"+minutes);
    }
	public String getCoursier() {
        return coursier;
    }

    public void setCoursier(String coursier) {
        this.coursier = coursier;
    }

	public double getMontant(){
		return this.montant;
	}
	public void setMontant(double montant){
		this.montant = montant;
	}
	
	public void setEtat(int etat){
		this.etat = etat;
	}
	
	public String getEtat(){
		return ConstanteEtat.etatToChaine(String.valueOf(etat));
	}	

    /**
     * @return the vente
     */
    public String getVente() {
        return vente;
    }

    /**
     * @param vente the vente to set
     */
    public void setVente(String vente) {
        this.vente = vente;
    }
}
