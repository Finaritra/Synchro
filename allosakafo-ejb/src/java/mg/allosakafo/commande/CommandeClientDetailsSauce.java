/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

/**
 *
 * @author MEVA
 */
public class CommandeClientDetailsSauce extends CommandeClientDetails{
    private String idaccompagnement;
    private String photo;

    public String getIdaccompagnement() {
        return idaccompagnement;
    }

    public void setIdaccompagnement(String idaccompagnement) {
        this.idaccompagnement = idaccompagnement;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
}
