/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import java.sql.Date;

/**
 *
 * @author ACER
 */
public class CommandeClientDetailsFM extends CommandeClientDetails{
    private String client, responsable, typecommande, remarque, numcommande, secteur, observation, quartier, prenom;
    private Date datesaisie, datecommande;

    private String adresseliv;
    private String heureliv;
    private double distance;
    private Date dateliv;
    private String vente;
    private String point;
    private int etatmere;

    public CommandeClientDetails getCommandeClientDetails() throws Exception{
        return new CommandeClientDetails(this.getId(),this.getIdmere(), this.getProduit(), this.getObservation(), this.getIdAccompagnementSauce(),
                this.getQuantite(), this.getPu(), this.getRemise(), this.getEtat(), this.getPrioriter(), this.getRevient());
    }
    
    public CommandeClient getCommandeClient() throws Exception{
        return new CommandeClient(this.getIdmere(), client, responsable, typecommande, remarque, numcommande, secteur, observation,
                quartier, prenom, datesaisie, datecommande, adresseliv, heureliv, distance, dateliv, vente, point, etatmere);
    }
    
    public CommandeClientDetailsFM(){
        this.setNomTable("AS_DETAILSCOMMANDE_FM");
    }
    
    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getTypecommande() {
        return typecommande;
    }

    public void setTypecommande(String typecommande) {
        this.typecommande = typecommande;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getNumcommande() {
        return numcommande;
    }

    public void setNumcommande(String numcommande) {
        this.numcommande = numcommande;
    }

    public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDatesaisie() {
        return datesaisie;
    }

    public void setDatesaisie(Date datesaisie) {
        this.datesaisie = datesaisie;
    }

    public Date getDatecommande() {
        return datecommande;
    }

    public void setDatecommande(Date datecommande) {
        this.datecommande = datecommande;
    }

    public String getAdresseliv() {
        return adresseliv;
    }

    public void setAdresseliv(String adresseliv) {
        this.adresseliv = adresseliv;
    }

    public String getHeureliv() {
        return heureliv;
    }

    public void setHeureliv(String heureliv) {
        this.heureliv = heureliv;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Date getDateliv() {
        return dateliv;
    }

    public void setDateliv(Date dateliv) {
        this.dateliv = dateliv;
    }

    public String getVente() {
        return vente;
    }

    public void setVente(String vente) {
        this.vente = vente;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public int getEtatmere() {
        return etatmere;
    }

    public void setEtatmere(int etatmere) {
        this.etatmere = etatmere;
    }
    
    
}
