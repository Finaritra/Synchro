/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.ClassEtat;
import bean.ClassMAPTable;
import java.sql.Date;
import utilitaire.ConstanteEtat;

/**
 *
 * @author Notiavina
 */
public class CommandeClientDetailsTypeP2 extends ClassMAPTable{
    private String id, nomtable, typeproduit, produit, heureliv, acco_sauce, observation, restaurant,responsable;
    private Date datecommande;
    private double quantite, pu, montant;
    private int etat;
    
    
    private int prioriter;

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public int getPrioriter() {
        return prioriter;
    }

    public void setPrioriter(int prioriter) {
        this.prioriter = prioriter;
    }
    
    
    public CommandeClientDetailsTypeP2() {
        super.setNomTable("VUE_CMD_DTLS_TYPEP_CLOT_PRIO2");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNomtable() {
        return nomtable;
    }

    public void setNomtable(String nomtable) {
        this.nomtable = nomtable;
    }

    public String getTypeproduit() {
        return typeproduit;
    }

    public void setTypeproduit(String typeproduit) {
        this.typeproduit = typeproduit;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String getHeureliv() {
        return heureliv;
    }

    public void setHeureliv(String heureliv) {
        this.heureliv = heureliv;
    }

    public Date getDatecommande() {
        return datecommande;
    }

    public void setDatecommande(Date datecommande) {
        this.datecommande = datecommande;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }
    
    public void setEtat(int etat) {
        this.etat = etat;
    }
    
    public String getEtat() {
        return ConstanteEtat.etatToChaine(String.valueOf(etat));
    }

    public String getAcco_sauce() {
        return acco_sauce;
    }

    public void setAcco_sauce(String acco_sauce) {
        this.acco_sauce = acco_sauce;
    }
    
    @Override
    public String getTuppleID() {
        return id;
    }
    public String getHeureCommande()throws Exception
    {
        int duree=-utilitaire.Constante.attenteLivraison;
        return utilitaire.Utilitaire.ajoutHeure(heureliv, 0, duree, 0);
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
}
