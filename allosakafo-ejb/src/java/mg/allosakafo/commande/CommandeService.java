/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.commande;

import bean.CGenUtil;
import bean.ClassMAPTable;
import bean.TypeObjet;
import historique.MapHistorique;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import mg.allosakafo.tiers.ClientAlloSakafo;
import user.UserEJB;
import user.UserEJBBean;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;
import java.util.stream.Collectors;
import mg.allosakafo.produits.ProduitsLibelle;
import utilitaire.Utilitaire;
import  mg.allosakafo.commande.CommandeClientDetails;
import mg.allosakafo.facture.Client;
import mg.allosakafo.facture.Client2;
import mg.allosakafo.fin.FactureClient;
import mg.allosakafo.fin.MvtCaisse;
import mg.allosakafo.livraison.CommandeLivraison;
import mg.allosakafo.produits.ProduitTypeLibellePrix;
import mg.allosakafo.produits.Produits;
import mg.allosakafo.produits.ProduitsTypeLibelle;
import mg.allosakafo.reservation.Reservation;
import mg.allosakafo.stock.MvtStock;
import service.AlloSakafoService;
import user.ValiderObject;
import utilitaire.Constante;
/**
 *
 * @author Joe
 */
public class CommandeService {
    
    public static ProduitsTypeLibelle[] produitValideParPoint(String idPoint,Connection c)throws Exception{
        ProduitsTypeLibelle[] produitsDispo = new ProduitsTypeLibelle[0];
        int verif=0;
        try{ 
            if(c==null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }

            String aWhere = " and id not in (select idproduit from indisponibilite where idpoint = '"+idPoint+"')";
            produitsDispo = (ProduitsTypeLibelle[]) CGenUtil.rechercher(new ProduitsTypeLibelle(), null, null, c, aWhere);
            
            if(verif==1)c.commit();
        } catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }finally{
            if(c!=null && verif==1)c.close();
        }
        return produitsDispo;
    }
    
    public static Client2 monterNouveauClient(String tablename, String restaurant) throws Exception{
        Connection c = null;
        Client2 clt=null;
        try{ 
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            
            clt = new Client2();
            clt.construirePK(c);
            clt.setNom(tablename);
            clt.setTelephone("0343191148");
            clt.setAdresse(restaurant);
            
            clt.insertToTable(c);
            c.commit();
        } catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
        return clt;
    }
    
    public static void monterPriorite(int priorite1, int priorite2, String idToMount,String refuser,Connection c) throws Exception{
        int verif=0;
        try{ 
            if(c==null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }
            CommandeClientDetails cmd = new CommandeClientDetails();
            CommandeClientDetails[] cmds = (CommandeClientDetails[]) CGenUtil.rechercher(cmd, null, null, c," and ID = '"+idToMount+"'");
            if(cmds.length > 0){
                cmds[0].setPrioriter((int)((priorite1 + priorite2)/2));
                cmds[0].updateToTableWithHisto(refuser, c);
            }
            if(verif==1)c.commit();
        } catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }finally{
            if(c!=null && verif==1)c.close();
        }
    }
    public static void monterPriorite(int priorite1, int priorite2, String idToMount) throws Exception{
        Connection c = null;
        try{ 
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            monterPriorite(priorite1, priorite2, idToMount,AlloSakafoService.getNomRestau(),c);
            c.commit();
        } catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }finally{
            if(c!=null)c.close();
        }
    }
    public static CommandeClient cloturerCommandeMobile(String id,String heureClot,String refuser,Connection c) throws Exception{
        int verif=0;
        if(heureClot==null || heureClot.compareToIgnoreCase("")==0 || Utilitaire.testHeureValideBool(heureClot)==false)
        {
            heureClot=Utilitaire.heureCouranteHMS();
        }
        
        try{    
            if(c==null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }

            CommandeClient cmd = new CommandeClient();
            cmd.setId(id);
            CommandeClient[] cmds = (CommandeClient[]) CGenUtil.rechercher(cmd, null, null, c,"");
            if(cmds.length<=0){
                throw new Exception("Commande non trouv�");
            }else{
                cmd=cmds[0];
            }
            if(cmd.getEtat()==9)return cmd;
            cmd.setDatecommande(Utilitaire.dateDuJourSql());
            cmd.setDatesaisie(Utilitaire.dateDuJourSql());
            cmd.setHeureliv(heureClot);
            cmd.setEtat(9);
            
            cmd.updateToTableWithHisto(refuser,c);
            cloturerDetail(id,c);
            if(verif==1)c.commit();
            return cmd;
        } catch (Exception ex) {
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }finally{
            try{
                if(c != null && verif==1) c.close();
            }catch(Exception e){ }
        }
    }
    public static CommandeClient cloturerCommandeMobile(String id,String heureClot) throws Exception{
        Connection c = null;
        try{    
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            
            CommandeClient cmd=cloturerCommandeMobile(id,heureClot,AlloSakafoService.getNomRestau(),c);
            
            c.commit();
            return cmd;
        } catch (Exception ex) {
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }finally{
            try{
                if(c != null) c.close();
            }catch(Exception e){ }
        }
    }
    public static void cloturerCommandeMobileLivraison(String[] id,String heureClot,String livraison,Connection c) throws Exception
    {
        cloturerCommandeMobileLivraison(id,Utilitaire.dateDuJour(),heureClot,livraison,c);
    }
     public static void cloturerCommandeMobileLivraison(String[] ids,String dateClot,String heureClot,String livraison,Connection c) throws Exception{
        int verif=0;
        if(heureClot==null || heureClot.compareToIgnoreCase("")==0 || Utilitaire.testHeureValideBool(heureClot)==false)
        {
            heureClot=Utilitaire.ajoutHeure(Utilitaire.heureCouranteHMS(), 0, Constante.attenteLivraison  ,0);
        }
        if(dateClot==null || dateClot.compareToIgnoreCase("")==0)
        {
            dateClot=Utilitaire.dateDuJour();
        }
        
        try{  
            if(c==null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }
            int etat = ConstanteEtat.getEtatCloture();
            
            
            String[]id=Utilitaire.formerTableauGroupe(ids);
            
            String apresW=" and id in("+ Utilitaire.tabToString(id, "'", ",") +")";
            String apresWFille=" and idmere in("+ Utilitaire.tabToString(id, "'", ",") +") and etat<="+ConstanteEtat.getEtatACloturer();
            CommandeClient[] cmds = (CommandeClient[])CGenUtil.rechercher(new CommandeClient(), null, null, c,apresW);
            CommandeClientDetails[]ldet=(CommandeClientDetails[])CGenUtil.rechercher(new CommandeClientDetails(), null, null, c,apresWFille);
            
            if(ldet.length<=0) throw new Exception("Commande non trouve");
            
            
            
            for(CommandeClient cmd:cmds)
            {
                cmd.setMode("modif");
                cmd.setDatecommande(Utilitaire.stringDate(dateClot));
                cmd.setDatesaisie(Utilitaire.dateDuJourSql());
                cmd.setHeureliv(heureClot);
                if(cmd.estReservation()==true)
                {
                    Reservation res=cmd.getReservation(null, c);
                    cmd.setDatecommande(res.getDatereservation());
                    cmd.setHeureliv(res.getHeuredebutreservation());
                    res.validerObject("1060", c);
                }
                if(cmd.estLivraison(c)==true && cmd.getEtat()<=ConstanteEtat.getEtatACloturer()){
                    etat=ConstanteEtat.getEtatACloturer();
                }
                cmd.setEtat(etat);
                cmd.updateToTableWithHisto("1078", c);
                //cloturerDetailLivraison(cmd.getId(),etat,c);
            }
            for(CommandeClientDetails det:ldet)
            {
                //if(ldet[i].getEtat())
                det.setEtat(etat);
                det.updateToTableWithHisto("1078",c);
            }
           if(verif==1&&verif==1) c.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if( c != null &&verif==1){c.rollback();}
            throw new Exception(ex.getMessage());
        }finally{
            try{
                if(c != null&&verif==1) c.close();
            }catch(Exception e){ }
        }
    }
     public static void cloturerDetailLivraison(String idCommande,int etat,Connection c) throws Exception{
	Statement st = null;
	try {
            st = c.createStatement();
            String rek = "update as_detailscommande set etat="+etat+" where idMere = '" + idCommande + "'";
            st.executeUpdate(rek);
	} catch (SQLException ex) {
	    ex.printStackTrace();
            if( c != null ){c.rollback();}
	    throw new Exception();
	} finally {
	    if (st != null) {
		st.close();
	    }
	}
    }
    public static CommandeClientTotal ajouterCommandeMobile(String idplat,String idclient,double quantiter,String sauce, String remarque) throws Exception{
        Connection c = null;
        try{
            
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            //get Current Table ID if not exist create table
            CommandeClient cmd = new CommandeClient();
            //cmd.setTables(request.getParameter("table"));
            ProduitTypeLibellePrix prd = new ProduitTypeLibellePrix();
            prd.setId(idplat);
            prd = (ProduitTypeLibellePrix)(CGenUtil.rechercher(prd, null, null, c, "")[0]);
            if(prd.getVal_calorie() == 0)
                throw new Exception("Produit indisponible");
            
            CommandeClient[] cmds = (CommandeClient[]) CGenUtil.rechercher(cmd, null, null, c, " and etat<8 and client='"+idclient+"'");
            if(cmds.length<=0){
                //Creer commande
                cmd.construirePK(c);
                cmd.setDatecommande(Utilitaire.dateDuJourSql());
                cmd.setHeureliv(Utilitaire.heureCouranteHMS());
                cmd.setClient(idclient);
                cmd.setNumcommande(cmd.getId());
                cmd.setDatesaisie(cmd.getDatecommande());
                cmd.setTypecommande("TPC00002");
                cmd.setVente("VEN000001");
                cmd.setSecteur("SCT000001");
                cmd.setResponsable("ASR000003");
                cmd.setEtat(1);
                cmd.insertToTable(c);
            }else{
                cmd=cmds[0];
            }
            
            for(int i = 0; i < quantiter; i++){
                //Ajouter commande
                CommandeClientDetails p = new CommandeClientDetails();
                p.construirePK(c);
                p.setIdmere(cmd.getId());
                p.setProduit(idplat);
                p.setPu(prd.getPu());
                p.setQuantite(1);
                p.setObservation(remarque);
                p.setIdAccompagnementSauce(sauce);
//                p.setObservation("");
                p.setEtat(1);
                //int priorite = Utilitaire.stringToInt(type_p.getDesce()) * 1000;
                int priorite = prd.getPriorite() * 1000;
                
                p.setPrioriter(priorite);
                p.insertToTable(c);
            }
            c.commit();
            CommandeClientTotal total = new CommandeClientTotal();
            total.setId(cmd.getId());
            total = ((CommandeClientTotal[]) CGenUtil.rechercher(total, null, null, c, ""))[0];
            return total;
        } catch (Exception ex) {
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw ex;
        }finally{
            try{
                if(c != null) c.close();
            }catch(Exception e){ }
        }
    }
     public static CommandeClientTotal ajouterCommandeMobileLivraison(String idplat,String idclient,double quantiter,String sauce, String remarque, String resto,String livraison,String choixresto,Connection c)throws Exception{
          CommandeClientTotal retour=null;
          int verif=0;
         try{
             if(c==null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
             }
             
             if(resto == null || resto.compareToIgnoreCase("") == 0) {
                 throw new Exception("Point choisi invalide");
             }
             
             retour= ajouterCommandeMobile(idplat,idclient,quantiter,sauce, remarque, resto,choixresto,c);
             CommandeLivraison lv = new CommandeLivraison(livraison);
             lv.updatecommandelivraison(retour.getId(),c);
            c.commit();
         }
         catch(Exception ex){
             ex.printStackTrace();
             if( c != null ){c.rollback();}
             throw ex;
         }
         finally{
            if(c != null && verif==1) c.close();
            
         }
         return retour; 
     }
     public static CommandeClientTotal ajouterCommandeMobile(String idplat,String idclient,double quantiter,String sauce, String remarque, String resto)throws Exception{
         Connection c=null;
         CommandeClientTotal retour=null;
         try{
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            retour= ajouterCommandeMobile(idplat,idclient,quantiter,sauce, remarque, resto,null,c);
            c.commit();
            return retour;
         }
         catch (Exception ex) {
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw ex;
        }finally{
            if(c != null) c.close();
        }
     }
    public static CommandeClientTotal ajouterCommandeMobile(String idplat,String idclient,double quantiter,String sauce, String remarque, String resto,String choixresto,Connection c) throws Exception{
      int verif=0;
        try{
            if(c==null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }
            String estResa="";
            String[]splitClient=Utilitaire.split(idclient, "--");
            idclient=splitClient[0];
            if(splitClient.length>1)estResa=splitClient[1];
            //get Current Table ID if not exist create table
            CommandeClient cmd = new CommandeClient();
            //cmd.setTables(request.getParameter("table"));
            cmd.setClient(idclient);
            ProduitTypeLibellePrix prd = new ProduitTypeLibellePrix();
            prd.setNomTable("as_produitsrevient");
            prd.setId(idplat);
            prd = (ProduitTypeLibellePrix)(CGenUtil.rechercher(prd, null, null, c, "")[0]);
            double prix=prd.getPu();
            if(choixresto!=null && choixresto.contains("lounge"))prix=prd.getPrixl();
            
            //Verification disponibilite plat dans point
            boolean isDispo = AlloSakafoService.verifDisponibilite(resto, idplat, c);
            
            if(!isDispo &&estResa.compareToIgnoreCase("")==0)
                throw new Exception("Produit indisponible");
            String aWhere="";
            if(estResa!="")aWhere=" and typeCommande='"+estResa+"'";
            else if(idclient.startsWith(Constante.marquageTableClient)==false)aWhere=" and typeCommande='"+Constante.idTypeComandeLivr+"'";
            CommandeClient[] cmds = (CommandeClient[]) CGenUtil.rechercher(cmd, null, null, c, " and etat<8 "+aWhere+" order by datecommande desc, id desc");
            
            
            if(cmds.length<=0)
            {
                //Creer commande
                cmd.construirePK(c);
                cmd.setDatecommande(Utilitaire.dateDuJourSql());
                cmd.setHeureliv(Utilitaire.heureCouranteHMS());
                cmd.setClient(idclient);
                cmd.setNumcommande(cmd.getId());
                cmd.setDatesaisie(cmd.getDatecommande());
                cmd.setTypecommandeAuto(estResa);
                cmd.setVente("VEN000001");
                cmd.setSecteur("SCT000001");
                cmd.setResponsable("ASR000003");
                cmd.setPoint(resto);
                cmd.setEtat(1);
                cmd.insertToTable(c);
            }
            else{
                cmd=cmds[0];
            }
            
            for(int i = 0; i < quantiter; i++){
                //Ajouter commande
              
                CommandeClientDetails p = new CommandeClientDetails();
                p.construirePK(c);
                p.setIdmere(cmd.getId());
                p.setProduit(idplat);
                p.setPu(prix);
                p.setRevient(prd.getRevient());
                p.setQuantite(1);
                p.setObservation(remarque);
                p.setIdAccompagnementSauce(sauce);
//                p.setObservation("");
                p.setEtat(1);
                //int priorite = Utilitaire.stringToInt(type_p.getDesce()) * 1000;
                int priorite = prd.getPriorite() * 1000;
                if(cmd.getTypecommande().compareToIgnoreCase(Constante.idTypeComandeLivr)==0&&prd.estDeTypeBoisson()==false)
                {
                    CommandeClientDetails barq=p.creerBarquette(choixresto,c);
                    barq.insertToTable(c);
                }
                p.setPrioriter(priorite);
                p.insertToTable(c);
            }
            if(verif==1)c.commit();
            CommandeClientTotal total = new CommandeClientTotal();
            total.setId(cmd.getId());
            //total = ((CommandeClientTotal[]) CGenUtil.rechercher(total, null, null, c, ""))[0];
            return total;
        } catch (Exception ex) {
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw ex;
        }finally{
            try{
                if(c != null && verif==1) c.close();
            }catch(Exception e){ }
        }
    }
    
    public static void cloturerDetail(String idCommande,Connection c) throws Exception{
	Statement st = null;
	try {
            st = c.createStatement();
            String rek = "update as_detailscommande set etat=9 where idMere = '" + idCommande + "'";
            st.executeUpdate(rek);
	} catch (SQLException ex) {
	    ex.printStackTrace();
            if( c != null ){c.rollback();}
	    throw new Exception();
	} finally {
	    if (st != null) {
		st.close();
	    }
	}
    }
    
    public static CommandeClient getInfoCommandeClient (String idCommande) throws Exception {
        
        try{
            CommandeClient cmt = new CommandeClient();
            CommandeClient[] listeRs;
            
                listeRs=(CommandeClient[])CGenUtil.rechercher(cmt, null, null, " AND ID = '" + idCommande + "'");
                if(listeRs.length>0)
                {
                    return listeRs[0]; 
                }
                else
                {
                    throw new Exception("Identifiant  invalide");
                }
         
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
	public static CommandeClient getInfoCommandeClient (String idCommande,String nomTable) throws Exception {
        
        try{
            CommandeClient cmt = new CommandeClient();
            CommandeClient[] listeRs;
                if(nomTable.compareToIgnoreCase("as_commandepoint")==0)
                {
                    cmt.setNomTable("as_commandepoint");
                    listeRs=(CommandeClient[])CGenUtil.rechercher(cmt, null, null, " AND ID = '" + idCommande + "'");
                    if(listeRs.length>0)
                    {
                        return listeRs[0]; 
                    }
                    else
                    {
                        throw new Exception("numero commande invalide");
                    }
                }
                else
                {
                    listeRs=(CommandeClient[])CGenUtil.rechercher(cmt, null, null, " AND ID = '" + idCommande + "'");
                    if(listeRs.length>0)
                    {
                        return listeRs[0]; 
                    }
                    else
                    {
                        throw new Exception("numero commande invalide");
                    }
                }
         
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
	public static CommandeClient getInfoCommandeClient2 (String idCommande) throws Exception {
        
        try{
            CommandeClient cmt = new CommandeClient();
            CommandeClient[] listeRs = (CommandeClient[])CGenUtil.rechercher(cmt, null, null, " AND ID = '" + idCommande + "'");
			
			ClientAlloSakafo cml = new ClientAlloSakafo();
			ClientAlloSakafo[] lst = (ClientAlloSakafo[])CGenUtil.rechercher(cml, null, null, " AND telephone like '%" + listeRs[0].getClient() + "%'");
			
			listeRs[0].setRemarque(lst[0].getId());
			
            return listeRs[0];
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    public static CommandeClient getInfoCommandeClient2(String idCommande, String nomtable) throws Exception {
        
        try{
            CommandeClient cmt = new CommandeClient();
            CommandeClient[] listeRs=null;
            if(nomtable!=null && nomtable.compareToIgnoreCase("as_commandepoint")==0)
            {
                cmt.setNomTable(nomtable);
                 listeRs = (CommandeClient[])CGenUtil.rechercher(cmt, null, null, " AND ID = '" + idCommande + "'");
            }
            else{
                listeRs = (CommandeClient[])CGenUtil.rechercher(cmt, null, null, " AND ID = '" + idCommande + "'");
            }
			ClientAlloSakafo cml = new ClientAlloSakafo();
			ClientAlloSakafo[] lst = (ClientAlloSakafo[])CGenUtil.rechercher(cml, null, null, " AND telephone like '%" + listeRs[0].getClient() + "%'");
			
			listeRs[0].setRemarque(lst[0].getId());
			
            return listeRs[0];
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    public static double calculerMontantCommande(String idCommande) throws Exception {
        
        try{
            CommandeClientDetails cmt = new CommandeClientDetails();
            CommandeClientDetails[] listeRs = (CommandeClientDetails[])CGenUtil.rechercher(cmt, null, null, " AND IDMERE = '" + idCommande + "'");
            
            double res = 0;
            
            for(int i = 0; i < listeRs.length; i++){
                double remise = ((listeRs[i].getPu() * listeRs[i].getQuantite()) * listeRs[i].getRemise()) / 100;
                res += (listeRs[i].getPu() * listeRs[i].getQuantite()) - remise;
            }

            return res;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
     public void modifierEtatCommandeMere(String idMere,Connection c)throws Exception{
         /*System.out.println("idMere = " + idMere);
          CommandeClientDetails cmt = new CommandeClientDetails();
          int indice = 0,count=0;
            try {
                if (c == null) {
                   c = new UtilDB().GetConn();
                   c.setAutoCommit(false);
                   indice = 1;
                }
                CommandeClientDetails[] listeTousValide = (CommandeClientDetails[])CGenUtil.rechercher(cmt, null, null,c, " and idMere ='"+idMere+"'");
                   int tailleValide = listeTousValide.length;
                   for(int i = 0;i<tailleValide;i++){
                       if(listeTousValide[i].getEtat()==ConstanteEtat.getFait()){
                           count++;
                       }
                   }
                   System.out.println("tailleValide = " + tailleValide+" count : "+count);
                   if(count==listeTousValide.length){
                      CommandeClient[] mere = ((CommandeClient[])CGenUtil.rechercher(new CommandeClient(), null, null,c, " and id ='"+idMere+"'"));
                      if(mere!=null && mere.length>0){
                        mere[0].setEtat(ConstanteEtat.getFait());
                        mere[0].updateToTable(c);
                      }
                     
                  }
                   if (indice == 1) {
                        c.commit();
                    }
             } 
            catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }*/
     }
     public ArrayList<Object> setEtatDetailCommande(String[] id,Connection c) throws Exception {
          CommandeClientDetails cmt = new CommandeClientDetails();
        int indice = 0;
        ArrayList<Object> mere = new ArrayList<Object>();
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            CommandeClientDetails[] listeComs = (CommandeClientDetails[])CGenUtil.rechercher(cmt, null, null,c, "");
            int tailleComs = listeComs.length,tailleId = id.length;
            for(int i = 0;i<tailleComs;i++){
                for(int j = 0;j<tailleId;j++){
                    if(listeComs[i].getId().equals(id[j])){
                        if(listeComs[i].getEtat() == ConstanteEtat.getEtatLivraison()){
                            throw new Exception("commande d�j� livr�");
                        }
                        mere.add(listeComs[i].getIdmere());
                        listeComs[i].setEtat(ConstanteEtat.getFait());
                        listeComs[i].updateToTable(c);
                    }
                }
            }
            
            if (indice == 1) {
                c.commit();
            }
            mere = (ArrayList) mere.stream().distinct().collect(Collectors.toList());
          return mere;
        } catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
    }
     
    public void validerDetailsCommande(String[] id,Connection c)throws Exception{
           int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            if(id == null){
                throw new Exception("Aucune commande selectione");
            }
            ArrayList<Object> mere = setEtatDetailCommande( id, c);
            for(int i=0;i<mere.size();i++){
                modifierEtatCommandeMere(mere.get(i).toString(),c);
            }
            
            if (indice == 1) {
                c.commit();
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if(c!=null && indice==1)c.close();
        }
     }
    
    public void validerDetailsCommande(String[] id)throws Exception{
        
        Connection c=null;
        try {
             
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                
            
            
            validerDetailsCommande(id,c);
        } catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
        
        
        
        finally
        {
            if(c!=null)c.close();
        }
     }
    
    public ArrayList<Object> setEtatDetailCommande(String[] id, int etat, String message,Connection c) throws Exception {
        CommandeClientDetailsFM cmt = new CommandeClientDetailsFM();
        int indice = 0;
        ArrayList<Object> mere = new ArrayList<Object>();
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            String apresWhere = " and id in ("+Utilitaire.tabToString(id, "'", ",")+")";
            CommandeClientDetailsFM[] listeComs = (CommandeClientDetailsFM[])CGenUtil.rechercher(cmt, null, null,c, apresWhere);
            for(CommandeClientDetailsFM cmd : listeComs){
                if(etat >= ConstanteEtat.getEtatPaye() && cmd.getEtat() != ConstanteEtat.getEtatLivraison()){
                    throw new Exception("Vous ne pouvez pas payer les commandes non livr�s ou Commande deja paye");
                }
                
                //mere.add(cmd.getIdmere());
                CommandeClientDetails  detailsCommande = cmd.getCommandeClientDetails();
                CommandeClient commandeClient = cmd.getCommandeClient();
                
                detailsCommande.setEtat(etat);
                detailsCommande.updateToTable(c);
                mere.add(commandeClient);
            }
            
            if (indice == 1) {
                c.commit();
            }
            
          return mere;
        } catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if(c!=null&&indice==1)c.close();
        }
    }
    
//    public ArrayList<Object> setEtatDetailCommande(String[] id, int etat, String message,Connection c) throws Exception {
//        CommandeClientDetails cmt = new CommandeClientDetails();
//        int indice = 0;
//        ArrayList<Object> mere = new ArrayList<Object>();
//        try {
//            if (c == null) {
//                c = new UtilDB().GetConn();
//                c.setAutoCommit(false);
//                indice = 1;
//            }
//            String apresWhere = " and id in ("+Utilitaire.tabToString(id, "'", ",")+")";
//            CommandeClientDetails[] listeComs = (CommandeClientDetails[])CGenUtil.rechercher(cmt, null, null,c, apresWhere);
//            for(CommandeClientDetails cmd : listeComs){
//                if(etat >= ConstanteEtat.getEtatPaye() && cmd.getEtat() != ConstanteEtat.getEtatLivraison()){
//                    throw new Exception("Vous ne pouvez pas payer les commandes non livr�s ou Commande deja paye");
//                }
//                
//                //mere.add(cmd.getIdmere());
//                cmd.setEtat(etat);
//                cmd.updateToTable(c);
//            }
//            
//            if (indice == 1) {
//                c.commit();
//            }
//            //mere = (ArrayList) mere.stream().distinct().collect(Collectors.toList());
//          return mere;
//        } catch(Exception ex){
//            ex.printStackTrace();
//            if( c != null ){c.rollback();}
//            throw new Exception(ex.getMessage());
//        }
//        finally
//        {
//            if(c!=null&&indice==1)c.close();
//        }
//    }

    public ArrayList<Object> setEtatDetailCommandeLivraison(String[] id, int etat, String message,Connection c) throws Exception {
        //CommandeClientDetails cmt = new CommandeClientDetails();
        int indice = 0;
        ArrayList<Object> mere = new ArrayList<Object>();
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            String apresWhere = " and id in ("+Utilitaire.tabToString(id, "'", ",")+") order by id desc";
            CommandeClientDetails details=null;
            //DetailCommandeClient[] listeComs = (DetailCommandeClient[])CGenUtil.rechercher(new DetailCommandeClient(), null, null,c, apresWhere);
            CommandeClientDetailsFM[] listeComs = (CommandeClientDetailsFM[])CGenUtil.rechercher(new CommandeClientDetailsFM(), null, null,c, apresWhere);
            for(int i=0;i<listeComs.length;i++){
                if(etat >= ConstanteEtat.getEtatPaye() && listeComs[i].getEtat() != ConstanteEtat.getEtatLivraison()){
                    throw new Exception("Vous ne pouvez pas payer les commandes non livr�s ou Comande deja paye");
                }
//                if(!listeComs[i].getIdclient().startsWith("CASM")){
                if(!listeComs[i].getClient().startsWith("CASM")){
                    etat=ConstanteEtat.getEtatALivrer();
                }else{
                    etat=ConstanteEtat.getEtatLivraison();
                }
//                details=new CommandeClientDetails(listeComs[i].getId(), listeComs[i].getIdmere(), listeComs[i].getProduit(), listeComs[i].getObservation(), listeComs[i].getIdAccompagnementSauce(), listeComs[i].getQuantite(), listeComs[i].getPu(), listeComs[i].getRemise(), etat, listeComs[i].getPrioriter(),listeComs[i].getRevient());
                details = listeComs[i].getCommandeClientDetails();
                details.setEtat(etat);
//                mere.add(listeComs[i].getIdmere()+"-"+etat);
                mere.add(listeComs[i].getCommandeClient());
                details.updateToTable(c);
            }
            
            if (indice == 1) {
                c.commit();
            }
            //mere = (ArrayList) mere.stream().distinct().collect(Collectors.toList());
          return mere;
        } catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if(c!=null&&indice==1)c.close();
        }
    }
    public ArrayList<Object> setEtatDetailCommandeWithUser(String[] id,String caisse,String refuser, int etat, String message,Connection c) throws Exception {
        CommandeClientDetailsFM cmt = new CommandeClientDetailsFM();
        int indice = 0;
        ArrayList<Object> mere = new ArrayList<Object>();
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            
            String apresWhere = " and id in ("+Utilitaire.tabToString(id, "'", ",")+")";
            //CommandeClientDetails[] listeComs = (CommandeClientDetails[])CGenUtil.rechercher(cmt, null, null,c, apresWhere);
            CommandeClientDetailsFM[] listeComs = (CommandeClientDetailsFM[])CGenUtil.rechercher(cmt, null, null,c, apresWhere);

            for(CommandeClientDetailsFM cmdFm : listeComs){
                CommandeClient cc = cmdFm.getCommandeClient();
                CommandeClientDetails cmd = cmdFm.getCommandeClientDetails();
                if(cmd.getEtat()!=ConstanteEtat.getEtatACloturer())
                {
                    throw new Exception("Comande non cloturable");
                }
                if(etat >= ConstanteEtat.getEtatPaye() && cmd.getEtat() != ConstanteEtat.getEtatLivraison()){
                    throw new Exception("Vous ne pouvez pas payer les commandes non livr�s ou Comande deja paye");
                }
                
                if(AlloSakafoService.getNomRestau().compareToIgnoreCase(cc.getPoint()) != 0){
                    throw new Exception("Action non permise");
                }
                
                mere.add(cmd.getIdmere());
                cmd.setEtat(etat);
                cmd.updateToTableWithHisto(refuser,c);
                
            }
            
            
            
            //String aWhereMere=and id in ("+Utilitaire.tabToString(id, "'", ",")+")"
            mere = (ArrayList) mere.stream().distinct().collect(Collectors.toList());
            String[]lMere=mere.toArray(new String[mere.size()]);
            CommandeClient[] lmere=(CommandeClient[])CGenUtil.rechercher(new CommandeClient(), null, null, c, " and id in ("+Utilitaire.tabToString(lMere, "'", ",")+")");
            //System.out.println("apresWhere ==== "+" and id in ("+Utilitaire.tabToString(lMere, "'", ",")+")");
            for(int im=0;im<lMere.length;im++)
            {
                boolean estChange=lmere[im].decalerLivraison(Utilitaire.heureCouranteHMS(), mg.cnaps.commun.Constante.dureeLivraisonDefaut);
                //System.out.println("HeureLiv vaovao "+lmere[im].getHeureliv());
                if(estChange==true) lmere[im].updateToTableWithHisto(refuser,c);
            }
            if (indice == 1) {
                c.commit();
            }
          return mere;
        } catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
    }
    public void modifierEtatCommandeMere(String idMere,int etat,Connection c)throws Exception{
          /*CommandeClientDetails cmt = new CommandeClientDetails();
          int indice = 0,count=0;
            try {
                if (c == null) {
                   c = new UtilDB().GetConn();
                   c.setAutoCommit(false);
                   indice = 1;
                }
                CommandeClientDetails[] listeTousValide = (CommandeClientDetails[])CGenUtil.rechercher(cmt, null, null,c, " and idMere ='"+idMere+"'");
                   int tailleValide = listeTousValide.length;
                   for(int i = 0;i<tailleValide;i++){
                       if(listeTousValide[i].getEtat()== etat){
                           count++;
                       }
                   }
                   
                   if(count==listeTousValide.length){
                      CommandeClient[] mere = ((CommandeClient[])CGenUtil.rechercher(new CommandeClient(), null, null,c, " and id ='"+idMere+"'"));
                      if(mere!=null && mere.length>0){
                        mere[0].setEtat(etat);
                        mere[0].updateToTable(c);
                      }
                     
                  }
                   if (indice == 1) {
                        c.commit();
                    }
             } 
            catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }*/
    }
    public void cloturerDetailsCommande(String[] id, Connection c)throws Exception{
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            if(id == null){
                throw new Exception("Aucune commande selectione");
            }
            ArrayList<Object> mere = setEtatDetailCommande(id,ConstanteEtat.getEtatCloture(),"Commande d�j� clotur�", c);
            
            for(int i=0;i<mere.size();i++){
                modifierEtatCommandeMere(mere.get(i).toString(),ConstanteEtat.getEtatCloture(),c);
            }
            
            if(verif == 1) c.commit();
        }
        catch(Exception ex){
            ex.printStackTrace();
            if(verif == 1) c.rollback();
            throw new Exception(ex.getMessage());
        }
        finally{
            if(c != null && verif ==1 ) c.close();
        }
    }
    public void cloturerDetailsCommandeWithUser(String[] id,String caisse,String refuser, Connection c)throws Exception{
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            if(id == null){
                throw new Exception("Aucune commande selectione");
            }
            
            setEtatDetailCommandeWithUser(id,caisse,refuser,ConstanteEtat.getEtatCloture(),"Commande d�j� clotur�", c);
            
            /*for(int i=0;i<mere.size();i++){
                modifierEtatCommandeMere(mere.get(i).toString(),ConstanteEtat.getEtatCloture(),c);
            }*/
            
            if(verif == 1) c.commit();
        }
        catch(Exception ex){
            ex.printStackTrace();
            if(verif == 1) c.rollback();
            throw new Exception(ex.getMessage());
        }
        finally{
            if(c != null && verif ==1 ) c.close();
        }
    }
    public void validerDetailsCommande2(String[] id, Connection c)throws Exception{
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            if(id == null){
                throw new Exception("Aucune commande selectione");
            }
            
            ArrayList<Object> mere = setEtatDetailCommande(id,ConstanteEtat.getEtatValider(),"Commande d�j� valid�", c);
            
            for(int i=0;i<mere.size();i++){
                CommandeClient clTemp = (CommandeClient) mere.get(i);
                if(clTemp.getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0) {
                    throw new Exception("Action non permise");
                }
                modifierEtatCommandeMere(mere.get(i).toString(),ConstanteEtat.getEtatValider(),c);
            }
            
            if(verif == 1) c.commit();
        }
        catch(Exception ex){
            ex.printStackTrace();
            if(verif == 1) c.rollback();
            throw new Exception(ex.getMessage());
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
    
        
        
    }
    
    public void insertlivrerCommandeDetails (String[] idCommande, Connection c) throws Exception{
        int verif = 0;
        try{
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            String id_str = Utilitaire.tabToString(idCommande, "'", ",");
            CommandeClientDetails[] listeDetails=(CommandeClientDetails[])CGenUtil.rechercher(new CommandeClientDetails(), null, null, c, " and id in (" + id_str + ")");
            for(int i=0;i<listeDetails.length;i++){
                Livraison liv = new Livraison();
                //liv.setCommande(listeDetails[i].getId());
                liv.setDaty(Utilitaire.dateDuJourSql());
                liv.setHeure(Utilitaire.heureCouranteHMS());
                liv.construirePK(c);
                liv.insertToTable(c);
            }
            if(verif == 1) c.commit();
        }
        catch(Exception e){
            if (c!=null) c.rollback();
            throw e;
        }
        finally{
            if (c!= null) c.close();
        }
    }
    
    public void livrerDetailsCommande(String[] id, Connection c)throws Exception{
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            ArrayList<Object> mere = setEtatDetailCommande(id,ConstanteEtat.getEtatLivraison(),"Commande d�j� livr�", c);
            
            for(int i=0;i<mere.size();i++){
                modifierEtatCommandeMere(mere.get(i).toString(),ConstanteEtat.getEtatLivraison(),c);
            }
            
            insertlivrerCommandeDetails(id, c);
            
            if(verif == 1) c.commit();
        }
        catch(Exception ex){
            ex.printStackTrace();
            if(verif == 1) c.rollback();
            throw new Exception(ex.getMessage());
        }
        finally{
            if(c != null && verif ==1 ) c.close();
        }
    }
    public void payerDetailsCommande(String[] id, Connection c)throws Exception{
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            ArrayList<Object> mere = setEtatDetailCommande(id,ConstanteEtat.getEtatPaye(),"Commande d�j� pay�", c);
            
            for(int i=0;i<mere.size();i++){
                modifierEtatCommandeMere(mere.get(i).toString(),ConstanteEtat.getEtatPaye(),c);
            }
            
            if(verif == 1) c.commit();
        }
        catch(Exception ex){
            ex.printStackTrace();
            if(verif == 1) c.rollback();
            throw new Exception(ex.getMessage());
        }
        finally{
            if(c != null && verif ==1 ) c.close();
        }
    }
     
     public  CommandeClientDetailsAccompagne[] getCommandeMine(String id,String idclient,Connection c)throws Exception{
         boolean verif=false; 
         try {
            if (c == null) {
                c = new UtilDB().GetConn();
                verif=true;
            }
            if(idclient==null){
                return new CommandeClientDetailsAccompagne[0];
            }
            CommandeClientDetailsAccompagne[] mescommandes;
            CommandeClientDetailsAccompagne cm = new CommandeClientDetailsAccompagne();
            if(id!=null){
                mescommandes = ((CommandeClientDetailsAccompagne[])CGenUtil.rechercher(cm, null, null,c, " and id ='"+id+"'"));
            }
            else{
                mescommandes = ((CommandeClientDetailsAccompagne[])CGenUtil.rechercher(cm, null, null,c, " and etat > 0 and etat< 8 and idclient='"+idclient+"'"));
            }
            return mescommandes;
              
          }
           catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally{
            if(c != null && verif ==true ) c.close();
        }
     }
     public  CommandeClientDetailsAccompagne[] getCommandeType(String typeCommande,String idclient,Connection c)throws Exception{
         boolean verif=false; 
         try {
            if (c == null) {
                c = new UtilDB().GetConn();
                verif=true;
            }
            
            String aWhere="";
            if(typeCommande!=null)aWhere=" and typeCommande='"+typeCommande+"'";
            else if(idclient.startsWith(Constante.marquageTableClient)==false)aWhere=" and typeCommande='"+Constante.idTypeComandeLivr+"'";
            CommandeClientDetailsAccompagne[] mescommandes;
            CommandeClientDetailsAccompagne cm = new CommandeClientDetailsAccompagne();
            cm.setNomTable("AS_DETAILSCOMMANDE_groupe");
            
            mescommandes = ((CommandeClientDetailsAccompagne[])CGenUtil.rechercher(cm, null, null,c, aWhere+ " and etat > 0 and etat< 8 and idclient='"+idclient+"'"));
            
            return mescommandes;
              
          }
           catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally{
            if(c != null && verif ==true ) c.close();
        }
     }
     
     
     
     public void updatePlat(int qte,String id,String sauce,String refuser,Connection c)throws Exception{
        int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            CommandeClientDetails cmd = new CommandeClientDetails();
            cmd.setId(id);
            cmd = ((CommandeClientDetails)CGenUtil.rechercher(cmd, null, null,c, "")[0]);
//             cmd.setQuantite(qte);
            cmd.setIdAccompagnementSauce(sauce);
            cmd.updateToTableWithHisto(refuser, c);

            for(int i = 1; i < qte; i++){
               //Ajouter commande
               CommandeClientDetails p = cmd;
               p.construirePK(c);
               p.insertToTableWithHisto(refuser, c);
            }

            if(qte==0){
               cmd.deleteToTableWithHisto(refuser, c);
            }

            if (indice == 1) c.commit();
        }catch(Exception ex){
         ex.printStackTrace();
         if(c!=null)c.rollback();
         throw new Exception("erreur update: "+ex.getMessage());
        }finally{
            if(c!=null && indice==1)c.close();
        }
    }
    public void updatePlat(int qte,String id,String sauce,Connection c)throws Exception{
        int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            updatePlat(qte,id,sauce,AlloSakafoService.getNomRestau(),c);

            if (indice == 1) c.commit();
        }catch(Exception ex){
            ex.printStackTrace();
            if(c!=null) c.rollback();
            throw new Exception("erreur update: "+ex.getMessage());
        }finally{
            if(c!=null && indice==1)c.close();
        }
     }
     
    public void deletePlat(String id,String refuser,Connection c)throws Exception{
        int indice = 0;
        try{
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            CommandeClientDetails cmd = new CommandeClientDetails();
            cmd.setId(id);
            cmd.deleteToTableWithHisto(refuser, c);
            if (indice == 1) c.commit();
        }catch(Exception ex){
            if(c!=null)c.rollback();
            ex.printStackTrace();
            throw new Exception("erreur delete: "+ex.getMessage());
        }finally{
            if(c!=null && indice==1)c.close();
        }
    }
    public void deletePlat(String id)throws Exception{
        Connection c=null;
        try{
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            deletePlat(id,AlloSakafoService.getNomRestau(),c);
            c.commit();
        }catch(Exception ex){
            if(c!=null)c.rollback();
            ex.printStackTrace();
            throw new Exception("erreur delete: "+ex.getMessage());
        }finally{
            if(c!=null)c.close();
        }
    }
    
    
    
    public void annulerDetailsCommande(String[] id,Connection c,String user)throws Exception{
        int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            if(id == null){
                throw new Exception("Aucune commande selectione");
            }
            String tid = Utilitaire.tabToString(id, "'", ",");
            String id_str = Utilitaire.tabToString(id, "'", ",");
            CommandeClientDetailsWithPoint[] cmds = (CommandeClientDetailsWithPoint[]) CGenUtil.rechercher(new CommandeClientDetailsWithPoint(), null, null, c, " and id in (" + id_str + ")");
            
            //DetailCommandeClient[] cmds = (DetailCommandeClient[]) CGenUtil.rechercher(new DetailCommandeClient(), null, null, c," and ID in ("+tid+")");
            if(cmds.length == 0){
                throw new Exception("Commande detail introuvable");
            }
            for(int i=0; i<id.length; i++)
            {
                CommandeClientDetails dts = cmds[i];
                
                if(cmds[i].getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0){
                    throw new Exception("Action non permise");
                }
                
                dts.setNomTable("as_detailscommande");
                if(cmds[i].getEtat()>=ConstanteEtat.getEtatLivraison())
                {
                    MvtStock mvt=cmds[i].creerMvtStock(c);
                    MvtStock contre=mvt.contrer(c);
                    contre.insertAvecFille(user, c);
                    contre.validerObject(user, c);
                }
                 dts.setEtat(ConstanteEtat.getEtatAnnuler());
                //cmds[i].setEtat(ConstanteEtat.getEtatCreer());
                //cmds[i].deleteToTable(c);
                MvtCaisse mvtC=dts.getMvtCaisse(c);
                //System.out.println("Mvt Caisse = "+mvtC);
                if(mvtC!=null)
                {
                    mvtC.annulerVisa(user, c);
                }
                
                if(!cmds[i].getClient().startsWith("CASM")&&cmds[i].getEtat()==ConstanteEtat.getEtatCloture()){
                    FactureClient[] lsfact=cmds[i].getFactureClient(c);
                    if(lsfact.length>0)lsfact[0].annulerFacture(user, c);
                    dts.setEtat(ConstanteEtat.getEtatACloturer());
                }
                dts.updateToTableWithHisto(user, c);
                
            }
            if (indice == 1) {
                c.commit();
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            if( indice == 1 ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if(indice==1)c.close();
        }
    }
    
    
    public void annulerValiderDetailsCommande(String[] id,String user,Connection c)throws Exception{
        int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            if(id == null){
                throw new Exception("Aucune commande selectione");
            }
            String tid = Utilitaire.tabToString(id, "'", ",");
            //CommandeClientDetails[] cmds = (CommandeClientDetails[]) CGenUtil.rechercher(new CommandeClientDetails(), null, null, c," and ID in ("+tid+")");
            CommandeClientDetailsFM[] cmds = (CommandeClientDetailsFM[]) CGenUtil.rechercher(new CommandeClientDetailsFM(), null, null, c," and ID in ("+tid+")");
            if(cmds.length == 0){
                throw new Exception("Commande detail introuvable");
            }
            
            for(int i=0; i<id.length; i++){
                CommandeClient ccl = cmds[i].getCommandeClient();
                CommandeClientDetails ccd = cmds[i].getCommandeClientDetails();
                
                if(ccl.getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0){
                    throw new Exception("Action non permise");
                }
                
                System.out.println("----i = " + i);
                ccd.setEtat(ConstanteEtat.getCloture());
                ccd.updateToTableWithHisto(user,c);
                
//                cmds[i].setEtat(ConstanteEtat.getCloture());
                //String obje, String action, String user, String refObje, Connection c
//                cmds[i].updateToTableWithHisto(user,c);
            }
            if (indice == 1) {
                c.commit();
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            if( indice == 1 ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if(indice==1)c.close();
        }
    }
    public void annulerBoisson(String[] id,Connection c)throws Exception{
        int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            if(id == null){
                throw new Exception("Aucune commande selectione");
            }
            String tid = Utilitaire.tabToString(id, "'", ",");
            CommandeClientDetails[] cmds = (CommandeClientDetails[]) CGenUtil.rechercher(new CommandeClientDetails(), null, null, c," and ID in ("+tid+")");
            if(cmds.length == 0){
                throw new Exception("Commande detail introuvable");
            }
            for(int i=0; i<id.length; i++){
                cmds[i].setEtat(ConstanteEtat.getEtatCreer());
                cmds[i].updateToTableWithHisto(cmds[i],c);
            }
            if (indice == 1) {
                c.commit();
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            if( indice == 1 ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if(indice==1)c.close();
        }
    }
    
    public void modifRemarque(String[] id, String[] remarque, String refuser,Connection c)throws Exception{
        int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            
            if(id == null){
                throw new Exception("Aucune commande selectione");
            }
            
            HashMap<String, Integer> indices = new HashMap<>();
            for(int j=0;j<id.length;j++){
                String[] id_indice = Utilitaire.split(id[j], "-");
                id[j] = id_indice[0];
                indices.put(id_indice[0], Integer.parseInt(id_indice[1]));
            }
            
            String tid = Utilitaire.tabToString(id, "'", ",");
            
            CommandeClientDetails[] cmds = (CommandeClientDetails[]) CGenUtil.rechercher(new CommandeClientDetails(), null, null, c," and ID in ("+tid+") order by prioriter asc, etat desc");
            if(cmds.length == 0){
                throw new Exception("Commande detail introuvable");
            }
            for(int i=0; i<id.length; i++){
                cmds[i].setObservation(remarque[indices.get(cmds[i].getId())]);
                cmds[i].updateToTableWithHisto(refuser, c);
            }
            if (indice == 1) {
                c.commit();
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            if( c!=null ){c.rollback();}
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if(c!=null && indice==1)c.close();
        }
    }
    public void modifRemarque(String[] id, String[] remarque, Connection c)throws Exception{
        int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            modifRemarque(id,remarque, AlloSakafoService.getNomRestau(),c);
            if (indice == 1) {
                c.commit();
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            if(c!=null){c.rollback();}
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if(c!=null && indice==1)c.close();
        }
    }
}
