/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.ded;

import bean.ClassEtat;
import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author BICI
 */
public class Report extends ClassEtat{
    private String id, devise, caisse;
    private Date daty;
    private double montant;

    
    public Report(){
         this.setNomTable("report");
    }
    public void construirePK(Connection c) throws Exception {
        this.preparePk("REP", "getseqReport");
        this.setId(makePK(c));
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }
    
    public String getCaisse(){
        return caisse;
    }
    
    public void setCaisse(String caisse){
     this.caisse=caisse;
    }
    
     public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (montant > 0) {
                this.montant = montant;
                return;
            } else {
                throw new Exception("Montant n�gatif ou null");
            }
        }
        this.montant = montant;
    }
    
    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
    
}
