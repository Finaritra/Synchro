/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.ded;

import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author Axel
 */
public class Annulation extends ClassEtat {

    private String id , idobjet,motif ;
    private double montant;
    private Date daty;
    
    
    public Annulation(){
        this.setNomTable("annulationop");
    }
    public Annulation (String id,String idObjet)
    {
        setId(id);
        setIdobjet(idObjet);
        this.setNomTable("annulationop");
    }
    public Annulation (String idObjet,String mot,double montan,Date dt,String nomTable)throws Exception
    {
        setIdobjet(idObjet);
        setMotif(mot);
        setMontant(montan);
        setDaty(dt);
        this.setNomTable(nomTable);
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    
    
    public String getIdobjet() {
        return idobjet;
    }

    public void setIdobjet(String idobjet) {
        this.idobjet = idobjet;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) throws Exception {
        if(this.getMode().compareToIgnoreCase("select")==0)
        {
            this.montant = montant;
            return;
        }
        if(montant<=0) throw new Exception("montant invalide");
        this.montant = montant;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date dat) {
        this.daty = dat;
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("AOP", "getSeqAnnulationOP");
        this.setId(makePK(c));
    }
    
    
    
    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
        

    
    
    
}
