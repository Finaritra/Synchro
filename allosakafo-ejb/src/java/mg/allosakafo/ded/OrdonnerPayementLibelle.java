package mg.allosakafo.ded;

import bean.ClassMAPTable;
import java.sql.Date;

/*
 import finance.MvtCaisse;
 import finance.MvtCaisseUtil;
 import facturefournisseur.FactureFournisseurLc;
 import lc.LigneCredit;
 */
public class OrdonnerPayementLibelle extends ClassMAPTable {

    private String id;
    private String ded_id;
    private String idligne;
    private java.sql.Date daty;
    private double montant;
    private String remarque;
    private String etat;
    
    public OrdonnerPayementLibelle() {
        this.setNomTable("AS_ORDONNERPAYEMENTLIBELLE");
    }
    
    public String getIdligne() {
        return idligne;
    }

    public void setIdligne(String idligne) {
        this.idligne = idligne;
    }
    

    public String getDed_id() {
        return ded_id;
    }

    public void setDed_id(String ded_id) {
        this.ded_id = ded_id;
    }
    
   

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
   

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

}
