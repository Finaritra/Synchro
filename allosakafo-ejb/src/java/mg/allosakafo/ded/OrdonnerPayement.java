package mg.allosakafo.ded;

import bean.*;
import java.sql.Connection;
import mg.allosakafo.facturefournisseur.EtatFacture;
import service.AlloSakafoService;
import utilitaire.UtilDB;
import utilitaire.ConstanteEtat;

/*
import finance.MvtCaisse;
import finance.MvtCaisseUtil;
import facturefournisseur.FactureFournisseurLc;
import lc.LigneCredit;
*/

public class OrdonnerPayement extends ClassEtat {

  private String id;
  private String ded_Id;
  private String idLigne;
  private java.sql.Date daty;
  private double montant;
  private String remarque;
  
  public OrdonnerPayement() {
    this.setNomTable("OrdonnerPayement");
  }
  public OrdonnerPayement(String id,String ded,String ligne,String dt,String mont,String rem,String eta) {
    this.setId(id);
    this.setDed_Id(ded);
    this.setIdLigne(ligne);
    this.setDaty((utilitaire.Utilitaire.string_date("dd/MM/yyyy",dt)));
    this.setMontant(utilitaire.Utilitaire.stringToDouble(mont));
    this.setRemarque(rem);
    this.setEtat(utilitaire.Utilitaire.stringToInt(eta));
  }
  public OrdonnerPayement(String id,String ded,java.sql.Date dt,double mont,String rem) {
    this.setId(id);
    this.setDed_Id(ded);
    this.setDaty(dt);
    this.setMontant(mont);
    this.setRemarque(rem);
  }
  public OrdonnerPayement(String ded,String ligne,String dt,String mont,String rem)throws Exception {
    this.setNomTable("OrdonnerPayement");
    this.preparePk("ORP","getSeqOp");
    this.setId(makePK());
    this.setDed_Id(ded);
    this.setIdLigne(ligne);
    this.setDaty(utilitaire.Utilitaire.string_date("dd/MM/yyyy",dt));
    this.setMontant(utilitaire.Utilitaire.stringToDouble(mont));
    this.setRemarque(rem);
  }
  
  public void construirePK(Connection c) throws Exception {
        this.preparePk("ORP", "getSeqOp");
        this.setId(makePK(c));
    }
  
  public String getAttributIDName() {
    return "id";
  }
  public String getTuppleID() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public String getId() {
    return id;
  }
  public void setDed_Id(String ded_Id) {
    this.ded_Id = ded_Id;
  }
  public String getDed_Id() {
    return ded_Id;
  }
  public void setIdLigne(String idLigne) {
    this.idLigne = idLigne;
  }
  public String getIdLigne() {
    return idLigne;
  }
  public void setDaty(java.sql.Date daty) {
    this.daty = daty;
  }
  public java.sql.Date getDaty() {
    return daty;
  }
  public void setMontant(double montant) {
    this.montant = montant;
  }
  public double getMontant() {
    return montant;
  }
  public void setRemarque(String remarque) {
    this.remarque = remarque;
  }
  public String getRemarque() {
    return remarque;
  }
  public void controler(java.sql.Connection c) throws Exception
  {
        EtatFacture fact=new EtatFacture();
        fact.setNomTable("ETATFACTURE_ENTITEVALIDE");
        fact.setId(this.getDed_Id());
        EtatFacture[] lf=(EtatFacture[])CGenUtil.rechercher(fact, null,null,c,"");
        
        if(lf != null && lf.length > 0)
        {
            if(lf[0].getEntite().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0) throw new Exception("Action non permise");
            if(this.getMontant()>lf[0].getReste())throw new Exception("montant superieur au reste");
        }
  }

}