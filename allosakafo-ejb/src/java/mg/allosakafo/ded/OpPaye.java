/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.ded;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author BICI
 */
public class OpPaye extends ClassMAPTable{
    private String id;
    private double apayer,payer,reste;

    public double getApayer() {
        return apayer;
    }

    public void setApayer(double apayer) {
        this.apayer = apayer;
    }

    public double getPayer() {
        return payer;
    }

    public void setPayer(double payer) {
        this.payer = payer;
    }

    public double getReste() {
        return reste;
    }

    public void setReste(double reste) {
        this.reste = reste;
    }
    
    public OpPaye(){
         this.setNomTable("OPPAYE");
    }
   
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
        
    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
    
}
