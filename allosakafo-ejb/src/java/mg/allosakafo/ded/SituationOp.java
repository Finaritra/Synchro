/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.ded;

import bean.ClassMAPTable;
import java.sql.Date;

/**
 *
 * @author Doudou Tiarilala
 */
public class SituationOp extends ClassMAPTable {

    private String id, remarque;
    private Date daty;
    private double apayer, payer, reste;

    public SituationOp() {
        this.setNomTable("SITUATION_OP");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public double getApayer() {
        return apayer;
    }

    public void setApayer(double apayer) {
        this.apayer = apayer;
    }

    public double getPayer() {
        return payer;
    }

    public void setPayer(double payer) {
        this.payer = payer;
    }

    public double getReste() {
        return reste;
    }

    public void setReste(double reste) {
        this.reste = reste;
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
}
