/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.facturefournisseur;

import bean.CGenUtil;
import bean.ClassMAPTable;
import java.sql.Connection;
import service.AlloSakafoService;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;

/**
 *
 * @author JESSI
 */
public class DetailsFactureFournisseur extends ClassMAPTable{
    private String id,idingredient,idmere,compte,idtypefacture,idtypefacturelib;
    private double qte,pu;

    public DetailsFactureFournisseur(String idmere, double qte, double pu)throws Exception {
      this.setNomTable("detailsfacturefournisseur");
      this.setIdmere(idmere);
      this.setQte(qte);
      this.setPu(pu);
    }

    public DetailsFactureFournisseur(String idingredient, String idmere, double qte, double pu)throws Exception {
      this.setNomTable("detailsfacturefournisseur");
      this.setIdingredient(idingredient);
      this.setIdmere(idmere);
      this.setQte(qte);
      this.setPu(pu);
    }

    public String getIdtypefacturelib() {
        return idtypefacturelib;
    }

    public void setIdtypefacturelib(String idtypefacturelib) {
        this.idtypefacturelib = idtypefacturelib;
    }

    public String getIdtypefacture() {
        return idtypefacture;
    }

    public void setIdtypefacture(String idtypefacture) {
        this.idtypefacture = idtypefacture;
    }

    public String getCompte() {
        return compte;
    }

    public void setCompte(String compte) {
        this.compte = compte;
    }


   

    public DetailsFactureFournisseur() {
        this.setNomTable("detailsfacturefournisseur");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdingredient() {
        return idingredient;
    }

    public void setIdingredient(String idingredient)throws Exception {
        if(this.getMode().compareToIgnoreCase("modif")==0 &&(idingredient==null||idingredient.compareToIgnoreCase("")==0) )throw new  Exception("Ingredient invalide");
        this.idingredient = idingredient;
    }

    public String getIdmere() {
        return idmere;
    }

    public void setIdmere(String idmere) {
        this.idmere = idmere;
    }

    public double getQte() {
        return qte;
    }

    public void setQte(double qte)throws Exception {
        if(this.getMode().compareToIgnoreCase("modif")==0 && qte<=0)throw new  Exception("quantite invalide");
  
        this.qte = qte;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu)throws Exception {
        if(this.getMode().compareToIgnoreCase("modif")==0 && pu<=0)throw new  Exception("Prix unitaire invalide");
        this.pu = pu;
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
      return "id";
    }

    @Override
    public void construirePK(Connection c) throws Exception {
         this.preparePk("DF", "getseqdfacturefournisseur");
        this.setId(makePK(c));
    }
    
    public void verifPoint(Connection c)throws Exception{
        int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                verif=1;
            }
           FactureFournisseur[] lsf=(FactureFournisseur[]) CGenUtil.rechercher(new FactureFournisseur(), null, null, c, " and id='"+this.getIdmere()+"'");
           if(lsf==null || lsf.length==0)throw new Exception("facture fournisseur introuvable");
           if(lsf[0].getEntite().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0) throw new Exception("Action non permise");
           if(lsf[0].getEtat()>= ConstanteEtat.getEtatValider())throw new Exception("facture fournisseur deja valid&eacute;");
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw ex;
        }
        finally{
            if(c!=null && verif==1)c.close();
        }
    }

    @Override
    public void controler(Connection c) throws Exception {
        //this.verifPoint(c);
    }
    
    @Override
    public void controlerUpdate(Connection c) throws Exception {
        //this.verifPoint(c);
    }

    @Override
    public int deleteToTableWithHisto(String refUser, Connection con) throws Exception {
        //this.verifPoint(con);
        return super.deleteToTableWithHisto(refUser, con); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
