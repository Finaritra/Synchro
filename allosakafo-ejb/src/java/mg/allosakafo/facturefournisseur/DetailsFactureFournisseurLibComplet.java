/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.facturefournisseur;

import java.sql.Date;

/**
 *
 * @author JESSI
 */
public class DetailsFactureFournisseurLibComplet extends FactureFournisseurLettre{
     private String idingredient,idmere,compte;
    private double qte,pu,livre,reste;

    public double getReste() {
        return reste;
    }

    public void setReste(double reste) {
        this.reste = reste;
    }

    public double getLivre() {
        return livre;
    }

    public void setLivre(double livre) {
        this.livre = livre;
    }
    private String idtypefacture,idtypefacturelib;

    public DetailsFactureFournisseurLibComplet() {
        this.setNomTable("detailsfflibcomplet");
    }

    public String getIdtypefacture() {
        return idtypefacture;
    }

    public void setIdtypefacture(String idtypefacture) {
        this.idtypefacture = idtypefacture;
    }

    public String getIdtypefacturelib() {
        return idtypefacturelib;
    }

    public void setIdtypefacturelib(String idtypefacturelib) {
        this.idtypefacturelib = idtypefacturelib;
    }

    public String getIdingredient() {
        return idingredient;
    }

    public void setIdingredient(String idingredient) {
        this.idingredient = idingredient;
    }

    public String getIdmere() {
        return idmere;
    }

    public void setIdmere(String idmere) {
        this.idmere = idmere;
    }

    public String getCompte() {
        return compte;
    }

    public void setCompte(String compte) {
        this.compte = compte;
    }

    public double getQte() {
        return qte;
    }

    public void setQte(double qte) {
        this.qte = qte;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumFact() {
        return numFact;
    }

    public void setNumFact(String numFact) {
        this.numFact = numFact;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public String getIdFournisseur() {
        return idFournisseur;
    }

    public void setIdFournisseur(String idFournisseur) {
        this.idFournisseur = idFournisseur;
    }

    public String getIdDevise() {
        return idDevise;
    }

    public void setIdDevise(String idDevise) {
        this.idDevise = idDevise;
    }

    public double getMontantTTC() {
        return montantTTC;
    }

    public void setMontantTTC(double montantTTC) {
        this.montantTTC = montantTTC;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public double getIdTVA() {
        return idTVA;
    }

    public void setIdTVA(double idTVA) {
        this.idTVA = idTVA;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Date getDateEmission() {
        return dateEmission;
    }

    public void setDateEmission(Date dateEmission) {
        this.dateEmission = dateEmission;
    }

}
