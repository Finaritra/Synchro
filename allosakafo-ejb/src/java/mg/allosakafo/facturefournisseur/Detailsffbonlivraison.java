package mg.allosakafo.facturefournisseur;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author JESSI
 */
public class Detailsffbonlivraison extends DetailsFactureFournisseur{
    private double livre,reste,restelivre;
    private String idblfille,idblmere;

    public double getRestelivre() {
        return restelivre;
    }

    public void setRestelivre(double restelivre) {
        this.restelivre = restelivre;
    }

    public String getIdblfille() {
        return idblfille;
    }

    public void setIdblfille(String idblfille) {
        this.idblfille = idblfille;
    }

    public String getIdblmere() {
        return idblmere;
    }

    public void setIdblmere(String idblmere) {
        this.idblmere = idblmere;
    }

    public Detailsffbonlivraison() {
        this.setNomTable("detailsffbonlivraison");
    }

    public double getLivre() {
        return livre;
    }

    public void setLivre(double livre) {
        this.livre = livre;
    }

    public double getReste() {
        return reste;
    }

    public void setReste(double reste) {
        this.reste = reste;
    }
}
