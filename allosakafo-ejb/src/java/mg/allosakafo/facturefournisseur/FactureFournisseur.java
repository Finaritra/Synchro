package mg.allosakafo.facturefournisseur;

import bean.*;
import mg.allosakafo.facture.Client;
import java.sql.Connection;
import java.sql.Date;
import mg.allosakafo.livraison.LivraisonApresCommande;
import service.AlloSakafoService;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;
/*
import ded.Visa;
import ded.VisaUtil;
import ded.OrdonnerPayement;
import ded.OpUtil;
import lc.LigneCredit;
import facture.Client;
*/

public class FactureFournisseur extends ClassEtat {

    public String id;
  public String numFact;
  public java.sql.Date daty;
  public String idFournisseur;
  public String idDevise;
  public double montantTTC;
  public String remarque;
  public double idTVA;
  public String designation;
  public java.sql.Date dateEmission;
  private String resp;
  private java.sql.Date datyecheance;
  private String idFournisseurlibelle;
  String entite;

    public String getEntite() {
        return entite;
    }

    public void setEntite(String entite) {
        this.entite = entite;
    }

    @Override
    public void controlerUpdate(Connection c) throws Exception {
        //this.verifPoint(c);  
    }
  
    @Override
    public void controler(Connection c) throws Exception {
        //this.verifPoint();   
    }
    
    public void verifPoint() throws Exception{
        if(this.getEntite().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0){
            throw new Exception("Action non permise");
        }
    }
    
    @Override
    public int annulerVisa(String user, Connection c) throws Exception {
        //verifPoint(c);
        return super.annulerVisa(user, c); 
    }
    
    public void verifPoint(Connection c) throws Exception{
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            
            String aWhere =  " and id like '"+this.getId()+"'";
            FactureFournisseur[] ff = (FactureFournisseur[]) CGenUtil.rechercher(new FactureFournisseur(), null, null, c, aWhere);
            if(ff.length > 0){
                ff[0].verifPoint();
            }
            
            if(c!=null && verif==1) c.commit();
        } catch (Exception e) {
            if (c != null && verif == 1) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
    }
  
  public FactureFournisseur() {
    super.setNomTable("FactureFournisseur");
  }
  public FactureFournisseur(String nomTable) {
    super.setNomTable(nomTable);
  }
  public void construirePK(Connection c) throws Exception {
        this.preparePk("FF", "getseqfacturefournisseur");
        this.setId(makePK(c));
    }
  public FactureFournisseur(String nomTable,String idf, String numFact,String designation, String daty, String fournisseur,  String tva, String montantTTC, String remarque, String dateemission, String idDevise)throws Exception{
      super.setNomTable(nomTable);
      setNumFact(numFact);
      setId (idf);
      setDesignation(designation);
      setDateEmission( java.sql.Date.valueOf(dateemission));
        setIdFournisseur(fournisseur);
        setMontantTTC((new Double(montantTTC)).doubleValue());

      setIdTVA(Integer.parseInt(tva));
      setRemarque(remarque);
      setIdDevise(idDevise);

  }
  public FactureFournisseur(String nomTable,String idf, String numFact,String designation, java.sql.Date daty, String fournisseur,  double tva, double montantTTC, String remarque, java.sql.Date dateemission, String idDevise)throws Exception{
      super.setNomTable(nomTable);
      setNumFact(numFact);
      setId (idf);
      setDesignation(designation);
      setDateEmission( dateemission);
      setIdFournisseur(fournisseur);
      setMontantTTC(montantTTC);
      setDaty(daty);
      setIdTVA(tva);
      setRemarque(remarque);
      setIdDevise(idDevise);

  }
  public FactureFournisseur(String idFF,String numFact,String designation,java.sql.Date dE,Date daty, String idFrns,int idTVA,double mTTC,String rmq, String idDevise) throws Exception{

     setNumFact(numFact);
     setId (idFF);
     this.setDaty(daty);
         setDateEmission(dE);
         setIdFournisseur(idFrns);
         setMontantTTC(mTTC);
         setIdTVA(idTVA);
         setRemarque(rmq);
         setIndicePk("FF");
         setDesignation(designation);
         setIdDevise(idDevise);
     }
   public FactureFournisseur(String designation,java.sql.Date dE,Date daty, String idFrns,String rmq, String idDevise) throws Exception{
       this.setNomTable("FactureFournisseur");
     this.setDaty(daty);
         setDateEmission(dE);
         setIdFournisseur(idFrns);
         setRemarque(rmq);
         setIndicePk("FF");
         setDesignation(designation);
         setIdDevise(idDevise);
     }

     public FactureFournisseur(Date dE,Date dR, String idFrns,Double mTHT) {
     super.setNomTable("FactureFournisseur");
         setDateEmission(dE);
     }
	 public FactureFournisseur(String nomTable,String numFact,String designation,String dE, String dR, String idFrns,String idTVA,String mTTC,String rmq, String idDevise)throws Exception {
       //Utilis�e pour la m�thode cr�ation
       super.setNomTable(nomTable);
       setNumFact(numFact);
       if(String.valueOf(dE).compareTo("") == 0 || dE == null)
         setDateEmission(Utilitaire.dateDuJourSql());
       else
         setDateEmission((Utilitaire.string_date("dd/MM/yyyy", dE)));
       this.setDaty(Utilitaire.string_date("dd/MM/yyyy", dR));
       setIdFournisseur(idFrns);
       setMontantTTC(Utilitaire.stringToDouble(mTTC));

       setIdTVA(Utilitaire.stringToDouble(idTVA));
       setRemarque(rmq);
       setIdDevise(idDevise);
       setDesignation(designation);
       CaractTable ct=(CaractTable)new bean.CaractTableUtil().rechercher(2,nomTable)[0];
       setIndicePk(ct.getNomSeq());
       setNomProcedureSequence(ct.getNomProc());
       setId(makePK());
     }
  public String getAttributIDName() {
return "id";
  }
  public String getTuppleID() {
    return this.getId();
  }
  public static void main(String[] args) {
    FactureFournisseur factureFournisseur1 = new FactureFournisseur();
  }
  public void setNumFact(String numFact) {
    this.numFact = numFact;
  }
  public java.sql.Date getDateEmission() {
  return dateEmission;
  }
  
  public String getNumFact() {
    return numFact;
  }
  public void setDesignation(String designation) {
    this.designation = designation;
  }
  public String getDesignation() {
    return designation;
  }
  public void setId(String id) {
    this.id = id;
  }
  public String getId() {
    return id;
  }
  public void setDaty(java.sql.Date daty) {
    this.daty = daty;
  }
  public java.sql.Date getDaty() {
    return daty;
  }
  public void setIdFournisseur(String idFournisseur) throws Exception {
    if(this.getMode().compareToIgnoreCase("modif")==0&&(idFournisseur==null||idFournisseur.compareToIgnoreCase("")==0))throw new Exception("Fournisseur non valide");
    this.idFournisseur = idFournisseur;
  }
  public String getIdFournisseur() {
    return idFournisseur;
  }
  public void setIdDevise(String idDevise) {
    this.idDevise = idDevise;
  }
  public String getIdDevise() {
    return idDevise;
  }
  public void setMontantTTC(double montantTTC) throws Exception {
    
    this.montantTTC = montantTTC;
    
  }
  public double getMontantTTC() {
    return montantTTC;
  }
  public void setRemarque(String remarque) {
    this.remarque = remarque;
  }
  public String getRemarque() {
    return remarque;
  }
  public void setIdTVA(double idTVA) throws Exception {
      if(this.getMode().compareToIgnoreCase("select")==0)
      {
        this.idTVA = idTVA;
        return;
      }
      if(idTVA<0)throw new Exception("tva invalide");
      this.idTVA = idTVA;
  }
  public double getIdTVA() {
    return idTVA;
  }
  public double calculMontantTva()
  {
    return getIdTVA();
  }
  public void setDateEmission(java.sql.Date dateEmission) {
      this.dateEmission = dateEmission;
  }
  public void setResp(String resp) {
    this.resp = resp;
  }
  public String getResp() {
    return resp;
  }
  public void setDatyecheance(java.sql.Date datyecheance) {
    this.datyecheance = datyecheance;
  }
  public java.sql.Date getDatyecheance() {
    return datyecheance;
  }

    public String getIdFournisseurlibelle() {
        return idFournisseurlibelle;
    }

    public void setIdFournisseurlibelle(String idFournisseurlibelle) {
        this.idFournisseurlibelle = idFournisseurlibelle;
    }
  
  
  /*
  public FactureFournisseur(String nomTable,String numFact,String designation,String dE, String dR, String idFrns,String idTVA,String mTTC,String rmq, String idDevise,String e,String a)throws Exception {
       //Utilis�e pour la m�thode cr�ation
       super.setNomTable(nomTable);
       setNumFact(numFact);
       if(String.valueOf(dE).compareTo("") == 0 || dE == null)
         setDateEmission(Utilitaire.dateDuJourSql());
       else
         setDateEmission((Utilitaire.string_date("dd/MM/yyyy", dE)));
       this.setDaty(Utilitaire.string_date("dd/MM/yyyy", dR));
       setIdFournisseur(Client.getIdDyn(idFrns));
       setMontantTTC(Utilitaire.stringToDouble(mTTC));

       setIdTVA(Utilitaire.stringToDouble(idTVA));
       setRemarque(rmq);
       setIdDevise(idDevise);
       setDesignation(designation);
       setIndicePk("FCP");
       setNomProcedureSequence("GETSEQFACTURECLIENTPROFORMA");
       setId(makePK());
     }
  public FactureFournisseur(String nomTable,String numFact,String designation,String dE, String dR, String idFrns,String idTVA,String mTTC,String rmq, String idDevise)throws Exception {
       //Utilis�e pour la m�thode cr�ation
       super.setNomTable(nomTable);
       setNumFact(numFact);
       if(String.valueOf(dE).compareTo("") == 0 || dE == null)
         setDateEmission(Utilitaire.dateDuJourSql());
       else
         setDateEmission((Utilitaire.string_date("dd/MM/yyyy", dE)));
       this.setDaty(Utilitaire.string_date("dd/MM/yyyy", dR));
       setIdFournisseur(Client.getIdDyn(idFrns));
       setMontantTTC(Utilitaire.stringToDouble(mTTC));

       setIdTVA(Utilitaire.stringToDouble(idTVA));
       setRemarque(rmq);
       setIdDevise(idDevise);
       setDesignation(designation);
       CaractTable ct=(CaractTable)new bean.CaractTableUtil().rechercher(2,nomTable)[0];
       setIndicePk(ct.getNomSeq());
       setNomProcedureSequence(ct.getNomProc());
       setId(makePK());
     }
  public LigneCredit[] getLigne(String nomTable,Connection c) throws Exception
  {
    if(nomTable.compareToIgnoreCase("FACTURECLIENTLC")==0) return getLigne(nomTable,c,"LigneCreditRecette");
    if(nomTable.compareToIgnoreCase("FACTUREFOURNISSEURLC")==0) return getLigne(nomTable,c,"LigneCredit");
    return null;
  }
  public LigneCredit[] getLigneClient(Connection c) throws Exception
  {
    return getLigne("FACTURECLIENTLC",c);
  }
  public LigneCredit[] getLigneFourn(Connection c) throws Exception
  {
    return getLigne("FACTUREFOURNISSEURLC",c);
  }
  public LigneCredit[] getLigne(String nomTable,Connection c,String nomTableLc) throws Exception
  {
    UnionIntraTable iut=new UnionIntraTable();
    iut.setNomTable(nomTable);
    iut.setId1(this.getId());
    UnionIntraTable result[]=(UnionIntraTable[])CGenUtil.rechercher(iut,null,null,c,"");
    LigneCredit lc=new LigneCredit();
    lc.setNomTable(nomTableLc);
    LigneCredit[] valiny=new LigneCredit[result.length];
    for(int i=0;i<result.length;i++)
    {
      lc.setIdLigne(result[i].getId2());
      valiny[i]=((LigneCredit[])CGenUtil.rechercher(lc,null,null,c,""))[0];
    }
    return valiny;
  }
  public boolean estPub(Connection c) throws Exception
  {
    FactureFournisseurCompte ffc=new FactureFournisseurCompte();
    ffc.setNomTable("FACTURECLIENTCOMPTE");
    ffc.setId(getTuppleID());
    FactureFournisseurCompte[] listeFc=(FactureFournisseurCompte[])CGenUtil.rechercher(ffc,null,null,c," and compteType not like '7011%'");
    if(listeFc.length>0) throw new Exception("Ceci n est pas une facture de publicite");
    return false;
  }
  public boolean estVente(Connection c) throws Exception
  {
    FactureFournisseurCompte ffc = new FactureFournisseurCompte();
    ffc.setNomTable("FACTURECLIENTCOMPTE");
    ffc.setId(getTuppleID());
    //FactureFournisseurCompte[] listeFc=(FactureFournisseurCompte[])CGenUtil.rechercher(ffc,null,null,c," and compteType not like '7010%'");
    //if(listeFc.length>0) throw new Exception("Ceci n est pas une facture de vente");
    return true;
  }
  public Visa getVisa(Connection c,String nomTable) throws Exception
  {
    VisaUtil vu = new VisaUtil();
    vu.setNomTable(nomTable);
    Visa[]v=(Visa[])vu.rechercher(4,this.getTuppleID(),c);
    if (v.length==0)return null;
    return v[0];
  }
  public boolean estAnnulable() throws Exception
  {
    Connection c=null;
    try {
      c=new UtilDB().GetConn();
      return estAnnulable(c);
    }
    catch (Exception ex) {
      throw new Exception(ex.getMessage()) ;
    }
    finally{
      if(c!=null)c.close();
    }
  }
  public boolean estAnnulable(Connection c) throws Exception
  {
    OrdonnerPayement[]op=this.getOpVise(c);
    Visa v=getVisa(c,"VISAFACTUREF");
    if(op.length>0||v==null) return false;
    return true;
  }
  public boolean estAnnulableClient() throws Exception
  {
    Connection c=null;
    try {
      c=new UtilDB().GetConn();
      return estAnnulableClient(c);
    }
    catch (Exception ex) {
      throw new Exception(ex.getMessage()) ;
    }
    finally{
      if(c!=null)c.close();
    }
  }
  public boolean estAnnulableClient(Connection c) throws Exception
  {
    OrdonnerPayement[]op=this.getOrVise(c);
    Visa v=getVisa(c,"VISAFACTURECLIENT");
    if(op.length>0||v==null) return false;
    return true;
  }
  
  public Visa estIlVise(Connection c) throws Exception
  {
    VisaUtil vu=new VisaUtil();
    vu.setNomTable("VISAFACTUREF");
    Visa[]v=(Visa[])vu.rechercher(4,this.getTuppleID(),c);
    if(v.length==0)return null;
    return v[0];
  }
  public Visa estIlVise(String nomTable,Connection c) throws Exception
  {
    VisaUtil vu=new VisaUtil();
    vu.setNomTable(nomTable);
    Visa[]v=(Visa[])vu.rechercher(4,this.getTuppleID(),c);
    if(v.length==0)return null;
    return v[0];
  }

  public OrdonnerPayement[] getOpCree(Connection c) throws Exception
  {
    return getOpRelatif("opvise",c);
  }
  public OrdonnerPayement[] getOpRelatif(String nomTable,Connection c) throws Exception
  {
    OpUtil opu=new OpUtil();
    opu.setNomTable (nomTable);
    return ((OrdonnerPayement[])opu.rechercher(2,this.getTuppleID(),c));
  }
  public OrdonnerPayement[] getOrCree(Connection c) throws Exception
  {
    return getOpRelatif("orvise",c);
  }
  public OrdonnerPayement[] getOpVise(Connection c) throws Exception
  {
    OpUtil opu=new OpUtil();
    opu.setNomTable("OpVise");
    return ((OrdonnerPayement[])opu.rechercher(2,this.getTuppleID(),c));
  }
  public OrdonnerPayement[] getOrVise(Connection c) throws Exception
{
  OpUtil opu=new OpUtil();
  opu.setNomTable("orvise");
  return ((OrdonnerPayement[])opu.rechercher(2,this.getTuppleID(),c));
  }*/
  
    public void gererFournisseur(String refuser,Connection c)throws Exception{
        int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }
            Client new_clt = new Client();
            new_clt.setNomTable("fournisseurProduits");
            System.out.println(" libelle forunisseur : "+this.getIdFournisseurlibelle()+" idfournisseur : "+this.getIdFournisseur());
            String id_popup = this.getIdFournisseur();
            Client fournisseur = new Client();
            fournisseur.setNomTable("fournisseurProduits");
            Client[] liste=(Client[]) CGenUtil.rechercher(fournisseur, null, null, c, " and id = '"+id_popup+"'");
            if(liste.length == 0){
                String[] frs = this.getIdFournisseurlibelle().split("-");
                if(frs.length==0)throw new Exception("fournisseur obligatoire");
                String where =" and replace (telephone,' ','') like replace ('%"+frs[0]+"%',' ','')";
                if(frs.length>1)where+=" or upper(nom) like upper('%"+frs[1]+"%')";
                System.out.println("sqllll : "+where);
                Client[] listeClt =(Client[]) CGenUtil.rechercher(fournisseur, null, null, c,where);
                if(listeClt.length == 0){
                    if(frs.length>1) new_clt.setNom(frs[1]);
                    new_clt.setTelephone(frs[0]);
                    new_clt.construirePK(c);
                    new_clt.insertToTableWithHisto(refuser, c);
                    
                }else{
                    new_clt = listeClt[0];
                }
            }else{
                new_clt = liste[0];
            }
        
            this.setIdFournisseur(new_clt.getId());
            
            if(c!=null && verif==1)c.commit();
        }catch(Exception e){
            if(c!=null && verif==1)c.rollback();
            throw e;
        }finally{
            if(c!=null && verif==1)c.close();
        }
        
    }
    public void controlerVisa(Connection c)throws Exception{
        //FACTUREFOURNISSEURCOMPTA
        int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                verif=1;
            }
           FactureFournisseur ff =  new FactureFournisseur();
           ff.setNomTable("FACTUREFOURNISSEURCOMPTA");
           ff.setId(this.getId());
            FactureFournisseur[] ls = (FactureFournisseur[])CGenUtil.rechercher(ff, null, null, c, "");
            if(ls==null || ls.length==0)throw new Exception("facture fournisseur introuvable");
            
        }
        catch(Exception ex){
           ex.printStackTrace();
           throw ex;
        }
        finally{
            if(verif==1 && c!=null){
                c.close();
            }
        }
    }
}