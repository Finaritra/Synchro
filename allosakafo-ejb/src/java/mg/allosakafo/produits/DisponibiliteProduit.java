/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

/**
 *
 * @author ACER
 */
public class DisponibiliteProduit extends ProduitsTypeLibelle{
    private String pointindisp, idproduit;
    
    public DisponibiliteProduit(){
        this.setNomTable("DISPONIBILITE_PRODUIT");
    }
    
    public String getPointindisp() {
        return pointindisp;
    }

    public void setPointindisp(String pointindisp) {
        this.pointindisp = pointindisp;
    }

    public String getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(String idproduit) {
        this.idproduit = idproduit;
    }
}
