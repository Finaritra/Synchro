/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author Joe
 */
public class TarifProduits extends ClassMAPTable{
    
    private String id, produit, observation;
    private Date dateapplication;
    private double montant;
    
    public TarifProduits() {
        this.setNomTable("as_prixproduit");
    }
	
	public TarifProduits(String produit, String observation, Date dateapplication, double montant) {
        this.setNomTable("as_prixproduit");
		this.setProduit(produit);
		this.setObservation(observation);
		this.setDateapplication(dateapplication);
		this.setMontant(montant);
    }
	
    public void construirePK(Connection c) throws Exception {
        this.preparePk("TPR", "getSeqASTarifProduits");
        this.setId(makePK(c));
    }
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Date getDateapplication() {
        return dateapplication;
    }

    public void setDateapplication(Date dateapplication) {
        this.dateapplication = dateapplication;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }
    
    
}
