/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import utilitaire.Utilitaire;

/**
 *
 * @author Notiavina
 */
public class ProduitsTypeLibelle extends ProduitsLibelle{
    private String idtypeproduit, idsouscategorie, ingredients;
    
    public ProduitsTypeLibelle() {
        this.setNomTable("as_produittype_libelle");
    }

    public String getIdtypeproduit() {
        return idtypeproduit;
    }

    public String getIngredients() {
        return ingredients;
    }

    public String getIdsouscategorie() {
        return idsouscategorie;
    }

    public String[] getListeIngredients(){
        return Utilitaire.split(ingredients, ";");
    }
    public void setIdtypeproduit(String idtypeproduit) {
        this.idtypeproduit = idtypeproduit;
    }

    public void setIdsouscategorie(String idsouscategorie) {
        this.idsouscategorie = idsouscategorie;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }    
}
