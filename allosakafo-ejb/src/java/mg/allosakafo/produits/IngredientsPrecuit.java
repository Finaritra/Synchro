/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.ClassMAPTable;

/**
 *
 * @author ITU
 */
public class IngredientsPrecuit extends ClassMAPTable{
    private String id;
    private String id1;
    private String id2;
    private int quantite1;
    private int quantite2;
    public IngredientsPrecuit()
    {
        this.setNomTable("ingredient_precuit");
    }
    @Override
    public String getTuppleID() {
        return this.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAttributIDName() {
        return "id"; //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the id1
     */
    public String getId1() {
        return id1;
    }

    /**
     * @param id1 the id1 to set
     */
    public void setId1(String id1) {
        this.id1 = id1;
    }

    /**
     * @return the id2
     */
    public String getId2() {
        return id2;
    }

    /**
     * @param id2 the id2 to set
     */
    public void setId2(String id2) {
        this.id2 = id2;
    }

    /**
     * @return the quantite1
     */
    public int getQuantite1() {
        return quantite1;
    }

    /**
     * @param quantite1 the quantite1 to set
     */
    public void setQuantite1(int quantite1) {
        this.quantite1 = quantite1;
    }

    /**
     * @return the quantite2
     */
    public int getQuantite2() {
        return quantite2;
    }

    /**
     * @param quantite2 the quantite2 to set
     */
    public void setQuantite2(int quantite2) {
        this.quantite2 = quantite2;
    }
    
}
