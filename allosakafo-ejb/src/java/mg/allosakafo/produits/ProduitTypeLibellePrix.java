/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

/**
 *
 * @author Notiavina
 */
public class ProduitTypeLibellePrix extends ProduitsLibelle{
    private int val_calorie, priorite;
    private double revient;

    public double getRevient() {
        return revient;
    }

    public void setRevient(double revient) {
        this.revient = revient;
    }
    
    public ProduitTypeLibellePrix(){
        super.setNomTable("as_produits_type_libelleprix");
    }

    public int getVal_calorie() {
        return val_calorie;
    }

    public int getPriorite() {
        return priorite;
    }

    public void setVal_calorie(int val_calorie) {
        this.val_calorie = val_calorie;
    }

    public void setPriorite(int priorite) {
        this.priorite = priorite;
    }
}
