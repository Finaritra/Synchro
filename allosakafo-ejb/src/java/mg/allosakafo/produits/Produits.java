/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.AdminGen;
import bean.CGenUtil;
import bean.ClassMAPTable;
import java.sql.Connection;
import java.util.ArrayList;
import user.UserEJB;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Joe
 */
public class Produits extends ClassMAPTable{
    
    private String id, nom, designation, photo, typeproduit, calorie, pa,idIngredient;
    private double prixl,poidsprecuit;

    public double getPoidsprecuit() {
        return poidsprecuit;
    }

    public void setPoidsprecuit(double poidsprecuit) {
        this.poidsprecuit = poidsprecuit;
    }

    public String getIdIngredient() {
        return idIngredient;
    }

    public void setIdIngredient(String idIngredient) {
        this.idIngredient = idIngredient;
    }
    private double poids;
    
    public String getValColLibelle(){
        return nom;
    }
    
    public Produits() {
        this.setNomTable("as_produits");
    }
	public Produits(String nom, String designation, String photo, String typeproduit, String calorie, String pa, double poids) throws Exception{
        this.setNomTable("as_produits");
		this.setNom(nom);
		this.setDesignation(designation);
		this.setPhoto(photo);
		this.setTypeproduit(typeproduit);
		this.setCalorie(calorie);
		this.setPa(pa);
		this.setPoids(poids);
    }
    public void construirePK(Connection c) throws Exception {
        this.preparePk("APS", "getSeqASProduits");
        this.setId(makePK(c));
    }
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }
    public String[] getListePhoto(){
        if(this.photo != null){
            return Utilitaire.split(this.photo, ";");
        }
        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (nom != null && nom.compareToIgnoreCase("") != 0) {
                this.nom = nom;
                return;
            } else {
                throw new Exception("Nom de produits invalide");
            }
        }
        this.nom = nom;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTypeproduit() {
        return typeproduit;
    }

    public void setTypeproduit(String typeproduit) {
        this.typeproduit = typeproduit;
    }

    public String getCalorie() {
        return calorie;
    }

    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    public String getPa() {
        return pa;
    }

    public void setPa(String pa) {
        this.pa = pa;
    }

    public double getPoids() {
        return poids;
    }

    public void setPoids(double poids) {
        this.poids = poids;
    }

    public double getPrixl() {
        return prixl;
    }

    public void setPrixl(double prixl) {
        this.prixl = prixl;
    }
    public RecetteLib[] getRecette(String table,Connection c)throws Exception{
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            RecetteLib crt=new RecetteLib();
            if(table!=null&&table.compareToIgnoreCase("")!=0) crt.setNomTable(table);
            return (RecetteLib[]) CGenUtil.rechercher(new RecetteLib(), null, null, c, " and idproduits = '" + this.getId() + "'");
        }
        catch(Exception e)
        {
            if( c != null ){c.rollback();}
            throw e;
        }
        finally{
            if(c!=null && estOuvert==true)c.close();
        }
    }
    
    public void insertIngredientRecetteBoisson(double montant,String user,Connection c)throws Exception{
        int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                verif=1;
            }
            if(this.getTypeproduit().compareToIgnoreCase("TPD00001")==0){
            Ingredients ing = new Ingredients(this.getNom(), "UNT00005",this.getPhoto(),"CI00001","1",montant,1,0,0,0);
            ing.construirePK(c);
            Recette rec=new Recette(this.getId(), "UNT00005",ing.getId() , 1);
            rec.construirePK(c);
            ing.insertToTableWithHisto(user, c);
            rec.insertToTableWithHisto(user, c);
            
            }
          if(verif==1)c.commit();
        }
        catch(Exception ex){
            if( c != null ){c.rollback();}
            throw ex;
        }
        finally{
            if(c!=null && verif==1)c.close();
        }
    }
    
    public ArrayList<Ingredients> decomposer(Connection c)throws Exception{
       ArrayList<Ingredients> liste=new ArrayList<Ingredients>();
       int verif=0;
        try{
            
            if(c==null){
                c=new UtilDB().GetConn();
                verif=1;
            } 
          this.decomposerIngredient(liste,this.getId(), c);           
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw ex;
        }
        finally{
            if(c!=null && verif==1)c.close();
        }
        return liste;
    }
    public Recette[] decomposerBase(Connection c) throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            String req="select ing.pu as qteav,cast(0 as number(10,2)) as qtetotal ,ing.unite as idproduits, ing.LIBELLE as idingredients,sum(rec.quantite*cast (nvl(to_number(SUBSTR((SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1)))," +
            "(INSTR(SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1)), '/', -1))+1)),1) as number(10,2))) as quantite" +
            " from as_recettecompose rec,as_ingredients_ff ing  where rec.compose=0 and rec.IDINGREDIENTS=ing.id" +
            "  start with idproduits ='"+this.getId()+"'" +
            "  connect by prior idingredients = idproduits" +
            "  group by ing.unite, ing.libelle,ing.pu";
            Recette rec=new Recette();
            rec.setNomTable("recettemontant");
            return (Recette[])CGenUtil.rechercher(rec ,req, c) ;
           
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(estOuvert==true&&c!=null)c.close();
        }
    }
    public Recette[] decomposerBase(int qte, Connection c) throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            if(qte==0)qte=1;
            String req="select ing.pu as qteav,cast(0 as number(10,2)) as qtetotal ,ing.unite as idproduits, ing.LIBELLE as idingredients,"+qte+"*sum(rec.quantite*cast (nvl(to_number(SUBSTR((SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1)))," +
            "(INSTR(SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1)), '/', -1))+1)),1) as number(10,2))) as quantite" +
            " from as_recettecompose rec,as_ingredients_ff ing  where rec.compose=0 and rec.IDINGREDIENTS=ing.id" +
            "  start with idproduits ='"+this.getId()+"'" +
            "  connect by prior idingredients = idproduits" +
            "  group by ing.unite, ing.libelle,ing.pu";
            Recette rec=new Recette();
            rec.setNomTable("recettemontant");
            return (Recette[])CGenUtil.rechercher(rec ,req, c) ;
           
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(estOuvert==true&&c!=null)c.close();
        }
    }
    public static Recette[] decomposerBase(String id, int qte, Connection c) throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            if(qte==0)qte=1;
            String req="select ing.pu as qteav,cast(0 as number(10,2)) as qtetotal ,ing.unite as idproduits, ing.LIBELLE as idingredients,"+qte+"*sum(rec.quantite*cast (nvl(to_number(SUBSTR((SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1)))," +
            "(INSTR(SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1)), '/', -1))+1)),1) as number(10,2))) as quantite" +
            " from as_recettecompose rec,as_ingredients_ff ing  where rec.compose=0 and rec.IDINGREDIENTS=ing.id" +
            "  start with idproduits ='"+id+"'" +
            "  connect by prior idingredients = idproduits" +
            "  group by ing.unite, ing.libelle,ing.pu";
            Recette rec=new Recette();
            rec.setNomTable("recettemontant");
            return (Recette[])CGenUtil.rechercher(rec ,req, c) ;
           
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(estOuvert==true&&c!=null)c.close();
        }
    }
    public void decomposerIngredient(ArrayList<Ingredients> liste,String id,Connection c)throws Exception{
        
        try{
           RecetteLib rec= new RecetteLib();
           rec.setNomTable("AS_RECETTE_LIBCOMPLET_CAT");
            RecetteLib[] lsrec=(RecetteLib[])CGenUtil.rechercher(rec, null, null, c, " and idproduits='"+id+"'");
            if(lsrec==null || lsrec.length==0){
               return;
            }
            else{
                for(int i=0;i<lsrec.length;i++){
                   
                    if(lsrec[i].getCategorieingredient()!=null && lsrec[i].getCategorieingredient().compareToIgnoreCase("CI00007")==0){
                         this.decomposerIngredient(liste,lsrec[i].getIdingredients(), c);
                    }
                    else{
                        //String id, String libelle, String categorieingredient, double quantiteparpack
                         liste.add(new Ingredients(lsrec[i].getIdingredients(),  lsrec[i].getLibelleingredient(), lsrec[i].getCategorieingredient(),1));
                    
                    }
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw ex;
        }
    }
    public void getProduitById(Connection c) throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            Produits[] lp=(Produits[])CGenUtil.rechercher(this ,null,null, c," and id='"+this.getId()+"'") ;
            if(lp.length==0) throw new Exception("Produit introuvable");
            this.setDesignation(lp[0].getDesignation());
            this.setNom(lp[0].getNom());
            this.setPhoto(lp[0].getPhoto());
            this.setTypeproduit(lp[0].getTypeproduit());
            this.setCalorie(this.getCalorie());
            this.setPoids(lp[0].getPoids());
            this.setPa(lp[0].getPa());
            this.setIdIngredient(lp[0].getIdIngredient());
            this.setPrixl(lp[0].getPrixl());
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(estOuvert==true&&c!=null)c.close();
        }
    }
    public Recette[] getRecetteTsotra(Connection c)throws Exception{
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            return (Recette[]) CGenUtil.rechercher(new Recette(), null, null, c, " and idproduits = '" + this.getId() + "'");
        }
        catch(Exception e)
        {
            if( c != null ){c.rollback();}
            throw e;
        }
        finally{
            if(c!=null && estOuvert==true)c.close();
        }
    }
     public TarifProduits[] getTarifProduit(Connection c)throws Exception{
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            return (TarifProduits[]) CGenUtil.rechercher(new TarifProduits(), null, null, c, " and produit = '" + this.getId() + "'");
        }
        catch(Exception e)
        {
            if( c != null ){c.rollback();}
            throw e;
        }
        finally{
            if(c!=null && estOuvert==true)c.close();
        }
    }
    public Produits dupliquer(UserEJB u,Connection c)throws Exception{
        boolean estOuvert=false;
        Produits p=null;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
                c.setAutoCommit(false);
            }
            this.getProduitById(c);
            Recette[] lsrec=this.getRecetteTsotra(c);
            TarifProduits[] ltarif=this.getTarifProduit(c);
            this.construirePK(c);
            u.createObject(this, c);
            if(lsrec.length>0){
                for(int i=0;i<lsrec.length;i++){
                    lsrec[i].setIdproduits(this.getId());
                    lsrec[i].construirePK(c);
                    u.createObject(lsrec[i], c);
                }
            }
            if(ltarif.length>0){
                for(int i=0;i<ltarif.length;i++){
                    ltarif[i].setProduit(this.getId());
                    ltarif[i].construirePK(c);
                    u.createObject(ltarif[i], c);
                }
            }
            if(estOuvert)c.commit();
        }
        catch(Exception e)
        {
            if( c != null ){c.rollback();}
            throw e;
        }
        finally{
            if(c!=null && estOuvert==true)c.close();
        }
        return p;
    }
}
