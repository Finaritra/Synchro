/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author Joe
 */
public class ProduitsPrix extends ClassMAPTable{
    
    private String id, nom, designation, photo, typeproduit, calorie, pa, observation;
    private double poids, montant,prixl;

    public double getPrixl() {
        return prixl;
    }

    public void setPrixl(double prixl) {
        this.prixl = prixl;
    }
    private Date dateapplication;
    
    
    public ProduitsPrix() {
        this.setNomTable("as_produit_prix");
    }
	
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) throws Exception{
        if (getMode().compareToIgnoreCase("modif") == 0) {
            if (nom != null && nom.compareToIgnoreCase("") != 0) {
                this.nom = nom;
                return;
            } else {
                throw new Exception("Nom de produits invalide");
            }
        }
        this.nom = nom;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTypeproduit() {
        return typeproduit;
    }

    public void setTypeproduit(String typeproduit) {
        this.typeproduit = typeproduit;
    }

    public String getCalorie() {
        return calorie;
    }

    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    public String getPa() {
        return pa;
    }

    public void setPa(String pa) {
        this.pa = pa;
    }

    public double getPoids() {
        return poids;
    }

    public void setPoids(double poids) {
        this.poids = poids;
    }
    
    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Date getDateapplication() {
        return dateapplication;
    }

    public void setDateapplication(Date dateapplication) {
        this.dateapplication = dateapplication;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }
}
