/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.ClassEtat;
import java.sql.Connection;

/**
 *
 * @author JESSI
 */
public class IngredientDisponible extends ClassEtat{
    private String id,idingredient;

    public IngredientDisponible() {
        this.setNomTable("ingredient_disponible");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdingredient() {
        return idingredient;
    }

    public void setIdingredient(String idingredient) {
        this.idingredient = idingredient;
    }
    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
     public void construirePK(Connection c) throws Exception {
        this.preparePk("IGD", "getseqingredient_disponible");
        this.setId(makePK(c));
    }
}
