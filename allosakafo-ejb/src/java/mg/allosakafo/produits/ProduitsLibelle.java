package mg.allosakafo.produits;

import bean.ClassMAPTable;
import com.google.gson.annotations.Expose;
import utilitaire.Constante;
import utilitaire.Utilitaire;

/**
 *
 * @author Joe
 */
public class ProduitsLibelle extends Produits{
   
    @Expose
    private double  pu;
    String pointindisp, idpoint;
    
    public ProduitsLibelle() {
        this.setNomTable("as_produits_libelleprix");
    }

    public String getPointindisp() {
        return pointindisp;
    }

    public void setPointindisp(String pointindisp) {
        this.pointindisp = pointindisp;
    }

    public String getIdpoint() {
        return idpoint;
    }

    public void setIdpoint(String idpoint) {
        this.idpoint = idpoint;
    }
    
    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }
    public boolean estDeTypeBoisson()throws Exception
    {
        if (this.getTypeproduit().compareToIgnoreCase(Constante.idTypeBoisson)==0||this.getId().startsWith("LIVR")==true) return true;
        return false;
    }
    
}