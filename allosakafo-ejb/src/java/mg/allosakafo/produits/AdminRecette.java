/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.CGenUtil;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import utilitaire.UtilDB;

/**
 *
 * @author tahina
 */
public class AdminRecette {
    public static List decomposer(List<Recette> liste) throws Exception
    {
        List<Recette> finale=new ArrayList(); 
        for(Recette l:liste)
        {
            if(l.getCompose()==1)
                l.decompose(liste);
            else
            {
                finale.add(l);
            }
        }
        return liste;
    }
    public static List<Recette> getListeInitiale(Connection c)throws Exception
    {
        boolean estOuvert=false;
        try
        {
            if(c==null)
            {
                c=new UtilDB().GetConn();
                estOuvert=true;
            }
            Recette crt=new Recette();
            crt.setNomTable("as_recettecompose");
            Recette[]listeInitiale=(Recette[])CGenUtil.rechercher(crt,null,null,c,"");
            return Arrays.asList(listeInitiale);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if(c!=null&&estOuvert==true)c.close();
        }
        
    }
    
}
