/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.CGenUtil;
import bean.TypeObjet;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import utilitaire.UtilDB;

/**
 *
 * @author MEVA
 */
public class RecetteLib extends Recette {

    private String libelleingredient, idunite, valunite, libelleproduit,categorieingredient,mere;

    public String getMere() {
        return mere;
    }

    public void setMere(String mere) {
        this.mere = mere;
    }
    private double pu;

    public String getCategorieingredient() {
        return categorieingredient;
    }

    public void setCategorieingredient(String categorieingredient) {
        this.categorieingredient = categorieingredient;
    }

   


    public String getValunite() {
        return valunite;
    }

    public void setValunite(String valunite) {
        this.valunite = valunite;
    }

    public RecetteLib() {
        this.setNomTable("AS_RECETTE_LIBCOMPLET");
    }

    public String getLibelleingredient() {
        return libelleingredient;
    }

    public void setLibelleingredient(String libelleingredient) {
        this.libelleingredient = libelleingredient;
    }

    public String getIdunite() {
        return idunite;
    }

    public void setIdunite(String idunite) {
        this.idunite = idunite;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    public String getLibelleproduit() {
        return libelleproduit;
    }

    public void setLibelleproduit(String libelleproduit) {
        this.libelleproduit = libelleproduit;
    }

    
    public RecetteLib[] getRecette(String idproduit,Connection c)throws Exception{
         return (RecetteLib[]) CGenUtil.rechercher(new RecetteLib(), null, null, c, " and idproduits = '" + idproduit + "'");
         
    }
    public String makeWhereBesoin(String query,HttpServletRequest request)throws Exception{
        String sql=query;
        try{
            Field[] field=this.getFieldList();
            for(int i=0;i<field.length;i++){
                String colonne=""+field[i].getName();
                
                if(request.getParameter(colonne)!=null && !request.getParameter(colonne).isEmpty() && colonne.compareToIgnoreCase("id")!=0){
                  
                    sql+=" and upper("+colonne+")";
                    if(field[i].getType()==String.class){
                        sql+=" like upper('%"+request.getParameter(colonne)+"%')";
                    }
                    else{
                        sql+=" ="+request.getParameter(colonne);
                    }
                }
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
            throw e;
        }
         return sql;
    }
    public TypeObjet[] getUnite(Connection c)throws Exception{
         TypeObjet unite = new TypeObjet();
         unite.setNomTable("AS_UNITE");
         return (TypeObjet[]) CGenUtil.rechercher(unite, null, null, c, "");
    }
    public HashMap getRecetteUniter(String idproduit) throws Exception {
        HashMap obj = new HashMap();
        UtilDB util = new UtilDB();
        Connection c = null;
        try {
            try {
                c = util.GetConn();
               RecetteLib[] liste=getRecette(idproduit,c);
               TypeObjet[] lsUniter = getUnite(c);
                obj.put("recette", liste);
                obj.put("unite", lsUniter);
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
            return obj;
        } finally {
                if (c != null) {
                    c.close();
            }
        }
    }

}
