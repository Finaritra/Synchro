/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.ClassMAPTable;

/**
 *
 * @author Notiavina
 */
public class TypeproduitSousCategorie extends ClassMAPTable{
    private String idtypeproduit, libelle_type, idsouscategories;
    
    public TypeproduitSousCategorie(){
        super.setNomTable("as_typeproduit_souscategorie");
    }

    public String getIdtypeproduit() {
        return idtypeproduit;
    }

    public String getLibelle_type() {
        return libelle_type;
    }

    public String getIdsouscategories() {
        return idsouscategories;
    }

    public void setIdtypeproduit(String idtypeproduit) {
        this.idtypeproduit = idtypeproduit;
    }

    public void setLibelle_type(String libelle_type) {
        this.libelle_type = libelle_type;
    }

    public void setIdsouscategories(String idsouscategorie) {
        this.idsouscategories = idsouscategorie;
    }
    
    @Override
    public String getTuppleID() {
        return ""; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAttributIDName() {
       return "" ; //To change body of generated methods, choose Tools | Templates.
    }
}
