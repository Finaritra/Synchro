/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.CGenUtil;
import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import mg.allosakafo.stock.MvtStock;
import mg.allosakafo.stock.MvtStockFille;
import user.UserEJBBean;
import user.ValiderObject;
import utilitaire.Constante;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author MEVA
 */
public class Fabrication extends ClassEtat{
    private String id;
    private Date daty;
    private String ingredient;
    private double qte;
    private String remarque, depot;

    public Fabrication() {
        super.setNomTable("fabrication");
    }

    public String getDepot() {
        return depot;
    }

    public void setDepot(String depot) {
        this.depot = depot;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) throws Exception {
        if(this.getMode().compareTo("modif")==0){
            if(ingredient==null || ingredient.compareTo("")==0) throw new Exception(" Ingr�dient obligatoire");
        }
        this.ingredient = ingredient;
    }

    public double getQte() {
        return qte;
    }

    public void setQte(double qte) throws Exception {
        if(this.getMode().compareTo("modif")==0){
            if(qte<=0) throw new Exception("Quantit� invalide");
        }
        this.qte = qte;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }
    

    @Override
    public String getTuppleID() {
        return this.id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
    public void construirePK(Connection c) throws Exception {
        this.preparePk("FAB", "getSeqFabrication");
        this.setId(makePK(c));
    }
    public void controler(Connection c) throws Exception{
        int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                verif=1;
            }
            Ingredients[] liste =(Ingredients[]) CGenUtil.rechercher(new Ingredients(), null, null, c, " and id='"+this.getIngredient()+"'");
            if(liste.length==0) throw new Exception("Ingr�dient introuvable");
        }catch(Exception e){
            throw e;
        }finally{
            if(c!=null && verif==1) c.close();
        }
    }
    
     public Recette[] decomposer(Connection c)throws Exception{
      Recette[] retour=null;
        int verif = 0;
        try{
            if(c==null){
            c = new UtilDB().GetConn();
             verif =1;
            }
              Recette recette = new Recette();
              recette.setIdproduits(this.getIngredient());
             retour= (Recette[]) CGenUtil.rechercher(recette, null, null, "");
            
             return retour;
        }
       catch (Exception e) {
            throw e;
        } finally {
            if (c != null && verif==1) {
                c.close();
            }
        }
    }
    
    public void fabriquer(String depot,UserEJBBean user,Connection c)throws Exception{
        int verif = 0;
        try{
             if(c==null){
                c = new UtilDB().GetConn();
                 verif =1;
             }
            MvtStock mere=new MvtStock("fabrication","fabrication","", this.getDaty(),depot);
            mere.setTypemvt(Constante.entreeStock);
            mere.setNumbc(this.getId());
            mere.controler(c);
            mere.construirePK(c);
            mere.insertToTableWithHisto(user.getUser().getTuppleID(), c);
            ValiderObject.validerObject(c, mere,user);
            Ingredients ing = new Ingredients();
            ing.setId(this.getIngredient());
            Ingredients[] lsing =(Ingredients[]) CGenUtil.rechercher(ing, null, null, c, "");
            if(lsing==null || lsing.length==0)throw new Exception("ingredient introuvable");
            Recette[] lsrec = this.decomposer(c);
            MvtStockFille mvt=new MvtStockFille(mere.getId(), this.getIngredient(),this.getQte(), 0,lsing[0].getUnite());
            mvt.construirePK(c);
            mvt.insertToTableWithHisto(user.getUser().getTuppleID(), c);
            //ValiderObject.validerObject(c, mvt,user);
            for(int i=0;i<lsrec.length;i++){
                MvtStockFille fille=new MvtStockFille(mere.getId(), lsrec[i].getIdingredients(),0, (this.getQte()*lsrec[i].getQuantite()),lsrec[i].getUnite());
                fille.construirePK(c);
                fille.insertToTableWithHisto(user.getUser().getTuppleID(), c);
                //ValiderObject.validerObject(c, fille,user);
            }
            
           if(verif==1)c.commit();
        }
        catch (Exception e) {
                throw e;
            } 
        finally {
                if (c != null && verif==1) {
                    c.close();
                }
            }
    }
}
