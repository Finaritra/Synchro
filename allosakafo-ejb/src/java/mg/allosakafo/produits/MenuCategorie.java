/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

/**
 *
 * @author Notiavina
 */
public class MenuCategorie {
    private String idtypeproduit, libelle_type;
    private MenuCategorie[] souscategorie;
    
    public MenuCategorie() {
    }
    
    public MenuCategorie(String idtypeproduit, String libelle_type) {
        this.idtypeproduit = idtypeproduit;
        this.libelle_type = libelle_type;
    }

    public String getIdtypeproduit() {
        return idtypeproduit;
    }

    public String getLibelle_type() {
        return libelle_type;
    }

    public MenuCategorie[] getSouscategorie() {
        return souscategorie;
    }

    public void setIdtypeproduit(String idtypeproduit) {
        this.idtypeproduit = idtypeproduit;
    }

    public void setLibelle_type(String libelle_type) {
        this.libelle_type = libelle_type;
    }

    public void setSouscategorie(MenuCategorie[] souscategorie) {
        this.souscategorie = souscategorie;
    }
}
