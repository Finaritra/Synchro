/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import com.google.gson.annotations.Expose;

/**
 *
 * @author HP
 */
public class ProduitsLibelleCategoriser extends ProduitsLibelle{
    @Expose
    private String idsouscategorie;
    
    public ProduitsLibelleCategoriser(){
        this.setNomTable("as_produits_libPrix_souscat");
    }

    public String getIdsouscategorie() {
        return idsouscategorie;
    }

    public void setIdsouscategorie(String idsouscategorie) {
        this.idsouscategorie = idsouscategorie;
    }
}
