/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.ClassEtat;
import com.google.gson.annotations.Expose;

/**
 *
 * @author HP
 */
public class AccompagnementSauce extends ClassEtat{
    @Expose
    private String id;
    @Expose
    private String idsauce;
    @Expose
    private String idaccompagnement;
    @Expose
    private String remarque;

    @Override
    public String getTuppleID() {
        return getId();
    }

    @Override
    public String getAttributIDName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getId() {
        return id;
    }

    public String getIdsauce() {
        return idsauce;
    }

    public String getIdaccompagnement() {
        return idaccompagnement;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIdsauce(String idsauce) {
        this.idsauce = idsauce;
    }

    public void setIdaccompagnement(String idaccompagnement) {
        this.idaccompagnement = idaccompagnement;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }
}
