/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import java.sql.Date;

/**
 *
 * @author Axel
 */
public class RecetteResteLib extends RecetteLib {
    private Date daty;

    public RecetteResteLib(){
        setNomTable("resteingredient");
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }
    
    
}
