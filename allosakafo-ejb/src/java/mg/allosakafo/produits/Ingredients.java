/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.produits;

import bean.CGenUtil;
import bean.ClassMAPTable;
import historique.MapHistorique;
import java.sql.Connection;
import mg.allosakafo.stock.InventaireDetails;
import mg.allosakafo.stock.InventaireMere;
import service.AlloSakafoService;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Joe
 */
public class Ingredients extends ClassMAPTable {

    protected String id, libelle, unite, photo, categorieingredient, calorie;
    private double pu, quantiteparpack, seuil;
    private int duree, compose;
    private String niveau;

    public Ingredients(String id, String libelle, String categorieingredient, double quantiteparpack) {
        this.setNomTable("as_ingredients");
        this.setId(id);
        this.setLibelle(libelle);
        this.setCategorieingredient(categorieingredient);
        this.setQuantiteparpack(quantiteparpack);
    }

    public Ingredients(String id, String unite, double quantiteparpack) {
        this.setNomTable("as_ingredients");
        this.setId(id);
        this.setUnite(unite);
        this.setQuantiteparpack(quantiteparpack);
    }

    public Ingredients(String libelle, String unite, String photo, String categorieingredient, String calorie, double pu, double quantiteparpack, double seuil, int duree, int compose) {
        this.setNomTable("as_ingredients");
        this.setUnite(unite);
        this.setQuantiteparpack(quantiteparpack);
        this.setLibelle(libelle);
        this.setCategorieingredient(categorieingredient);
        this.setPhoto(photo);
        this.setCalorie(calorie);
        this.setPu(pu);
        this.setSeuil(seuil);
        this.setDuree(duree);
        this.setCompose(compose);
    }

    public Ingredients(String id) {
        this.setNomTable("as_ingredients");
        this.setId(id);
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCalorie() {
        return calorie;
    }

    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public Ingredients() {
        this.setNomTable("AS_INGREDIENTS");
    }

    public Ingredients(String id, double qte) {
        this.setNomTable("AS_INGREDIENTS");
        this.setId(id);
        this.setQuantiteparpack(qte);
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("IG", "getSeqIngredients");
        this.setId(makePK(c));
    }

    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public double getQuantiteparpack() {
        return quantiteparpack;
    }

    public void setQuantiteparpack(double quantiteparpack) {
        this.quantiteparpack = quantiteparpack;
    }

    public double getSeuil() {
        return seuil;
    }

    public void setSeuil(double seuil) {
        this.seuil = seuil;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    public int getCompose() {
        return compose;
    }

    public void setCompose(int compose) {
        this.compose = compose;
    }

    public String getCategorieingredient() {
        return categorieingredient;
    }

    public void setCategorieingredient(String categorieingredient) {
        this.categorieingredient = categorieingredient;
    }
        
    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public Recette[] decomposerBase(Connection c) throws Exception {
        boolean estOuvert = false;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                estOuvert = true;
            }
            Produits p = new Produits();
            p.setId(this.getId());
            return p.decomposerBase(c);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (estOuvert == true && c != null) {
                c.close();
            }
        }
    }

    public Ingredients[] decomposer(Connection c) throws Exception {
        Ingredients[] retour = null;
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                verif = 1;
            }
            if (this.getCompose() == 0) {
                return retour;
            } else {
                Recette recette = new Recette();
                recette.setIdingredients(this.getId());
                Recette[] listef = (Recette[]) CGenUtil.rechercher(recette, null, null, "");
                String[] tab = new String[listef.length];
                for (int i = 0; i < listef.length; i++) {
                    tab[i] = listef[i].getIdproduits();
                }
                retour = (Ingredients[]) CGenUtil.rechercher(new Ingredients(), " select * from ingredients where id in(" + Utilitaire.tabToString(tab, "'", ",") + ")", c);

                return retour;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
    }

    public RecetteLib[] getRecette(String table, Connection c) throws Exception {
        boolean estOuvert = false;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                estOuvert = true;
            }
            RecetteLib crt = new RecetteLib();
            if (table != null && table.compareToIgnoreCase("") != 0) {
                crt.setNomTable(table);
            }
            return (RecetteLib[]) CGenUtil.rechercher(new RecetteLib(), null, null, c, " and idproduits = '" + this.getId() + "'");
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null && estOuvert == true) {
                c.close();
            }
        }
    }

    public RecetteLib[] getRecetteIngredient(String table, Connection c) throws Exception {
        boolean estOuvert = false;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                estOuvert = true;
            }
            RecetteLib crt = new RecetteLib();
            if (table != null && table.compareToIgnoreCase("") != 0) {
                crt.setNomTable(table);
            }
            return (RecetteLib[]) CGenUtil.rechercher(new RecetteLib(), null, null, c, " and IDINGREDIENTS = '" + this.getId() + "'");
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null && estOuvert == true) {
                c.close();
            }
        }
    }

    public Ingredients getIngredient(Connection c) throws Exception {
        boolean estOuvert = false;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                estOuvert = true;
            }
            Ingredients[] lsing = (Ingredients[]) CGenUtil.rechercher(new Ingredients(), null, null, c, " and id = '" + this.getId() + "'");
            if (lsing == null || lsing.length == 0) {
                throw new Exception("ingredient introuvable");
            }
            return lsing[0];
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null && estOuvert == true) {
                c.close();
            }
        }
    }

    public String genererCompte(Connection c) throws Exception {
        boolean estOuvert = false;
        String valiny = "";
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                estOuvert = true;
            }
            Ingredients[] liste = (Ingredients[]) CGenUtil.rechercher(new Ingredients(), "select * from as_ingredients where photo=(select max(photo) from as_ingredients where categorieingredient='" + this.getCategorieingredient() + "')", c);
            if (liste.length > 0) {
                int compte = Utilitaire.stringToInt(liste[0].getPhoto()) + 1;
                valiny = "" + compte;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null && estOuvert) {
                c.close();
            }
        }
        return valiny;
    }
    public InventaireMere initierInventaire(Connection c)throws Exception
    {
        InventaireMere mere=new InventaireMere();
        mere.setDaty(utilitaire.Utilitaire.stringDate("01/01/"+utilitaire.Utilitaire.getAnneeEnCours()));
        mere.setDepot(AlloSakafoService.getNomRestau());
        mere.setIdcategorieingredient(this.getCategorieingredient());
        mere.setObservation("init");
        mere.setType("2");
        mere.construirePK(c);
        mere.setEtat(11);
        InventaireDetails det=new InventaireDetails();
        det.setDaty(mere.getDaty());
        det.setExplication("init");
        det.setIdingredient(this.getTuppleID());
        det.setIdcategorieingredient(this.getCategorieingredient());
        det.setIdmere(mere.getId());
        det.setQtetheorique(0);
        det.setQuantiter(0);
        det.construirePK(c);
        InventaireDetails[]liste=new InventaireDetails[1];
        liste[0]=det;
        mere.setListeDetails(liste);
        return mere;
    }

    @Override
    public int insertToTableWithHisto(String refUser, java.sql.Connection c) throws Exception {
        boolean estOuvert = false;
        int retour = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                estOuvert = true;
                c.setAutoCommit(false);
            }
            this.setPhoto(genererCompte(c));
            String temps = refUser;
            if (refUser != null && refUser.contains("/")) {
                String[] g = Utilitaire.split(refUser, "/");
                refUser = g[0];
            }
            MapHistorique histo = new MapHistorique(this.getNomTable(), "Insertion par " + temps, refUser, this.getTuppleID(), c);
            histo.setObjet(this.getClassName());
            retour = insertToTable(histo, c);
            System.out.println("niditra vonjo "+this.getTuppleID());
            initierInventaire(c).createObject(refUser, c);
            if (c != null && estOuvert) {
                c.commit();
            }
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && estOuvert) {
                c.close();
            }
        }
        return retour;
    }
    private String idunite;

    public String getIdunite() {
        return idunite;
    }

    public void setIdunite(String idunite) {
        this.idunite = idunite;
    }
    
    
    @Override
    public String getValColLibelle(){
        return libelle+";"+idunite;
    }
        
}
