/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.appel;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author Joe
 */
public class Appel extends ClassMAPTable{
    
    private String id, numeroappele, numeroappelant, motif, aboutissement, observation;
    private String heure;
    private Date daty;

    public Appel() {
        this.setNomTable("as_appel");
    }
    
    public void construirePK(Connection c) throws Exception {
        this.preparePk("AAS", "getSeqASAppel");
        this.setId(makePK(c));
    }
    
    public String getNumeroappelant() {
        return numeroappelant;
    }

    public void setNumeroappelant(String numeroappelant) {
        this.numeroappelant = numeroappelant;
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumeroappele() {
        return numeroappele;
    }

    public void setNumeroappele(String numeroappele) {
        this.numeroappele = numeroappele;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getAboutissement() {
        return aboutissement;
    }

    public void setAboutissement(String aboutissement) {
        this.aboutissement = aboutissement;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }
    
    
}
