/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.livraison;

import bean.CGenUtil;
import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import mg.allosakafo.commande.CommandeClient;
import mg.allosakafo.commande.CommandeClientDetails;
import mg.allosakafo.commande.CommandeClientDetailsFM;
import service.AlloSakafoService;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Joe
 */
public class LivraisonApresCommande extends ClassEtat{
    
   private String id ,heure,idcommnademere,idpoint,adresse,idlivreur;
   private Date daty;

    public LivraisonApresCommande() {
        super.setNomTable("LIVRAISON");
    }

    public LivraisonApresCommande(String id, String heure, String idcommnademere, String idpoint, String adresse, String idlivreur, Date daty) {
        super.setNomTable("LIVRAISON");
        this.setId(idlivreur);
        this.setHeure(heure);
        this.setIdcommnademere(idcommnademere);
        this.setIdpoint(idpoint);
        this.setAdresse(adresse);
        this.setIdlivreur(idlivreur);
        this.setDaty(daty);
    }

    @Override
    public void controler(Connection c) throws Exception {
        verifPoint(c);
    }

    @Override
    public void controlerUpdate(Connection c) throws Exception {
        verifPoint(c);
    }
    
    public void verifPoint(Connection c) throws Exception{
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            
            String aWhere =  " and id like '"+this.getId()+"'";
            LivraisonApresCommande[] lac = (LivraisonApresCommande[]) CGenUtil.rechercher(new LivraisonApresCommande(), null, null, c, aWhere);
            if(lac.length > 0){
                lac[0].verifPoint();
            }

            if(c!=null && verif==1) c.commit();
        } catch (Exception e) {
            if (c != null && verif == 1) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
    }
   
    public void verifPoint() throws Exception{
        if(this.getIdpoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0){
            throw new Exception("Action non permise");
        }
    }

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getIdcommnademere() {
        return idcommnademere;
    }

    public void setIdcommnademere(String idcommnademere) {
        this.idcommnademere = idcommnademere;
    }

    public String getIdpoint() {
        return idpoint;
    }

    public void setIdpoint(String idpoint) {
        this.idpoint = idpoint;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        //if (adresse!=null && adresse.compareToIgnoreCase("")!=0&&this.getMode().compareToIgnoreCase("modif")==0)
            //this.setIdpoint(null);
        this.adresse = adresse;
    }

    public String getIdlivreur() {
        return idlivreur;
    }

    public void setIdlivreur(String idlivreur) {
        this.idlivreur = idlivreur;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }
   
   
   

     public void construirePK(Connection c) throws Exception {
        this.preparePk("LIV", "getSeqlivraison");
        this.setId(makePK(c));
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }
    /*public void controler(Connection c) throws  Exception{
         int verif=0;
        try{
            if(c==null){
                c=new UtilDB().GetConn();
                verif=1;
            }
            LivraisonApresCommande nomTab=new LivraisonApresCommande();
            nomTab.setNomTable("VUE_CMD_LIV_DTLS_ALIVRER_PT");
            LivraisonApresCommande[] liste = (LivraisonApresCommande[]) CGenUtil.rechercher(nomTab, null, null, c, " and id in("+this.getIdcommnademere()+")");
            if(liste.length!=0)
            {
                for(int i=1;i<liste.length;i++)
                {
                    
                    if(liste[0].getAdresse()==null)
                    {
                        if(liste[i].getIdpoint()==null || liste[0].getIdpoint().compareTo(liste[i].getIdpoint())!=0)
                        {
                            throw new Exception("Adresse ou Point de livraison Invalide");
                        }
                    }
                    else
                    {
                        if(liste[i].getAdresse()==null || liste[0].getAdresse().compareTo(liste[i].getAdresse())!=0)
                        {
                             throw new Exception("Adresse ou Point de livraison Invalide");
                        }
                    }
                }
            }
            if(verif==1)c.commit();
        }
        catch(Exception ex){
            ex.printStackTrace();
            c.rollback();
            throw ex;
        }
        finally{
            if(c!=null && verif==1){
                c.close();
            }
        }
    }*/
    
   @Override
    public int annuler(Connection c)throws Exception
    {
        System.out.println("JE SUIS DANS ANNULER DE LIVRAISON APRES COMMANDE");
        CommandeClientDetailsFM [] listeDetails=(CommandeClientDetailsFM[]) CGenUtil.rechercher(new CommandeClientDetailsFM(), null, null, c, " and id = '"+this.getIdcommnademere()+"'");
        for(int i=0;i<listeDetails.length;i++)
        {
            CommandeClientDetails ccd = listeDetails[i].getCommandeClientDetails();
            CommandeClient cc = listeDetails[i].getCommandeClient();
            
            //verification du point du restaurant
            cc.verifPoint();
            
            ccd.setEtat(12);
            ccd.updateToTable(c);
        }
        super.annuler(c);
        
        return 0;
    }
	
    

}
