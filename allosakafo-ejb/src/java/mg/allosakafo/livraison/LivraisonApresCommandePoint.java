/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.livraison;

/**
 *
 * @author Fandresena
 */
public class LivraisonApresCommandePoint extends LivraisonApresCommandeComplet {
    private String point;
    private String pointlib;

    public LivraisonApresCommandePoint(){
        this.setNomTable("LIVRAISONAPRESCOMMANDEPOINT");
    }
    
    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getPointlib() {
        return pointlib;
    }

    public void setPointlib(String pointlib) {
        this.pointlib = pointlib;
    }
}
