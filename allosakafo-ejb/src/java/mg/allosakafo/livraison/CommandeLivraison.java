/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.livraison;

import bean.CGenUtil;
import bean.ClassEtat;
import java.sql.Connection;
import java.sql.Date;
import mg.allosakafo.commande.CommandeClient;
import mg.allosakafo.commande.CommandeClientDetails;
import mg.allosakafo.commande.CommandeClientDetailsFM;
import mg.allosakafo.facture.Client;
import mg.allosakafo.fin.EtatOP;
import mg.allosakafo.fin.Report;
import mg.allosakafo.produits.Produits;
import service.AlloSakafoService;
import utilitaire.Constante;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Antsa
 */
public class CommandeLivraison extends ClassEtat {

    private String id, idclient, adresse, idpoint, heure;
    private Date daty;
    private double frais;

    public CommandeLivraison() {
        this.setNomTable("commandeLivraison");
    }
    
    public boolean isSynchro()
    {
        return true;
    }

    @Override
    public void controlerUpdate(Connection c) throws Exception {
        this.verifPoint(c);
    }

    @Override
    public void controlerCloture(Connection c) throws Exception {
        this.verifPoint(c);
    }
    
    public CommandeLivraison(String id) {
        this.setNomTable("commandeLivraison");
        this.setId(id);
    }
    public CommandeLivraison(String id, String idclient, String adresse, String idpoint, String heure, Date daty, double frais) throws Exception {
        this.setNomTable("commandeLivraison");
        this.setId(id);
        this.setIdclient(idclient);
        this.setAdresse(adresse);
        this.setIdpoint(idpoint);
        this.setHeure(heure);
        this.setDaty(daty);
        this.setFrais(frais);
    }

      public CommandeLivraison(String idclient, String adresse, String idpoint, String heure, String daty, String frais) throws Exception {
        this.setNomTable("commandeLivraison");
        this.setIdclient(idclient);
        this.setAdresse(adresse);
        this.setIdpoint(idpoint);
        this.setHeure(heure);
        this.setDaty(Utilitaire.stringDate(daty));
        this.setFrais(Utilitaire.stringToDouble(frais));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdclient() {
        return idclient;
    }

    public void setIdclient(String idclient) {
        this.idclient = idclient;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getIdpoint() {
        if(idpoint==null)return "";
        return idpoint;
    }

    public void setIdpoint(String idpoint) {
        this.idpoint = idpoint;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) throws Exception {
        if(this.getMode().compareToIgnoreCase("modif")==0)
        {
            if(heure==null||heure.compareToIgnoreCase("")==0)
            {
                throw new Exception("heure non valide");
            }
        }
        this.heure = heure;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) throws Exception{
        if(this.getMode().compareToIgnoreCase("modif")==0)
        {
            if(daty==null)
            {
                throw new Exception("date non valide");
            }
            int diffJour=Utilitaire.diffJourDaty(daty, utilitaire.Utilitaire.dateDuJourSql());
            
            if(diffJour<0)
            {
                throw new Exception("date anterieur");
            }
        }
        this.daty = daty;
    }

    public double getFrais() {
        return frais;
    }

    public void setFrais(double frais) throws Exception {
        if (getMode().compareTo("modif") == 0) {
            if (frais < 0) {
                throw new Exception("Les frais doivent �tre positifs");
            } else {
                this.frais = frais;
            }
            return;
        }
        this.frais = frais;
    }

    public void construirePK(Connection c) throws Exception {
        this.preparePk("CMDL", "getSeqcommandeLivraison");
        this.setId(makePK(c));
    }

    @Override
    public String getTuppleID() {
        return id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    public void gererClient(Connection c) throws Exception {
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            Client[] liste = (Client[]) CGenUtil.rechercher(new Client(), null, null, c, " and id ='" + getIdclient() + "'");
            if (liste.length > 0) {
                return;
            }

            String[] client = this.getIdclient().split("-");
            if (client.length == 0 || getIdclient().compareToIgnoreCase("") == 0) {
                throw new Exception("T�l�phone et nom obligatoires");
            }
            
            if (Utilitaire.estTelephoneValide(client[0]) == false) {
                throw new Exception("Telephone non valide");
            }
            
            Client cl = new Client();
            liste = (Client[]) CGenUtil.rechercher(cl, null, null, c, " and telephone like '%" + client[0] + "%'");
            if (liste.length == 0 || client[0].compareToIgnoreCase("") == 0) {
                if (client.length > 1) {
                    cl.setNom(client[1]);
                }
                cl.setTelephone(client[0]);
                cl.construirePK(c);
                cl.insertToTable(c);
            } else {
                cl = liste[0];
            }
            this.setIdclient(cl.getId());
            if (c != null && verif == 1) {
                c.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null && verif == 1) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }

    }
    

    public String inserer(Connection c) throws Exception {
        String idclient=null;
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            this.gererClient(c);
            idclient=this.getIdclient();
            this.setIdclient(null);
            this.construirePK(c);
            this.insertToTable(c);

            if (c != null && verif == 1) {
                c.commit();
            }
            return idclient;
        } catch (Exception e) {
            e.printStackTrace();
            //if( c != null ){c.rollback();}
            if (c != null && verif == 1) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
    }
    
    public  void verifPoint(Connection c) throws Exception{
        int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            
            String aWhere = " and id like '"+this.getId()+"'";
            CommandeLivraison[] livraisons = (CommandeLivraison[]) CGenUtil.rechercher(new CommandeLivraison(), null, null, c, aWhere);
            if(livraisons.length > 0){
                System.out.println("Livraison[0].getIdpoint() = "+livraisons[0].getIdpoint());
                String pointActu = AlloSakafoService.getNomRestau();
                if(livraisons[0].getIdpoint().compareToIgnoreCase(pointActu) != 0){
                    throw new Exception("ACTION  NON PERMISE");
                }
            }
            
            if (indice == 1) {
                c.commit();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        } finally {
            if (indice == 1) {
                c.close();
            }
        }
    }
    
    public  void verifPoint(String idPoint)throws Exception{
        if(idPoint.compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0){
            throw new Exception("ACTION NON PERMISE");
        }
    }
    
    public void updateEtatDetailsLivraison(String[] id, Connection c) throws Exception {
        int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }
            if (id == null) {
                throw new Exception("Aucune commande selectione");
            }
                       
            String tid = Utilitaire.tabToString(id, "'", ",");
            CommandeClientDetailsFM[] ccfm = (CommandeClientDetailsFM[]) CGenUtil.rechercher(new CommandeClientDetailsFM(), null, null, c, " and ID in (" + tid + ")");
           
            if (ccfm.length == 0) {
                throw new Exception("Commande detail introuvable");
            }
            
            for (int i = 0; i < id.length; i++) {
                CommandeClient cc = ccfm[i].getCommandeClient();
                CommandeClientDetails cmds = ccfm[i].getCommandeClientDetails();
                 
                //Verification du point de vente
                this.verifPoint(cc.getPoint());
                
                cmds.setEtat(this.getEtat());
                cmds.updateToTableWithHisto(cmds, c);
            }
            if (indice == 1) {
                c.commit();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw new Exception(ex.getMessage());
        } finally {
            if (indice == 1) {
                c.close();
            }
        }
    }

    public void controler() throws Exception {
        if (adresse.compareTo("")==0 && idpoint.compareTo("")==0) {
            throw new Exception("Saisir soit une adresse soit un point de livraison");
        }
        if (heure.compareTo("")==0) {
            throw new Exception(" Saisir une heure");
        }
    }
    
    public void updatecommandelivraison(String idcmd,Connection c)throws Exception{
        int verif=0;
        try{
            if(c==null){
                c= new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif=1;
            }
            if(this.getId()!=null && this.getId().compareToIgnoreCase("")!=0){
                CommandeLivraison[] cmdl = (CommandeLivraison[])CGenUtil.rechercher(this, null, null, c, "");
                if(cmdl!=null && cmdl.length>0){
                    cmdl[0].setIdclient(idcmd);
                    cmdl[0].updateToTable(c);
                }
                if(verif==1)c.commit();
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            if( c != null ){c.rollback();}
            throw ex;
        }
        finally{
            if(verif==1 && c!=null)c.close();
        }
    }
    public Produits getLivraison(Connection c) throws Exception
    {
        Produits crt=new Produits();
        crt.setNomTable("as_produits_avecPrix");
        crt.setId("LIVR%");
        Produits[]lp=(Produits[])CGenUtil.rechercher(crt, null, null,c, " and pa="+this.getFrais());
        if(lp.length==0||lp.length>1)throw new Exception("prix de livraison invalide");
        return lp[0];      
    }
            
}
