/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.livraison;

/**
 *
 * @author tahina
 */
public class LivraisonApresCommandeComplet extends LivraisonApresCommande {

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }
    String client;
    double montant;
    String idMere;

    public String getIdMere() {
        return idMere;
    }

    public void setIdMere(String idMere) {
        this.idMere = idMere;
    }
    public LivraisonApresCommandeComplet()
    {
        this.setNomTable("LIVRAISONLIBCOMPLETMEREGroupe");
    }
    
}
