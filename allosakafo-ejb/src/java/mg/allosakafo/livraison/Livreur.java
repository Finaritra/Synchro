/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.livraison;

import bean.ClassEtat;
import java.sql.Connection;

/**
 *
 * @author MEVA
 */
public class Livreur extends ClassEtat{

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    private String id,nom,contact;

    public Livreur() {
        super.setNomTable("livreur");
    }
    
    
    @Override
    public String getTuppleID() {
        return this.id;
    }

    @Override
    public String getAttributIDName() {
        return "id";
    }

    @Override
    public void construirePK(Connection c) throws Exception {
        this.preparePk("LIV", "getSeqlivreur");
        this.setId(makePK(c));
    }
    
    
    
}
