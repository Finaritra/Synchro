/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.livraison;

import mg.allosakafo.commande.CommandeClientDetails;

/**
 *
 * @author Maharo R.
 */
public class CommandeLivraisonLibAdresse extends CommandeClientDetails {
     private String idLivraison,adresse,idpoint;

    public String getIdLivraison() {
        return idLivraison;
    }

    public void setIdLivraison(String idLivraison) {
        this.idLivraison = idLivraison;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getIdpoint() {
        return idpoint;
    }

    public void setIdpoint(String idpoint) {
        this.idpoint = idpoint;
    }
    
    
    public CommandeLivraisonLibAdresse() {
        super.setNomTable("commandelivraisonlible");
    }
    
}
