/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.livraison;

import mg.allosakafo.commande.CommandeClientDetails;

/**
 *
 * @author MEVA
 */
public class CommandeLivraisonLib extends CommandeClientDetails{
    private String idLivraison;

    public String getIdLivraison() {
        return idLivraison;
    }

    public void setIdLivraison(String idLivraison) {
        this.idLivraison = idLivraison;
    }

    public CommandeLivraisonLib() {
        super.setNomTable("commandelivraisonlibl");
    }
    
}
