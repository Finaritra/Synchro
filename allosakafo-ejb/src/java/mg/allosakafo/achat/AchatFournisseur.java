/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.achat;

/**
 *
 * @author ionyr
 */
public class AchatFournisseur extends Achat{
    private String ingredient, unite, fournisseur;
    

    public String getFournisseur() {
        return fournisseur;
    }

    public String getIngredient() {
        return ingredient;
    }

    public String getUnite() {
        return unite;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }
}
