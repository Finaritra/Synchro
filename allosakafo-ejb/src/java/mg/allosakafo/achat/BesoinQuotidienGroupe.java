/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.achat;

/**
 *
 * @author Notiavina
 */
public class BesoinQuotidienGroupe extends BesoinQuotidien{
    private String ingredient, unite;
    
    public BesoinQuotidienGroupe(){
        super.setNomTable("VUE_BESOIN_QUOTIDIEN_GROUPE");
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }
}
