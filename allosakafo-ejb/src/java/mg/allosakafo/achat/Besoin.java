/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.achat;

import bean.CGenUtil;
import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import mg.allosakafo.produits.RecetteLib;
import mg.allosakafo.stock.InventaireDetails;
import user.UserEJB;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Notiavina
 */
public class Besoin extends ClassMAPTable{
    private String id, idproduit, remarque;
    private double quantite;
    String libelleProduit;
    private int jourssemaine ;
    
    
    public int getJourssemaine() {
        return jourssemaine;
    }

    public void setJourssemaine(int jourssemaine) {
        this.jourssemaine = jourssemaine;
    }
    

    public String getLibelleProduit() {
        return libelleProduit;
    }

    public void setLibelleProduit(String libelleProduit) {
        this.libelleProduit = libelleProduit;
    }
    
    public Besoin(){
        super.setNomTable("besoin");
    }
    
    public boolean isSynchro()
    {
        return true;
    }

    public Besoin(String id,String idproduit,Double quantite,String remarque ){
        this.setNomTable("besoin");
        setId(id);
        setIdproduit(idproduit);
        setQuantite(quantite);
        setRemarque(remarque);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(String idproduit) {
        this.idproduit = idproduit;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }
    
    public void construirePK(Connection c) throws Exception {
        this.preparePk("BSN", "getseqbesoin");
        
        this.setId(makePK(c));
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    @Override
    public void controler(Connection c) throws Exception {
        super.controler(c); //To change body of generated methods, choose Tools | Templates.
        Besoin[] liste =  this.getBesoinSimple(c);
        if(liste!=null && liste.length > 0)throw new Exception("Besoin d�ja existant");
    }
    

    public Besoin(String idproduit, String remarque, double quantite) {
        this.setNomTable("besoin");
        this.setIdproduit(idproduit);
        this.setRemarque(remarque);
        this.setQuantite(quantite);
    }
    public Besoin[] getBesoinMultiple(String[] id,Connection c)throws Exception{
        int verif = 0;
        try {
            if(c==null){
            c = new UtilDB().GetConn();
            verif =1;
            }
            String tab = Utilitaire.tabToString(id, "'", ",");
           return (Besoin[])CGenUtil.rechercher(this, null, null, c, " and idproduit in ("+tab+")");
        }
        catch (Exception e) 
        {
                throw e;
        } 
        finally 
        {
                if (c != null && verif==1) {
                    c.close();
            }
        }
        
    }
   public  void insertBesoinMultiple(String[] valeur,String[] id,String user,Connection c)throws Exception{
        int verif = 0;
        try {
            if(c==null){
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            verif =1;
            }
            
            if(valeur==null)throw new Exception("valeur non trouv�e");
           Besoin[] liste =  this.getBesoinMultiple(id, c);
            for(int j=0;j<valeur.length;j++){
                int insert=0;
                 String[] detailsvaleur = valeur[j].split("-");
                 String qte = detailsvaleur[1];
                 String idvaleur =detailsvaleur[0];
                if(liste!=null){
                    for(int i=0;i<liste.length;i++){
                            if(idvaleur.compareToIgnoreCase(liste[i].getIdproduit())==0){
                                Besoin details= new Besoin(liste[i].getId(),liste[i].getIdproduit(),Utilitaire.stringToDouble(qte),liste[i].getRemarque());
                                details.updateToTableWithHisto(user, c);
                                insert=1;
                                break;
                            }
                        }
                }
                     if(insert==0){
                        Besoin details= new Besoin(idvaleur,null,Utilitaire.stringToDouble(qte));
                        details.construirePK(c);
                        details.insertToTableWithHisto(user, c);
                     }
                }
           
              if (verif==1)c.commit();       
            }
        catch (Exception e) 
        {
               if(c!=null) c.rollback();
                throw e;
        } 
        finally 
        {
                if (c != null && verif==1) {
                    c.close();
            }
        }
   }
   
   public Besoin[] getBesoinSimple( Connection c)throws Exception{
        int verif = 0;
        try {
            if(c==null){
            c = new UtilDB().GetConn();
            verif =1;
            }
           return (Besoin[])CGenUtil.rechercher(new Besoin(), null, null, c, " and idproduit = ('"+this.getIdproduit()+"')");
        }
        catch (Exception e) 
        {
                throw e;
        } 
        finally 
        {
                if (c != null && verif==1) {
                    c.close();
            }
        }
        
    }
   public Besoin[] getBesoinMultiple( String nomtable,String idproduit,Connection c)throws Exception{
        int verif = 0;
        try {
            if(c==null){
            c = new UtilDB().GetConn();
            verif =1;
            }
            Besoin b= new Besoin();
            if(nomtable!=null)b.setNomTable(nomtable);
           return (Besoin[])CGenUtil.rechercher(b, null, null, c, " and idproduit in ("+idproduit+")");
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            throw e;
        } 
        finally 
        {
                if (c != null && verif==1) {
                    c.close();
            }
        }
        
    }
   public void passerBesoin(UserEJB u,ClassMAPTable[] objet,Connection c)throws Exception{
         int verif = 0;
        try {
                if(c==null){
                    c = new UtilDB().GetConn();
                    c.setAutoCommit(false);
                    verif =1;
                } 
                if(objet==null || objet.length==0)throw new Exception("veuillez cochez au moins un");
                String [] ids=new String[objet.length];
                for(int i=0;i<objet.length;i++){
                    ids[i]= ((Besoin)objet[i]).getIdproduit();
                }
                String idproduit=Utilitaire.tabToString(ids, "'", ",");
                Besoin[] lsbesoin=this.getBesoinMultiple("besoin",idproduit,c);
                this.deleteMultiple(u,lsbesoin, c);
                this.insertMultiple(u,objet,c);
                if(verif==1)c.commit();
       }
       catch (Exception e) 
        {
            e.printStackTrace();
            if( c != null ){c.rollback();}
            throw e;
        } 
        finally 
        {
            if (c != null && verif==1) {
                c.close();
            }
        }
        
   }
   public  void insertMultiple(UserEJB u,ClassMAPTable[] lsbesoin,Connection c) throws Exception{
	Statement st = null;
	try {
                for(int i=0;i<lsbesoin.length;i++){
                    Besoin b=(Besoin)lsbesoin[i];
                   u.createObject(b, c);
                }
	} catch (SQLException ex) {
	    ex.printStackTrace();
            if( c != null ){c.rollback();}
	    throw new Exception();
	} finally {
	    if (st != null) {
		st.close();
	    }
	}
    }
   
     public  void deleteMultiple(UserEJB u,Besoin[] lsbesoin,Connection c) throws Exception{
	try {
                for(int i=0;i<lsbesoin.length;i++){
                    System.out.println(lsbesoin[i].getIdproduit()+" id : "+lsbesoin[i].getId());
                   u.deleteObject(lsbesoin[i], c);
                }
	} catch (Exception ex) {
	    ex.printStackTrace();
            if( c != null ){c.rollback();}
	    throw ex;
	}
    }
     public static RecetteLib[] simulerBesoin(HttpServletRequest request,String[] indice,Connection c)throws Exception{
         try{
         if(indice==null || indice.length==0)throw new Exception("veuillez selectionner au moins une");
            String sql="";
           String idproduits="'"+request.getParameter("idproduit_"+indice[0])+"'";
           for(int i=0;i<indice.length;i++){
               idproduits+=",'"+request.getParameter("idproduit_"+indice[i])+"'";
           }
           sql="select * from as_recettebesoincompose_lib where mere in("+idproduits+")";
            RecetteLib rec=new RecetteLib();
            sql=rec.makeWhereBesoin(sql,request);
            
            rec.setNomTable("as_recettebesoincompose_lib");
           RecetteLib[] rc = (RecetteLib[])CGenUtil.rechercher(rec, sql, c);
             
             HashMap<String, RecetteLib> lsgrouprc = new HashMap<String, RecetteLib>();
           for(int i=0;i<rc.length;i++){
               
              
               for(int j=0;j<indice.length;j++){
                   if(rc[i].getMere().compareTo(request.getParameter("idproduit_"+indice[j]))==0)
                   {
                       double qtebesoin=Utilitaire.stringToDouble(request.getParameter("quantite_"+indice[j]));
                       
                           rc[i].setQuantite(rc[i].getQteav()*rc[i].getQuantite()*qtebesoin);
                    }
               }
               if(lsgrouprc.containsKey(rc[i].getIdingredients())){
                   RecetteLib temp= lsgrouprc.get(rc[i].getIdingredients());
                   temp.setQuantite(temp.getQuantite()+rc[i].getQuantite());
                   lsgrouprc.remove(rc[i].getIdingredients());
                   lsgrouprc.put(rc[i].getIdingredients(), temp);
               }
               else{
                   lsgrouprc.put(rc[i].getIdingredients(), rc[i]);
               }
                              
           }
             return Arrays.copyOf(lsgrouprc.values().toArray(),lsgrouprc.size(),RecetteLib[].class);
         }
         catch(Exception ex){
            ex.printStackTrace();
	    throw ex;
         }
     }
    
    /*public static RecetteLib[] simulerBesoin(HttpServletRequest request,String[] indice,Connection c)throws Exception{
         try{
         if(indice==null || indice.length==0)throw new Exception("veuillez selectionner au moins une");
            String sql="";
           String idproduits="'"+request.getParameter("idproduit_"+indice[0])+"'";
           for(int i=0;i<indice.length;i++){
               idproduits+=",'"+request.getParameter("idproduit_"+indice[i])+"'";
           }
           sql="select * from as_recettebesoincompose_lib where idproduits in("+idproduits+")";
            RecetteLib rec=new RecetteLib();
            sql=rec.makeWhereBesoin(sql,request);
            
             System.out.println("sql = " + sql);
            rec.setNomTable("as_recettebesoincompose_lib");
           RecetteLib[] rc = (RecetteLib[])CGenUtil.rechercher(rec, sql, c);
             
             HashMap<String, RecetteLib> lsgrouprc = new HashMap<String, RecetteLib>();
           for(int i=0;i<rc.length;i++){
               
              
               for(int j=0;j<indice.length;j++){
                   if(rc[i].getIdproduits().compareTo(request.getParameter("idproduit_"+indice[j]))==0){
                       double qtebesoin=Utilitaire.stringToDouble(request.getParameter("quantite_"+indice[j]));
                           rc[i].setQuantite(rc[i].getQteav()*rc[i].getQuantite()*qtebesoin);
                    }
               }
               if(lsgrouprc.containsKey(rc[i].getIdingredients())){
                   RecetteLib temp= lsgrouprc.get(rc[i].getIdingredients());
                   temp.setQuantite(temp.getQuantite()+rc[i].getQuantite());
                   lsgrouprc.remove(rc[i].getIdingredients());
                   lsgrouprc.put(rc[i].getIdingredients(), temp);
               }
               else{
                   lsgrouprc.put(rc[i].getIdingredients(), rc[i]);
               }
                              
           }
             return Arrays.copyOf(lsgrouprc.values().toArray(),lsgrouprc.size(),RecetteLib[].class);
         }
         catch(Exception ex){
            ex.printStackTrace();
	    throw ex;
         }
     }*/
   
   
}
