/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.achat;

import bean.ClassMAPTable;
import java.sql.Connection;

/**
 *
 * @author Notiavina
 */
public class Fournisseur extends ClassMAPTable{
    private String idfournisseur, nomfournisseur, niffournisseur, statfournisseur, telfournisseur, emailfournisseur, adressefournisseur;
    
    public Fournisseur(){
        super.setNomTable("FFournisseur");
    }

    public String getIdfournisseur() {
        return idfournisseur;
    }

    public String getNomfournisseur() {
        return nomfournisseur;
    }

    public String getNiffournisseur() {
        return niffournisseur;
    }

    public String getStatfournisseur() {
        return statfournisseur;
    }

    public String getTelfournisseur() {
        return telfournisseur;
    }

    public String getEmailfournisseur() {
        return emailfournisseur;
    }

    public String getAdressefournisseur() {
        return adressefournisseur;
    }

    public void setIdfournisseur(String idfournisseur) {
        this.idfournisseur = idfournisseur;
    }

    public void setNomfournisseur(String nomfournisseur) {
        this.nomfournisseur = nomfournisseur;
    }

    public void setNiffournisseur(String niffournisseur) {
        this.niffournisseur = niffournisseur;
    }

    public void setStatfournisseur(String statfournisseur) {
        this.statfournisseur = statfournisseur;
    }

    public void setTelfournisseur(String telfournisseur) {
        this.telfournisseur = telfournisseur;
    }

    public void setEmailfournisseur(String emailfournisseur) {
        this.emailfournisseur = emailfournisseur;
    }

    public void setAdressefournisseur(String adressefournisseur) {
        this.adressefournisseur = adressefournisseur;
    }
    
    public void construirePK(Connection c) throws Exception {
        this.preparePk("FRNS", "getseq_fournisseur");
        this.setIdfournisseur(makePK(c));
    }
    
    public String getTuppleID() {
        return idfournisseur;
    }

    public String getAttributIDName() {
        return "idfournisseur";
    }
}
