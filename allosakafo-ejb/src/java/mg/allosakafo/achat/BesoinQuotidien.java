/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.achat;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;
import mg.allosakafo.produits.Ingredients;

/**
 *
 * @author Notiavina
 */
public class BesoinQuotidien extends ClassMAPTable{
    public String id, idunite, idingredient, iduniterestant, iduser;
    public Date daty;
    public double quantite, quantiteRestant;
    
    public BesoinQuotidien(){
        super.setNomTable("besoinquotidien");
    }
    
    public BesoinQuotidien(Date daty, String idunite, double qte, String idingr, double qteRest, String iduser){
        super.setNomTable("besoinquotidien");
        this.setDaty(daty);
        this.setIdunite(idunite);
        this.setQuantite(qte);
        this.setIdingredient(idingr);
        this.setQuantiteRestant(qteRest);
        this.setIduniterestant(idunite);
        this.setIduser(iduser);
    }
    
    public void construirePK(Connection c) throws Exception {
        this.preparePk("BSNQTD", "getseqbesoinQuotidien");
        this.setId(makePK(c));
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }

    public String getIduser() {
        return iduser;
    }

    public String getIdunite() {
        return idunite;
    }

    public String getId() {
        return id;
    }

    public String getIdingredient() {
        return idingredient;
    }

    public String getIduniterestant() {
        return iduniterestant;
    }

    public Date getDaty() {
        return daty;
    }

    public double getQuantite() {
        return quantite;
    }

    public double getQuantiteRestant() {
        return quantiteRestant;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public void setIdunite(String idunite) {
        this.idunite = idunite;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    public void setQuantiteRestant(double quantiteRestant) {
        this.quantiteRestant = quantiteRestant;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIdingredient(String idingredient) {
        this.idingredient = idingredient;
    }

    public void setIduniterestant(String iduniterestant) {
        this.iduniterestant = iduniterestant;
    }
}
