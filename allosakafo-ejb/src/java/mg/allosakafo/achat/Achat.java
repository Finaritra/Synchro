/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.allosakafo.achat;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

/**
 *
 * @author Notiavina
 */
public class Achat extends ClassMAPTable{
    private String id, idingredient,idfournisseur, idunite, iduser;
    private double quantite, prixunitaire, montant;
    private Date daty;
    
    public Achat(){
        super.setNomTable("achat");
    }
    
    public Achat(String idingredient, double qte, String idunite, double pu, double montant, Date daty, String iduser, String idfournisseur){
        super.setNomTable("achat");
        this.setIdingredient(idingredient);
        this.setQuantite(qte);
        this.setIdunite(idunite);
        this.setPrixunitaire(pu);
        this.setMontant(montant);
        this.setDaty(daty);
        this.setIduser(iduser);
        this.setIdfournisseur(idfournisseur);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdingredient() {
        return idingredient;
    }

    public void setIdingredient(String idingredient) {
        this.idingredient = idingredient;
    }
    
    public String getIdfournisseur() {
        return idfournisseur;
    }

    public void setIdfournisseur(String idfournisseur) {
        this.idfournisseur = idfournisseur;
    }

    public String getIdunite() {
        return idunite;
    }

    public void setIdunite(String idunite) {
        this.idunite = idunite;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    public double getPrixunitaire() {
        return prixunitaire;
    }

    public void setPrixunitaire(double prixunitaire) {
        this.prixunitaire = prixunitaire;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }
    
    public void construirePK(Connection c) throws Exception {
        this.preparePk("ACHAT", "getseq_achat");
        this.setId(makePK(c));
    }
    
    public String getTuppleID() {
        return id;
    }

    public String getAttributIDName() {
        return "id";
    }
}
