package historique;

import java.sql.Connection;
import java.util.*;

public class MapUtilisateur extends bean.ClassMAPTable implements java.io.Serializable {

    private String loginuser;
    private String pwduser;
    private String idrole;
    private String nomuser;
    private String adruser;
    private String teluser;
    private int refuser;
    private int rang;

    public int getRang() {
        return rang;
    }

    public void setRang(int rang) {
        this.rang = rang;
    }

    public MapUtilisateur() {
        //setIndicePk("US");
        setNomTable("utilisateur");
        //idrole="admin";
    }

    public MapUtilisateur(int refus, String loginus, String pwdus, String nomus, String adrus, String telus, String idrol) {
        this.setRefuser(refus);
        this.setLoginuser(loginus);
        this.setPwduser(pwdus);
        this.setNomuser(nomus);
        this.setAdruser(adrus);
        this.setTeluser(telus);
        this.setIdrole(idrol);
        super.setNomTable("utilisateur");
    }

    public MapUtilisateur(String refus, String loginus, String pwdus, String nomus, String adrus, String telus, String idrol) {
        this.setRefuser(Integer.valueOf(refus));
        this.setLoginuser(loginus);
        this.setPwduser(pwdus);
        this.setNomuser(nomus);
        this.setAdruser(adrus);
        this.setTeluser(telus);
        this.setIdrole(idrol);
        super.setNomTable("utilisateur");
    }

    public MapUtilisateur(String loginus, String pwdus, String nomus, String adrus, String telus, String idrol) {
        super.setNomTable("utilisateur");
        this.setRefuser(utilitaire.Utilitaire.getMaxSeq("getSeqUtilisateur"));
        this.setLoginuser(loginus);
        this.setPwduser(pwdus);
        this.setNomuser(nomus);
        this.setAdruser(adrus);
        this.setTeluser(telus);
        this.setIdrole(idrol);
        super.setNomTable("utilisateur");
    }

    @Override
    public String getAttributIDName() {
        return "refuser";
    }

    @Override
    public String getTuppleID() {
        return String.valueOf(refuser);
    }

    public MapUtilisateur(String loginuser, String pwduser) {
        this.loginuser = loginuser;
        this.pwduser = pwduser;
        setNomTable("utilisateur");
        super.setNombreChamp(7);
    }

    public MapRoles getRole() {
        //RoleUtil rU=new RoleUtil();
        //MapRoles a=rU.rechercheById(idrole);
        return null;
    }

    public String getEtat() {
        AnnulationUtilisateurUtil au = new AnnulationUtilisateurUtil();
        System.out.println(" ato anatin ito ve "+this.getTuppleID());
        if (au.rechercher(2, this.getTuppleID()).length > 0)//si lutilisateur est desactive
        {
            return "desactiver";
        } else {
            return "activer"; //si l'utilisateur est active
        }
    }

    public boolean isSuperUser() {
        if (getIdrole().compareToIgnoreCase("dg") == 0 || getIdrole().compareToIgnoreCase("controle") == 0) {
            return true;
        }
        return false;
    }

    public String getLoginuser() {
        return loginuser;
    }

    public void setLoginuser(String loginuser) {
        this.loginuser = loginuser;
    }

    public String getPwduser() {
        return pwduser;
    }

    public void setPwduser(String pwduser) {
        this.pwduser = pwduser;
    }

    public String getIdrole() {
        return idrole;
    }

    public void setIdrole(String idrole) {
        this.idrole = idrole;
    }

    public String getNomuser() {
        return nomuser;
    }

    public void setNomuser(String nomuser) {
        this.nomuser = nomuser;
    }

    public String getAdruser() {
        return adruser;
    }

    public void setAdruser(String adruser) {
        this.adruser = adruser;
    }

    public String getTeluser() {
        return teluser;
    }

    public void setTeluser(String teluser) {
        this.teluser = teluser;
    }

    public int getRefuser() {
        return refuser;
    }

   

    public void setRefuser(String refuser) {
        if(refuser!=null)this.refuser = Integer.valueOf(refuser);
    }
     public void setRefuser(int refuser) {
        this.refuser = refuser;
    }
     
    @Override
    public void construirePK(Connection c) throws Exception {
        super.setNomTable("utilisateur");
        this.preparePk("", "getSeqUtilisateur");
        this.setRefuser(makePK(c));
    }
}
