package edt;

import bean.ClassMAPTable;
import java.sql.Connection;
/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class Ressource extends ClassMAPTable{

  private String id;
  private String semestre;
  private String salle;
  private String matiere;
  private String heuredebut;
  private String heurefin;
  private String jour;
  private String val;

  public Ressource() {
    super.setNomTable("ressource");
  }
  public String getTuppleID(){
   return id;
 }
 public String getAttributIDName(){
   return "id";
 }
 public void construirePK(Connection c) throws Exception{
  super.setNomTable("ressource");
  this.preparePk("RES","getSeqRessource");
  this.setId(makePK(c));
  }
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public void setSemestre(String semestre) {
    this.semestre = semestre;
  }
  public String getSemestre() {
    return semestre;
  }
  public void setSalle(String salle) {
    this.salle = salle;
  }
  public String getSalle() {
    return salle;
  }
  public void setMatiere(String matiere) {
    this.matiere = matiere;
  }
  public String getMatiere() {
    return matiere;
  }
  public void setHeuredebut(String heuredebut) {
    this.heuredebut = heuredebut;
  }
  public String getHeuredebut() {
    return heuredebut;
  }
  public void setHeurefin(String heurefin) {
    this.heurefin = heurefin;
  }
  public String getHeurefin() {
    return heurefin;
  }
  public void setJour(String jour) {
     this.jour = jour;
   }
   public String getJour() {
     return jour;
  }
  public void setVal(String val) {
   this.val = val;
 }
 public String getVal() {
   return val;
  }
}