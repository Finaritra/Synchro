package edt;

import bean.*;


/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class RessourceCompletEDT extends ClassMAPTable {

  private String id;
  private String semestre;
  private String salle;
  private String codematiere;
  private String matiere;
  private String enseignant;
  private String heureD;
  private String heureF;
  private String jour;
  private String val;

  public RessourceCompletEDT() {
    super.setNomTable("RESSOURCECOMPLETEDT");
  }
  public String getAttributIDName() {
    return "id";
  }
  public String getTuppleID() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public String getId() {
    return id;
  }
  public void setSemestre(String semestre) {
    this.semestre = semestre;
  }
  public String getSemestre() {
    return semestre;
  }
  public void setSalle(String salle) {
    this.salle = salle;
  }
  public String getSalle() {
    return salle;
  }
  public void setCodematiere(String codematiere) {
    this.codematiere = codematiere;
  }
  public String getCodematiere() {
    return codematiere;
  }
  public void setMatiere(String matiere) {
    this.matiere = matiere;
  }
  public String getMatiere() {
    return matiere;
  }
  public void setEnseignant(String enseignant) {
    this.enseignant = enseignant;
  }
  public String getEnseignant() {
    return enseignant;
  }
  public void setHeureD(String heureD) {
    this.heureD = heureD;
  }
  public String getHeureD() {
    return heureD;
  }
  public void setHeureF(String heureF) {
    this.heureF = heureF;
  }
  public String getHeureF() {
    return heureF;
  }
  public void setJour(String jour) {
    this.jour = jour;
  }
  public String getJour() {
    return jour;
  }
  public void setVal(String val) {
   this.val = val;
 }
 public String getVal() {
   return val;
  }
}