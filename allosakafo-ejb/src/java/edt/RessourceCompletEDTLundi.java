package edt;

import bean.*;
import utilitaire.*;

/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class RessourceCompletEDTLundi extends RessourceCompletEDT {

  public RessourceCompletEDTLundi() {
    super.setNomTable("RESSOURCECOMPLETEDTLUNDI");
  }
  public boolean getHeureValidation(String a)
  {
    String[] tempsD=Utilitaire.split(this.getHeureD(),":");
    int tempD=Utilitaire.stringToInt(tempsD[0]);int tempDebutMin=Utilitaire.stringToInt(tempsD[1]);
    String[] tempsF=Utilitaire.split(this.getHeureF(),":");
    int tempF=Utilitaire.stringToInt(tempsF[0]);int tempFinMin=Utilitaire.stringToInt(tempsF[1]);
    String[] tempsCourant=Utilitaire.split(a,":");
    int tempCourant=Utilitaire.stringToInt(tempsCourant[0]);int tempCourantMin=Utilitaire.stringToInt(tempsCourant[1]);
    if(tempCourant>=tempD && tempF>=tempCourant){
       return true;
      }
    else{
      return false;
    }
  }
}