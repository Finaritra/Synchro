package edt;

import bean.ClassMAPTable;
import java.sql.Connection;
/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class ProgrammeDetaille extends ClassMAPTable{

  private String id;
  private String matiere;
  private String progdetaille;

  public ProgrammeDetaille() {
    super.setNomTable("progdetaille");
  }
  public String getTuppleID(){
   return id;
 }
 public String getAttributIDName(){
   return "id";
 }
 public void construirePK(Connection c) throws Exception{
  super.setNomTable("progdetaille");
  this.preparePk("PGD","getSeqProgDetaille");
  this.setId(makePK(c));
  }
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public void setMatiere(String matiere) {
    this.matiere = matiere;
  }
  public String getMatiere() {
    return matiere;
  }
  public void setProgdetaille(String progdetaille) {
    this.progdetaille = progdetaille;
  }
  public String getProgdetaille() {
    return progdetaille;
  }
}