package edt;

import bean.ClassMAPTable;
import java.sql.Connection;
/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class ProgrammeComplet extends ProgrammeDetaille{
  private String intitule;
  private int coeff;
  private String enseignant;
  private String semestre;

  public ProgrammeComplet() {
    super.setNomTable("programmecomplet");
  }
  public void setIntitule(String intitule) {
    this.intitule = intitule;
  }
  public String getIntitule() {
    return intitule;
  }
  public void setCoeff(int coeff) {
    this.coeff = coeff;
  }
  public int getCoeff() {
    return coeff;
  }
  public void setEnseignant(String enseignant) {
     this.enseignant = enseignant;
   }
   public String getEnseignant() {
     return enseignant;
  }
  public void setSemestre(String semestre) {
     this.semestre = semestre;
   }
   public String getSemestre() {
     return semestre;
  }
}