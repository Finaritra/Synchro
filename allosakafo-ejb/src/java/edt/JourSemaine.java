package edt;

import bean.ClassMAPTable;
import java.sql.Connection;
/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class JourSemaine extends ClassMAPTable{

  private String id;
  private String libelle;

  public JourSemaine() {
    super.setNomTable("jourdelasemaine");
  }
  public String getTuppleID(){
   return id;
 }
 public String getAttributIDName(){
   return "id";
 }
 public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  public String getLibelle() {
    return libelle;
  }
}