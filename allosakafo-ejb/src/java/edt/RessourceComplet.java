package edt;

import bean.ClassMAPTable;
import java.sql.Connection;
/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class RessourceComplet extends ClassMAPTable{
  private String id;
  private String semestre;
  private String salle;
  private String enseignant;
  private String matiere;
  private String intitule;
  private String heure;
  private String jour;

  public RessourceComplet() {
    super.setNomTable("ressourcecomplet");
  }
  public String getTuppleID(){
  return id;
 }
 public String getAttributIDName(){
  return "id";
 }
 public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public void setSemestre(String semestre) {
    this.semestre = semestre;
  }
  public String getSemestre() {
    return semestre;
  }
  public void setSalle(String salle) {
    this.salle = salle;
  }
  public String getSalle() {
    return salle;
  }
  public void setEnseignant(String enseignant) {
    this.enseignant = enseignant;
  }
  public String getEnseignant() {
    return enseignant;
  }
  public void setMatiere(String matiere) {
    this.matiere = matiere;
  }
  public String getMatiere() {
    return matiere;
  }
  public void setIntitule(String intitule) {
    this.intitule = intitule;
  }
  public String getIntitule() {
    return intitule;
  }
  public void setHeure(String heure) {
    this.heure = heure;
  }
  public String getHeure() {
    return heure;
  }
  public void setJour(String jour) {
     this.jour = jour;
   }
   public String getJour() {
     return jour;
  }
}