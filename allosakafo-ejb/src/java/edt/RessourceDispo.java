package edt;
import bean.ClassMAPTable;
import java.sql.Connection;

public class RessourceDispo extends ClassMAPTable{

  private String id;
  private String codematiere;
  private String matiere;
  private String enseignant;
  private String semestre;
  private int coeff;
  public RessourceDispo() {
    super.setNomTable("ressourcedispo");
  }
  public String getTuppleID(){
  return id;
}
public String getAttributIDName(){
  return "id";
}
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public void setCodematiere(String codematiere) {
    this.codematiere = codematiere;
  }
  public String getCodematiere() {
    return codematiere;
  }
  public void setEnseignant(String enseignant) {
    this.enseignant = enseignant;
  }
  public String getEnseignant() {
    return enseignant;
  }
  public void setSemestre(String semestre) {
    this.semestre = semestre;
  }
  public String getSemestre() {
    return semestre;
  }
  public int getCoeff() {
    return coeff;
  }
  public void setCoeff(int coeff)
  {
    this.coeff = coeff;
  }
  public void setMatiere(String matiere){
    this.matiere = matiere;
  }
  public String getMatiere() {
    return matiere;
  }
}