/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.sql.Connection;
import java.sql.Statement;
import utilitaire.ConstanteEtat;

/**
 *
 * @author GILEADA
 */
public abstract class ClassEtat extends ClassUser{

    int etat = 1;
    int[]listeEtat={0,1,9,11,20,40};

    public int[] getListeEtat() {
        return listeEtat;
    }

    public void setListeEtat(int[] listeEtat) {
        this.listeEtat = listeEtat;
    }

    public String[] getEtatLettre() {
        return etatLettre;
    }

    public void setEtatLettre(String[] etatLettre) {
        this.etatLettre = etatLettre;
    }
    String[]etatLettre={"annule","cree","cloture","valide","fait","paye"};

    public int getEtat() {
        return etat;
    }
    
    public void setEtat(int etat) {
        this.etat = etat;
    }
    
    public String getEtatText(int value){
        return chaineEtat(value);
    }
    public String getEtatEnString(int valeur)
    {
        int i=0;
        for(i=0;i<getListeEtat().length;i++)
        {
            if(valeur==getListeEtat()[i]) break;
        }
        if(i>=getListeEtat().length)return "";
        return getEtatLettre()[i];
    }
    public int getEtatNombre(String eta)
    {
        int i=0;
        for(i=0;i<getEtatLettre().length;i++)
        {
            if(eta.compareToIgnoreCase(getEtatLettre()[i])==0) break;
        }
        if(i>=getEtatLettre().length)return -1;
        return getListeEtat()[i];
    }
    public String chaineEtat(int value){
        if(value == ConstanteEtat.getEtatCreer()) return "<b style='color:lightskyblue'>CR&Eacute;&Eacute;(E)</b>";
        if(value == ConstanteEtat.getEtatValider()) return "<b style='color:green'>VIS&Eacute;(E)</b>";
        if(value == ConstanteEtat.getEtatAnnuler()) return "<b style='color:orange'>ANNUL&Eacute;(E)</b>";
        if(value == ConstanteEtat.getEtatCloture()) return "<b style='color:orange'>CLOTUR&Eacute;(E)</b>";
        if(value == ConstanteEtat.getEtatLivraison()) return "<b style='color:green'>LIVR&Eacute;(E)</b>";
        if(value == ConstanteEtat.getEtatPaye()) return "<b style='color:green'>PAY&Eacute;</b>";
        if(value == ConstanteEtat.getEtatFait()) return "<b style='color:green'>PRET A LIVRER</b>";
         if(value == ConstanteEtat.getEffectuer()) return "<b style='color:green'>EFFECTUE</b>";
	if(value == ConstanteEtat.getEtatEncoursPayement()) return "<b style='color:yellow'>EN COURS DE PAIEMENT</b>";
        return null;
    }
    public int updateEtat(int valeurEtat, String id, Connection c) throws Exception {
        Statement cmd = null;
        try {
            String req = "update " + getNomTable() + " set etat=" + valeurEtat + " where " + getAttributIDName() + " = '" + id + "'";
            cmd = c.createStatement();
            return cmd.executeUpdate(req);

        } catch (Exception ex) {
            if( c != null ){c.rollback();}
            throw ex;
        } finally {
            cmd.close();
        }
    }
    public int annuler(Connection c)throws Exception
    {
        if(getEtat()>=ConstanteEtat.getEtatValider()) throw new Exception("Impossible � annuler, deja valide");
        setMode("modif");
        return this.updateEtat(ConstanteEtat.getEtatAnnuler(), CGenUtil.getValeurInsert(this, "id"), c);
    }
    public int annuler(String user,Connection c)throws Exception
    {
        setMode("modif");
        this.setEtat(ConstanteEtat.getEtatAnnuler());
        return this.updateToTableWithHisto(user, c);
    }
    public int annulerVisa(String user,Connection c) throws Exception
    {
        if(getEtat()>ConstanteEtat.getEtatValider()) throw new Exception("Impossible � annuler, superieur valide");
        setMode("modif");
        return this.updateEtat(ConstanteEtat.getEtatCreer(), this.getTuppleID(), c);
    }
    public Object validerObject(String u, Connection c) throws Exception
    {
        setMode("modif");
        controler(c);
        this.setEtat(ConstanteEtat.getEtatValider());
        this.updateToTableWithHisto(u, c);
        return this;
    }
    
    public Object cloturerObject(String u, Connection c) throws Exception
    {
        setMode("modif");
        controlerCloture(c);
        this.setEtat(ConstanteEtat.getEtatCloture());
        this.updateToTableWithHisto(u, c);
        return this;
    }
    
    public Object annulerValidation(String u, Connection c) throws Exception
    {
        return null;
    }
}
