/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import bean.CGenUtil;
import bean.ClassMAPTable;
import bean.TypeObjet;
import fichiers.Lecture;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import mg.allosakafo.achat.Achat;
import mg.allosakafo.achat.Besoin;
import mg.allosakafo.achat.BesoinQuotidien;
import mg.allosakafo.achat.Fournisseur;
import mg.allosakafo.commande.CommandeClient;
import mg.allosakafo.commande.CommandeClientDetails;
import mg.allosakafo.commande.CommandeClientDetailsLibelle;
import mg.allosakafo.commande.CommandeClientDetailsSauce;
import mg.allosakafo.facture.Client2;
import mg.allosakafo.facture.ClientPoint;

import mg.allosakafo.fin.MvtCaisse;
import mg.allosakafo.fin.MvtIntraCaisse;
import mg.allosakafo.fin.Report;
import mg.allosakafo.produits.DisponibiliteProduit;

import mg.allosakafo.produits.MenuCategorie;
import mg.allosakafo.produits.Produits;
import mg.allosakafo.produits.ProduitsTypeLibelle;
import mg.allosakafo.produits.TypeproduitSousCategorie;
import user.UserEJB;
import user.UserEJBBean;
import utilitaire.Constante;
import utilitaire.ConstanteEtat;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 *
 * @author Notiavina
 */
public class AlloSakafoService {
    
    public static String getCaisseDefaut() throws Exception{
        try{
            ArrayList<String> listeNom=Lecture.lireFichier(Constante.caisseDefaut);
            if(listeNom.size()<=0){
                throw new Exception("Fichier invalide");
            }
            return listeNom.get(0);
        }catch(Exception e){
            throw e;
        }
    }
    
    public static boolean verifDisponibilite(String idpoint, String idproduit,Connection c)throws Exception{
        int verif = 0;
        DisponibiliteProduit[] resultat = new DisponibiliteProduit[0];
        try{
            if(c == null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            DisponibiliteProduit dp = new DisponibiliteProduit();
            String aWhere = " and idpoint like '"+idpoint+"' and id like '"+idproduit+"'";
            resultat = (DisponibiliteProduit[])CGenUtil.rechercher(dp, null, null, c, aWhere);
            
            if(resultat.length > 0){
                if(resultat[0].getPointindisp() != null){
                    return false;
                }
            }
            
            if(verif == 1) c.commit();
        }catch(Exception e){
            if(c!=null)c.rollback();
            e.printStackTrace();
            throw e;
        }
        finally{
            if(verif==1 && c!=null)c.close();
        }    
        return true;
    }
    
    public static String getNomRestau() throws Exception{
        try{
            ArrayList<String> listeNom=Lecture.lireFichier(Constante.fichierRestau);
            if(listeNom.size()<=0){
                throw new Exception("Fichier invalide");
            }
            return listeNom.get(0);
        }catch(Exception e){
            throw e;
        }
    }

    public static String[] getDetailsRestau() throws Exception{
        try{
            ArrayList<String> listeNom=Lecture.lireFichier(Constante.fichierRestau);
            if(listeNom.size()<=1){
                throw new Exception("Fichier invalide");
            }
            return listeNom.toArray(new String[0]);
        }catch(Exception e){
            throw e;
        }
    }

    public static TypeObjet getRestau(Connection c) throws Exception{
        int verif = 0;
        TypeObjet resultat = null;
        try{
            if(c == null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            String id = getNomRestau();
            TypeObjet point = new TypeObjet();
            point.setNomTable("POINT");
            Object[] listeRestau = CGenUtil.rechercher(point, null, null, c, "and UPPER(id) like UPPER('"+id+"')");
            if(listeRestau != null && listeRestau.length > 0){
                resultat = (TypeObjet) listeRestau[0];
            }
            
            if(verif == 1) c.commit();
        }catch(Exception e){
            if(c!=null)c.rollback();
            e.printStackTrace();
            throw e;
        }
        finally{
            if(verif==1 && c!=null)c.close();
        }    
        return resultat;
    }
    
    public static ClientPoint[] getTablePoints(Connection c)throws Exception{
        int verif = 0;
        ClientPoint[] resultat = null;
        try{
            if(c == null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }

            String idRestau = getRestau(c).getId();
            ClientPoint cp = new ClientPoint();
            Object[] listeTable = CGenUtil.rechercher(cp, null, null, c, "and (point like '"+idRestau+"' or point is null)");
            if(listeTable != null && listeTable.length > 0){
                resultat = (ClientPoint[]) listeTable;
            }
            
            if(verif == 1) c.commit();
        }catch(Exception e){
            if(c!=null)c.rollback();
            e.printStackTrace();
            throw e;
        }
        finally{
            if(verif==1 && c!=null)c.close();
        }    
        return resultat;
    }

    public static void achatBesoinQuotidien(String daty, String[] idingredients, String[] qte, String[] idunite, String[] prix, String[] nomFournisseur, UserEJB u)throws Exception{
        Connection c = null;
        try{
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            
            Fournisseur[] fournisseurs = (Fournisseur[]) CGenUtil.rechercher(new Fournisseur(), null, null, c, "");
            HashMap<String,Fournisseur> liste = new HashMap<>();
            for(Fournisseur frns : fournisseurs){
                liste.put(frns.getNomfournisseur(), frns);
            }
            
            Date date = Utilitaire.string_date("dd/MM/yyyy", daty);
            String iduser = ""+u.getUser().getRefuser();
            double quantite = 0, pu = 0, montant = 0;
            Fournisseur fournisseur = null;
            int size = idingredients.length;
            for(int i=0; i<size; i++){
                quantite = Utilitaire.stringToDouble(qte[i]);
                pu = Utilitaire.stringToDouble(prix[i]);
                montant = quantite * pu;
                fournisseur = liste.get(nomFournisseur[i]);
                if(fournisseur == null)
                    throw new Exception("Fournisseur introuvable: "+nomFournisseur[i]);
                Achat achat = new Achat(idingredients[i],quantite,idunite[i],pu,montant,date,iduser,fournisseur.getIdfournisseur());
                System.out.println(achat.getIdingredient()+", "+achat.getQuantite()+", "+achat.getIdunite()+", "+achat.getPrixunitaire()+", "+achat.getMontant()+", "+achat.getDaty()+", "+achat.getIduser()+", "+achat.getIdfournisseur());
                u.createObject(achat, c);
            }
            c.commit();
        }
        catch(Exception e){
            e.printStackTrace();
            if( c != null ){c.rollback();}
            throw e;
        }
        finally{
            if(c != null) c.close();
        }
    }
    public static void ajoutBesoinQuotidienMultiple(String daty, String[] idunites, String[] qtes, String[] idingredients, String[] qteRest, UserEJB u)throws Exception{
        Connection c = null;
        try{
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            
            Date date = Utilitaire.string_date("dd/MM/yyyy", daty);
            String iduser = ""+u.getUser().getRefuser();
            int size = idingredients.length;
            for(int i=0; i<size; i++){
                double quantite = Utilitaire.stringToDouble(qtes[i]);
                double qteRestant = Utilitaire.stringToDouble(qteRest[i]);
                BesoinQuotidien besoin = new BesoinQuotidien(date, idunites[i], quantite, idingredients[i], qteRestant, iduser);
                //System.out.println(date+", "+idunites[i]+", "+quantite+", "+idingredients[i]+", "+qteRestant+", "+iduser);
                u.createObject(besoin, c);
            }
            c.commit();
        }
        catch(Exception e){
            e.printStackTrace();
            if( c != null ){c.rollback();}
            throw e;
        }
        finally{
            if(c != null) c.close();
        }
    }
    
    public static void updateBesoin(String[] idbesoins,String[]quantites,Connection c) throws Exception{
        int verif = 0;
        try{
            if(c == null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            
            Besoin[] besoin=(Besoin[]) (CGenUtil.rechercher(new Besoin(),null,null,c,""));
            
            for(int i=0;i<idbesoins.length;i++){
                for(int j=0;j<besoin.length;j++){
                    if(besoin[j].getId().compareTo(idbesoins[i])==0){
                        besoin[j].setQuantite(Utilitaire.stringToDouble(quantites[i]));
                        besoin[j].updateToTable(c);
                    }
                }
            }
            if(verif == 1) c.commit();
        }
        catch(Exception e){
            e.printStackTrace();
            if(verif == 1) c.rollback();
            throw e;
        }
        finally{
            if(c != null && verif == 1) c.close();
        }
    }
    
    public static String getLastClient(Connection c,String table, String restaurant) throws Exception{
        String rep=null;
        Client2 clt = new Client2();
        boolean estOuvert=false;
        try{
            if(c == null)
            {
                c = new UtilDB().GetConn();
                estOuvert=true;
            }
            clt.setNomTable("as_client");
            clt.setNom(table);
            clt.setAdresse(restaurant);
            Client2[] liste=(Client2[])CGenUtil.rechercher(clt,null,null,c,"order by id desc");
            if(liste.length>0)
                clt=liste[0];
            rep=clt.getId();
        }
        catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        finally{
            if(c != null&&estOuvert==true) c.close();
        }
        return rep;
    }
    
    public static HashMap<String,Object> getListeCommandeEffectuees(String idClient,String date) throws Exception, SQLException{
        HashMap<String,Object> listeFinale=null;
        HashMap<String,Object[]> listeCommande=null;
        HashMap<String,Object[]> listeDetailCommande=null;
        HashMap<String,Object[]> listePhotos=null;
        double somme =0;
        Connection c=null;
        try{
            c=new UtilDB().GetConn();
            listeFinale=new HashMap<String,Object>();
            listeCommande=new HashMap<String,Object[]>();
            listeDetailCommande=new HashMap<String,Object[]>();
            listePhotos=new HashMap<String,Object[]>();
            
            CommandeClient cc=new CommandeClient();
            cc.setNomTable("AS_COMMANDECLIENT_LIBELLE_ID");
            System.out.println("Date ::"+date);
            String afterwhere = " and client='"+idClient+"' and etat>=9 and etat < 40";
            //
            if(date!= null && !date.isEmpty())
                afterwhere+=" and datecommande >= '"+Utilitaire.formatterDaty(date)+"'";
            CommandeClient[] listeCo=(CommandeClient[]) AlloSakafoService.getListe(cc,afterwhere,c);
            listeCommande.put("listeCommande", listeCo);
            
            CommandeClientDetailsSauce[] listeDetail=null;
            CommandeClientDetailsSauce ccd=new CommandeClientDetailsSauce();
            ccd.setNomTable("AS_DETAILSCOMMANDE_LIB_SAUCE");
            for(int i=0;i<listeCo.length;i++){
                listeDetail=(CommandeClientDetailsSauce[])AlloSakafoService.getListe(ccd, " and idMere='"+listeCo[i].getId()+"'", c);
                listeDetailCommande.put(listeCo[i].getId(), listeDetail);
                if(listeDetail.length>0){
                for(int j=0;j<listeDetail.length;j++){
                    String nomProduit=listeDetail[j].getProduit();
                    Produits[] prod=(Produits[])CGenUtil.rechercher(new Produits(),null,null,c," and designation='"+nomProduit+"'");
                    if(prod.length>0){
                        String image=prod[0].getPhoto();
                        if(image!=null){
                            listePhotos.put(nomProduit,image.split(";"));
                        }
                    }
                    somme+=(listeDetail[j].getPu()*listeDetail[j].getQuantite());
                }
            }
            }
            listeFinale.put("commande",listeCommande);
            listeFinale.put("listedetails", listeDetailCommande);
            listeFinale.put("listePhotos",listePhotos);
            listeFinale.put("total",Utilitaire.formaterAr(somme));
        }catch(Exception e){
            throw e;
        }finally{
            if(c!=null)c.close();
        }
        return listeFinale;
    }
    
    public static MenuCategorie[] getMenuCategorie() throws Exception {
        Connection c = null;
        try{
            c = new UtilDB().GetConn();
            
            MenuCategorie[] retour = null;
            
            TypeproduitSousCategorie[] listeTypeSousCat = (TypeproduitSousCategorie[]) CGenUtil.rechercher(new TypeproduitSousCategorie(), null, null, c, "");
            retour = new MenuCategorie[listeTypeSousCat.length];
            TypeObjet tp = new TypeObjet();
            tp.setNomTable("as_souscategorie");
            TypeObjet[] listeSousCat = (TypeObjet[]) CGenUtil.rechercher(tp, null, null, c, "");
            
            HashMap<String,TypeObjet> mapSousCat = new HashMap<>();
            for(TypeObjet sousCat : listeSousCat){
                mapSousCat.put(sousCat.getId(), sousCat);
            }
            
            int i = 0;
            for(TypeproduitSousCategorie typeSousCat : listeTypeSousCat){
                MenuCategorie menuCategorie = new MenuCategorie();
                menuCategorie.setIdtypeproduit(typeSousCat.getIdtypeproduit());
                menuCategorie.setLibelle_type(typeSousCat.getLibelle_type());
                
                String idsouscategories = typeSousCat.getIdsouscategories();
                if(idsouscategories != null && !"".equals(idsouscategories)){
                    String[] listeIdSousCat = Utilitaire.split(idsouscategories, ";");
                    MenuCategorie[] listeSousCategorie = new MenuCategorie[listeIdSousCat.length];
                    int j = 0;
                    for(String idSousCat : listeIdSousCat){
                        TypeObjet sousCat = mapSousCat.get(idSousCat);
                        listeSousCategorie[j] = new MenuCategorie(sousCat.getId(),sousCat.getVal());
                        j++;
                    }
                    menuCategorie.setSouscategorie(listeSousCategorie);
                }
                retour[i] = menuCategorie;
                i++;
            }
            
            return retour;
        }
        catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        finally{
            if(c != null) c.close();
        }
    }
    
    public static ProduitsTypeLibelle findProduitById(String id)throws Exception{
        Connection c = null;
        try{
            c = new UtilDB().GetConn();
            
            ProduitsTypeLibelle[] liste = (ProduitsTypeLibelle[]) CGenUtil.rechercher(new ProduitsTypeLibelle(), null, null, c, " and id='"+id+"'");
            return liste[0];
        }
        catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        finally{
            if(c != null) c.close();
        }
    }
    public static Object[] getListe(ClassMAPTable to,String aw)throws Exception{
        Object[] liste=null;
        try{
            liste=(Object[]) CGenUtil.rechercher(to,"select * from "+to.getNomTable()+" where 1<2 "+aw);
        }catch(Exception e){
            throw e;
        }
        return liste;
    }
    public static Object[] getListe(ClassMAPTable to,String aw,Connection c)throws Exception{
        Object[] liste=null;
        try{
            liste=(Object[]) CGenUtil.rechercher(to,null,null,c,aw);
        }catch(Exception e){
            throw e;
        }
        return liste;
    }
    public static HashMap<String,String[]> getPhotos(CommandeClientDetails[] listeCommande) throws Exception{
        Connection c=null;
        HashMap<String,String[]> listeImage=null;
        try{
            c=new UtilDB().GetConn();
            listeImage=new HashMap<String,String[]>();
            if(listeCommande.length>0){
                for(int i=0;i<listeCommande.length;i++){
                    String nomProduit=listeCommande[i].getProduit();
                    Produits[] prod=(Produits[])CGenUtil.rechercher(new Produits(),null,null,c," and designation='"+nomProduit+"'");
                    if(prod.length>0){
                        String image=prod[0].getPhoto();
                        if(image!=null){
                            listeImage.put(nomProduit,image.split(";"));
                        }
                    }
                }
            }else{
                throw new Exception("Pas de liste commande en vue!");
            }
        }catch(Exception e){
            throw e;
        }finally{
            if(c!=null){
                c.close();
            }
        }
        return listeImage;
    }
    public static String formaterHeure(String heure) throws Exception{
        String heureFinale=Utilitaire.testHeureValide2(heure);
        if(heureFinale.split(":").length==2){
            heureFinale+=":00";
        }
        return heureFinale;
    }
    
    public static void saisie2Mvt(ClassMAPTable o,UserEJBBean user,Connection c)throws Exception{
        int verif = 0;
        try{
            if(c==null){
               c=new UtilDB().GetConn();
               c.setAutoCommit(false);
               verif = 1;
            }
             Report report = (Report)o; 
              report=(Report)CGenUtil.rechercher(new Report(),null,null,c," and id='"+report.getId()+"'")[0];
              MvtCaisse[] saisieMvt = new MvtCaisse[2];
                Date datyj2 = Utilitaire.ajoutJourDate(Utilitaire.datetostring(report.getDaty()), 1);
                saisieMvt[0] = new MvtCaisse("report du "+report.getDaty(),report.getDevise(),"pay1", report.getCaisse(),"report",null, null,0, report.getMontant(), report.getDaty(), ConstanteEtat.etatreport,1);
                saisieMvt[0].construirePK(c);
                saisieMvt[0].insertToTableWithHisto(""+user.getUser().getRefuser(), c);
                //pay1
                saisieMvt[1] = new MvtCaisse("report du "+report.getDaty(),report.getDevise(),"pay1", report.getCaisse(),"report",null, null,report.getMontant(),0, datyj2, ConstanteEtat.etatreport,1);
                saisieMvt[1].construirePK(c);
                saisieMvt[1].insertToTableWithHisto(""+user.getUser().getRefuser(), c);
                
                user.validerObject(saisieMvt[0],c);
                user.validerObject(saisieMvt[1],c);
                c.commit();
            
                 
        }
        catch(Exception ex){
            if(c!=null)c.rollback();
            ex.printStackTrace();
            throw ex;
        }
        finally{
            if(verif==1 && c!=null)c.close();
        }
    }
    
    public static void verifierMvtIntraCaisse(MvtIntraCaisse mvt,Connection c)throws Exception
    {
        int verif=0;
        try
        {
            if(c == null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            
            if(mvt.getCaissearrive().compareToIgnoreCase(getCaisseDefaut()) != 0){
                throw new Exception("Action non permise");
            }
            
             MvtIntraCaisse[] lsMvt=(MvtIntraCaisse[])CGenUtil.rechercher(new MvtIntraCaisse(),null,null,c," and id!='"+mvt.getId()+"' and daty ='"+Utilitaire.datetostring(mvt.getDaty())+"' and  caissearrive='"+mvt.getCaissearrive()+"' and caissedepart='"+mvt.getCaissedepart()+"' and montant="+mvt.getMontant());
             if(lsMvt.length>0)
                 {
                     throw new Exception("Mouvement Intra caisse d�ja existant");
                 }
            
                 
        }
        catch(Exception e)
        {
            if( c != null ){c.rollback();}
            throw e;
        }
        finally
        {
            if(c!=null && verif==1)
            {
                c.close();
            }
            
        }
       
    }
    public static void viserMvtIntraCaisse(MvtIntraCaisse o,UserEJBBean user,Connection c)throws Exception
    {
        
        int verif=0;
        try
        {
            
            if(c == null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
                MvtIntraCaisse mvt=o;
                mvt=((MvtIntraCaisse[])(CGenUtil.rechercher(new MvtIntraCaisse(),null,null,c," and Id='"+mvt.getId()+"'")))[0];
              
                //verifierMvtIntraCaisse(mvt,c);
                    
                
             
                
                MvtCaisse mvtCaisse1=new MvtCaisse("Mouvement inter caisse le "+mvt.getDaty()+" entre "+mvt.getCaissedepart()+" et "+mvt.getCaissearrive(),"Ar","pay1",mvt.getCaissearrive(),"mouvement intra caisse",null,null,mvt.getMontant(),0,mvt.getDaty(),"4",1);
                mvtCaisse1.construirePK(c);
                mvtCaisse1.insertToTableWithHisto(""+user.getUser().getRefuser(), c);
               
                
                MvtCaisse mvtCaisse2=new MvtCaisse("Mouvement inter caisse le "+mvt.getDaty()+" entre "+mvt.getCaissedepart()+" et "+mvt.getCaissearrive(),"Ar","pay1",mvt.getCaissedepart(),"mouvement intra caisse",null,null,0,mvt.getMontant(),mvt.getDaty(),"4",1);
                mvtCaisse2.construirePK(c);
                mvtCaisse2.insertToTableWithHisto(""+user.getUser().getRefuser(), c);
              
               
                
                 user.validerObject(mvtCaisse1,c);
                 user.validerObject(mvtCaisse2,c);
                 c.commit();
                
               
                
        }
        catch(Exception e)
        {
            if( c != null ){c.rollback();}
            throw e;
        }
        finally
        {
            if(c!=null && verif==1)
            {
                c.close();
            }
        }
    }
    
     public static CommandeClientDetailsLibelle[] getdetailscommande(String idcommande,Connection c)throws Exception{
       int verif=0;
        try
        {
            if(c == null){
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            CommandeClientDetailsLibelle dtls = new CommandeClientDetailsLibelle();
            dtls.setNomTable("AS_DETAILS_COMMANDE_ETAT");
            CommandeClientDetailsLibelle[] mvt=((CommandeClientDetailsLibelle[])(CGenUtil.rechercher(dtls," select produit,sum(quantite) as quantite,pu from "+dtls.getNomTable()+" where idmere='"+idcommande+"' group by produit,pu",c)));
              return mvt;
            
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            if(c!=null && verif==1)
            {
                c.close();
            }
        }
     }
}
