/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import bean.CGenUtil;
import bean.TypeObjet;
import java.sql.Connection;
import java.sql.SQLException;
import mg.allosakafo.commande.CommandeClient;
import mg.allosakafo.facture.ClientPoint;
import mg.allosakafo.produits.Produits;
import service.AlloSakafoService;
import utilitaire.UtilDB;

/**
 *
 * @author nyamp
 */
public class CommandeModifData {
    private CommandeClient[] commandeClient;
    private ClientPoint[] clientPoint;
    
    public static  CommandeModifData getCommandeModifData(String idCommande, String pointDefaut) throws Exception{
        CommandeModifData data=new CommandeModifData();
        int verif = 0;
        Connection c=null;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            /*eto no apetraka ny code*/
            data.setClientPoint(pointDefaut, c);
            data.setCommandeClient(idCommande, c);
            
            if(c!=null && verif==1) c.commit();
        } catch (Exception e) {
            if (c != null && verif == 1) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
        return data;
    }
    
    public static  CommandeModifData getCommandeModifData(String idCommande) throws Exception{
        CommandeModifData data=new CommandeModifData();
        int verif = 0;
        Connection c=null;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            /*eto no apetraka ny code*/
            String pointDefaut=AlloSakafoService.getNomRestau();
            data.setClientPoint(pointDefaut, c);
            data.setCommandeClient(idCommande, c);
            
            if(c!=null && verif==1) c.commit();
        } catch (Exception e) {
            if (c != null && verif == 1) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
        return data;
    }
    
    public void setClientPoint(String pointDefaut, Connection c) throws SQLException, Exception {
        ClientPoint point=new ClientPoint();
        point.setPoint(pointDefaut);
        clientPoint=(ClientPoint[])CGenUtil.rechercher(point,null,null,c," or point is null");
    }

    
    
    public void setCommandeClient(String idCommande, Connection c) throws SQLException, Exception {
        CommandeClient commande=new CommandeClient();
        commande.setId(idCommande);
        commandeClient=(CommandeClient[])CGenUtil.rechercher(commande,null,null,c,"");
    }


    public CommandeClient[] getCommandeClient() {
        return commandeClient;
    }

    public void setCommandeClient(CommandeClient[] commandeClient) {
        this.commandeClient = commandeClient;
    }

    public ClientPoint[] getClientPoint() {
        return clientPoint;
    }

    public void setClientPoint(ClientPoint[] clientPoint) {
        this.clientPoint = clientPoint;
    }
    
    

}
