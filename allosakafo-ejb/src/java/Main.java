/**
 *
 * @author NERD
 */
import java.math.RoundingMode;
import java.text.DecimalFormat;
import service.AlloSakafoService;
public class Main {
    public static double arrondirDecimalWithMode(double a, String pattern, RoundingMode mode) {//pattern deux chffires apr�s la virgule #.##
        DecimalFormat df = new DecimalFormat(pattern);
        df.setRoundingMode(mode); //RoundingMode.HALF_UP, RoundingMode.HALF_DOWN
        String format = df.format(a).replace(",", ".");
        return Double.valueOf(format);
    }
    public static double[] arrondirEtAppoint(double montant , int arrondi){
        double[] ret = new double[2];
        System.out.println("montant : "+montant);
        int partieEntiere = (int) montant;
        System.out.println("partieEntiere : "+partieEntiere);
        double partieDecimal = montant - partieEntiere;
        partieDecimal = arrondirDecimalWithMode(partieDecimal, "#.##", RoundingMode.HALF_UP);
        System.out.println("partieDecimal : "+partieDecimal);
        int dernierNombreParLArrondi = partieEntiere % arrondi;
        System.out.println("dernierNombreParLArrondi : "+dernierNombreParLArrondi);
        double test = dernierNombreParLArrondi + partieDecimal;
        System.out.println("test : "+test);
        if(test>=50){
            System.out.println("mididtra 1");
            System.out.println("dernierNombreParLArrondi + (arrondi - dernierNombreParLArrondi)) "+(arrondi - dernierNombreParLArrondi));
            ret[0] = partieEntiere + (arrondi - dernierNombreParLArrondi);
            ret[1] = arrondirDecimalWithMode((ret[0] - montant), "#.##", RoundingMode.CEILING);
        }
        if(test<50){
            System.out.println("mididtra 3");
            ret[0] = partieEntiere - dernierNombreParLArrondi;
            ret[1] = arrondirDecimalWithMode((ret[0] - montant), "#.##", RoundingMode.CEILING);
        }
        return ret;
    }
    public static void main(String[] args) {
//        //System.out.println("HELLO WORLD : "+utilitaire.Utilitaire.dateDuJour());
//        final GsonBuilder builder = new GsonBuilder();
//        final Gson gson = builder.create();
//        Date daty = Utilitaire.stringDate("02/11/2015");

        
        
        
        try {
            double[] ret = arrondirEtAppoint(110403.92, 100);
            System.out.println(ret[0]);
            System.out.println(ret[1]);
//            String id  = AlloSakafoService.getIdRestau(null);
//            System.out.println("idRestau -----------------------------------------"+id);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }
}
