package affichage;

import bean.CGenUtil;
import bean.ClassMAPTable;
import java.lang.reflect.Method;
import java.sql.Connection;
import user.UserEJB;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;

/**
 * <p>
 * Title: Gestion des recettes </p>
 * <p>
 * Description: </p>
 * <p>
 * Copyright: Copyright (c) 2005</p>
 * <p>
 * Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */
public class Champ {

    private String nom;
    private String defaut = "";
    private String css = "form-control";
    private String type = ConstanteAffichage.typeTextBox;
    private String libelle;
    private String pageAppel;
    private String cssLibelle = "left";
    private String html;
    private int taille;
    private String valeur;
    private String typeData = ConstanteAffichage.typeTexte;
    private java.lang.reflect.Field attribut;
    private int estIntervalle = 0;
    private String autre;
    private String libelleAffiche;
    private String htmlInsert;
    private boolean visible = true;
    private boolean photo = false;
    private String htmlTableauInsert;
    private String htmlTableau;
    private String champReturn;
    private String apresLienPageappel = "";
    private String urlFiche = "";
    PageConsulteLien lien;
    private String valeur_ac;
    private boolean autoComplete = false;

    /*public String getLien() {
     return lien;
     }

     public void setLien(String lien) {
     this.lien = lien;
     }*/
     public String getValeur_ac() {
        return valeur_ac;
    }

    public void setValeur_ac(String valeur_ac) {
        this.valeur_ac = valeur_ac;
    }
    
    public String getUrlFiche() {
        return urlFiche;
    }

    public void setUrlFiche(String urlFiche) {
        this.urlFiche = urlFiche;
    }
    
    public String getApresLienPageappel() {
        return apresLienPageappel;
    }

    public void setApresLienPageappel(String apresLienPageappel) {
        this.apresLienPageappel = apresLienPageappel;
    }

    public PageConsulteLien getLien() {
        return lien;
    }

    public void setLien(PageConsulteLien lien) {
        this.lien = lien;
    }

    public void setLien(String lien, String queryString) {
        setLien(new PageConsulteLien(lien, queryString));
    }

    public boolean isPhoto() {
        return photo;
    }

    public void setPhoto(boolean photo) {
        this.photo = photo;
    }

    public Champ() {
    }

    public Champ(String nom) {
        setNom(nom);
    }

    public String toString() {
        return getNom();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDefaut(String defaut) {
        this.defaut = defaut;
    }

    public String getDefaut() {
        return defaut;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public String getCss() {
        return css;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getLibelle() {
        if (libelle == null || libelle.compareToIgnoreCase("") == 0) {
            return nom;
        }
        return libelle;
    }

    public void setPageAppel(String pageAppel) {
        this.pageAppel = pageAppel;
        this.champReturn = this.getNom() + ";" + this.getNom()+"libelle";
    }

    public void setPageAppelIndice(String pageAppel, int i) {
        this.pageAppel = pageAppel;
        String[] tGetNom = this.getNom().split("_");
        if(tGetNom.length > 0)
            this.champReturn = this.getNom() + ";" + tGetNom[0]+"libelle_"+ tGetNom[1];
    }
            
    public String getChampReturn() {
        return champReturn;
    }

    public void setChampReturn(String champReturn) {
        this.champReturn = champReturn;
    }

    public void setPageAppel(String pageAppel, String champReturnn) {
        this.pageAppel = pageAppel;
        this.champReturn = champReturnn;
    }

    public String getPageAppel() {
        return pageAppel;
    }

    public void setCssLibelle(String cssLibelle) {
        this.cssLibelle = cssLibelle;
    }

    public String getCssLibelle() {
        return cssLibelle;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public void setHtmlTableau(String htmlTableau) {
        this.htmlTableau = htmlTableau;
    }

    public String getHtml() throws Exception {
        if (html == null || html.compareToIgnoreCase("") == 0) {
            this.makeHtml();
        }
        return html;
    }

    public String getHtmlTableau(int i) throws Exception {
        this.makeHtmlTableau(i);
        return htmlTableau;
    }

    public void makeHtml() throws Exception {
        String temp = "";
        if (type.compareTo(ConstanteAffichage.textarea) == 0) {
            //temp = "<textarea name='" + getNom() + "' row='3' class='" + getCss() + "' id='" + getNom() + "' maxlength='" + getTaille() + "'>" + getValeur() + "</textarea>";
            temp = "<textarea name='" + getNom() + "' class='" + getCss() + "' id='" + getNom() + "' style='resize: none;'>" + getValeur() + "</textarea>";
        } else if (!this.isPhoto()) {
            temp = "<input name=" + getNom() + " type=" + getType() + " class=" + getCss() + "  id=" + getNom() + " value='" + getValeur() + "' " + getAutre() + ">";
            
        } else {

            temp = "<input name=" + getNom() + " type=\"file\" class=" + getCss() + " id=" + getNom() + " " + getAutre() + ">";
            if (!getValeur().isEmpty()) {
                temp += "<input type=\"hidden\" value=\"" + getValeur() + "\" name=\"" + getNom() + "2" + "\">";
            }
        }
        setHtml(temp);
    }

    /*public void makeHtmlTableau(int i) throws Exception {
        String temp = "";
        //System.out.println("i="+i);
        if (type.compareTo(ConstanteAffichage.textarea) == 0) {
            //temp = "<textarea name='" + getNom() + "' row='3' class='" + getCss() + "' id='" + getNom() + "' maxlength='" + getTaille() + "'>" + getValeur() + "</textarea>";
            temp = "<textarea name='" + getNom() + "_" + i + "' class='" + getCss() + "' id='" + getNom() + "' style='resize: none;'>" + getValeur() + "</textarea>";
        } else if (!this.isPhoto()) {
            temp = "<input name='" + getNom() + "_" + i + "' type=" + getType() + " class=" + getCss() + "  id=" + getNom() + " value='" + getValeur() + "' " + getAutre() + ">";

        } else {
            //System.out.println("getValeur() insert ===  " + getValeur());
            temp = "<input name='" + getNom() + "_" + i + "' type=\"file\" class=col-md-9 " + getCss() + " id=" + getNom() + " " + getAutre() + ">";
            if (!getValeur().isEmpty()) {

                temp += "<input type=\"hidden\" value=\"" + getValeur() + "\" name=\"" + getNom() + "2" + "_" + i + "\">";
            }
        }
        setHtmlTableau(temp);
    }*/
    
    public void makeHtmlTableau(int i) throws Exception {
        String temp = "";
        //System.out.println("i="+i);
        if(getVisible()==false)
        {
            temp = "<input name='" + getNom() + "' type='hidden' class=" + getCss() + "  id=" + getNom() + " value='" + getValeur() + "' " + getAutre() + ">";
        }else if (type.compareTo(ConstanteAffichage.textarea) == 0) {
            //temp = "<textarea name='" + getNom() + "' row='3' class='" + getCss() + "' id='" + getNom() + "' maxlength='" + getTaille() + "'>" + getValeur() + "</textarea>";
            temp = "<textarea name='" + getNom() + "' class='" + getCss() + "' id='" + getNom() + "' style='resize: none;'>" + getValeur() + "</textarea>";
        } else if (!this.isPhoto()) {
            //System.out.println("***************"+getValeur());
            temp = "<input name='" + getNom() + "' type=" + getType() + " class=" + getCss() + "  id=" + getNom() + " value='" + getValeur() + "' " + getAutre() + ">";

        } else {
            //System.out.println("getValeur() insert ===  " + getValeur());
            temp = "<input name='" + getNom() + "' type=\"file\" class=" + getCss() + " id=" + getNom() + " " + getAutre() + ">";
            if (!getValeur().isEmpty()) {

                temp += "<input type=\"hidden\" value=\"" + getValeur() + "\" name=\"" + getNom() + "2" + "_" + i + "\">";
            }
        }
        setHtmlTableau(temp);
    }

    public void makeHtmlInsert() throws Exception {
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    public int getTaille() {
        return taille;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public String getValeur() {
        if (valeur == null || valeur.compareToIgnoreCase("null") == 0 || valeur.compareToIgnoreCase("") == 0) {
            if (getDefaut() == null || getDefaut().compareToIgnoreCase("null") == 0) {
                return "";
            }
            return getDefaut();
        }
        return valeur;
    }

    public void setTypeData(String typeData) {
        this.typeData = typeData;
    }

    public String getTypeData() {
        return typeData;
    }

    public void setAttribut(java.lang.reflect.Field attribut) {
        this.attribut = attribut;
    }

    public java.lang.reflect.Field getAttribut() {
        return attribut;
    }

    public void setEstIntervalle(int estIntervalle) {
        this.estIntervalle = estIntervalle;
    }

    public int getEstIntervalle() {
        return estIntervalle;
    }

    public void findData(Connection c) throws Exception {
    }

    public void setAutre(String autre) {
        this.autre = autre;
    }

    public String getAutre() {
        if (autre == null) {
            return "";
        }
        return autre;
    }

    public void setLibelleAffiche(String libelleAffiche) {
        this.libelleAffiche = libelleAffiche;
    }

    public String getLibelleAffiche() {
        if (libelleAffiche == null || libelleAffiche.compareToIgnoreCase("") == 0) {
            return getLibelle();
        }
        return libelleAffiche;
    }

    public void setHtmlInsert(String htmlInsert) {
        this.htmlInsert = htmlInsert;
    }

    public String getHtmlInsert() throws Exception {
        if (htmlInsert == null || htmlInsert.compareToIgnoreCase("") == 0) {
            makeHtmlInsert();
        }
        if (htmlInsert == null || htmlInsert.compareToIgnoreCase("") == 0) {
            htmlInsert = getHtml();
        }
        return htmlInsert;
    }

    public String getInputHidden() throws Exception {
        makeInputPopup();
        return htmlInsert;
    }

    public void makeInputPopup() throws Exception {
        String temp = "";
        temp = "<input name=" + getNom() + "libelle" + " type=" + getType() + " class=" + getCss() + "  id=" + getNom() + "libelle" + " value='" + getValeur() + "' " + getAutre() + ">";
        temp += "<input type='hidden' value='" + getValeur() + "' name='" + getNom() + "' id='" + getNom() + "'>";
        setHtml(temp);
        setHtmlInsert(temp);
    }
    
    public String getInpuTabtHidden(int i) throws Exception {
        makeInputTabPopup(i);
        return htmlInsert;
    }

    public void makeInputTabPopup(int i) throws Exception {
        String temp = "";
        String[] tGetNom = this.getNom().split("_");
        if(tGetNom.length > 0)
            temp = "<input name=" + tGetNom[0]+"libelle_"+ tGetNom[1] + " type=" + getType() + " class=" + getCss() + "  id=" + tGetNom[0]+"libelle_"+ tGetNom[1] + " value='" + getValeur() + "' " + getAutre() + ">";
        temp += "<input type='hidden' value='" + getValeur() + "' name='" + getNom() + "' id='" + getNom() + "'>";
        setHtml(temp);
        setHtmlInsert(temp);
    }
    

    public void setHtmlTableauInsert(String htmlTableauInsert) {
        this.htmlTableauInsert = htmlTableauInsert;
    }

    /*public String getHtmlTableauInsert(int i) throws Exception {
        htmlTableauInsert = getHtmlTableau(i);
        return htmlTableauInsert;
    }*/
    
    public String getHtmlTableauInsert(int i) throws Exception {
        htmlTableauInsert = null;
        makeHtmlInsertTableau(i);
        if (htmlTableauInsert == null || htmlTableauInsert.compareToIgnoreCase("") == 0) {
            htmlTableauInsert = getHtmlTableau(i);
        }
        return htmlTableauInsert;
        //htmlTableauInsert = getHtmlTableau(i);
        //return htmlTableauInsert;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean getVisible() {
        return visible;
    }

    public void setAutocomplete(String affiche, String valeur, String nomTable) {

    }
      public String getHtmlTableau(int i, String valeur) throws Exception {
        this.makeHtmlTableau(i, valeur);
        return htmlTableau;
    }

    public void makeHtmlTableau(int i, String valeur) throws Exception {
        String temp = "";
        setValeur(valeur);
        if (type.compareTo(ConstanteAffichage.textarea) == 0) {
            temp = "<textarea name='" + getNom() + "_" + i + "' class='" + getCss() + "' id='" + getNom() + "_" + i + "' style='resize: none;'>" + getValeur() + "</textarea>";
        } else if (typeData.compareTo(ConstanteAffichage.typeDaty) == 0) {
            temp = "<input name=" + getNom() + "_" + i + " type=" + getType() + " class='" + getCss() + " datepicker'  id=" + getNom() + "_" + i + " value='" + getValeur() + "' " + getAutre() + ">";

        } else if (!this.isPhoto()) {
            temp = "<input name=" + getNom() + "_" + i + " type=" + getType() + " class=" + getCss() + "  id=" + getNom() + "_" + i + " value='" + getValeur() + "' " + getAutre() + ">";

        } else {
            temp = "<input name=" + getNom() + "_" + i + " type=\"file\" class=" + getCss() + " id=" + getNom() + "_" + i + " " + getAutre() + ">";
            if (!getValeur().isEmpty()) {
                temp += "<input type=\"hidden\" value=\"" + getValeur() + "\" name=\"" + getNom() + "_" + i + "2" + "\">";
            }
        }
        setHtmlTableau(temp);
    }
    public void makeHtmlInsertTableau(int indice, String valeur) throws Exception {
    }

      public String getHtmlTableauInsert(int i, String valeur) throws Exception {
        htmlTableauInsert = null;
        makeHtmlInsertTableau(i, valeur);
        if (htmlTableauInsert == null || htmlTableauInsert.compareToIgnoreCase("") == 0) {
            htmlTableauInsert = getHtmlTableau(i, valeur);
        }
        return htmlTableauInsert;
    }
      
      public boolean isAutoComplete() {
        return autoComplete;
    }
    public void setAutoComplete(boolean autoComplete) {
        this.autoComplete = autoComplete;
    }
    
    
    public String getInputHiddenAutoComplete() throws Exception {
        String temp = "";
        temp = "<input name=" + getNom() + "libelle" + " type=" + getType() + " class=" + getCss() + "  id=" + getNom() + "libelle" + " value='" + getValeur() + "' " + getAutre() + ">";
        temp += "<input type='hidden' value='" + getValeur() + "' name='" + getNom() + "' id='" + getNom() + "'>";
        return temp;
    }
    
     public String getChampReturn(int indexe) {
        String champ = "";
        String[] tchampReturn = Utilitaire.split(champReturn, ";");
        for(int i=0; i<tchampReturn.length; i++){
            champ += tchampReturn[i];
            if(i != tchampReturn.length-1)
                champ += ";";
        }
        return champ;
    }
     
    public void makeHtmlInsertTableau(int indice) throws Exception {
    }
    
    public static void setVisible(Champ[] tChamp, boolean visible) {
        for(Champ c : tChamp)
            c.setVisible(visible);
    }
}
