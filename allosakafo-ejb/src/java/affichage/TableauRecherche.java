package affichage;

import bean.CGenUtil;
import bean.ListeColonneTable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import utilitaire.Utilitaire;

/**
 * <p>
 * Title: Gestion des recettes </p>
 * <p>
 * Description: </p>
 * <p>
 * Copyright: Copyright (c) 2005</p>
 * <p>
 * Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */
public class TableauRecherche {

    private String[] libeEntete;
    private bean.ClassMAPTable[] data;
    private String cssEntete = "head";
    private String cssTableau = "table" + " " + "table-hover";
    private String html;
    private String titre = "LISTE";
    private int[] propEntet;
    private String tailleTableau = "100%";
    private String[] libelleAffiche;
    private String[][] dataDirecte;
    private String[] lien;
    private String[] colonneLien;
    private String modelePage;
    private String[] attLien = null;
    private String[] valeurLien;
    private String expxml;
    private String expcsv;
    private String exppdf;
    private double[][] pourc;
    private String[] pourcentage;
    private String critereLienString = "";
    private boolean afficheBouttondevalider = false;
    private String ids;
    private String[] apresLienTab = null;
    Field[] listeField = null;
    private String[] urlLien;
    private String[] urlLienAffiche;
    private String nameBoutton = "Valider";
    private String nameActe = "attacher";
    private String nameBoutton2 = "";
    private String nameActe2 = "";
    private Map libelleEnteteAAffiche = new HashMap();
   
    private String[] lienGroupe = null;
    private String[] variableLien = null;
     private Formulaire formu;
    public Formulaire getFormu() {
        return formu;
    }

    public void setFormu(Formulaire formu) {
        this.formu = formu;
    }

    public String[] getVariableLien() {
        if(variableLien==null)return this.getLibeEntete();
        return variableLien;
    }

    public void setVariableLien(String[] variableLienGroupe) {
        this.variableLien = variableLienGroupe;
    }
    private String lienFormulaire = "";

    public String getLienFormulaire() {
        return lienFormulaire;
    }

    public void setLienFormulaire(String lienFormulaire) {
        this.lienFormulaire = lienFormulaire;
    }
    
   

    
    
    public String[] getLienGroupe() {
        return lienGroupe;
    }

    public void setLienGroupe(String[] lienGroupe) {
        List<String> list = new ArrayList();
        for (int i = 0; i < lienGroupe.length; i++) {
            if (lienGroupe[i] != null && !lienGroupe[i].equals("") && !lienGroupe[i].equals("%") && !lienGroupe[i].isEmpty()) {
                list.add(lienGroupe[i]);
            }
        }
        this.lienGroupe = list.toArray(new String[0]);
    }

    public String getNameBoutton2() {
        return nameBoutton2;
    }

    public void setNameBoutton2(String nameBoutton2) {
        this.nameBoutton2 = nameBoutton2;
    }

    public String getNameActe2() {
        return nameActe2;
    }

    public void setNameActe2(String nameActe2) {
        this.nameActe2 = nameActe2;
    }

    public void setNameBoutton(String nameBoutton) {
        this.nameBoutton = nameBoutton;
    }

    public void setNameActe(String nameActe) {
        this.nameActe = nameActe;
    }

    public String[] getUrlLienAffiche() {
        return urlLienAffiche;
    }

    public void setUrlLienAffiche(String[] urlLienAffiche) {
        this.urlLienAffiche = urlLienAffiche;
    }

    public Field[] getListeField() {
        return listeField;
    }

    public void setListeField(Field[] listeField) {
        this.listeField = listeField;
    }

    public String[] getApresLienTab() {
        return apresLienTab;
    }

    public void setApresLienTab(String[] apresLienTab) {
        this.apresLienTab = apresLienTab;
    }

    public boolean isAfficheBouttondevalider() {
        return afficheBouttondevalider;
    }

    public void setAfficheBouttondevalider(boolean afficheBouttondevalider) {
        this.afficheBouttondevalider = afficheBouttondevalider;
    }

    public String getCritereLienString() {
        return critereLienString;
    }

    public void setCritereLienString(String critereLienString) {
        this.critereLienString = critereLienString;
    }

    public TableauRecherche(bean.ClassMAPTable[] donne, String lib[], int[] prop) throws Exception {
        setData(donne);
        setLibeEntete(lib);
        setPropEntet(prop);
        transformerDataString();
    }

    public TableauRecherche(bean.ClassMAPTable[] donne, String[] libEnte) throws Exception {
        setData(donne);
        setLibeEntete(libEnte);
        transformerDataString();
    }

    public TableauRecherche(String[][] donne, String[] libEnte) throws Exception {
        setLibeEntete(libEnte);
        setDataDirecte(donne);
    }

    public TableauRecherche(bean.ClassMAPTable[] donne, String[] libEnte, String critereLienTab, Field[] listFields) throws Exception {
        setData(donne);
        setListeField(listFields);
        setCritereLienString(critereLienTab);
        setLibeEntete(libEnte);
        transformerDataString();
    }

    public TableauRecherche(bean.ClassMAPTable[] donne, String[] libEnte, String critereLienTab) throws Exception {
        setData(donne);
        setCritereLienString(critereLienTab);
        setLibeEntete(libEnte);
        transformerDataString();
    }

    public TableauRecherche(bean.ClassMAPTable[] donne, String[] libEnte, String critereLienTab, String ids) throws Exception {
        setData(donne);
        setCritereLienString(critereLienTab);
        setLibeEntete(libEnte);
        setIds(ids);
        transformerDataString();
    }

    public TableauRecherche(bean.ClassMAPTable[] donne, String[] libEnte, String[] pourcentage, double[][] pourc, String critereLienTab) throws Exception {
        setData(donne);
        setPourc(pourc);
        //setPourcentage(pourcentage);
        setLibeEntete(libEnte);
        setCritereLienString(critereLienTab);
        transformerDataString();
    }

    public TableauRecherche(bean.ClassMAPTable[] donne, String[] libEnte, String[] pourcentage, double[][] pourc, String critereLienTab, Field[] listFields) throws Exception {
        setData(donne);
        setPourc(pourc);
        setListeField(listFields);
        //setPourcentage(pourcentage);
        setLibeEntete(libEnte);
        setCritereLienString(critereLienTab);
        transformerDataString();
    }

    public void makeHtml() throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";
        String tempcsv = "";
        String tempxml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \r\n <tableau>";
        temp += "<div class=\"row\">";
        temp += "<div class=\"row col-md-12\">";
        temp += "<div class=\"box box-primary\">";
        temp += "<div class=\"box-header\">";
        temp += "<h3 class=\"box-title\" align=\"center\">" + getTitre() + "</h3>";
        temp += "</div>";
        temp += "<div class=\"box-body table-responsive no-padding\">";
        temp += "<div id=\"selectnonee\">";
        temp += "<table width=\"" + getTailleTableau() + "\" border=\"0\" align=\"center\" cellpadding=\"3\" cellspacing=\"3\" class=\"table table-hover table-bordered\">";
        temp += "<thead>";
        temp += "<tr class=\"" + getCssEntete() + "\">";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;

        Field[] listeFields = getListeField();

        nombreColonne = getDataDirecte()[0].length;
        for (int i = 0; i < nombreColonne; i++) {
            String alignement = "center";

            if (getCritereLienString().compareToIgnoreCase("") != 0) {
                String libelle = getLibelleEnteteAAffiche().get(getLibeEntete()[i]) == null ? getLibelleAffiche()[i] : (String) getLibelleEnteteAAffiche().get(getLibeEntete()[i]);
                temp += "<th width=\"" + getPropEntet()[i] + "%\" align=\"" + alignement + "\" valign=\"top\" style=\"background-color:#103a8e\">" + getCritereLienString() + "&triCol=yes&newcol=" + getLibeEntete()[i] + " style='color:white;'>" + libelle + "</a></th>";
            } else {
                temp += "<th width=\"" + getPropEntet()[i] + "%\" align=\"" + alignement + "\" valign=\"top\" style=\"background-color:#ffffff\">" + getLibeEntete()[i] + "</th>";
            }
            tempcsv += getLibelleAffiche()[i];
            if (i != nombreColonne - 1) {
                tempcsv += ";";
            }
        }
        temp += "</tr>";
        temp += "</thead>";
        temp += "<tbody>";
        for (int i = 0; i < nombreLigne; i++) {

            temp += "<tr onmouseover=\"this.style.backgroundColor='#EAEAEA'\" onmouseout=\"this.style.backgroundColor=''\">";
            tempxml += "<row>";
            int nombreLien = 0, j = 0, l = 0;
            for (j = 0; j < nombreColonne; j++) {
                String alignement = "left";
                if (listeFields != null) {
                    for (int k = 0; k < listeFields.length; k++) {
                        if (listeFields[k].getName().compareToIgnoreCase(getLibeEntete()[j]) == 0) {
                            if (listeFields[k].getType().getSimpleName().compareToIgnoreCase("String") == 0) {
                                alignement = "left";
                                break;
                            } else if (listeFields[k].getType().getSimpleName().compareToIgnoreCase("Double") == 0) {
                                alignement = "right";
                                break;
                            } else {
                                alignement = "center";
                                break;
                            }
                        }
                    }
                }
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                
                if (numeroColonne != -1) {
                    String colLien = getVariableLien()[l];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    String rajoutLien = "";
                    if (getUrlLien() != null) {
                        rajoutLien = getRajoutLienMultiple(getData()[i], getUrlLien()[l], getUrlLienAffiche()[l]);
                    }
                    l++;
                    String lienAutre = "";
                    if (getLienGroupe() != null) {
                        //System.out.println("MANDALOOOO");
                        for (int g = 0; g < getLienGroupe().length - 1; g++) {
                           
                            lienAutre += getLienGroupe()[g] + "=" + getDataDirecte()[i][g] + "&";
                        }
                        lienAutre += getLienGroupe()[getLienGroupe().length - 1] + "=" + getDataDirecte()[i][getLienGroupe().length - 1]+"&"+getLienFormulaire();
                    }else{
                        lienAutre = colLien + "=" + valL;
                    }

                    lien = "<a href='" + getLien()[numeroColonne] + "&" + lienAutre + rajoutLien + "'>";
                    apresLien = "</a>";
                }
                boolean test = false;
                if (getDataDirecte()[i][j].getClass().getName().compareToIgnoreCase("java.lang.String") == 0) {
                    alignement = "left";
                }
                String testsplit = Utilitaire.enleverEspaceDoubleBase(getDataDirecte()[i][j]);
                if (testsplit.contains(",")) {
                    String[] testsplits = testsplit.split(",");
                    if (Utilitaire.isStringNumeric(testsplits[0]) && Utilitaire.isStringNumeric(testsplits[1])) {
                        alignement = "right";
                    }
                }
                if (getPourcentage() != null) {
                    for (int element = 0; element < getPourcentage().length; element++) {
                        if (libeEntete[j].equals(getPourcentage()[element])) {
                            test = true;
                            temp += "<td width=\"" + getPropEntet()[j] + "%\" align=\"" + alignement + "\">" + lien + Utilitaire.champNull(getDataDirecte()[i][j]) + " (" + Utilitaire.formaterAr(getPourc()[element][i]) + " %)" + apresLien + " </td>";
                            break;
                        }
                    }
                }
                if (!test) {

                    temp += "<td width=\"" + getPropEntet()[j] + "%\" align=\"" + alignement + "\" >" + lien + Utilitaire.champNull(getDataDirecte()[i][j]) + apresLien + " </td>";
                }

                tempcsv += Utilitaire.verifNumerique(getDataDirecte()[i][j]);
                tempxml += "<" + getLibelleAffiche()[j] + ">" + Utilitaire.verifNumerique(getDataDirecte()[i][j]) + "</" + getLibelleAffiche()[j] + ">\r\n";
                if (j != nombreColonne - 1) {
                    tempcsv += ";";
                }
            }
            temp += "</tr>";
            tempcsv += "\r\n";
            tempxml += "</row>\r\n";
        }
        temp += "</tbody>";
        tempxml += "</tableau>\r\n";
        temp += "</table>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        setHtml(temp);
        setExpcsv(tempcsv);
        setExpxml(tempxml);
    }

    public void makeHtml(boolean recap, String recap1, String recap2) throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";
        String tempcsv = "";
        String tempxml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \r\n <tableau>";
        temp += "<div class=\"row\">";
        temp += "<div class=\"row col-md-12\">";
        temp += "<div class=\"box\">";
        temp += "<div class=\"box-header\">";
        temp += "<h3 class=\"box-title\" align=\"center\">" + getTitre() + "</h3>";
        temp += "</div>";
        temp += "<div class=\"box-body table-responsive no-padding\">";
        temp += "<div id=\"selectnonee\">";
        temp += "<table width=\"" + getTailleTableau() + "\" border=\"1\" align=\"center\" cellpadding=\"3\" cellspacing=\"3\" class=\"table table-hover table-bordered\">";
        temp += "<thead>";

        if (recap) {
            System.out.println("******** MISY RECAP");
            temp += "<tr class=\"" + getCssEntete() + "\">";
            temp += "<th></th>";
            temp += "<th></th>";
            temp += "<th></th>";
            temp += "<th></th>";
            temp += "<th></th>";
            temp += "<th>Somme montant</th>";
            temp += "<th>" + recap2 + "</th>";
            temp += "<th>Somme credit</th>";
            temp += "<th>" + recap2 + "</th>";
            temp += "</tr>";
        }

        temp += "<tr class=\"" + getCssEntete() + "\">";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;

        Field[] listeFields = getListeField();

        nombreColonne = getDataDirecte()[0].length;
        for (int i = 0; i < nombreColonne; i++) {
            String alignement = "center";

            if (getCritereLienString().compareToIgnoreCase("") != 0) {
                String libelle = getLibelleEnteteAAffiche().get(getLibeEntete()[i]) == null ? getLibelleAffiche()[i] : (String) getLibelleEnteteAAffiche().get(getLibeEntete()[i]);
                temp += "<th width=\"" + getPropEntet()[i] + "%\" align=\"" + alignement + "\" valign=\"top\" style=\"background-color:#103a8e\">" + getCritereLienString() + "&triCol=yes&newcol=" + getLibeEntete()[i] + " style='color:white;'>" + libelle + "</a></th>";
            } else {
                temp += "<th width=\"" + getPropEntet()[i] + "%\" align=\"" + alignement + "\" valign=\"top\" style=\"background-color:#103a8e\">" + getLibeEntete()[i] + "</th>";
            }
            tempcsv += getLibelleAffiche()[i];
            if (i != nombreColonne - 1) {
                tempcsv += ";";
            }
        }
        temp += "</tr>";
        temp += "</thead>";
        temp += "<tbody>";
        for (int i = 0; i < nombreLigne; i++) {

            temp += "<tr onmouseover=\"this.style.backgroundColor='#EAEAEA'\" onmouseout=\"this.style.backgroundColor=''\">";
            tempxml += "<row>";
            int nombreLien = 0, j = 0, l = 0;
            for (j = 0; j < nombreColonne; j++) {
                String alignement = "left";
                if (listeFields != null) {
                    for (int k = 0; k < listeFields.length; k++) {
                        if (listeFields[k].getName().compareToIgnoreCase(getLibeEntete()[j]) == 0) {
                            if (listeFields[k].getType().getSimpleName().compareToIgnoreCase("String") == 0) {
                                alignement = "left";
                                break;
                            } else if (listeFields[k].getType().getSimpleName().compareToIgnoreCase("Double") == 0) {
                                alignement = "right";
                                break;
                            } else {
                                alignement = "center";
                                break;
                            }
                        }
                    }
                }
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());

                if (numeroColonne != -1) {
                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    String rajoutLien = "";
                    if (getUrlLien() != null) {
                        rajoutLien = getRajoutLienMultiple(getData()[i], getUrlLien()[l], getUrlLienAffiche()[l]);
                    }
                    l++;
                    lien = "<a href='" + getLien()[numeroColonne] + "&" + colLien + "=" + valL + rajoutLien + "'>";
                    apresLien = "</a>";
                }
                boolean test = false;
                if (getDataDirecte()[i][j].getClass().getName().compareToIgnoreCase("java.lang.String") == 0) {
                    alignement = "left";
                }
                String testsplit = Utilitaire.enleverEspaceDoubleBase(getDataDirecte()[i][j]);
                if (testsplit.contains(",")) {
                    String[] testsplits = testsplit.split(",");
                    if (Utilitaire.isStringNumeric(testsplits[0]) && Utilitaire.isStringNumeric(testsplits[1])) {
                        alignement = "right";
                    }
                }
                if (getPourcentage() != null) {
                    for (int element = 0; element < getPourcentage().length; element++) {
                        if (libeEntete[j].equals(getPourcentage()[element])) {
                            test = true;
                            temp += "<td width=\"" + getPropEntet()[j] + "%\" align=\"" + alignement + "\">" + lien + Utilitaire.champNull(getDataDirecte()[i][j]) + " (" + Utilitaire.formaterAr(getPourc()[element][i]) + " %)" + apresLien + " </td>";
                            break;
                        }
                    }
                }
                if (!test) {

                    temp += "<td width=\"" + getPropEntet()[j] + "%\" align=\"" + alignement + "\" >" + lien + Utilitaire.champNull(getDataDirecte()[i][j]) + apresLien + " </td>";
                }

                tempcsv += Utilitaire.verifNumerique(getDataDirecte()[i][j]);
                tempxml += "<" + getLibelleAffiche()[j] + ">" + Utilitaire.verifNumerique(getDataDirecte()[i][j]) + "</" + getLibelleAffiche()[j] + ">\r\n";
                if (j != nombreColonne - 1) {
                    tempcsv += ";";
                }
            }
            temp += "</tr>";
            tempcsv += "\r\n";
            tempxml += "</row>\r\n";
        }
        temp += "</tbody>";
        tempxml += "</tableau>\r\n";
        temp += "</table>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        setHtml(temp);
        setExpcsv(tempcsv);
        setExpxml(tempxml);
    }

    public void makeHtmlPDF() throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";

        temp += "<table align='center' border='1'>";
        temp += "<thead>";
        temp += "<tr>";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;
        nombreColonne = getDataDirecte()[0].length;
        for (int i = 0; i < nombreColonne; i++) {
            temp += "<th style=\"font-size:6px\">" + getLibelleAffiche()[i] + "</th>";
        }
        temp += "</tr>";

        temp += "</thead>";
        temp += "<tbody>";
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<tr>";
            int nombreLien = 0, j = 0;
            for (j = 0; j < nombreColonne; j++) {
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                if (numeroColonne != -1) {
                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    lien = "";
                    apresLien = "";
                }
                boolean test = false;
                if (getPourcentage() != null) {
                    for (int element = 0; element < getPourcentage().length; element++) {
                        if (libeEntete[j].equals(getPourcentage()[element])) {
                            test = true;
                            temp += "<td align='center' style=\"font-size:6px\">" + lien + getDataDirecte()[i][j] + " (" + Utilitaire.formaterAr(getPourc()[element][i]) + " %)" + apresLien + " </td>";
                            break;
                        }
                    }
                }
                if (!test) {
                    //if (Utilitaire.isStringNumeric(getDataDirecte()[i][j])) {
                    temp += "<td align='center' style=\"font-size:6px\">" + lien + getDataDirecte()[i][j] + apresLien + " </td>";

                }

            }

            temp += "</tr>";
        }
        temp += "</tbody>";
        temp += "</table>";
        //temp+=" <p> "+tempxml+" </p>";
        setHtml(temp);

    }

    public void makeHtmlPDF(boolean recap, String v1, String v2) throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";

        temp += "<table align='center' border='1'>";
        temp += "<thead>";
        if (recap) {
            System.out.println("******** MISY RECAP");
            temp += "<tr>";
            temp += "<th></th>";
            temp += "<th></th>";
            temp += "<th></th>";
            temp += "<th></th>";
            temp += "<th></th>";
            temp += "<th>Somme montant</th>";
            temp += "<th>" + v1 + "</th>";
            temp += "<th>Somme credit</th>";
            temp += "<th>" + v2 + "</th>";
            temp += "</tr>";
        }
        temp += "<tr>";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;
        nombreColonne = getDataDirecte()[0].length;
        for (int i = 0; i < nombreColonne; i++) {
            temp += "<th style=\"font-size:6px\">" + getLibelleAffiche()[i] + "</th>";
        }
        temp += "</tr>";

        temp += "</thead>";
        temp += "<tbody>";
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<tr>";
            int nombreLien = 0, j = 0;
            for (j = 0; j < nombreColonne; j++) {
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                if (numeroColonne != -1) {
                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    lien = "";
                    apresLien = "";
                }
                boolean test = false;
                if (getPourcentage() != null) {
                    for (int element = 0; element < getPourcentage().length; element++) {
                        if (libeEntete[j].equals(getPourcentage()[element])) {
                            test = true;
                            temp += "<td align='center' style=\"font-size:6px\">" + lien + getDataDirecte()[i][j] + " (" + Utilitaire.formaterAr(getPourc()[element][i]) + " %)" + apresLien + " </td>";
                            break;
                        }
                    }
                }
                if (!test) {
                    //if (Utilitaire.isStringNumeric(getDataDirecte()[i][j])) {
                    temp += "<td align='center' style=\"font-size:6px\">" + lien + getDataDirecte()[i][j] + apresLien + " </td>";

                }

            }

            temp += "</tr>";
        }
        temp += "</tbody>";
        temp += "</table>";
        //temp+=" <p> "+tempxml+" </p>";
        setHtml(temp);

    }

    public String getRajoutLienMultiple(bean.ClassMAPTable e, String lien, String lienAffiche) {
        String retour = "";
        
        if (lien != null || lien.compareTo("") != 0) {
            String[] g = Utilitaire.split(lien, "-");
            String[] affiche = Utilitaire.split(lienAffiche, "-");
            Field[] lf = getListeField();
            for (int i = 0; i < g.length; i++) {
                String valeur = e.getValInsert(g[i]);
                for (int k = 0; k < lf.length; k++) {
                    if (lf[k].getName().compareTo(g[i]) == 0) {
                        if (lf[k].getType().getSimpleName().compareToIgnoreCase("Date") == 0) {
                            valeur = Utilitaire.convertDatyFormtoRealDatyFormat(valeur);
                        }
                        break;
                    }
                }
                if (lienAffiche != null) {
                    retour = retour + "&" + affiche[i] + "=" + valeur;
                } else {
                    retour = retour + "&" + g[i] + "=" + valeur;
                }
            }
        }
        return retour;
    }

    public void transformerDataString() throws Exception {
        Object valeurC = null;
        dataDirecte = new String[getData().length][getLibeEntete().length];
        for (int i = 0; i < getData().length; i++) {
            int j = 0;
            for (j = 0; j < getLibeEntete().length; j++) {
                bean.Champ c = ListeColonneTable.getChamp(data[i], getLibeEntete()[j]);
                valeurC = CGenUtil.getValeurFieldByMethod(data[i], getLibeEntete()[j]);
                if (valeurC == null || c == null) {
                    dataDirecte[i][j] = "";
                    continue;
                }
                if (c.getTypeJava().compareToIgnoreCase("double") == 0) {
                    dataDirecte[i][j] = utilitaire.Utilitaire.formaterAr(String.valueOf(valeurC));
                } else if (c.getTypeJava().compareToIgnoreCase("java.sql.Date") == 0) {
                    dataDirecte[i][j] = Utilitaire.formatterDaty((java.sql.Date) (valeurC));
                } else {
                    dataDirecte[i][j] = String.valueOf(valeurC);
                }
            }

            //valeurC = CGenUtil.getValeurFieldByMethod(data[i], "nombrepargroupe");
            //dataDirecte[i][j-1] = String.valueOf(valeurC);
        }

    }

    public String[] getLibeEntete() {
        return libeEntete;
    }

    public void setLibeEntete(String[] libeEntete) {
        this.libeEntete = libeEntete;
    }

    public void setData(bean.ClassMAPTable[] data) {
        this.data = data;
    }

    public bean.ClassMAPTable[] getData() {
        return data;
    }

    public void setCssEntete(String cssEntete) {
        this.cssEntete = cssEntete;
    }

    public String getCssEntete() {
        return cssEntete;
    }

    public void setCssTableau(String cssTableau) {
        this.cssTableau = cssTableau;
    }

    public String getCssTableau() {
        return cssTableau;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getHtml() throws Exception {
        if (html == null || html.compareToIgnoreCase("") == 0) {
            this.makeHtml();
        }
        return html;
    }

    public String getHtmlPDF() throws Exception {
        this.makeHtmlPDF();
        return html;
    }

    public String getHtmlPDF(boolean recap, String v1, String v2) throws Exception {
        this.makeHtml(recap, v1, v2);
        return html;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTitre() {
        return titre;
    }

    public void setPropEntet(int[] propEntet) {
        this.propEntet = propEntet;
    }

    public int[] getPropEntet() {
        if (propEntet == null) {
            int[] ret = new int[getLibeEntete().length];
            for (int i = 0; i < ret.length; i++) {
                ret[i] = 100 / ret.length;
            }
            return ret;
        }
        return propEntet;
    }

    public void setTailleTableau(String tailleTableau) {
        this.tailleTableau = tailleTableau;
    }

    public String getTailleTableau() {
        return tailleTableau;
    }

    public void setLibelleAffiche(String[] libelleAffiche) {
        this.libelleAffiche = libelleAffiche;
    }

    public String[] getLibelleAffiche() {
        if (libelleAffiche == null) {
            return getLibeEntete();
        }
        return libelleAffiche;
    }

    public void setDataDirecte(String[][] dataDirecte) {
        this.dataDirecte = dataDirecte;
    }

    public String[][] getDataDirecte() {
        return dataDirecte;
    }

    public void setLien(String[] lien) {
        this.lien = lien;
    }

    public String[] getLien() {
        return lien;
    }

    public String[] getUrlLien() {
        return urlLien;
    }

    public void setUrlLien(String[] listeLien) {
        urlLien = listeLien;
    }

    public void setColonneLien(String[] colonneLien) {
        this.colonneLien = colonneLien;
    }

    public String[] getColonneLien() {
        return colonneLien;
    }

    public void setModelePage(String modelePage) {
        this.modelePage = modelePage;
    }

    public String getModelePage() {
        return modelePage;
    }

    public void setAttLien(String[] attLien) {
        this.attLien = attLien;
    }

    public String[] getAttLien() {
        return attLien;
    }

    public void setValeurLien(String[] valeurLien) {
        this.valeurLien = valeurLien;
    }

    public String[] getValeurLien() {
        return valeurLien;
    }

    public String getExpxml() {
        return expxml;
    }

    public void setExpxml(String s) {
        expxml = s;
    }

    public String getExpcsv() {
        return expcsv;
    }

    public void setExpcsv(String s) {
        expcsv = s;
    }

    public String getExppdf() {
        return exppdf;
    }

    public void setExppdf(String s) {
        exppdf = s;
    }

    public void makeHtmlWithRadioButton() throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";
        temp += "<p align=center><strong><u>" + getTitre() + "</u></strong></p>";
        temp += "<div id=\"divchck\">";
        temp += "<table width=" + getTailleTableau() + " border=0 align=center cellpadding=3 cellspacing=3 class=" + getCssTableau() + ">";
        temp += "<tr class=" + getCssEntete() + ">";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;
        nombreColonne = getDataDirecte()[0].length;
        temp += "<td align=center valign=top></td>";
        for (int i = 0; i < nombreColonne; i++) {
            temp += "<td width=" + getPropEntet()[i] + "% align=center valign=top>" + getLibelleAffiche()[i] + "</td>";
        }
        temp += "</tr>";
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<tr onmouseover=this.style.backgroundColor='#EAEAEA' onmouseout=\"this.style.backgroundColor=''\">";

            temp += "<td align=center>";

            if (data[i].getValColLibelle() != null && data[i].getValColLibelle().compareToIgnoreCase("") != 0) {
                String temps = data[i].getValColLibelle();
                if (temps != null && temps.contains("'")) {
                    temps = temps.replaceAll("'", "&apos;");
                }
                if (temps != null && temps.contains("\"")) {
                    temps = temps.replaceAll("\"", " ");
                }
                temp += "<input type='radio' value='" + getDataDirecte()[i][0] + ";" + temps + "' name='choix' onMouseDown='getChoix()' id='choix' class='radio'/>";

            } else {
                temp += "<input type='radio' value='" + getDataDirecte()[i][0] + ";" + getDataDirecte()[i][0] + "' name='choix' onMouseDown='getChoix()' id='choix' class='radio'/>";
            }
            temp += "</td>";

            int nombreLien = 0;
            for (int j = 0; j < nombreColonne; j++) {
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                if (numeroColonne != -1) {

                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    lien = "<a href='" + getLien()[numeroColonne] + "&" + colLien + "=" + valL + "'>";
                    apresLien = "</a>";
                }
                String alignement = "left";
                if (getDataDirecte()[i][j].getClass().getName().compareToIgnoreCase("java.lang.String") == 0) {
                    alignement = "left";
                }
                String testsplit = Utilitaire.enleverEspaceDoubleBase(getDataDirecte()[i][j]);
                if (testsplit.contains(",")) {
                    String[] testsplits = testsplit.split(",");
                    if (Utilitaire.isStringNumeric(testsplits[0]) && Utilitaire.isStringNumeric(testsplits[1])) {
                        alignement = "right";
                    }
                }
                if (j == 1) {
                    temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + ">" + getDataDirecte()[i][j] + "</td>";
                } else {
                    temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + ">" + lien + getDataDirecte()[i][j] + apresLien + "</td>";
                }
            }

            temp += "</tr>";
        }
        temp += "</table>";
        temp += "</div>";
        setHtml(temp);
    }

    public void makeHtmlWithMultipleCheckbox() throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";
        temp += "<p align=center><strong><u>" + getTitre() + "</u></strong></p>";
        temp += "<div id=\"divchck\">";
        temp += "<table width=" + getTailleTableau() + " border=0 align=center cellpadding=3 cellspacing=3 class=" + getCssTableau() + ">";
        temp += "<tr class=" + getCssEntete() + ">";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;
        nombreColonne = getDataDirecte()[0].length;
        temp += "<td align=center valign=top></td>";
        for (int i = 0; i < nombreColonne; i++) {
            temp += "<td width=" + getPropEntet()[i] + "% align=center valign=top>" + getLibelleAffiche()[i] + "</td>";
        }
        temp += "</tr>";
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<tr onmouseover=this.style.backgroundColor='#EAEAEA' onmouseout=\"this.style.backgroundColor=''\">";

            //temp += "<td align=center><input type='checkbox' value='" + getDataDirecte()[i][0] + "' name='choix' id='choix' class='checkbox'/></td>";
            temp += "<td align=center>";

            if (data[i].getValColLibelle() != null && data[i].getValColLibelle().compareToIgnoreCase("") != 0) {

                temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + data[i].getValColLibelle() + "' name='choix' id='choix' class='checkbox'/>";

            } else {
                temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + getDataDirecte()[i][0] + "' name='choix' id='choix' class='checkbox'/>";
            }
            temp += "</td>";

            int nombreLien = 0;
            for (int j = 0; j < nombreColonne; j++) {
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                if (numeroColonne != -1) {

                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    lien = "<a href='" + getLien()[numeroColonne] + "&" + colLien + "=" + valL + "'>";
                    apresLien = "</a>";
                }
                String alignement = "left";
                if (getDataDirecte()[i][j].getClass().getName().compareToIgnoreCase("java.lang.String") == 0) {
                    alignement = "left";
                }
                String testsplit = Utilitaire.enleverEspaceDoubleBase(getDataDirecte()[i][j]);
                if (testsplit.contains(",")) {
                    String[] testsplits = testsplit.split(",");
                    if (Utilitaire.isStringNumeric(testsplits[0]) && Utilitaire.isStringNumeric(testsplits[1])) {
                        alignement = "right";
                    }
                }
                if (j == 1) {
                    temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + "><input type='text' readonly=true style='border:none;text-align:center;' name='idEmp" + i + "' value='" + getDataDirecte()[i][j] + "'></td>";
                } else {
                    temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + ">" + lien + getDataDirecte()[i][j] + apresLien + "</td>";
                }
            }

            temp += "</tr>";
        }
        temp += "</table>";
        temp += "<input class=\"btn btn-success\" type=\"submit\" value=\"Valider\" />";
        temp += "</div>";
        setHtml(temp);
    }

    public void makeHtmlWithMultipleCheckboxTous() throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";
        temp += "<p align=center><strong><u>" + getTitre() + "</u></strong></p>";
        temp += "<div id=\"divchck\">";
        temp += "<table width=" + getTailleTableau() + " border=0 align=center cellpadding=3 cellspacing=3 class=" + getCssTableau() + ">";
        temp += "<tr class=" + getCssEntete() + ">";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;
        nombreColonne = getDataDirecte()[0].length;
        temp += "<td align=center valign=top></td>";
        for (int i = 0; i < nombreColonne; i++) {
            temp += "<td width=" + getPropEntet()[i] + "% align=center valign=top>" + getLibelleAffiche()[i] + "</td>";
        }
        temp += "</tr>";
        temp += "<th align=center valign=top style='background-color:#bed1dd'><input onclick=\"CocheToutCheckbox(this, 'choix')\" type=\"checkbox\"></th>";
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<tr onmouseover=this.style.backgroundColor='#EAEAEA' onmouseout=\"this.style.backgroundColor=''\">";

            //temp += "<td align=center><input type='checkbox' value='" + getDataDirecte()[i][0] + "' name='choix' id='choix' class='checkbox'/></td>";
            temp += "<td align=center>";

            if (data[i].getValColLibelle() != null && data[i].getValColLibelle().compareToIgnoreCase("") != 0) {

                temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + data[i].getValColLibelle() + "' name='choix' id='choix' class='checkbox'/>";

            } else {
                temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + getDataDirecte()[i][0] + "' name='choix' id='choix' class='checkbox'/>";
            }
            temp += "</td>";

            int nombreLien = 0;
            for (int j = 0; j < nombreColonne; j++) {
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                if (numeroColonne != -1) {

                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    lien = "<a href='" + getLien()[numeroColonne] + "&" + colLien + "=" + valL + "'>";
                    apresLien = "</a>";
                }
                String alignement = "left";
                if (getDataDirecte()[i][j].getClass().getName().compareToIgnoreCase("java.lang.String") == 0) {
                    alignement = "left";
                }
                String testsplit = Utilitaire.enleverEspaceDoubleBase(getDataDirecte()[i][j]);
                if (testsplit.contains(",")) {
                    String[] testsplits = testsplit.split(",");
                    if (Utilitaire.isStringNumeric(testsplits[0]) && Utilitaire.isStringNumeric(testsplits[1])) {
                        alignement = "right";
                    }
                }
                if (j == 1) {
                    temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + "><input type='text' readonly=true style='border:none;text-align:center;' name='idEmp" + i + "' value='" + getDataDirecte()[i][j] + "'></td>";
                } else {
                    temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + ">" + lien + getDataDirecte()[i][j] + apresLien + "</td>";
                }
            }

            temp += "</tr>";
        }
        temp += "</table>";
        temp += "<input class=\"btn btn-success\" type=\"submit\" value=\"Valider\" />";
        temp += "</div>";
        setHtml(temp);
    }

    public void makeHtmlWithMultipleCheckboxRetientChoix(String[] choix) throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";
        temp += "<p align=center><strong><u>" + getTitre() + "</u></strong></p>";
        temp += "<div id=\"divchck\">";
        temp += "<table width=" + getTailleTableau() + " border=0 align=center cellpadding=3 cellspacing=3 class=" + getCssTableau() + ">";
        temp += "<tr class=" + getCssEntete() + ">";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;
        nombreColonne = getDataDirecte()[0].length;
        temp += "<td align=center valign=top></td>";
        for (int i = 0; i < nombreColonne; i++) {
            temp += "<td width=" + getPropEntet()[i] + "% align=center valign=top>" + getLibelleAffiche()[i] + "</td>";
        }
        temp += "</tr>";

        for (int i = 0; i < nombreLigne; i++) {
            temp += "<tr onmouseover=this.style.backgroundColor='#EAEAEA' onmouseout=\"this.style.backgroundColor=''\">";

            //temp += "<td align=center><input type='checkbox' value='" + getDataDirecte()[i][0] + "' name='choix' id='choix' class='checkbox'/></td>";
            temp += "<td align=center>";

            int dansChoix = Utilitaire.estIlDedans(getDataDirecte()[i][0], choix);
            if (data[i].getValColLibelle() != null && data[i].getValColLibelle().compareToIgnoreCase("") != 0) {
                if (dansChoix != -1) {
                    temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + data[i].getValColLibelle() + "' name='choix' id='choix' class='checkbox' checked/>";
                } else {
                    temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + data[i].getValColLibelle() + "' name='choix' id='choix' class='checkbox'/>";
                }
            } else {
                if (dansChoix != -1) {
                    temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + getDataDirecte()[i][0] + "' name='choix' id='choix' class='checkbox' checked/>";
                } else {
                    temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + getDataDirecte()[i][0] + "' name='choix' id='choix' class='checkbox'/>";
                }
            }
            temp += "</td>";

            int nombreLien = 0;
            for (int j = 0; j < nombreColonne; j++) {
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                if (numeroColonne != -1) {

                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    lien = "<a href='" + getLien()[numeroColonne] + "&" + colLien + "=" + valL + "'>";
                    apresLien = "</a>";
                }
                String alignement = "left";
                if (getDataDirecte()[i][j].getClass().getName().compareToIgnoreCase("java.lang.String") == 0) {
                    alignement = "left";
                }
                String testsplit = Utilitaire.enleverEspaceDoubleBase(getDataDirecte()[i][j]);
                if (testsplit.contains(",")) {
                    String[] testsplits = testsplit.split(",");
                    if (Utilitaire.isStringNumeric(testsplits[0]) && Utilitaire.isStringNumeric(testsplits[1])) {
                        alignement = "right";
                    }
                }
                if (j == 1) {
                    temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + "><input type='text' readonly=true style='border:none;text-align:center;' name='idEmp" + i + "' value='" + getDataDirecte()[i][j] + "'></td>";
                } else {
                    temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + ">" + lien + getDataDirecte()[i][j] + apresLien + "</td>";
                }
            }

            temp += "</tr>";
        }
        temp += "</table>";
        temp += "<input class=\"btn btn-success\" type=\"submit\" value=\"Valider\" />";
        temp += "</div>";
        setHtml(temp);
    }

    public void makeHtmlWithMultipleCheckboxRetientChoixTous(String[] choix) throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";
        temp += "<p align=center><strong><u>" + getTitre() + "</u></strong></p>";
        temp += "<div id=\"divchck\">";
        temp += "<table width=" + getTailleTableau() + " border=0 align=center cellpadding=3 cellspacing=3 class=" + getCssTableau() + ">";
        temp += "<tr class=" + getCssEntete() + ">";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;
        nombreColonne = getDataDirecte()[0].length;
        temp += "<td align=center valign=top></td>";
        for (int i = 0; i < nombreColonne; i++) {
            temp += "<td width=" + getPropEntet()[i] + "% align=center valign=top>" + getLibelleAffiche()[i] + "</td>";
        }
        temp += "</tr>";
        temp += "<th align=center valign=top style='background-color:#bed1dd'><input onclick=\"CocheToutCheckbox(this, 'id')\" type=\"checkbox\"></th>";
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<tr onmouseover=this.style.backgroundColor='#EAEAEA' onmouseout=\"this.style.backgroundColor=''\">";

            //temp += "<td align=center><input type='checkbox' value='" + getDataDirecte()[i][0] + "' name='choix' id='choix' class='checkbox'/></td>";
            temp += "<td align=center>";

            int dansChoix = Utilitaire.estIlDedans(getDataDirecte()[i][0], choix);
            if (data[i].getValColLibelle() != null && data[i].getValColLibelle().compareToIgnoreCase("") != 0) {
                if (dansChoix != -1) {
                    temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + data[i].getValColLibelle() + "' name='choix' id='choix' class='checkbox' checked/>";
                } else {
                    temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + data[i].getValColLibelle() + "' name='choix' id='choix' class='checkbox'/>";
                }
            } else {
                if (dansChoix != -1) {
                    temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + getDataDirecte()[i][0] + "' name='choix' id='choix' class='checkbox' checked/>";
                } else {
                    temp += "<input type='checkbox' value='" + getDataDirecte()[i][0] + ";" + getDataDirecte()[i][0] + "' name='choix' id='choix' class='checkbox'/>";
                }
            }
            temp += "</td>";

            int nombreLien = 0;
            for (int j = 0; j < nombreColonne; j++) {
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                if (numeroColonne != -1) {

                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    lien = "<a href='" + getLien()[numeroColonne] + "&" + colLien + "=" + valL + "'>";
                    apresLien = "</a>";
                }
                String alignement = "left";
                if (getDataDirecte()[i][j].getClass().getName().compareToIgnoreCase("java.lang.String") == 0) {
                    alignement = "left";
                }
                String testsplit = Utilitaire.enleverEspaceDoubleBase(getDataDirecte()[i][j]);
                if (testsplit.contains(",")) {
                    String[] testsplits = testsplit.split(",");
                    if (Utilitaire.isStringNumeric(testsplits[0]) && Utilitaire.isStringNumeric(testsplits[1])) {
                        alignement = "right";
                    }
                }
                if (j == 1) {
                    temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + "><input type='text' readonly=true style='border:none;text-align:center;' name='idEmp" + i + "' value='" + getDataDirecte()[i][j] + "'></td>";
                } else {
                    temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + ">" + lien + getDataDirecte()[i][j] + apresLien + "</td>";
                }
            }

            temp += "</tr>";
        }
        temp += "</table>";
        temp += "<input class=\"btn btn-success\" type=\"submit\" value=\"Valider\" />";
        temp += "</div>";
        setHtml(temp);
    }

    public String getHtmlWithRadioButton() throws Exception {
        this.makeHtmlWithRadioButton();
        return html;
    }

    public String getHtmlWithMultipleCheckbox() throws Exception {
        this.makeHtmlWithMultipleCheckbox();
        return html;
    }

    public String getHtmlWithMultipleCheckboxRetientChoix(String choix) throws Exception {
        System.out.println("-----------     choix        ---------------    " + choix);
        if (choix != null && choix.compareTo("") != 0) {
            String[] listeChoix = choix.split(";");
            this.makeHtmlWithMultipleCheckboxRetientChoix(listeChoix);
        } else {
            System.out.println("-----------     Dans else        ---------------    " + choix);
            this.makeHtmlWithMultipleCheckbox();
        }
        return html;
    }

    public String getHtmlWithMultipleCheckboxRetientChoixTous(String choix) throws Exception {
        System.out.println("-----------     choix        ---------------    " + choix);
        if (choix != null && choix.compareTo("") != 0) {
            String[] listeChoix = choix.split(";");
            this.makeHtmlWithMultipleCheckboxRetientChoixTous(listeChoix);
        } else {
            System.out.println("-----------     Dans else        ---------------    " + choix);
            this.makeHtmlWithMultipleCheckboxTous();
        }
        return html;
    }

    public void setPourc(double[][] pourc) {
        this.pourc = pourc;
    }

    public double[][] getPourc() {
        return pourc;
    }

    public void setPourcentage(String[] pourcentage) {
        this.pourcentage = pourcentage;
    }

    public String[] getPourcentage() {
        //System.out.print(pourcentage.length);
        return pourcentage;
    }

    public void makeHtmlWithCheckbox() throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";
        String tempcsv = "";
        String tempxml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \r\n <tableau>";
        temp += "<div class='row'>";
        temp += "<div class='row col-md-12'>";
        temp += "<div class='box box-warning'>";
        temp += "<div class='box-header'>";
        temp += "<h3 class='box-title' align=center>" + getTitre() + "</h3>";
        temp += "</div>";
        temp += "<div class='box-body table-responsive no-padding'>";
        temp += "<div id=\"selectnonee\">";
        temp += "<table width=" + getTailleTableau() + " border=0 align=center cellpadding=3 cellspacing=3 class='table table-hover'>";
        temp += "<thead>";
        temp += "<tr class=" + getCssEntete() + ">";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;
        nombreColonne = getDataDirecte()[0].length;
        temp += "<th align=center valign=top style='background-color:#bed1dd'><input onclick=\"CocheToutCheckbox(this, 'id')\" type=\"checkbox\"></th>";
        for (int i = 0; i < nombreColonne; i++) {
//            temp += "<th width=" + getPropEntet()[i] + "% align=center valign=top style='background-color:#bed1dd'>" + getLibelleAffiche()[i] + "</th>";
            if (getCritereLienString().compareToIgnoreCase("") != 0) {
                temp += "<th width=" + getPropEntet()[i] + "% align=center valign=top style='background-color:#bed1dd'>" + getCritereLienString() + "&triCol=yes&newcol=" + getLibeEntete()[i] + ">" + getLibelleAffiche()[i] + "</a></th>";
            } else {
                temp += "<th width=" + getPropEntet()[i] + "% align=center valign=top style='background-color:#bed1dd'>" + getLibelleAffiche()[i] + "</th>";
            }
            tempcsv += getLibelleAffiche()[i];
            if (i != nombreColonne - 1) {
                tempcsv += ";";
            }
        }
        //temp += "<th align=center valign=top style='background-color:#bed1dd'>Nombre</th>";
        temp += "</tr>";
        tempcsv += "\r\n";
        tempxml += "\r\n";
        temp += "</thead>";
        temp += "<tbody>";
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<tr onmouseover=this.style.backgroundColor='#EAEAEA' onmouseout=\"this.style.backgroundColor=''\">";
            tempxml += "<row>\r\n";
            int nombreLien = 0, j = 0, l = 0;
            temp += "<td align=center><input type='checkbox' value='" + getDataDirecte()[i][0] + "' name='id' id='checkbox" + i + "'/></td>";
            for (j = 0; j < nombreColonne; j++) {
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                
                if (numeroColonne != -1) {
                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    String rajoutLien = "";
                    if (getUrlLien() != null) {
                        rajoutLien = getRajoutLienMultiple(getData()[i], getUrlLien()[l], getUrlLienAffiche()[l]);
                    }
                    l++;
                    
                    lien = "<a href='" + getLien()[numeroColonne] + "&" + colLien + "=" + valL + rajoutLien + "'>";
                    apresLien = "</a>";
                }

                //if (Utilitaire.isStringNumeric(getDataDirecte()[i][j])) {
                String alignement = "left";
                if (getDataDirecte()[i][j].getClass().getName().compareToIgnoreCase("java.lang.String") == 0) {
                    alignement = "left";
                }
                String testsplit = Utilitaire.enleverEspaceDoubleBase(getDataDirecte()[i][j]);
                if (testsplit.contains(",")) {
                    String[] testsplits = testsplit.split(",");
                    if (Utilitaire.isStringNumeric(testsplits[0]) && Utilitaire.isStringNumeric(testsplits[1])) {
                        alignement = "right";
                    }
                }
                temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + " >" + lien + getDataDirecte()[i][j] + apresLien + " </td>";

                //temp += "<td width=" + getPropEntet()[j] + "%>" + lien + getDataDirecte()[i][j] + apresLien + "</td>";
                tempcsv += Utilitaire.verifNumerique(getDataDirecte()[i][j]);
                tempxml += "<" + getLibelleAffiche()[j] + ">" + Utilitaire.verifNumerique(getDataDirecte()[i][j]) + "</" + getLibelleAffiche()[j] + ">\r\n";
                if (j != nombreColonne - 1) {
                    tempcsv += ";";
                }
            }
            temp += "</tr>";
            tempcsv += "\r\n";
            tempxml += "</row>\r\n";
        }
        temp += "<input type=\"hidden\" name=\"ids\" value=\"" + getIds() + "\">";
        temp += "</tbody>";
        tempxml += "</tableau>\r\n";
        temp += "</table>";
        temp += "</div>";
        temp += "</div>";
        temp += "<div class='box-footer'>";
        if (isAfficheBouttondevalider()) {
            temp += "<input id='acte' type='hidden' name='acte' value='update'/> ";
            temp += "<button type='button' name='Submit2' class='btn btn-success pull-left' style='margin-left: 25px;' onClick=\"document.getElementById('acte').value='detacher'; document.getElementById('formmultiple').submit();\">D&eacute;tacher</button> ";
            temp += "<button type='button' name='Submit2' class='btn btn-success pull-left' style='margin-left: 25px;' onClick=\"document.getElementById('acte').value='valider'; document.getElementById('formmultiple').submit();\">Viser</button> ";
        } else {
            if (this.getNameBoutton2().compareTo("") != 0) {
                temp += "<button type='submit' name='Submit2' class='btn btn-danger pull-right' style='margin-right: 25px;' onClick=\"acte.value='" + getNameActe2() + "'\">" + getNameBoutton2() + "</button> ";
            }
            temp += "<button type='submit' name='Submit2' class='btn btn-success pull-right' style='margin-right: 25px;' onClick=\"acte.value='" + getNameActe() + "'\">" + getNameBoutton() + "</button> ";

        }
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "<div class='col-xs-12'>";

        temp += "</div>";
        //temp+=" <p> "+tempxml+" </p>";
        setHtml(temp);
        setExpcsv(tempcsv);
        setExpxml(tempxml);

    }

    public void makeHtmlCorrection() throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";
        String tempcsv = "";
        String tempxml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \r\n <tableau>";
        temp += "<div class='row'>";
        temp += "<div class='row col-md-12'>";
        temp += "<div class='box box-warning'>";
        temp += "<div class='box-header'>";
        temp += "<h3 class='box-title' align=center>" + getTitre() + "</h3>";
        temp += "</div>";
        temp += "<div class='box-body table-responsive no-padding'>";
        temp += "<div id=\"selectnonee\">";
        temp += "<table width=" + getTailleTableau() + " border=0 align=center cellpadding=3 cellspacing=3 class='table table-hover'>";
        temp += "<thead>";
        temp += "<tr class=" + getCssEntete() + ">";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;
        nombreColonne = getDataDirecte()[0].length;
        temp += "<th align=center valign=top style='background-color:#bed1dd'></th>";
        for (int i = 0; i < nombreColonne; i++) {
//            temp += "<th width=" + getPropEntet()[i] + "% align=center valign=top style='background-color:#bed1dd'>" + getLibelleAffiche()[i] + "</th>";
            if (getCritereLienString().compareToIgnoreCase("") != 0) {
                temp += "<th width=" + getPropEntet()[i] + "% align=center valign=top style='background-color:#bed1dd'>" + getCritereLienString() + "&triCol=yes&newcol=" + getLibeEntete()[i] + ">" + getLibelleAffiche()[i] + "</a></th>";
            } else {
                temp += "<th width=" + getPropEntet()[i] + "% align=center valign=top style='background-color:#bed1dd'>" + getLibelleAffiche()[i] + "</th>";
            }
            tempcsv += getLibelleAffiche()[i];
            if (i != nombreColonne - 1) {
                tempcsv += ";";
            }
        }
        //temp += "<th align=center valign=top style='background-color:#bed1dd'>Nombre</th>";
        temp += "</tr>";
        tempcsv += "\r\n";
        tempxml += "\r\n";
        temp += "</thead>";
        temp += "<tbody>";
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<tr onmouseover=this.style.backgroundColor='#EAEAEA' onmouseout=\"this.style.backgroundColor=''\">";
            tempxml += "<row>\r\n";
            int nombreLien = 0, j = 0;
            temp += "<td align=center><input type='checkbox' value='" + getDataDirecte()[i][0] + "' name='id' id='checkbox" + i + "'/></td>";
            for (j = 0; j < nombreColonne; j++) {
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                if (numeroColonne != -1) {
                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    lien = "<a href='" + getLien()[numeroColonne] + "&" + colLien + "=" + valL + "'>";
                    apresLien = "</a>";
                }

                //if (Utilitaire.isStringNumeric(getDataDirecte()[i][j])) {
                String alignement = "left";
                if (getDataDirecte()[i][j].getClass().getName().compareToIgnoreCase("java.lang.String") == 0) {
                    alignement = "left";
                }
                String testsplit = Utilitaire.enleverEspaceDoubleBase(getDataDirecte()[i][j]);
                if (testsplit.contains(",")) {
                    String[] testsplits = testsplit.split(",");
                    if (Utilitaire.isStringNumeric(testsplits[0]) && Utilitaire.isStringNumeric(testsplits[1])) {
                        alignement = "right";
                    }
                }
                temp += "<td width=" + getPropEntet()[j] + "% align=" + alignement + " >" + lien + getDataDirecte()[i][j] + apresLien + " </td>";

                //temp += "<td width=" + getPropEntet()[j] + "%>" + lien + getDataDirecte()[i][j] + apresLien + "</td>";
                tempcsv += Utilitaire.verifNumerique(getDataDirecte()[i][j]);
                tempxml += "<" + getLibelleAffiche()[j] + ">" + Utilitaire.verifNumerique(getDataDirecte()[i][j]) + "</" + getLibelleAffiche()[j] + ">\r\n";
                if (j != nombreColonne - 1) {
                    tempcsv += ";";
                }
            }
            temp += "</tr>";
            tempcsv += "\r\n";
            tempxml += "</row>\r\n";
        }
        temp += "<input type=\"hidden\" name=\"ids\" value=\"" + getIds() + "\">";
        temp += "</tbody>";
        tempxml += "</tableau>\r\n";
        temp += "</table>";
        temp += "</div>";
        temp += "</div>";
        temp += "<div class='box-footer'>";
        temp += "<input type='hidden' name='acte' value='update'/> ";
        temp += "<button type='submit' name='Submit2' class='btn btn-success pull-right' style='margin-left: 25px;' onClick=\"acte.value='corriger'\">Corriger</button> ";

        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "<div class='col-xs-12'>";

        temp += "</div>";
        setHtml(temp);
        setExpcsv(tempcsv);
        setExpxml(tempxml);

    }

    public String getHtmlWithCheckbox() throws Exception {
        if (html == null || html.compareToIgnoreCase("") == 0) {
            this.makeHtmlWithCheckbox();
        }
        return html;
    }

    public String getHtmlCorrection() throws Exception {
        if (html == null || html.compareToIgnoreCase("") == 0) {
            this.makeHtmlCorrection();
        }
        return html;
    }

    public String getHtmlWithCheckboxSelected() throws Exception {
        this.setAfficheBouttondevalider(true);
        if (html == null || html.compareToIgnoreCase("") == 0) {
            this.makeHtmlWithCheckbox();
        }
        return html;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getIds() {
        return ids;
    }

    private String getNameBoutton() {
        return nameBoutton;
    }

    private String getNameActe() {
        return nameActe;
    }

    public Map getLibelleEnteteAAffiche() {
        return libelleEnteteAAffiche;
    }

    public void setLibelleEnteteAAffiche(Map libelleEnteteAAffiche) {
        this.libelleEnteteAAffiche = libelleEnteteAAffiche;
    }
    
//    public String champUrl(){
//        String retour = "";
//        for(int i=0;i<this.getListeField().length;i++){
//            retour+=this.getListeField()[i].getName()+"="+this.getListeField()[i].get();
//        }
//    }
    
     public String getHtmlWithCheckboxUpdateMultiple() throws Exception {
        if (html == null || html.compareToIgnoreCase("") == 0) {
            this.makeHtmlWithCheckboxUpdateMultiple();
        }
        return html;
    }
    
    public void makeHtmlWithCheckboxUpdateMultiple() throws Exception {
        if (getDataDirecte() == null || getDataDirecte().length == 0) {
            return;
        }
        String temp = "";
        String tempcsv = "";
        String tempxml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \r\n <tableau>";
        temp += "<div class='row'>";
        temp += "<div class='row col-md-12'>";
        temp += "<div class='box box-warning'>";
        temp += "<div class='box-header'>";
        temp += "<h3 class='box-title' align=center>" + getTitre() + "</h3>";
        temp += "</div>";
        temp += "<div class='box-body table-responsive no-padding'>";
        temp += "<div id=\"selectnonee\">";
        temp += "<table width=" + getTailleTableau() + " border=0 align=center cellpadding=3 cellspacing=3 class='table table-hover'>";
        temp += "<thead>";
        temp += "<tr class=" + getCssEntete() + ">";
        int nombreLigne = 0, nombreColonne = 0;
        nombreLigne = getDataDirecte().length;
        nombreColonne = getDataDirecte()[0].length;
        temp += "<th align=center valign=top style='background-color:#bed1dd'><input onclick=\"CocheToutCheckbox(this, 'id')\" type=\"checkbox\"></th>";
        for (int i = 0; i < nombreColonne; i++) {
            if (getCritereLienString().compareToIgnoreCase("") != 0) {
                temp += "<th width=" + getPropEntet()[i] + "% align=center valign=top style='background-color:#bed1dd'>" + getCritereLienString() + "&triCol=yes&newcol=" + getLibeEntete()[i] + ">" + getLibelleAffiche()[i] + "</a></th>";
            } else {
                temp += "<th width=" + getPropEntet()[i] + "% align=center valign=top style='background-color:#bed1dd'>" + getLibelleAffiche()[i] + "</th>";
            }
            tempcsv += getLibelleAffiche()[i];
            if (i != nombreColonne - 1) {
                tempcsv += ";";
            }
        }
        temp += "</tr>";
        tempcsv += "\r\n";
        tempxml += "\r\n";
        temp += "</thead>";
        temp += "<tbody>";
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<tr onmouseover=this.style.backgroundColor='#EAEAEA' onmouseout=\"this.style.backgroundColor=''\">";
            tempxml += "<row>\r\n";
            int nombreLien = 0, j = 0, l=0;
            temp += "<td align=center><input type='checkbox' value='" + i + "' name='id' id='checkbox" + i + "'/></td>";
            
            for (j = 0; j < nombreColonne; j++) {
                String lien = "";
                String apresLien = "";
                int numeroColonne = Utilitaire.estIlDedans(getLibeEntete()[j], getColonneLien());
                if (numeroColonne != -1) {
                    String colLien = getLibeEntete()[j];
                    String valL = getDataDirecte()[i][j];
                    if (getAttLien() != null) {
                        colLien = getAttLien()[nombreLien];
                    }
                    if (getValeurLien() != null) {
                        Object valeurC = CGenUtil.getValeurFieldByMethod(data[i], getValeurLien()[nombreLien]);
                        valL = String.valueOf(valeurC);
                    }
                    nombreLien++;
                    String rajoutLien = "";
                    if (getUrlLien() != null) {
                        rajoutLien = getRajoutLienMultiple(getData()[i], getUrlLien()[l], getUrlLienAffiche()[l]);
                    }
                    l++;
                    lien = "<a href='" + getLien()[numeroColonne] + "&" + colLien + "=" + valL + rajoutLien +"'>";
                    apresLien = "</a>";
                }

                //if (Utilitaire.isStringNumeric(getDataDirecte()[i][j])) {
                String alignement="left";
                if(getDataDirecte()[i][j].getClass().getName().compareToIgnoreCase("java.lang.String")==0) alignement = "left";
                String testsplit = Utilitaire.enleverEspaceDoubleBase(getDataDirecte()[i][j]);
                if(testsplit.contains(",")){
                    String[] testsplits = testsplit.split(",");
                    if(Utilitaire.isStringNumeric(testsplits[0]) && Utilitaire.isStringNumeric(testsplits[1])) alignement ="right";
                }
                String val = lien + getDataDirecte()[i][j] + apresLien;
                //temp += "<td>"+val+"</td>";
                if(!getIds().equals(getLibeEntete()[j])){
                    temp += getFormu().makeHtmlUpdateTableau(getLibeEntete()[j], val, i);
                }else{
                    temp += "<td width=" + getPropEntet()[j] + "% align="+alignement+" ><input type='hidden' id='"+getLibeEntete()[j]+"_"+i+"' name='"+getLibeEntete()[j]+"_"+i+"' value='"+lien + getDataDirecte()[i][j] + apresLien+"'>"+lien + getDataDirecte()[i][j] + apresLien+"</td>";   
                }
                //temp += "<td width=" + getPropEntet()[j] + "% align="+alignement+" ><input type='textbox' id='"+getLibeEntete()[j]+"_"+i+"' name='"+getLibeEntete()[j]+"_"+i+"' value='"+lien + getDataDirecte()[i][j] + apresLien+"'></td>";
                
                tempcsv += Utilitaire.verifNumerique(getDataDirecte()[i][j]);
                tempxml += "<" + getLibelleAffiche()[j] + ">" + Utilitaire.verifNumerique(getDataDirecte()[i][j]) + "</" + getLibelleAffiche()[j] + ">\r\n";
                if (j != nombreColonne - 1) {
                    tempcsv += ";";
                }
            }
            temp += "</tr>";
            tempcsv += "\r\n";
            tempxml += "</row>\r\n";
        }
        temp += "<input type=\"hidden\" name=\"ids\" value=\"" + getIds() + "\">";
        temp += "</tbody>";
        tempxml += "</tableau>\r\n";
        temp += "</table>";
        temp += "</div>";
        temp += "</div>";
        temp += "<div class='box-footer'>";
        temp += "<div class='col-xs-12'>";
        temp += "<button type='submit' name='Submit2' class='btn btn-success pull-right' style='margin-right: 25px;'>Enregistrer</button> ";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "<div class='col-xs-12'>";

        temp += "</div>";
        //temp+=" <p> "+tempxml+" </p>";
        setHtml(temp);
        setExpcsv(tempcsv);
        setExpxml(tempxml);

    }

}
