package affichage;

import javax.servlet.http.HttpServletRequest;
import bean.ListeColonneTable;
import java.util.Vector;
import java.lang.reflect.Field;
import java.sql.Connection;
import utilitaire.Utilitaire;
import bean.ClassMAPTable;
import bean.ResultatEtSomme;

/**
 * <p>
 * Title: Gestion des recettes </p>
 * <p>
 * Description: </p>
 * <p>
 * Copyright: Copyright (c) 2005</p>
 * <p>
 * Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */
public class PageRecherche extends Page {

    private TableauRecherche listeTabDet;
    private bean.ClassMAPTable[] liste;
    private bean.ClassMAPTable critere;
    private String[] colInt;
    private String[] valInt;
    private bean.ResultatEtSomme rs;
    private int numPage = 1;
    private String[] colSomme;
    private String apres;
    private String aWhere = "";
    private String ordre = "";
    private String apresLienPage = "";
    private int npp = 0;
    private String[] tableauAffiche;
    private String[] tableauAfficheDefaut;
    private int nbTableauAffiche;
    private boolean premier = false;

    public void makeTableauAffiche() throws Exception {
        String temp[] = new String[getNbTableauAffiche()];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = getReq().getParameter("colAffiche" + (i + 1));
        }
        setTableauAffiche(Utilitaire.formerTableauGroupe(temp));
    }

    public void preparerDataList(Connection c) throws Exception {
        critere.setNomTable(getBase().getNomTable());
        makeCritere();
        if (premier == true) {
            rs = new bean.ResultatEtSomme();
            rs.initialise(getColSomme());
        } else {
            rs = getUtilisateur().getDataPage(critere, getColInt(), getValInt(), getNumPage(), getAWhere(), getColSomme(), c, npp);
        }
        setListe((bean.ClassMAPTable[]) rs.getResultat());
    }

    public String getOrdre() {
        return ordre;
    }

    public void setOrdre(String ordre) {
        this.ordre = ordre;
    }

    public void preparerDataFormu() throws Exception {
        Connection c = null;
        try {
            c = new utilitaire.UtilDB().GetConn();
            formu.getAllData(c);
        } catch (Exception ex) {
            throw (ex);
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void setPremier(boolean premier) {
        this.premier = premier;
    }

    public boolean getPremier(){
        return premier;
    }
    public boolean isPremier() {
        return premier;
    }

    public void preparerData() throws Exception {
        Connection c = null;
        try {
            c = new utilitaire.UtilDB().GetConn();
            preparerDataList(c);
            formu.getAllData(c);
            getReq().getSession().setAttribute("critere", getCritere());
        } catch (Exception ex) {
            throw (ex);
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }
    public void preparerData(Formulaire autre) throws Exception {
        Connection c = null;
        try {
            c = new utilitaire.UtilDB().GetConn();
            preparerDataList(c);
            formu.getAllData(c);
            autre.getAllData(c);
            getReq().getSession().setAttribute("critere", getCritere());
        } catch (Exception ex) {
            throw (ex);
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }
    public void makeCritere() throws Exception {
        Vector colIntv = new Vector();
        Vector valIntv = new Vector();
        Champ[] tempChamp = formu.getCrtFormu();

        String temp = "";
        for (int i = 0; i < tempChamp.length; i++) {
            Field f = bean.CGenUtil.getField(getBase(), tempChamp[i].getNom());
            //System.out.println("getParamSansnUll "+getParamSansNull(tempChamp[i].getNom()) + "tempChamp === "+tempChamp[i].getValeur());
            if (tempChamp[i].getEstIntervalle() == 1) {
                colIntv.add(tempChamp[i].getNom());
                if((getParamSansNull(tempChamp[i].getNom() + "1")==null||getParamSansNull(tempChamp[i].getNom() + "1").compareToIgnoreCase("")==0)&&this.getFormu().getChamp((tempChamp[i].getNom() + "1"))!=null&&this.getFormu().getChamp((tempChamp[i].getNom() + "1")).getDefaut()!=null&&this.getFormu().getChamp((tempChamp[i].getNom() + "1")).getDefaut().compareToIgnoreCase("")!=0)
                {
                    valIntv.add(this.getFormu().getChamp((tempChamp[i].getNom() + "1")).getDefaut());
                }
                else valIntv.add(getParamSansNull(tempChamp[i].getNom() + "1"));
                if((getParamSansNull(tempChamp[i].getNom() + "2")==null||getParamSansNull(tempChamp[i].getNom() + "2").compareToIgnoreCase("")==0)&&this.getFormu().getChamp((tempChamp[i].getNom() + "2"))!=null&&this.getFormu().getChamp((tempChamp[i].getNom() + "2")).getDefaut()!=null&&this.getFormu().getChamp((tempChamp[i].getNom() + "2")).getDefaut().compareToIgnoreCase("")!=0)
                {
                    valIntv.add(this.getFormu().getChamp((tempChamp[i].getNom() + "2")).getDefaut());
                }
                else valIntv.add(getParamSansNull(tempChamp[i].getNom() + "2"));
            } else if (f.getType().getName().compareToIgnoreCase("java.lang.String") == 0) {
                if((getParamSansNull(tempChamp[i].getNom())==null||getParamSansNull(tempChamp[i].getNom()).compareToIgnoreCase("")==0)&&this.getFormu().getChamp((tempChamp[i].getNom()))!=null&&this.getFormu().getChamp((tempChamp[i].getNom())).getDefaut()!=null&&this.getFormu().getChamp((tempChamp[i].getNom())).getDefaut().compareToIgnoreCase("")!=0)
                {
                    bean.CGenUtil.setValChamp(getCritere(), f, this.getFormu().getChamp(tempChamp[i].getNom()).getValeur());
                }
                else
                {
                    String valeurChamp = getReq().getParameter(utilitaire.Utilitaire.remplacerUnderscore(tempChamp[i].getNom()));
                    if (valeurChamp != null && valeurChamp.compareTo("") != 0) {
                        // System.out.println(" getCritere() = " + getCritere() + " f.getName() " + f.getName() + " getParamSansNull(tempChamp[i].getNom()) " + getParamSansNull(tempChamp[i].getNom()));

                        bean.CGenUtil.setValChamp(getCritere(), f, getParamSansNull(tempChamp[i].getNom()));
                    }
                }
            } 
            else {
                if((getParamSansNull(tempChamp[i].getNom())==null||getParamSansNull(tempChamp[i].getNom()).compareToIgnoreCase("")==0)&&this.getFormu().getChamp((tempChamp[i].getNom()))!=null&&this.getFormu().getChamp((tempChamp[i].getNom())).getDefaut()!=null&&this.getFormu().getChamp((tempChamp[i].getNom())).getDefaut().compareToIgnoreCase("")!=0)
                    aWhere += " and " + f.getName() + " like '" + Utilitaire.getValeurNonNull(this.getFormu().getChamp(tempChamp[i].getNom()).getValeur()) + "'";
                else    
                    aWhere += " and " + f.getName() + " like '" + Utilitaire.getValeurNonNull(getParamSansNull(tempChamp[i].getNom())) + "'";
            }
        }
        colInt = new String[colIntv.size()];
        colIntv.copyInto(colInt);
        valInt = new String[valIntv.size()];
        valIntv.copyInto(valInt);
        if (getReq().getParameter("triCol") != null) {
            formu.getChamp("colonne").setValeur(getReq().getParameter("newcol"));
            if (getReq().getParameter("ordre").compareToIgnoreCase("") == 0) {
                formu.getChamp("ordre").setValeur("asc");
                this.setOrdre(" order by " + getReq().getParameter("newcol") + " asc ");
            } else if (getReq().getParameter("ordre").compareToIgnoreCase("desc") == 0) {
                formu.getChamp("ordre").setValeur("asc");
                this.setOrdre(" order by " + getReq().getParameter("newcol") + " asc ");
            } else {
                formu.getChamp("ordre").setValeur("desc");
                this.setOrdre(" order by " + getReq().getParameter("newcol") + " desc ");
            }
        }
        if (formu.getChamp("colonne").getValeur() != null && formu.getChamp("ordre").getValeur().compareToIgnoreCase("-") != 0 && formu.getChamp("colonne").getValeur().compareToIgnoreCase("") != 0 ) {
            this.setOrdre(" order by " + formu.getChamp("colonne").getValeur() + " " + formu.getChamp("ordre").getValeur());
        }
        aWhere += this.getOrdre();
    }
    /*
     public void creerObjetPage(String libEntete[]) throws Exception
     {
     getValeurFormulaire();
     preparerData();
     makeTableauRecap();
     setTableau(new TableauRecherche(getListe(),libEntete));
     }*/

    public void creerObjetPage(String libEntete[]) throws Exception {
        getValeurFormulaire();
        preparerData();
        makeTableauRecap();
        String critereLienTab = "<a href=" + getLien() + "?but=" + getApres() + "&numPag=1" + getApresLienPage() + formu.getListeCritereString() + "";

        ClassMAPTable crt = getCritere();
        
        Field[] listF = crt.getClass().getDeclaredFields();
        setTableau(new TableauRecherche(getListe(), libEntete, critereLienTab, listF));

    }

    public void creerObjetPage(ClassMAPTable[] liste, String libEntete[]) throws Exception {
        setListe(liste);
        creerObjetPage(libEntete);
    }
    /*
     public void creerObjetPage(String libEnteteDefaut[],String []colSom) throws Exception
     {
     setColSomme(colSom);
     getValeurFormulaire();
     preparerData();
     makeTableauRecap();
     String[]enteteAuto= getTableauAffiche();
     if(getTableauAffiche()==null){
     setTableau(new TableauRecherche(getListe(),libEnteteDefaut));
     }
     else
     setTableau(new TableauRecherche(getListe(),enteteAuto));
     }*/

    public void creerObjetPage(String libEnteteDefaut[], String[] colSom) throws Exception {
        setColSomme(colSom);
        getValeurFormulaire();
        preparerData();
        makeTableauRecap();
        String critereLienTab = "<a href=" + getLien() + "?but=" + getApres() + "&numPag=1" + getApresLienPage() + formu.getListeCritereString();

        ClassMAPTable crt = getCritere();
        Field[] listF = crt.getClass().getDeclaredFields();

        String[] enteteAuto = getTableauAffiche();
        if (getTableauAffiche() == null) {
            setTableau(new TableauRecherche(getListe(), libEnteteDefaut, critereLienTab, listF));
        } else {
            setTableau(new TableauRecherche(getListe(), enteteAuto, critereLienTab, listF));
        }
        //this.getTableau().setListeField(listF);
    }
    public void creerObjetPage(String libEnteteDefaut[], String[] colSom,Formulaire autre) throws Exception {
        setColSomme(colSom);
        getValeurFormulaire();
        preparerData(autre);
        makeTableauRecap();
        String critereLienTab = "<a href=" + getLien() + "?but=" + getApres() + "&numPag=1" + getApresLienPage() + formu.getListeCritereString();

        ClassMAPTable crt = getCritere();
        Field[] listF = crt.getClass().getDeclaredFields();

        String[] enteteAuto = getTableauAffiche();
        if (getTableauAffiche() == null) {
            setTableau(new TableauRecherche(getListe(), libEnteteDefaut, critereLienTab, listF));
        } else {
            setTableau(new TableauRecherche(getListe(), enteteAuto, critereLienTab, listF));
        }
        //this.getTableau().setListeField(listF);
    }
    /* public void creerObjetPage() throws Exception
     {
     getValeurFormulaire();
     preparerData();
     makeTableauRecap();
     String[]enteteAuto= getTableauAffiche();
     setTableau(new TableauRecherche(getListe(),enteteAuto));
     } */

    public PageRecherche(bean.ClassMAPTable o, HttpServletRequest req, String[] vrt, String[] listInterval, int nbRange, String[] tabAff, int nbAff) throws Exception {
        setBase(o);
        getBase().setMode("select");
        //setTitre(titre);
        setCritere((bean.ClassMAPTable) (Class.forName(o.getClassName()).newInstance()));
        setReq(req);
        if(getReq().getParameter("premier") != null && getReq().getParameter("premier").compareToIgnoreCase("") != 0){
            setPremier(Boolean.valueOf(getReq().getParameter("premier")).booleanValue());
        }
        formu = new Formulaire();
        formu.setObjet(getBase());
        formu.makeChampFormu(vrt, listInterval, nbRange);
        setNbTableauAffiche(nbAff);
        setTableauAfficheDefaut(tabAff);
        makeTableauAffiche();
        formu.makeChampTableauAffiche(getNbTableauAffiche(), tabAff);
    }
    

    public PageRecherche() {
    }
    public PageRecherche(bean.ClassMAPTable o, HttpServletRequest req, String[] vrt, String[] listInterval, int nbRange, String[] tabAff) throws Exception {
        setBase(o);
        //setTitre(titre);
        setCritere((bean.ClassMAPTable) (Class.forName(o.getClassName()).newInstance()));
        setReq(req);
        if(getReq().getParameter("premier") != null && getReq().getParameter("premier").compareToIgnoreCase("") != 0){
            setPremier(Boolean.valueOf(getReq().getParameter("premier")).booleanValue());
        }
        formu = new Formulaire();
        formu.setObjet(getBase());
        formu.makeChampFormu(vrt, listInterval, nbRange);
        setNbTableauAffiche(tabAff.length);
        setTableauAfficheDefaut(tabAff);
        makeTableauAffiche();
        formu.makeChampTableauAffiche(getNbTableauAffiche(), tabAff);
    }

    public PageRecherche(bean.ClassMAPTable o, HttpServletRequest req, String[] vrt, String[] listInterval, int nbRange) throws Exception {
        setBase(o);
        //setTitre(titre);
        setCritere((bean.ClassMAPTable) (Class.forName(o.getClassName()).newInstance()));
        setReq(req);
        if(getReq().getParameter("premier") != null && getReq().getParameter("premier").compareToIgnoreCase("") != 0){
            setPremier(Boolean.valueOf(getReq().getParameter("premier")).booleanValue());
        }
        formu = new Formulaire();
        formu.setObjet(getBase());
        formu.makeChampFormu(vrt, listInterval, nbRange);
    }

    public bean.ClassMAPTable[] getListe() {
        return liste;
    }

    public void setListe(bean.ClassMAPTable[] liste) {
        this.liste = liste;
    }

    public void setListeTabDet(TableauRecherche listeTabDet) {
        this.listeTabDet = listeTabDet;
    }

    public TableauRecherche getListeTabDet() {
        return listeTabDet;
    }

    public void setCritere(bean.ClassMAPTable critere) {
        this.critere = critere;
        this.critere.setMode("select");
    }

    public bean.ClassMAPTable getCritere() {
        return critere;
    }

    public void setColInt(String[] colInt) {
        this.colInt = colInt;
    }

    public String[] getColInt() {
        return colInt;
    }

    public void setValInt(String[] valInt) {
        this.valInt = valInt;
    }

    public String[] getValInt() {
        return valInt;
    }

    public void makeHtml() {

    }

    public void setRs(bean.ResultatEtSomme rs) {
        this.rs = rs;
    }

    public bean.ResultatEtSomme getRs() {
        return rs;
    }

    public void setNumPage(int numPage) {
        this.numPage = numPage;
    }

    public int getNumPage() {
        int temp = 1;
        String tempS = getParamSansNull("numPag");
        //System.out.println("tempsS==>"+tempS);
        if (tempS == null || tempS.compareToIgnoreCase("") == 0) {
            return numPage;
        }
        temp = utilitaire.Utilitaire.stringToInt(tempS);
        if (temp > 0) {
            return temp;
        }
        return numPage;
    }

    public void setColSomme(String[] colSomme) {
        this.colSomme = colSomme;
    }

    public String[] getColSomme() {
        return colSomme;
    }

    public void makeTableauRecap() throws Exception {
        String[][] data = null;
        String[] entete = null;
        int nbColSomme = 0;
        if (getColSomme() != null) {
            nbColSomme = getColSomme().length;
        }
        entete = new String[nbColSomme + 2];
        data = new String[1][entete.length];
        entete[0] = "";
        data[0][0] = "Total";
        entete[1] = "Nombre";
        data[0][1] = utilitaire.Utilitaire.formaterSansVirgule(rs.getSommeEtNombre()[nbColSomme]);
        for (int i = 0; i < nbColSomme; i++) {
            data[0][i + 2] = utilitaire.Utilitaire.formaterAr(String.valueOf(rs.getSommeEtNombre()[i]));
            entete[i + 2] = "Somme de " + String.valueOf(getColSomme()[i]);
        }
        TableauRecherche t = new TableauRecherche(data, entete);
        t.setTitre("RECAPITULATION");
        setTableauRecap(t);
    }

    public int getNombrePage() {
        return (Utilitaire.calculNbPage(rs.getSommeEtNombre()[getNombreColonneSomme()], getNpp()));
    }

    public int getNombreColonneSomme() {
        int nbColSomme = 0;
        if (getColSomme() != null) {
            nbColSomme = getColSomme().length;
        }
        return nbColSomme;
    }
    /*public String getBasPage()
     {
     String retour="";
     retour+="<div class='col-md-12'>";
     retour+="<div class='col-md-6'>";
     retour+="<b>Nombre de r&eacute;sultats trouv&eacute;s:</b> "+utilitaire.Utilitaire.formaterSansVirgule(rs.getSommeEtNombre()[getNombreColonneSomme()])+"";
     retour+="</div>";
     retour+="<div class='col-md-6'>";
     retour+="<div class='col-md-2' align='left'>";
     retour+="<p><button class='btn btn-block btn-default btn-flat ";
     System.out.println(getNumPage());
     System.out.println("==============");
     if(getNumPage()<=1)
     {
     retour+="disabled";
     }
     retour+="'><a href="+getLien()+"?but="+getApres()+"&numPag="+String.valueOf(getNumPage()-1)+getApresLienPage()+formu.getListeCritereString()+"><i class='fa fa-arrow-circle-left'></i> </a></button>";
     retour+="</div>";
     retour+="<div class='col-md-2' align='center'>";
     retour+="<strong>page</strong> "+getNumPage()+" <b>sur </b>"+getNombrePage()+"";
     retour+="</div>";
     retour+="<div class='col-md-2' align='right'>";
     retour+="<button class='btn btn-block btn-default btn-flat ";
     if(getNumPage()>=getNombrePage())
     {
     retour+="disabled";
     }
     retour+="'><a href="+getLien()+"?but="+getApres()+"&numPag="+String.valueOf(getNumPage()+1)+getApresLienPage()+formu.getListeCritereString()+"> <i class='fa fa-arrow-circle-right'></i> </a></button></p>";
     retour+="</div>";
     retour+="</div></div>";
     return retour;
     }*/

    public String getBasPage() {
        String retour = "";
        retour += "<table border=0 cellpadding=0 cellspacing=0 align=center width='100%'>";
        retour += "<tr><td height=25><b>Nombre de r&eacute;sultat :</b> " + utilitaire.Utilitaire.formaterSansVirgule(rs.getSommeEtNombre()[getNombreColonneSomme()]) + "</td><td align='right'><strong>page</strong> " + getNumPage() + " <b>sur</b> " + getNombrePage() + "</td>";
        retour += "</tr>";
        retour += "<tr>";
        retour += "<td width=50% valign='top' height=25>";
        if (getNumPage() > 1) {
            retour += "<a href=" + getLien() + "?but=" + getApres() + "&premier="+ getPremier() +"&numPag=" + String.valueOf(getNumPage() - 1) + getApresLienPage() + formu.getListeCritereString() + ">&lt;&lt;Page pr&eacute;c&eacute;dente</a>";
        }
        retour += "</td>";
        retour += "<td width=50% align=right>";
        if (getNumPage() < getNombrePage()) {
            retour += "<a href=" + getLien() + "?but=" + getApres() + "&premier="+ getPremier()+"&numPag=" + String.valueOf(getNumPage() + 1) + getApresLienPage() + formu.getListeCritereString() + ">Page suivante&gt;&gt;</a>";
        }
        retour += "</td>";
        retour += "</tr>";
        retour += "</table>";
        retour += getDownloadForm();
        return retour;
    }
    
    public String getBasPageFront() {
        int x = (getNumPage() == 1) ? 1 : ((getNumPage() - 1)) * (getNpp() - 1) + 1;
        int y = (x - 1) + (getListe().length);
        String previous = ((getNumPage() > 1)) ? "<li>" : "<li class=\"disabled\" style=\"display:none\">";
        String next = (getNumPage() < getNombrePage()) ? "<li>" : "<li class=\"disabled\" style=\"display:none\">";
        String val = "<span class=\"pagination-libelle\">Affichage de l'&eacute;l&eacute;ment " + x + " &agrave; " + y + " sur " + ((int) rs.getSommeEtNombre()[getNombreColonneSomme()]) + " &eacute;l&eacute;ments</span>";
        if (getNumPage() == 1) {
            val = "<span class=\"pagination-libelle\">R&eacute;sultat(s) " + 1 + " &agrave; " + y + " sur " + ((int) rs.getSommeEtNombre()[getNombreColonneSomme()]) + " &eacute;l&eacute;ments</span>";
        }
        String pages = "";
        int gauche = 0;
        if (getNombrePage() > 16) {
            if (getNombrePage() <= getNumPage() + 7) {
                int j = 0;
                for (int i = getNumPage() + 1; i <= getNombrePage(); i++) {
                    pages += "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + i + getApresLienPage() + "\">" + i + "</a></li>";
                    j++;
                }
                String avant = "";
                if (getNumPage() - 7 > 1) {
                    avant += "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=1" + getApresLienPage() + "\">1</a></li>";
                    avant += "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=2" + getApresLienPage() + "\">2</a></li>";
                    avant += "<li class=\"disabled\"><a class=\"page-etc\">...</a></li>";
                    for (int g = 11 - j; g >= 1; g--) {
                        avant += "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + (getNumPage() - g) + getApresLienPage() + "\">" + (getNumPage() - g) + "</a></li>";
                    }
                    avant += "<li class=\"active\"><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + getNumPage() + getApresLienPage() + "\">" + getNumPage() + "</a></li>";
                }
                pages = avant + pages;
            }
            if (getNombrePage() > getNumPage() + 7) {
                if (getNumPage() - 7 <= 1) {
                    for (int i = 1; i <= getNumPage(); i++) {
                        pages += (i == getNumPage()) ? "<li class=\"active\"><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + i + getApresLienPage() + "\">" + i + "</a></li>" : "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + i + getApresLienPage() + "\">" + i + "</a></li>";
                        gauche++;
                    }
                }
                if (getNumPage() - 7 > 1) {
                    pages += "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=1" + getApresLienPage() + "\">1</a></li>";
                    pages += "<li class=\"disabled\"><a class=\"page-etc\">...</a></li>";
                    for (int g = 5; g >= 1; g--) {
                        pages += "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + (getNumPage() - g) + getApresLienPage() + "\">" + (getNumPage() - g) + "</a></li>";
                        gauche++;
                    }
                    pages += "<li class=\"active\"><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + getNumPage() + getApresLienPage() + "\">" + getNumPage() + "</a></li>";
                    gauche = gauche + 3;
                }
                int numpage = getNumPage() + 1;
                for (int d = 1; d <= 13 - gauche; d++) {
                    pages += "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + numpage + getApresLienPage() + "\">" + numpage + "</a></li>";
                    numpage++;
                }
                pages += "<li class=\"disabled\"><a class=\"page-etc\">...</a></li>";
                pages += "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + (getNombrePage() - 1) + getApresLienPage() + "\">" + (getNombrePage() - 1) + "</a></li>";
                pages += "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + getNombrePage() + getApresLienPage() + "\">" + getNombrePage() + "</a></li>";
            }
        } else {
            for (int i = 1; i <= getNombrePage(); i++) {
                if (i == getNumPage()) {
                    pages += "<li class=\"active\"><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + i + getApresLienPage() + "\">" + i + "</a></li>";
                } else {
                    pages += "<li><a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + i + getApresLienPage() + "\">" + i + "</a></li>";
                }
            }
        }
        String retour = "<div class=\"row\">\n"
                + "<div class=\"col-xs-12\">\n"
                + val + "\n"
                + "</div></div>\n"
                + "<div class=\"row\"><div class=\"col-xs-12\" style=\"text-align: left;\">       \n"
                + "<nav>\n"
                + "<ul class=\"pagination\">\n"
                + previous + "\n"
                + "<a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + String.valueOf(getNumPage() - 1) + getApresLienPage() + "\" aria-label=\"Pr&eacute;cedent\" title=\"Pr&eacute;cedent\">\n"
                + "<span aria-hidden=\"true\">&laquo;</span>\n"
                + "</a>\n"
                + "</li>\n"
                + pages + "\n"
                + next + "\n"
                + "<a href=\"" + getLien() + "?but=" + getApres() + formu.getListeCritereString() + "&numPag=" + String.valueOf(getNumPage() + 1) + getApresLienPage() + "\" aria-label=\"Suivant\" title=\"Suivant\">\n"
                + "<span aria-hidden=\"true\">&raquo;</span>\n"
                + "</a>\n"
                + "</li>\n"
                + "</ul>\n"
                + "</nav>\n"
                + "</div>\n"
                + "</div>";
        if(getNombrePage()>1)
            return retour;
        return "";
    }

    public String getBasPagee() {
        String retour = "";
        retour += "<table border=0 cellpadding=0 cellspacing=0 align=center width='100%'>";
        retour += "<tr><td height=25><b>Nombre de r&eacute;sultat :</b> " + utilitaire.Utilitaire.formaterAr(String.valueOf(rs.getSommeEtNombre()[getNombreColonneSomme()])) + "</td><td align='right'><strong>page</strong> " + getNumPage() + " <b>sur</b> " + getNombrePage() + "</td>";
        retour += "</tr>";
        retour += "<tr>";
        retour += "<td width=50% valign='top' height=25>";
        if (getNumPage() > 1) {
            retour += "<a href=" + getApres() + "?a=1" + formu.getListeCritereString() + "&premier="+ getPremier()+"&numPag=" + String.valueOf(getNumPage() - 1) + getApresLienPage() + "&champReturn=etudiant>&lt;&lt;Page pr&eacute;c&eacute;dente</a>";
        }
        retour += "</td>";
        retour += "<td width=50% align=right>";
        if (getNumPage() < getNombrePage()) {
            retour += "<a href=" + getApres() + "?a=1" + formu.getListeCritereString() + "&premier="+ getPremier()+"&numPag=" + String.valueOf(getNumPage() + 1) + getApresLienPage() + "&champReturn=etudiant>Page suivante&gt;&gt;</a>";
        }
        retour += "</td>";
        retour += "</tr>";
        retour += "</table>";
        return retour;
    }

    public void setApres(String apres) {
        this.apres = apres;
    }

    public String getApres() {
        return apres;
    }

    public String getAWhere() {
        return aWhere;
    }

    public void setAWhere(String aWhere) {
        this.aWhere = aWhere;
    }

    public void setApresLienPage(String apresLienPage) {
        this.apresLienPage = apresLienPage;
    }

    public String getApresLienPage() {
        return apresLienPage;
    }

    public void setNpp(int npp) {
        this.npp = npp;
    }

    public int getNpp() {
        return npp;
    }

    public String[] getTableauAffiche() {
        if (tableauAffiche == null || tableauAffiche.length == 0) {
            return tableauAfficheDefaut;
        }
        return tableauAffiche;
    }

    public String[] getTableauAfficheDefaut() {
        return tableauAfficheDefaut;
    }

    public void setTableauAfficheDefaut(String[] tableauAfficheDefaut) {
        this.tableauAfficheDefaut = tableauAfficheDefaut;
    }

    public void setTableauAffiche(String[] tableauAffiche) {
        this.tableauAffiche = tableauAffiche;
    }

    public int getNbTableauAffiche() {
        return nbTableauAffiche;
    }

    public void setNbTableauAffiche(int nbTableauAffiche) {
        this.nbTableauAffiche = nbTableauAffiche;
    }
    public void rajoutLienBasPage(String var,String valeur)
    {
        setApresLienPage(getApresLienPage()+"&"+var+"="+valeur);
    }
    public String getDownloadForm() {
        String tab = "";
        String tab1 = "";
        try {
            tab = this.getTableau().getHtml();
            tab1 = this.getTableau().getHtmlPDF();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String temp = "";
        temp += "<div class='row'>";
        temp += "<div class='row col-md-12'>";
        temp += "<div class='box box-primary box-solid collapsed-box'>";
        temp += "<div class='box-header with-border'>";
        temp += "<h3 class='box-title'>Exporter</h3>";
        temp += "<div class='box-tools pull-right'><button data-original-title='Collapse' class='btn btn-box-tool' data-widget='collapse' data-toggle='tooltip' title=''><i class='fa fa-plus'></i></button> </div>";
        temp += "</div>";
        temp += "<div class='box-body'>";
        temp += "<form action=\"../download\" method=\"post\">";
        temp += "<table id='export'>";
        temp += "<tr>";
        temp += "<th colspan='4'><h3 class='box-title'>Format</h3></th>";
        temp += "<th colspan='2'><h3 class='box-title'>Donn&eacute;es &agrave; exporter</h3></th>";
        temp += "<th></th>";
        temp += "</tr>";
        temp += "<tr>";
        temp += "<td>";
        temp += "<img src='../dist/img/csv_text.png'>";
        temp += "<input type='radio' name='ext' value='csv' checked='checked' />";
        temp += "</td>";
        temp += "<td>";
        temp += "<img src='../dist/img/file_xls.png'>";
        temp += "<input type='radio' name='ext' value='xls' />";
        temp += "</td>";
        temp += "<td>";
        temp += "<img src='../dist/img/file_xml.png'>";
        temp += "<input type='radio' name='ext' value='xml' />";
        temp += "</td>";
        temp += "<td>";
        temp += "<img src='../dist/img/file_pdf.png'>";
        temp += "<input type='radio' name='ext' value='pdf' />";
        temp += "</td>";
        temp += "<td>";
        temp += "<label for='donnee'>Page En Cours </label>";
        temp += "<input type='radio' name='donnee' value=\"" + 0 + "\" checked='checked' />";
        temp += "</td>";
        temp += "<td>";
        temp += "<label for='donnee'>Toutes </label>";
        temp += "<input type='radio' name='donnee' value=\"" + 1 + "\" />";
        temp += "</td>";
        temp += "<td>";
        if (this.getTableau().getExpxml() != null) {
            temp += "<input type=\"hidden\" name=\"xml\" value=\"" + this.getTableau().getExpxml().replace('"', '*') + "\" />";
            temp += "<input type=\"hidden\" name=\"csv\" value=\"" + this.getTableau().getExpcsv().replace('"', '*') + "\" />";
            temp += "<input type=\"hidden\" name=\"awhere\" value=\"" + this.getAWhere().replace('"', '\'') + "\" />";
            temp += "<input type=\"hidden\" name=\"table\" value=\"" + tab1.replace('"', '*') + "\" />";
        }
        temp += "<input type='submit' value='Exporter' class='btn btn-default' />";
        temp += "</td>";
        temp += "</tr>";
        temp += "</table>";
        temp += "</form>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        return temp;
    }
}
