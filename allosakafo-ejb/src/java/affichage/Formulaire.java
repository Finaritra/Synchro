package affichage;

import bean.ListeColonneTable;
import com.google.gson.Gson;
import java.util.Vector;
import java.sql.Connection;

/**
 * <p>
 * Title: Gestion des recettes </p>
 * <p>
 * Description: </p>
 * <p>
 * Copyright: Copyright (c) 2005</p>
 * <p>
 * Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */
public class Formulaire {

    private Champ[] listeChamp;
    private String id;
    private int nbRangee = 2;
    private String html;
    private String tailleTableau = "100%";
    private String cssTableau = "monographe";
    private String cssClass;
    private bean.ClassMAPTable objet;
    private Champ[] crtFormu;
    private int nbIntervalle = 0;
    private String htmlTri;
    private String htmlButton;
    private String htmlInsert;
    private Champ[] champGroupe;
    private Champ[] champTableauAff;
    private String htmlGroupe;
    private String htmlTableauAff;
    private String htmlTableauInsert;
    private String[] ordre = null;
    private int nbLigne = 0;

    

    public int getNbLigne() {
        return nbLigne;
    }

    public void setNbLigne(int nbLigne) {
        this.nbLigne = nbLigne;
    }

    public Formulaire() {
    }

    public static Champ[] transformerEnChamp(String[] list) {
        Champ[] ret = new Champ[list.length];
        for (int i = 0; i < list.length; i++) {
            ret[i] = new Champ(list[i]);
        }
        return ret;
    }

    public void changerEnChamp(Champ[] list) throws Exception {
        int nombre = list.length;
        for (int i = 0; i < nombre; i++) {
            for (int j = 0; j < listeChamp.length; j++) {
                if (listeChamp[j].getNom().compareToIgnoreCase(list[i].getNom()) == 0) {
                    if (list[i].getValeur() == null || list[i].getValeur().compareToIgnoreCase("") == 0) {
                        list[i].setValeur(listeChamp[j].getValeur());
                    }
                    if (list[i].getNom().compareToIgnoreCase("mois2") == 0) {
                        ((Liste) (list[i])).setDefaultSelected("12");
                        //System.out.println("niditra "+i+" nom = "+list[i].getNom());
                    }
                    listeChamp[j] = list[i];
                    
                    break;
                }
            }
        }
    }

    public Formulaire(Champ[] listeC, int range) {
        setListeChamp(listeC);
        setNbRangee(range);
    }

    public Formulaire(String[] listeC, int range) {
        setListeChamp(transformerEnChamp(listeC));
        setNbRangee(range);
    }

    public String makeHtmlForm() throws Exception {
        String retour = "";
        //retour+="<form action="++"?but="++" method='post' name='formu'";
        return retour;
    }

    public void makeHtmlTri() throws Exception {
        String temp = "";
        temp += "<div class='col-md-4 triecolonne'>";
        temp += "<div class='box box-primary box-solid collapsed-box'>";
        temp += "<div class='box-header with-border'>";
        temp += "<h3 class='box-title' color='#edb031'><span color='#edb031'>Tri</span></h3>";
        temp += "<div class='box-tools pull-right'><button data-original-title='Collapse' class='btn btn-box-tool' data-widget='collapse' data-toggle='tooltip' title=''><i class='fa fa-plus'></i></button> </div>";
        temp += "</div>";
        temp += "<div class='box-body'>";
        temp += "<div class='form-group'>";
        temp += "<div class='col-xs-5'>";
        temp += "<label for='Colonne'>Colonne</label>";
        temp += getListeChamp()[getListeChamp().length - 2].getHtml();
        temp += "</div>";
        temp += "<div class='col-xs-5'>";
        temp += "<label for='Ordre'>Ordre</label>";
        temp += getListeChamp()[getListeChamp().length - 1].getHtml();
        temp += "</div>";
        temp += "</div>";
        temp += "</div></div>";
        setHtmlTri(temp);
    }

    public String makeHtmlButton() {
        String temp = "";
//        temp += "<input type='hidden' name='premier' value='false'/>";
        temp += "<div class='col-xs-6' align='right'>";
        temp += "<button type='submit' class='btn btn-primary' id='btnListe'>Afficher</button>";
        temp += "</div>";
        temp += "<div class='col-xs-6' align='left'>";
        temp += "<button type='reset' class='btn btn-default'>R&eacute;initialiser</button>";
        temp += "</div>";
        return temp;
    }

    public Champ[] getListeIntervalle() {
        Vector ret = new Vector();
        for (int i = 0; i < getListeChamp().length; i++) {
            if (getListeChamp()[i].getEstIntervalle() == 1) {
                ret.add(getListeChamp()[i]);
            }
        }
        Champ[] retour = new Champ[ret.size()];
        ret.copyInto(retour);
        return retour;
    }

    public String getHtmlEnsemble() throws Exception {
        String temp = "";
        temp += "<div class='row'>";

        temp += "<div class='row col-md-12'>";
        temp += getHtml();
        temp += "<div class='box-footer'>";
        makeHtmlGroupe();
        temp += getHtmlGroupe();
        makeHtmlTableauAff();
        temp += getHtmlTableauAff();
        temp += getHtmlTri();
        temp += "</div>";
        temp += "</div>";
        temp += "<div class='row col-md-12'>";
        temp += makeHtmlButton();
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";

        temp += "</div>";
        //temp+="</div>";
        return temp;
    }

    public Champ getChamp(String nomChamp) {
        for (int i = 0; i < getListeChamp().length; i++) {
            if (getListeChamp()[i].getNom().compareToIgnoreCase(nomChamp) == 0) {
                return getListeChamp()[i];
            }
        }
        return null;
    }

    public void changerLibelle(String[] libelle) throws Exception {
        if (libelle.length != getListeChamp().length) {
            throw new Exception("nombre de champ different de linitial");
        }
        for (int i = 0; i < getListeChamp().length; i++) {
            getListeChamp()[i].setLibelle(libelle[i]);
        }
    }
    public void makeChampFormuLigne(String[] aAff) throws Exception {
        Vector liste = new Vector();
        for (int i = 0; i < aAff.length; i++) {
            //System.out.println("critere = "+crt[i]+" get Champ =========="+getObjet()+"======= "+ListeColonneTable.getChamp(getObjet(),crt[i]));
            //String type = ListeColonneTable.getChamp(getObjet(), aAff[i]).getTypeColonne();
            
            liste.add(new Champ(aAff[i]));
            
        }
        bean.Champ[] nomCol = ListeColonneTable.getFromListe(getObjet(), null);
        Champ[] listeForm = new Champ[liste.size()];
        liste.copyInto(listeForm);
        setListeChamp(listeForm);
    }

    public void makeChampFormu(String[] crt, String[] listeInterval, int nbRange) throws Exception {
        setCrtFormu(transformerEnChamp(crt));
        Vector liste = new Vector();
        for (int i = 0; i < crt.length; i++) {
            String type = ListeColonneTable.getChamp(getObjet(), crt[i]).getTypeColonne();
            //System.out.println("type ="+type);
            if (utilitaire.Utilitaire.estIlDedans(crt[i], listeInterval) != -1) //On fait des intervalles pour les valeurs dates et montant
            {
                getCrtFormu()[i].setEstIntervalle(1);
                setNbIntervalle(getNbIntervalle() + 1);
                String t1 = crt[i] + "1";
                String t2 = crt[i] + "2";
                liste.add(new Champ(t1));
                liste.add(new Champ(t2));
                if (type.compareToIgnoreCase("java.sql.Date") == 0) {
                    getCrtFormu()[i].setTypeData(ConstanteAffichage.typeDaty);
                }
            } else {
                liste.add(new Champ(crt[i]));
            }
        }
        bean.Champ[] nomCol = ListeColonneTable.getFromListe(getObjet(), null);
        Champ col = new Liste("colonne", nomCol);
        String[] val = {"-", "DESC", "ASC"};
        Champ ord = new Liste("ordre", val);
        liste.add(col);
        liste.add(ord);
        Champ[] listeForm = new Champ[liste.size()];
        liste.copyInto(listeForm);
        setListeChamp(listeForm);
        setNbRangee(nbRange);
    }

    public void makeChampFormuInsert() throws Exception {
    }

    public void makeHtml() throws Exception {

        String temp = "";
        //temp+="<form action="++"?but="++" method='post' name='formu'";
        //temp+="<table width="+getTailleTableau()+" border='0' align='center' cellpadding=2 cellspacing=0 class="+getCssTableau()+">";
        temp += "<div class='box box-primary box-solid collapsed collapsed-box'>";
        temp += "<div class='box-header with-border'>";
        temp += "<h3 class='box-title' color='#edb031'><span color='#edb031'>Recherche avanc&eacute;e</span></h3>";
        temp += "<div class='box-tools pull-right'><button data-original-title='Collapse' class='btn btn-box-tool' data-widget='collapse' data-toggle='tooltip' title=''><i class='fa fa-plus'></i></button> </div>";
        temp += "</div>";
        temp += "<div class='box-body' id='pagerecherche'>";
        int reste = listeChamp.length % getNbRangee();
        int nombreLigne = 0;
        if (reste != 0) {
            nombreLigne = listeChamp.length / getNbRangee();
        } else {
            nombreLigne = (listeChamp.length / getNbRangee()) + 1;
        }
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<div class='form-group'>";
            for (int j = 0; j < getNbRangee(); j++) {
                temp += "<div class='col-md-4'>";
                if (((i * getNbRangee()) + j) >= listeChamp.length - 2) {
                    temp += "</div>";
                    break;
                };
                Champ c = listeChamp[(i * getNbRangee()) + j];
                c.setAutre("onkeydown='return searchKeyPress(event)'");
                String troisiemeTd = "";
                if (c.getTypeData() != null && c.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeDaty) == 0) {
                    troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + c.getPageAppel() + "?champReturn=" + c.getNom() + "&apresLienPageAppel=" + c.getApresLienPageappel() + "') value=... />";
                }
                if (c.getPageAppel() != null && c.getPageAppel().compareToIgnoreCase("") != 0) {
                    troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + c.getPageAppel() + "?champReturn=" + c.getNom() + "&apresLienPageAppel=" + c.getApresLienPageappel() + "') value=... />";
                }
                temp += "<label for=" + c.getLibelleAffiche() + ">" + c.getLibelleAffiche() + "</label>";
                temp += c.getHtml();
                temp += troisiemeTd;
                temp += "</div>";
            }
            temp += "</div>";
        }
        temp += "</div>";
        setHtml(temp);
    }

    public void makeChampTableauAffiche(int nbTabAffiche, String[] defaut) throws Exception {
        Champ[] listeAff = new Champ[nbTabAffiche];
        bean.Champ[] nomCol = ListeColonneTable.getFromListe(getObjet(), null);
        String valeur[] = new String[nomCol.length + 1];
        valeur[0] = "-";
        for (int k = 0; k < nomCol.length; k++) {
            valeur[k + 1] = nomCol[k].getNomColonne();
        }
        for (int i = 0; i < nbTabAffiche; i++) {
            listeAff[i] = new Liste("colAffiche" + (i + 1), "Colonne " + (i + 1), valeur);
            //System.out.println("colAffiche"+(i+1)+" == "+valeur);
            ((Liste) (listeAff[i])).setDefaultSelected(defaut[i]);
        }
        setChampTableauAff(listeAff);
    }

    public void makeChampGroupe(int nombreChamp, int nbsomme) throws Exception {
        Champ[] listeGroupe = new Champ[nombreChamp + nbsomme];
        bean.Champ[] nomCol = ListeColonneTable.getFromListe(getObjet(), null);
        String valeur[] = new String[nomCol.length + 1];
        valeur[0] = "-";
        for (int k = 0; k < nomCol.length; k++) {
            valeur[k + 1] = nomCol[k].getNomColonne();
        }
        //Champ col=new Liste("colonne",nomCol);
        //System.out.println("nombre="+nombreChamp+"nbsomme="+nbsomme+"nombreChamp+nbsomme="+(nombreChamp+nbsomme));
        for (int i = 0; i < nombreChamp + nbsomme; i++) {
            if (i < nombreChamp) {
                listeGroupe[i] = new Liste("colGroupe" + (i + 1), "Colonne Group&eacute;e" + (i + 1), valeur);
            } else {
                listeGroupe[i] = new Liste("colSomme" + (i + 1), valeur);
            }
        }
        setChampGroupe(listeGroupe);
    }

    //Colonne a afficher
    public void makeHtmlTableauAff() throws Exception {
        if (getChampTableauAff() == null) {
            setHtmlTableauAff("");
            return;
        }
        String temp = "";
        temp += "<div class='row col-md-12'></div>";
        temp += "<div class='row col-md-12 containerColAffTrie'>";
        temp += "<div class='col-md-8 tableauaffiche'>";
        temp += "<div class='box box-primary box-solid collapsed-box'>";
        temp += "<div class='box-header with-border'>";
        temp += "<h3 class='box-title'>Choix des colonnes &agrave; afficher</h3>";
        temp += "<div class='box-tools pull-right'><button data-original-title='Collapse' class='btn btn-box-tool' data-widget='collapse' data-toggle='tooltip' title=''><i class='fa fa-plus'></i></button> </div>";
        temp += "</div>";
        temp += "<div class='box-body' id='pagerecherche'>";
        int reste = getChampTableauAff().length % getNbRangee();
        int nombreLigne = 0;
        if (reste != 0) {
            nombreLigne = getChampTableauAff().length / getNbRangee() + 1;
        } else {
            nombreLigne = (getChampTableauAff().length / getNbRangee());
        }
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<div class='col-md-4'>";
            temp += "<div class='form-group'>";
            for (int j = 0; j < getNbRangee(); j++) {
                if (((i * getNbRangee()) + j) == getChampTableauAff().length) {
                    break;
                }
                Champ c = getChampTableauAff()[(i * getNbRangee()) + j];
                temp += "<label for=" + c.getNom() + ">" + c.getLibelleAffiche() + "</label>";
                temp += c.getHtml();
            }
            temp += "</div>";
            temp += "</div>";
        }
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        setHtmlTableauAff(temp);
    }

    public void makeHtmlGroupe() throws Exception {
        if (getChampGroupe() == null) {
            setHtmlGroupe("");
            return;
        }
        //temp+="<form action="++"?but="++" method='post' name='formu'";
        String temp = "";
        temp += "<div class='row col-md-12'></div>";
        temp += "<div class='row col-md-12 containerColAffTrie'>";
        temp += "<div class='col-md-8 tableauaffiche'>";
        temp += "<div class='box box-primary box-solid collapsed-box'>";
        temp += "<div class='box-header with-border'>";
        temp += "<h3 class='box-title'>Choix des colonnes group&eacute;es &agrave; afficher</h3>";
        temp += "<div class='box-tools pull-right'><button data-original-title='Collapse' class='btn btn-box-tool' data-widget='collapse' data-toggle='tooltip' title=''><i class='fa fa-plus'></i></button> </div>";
        temp += "</div>";
        temp += "<div class='box-body' id='pagerecherche'>";
        int reste = getChampGroupe().length % getNbRangee();
        int nombreLigne = 0;
        if (reste != 0) {
            nombreLigne = getChampGroupe().length / getNbRangee() + 1;
        } else {
            nombreLigne = (getChampGroupe().length / getNbRangee());
        }
        for (int i = 0; i < nombreLigne; i++) {
            temp += "<div class='col-md-4'>";
            temp += "<div class='form-group'>";
            for (int j = 0; j < getNbRangee(); j++) {
                if (((i * getNbRangee()) + j) == getChampGroupe().length) {
                    break;
                }
                Champ c = getChampGroupe()[(i * getNbRangee()) + j];
                temp += "<label for=" + c.getNom() + ">" + c.getLibelleAffiche() + "</label>";
                temp += c.getHtml();
            }
            temp += "</div>";
            temp += "</div>";
        }
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        setHtmlGroupe(temp);
    }

    public void setLibelleAffiche(String[] liste) {
        for (int i = 0; i < liste.length; i++) {
            listeChamp[i].setLibelleAffiche(liste[i]);
        }
    }

    public void setLibelleAffiche(String libelle, int numeroLib) {
        listeChamp[numeroLib].setLibelleAffiche(libelle);
    }

    public void makeHtmlInsert() throws Exception {
        String temp = "";
        temp += "<div class='row'>";
        temp += "<div class='col-md-1'></div>";
        temp += "<div class='col-md-10'>";
        temp += "<div class='box box-primary'>";
        temp += "<div class='box-body'>";
        temp += "<table class=\"table table-bordered\">";
        for (int i = 0; i < getListeChamp().length; i++) {
            Champ c = getListeChamp()[i];
            String troisiemeTd = "";

            if (c.getVisible() == true) {
                temp += "<tr>";
                temp += "<th>";
                temp += "<label for='" + getListeChamp()[i].getLibelle() + "'>" + getListeChamp()[i].getLibelle() + "</label>";
                temp += "</th>";
                //System.out.println("getListeChamp()[i].getHtmlInsert() ====== " + getListeChamp()[i].getHtmlInsert());
                
                if (c.getPageAppel() != null && c.getPageAppel().compareToIgnoreCase("") != 0) {
                    temp += "<td>" + getListeChamp()[i].getInputHidden() + "</td>";
                    troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + c.getPageAppel() + "?champReturn=" + c.getChampReturn() + "&apresLienPageAppel=" + c.getApresLienPageappel() + "') value=... />";
                    temp += "<td>" + troisiemeTd + "</td>";
                } else {
                    temp += "<td>" + getListeChamp()[i].getHtmlInsert()+ "</td>";
                }
                
                temp += "</tr>";
            } else {
                getListeChamp()[i].setType("hidden");
                temp += getListeChamp()[i].getHtmlInsert();
            }
        }
        if (getObjet().getEstHistorise()) {
            temp += "<tr>";
            temp += "<th>";
            temp += "<label for='Memo'>Memo Modification</label>";
            temp += "</th>";
            temp += "<td>";
            temp += "<textarea name='memo' class='form-control'></textarea>";
            temp += "</td>";
            temp += "</tr>";
        }
        temp += "</table>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "<div class='col-md-1'></div>";
        temp += "</div>";

        setHtmlInsert(temp);
    }

    public void makeHtmlAddTabIndex() throws Exception {
        String temp = "";
        temp += "<div class='row'>";
        temp += "<div class='insertFormulaire'>";
        temp += "<div>";
        temp += "<div class='box box-primary'>";
        temp += "<div class='box-body'>";
        temp += "<table class='table table-bordered'>";
        for (int i = 0; i < getListeChamp().length; i++) {
            Champ c = getListeChamp()[i];
            String troisiemeTd = "";
            if (c.getNom().compareToIgnoreCase("iduser") == 0) {
                c.setVisible(false);
            }
            if (c.getVisible() == true) {
                //temp+="<div class='form-group'>";
                c.setAutre("tabindex='" + (i + 1) + "'" + c.getAutre());
                temp += "<tr>";
                temp += "<th><label for='" + getListeChamp()[i].getLibelle() + "' " + c.getAutre() + ">" + getListeChamp()[i].getLibelle() + "</label></th>";
                
                if (c.getPageAppel() != null && c.getPageAppel().compareToIgnoreCase("") != 0) {
                    temp += "<td>" + getListeChamp()[i].getInputHidden() + "</td>";
                    troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + c.getPageAppel() + "?champReturn=" + c.getChampReturn() + "&apresLienPageAppel=" + c.getApresLienPageappel() + "') value=... />";
                    temp += "<td>" + troisiemeTd + "</td>";
                } else {
                    temp += "<td>" + getListeChamp()[i].getHtmlInsert()+ "</td>";
                }
                
                if (c.getTypeData() != null && c.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeDaty) == 0) {
                    troisiemeTd = "<a href='javascript:cal.popup()'><img src='calendar/img/cal.gif' alt='Cliquez ici pour choisir une date' width=16 height=16 border=0 align='absmiddle' /></a>";
                    temp += "<td>" + troisiemeTd + "</td>";
                }
                temp += "</tr>";
        
            }
        }
        temp += "</table>";
        temp += "</div>";
        temp += "<div class='box-footer'>";
        temp += "<div class='col-xs-12'>";
        temp += "<button type='submit' name='Submit2' class='btn btn-success pull-right' style='margin-right: 25px;' tabIndex='" + getListeChamp().length + 1 + "'>Ajouter</button> ";
        temp += "<button type='reset' name='Submit2' class='btn btn-default pull-right' style='margin-right: 15px;' tabIndex='" + getListeChamp().length + 2 + "'>R&eacute;initialiser</button>";
        temp += "</div></div></div></div></div></div>";
        setHtmlInsert(temp);
    }

    public void makeHtmlInsertCommande() throws Exception {
        String temp = "";
        temp += "<div class='row'>";
        temp += "<div class='insertFormulaire'>";
        temp += "<div>";
        temp += "<div class='box box-primary'>";
        temp += "<div class='box-body'>";
        temp += "<table class=\"table table-bordered\">";
        
        
        for (int i = 0; i < getListeChamp().length; i++) {
            Champ c = getListeChamp()[i];
            //System.out.println("ch.getNom() " + c.getNom());
            String troisiemeTd = "";
            if (c.getNom().compareToIgnoreCase("iduser") == 0) {
                c.setVisible(false);
            }
            if (c.getVisible() == true) {
                //temp+="<div class='form-group'>";
                c.setAutre("tabindex='" + (i + 1) + "'" + c.getAutre());
                temp += "<tr>";
                temp += "<th><label for='" + getListeChamp()[i].getLibelle() + "' " + c.getAutre() + ">" + getListeChamp()[i].getLibelle() + "</label></th>";
                
                if (c.getPageAppel() != null && c.getPageAppel().compareToIgnoreCase("") != 0) {
                    temp += "<td>" + getListeChamp()[i].getInputHidden() + "</td>";
                    troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + c.getPageAppel() + "?champReturn=" + c.getChampReturn() + "&apresLienPageAppel=" + c.getApresLienPageappel() + "') value=... />";
                    temp += "<td>" + troisiemeTd + "</td>";
                } else {
                    temp += "<td>" + getListeChamp()[i].getHtmlInsert()+ "</td>";
                }
                if (c.getTypeData() != null && c.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeDaty) == 0) {
                    //System.out.println("niditra tato ndray");
                    troisiemeTd = "<a href='javascript:cal.popup()'><img src='calendar/img/cal.gif' alt='Cliquez ici pour choisir une date' width=16 height=16 border=0 align='absmiddle' /></a>";
                    temp += "<td>" + troisiemeTd + "</td>";
                }
                temp += "</tr>";
                
            }
        }
        temp += "</table>";
        temp += "</div>";
        temp += "<div class='box-footer'>";                              
        temp += "<div class='col-xs-12'>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        setHtmlInsert(temp);
    }
    
     public void makeHtmlInsertTabIndex() throws Exception {
        String temp = "";
        if (getListeChamp().length < 13) {

            temp += "<div class='row'>";
            temp += "<div class='col-md-3'></div>";
            temp += "<div class='col-md-6'>";
            temp += "<div class='box-insert-amadia'>";
            temp += "<div class='box'>";
            temp += "<div class='box-body'>";
            temp += "<table class=\"table table-bordered\">";
            for (int i = 0; i < getListeChamp().length; i++) {
                Champ c = getListeChamp()[i];
                String troisiemeTd = "";
                if (c.getNom().compareToIgnoreCase("iduser") == 0) {
                    c.setVisible(false);
                }
                if (c.getVisible() == true) {
                    //temp+="<div class='form-group'>";
                    c.setAutre("tabindex='" + (i + 1) + "'" + c.getAutre());
                    temp += "<tr>";
                    temp += "<th><label for='" + getListeChamp()[i].getLibelle() + "' " + c.getAutre() + ">" + getListeChamp()[i].getLibelle() + "</label></th>";
                    if (c.isAutoComplete()) {
                        temp += "<td>" + getListeChamp()[i].getInputHiddenAutoComplete() + "</td>";
                    } else if (c.getPageAppel() != null && c.getPageAppel().compareToIgnoreCase("") != 0) {
                        temp += "<td>" + getListeChamp()[i].getInputHidden() + "</td>";
                        troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + c.getPageAppel() + "?champReturn=" + c.getChampReturn() + "&apresLienPageAppel=" + c.getApresLienPageappel() + "') value=... />";
                        temp += "<td>" + troisiemeTd + "</td>";
                    } else {
                        temp += "<td>" + getListeChamp()[i].getHtmlInsert() + "</td>";
                    }
                    if (c.getTypeData() != null && c.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeDaty) == 0) {
                        //System.out.println("niditra tato ndray");
                        troisiemeTd = "<a href='javascript:cal.popup()'><img src='calendar/img/cal.gif' alt='Cliquez ici pour choisir une date' width=16 height=16 border=0 align='absmiddle' /></a>";
                        temp += "<td>" + troisiemeTd + "</td>";
                    }
                    temp += "</tr>";

                }
            }
            temp += "</table>";
            temp += "</div>";
            temp += "<div class='box-footer'>";
            temp += "<div class='col-xs-12'>";
            temp += "<button type='submit' name='Submit2' class='btn btn-success pull-right' style='margin-right: 25px;' tabIndex='" + getListeChamp().length + 1 + "'>Enregistrer</button> ";
            temp += "<button type='reset' name='Submit2' class='btn btn-default pull-right' style='margin-right: 15px;' tabIndex='" + getListeChamp().length + 2 + "'>R&eacute;initialiser</button>";
            temp += "</div>";
            temp += "</div>";
            temp += "</div>";
            temp += "</div>";
            temp += "</div>";
            temp += "<div class='col-md-3'></div>";
            temp += "</div>";
        } else {
            int nb2 = getListeChamp().length / 2;

            temp += "<div class='box-insert-amadia'>";
            temp += "<div class='box'>";
            temp += "<div class='box-body'>";

            temp += "<div class='row'>";
            temp += "<div class='col-md-6'>";
            temp += "<table class='table table-bordered'>";
            for (int i = 0; i < nb2; i++) {
                Champ c = getListeChamp()[i];
                String troisiemeTd = "";
                if (c.getNom().compareToIgnoreCase("iduser") == 0) {
                    c.setVisible(false);
                }
                if (c.getVisible() == true) {
                    //temp+="<div class='form-group'>";
                    c.setAutre("tabindex='" + (i + 1) + "'" + c.getAutre());
                    temp += "<tr>";
                    temp += "<th><label for='" + getListeChamp()[i].getLibelle() + "' " + c.getAutre() + ">" + getListeChamp()[i].getLibelle() + "</label></th>";

                    if (c.getPageAppel() != null && c.getPageAppel().compareToIgnoreCase("") != 0) {
                        temp += "<td>" + getListeChamp()[i].getInputHidden() + "</td>";
                        troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + c.getPageAppel() + "?champReturn=" + c.getChampReturn() + "&apresLienPageAppel=" + c.getApresLienPageappel() + "') value=... />";
                        temp += "<td>" + troisiemeTd + "</td>";
                    } else {
                        temp += "<td>" + getListeChamp()[i].getHtmlInsert() + "</td>";
                    }
                    if (c.getTypeData() != null && c.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeDaty) == 0) {
                        //System.out.println("niditra tato ndray");
                        troisiemeTd = "<a href='javascript:cal.popup()'><img src='calendar/img/cal.gif' alt='Cliquez ici pour choisir une date' width=16 height=16 border=0 align='absmiddle' /></a>";
                        temp += "<td>" + troisiemeTd + "</td>";
                    }
                    temp += "</tr>";

                }
            }
            temp += "</table>";
            temp += "</div>";
            temp += "<div class='col-md-6'>";
            temp += "<table class='table table-bordered'>";
            for (int i = nb2; i < getListeChamp().length; i++) {
                Champ c = getListeChamp()[i];
                String troisiemeTd = "";
                if (c.getNom().compareToIgnoreCase("iduser") == 0) {
                    c.setVisible(false);
                }
                if (c.getVisible() == true) {
                    //temp+="<div class='form-group'>";
                    c.setAutre("tabindex='" + (i + 1) + "'" + c.getAutre());
                    temp += "<tr>";
                    temp += "<th><label for='" + getListeChamp()[i].getLibelle() + "' " + c.getAutre() + ">" + getListeChamp()[i].getLibelle() + "</label></th>";

                    if (c.getPageAppel() != null && c.getPageAppel().compareToIgnoreCase("") != 0) {
                        temp += "<td>" + getListeChamp()[i].getInputHidden() + "</td>";
                        troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + c.getPageAppel() + "?champReturn=" + c.getChampReturn() + "&apresLienPageAppel=" + c.getApresLienPageappel() + "') value=... />";
                        temp += "<td>" + troisiemeTd + "</td>";
                    } else {
                        temp += "<td>" + getListeChamp()[i].getHtmlInsert() + "</td>";
                    }
                    if (c.getTypeData() != null && c.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeDaty) == 0) {
                        //System.out.println("niditra tato ndray");
                        troisiemeTd = "<a href='javascript:cal.popup()'><img src='calendar/img/cal.gif' alt='Cliquez ici pour choisir une date' width=16 height=16 border=0 align='absmiddle' /></a>";
                        temp += "<td>" + troisiemeTd + "</td>";
                    }
                    temp += "</tr>";

                }
            }
            temp += "</table>";
            temp += "</div>";
            temp += "</div>";
            temp += "</div>";
            temp += "<div class='box-footer'>";
            temp += "<div class='col-xs-12'>";
            temp += "<button type='submit' name='Submit2' class='btn btn-success pull-right' style='margin-right: 25px;' tabIndex='" + getListeChamp().length + 1 + "'>Enregistrer</button> ";
            temp += "<button type='reset' name='Submit2' class='btn btn-default pull-right' style='margin-right: 15px;' tabIndex='" + getListeChamp().length + 2 + "'>R&eacute;initialiser</button>";
            temp += "</div>";
            temp += "</div>";
            temp += "</div>";
            temp += "</div>";

        }
        setHtmlInsert(temp);
    }
    
   /* public void makeHtmlInsertTabIndex() throws Exception {
        String temp = "";
        temp += "<div class='row'>";
        temp += "<div class='insertFormulaire'>";
        temp += "<div>";
        temp += "<div class='box box-primary'>";
        temp += "<div class='box-body'>";
        temp += "<table class=\"table table-bordered\">";
        for (int i = 0; i < getListeChamp().length; i++) {
            Champ c = getListeChamp()[i];
            String troisiemeTd = "";
            if (c.getNom().compareToIgnoreCase("iduser") == 0) {
                c.setVisible(false);
            }
            if (c.getVisible() == true) {
                //temp+="<div class='form-group'>";
                c.setAutre("tabindex='" + (i + 1) + "'" + c.getAutre());
                temp += "<tr>";
                temp += "<th><label for='" + getListeChamp()[i].getLibelle() + "' " + c.getAutre() + ">" + getListeChamp()[i].getLibelle() + "</label></th>";
                
                if (c.getPageAppel() != null && c.getPageAppel().compareToIgnoreCase("") != 0) {
                    temp += "<td>" + getListeChamp()[i].getInputHidden() + "</td>";
                    troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + c.getPageAppel() + "?champReturn=" + c.getChampReturn() + "&apresLienPageAppel=" + c.getApresLienPageappel() + "') value=... />";
                    temp += "<td>" + troisiemeTd + "</td>";
                } else {
                    temp += "<td>" + getListeChamp()[i].getHtmlInsert()+ "</td>";
                }
                if (c.getTypeData() != null && c.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeDaty) == 0) {
                    //System.out.println("niditra tato ndray");
                    troisiemeTd = "<a href='javascript:cal.popup()'><img src='calendar/img/cal.gif' alt='Cliquez ici pour choisir une date' width=16 height=16 border=0 align='absmiddle' /></a>";
                    temp += "<td>" + troisiemeTd + "</td>";
                }
                temp += "</tr>";
                
            }
        }
        temp += "</table>";
        temp += "</div>";
        temp += "<div class='box-footer'>";                              
        temp += "<div class='col-xs-12'>";
        temp += "<button type='submit' name='Submit2' class='btn btn-success pull-right' style='margin-right: 25px;' tabIndex='" + getListeChamp().length + 1 + "'>Enregistrer</button> ";
        temp += "<button type='reset' name='Submit2' class='btn btn-default pull-right' style='margin-right: 15px;' tabIndex='" + getListeChamp().length + 2 + "'>R&eacute;initialiser</button>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        setHtmlInsert(temp);
    }*/
      public String getHtmlAutocomplete(int k) throws Exception {
        String retour = "<script> jQuery(document).ready(function () {";
        for (int i = 0; i < getListeChamp().length; i++) {
            Champ c = getListeChamp()[i];
            if (c.getValeur_ac() != null) {
                retour += "$(document).on('keydown', '#" + c.getNom() + "_"+k+"', function (e) {\n"
                        + "            var keyCode = e.keyCode || e.which;\n"
                        + "            if ((keyCode == 9 || keyCode == 13)) {\n"
                        + "                var prest = $('#" + c.getNom() + "_"+k+"').val();\n"
                        + "                if (prest != null && prest.trim() != \"\") {\n"
                        + "                    var temp = prest.split(\":\");\n"
                        + "                    $('#" + c.getNom() + "_"+k+"').val(temp[0].trim());\n"
                        + "                }\n"
                        + "\n"
                        + "            } else {\n"
                        + "                var data = " + c.getValeur_ac() + "\n"
                        + "                $('#" + c.getNom() + "_"+k+"').autocomplete({\n"
                        + "                    source: function (request, response) {\n"
                        + "                        var matches = $.map(data, function (acItem) {\n"
                        + "                            if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {\n"
                        + "                                return acItem;\n"
                        + "                            }\n"
                        + "                        });\n"
                        + "                        response(matches);\n"
                        + "                    },\n"
                        + "                    minLength: 0,\n"
                        + "                    select: function (e, ui) {\n"
                        + "                        var selectedObj = ui.item;\n"
                        + "                        var prest = selectedObj.value;\n"
                        + "                        var temp = prest.split(\":\");\n"
                        + "                        var compte = temp[0].trim();\n"
                        + "$('#" + c.getNom() + "_"+k+"').focusout(function() { "
                        + "                        $('#" + c.getNom() + "_"+k+"').val(compte);\n"
                        
                        + "                        console.log(compte);\n"
                        + "});"
                        + "                    }\n"
                        + "                });\n"
                        + "            }\n"
                        + "        });";
            }
        }
        retour += "}); "
                + ""
                + "</script>";
        return retour;
    }

      
      
      
    public void makeHtmlInsertTableauIndex() throws Exception {
        String temp = "";
        String acstring = "";
        temp += "<div class='row'>";
        temp += "<div class='row col-md-12'>";
        temp += "<div class='box'>";
        temp += "<div class='box box-primary'>";
        temp += "<div class='box-body'>";
        temp += "<input id='nbrLigne' name='nbrLigne' type='hidden' value='" + getNbLigne() + "'/>";
        temp += "<input name='indexMultiple' id='indexMultiple' type='hidden' value='" + getNbLigne() + "'/>";
        temp += "<table class='table table-bordered'>";
        temp += "<tr>";
        
        /*for (int i = 0; i < getListeChamp().length; i++) {
            Champ c = getListeChamp()[i];
            if (c.getVisible() == true) {
                c.setAutre("tabindex='" + (i + 1) + "'" + c.getAutre());
                temp += "<th style='background-color:#bed1dd' colspan='2'><label for='" + getListeChamp()[i].getLibelle() + "' " + c.getAutre() + ">" + getListeChamp()[i].getLibelle() + "</label></th>";

            }
        }*/
        
        bean.Champ[] champfle = bean.ListeColonneTable.getFromListe(getObjet(), null);
        for (int i = 0; i < champfle.length; i++) {
            Champ c = this.getChamp(champfle[i].getNomColonne()+"_"+0);
            if(i == 0)
                temp += "<th style='background-color:#bed1dd' colspan='1'></th>";
            if (c.getVisible() == true) {
                c.setAutre("tabindex='" + (i + 1) + "'" + c.getAutre());
                temp += "<th style='background-color:#bed1dd' colspan='1'><label for='" + c.getNom() + "' " + c.getAutre() + ">" + c.getLibelle() + "</label></th>";
            }
        }
        
        temp += "</tr>";
        temp += "<tbody id='ajout_multiple_ligne'>";
        //System.out.println("getNbLigne() =========================== " + getNbLigne());
        for (int iLigne = 0; iLigne < getNbLigne(); iLigne++) {
            acstring += getHtmlAutocomplete(iLigne);
            String troisiemeTd = "";
            temp += "<tr id='ligne-multiple-" + iLigne + "'>";
            temp += "<td align=center><input type='checkbox' value='" + iLigne + "' name='id' id='checkbox" + iLigne + "'/></td>";
            //System.out.println("**********************"+getListeChamp().length);
            //bean.Champ[] champfle = bean.ListeColonneTable.getFromListe(getObjet(), null);
            for (int iCol = 0; iCol < champfle.length; iCol++) {
                //Champ ch = getListeChamp()[iCol];
                //System.out.println("***-------------"+champfle[iCol].getNomColonne()+"_"+iLigne);
                Champ ch = this.getChamp(champfle[iCol].getNomColonne()+"_"+iLigne);
                if (ch.getVisible() == true)
                    temp += "<td>" ;
                    //temp += ch.getHtmlTableauInsert(iLigne); 
                //if (ch.getVisible() == true)
                    //temp += "</td>";
                    /*if (ch.getPageAppel() != null && ch.getPageAppel().compareToIgnoreCase("") != 0) {
                        troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + ch.getPageAppel() + "?champReturn=" + ch.getNom() + "_" + iLigne + "') value=... />";
                        temp += "<td>" + troisiemeTd + "</td>";
                    } */
                    if (ch.getPageAppel() != null && ch.getPageAppel().compareToIgnoreCase("") != 0) {
                        temp += ch.getInpuTabtHidden(iLigne);
                        troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + ch.getPageAppel() + "?champReturn=" + ch.getChampReturn(iLigne) + "&apresLienPageAppel=" + ch.getApresLienPageappel() + "') value=... />";
                        //temp += "<td>" + troisiemeTd + "</td>";
                        temp += troisiemeTd;
                    }else{
                        temp += ch.getHtmlTableauInsert(iLigne); 
                    } 
                    if (ch.getTypeData() != null && ch.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeDaty) == 0) {
                        troisiemeTd = "<a href='javascript:cal.popup()'><img src='calendar/img/cal.gif' alt='Cliquez ici pour choisir une date' width=16 height=16 border=0 align='absmiddle' /></a>";
                        //temp += "<td>" + troisiemeTd + "</td>";
                        temp += troisiemeTd;
                        //temp += "<td></td>";
                    } /*else {
                        temp += "<td></td>";
                    }*/
                temp += "</td>" ;
                //}
            }
            temp += "<td><a onclick='removeLineByIndex(" + iLigne + ")'><span class='glyphicon glyphicon-remove'></span></a></td>";
            temp += "</tr>";
        }
        temp += "</tbody>";
        temp += "</table>";
        temp += "</div>";
        temp += "<div class='box-footer'>";

        temp += "<div class='col-xs-12'>";
        Gson gson = new Gson();
        String json = gson.toJson(getListeChamp());
        temp += "<button type='submit' name='Submit2' class='btn btn-success pull-right' style='margin-right: 25px;' tabIndex='" + getListeChamp().length + 1 + "'>Enregistrer</button> ";
        temp += "<button type='reset' name='Submit2' class='btn btn-default pull-right' style='margin-right: 15px;' tabIndex='" + getListeChamp().length + 2 + "'>R&eacute;initialiser</button>";
        
        temp += "<button type='button' class='btn btn-default pull-right' onclick='add_line_tab(JSON.stringify("+json+"))' style='margin-right: 15px;'>Ajouter une ligne</button>";
        //temp += "<button type='button' class='btn btn-default pull-right' onclick='add_line()' style='margin-right: 15px;'>Ajouter une ligne</button>";
        //temp += "<button type='reset' name='Submit2' class='btn btn-default pull-right' style='margin-right: 15px;' tabIndex='" + getListeChamp().length + 2 + "'>R&eacute;initialiser</button>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += acstring;
        
        setHtmlTableauInsert(temp);
    }
 /* public void makeHtmlInsertTableauIndex() throws Exception {
        String temp = "";
        String acstring = "";
        temp += "<div class='row'>";
        temp += "<div class='row col-md-12'>";
        temp += "<div class='box'>";
        temp += "<div class='box box-primary'>";
        temp += "<div class='box-body'>";
        temp += "<input id='nbrLigne' name='nbrLigne' type='hidden' value='" + getNbLigne() + "'/>";
        temp += "<input name='indexMultiple' id='indexMultiple' type='hidden' value='" + getNbLigne() + "'/>";
        temp += "<table class='table table-bordered'>";
        temp += "<tr>";
        for (int i = 0; i < getListeChamp().length; i++) {
            Champ c = getListeChamp()[i];
            if (c.getVisible() == true) {
                c.setAutre("tabindex='" + (i + 1) + "'" + c.getAutre());
                temp += "<th style='background-color:#bed1dd' colspan='2'><label for='" + getListeChamp()[i].getLibelle() + "' " + c.getAutre() + ">" + getListeChamp()[i].getLibelle() + "</label></th>";

            }
        }
        temp += "</tr>";
        temp += "<tbody id='ajout_multiple_ligne'>";
        //System.out.println("getNbLigne() =========================== " + getNbLigne());
        for (int iLigne = 0; iLigne < getNbLigne(); iLigne++) {
            acstring += getHtmlAutocomplete(iLigne);
            String troisiemeTd = "";
            temp += "<tr id='ligne-multiple-" + iLigne + "'>";
            temp += "<td align=center><input type='checkbox' value='" + iLigne + "' name='id' id='checkbox" + iLigne + "'/></td>";
            for (int iCol = 0; iCol < getListeChamp().length; iCol++) {
                Champ ch = getListeChamp()[iCol];
                if (ch.getVisible() == true) {
                    temp += "<td>" + ch.getHtmlTableauInsert(iLigne) + "</td>";
                    if (ch.getPageAppel() != null && ch.getPageAppel().compareToIgnoreCase("") != 0) {
                        troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + ch.getPageAppel() + "?champReturn=" + ch.getNom() + "_" + iLigne + "') value=... />";
                        temp += "<td>" + troisiemeTd + "</td>";
                    } else if (ch.getTypeData() != null && ch.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeDaty) == 0) {
                        troisiemeTd = "<a href='javascript:cal.popup()'><img src='calendar/img/cal.gif' alt='Cliquez ici pour choisir une date' width=16 height=16 border=0 align='absmiddle' /></a>";
                        temp += "<td>" + troisiemeTd + "</td>";
                        temp += "<td></td>";
                    } else {
                        temp += "<td></td>";
                    }
                }
            }
            temp += "<td><a onclick='removeLineByIndex(" + iLigne + ")'><span class='glyphicon glyphicon-remove'></span></a></td>";
            temp += "</tr>";
        }
        temp += "</tbody>";
        temp += "</table>";
        temp += "</div>";
        temp += "<div class='box-footer'>";

        temp += "<div class='col-xs-12'>";
        Gson gson = new Gson();
        String json = gson.toJson(getListeChamp());
        temp += "<button type='submit' name='Submit2' class='btn btn-success pull-right' style='margin-right: 25px;' tabIndex='" + getListeChamp().length + 1 + "'>Enregistrer</button> ";
        temp += "<button type='reset' name='Submit2' class='btn btn-default pull-right' style='margin-right: 15px;' tabIndex='" + getListeChamp().length + 2 + "'>R&eacute;initialiser</button>";
        
        temp += "<button type='button' class='btn btn-default pull-right' onclick='add_line_tab(JSON.stringify("+json+"))' style='margin-right: 15px;'>Ajouter une ligne</button>";
        //temp += "<button type='button' class='btn btn-default pull-right' onclick='add_line()' style='margin-right: 15px;'>Ajouter une ligne</button>";
        //temp += "<button type='reset' name='Submit2' class='btn btn-default pull-right' style='margin-right: 15px;' tabIndex='" + getListeChamp().length + 2 + "'>R&eacute;initialiser</button>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += "</div>";
        temp += acstring;
        
        setHtmlTableauInsert(temp);
    }*/
  
    public String getBoutonsValiderAnnulerTabIndex() {
        String temp = "";
        temp += "<input type='submit' name='Submit2' value='valider' class='submit' tabIndex='" + getListeChamp().length + 1 + "'>";
        temp += "<input type='reset' name='Submit2' value='Annuler' class='submit' tabIndex='" + getListeChamp().length + 2 + "'>";
        return temp;
    }

    public String getListeCritereString() {
        String retour = "";
        for (int i = 0; i < getListeChamp().length; i++) {
            Champ temp = getListeChamp()[i];
            String valeur = temp.getValeur();
            if (temp.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeTexte) == 0) {
                valeur = utilitaire.Utilitaire.remplacePourcentage(valeur);
            }
            retour += "&" + temp.getNom() + "=" + valeur;
        }
        if (getChampGroupe() != null) {
            for (int j = 0; j < getChampGroupe().length; j++) {
                Champ temp = getChampGroupe()[j];
                String valeur = temp.getValeur();
                if (temp.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeTexte) == 0) {
                    valeur = utilitaire.Utilitaire.remplacePourcentage(valeur);
                }
                retour += "&" + temp.getNom() + "=" + valeur;
            }
        }
        if (getChampTableauAff() != null) {
            for (int j = 0; j < getChampTableauAff().length; j++) {
                Champ temp = getChampTableauAff()[j];
                String valeur = temp.getValeur();
                if (temp.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeTexte) == 0) {
                    valeur = utilitaire.Utilitaire.remplacePourcentage(valeur);
                }
                retour += "&" + temp.getNom() + "=" + valeur;
            }
        }
        return retour;
    }

    public void getAllData(Connection c) throws Exception {
        
        for (int i = 0; i < getListeChamp().length; i++) {
            getListeChamp()[i].findData(c);
        }
    }

    public Champ[] getListeChamp() {
        return listeChamp;
    }

    public void setListeChamp(Champ[] listeChamp) {
        this.listeChamp = listeChamp;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNbRangee(int nbRangee) {
        this.nbRangee = nbRangee;
    }

    public int getNbRangee() {
        return nbRangee;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getHtml() throws Exception {
        if (html == null || html.compareToIgnoreCase("") == 0) {
            this.makeHtml();
        }
        return html;
    }

    public void setTailleTableau(String tailleTableau) {
        this.tailleTableau = tailleTableau;
    }

    public String getTailleTableau() {
        return tailleTableau;
    }

    public void setCssTableau(String cssTableau) {
        this.cssTableau = cssTableau;
    }

    public String getCssTableau() {
        return cssTableau;
    }

    public void setObjet(bean.ClassMAPTable objet) {
        this.objet = objet;
    }

    public bean.ClassMAPTable getObjet() {
        return objet;
    }

    public void setCrtFormu(Champ[] crtFormu) {
        this.crtFormu = crtFormu;
    }

    public Champ[] getCrtFormu() {
        return crtFormu;
    }

    public void setNbIntervalle(int nbIntervalle) {
        this.nbIntervalle = nbIntervalle;
    }

    public int getNbIntervalle() {
        return nbIntervalle;
    }

    public void setHtmlTri(String htmlTri) {
        this.htmlTri = htmlTri;
    }

    public String getHtmlTri() throws Exception {
        if (htmlTri == null || htmlTri.compareToIgnoreCase("") == 0) {
            this.makeHtmlTri();
        }
        return htmlTri;
    }

    public void setHtmlButton(String htmlButton) {
        this.htmlButton = htmlButton;
    }

    public String getHtmlButton() {
        return htmlButton;
    }

    public void setHtmlInsert(String htmlInsert) {
        this.htmlInsert = htmlInsert;
    }

    public String getHtmlInsert() throws Exception {
        if (htmlInsert == null || htmlInsert.compareToIgnoreCase("") == 0) {
            makeHtmlInsert();
        }
        return htmlInsert;
    }

    
    public String getHtmlAdd() throws Exception {
        if (htmlInsert == null || htmlInsert.compareToIgnoreCase("") == 0) {
            makeHtmlInsert();
        }
        return htmlInsert;
    }

    public String getHtmlTableauInsert() throws Exception {
        if (htmlTableauInsert == null || htmlTableauInsert.compareToIgnoreCase("") == 0) {
            makeHtmlInsertTableauIndex();
        }
        return htmlTableauInsert;
    }

    public void setHtmlTableauInsert(String htmlTableauInsert) {
        this.htmlTableauInsert = htmlTableauInsert;
    }

    public void setChampGroupe(Champ[] champGroupe) {
        this.champGroupe = champGroupe;
    }

    public Champ[] getChampGroupe() {
        return champGroupe;
    }

    public void setHtmlGroupe(String htmlGroupe) {
        this.htmlGroupe = htmlGroupe;
    }

    public String getHtmlGroupe() {
        return htmlGroupe;
    }

    public Champ[] getChampTableauAff() {
        return champTableauAff;
    }

    public void setChampTableauAff(Champ[] champTableauAff) {
        this.champTableauAff = champTableauAff;
    }

    public String getHtmlTableauAff() {
        return htmlTableauAff;
    }

    public void setHtmlTableauAff(String htmlTableauAff) {
        this.htmlTableauAff = htmlTableauAff;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public String getCssClass() {
        return cssClass;
    }
    
    public String getHtmlEtatFiltre(String table, String[] texte, String[] vue) throws Exception {
        String temp = "";
        if(texte.length != vue.length){
            throw new Exception("Valeur invalide");
        }
        if(table == null && table.equals("")) table = "table";
        temp += "<div class=\"row col-md-12\">";
        temp += "<div class=\"col-md-4\"></div>";
        temp += "<div class=\"col-md-4\">";
        temp += "<select name="+table+" class=\"champ\" id="+table+" onchange=\"changerDesignation()\" >";
        for(int i=0; i<texte.length; i++){
            if (table != null && table.compareToIgnoreCase(vue[i]) == 0) { 
                temp += "<option value="+vue[i]+" selected>"+texte[i]+"</option>";
            } else {
                temp += "<option value="+vue[i]+" >"+texte[i]+"</option>";
            } 
        }
        temp += "</select>";
        temp += "</div>";
        return temp;
    }
      public String makeHtmlUpdateTableau(String libEntete, String valeur, int iLigne) throws Exception {
        String temp = "";
        String troisiemeTd = "";
        
        //for (int iCol = 0; iCol < getListeChamp().length; iCol++) 
        {
            Champ ch=this.getChamp(libEntete);
            //Champ ch = getListeChamp()[iCol];
            if (ch!=null && ch.getVisible() == true) 
            {
                //if(libEntete.equals(getListeChamp()[iCol].getNom()))
                {
                    temp += "<td>" + ch.getHtmlTableauInsert(iLigne, valeur) + "</td>";
                    if (ch.getPageAppel() != null && ch.getPageAppel().compareToIgnoreCase("") != 0) {
                        troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + ch.getPageAppel() + "?champReturn=" + ch.getNom() + "_" + iLigne + "') value=... />";
                        temp += "<td>" + troisiemeTd + "</td>";
                    }
                }
                
                /*if (ch.getPageAppel() != null && ch.getPageAppel().compareToIgnoreCase("") != 0) {
                    troisiemeTd = "<input name='choix' type='button' class='submit' onclick=pagePopUp('" + ch.getPageAppel() + "?champReturn=" + ch.getNom() + "_" + iLigne + "') value=... />";
                    temp += "<td>" + troisiemeTd + "</td>";
                } else if (ch.getTypeData() != null && ch.getTypeData().compareToIgnoreCase(ConstanteAffichage.typeDaty) == 0) {
                    troisiemeTd = "<a href='javascript:cal.popup()'><img src='calendar/img/cal.gif' alt='Cliquez ici pour choisir une date' width=16 height=16 border=0 align='absmiddle' /></a>";
                    temp += "<td>" + troisiemeTd + "</td>";
                    temp += "<td></td>";
                } else {
                    temp += "<td></td>";
                }*/
            }
        }
        return temp;
    }
      
    public Champ[] getChampFille(String colonne) throws Exception{
        Champ[] tChamp = new Champ[getNbLigne()];
        int iV = 0;
        bean.Champ[] champfle = bean.ListeColonneTable.getFromListe(getObjet(), null);
        for(int ligne =0; ligne<getNbLigne(); ligne++){
            for(int i =0; i<champfle.length; i++){
                if((colonne).compareToIgnoreCase(champfle[i].getNomColonne()) == 0){
                    tChamp[iV] = this.getChamp(colonne+"_"+ligne);
                    iV ++;
                }
            }
        }
        return tChamp;
    }
}
