package affichage;

/**
 * <p>Title: Gestion des recettes </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class ConstanteAffichage {
  public static String typeTextBox = "textbox";
  public static String textarea = "textarea";
  public static String checkbox = "checkbox";
  public static String radio = "radio";
  public static String asterisque="%";
  public static String typeDaty="date";
  public static String typeTexte="texte";
  public static int propSupr=3;
  public static String[] format={"3x3","7x3","7x6","11x6","1/8","1/4","1/3","1/2","1","encartage","titre"};
  //public static String typeFenetre="date";
  public ConstanteAffichage() {
  }
}