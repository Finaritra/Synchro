/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package affichage;

import bean.CGenUtil;
import bean.ClassMAPTable;
import java.lang.reflect.Field;
import javax.servlet.http.HttpServletRequest;
import utilitaire.Utilitaire;

/**
 *
 * @author Joe
 */
public class PageConsulteMultiple extends Page {

    private Champ[] listeChamp;
    private Champ[][] listeChampfle;
    private bean.ClassMAPTable critere;
    private bean.ClassMAPTable criterefille;
    private bean.ClassMAPTable[] listeFille;
    private String[] libAffichage;
    private String[] libAffichagefille;
    private String cssTableau = "monographe";
    private String apres;
    private String apresLienPage = "";
    private String donnee;
    private String bute = "";
    private String titre;
    private boolean canUploadFile = false;
    private String fichier_table_cible;
    private String fichier_table_procedure;
    private String nomClasseFilleADuplique = "";
    private String nomColonneMere = "";

    public String getBute() {
        return bute;
    }

    public String[] getLibAffichagefille() {
        return libAffichagefille;
    }

    public void setLibAffichagefille(String[] libAffichagefille) {
        //this.libAffichagefille = libAffichagefille;
        if (getListeChampfle() != null && getListeChampfle().length > 0 && getListeChampfle()[0].length > 0) {
            for (int i = 0; i < getListeChampfle()[0].length; i++) {
                // System.out.println(" miditra  ==== " + i);
                Champ c = getListeChampfle()[0][i];
                // System.out.println(" miditra  ==== " + c.getNom());
                c.setLibelle(libAffichagefille[i]);
            }
        }
    }

    public void setBute(String bute) {
        this.bute = bute;
    }

    public Champ[][] getListeChampfle() {
        return listeChampfle;
    }

    public void setListeChampfle(Champ[][] listeChampfle) {
        this.listeChampfle = listeChampfle;
    }

    public ClassMAPTable[] getListeFille() {
        return listeFille;
    }

    public void setListeFille(ClassMAPTable[] listeFille) {
        this.listeFille = listeFille;
    }

    public Champ[] getListeChamp() {
        return listeChamp;
    }

    public void setListeChamp(Champ[] listeChamp) {
        this.listeChamp = listeChamp;
    }

    public ClassMAPTable getCritere() {
        return critere;
    }

    public void setCritere(ClassMAPTable critere) {
        this.critere = critere;
    }

    public ClassMAPTable getCriterefille() {
        return criterefille;
    }

    public void setCriterefille(ClassMAPTable criterefille) {
        this.criterefille = criterefille;
    }

    public String[] getLibAffichage() {
        return libAffichage;
    }

    public void setLibAffichage(String[] libAffichage) {
        //this.libAffichage = libAffichage;
        for (int i = 0; i < libAffichage.length; i++) {
            Champ c = getListeChamp()[i];
            c.setLibelle(libAffichage[i]);
        }
    }

    public String getApresLienPage() {
        return apresLienPage;
    }

    public void setApresLienPage(String apresLienPage) {
        this.apresLienPage = apresLienPage;
    }

    public PageConsulteMultiple(ClassMAPTable o, ClassMAPTable fille, String nomColMere, HttpServletRequest req, user.UserEJB u) throws Exception {
        try {
            setCritere(o);

            setCriterefille(fille);
            setNomColonneMere(nomColMere);
            setReq(req);
            makeCritere();
            setUtilisateur(u);
            getData();
            makeChamp();
            //System.out.print(" mandalo make makeChamp ==== " );
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    public void getData() throws Exception {
        ClassMAPTable[] result = (ClassMAPTable[]) getUtilisateur().getData(getCritere(), null, null, null, "");
        //System.out.println(" and  + getNomColonneMere() " + " and " + getNomColonneMere()+ " = '" + getCritere().getTuppleID() + "'" + " getCriterefille() == " + getCriterefille().getClass().getName());
        ClassMAPTable[] resultfille = (ClassMAPTable[]) getUtilisateur().getData(getCriterefille(), null, null, null, " and " + getNomColonneMere() + " = '" + getCritere().getTuppleID() + "'");
        if (result == null || result.length == 0) {
            throw new Exception("Pas de resultat pour votre consultation");
        }
        setBase(result[0]);
        setListeFille(resultfille);
    }

    public void makeChamp() throws Exception {
        try {
            Champ[] listeChamp = null;
            Champ[][] listeChampfle = null;

            bean.Champ[] t = bean.ListeColonneTable.getFromListe(getBase(), null);
            listeChamp = new Champ[t.length];
            //Field[] f = getBase().getFieldList();
            for (int i = 0; i < t.length; i++) {
                listeChamp[i] = new Champ(t[i].getNomColonne());
                listeChamp[i].setLibelle(t[i].getNomColonne());
                Object tempVal = CGenUtil.getValeurFieldByMethod(getBase(), t[i].getNomColonne());
                String valeur = String.valueOf(tempVal);
                if (valeur != null && valeur.contains("'")) {
                    valeur = valeur.replaceAll("'", "&apos;");
                }
                if (valeur != null && valeur.contains("\"")) {
                    valeur = valeur.replaceAll("\"", " ");
                }
                if (tempVal == null) {
                    valeur = "";
                } else if (t[i].getTypeJava().compareToIgnoreCase("java.sql.Date") == 0) {
                    valeur = utilitaire.Utilitaire.formatterDaty(valeur);
                } else if (t[i].getTypeJava().compareToIgnoreCase("double") == 0) {
                    valeur = utilitaire.Utilitaire.formaterAr(valeur);
                }
                listeChamp[i].setValeur(valeur);
            }
            setListeChamp(listeChamp);

            bean.Champ[] tfle = bean.ListeColonneTable.getFromListe(getCriterefille(), null);
            
            //Field[] f = getBase().getFieldList();
            int tmpIndice = 0;
            System.out.println(" listeFille.length ========== " + listeFille.length + " tfle.length ======= " + tfle.length);
            if(listeFille.length > 0){
                listeChampfle = new Champ[listeFille.length][tfle.length];
                for (int j = 0; j < listeFille.length; j++) {
                    listeChampfle[j] = new Champ[tfle.length];
                    for (int i = 0; i < tfle.length; i++) {
                        System.out.println(" i =========== " + i + " j ===== " + j + " + tfle[i].getNomColonne() == " + tfle[i].getNomColonne());
                        listeChampfle[j][i] = new Champ(tfle[i].getNomColonne());
                        listeChampfle[j][i].setLibelle(tfle[i].getNomColonne());
                        Object tempVal = CGenUtil.getValeurFieldByMethod(listeFille[j], tfle[i].getNomColonne());
                        String valeur = String.valueOf(tempVal);

                        if (valeur != null && valeur.contains("'")) {
                            valeur = valeur.replaceAll("'", "&apos;");
                        }
                        if (valeur != null && valeur.contains("\"")) {
                            valeur = valeur.replaceAll("\"", " ");
                        }
                        if (tempVal == null) {
                            valeur = "";
                        } else if (tfle[i].getTypeJava().compareToIgnoreCase("java.sql.Date") == 0) {
                            valeur = utilitaire.Utilitaire.formatterDaty(valeur);
                        } else if (tfle[i].getTypeJava().compareToIgnoreCase("double") == 0) {
                            valeur = utilitaire.Utilitaire.formaterAr(valeur);
                        }
                        listeChampfle[j][i].setValeur(valeur);
                        tmpIndice++;
                    }
                }
            } else {
                listeChampfle = new Champ[1][tfle.length];
                for (int j = 0; j < 1; j++) {
                    listeChampfle[j] = new Champ[tfle.length];
                    for (int i = 0; i < tfle.length; i++) {
                        listeChampfle[j][i] = new Champ(tfle[i].getNomColonne());
                        listeChampfle[j][i].setLibelle(tfle[i].getNomColonne());
                        tmpIndice++;
                    }
                }
            }
            setListeChampfle(listeChampfle);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    public void makeCritere() throws Exception {
        String valeur = getReq().getParameter(getCritere().getAttributIDName());
        Field f = CGenUtil.getField(getCritere(), getCritere().getAttributIDName());
        CGenUtil.setValChamp(getCritere(), f, valeur);
    }

    public void makeHtml() {
        try {
            java.util.Properties prop = configuration.CynthiaConf.load();
            String temp = "";
            Class[] paramType = {int.class};
            temp += "<div class='row'>";
            temp += "<div class='col-md-6'>"
                    + "<div class='box-fiche'>"
                    + "<div class='box'>"
                    + "<div class='box-title with-border'>"
                    + "<h1 class='box-title'><a onclick='history.back()'><i class='fa fa-arrow-circle-left'></i></a>" + getTitre() + " " + getCritere().getTuppleID() + "</h1></div><div class='box-body'>";
            temp += "<table class='table table-bordered'>";

        //System.out.print(" mandalo make html ==== " + getListeChamp().length);
            for (int i = 0; i < getListeChamp().length; i++) {
                Champ champ = getListeChamp()[i];
                String lienDebut = champ.getLien() == null ? "" : " <a href='" + champ.getLien().getLien() + "&" + champ.getLien().getQueryString() + getListeChamp()[i].getValeur() + "'>";
                //System.out.println("lienDebut ==== " + lienDebut);
                String lienFin = champ.getLien() == null ? "" : "</a>";
                if (getListeChamp()[i].getTypeData().compareToIgnoreCase("date") == 0) {
                }
                if (getListeChamp()[i].isPhoto()) {
                    temp += "<tr>";
                    temp += "<td class='" + getListeChamp()[i].getCssLibelle() + "'><B>" + getListeChamp()[i].getLibelle() + "</B></td>";
                    temp += "<td>" + lienDebut + "<img src='" + prop.getProperty("cdnReadUri") + getListeChamp()[i].getValeur() + "' alt='" + getListeChamp()[i].getValeur() + "' style='width:100%;'/>" + lienFin + " </td>";
                    temp += "</tr>";
                } else if (getListeChamp()[i].getVisible() == true) {
                    temp += "<tr>";
                    temp += "<td class='" + getListeChamp()[i].getCssLibelle() + "'><B>" + getListeChamp()[i].getLibelle() + "</B></td>";
                    //if()System.out.println(" instance classe etat page consulte");
                    if (getListeChamp()[i].getNom().compareToIgnoreCase("etat") == 0 && getBase() instanceof bean.ClassEtat) { // 
                        temp += "<td>" + lienDebut + "<b>" + ((bean.ClassEtat) getBase()).getEtatText(Utilitaire.stringToInt(getListeChamp()[i].getValeur())) + "</b>" + lienFin + "</td>";
                    } else {
                        temp += "<td>" + lienDebut + getListeChamp()[i].getValeur() + lienFin + "</td>";
                    }
                    temp += "</tr>";
                }
            }
            temp += "<input type='hidden' name='id' value='" + getListeChamp()[0].getValeur() + "'/>";

            temp += "</table>";
            temp += "<br/></div></div></div></div></div>";

            /* ============================== fiche fille ================================== */
            if (getListeChampfle() != null && getListeChampfle().length > 0) {
                temp += "<div class='row'>";
                temp += "<div class='row col-md-12'>";
                temp += "<div class='box'>";
                temp += "<div class='box box-primary'>";
                temp += "<div class='box-body'>";
                temp += "<table class=\"table table-bordered\">";
                temp += "<tr>";

                for (int i = 0; i < getListeChampfle()[0].length; i++) {
                    Champ c = getListeChampfle()[0][i];
                    if (c.getNom().compareToIgnoreCase("etat") == 0 && getCriterefille() instanceof bean.ClassEtat) {

                    } else if (c.getVisible() == true) {
                        c.setAutre("tabindex='" + (i + 1) + "'" + c.getAutre());
                        temp += "<th style='background-color:#bed1dd' ><label for='" + getListeChampfle()[0][i].getLibelle() + "' " + c.getAutre() + ">" + getListeChampfle()[0][i].getLibelle() + "</label></th>";

                    }
                }
                temp += "</tr>";
                //System.out.println("getNbLigne() =========================== " + getListeFille());
                for (int iLigne = 0; iLigne < getListeFille().length; iLigne++) {
                    String troisiemeTd = "";
                    temp += "<tr>";
                    for (int iCol = 0; iCol < getListeChampfle()[iLigne].length; iCol++) {
                        Champ ch = getListeChampfle()[iLigne][iCol];
                        String lienDebut = ch.getLien() == null ? "" : " <a href='" + ch.getLien().getLien() + "&" + ch.getLien().getQueryString() + ch.getValeur() + "'>";
                        //System.out.println("ch.getVisible() ==== " + ch.getVisible() + " ch.getName() " + ch.getNom());
                        String lienFin = ch.getLien() == null ? "" : "</a>";
                        if (ch.getNom().compareToIgnoreCase("etat") == 0 && getCriterefille() instanceof bean.ClassEtat) {

                        } else if (ch.isPhoto()) {
                            temp += "<td>" + lienDebut + "<img src='" + prop.getProperty("cdnReadUri") + ch.getValeur() + "' alt='" + ch.getValeur() + "' style='width:100%;'/>" + lienFin + " </td>";

                        } else if (getListeChampfle()[0][iCol].getVisible() == true) {
                            if (ch.getNom().compareToIgnoreCase("etat") == 0 && getCriterefille() instanceof bean.ClassEtat) { // 
                                temp += "<td>" + lienDebut + "<b>" + ((bean.ClassEtat) getCriterefille()).getEtatText(Utilitaire.stringToInt(ch.getValeur())) + "</b>" + lienFin + "</td>";
                            } else {
                                temp += "<td>" + lienDebut + ch.getValeur() + lienFin + "</td>";
                            }

                        }
                    }
                    temp += "<td><a href='" + getLien() + "?but=apresInsertMultiple.jsp&idmultiple=" + getListeFille()[iLigne].getTuppleID() + "&acte=deleteMultiple&classe=" + getCriterefille().getClass().getName() + "&idmere=" + getBase().getTuppleID() + "&bute=" + getBute() + "'><span class='glyphicon glyphicon-remove'></span></a></td>";
                    temp += "</tr>";
                }
                temp += "</table>";
                temp += "</div>";
                temp += "<div class='box-footer'>";

                temp += "</div>";
                temp += "</div>";
                temp += "</div>";
                temp += "</div>";
                temp += "</div>";
            }
            setHtml(temp);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getNomColonneMere() {
        return nomColonneMere;
    }

    public void setNomColonneMere(String nomColonneMere) {
        this.nomColonneMere = nomColonneMere;
    }

    public String getNomClasseFilleADuplique() {
        return nomClasseFilleADuplique;
    }

    public void setNomClasseFilleADuplique(String nomClasseFilleADuplique) {
        this.nomClasseFilleADuplique = nomClasseFilleADuplique;
    }

    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public PageConsulteMultiple() {
    }

    public boolean isCanUploadFile() {
        return canUploadFile;
    }

    public void setCanUploadFile(boolean canUploadFile) {
        this.canUploadFile = canUploadFile;
    }

    public String getFichier_table_cible() {
        return fichier_table_cible;
    }

    public void setFichier_table_cible(String fichier_table_cible) {
        this.fichier_table_cible = fichier_table_cible;
    }

    public String getFichier_table_procedure() {
        return fichier_table_procedure;
    }

    public void setFichier_table_procedure(String fichier_table_procedure) {
        this.fichier_table_procedure = fichier_table_procedure;
    }

    public Champ getChampByName(String nomChamp) {
        for (int i = 0; i < getListeChamp().length; i++) {
            if (getListeChamp()[i].getNom().compareToIgnoreCase(nomChamp) == 0) {
                return getListeChamp()[i];
            }
        }
        return null;
    }

    public Champ getChampFilleByName(String nomChamp) {
        //if (getListeChampfle() != null && getListeChampfle().length > 0) {
            for (int i = 0; i < getListeChampfle()[0].length; i++) {
                System.out.println(" getListeChampfle()[0][i].getNom() " + getListeChampfle()[0][i].getNom() + " getListeChampfle().length " + getListeChampfle().length);
                if (getListeChampfle()[0][i].getNom().compareToIgnoreCase(nomChamp) == 0) {
                    return getListeChampfle()[0][i];
                }
            }
        //}
        return null;
    }
}
