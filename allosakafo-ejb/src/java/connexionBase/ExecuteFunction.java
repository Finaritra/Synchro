package connexionBase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;
import utilitaire.UtilDB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Mahefa
 */
public class ExecuteFunction{

    /*Fonction to date et fonction to date time*/
    public static String to_dateTime(String dateTime) throws Exception{
        String result = new String();
        if(dateTime!=null || dateTime.compareTo("null")!=0){
            result = "null";
        }else result = "TO_DATE('"+dateTime+"','YYYY-MM-DD HH24:MI:SS')";
        return result;
    }
    public static String to_date(String date) throws Exception{
        String result = new String();
        if(date==null || date.compareTo("null")==0){
            result = "null";
        }else result = "TO_DATE('"+date+"','YYYY-MM-DD')";
        return result;
    }
    
	public static String to_dateBDD(String date) throws Exception{
        String result = new String();
        String[] dateBdd = date.split(" ");
        if(dateBdd.length>1){
            result = dateBdd[0];
        }else result = date;
        return result;
    }
	
    public static JSONArray resultJSON(String req) throws Exception {
        
        ResultSet resultSet = null;
        Statement requete = null;
        JSONArray resultatJSON = null;
        //Connexion a la base de donnees
        Connection newConnection = null;
		try {
            newConnection = new UtilDB().GetConn();
            newConnection.setAutoCommit(false);
            requete = newConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultSet = requete.executeQuery(req);
            resultatJSON = convertToJSON(resultSet);
            newConnection.commit();
            return resultatJSON;
        } catch (Exception e) {
            newConnection.rollback();
            throw new Exception(e);
        }
        finally  {        
            if (resultSet != null) {
		resultSet.close();
	    }
            if (newConnection != null) {
		newConnection.close();
	    }
        }
        
    }
    
    public static JSONArray resultJSONChamp(String table) throws Exception {
        String req = "select column_name,data_type,data_length,data_precision FROM user_tab_columns WHERE upper(table_name) = upper('" + table + "') order by column_id asc";
        ResultSet resultSet = null;
        Statement requete = null;
        JSONArray resultatJSON = null;
        //Connexion a la base de donnees
        Connection newConnection = null;
		try {
            newConnection = new UtilDB().GetConn();
            newConnection.setAutoCommit(false);
            requete = newConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultSet = requete.executeQuery(req);
            resultatJSON = convertToJSON(resultSet);
            newConnection.commit();
            return resultatJSON;
        } catch (Exception e) {
            newConnection.rollback();
            throw new Exception(e);
        }
        finally  {        
            if (resultSet != null) {
		resultSet.close();
	    }
            if (newConnection != null) {
		newConnection.close();
	    }
        }
        
    }
	
	public static String as_executeQuery(String req) throws Exception{
		ResultSet resultSet = null;
        Statement requete = null;
		String resultatFinal = null;
        //Connexion a la base de donnees
        Connection newConnection = null;
		try {
            newConnection = new UtilDB().GetConn();
            newConnection.setAutoCommit(false);
            requete = newConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultSet = requete.executeQuery(req);
            resultatFinal = "ok";
            return resultatFinal;
        } catch (Exception e) {
            newConnection.rollback();
            throw new Exception(e);
        }
        finally  {        
            if (resultSet != null) {
		resultSet.close();
	    }
            if (newConnection != null) {
		newConnection.close();
	    }
        }
	} 

    public static JSONArray convertToJSON(ResultSet resultSet) throws Exception {
        JSONArray jsonArray = new JSONArray();
        int total_rows = 0;
        while (resultSet.next()) {
            total_rows = resultSet.getMetaData().getColumnCount();
            JSONObject obj = new JSONObject();
            for (int i = 0; i < total_rows; i++) {
                obj.put(resultSet.getMetaData().getColumnLabel(i + 1).toLowerCase(), resultSet.getObject(i + 1));
                if(resultSet.getObject(i + 1)==null){
                    obj.put(resultSet.getMetaData().getColumnLabel(i + 1).toLowerCase(),"null");
                }
            }
            jsonArray.put(obj);
        }
        return jsonArray;
    }

    
    public static JSONArray select_action_tableFieldParameter(String table, String field, String parameter)throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM "+table+" WHERE "+field+"='"+parameter+"'";
        System.out.println(req);
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    public static JSONArray select_all_action(String table) throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM "+table;
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    public static JSONArray select_SommeProduitAchat_critere_limite(String etat, String ordre, int limite) throws Exception{
        limite = limite +1 ;
        JSONArray result = null;
        String req = "SELECT * FROM (SELECT * FROM AS_V_SOMME_PRODUIT_VENTE_A WHERE ETAT='"+etat+"' ORDER BY NBRCOMMANDE "+ordre+") WHERE ROWNUM <"+limite;
        try {
            
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    public static JSONArray select_list_order_by(String table, String champ, String ordre) throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM "+table+" ORDER BY "+champ+" "+ordre;
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    public static JSONArray select_list_all(String table) throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM "+table;
        try {
            
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    public static JSONArray select_list_order_by_limit(String table, String champ, String ordre, int limite) throws Exception{
        limite = limite + 1;
        JSONArray result = null;
        String req = "SELECT * FROM (SELECT * FROM "+table+" ORDER BY "+champ+" "+ordre+") WHERE ROWNUM <"+limite;
        try {
            
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    public static JSONArray select_list_order_by_limit_whereCondition(String table, String champ, String ordre, int limite, String champ_condition, String champ_valeur) throws Exception{
        limite = limite + 1;
        JSONArray result = null;
        String req = "SELECT * FROM (SELECT * FROM "+table+" WHERE "+champ_condition+" = '"+champ_valeur+"' ORDER BY "+champ+" "+ordre+") WHERE ROWNUM <"+limite;
        try {
            
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    public static String dateActuelle() throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
    
    public static JSONArray select_promotion_actuelle(int limite,String idTypeAbonnement) throws Exception{
        limite = limite + 1;
        JSONArray result = null;
        String nomTable = "AS_PROMOTION";
        String nomView = "AS_V_PROMOTION";
        String dateActuelle = ExecuteFunction.dateActuelle();
        dateActuelle = "TO_DATE('"+dateActuelle+"','yyyy/MM/dd HH24:MI:ss')";
        String req = "";
        String champOrderBy = "NOMPROMOTION";
        String ordreOrderBy = "ASC";
        if(limite!=0){
            if(idTypeAbonnement.compareTo("0")!=0){
                req = "SELECT * FROM (SELECT * FROM "+nomView+" WHERE IDTYPEABONNEMENT = '"+idTypeAbonnement+"' AND DATEDEBUT <= "+dateActuelle+" AND DATEFIN >= "+dateActuelle+" ORDER BY "+champOrderBy+" "+ordreOrderBy+") WHERE ROWNUM <"+limite;
            }
            else req = "SELECT * FROM (SELECT * FROM "+nomTable+" WHERE DATEDEBUT <= "+dateActuelle+" AND DATEFIN >= "+dateActuelle+" ORDER BY "+champOrderBy+" "+ordreOrderBy+") WHERE ROWNUM <"+limite;
        }else{
            if(idTypeAbonnement.compareTo("0")!=0){
                req = "SELECT * FROM "+nomView+" WHERE IDTYPEABONNEMENT = '"+idTypeAbonnement+"' AND DATEDEBUT <= "+dateActuelle+" AND DATEFIN >= "+dateActuelle+" ORDER BY "+champOrderBy+" "+ordreOrderBy;
            }
            else req = "SELECT * FROM "+nomTable+" WHERE DATEDEBUT <= "+dateActuelle+" AND DATEFIN >= "+dateActuelle+" ORDER BY "+champOrderBy+" "+ordreOrderBy;
        }
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    
    //Fonction pour v�rifier le nombre de ligne correspondant � 3 conditions
    public static JSONArray select_count_3where(String table, String champ1,String valChamp1, String champ2, String valChamp2, String champ3, String valChamp3) throws Exception{
        JSONArray result = null;
        String req = "SELECT COUNT(*) AS NBR FROM "+table+" WHERE "+champ1+" = '"+valChamp1+"' AND "+champ2+" = '"+valChamp2+"' AND "+champ3+" = '"+valChamp3+"'";
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour r�cup�rer le nombre d'abonnement actuel d'un utilisateur
    public static JSONArray select_count_abonnementActuelle(String idUtilisateur) throws Exception{
        JSONArray result = null;
        String req = "SELECT COUNT(*) AS NBR FROM AS_V_ABONNEMENT_ACTUEL WHERE IDUTILISATEUR='"+idUtilisateur+"'";
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour vérifier l'authentification
    public static JSONArray select_authentification_count(String table, String champ1,String valChamp1, String champ2, String valChamp2) throws Exception{
        JSONArray result = null;
        String req = "SELECT COUNT(*) AS AUTHENTIFICATION FROM "+table+" WHERE "+champ1+" = '"+valChamp1+"' AND "+champ2+" = '"+valChamp2+"'";
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
        
    //Fonction pour vérifier l'authentification null
    public static JSONArray select_authentification_countNull(String email) throws Exception{
        JSONArray result = null;
        String req = "SELECT COUNT(*) AS AUTHENTIFICATION FROM AS_UTILISATEUR WHERE EMAILUTILISATEUR='"+email+"' AND MOTDEPASSE IS NULL";
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour compter le nombre d'élément respectant les paramètres
    public static JSONArray select_count_table(String table, String champ1,String valChamp1) throws Exception{
        JSONArray result = null;
        String req = "SELECT COUNT(*) AS NBR FROM "+table+" WHERE "+champ1+" = '"+valChamp1+"'";
        try {
             
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    //Fonction pour compter le nombre d'�l�ment dans une table
    public static JSONArray select_count_table_all(String table) throws Exception{
        JSONArray result = null;
        String req = "SELECT COUNT(*) AS NBR FROM "+table;
        try {
             
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour vérifier si l'email existe
    public static JSONArray select_authentification_email_count(String table, String champ1,String valChamp1) throws Exception{
        JSONArray result = null;
        String req = "SELECT COUNT(*) AS AUTHENTIFICATION FROM "+table+" WHERE "+champ1+" = '"+valChamp1+"'";
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Récupération des informations d'une table par son champ et la valeur de ce dernier
    public static JSONArray select_info_by_champ(String table, String champ, String valeur)throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM "+table+" WHERE "+champ+" = '"+valeur+"'";
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
	
    //Fonction pour récupérer les valeurs d'une table à partir de 2 champs et leurs valeurs respectives
    public static JSONArray valeurSequenceSuivante(String nomSequence) throws Exception{
        JSONArray result = null;
        String req = "SELECT "+nomSequence+".NEXTVAL AS SUIVANT FROM DUAL";
        try {             
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    //Fonction pour r�cup�rer la derniere valeurs d'une sequence
    public static JSONArray derniereValeurSequence(String nomSequence) throws Exception{
        JSONArray result = null;
        //String req = "SELECT "+nomSequence+".NEXTVAL AS SUIVANT FROM DUAL";
        String req = "SELECT "+nomSequence+".CURRVAL as last_val FROM DUAL";
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour récupérer les valeurs d'une table à partir de 2 champs et leurs valeurs respectives
    public static JSONArray select_deux_criteres(String table, String champ1,String valChamp1, String champ2, String valChamp2) throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM "+table+" WHERE "+champ1+" = '"+valChamp1+"' AND "+champ2+"='"+valChamp2+"'";
        System.out.println(req);
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    public static JSONArray select_count_2criteres(String table, String nom, String champ1,String valChamp1, String champ2, String valChamp2) throws Exception{
        JSONArray result = null;
        String req = "SELECT COUNT(*) as "+nom+" FROM "+table+" WHERE "+champ1+" = '"+valChamp1+"' AND "+champ2+"='"+valChamp2+"'";
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour récupérer la dernière valeur d'une sequence
    public static JSONArray select_last_number(String sequence) throws Exception{
        JSONArray result = null;
        String req = "SELECT last_number from all_sequences WHERE sequence_name = '"+sequence+"'";
        try {
             
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour récupérer les informations de l'abonnement actuel d'un utilisateur*/
    public static JSONArray select_abt_actuel_util(String idUtilisateur)throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM AS_V_ABONNEMENT_ACTUEL WHERE IDUTILISATEUR = '"+idUtilisateur+"'";
        try {
             
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    //Fonction pour avoir une liste paginée avec un parametre
    public static JSONArray select_listWhere0OrderByLimit(String table, String champOrdre, String valueOrderBy, int limite, int debut) throws Exception{
        JSONArray result = null;
        debut = debut +1;
        int nouveauDebut = ((debut*limite)+1)-limite;
        int nouvelleFin = nouveauDebut + limite;
        String req = "SELECT*FROM "+table+" ORDER BY "+champOrdre+" "+valueOrderBy;
        String requeteFinale = "SELECT * FROM ( SELECT a.*, ROWNUM rnum FROM ("+req+") a WHERE ROWNUM <"+nouvelleFin+") WHERE rnum >="+nouveauDebut;
        System.out.println(requeteFinale);
        try {
            result = ExecuteFunction.resultJSON(requeteFinale);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour avoir une liste paginée avec un parametre
    public static JSONArray select_listWhere1OrderByLimit(String table, String champ, String valeurChamp, String champOrdre, String valueOrderBy, int limite, int debut) throws Exception{
        JSONArray result = null;
        debut = debut +1;
        int nouveauDebut = ((debut*limite)+1)-limite;
        int nouvelleFin = nouveauDebut + limite;
        String req = "SELECT*FROM "+table+" WHERE "+champ+"='"+valeurChamp+"' ORDER BY "+champOrdre+" "+valueOrderBy;
        String requeteFinale = "SELECT * FROM ( SELECT a.*, ROWNUM rnum FROM ("+req+") a WHERE ROWNUM <"+nouvelleFin+") WHERE rnum >="+nouveauDebut;
        try {
            result = ExecuteFunction.resultJSON(requeteFinale);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour avoir une liste paginée avec un parametre
    public static JSONArray select_listWhere2OrderByLimit(String table, String champ1, String valeurChamp1, String champ2, String valeurChamp2, String champOrdre, String valueOrderBy, int limite, int debut) throws Exception{
        JSONArray result = null;
        debut = debut +1;
        int nouveauDebut = ((debut*limite)+1)-limite;
        int nouvelleFin = nouveauDebut + limite;
        String req = "SELECT * FROM "+table+" WHERE "+champ1+"='"+valeurChamp1+"' AND "+champ2+"='"+valeurChamp2+"' ORDER BY "+champOrdre+" "+valueOrderBy;
        String requeteFinale = "SELECT * FROM ( SELECT a.*, ROWNUM rnum FROM ("+req+") a WHERE ROWNUM <"+nouvelleFin+") WHERE rnum >="+nouveauDebut;
        try {
             
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour lister les promotions respectant la date de début/fin sans limite
    public static JSONArray select_list_promotion(String champ, String ordre, String dateDebut, String dateFin) throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM AS_PROMOTION WHERE 1<2 ";
        if(dateDebut!=null){
            dateDebut = "TO_DATE('"+dateDebut+"','YYYY-MM-DD HH24:MI:SS')";
            req=req+"AND DATEDEBUT<="+dateDebut+" ";
        }
        if(dateFin!=null){
            dateFin = "TO_DATE('"+dateFin+"','YYYY-MM-DD HH24:MI:SS')";
            req=req+"AND DATEFIN>="+dateFin+" ";
        }
        req = req +"  ORDER BY "+champ+" "+ordre;
        try {
             
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour lister les �l�ments d'une table id val desc en respectant la date de début/fin avec limite
    public static JSONArray select_list_idValDesce_limite(String table, String champ, String ordre, int limite, int debut) throws Exception{
        JSONArray result = null;
        debut = debut +1;
        int nouveauDebut = ((debut*limite)+1)-limite;
        int nouvelleFin = nouveauDebut + limite;
        String req = "SELECT * FROM "+table+" WHERE 1<2 ORDER BY "+champ+" "+ordre;
        String requeteFinale = "SELECT * FROM ( SELECT a.*, ROWNUM rnum FROM ("+req+") a WHERE ROWNUM <"+nouvelleFin+") WHERE rnum >="+nouveauDebut;
        try {
            System.out.println("Requete finale : "+requeteFinale);
            result = ExecuteFunction.resultJSON(requeteFinale);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
            
            
    //Fonction pour lister les promotions respectant la date de d�but/fin avec limite
    public static JSONArray select_list_promotion_limite(String champ, String ordre, int limite, int debut, String dateDebut, String dateFin, String heureDebut, String heureFin) throws Exception{
        JSONArray result = null;
        try {
            System.out.println("select_list_promotion_limite ok");
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    public static JSONArray select_list_promotion_sansLimite(String dateDebut, String dateFin, String heureDebut, String heureFin) throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM AS_PROMOTION WHERE 1<2 ";
        String val_dateDebut = null;
        String val_dateFin = null;
        if(dateDebut.compareTo("null")!=0 && dateDebut.compareTo("")!=0){
            if(heureDebut.compareTo("null")==0 && heureDebut.compareTo("")==0){
                val_dateDebut = "TO_DATE('"+dateDebut+" "+heureDebut+"','YYYY-MM-DD HH24:MI:SS')";
            }else val_dateDebut = "TO_DATE('"+dateDebut+"','YYYY-MM-DD')";
            req=req+"AND DATEDEBUT<="+val_dateDebut+" ";
        }
        System.out.println("Date debut:"+dateDebut);
        System.out.println("Heure debut:"+dateDebut);
        System.out.println(val_dateDebut);
        if(dateFin.compareTo("null")!=0 && dateFin.compareTo("")!=0){
            if(heureFin.compareTo("null")==0 && heureFin.compareTo("")==0){
                val_dateFin = "TO_DATE('"+dateFin+" "+heureFin+"','YYYY-MM-DD HH24:MI:SS')";
            }else val_dateFin = "TO_DATE('"+dateFin+"','YYYY-MM-DD')";
            req=req+"AND DATEFIN>="+val_dateFin+" ";
        }
        System.out.println(val_dateFin);
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    public static JSONArray select_list_promotion_limite1(String champ, String ordre, int limite, int debut, String dateDebut, String dateFin, String heureDebut, String heureFin) throws Exception{
        JSONArray result = null;
        debut = debut +1;
        int nouveauDebut = ((debut*limite)+1)-limite;
        int nouvelleFin = nouveauDebut + limite;
        String req = "SELECT * FROM AS_PROMOTION WHERE 1<2 ";
        System.out.println(req);
        String val_dateDebut = null;
        String val_dateFin = null;
        if(dateDebut.compareTo("null")!=0 && dateDebut.compareTo("")!=0){
            if(heureDebut.compareTo("null")==0 && heureDebut.compareTo("")==0){
                val_dateDebut = "TO_DATE('"+dateDebut+" "+heureDebut+"','YYYY-MM-DD HH24:MI:SS')";
            }else val_dateDebut = "TO_DATE('"+dateDebut+"','YYYY-MM-DD')";
            req=req+"AND DATEDEBUT<="+val_dateDebut+" ";
        }
        
        System.out.println("Date debut:"+dateDebut);
        System.out.println("Heure debut:"+dateDebut);
        System.out.println(val_dateDebut);
        if(dateFin.compareTo("null")!=0 && dateFin.compareTo("")!=0){
            if(heureFin.compareTo("null")==0 && heureFin.compareTo("")==0){
                val_dateFin = "TO_DATE('"+dateFin+" "+heureFin+"','YYYY-MM-DD HH24:MI:SS')";
            }else val_dateFin = "TO_DATE('"+dateFin+"','YYYY-MM-DD')";
            req=req+"AND DATEFIN>="+val_dateFin+" ";
        }
        System.out.println(val_dateFin);
        req = req +"  ORDER BY "+champ+" "+ordre;
        String requeteFinale = "SELECT * FROM ( SELECT a.*, ROWNUM rnum FROM ("+req+") a WHERE ROWNUM <"+nouvelleFin+") WHERE rnum >="+nouveauDebut;
        try {
            System.out.println(requeteFinale);
            result = ExecuteFunction.resultJSON(requeteFinale);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
	
	//Fonction pour lister les promotions respectant la date de début/fin avec limite
    public static JSONArray select_whereOrderBy(String table, String colonne, String valeur, String orderBy, String ordre) throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM "+table+" WHERE "+colonne+" = '"+valeur+"' ORDER BY "+orderBy+" "+ordre;
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour lister les promotions actuelles bénéficiées par un idAbonnement
    public static JSONArray selectPromo_date_idAbt(String idAbonnement,int debut, int limite, String date, String colonne, String ordre) throws Exception{
        JSONArray result = null;
        date = to_dateTime(date);
        String req = "SELECT * FROM AS_V_PROMOTIONTYPEABONNEMENT WHERE IDTYPEABONNEMENT = '"+idAbonnement+"' AND DATEDEBUT <= "+date+" AND DATEFIN >= "+date+" ORDER BY "+colonne+" "+ordre;
        debut = debut +1;
        int nouveauDebut = ((debut*limite)+1)-limite;
        int nouvelleFin = nouveauDebut + limite;
        String requeteFinale = "SELECT * FROM ( SELECT a.*, ROWNUM rnum FROM ("+req+") a WHERE ROWNUM <"+nouvelleFin+") WHERE rnum >="+nouveauDebut;
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour lister les plages horaires de livraison disponibles par rapport à une heure
    public static JSONArray listeHoraire_heure(String mois, String annee, String heure, String colonne, String ordre) throws Exception{
        JSONArray result = null;
        String req = "SELECT * FROM AS_HORAIRELIVRAISON WHERE FINCOMMANDE >= TO_DATE('01/"+mois+"/"+annee+" "+heure+"','DD/MM/YYYY HH24:MI') ORDER BY "+colonne+" "+ordre;
        System.out.println(req);
        try {
			result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //-----------------------INSERTION-----------------------------------*/
    
    /*Fonction pour ajouter un frais li� � une commande*/
    public static String inserer_fraisCommande(String id, String idCommande, String frais, String commentaire) throws Exception{
        String result = null;
        /*Commentaire*/
        if(commentaire==null) commentaire ="null"; else commentaire = "'"+commentaire+"'";
        System.out.println("ID: "+id);
        System.out.println("idCommande: "+idCommande);
        System.out.println("Frais: "+frais);
        System.out.println("Commentaire: "+commentaire);
        
        String req = "INSERT INTO AS_FRAIS_COMMANDE (ID, COMMANDE, FRAIS, COMMENTAIRE)"
                + " VALUES ('"+id+"','"+idCommande+"',"+frais+","+commentaire+")";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour ajouter un produit au favori par l'utilisateur*/
    public static String inserer_favori_produit(String id, String idProduit, String idUtilisateur, String dateAjout, String commentaire) throws Exception{
        String result = null;
        /*Date d'ajout*/
        if(dateAjout==null) dateAjout = "null";
        else dateAjout = "TO_DATE('"+dateAjout+"','YYYY-MM-DD')";
        
        /*Commentaire*/
        if(commentaire==null) commentaire ="null"; else commentaire = "'"+commentaire+"'";
        
        String req = "INSERT INTO AS_PRODUIT_FAVORI(ID,IDPRODUIT,IDUTILISATEUR,DATEAJOUT,COMMENTAIRE)"
                + " VALUES ('"+id+"','"+idProduit+"','"+idUtilisateur+"',"+dateAjout+","+commentaire+")";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
	
	/*Fonction pour ajouter un utilisateur*/
    public static String inserer_utilisateur(String id, String civilite, String nom, String prenom, String dateNaissance, String email, String identifiant, String mdp, String statut, String idSession, String commentaire, String photo) throws Exception{
        String result = null;
        /*Date de naissance*/
        if(dateNaissance==null) dateNaissance = "null";
        else dateNaissance = "TO_DATE('"+dateNaissance+"','DD/MM/YYYY')";
        
        
        /*Commentaire*/
        if(commentaire==null) commentaire ="null"; else commentaire = "'"+commentaire+"'";
        if(photo==null) photo="null"; else photo = "'"+photo+"'";
        
        String req = "INSERT INTO AS_UTILISATEUR(IDUTILISATEUR,CIVILITE,NOMUTILISATEUR,PRENOMUTILISATEUR,NAISSANCEUTILISATEUR,EMAILUTILISATEUR,IDENTIFIANT,MOTDEPASSE,STATUTUTILISATEUR,IDSESSION,COMMENTAIRE,PHOTO)"
                + " VALUES ('"+id+"',"+civilite+",'"+nom+"','"+prenom+"',"+dateNaissance+",'"+email+"','"+identifiant+"','"+mdp+"','"+statut+"','"+idSession+"',"+commentaire+","+photo+")";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            System.out.println(startAction);
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
            System.out.println(result);
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour inserer un article au panier*/
    public static String inserer_panier_article(String id, String idProduit, String idUtilisateur, int quantite, float pu, float remise, int statut, String observation) throws Exception{
        String result = null;
        String req = "INSERT INTO AS_PANIER_ARTICLE(ID,IDPRODUIT,IDUTILISATEUR,QUANTITE,PU,REMISE,STATUTPANIER,OBSERVATION) VALUES ('"+id+"','"+idProduit+"','"+idUtilisateur+"',"+quantite+","+pu+","+remise+","+statut+",'"+observation+"')";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "notok";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    /*Fonction pour inserer un nouvel administrateur*/
    public static String inserer_admin_article(String id, String nom, String prenom, String identifiant, String motdepasse, String email, int statut, String commentaire) throws Exception{
        String result = null;
        String req = "INSERT INTO AS_ADMIN(ID,NOM,PRENOM,IDENTIFIANT,MOTDEPASSE,EMAIL,STATUTADMINISTRATEUR,COMMENTAIRE) VALUES ('"+id+"','"+nom+"','"+prenom+"','"+identifiant+"','"+motdepasse+"','"+email+"',"+statut+",'"+commentaire+"')";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    /*Fonction pour inserer un message*/
    public static String inserer_message(String id, String idTypeMessage, String nom, String email, String contenu, int etatMessage, String dateEnvoi, String commentaire) throws Exception{
        String result = null;
        dateEnvoi = "TO_DATE('"+dateEnvoi+"','YYYY-MM-DD HH24:MI:SS')";
        String req = null;
        commentaire = "'"+commentaire+"'";
        if(commentaire.compareTo("null")==0){
            commentaire = "null";
        }
        req = "INSERT INTO AS_MESSAGE(ID,IDTYPEMESSAGE,NOM,EMAIL,CONTENU,ETATMESSAGE,DATEENVOI,COMMENTAIRE) VALUES ('"+id+"','"+idTypeMessage+"','"+nom+"','"+email+"','"+contenu+"',"+etatMessage+","+dateEnvoi+","+commentaire+")";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour inserer un numéro téléphone*/
    public static String inserer_telephone(String id, String utilisateur, String numero, String commentaire) throws Exception{
        String result = null;
        String req = null;
        if(commentaire.compareTo("null")==0){
            req = "INSERT INTO AS_TELEPHONE(ID,IDUTILISATEUR,NUMEROTELEPHONE,COMMENTAIRE) VALUES ('"+id+"','"+utilisateur+"','"+numero+"',null)";
        }
        else req = "INSERT INTO AS_TELEPHONE(ID,IDUTILISATEUR,NUMEROTELEPHONE,COMMENTAIRE) VALUES ('"+id+"','"+utilisateur+"','"+numero+"','"+commentaire+"')";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                System.out.println("Ajout r�ussi.");
                result = "[ok]";
            }else result = "[notok]";        }
        catch(Exception e){
            result = "["+e.getMessage()+"]";
        }
        return result;
    }
    
    /*Fonction pour inserer une nouvelle adresse*/
    public static String inserer_adresse(String id, String idquartier, String idutilisateur, String idtypeadresse, String adresse, String commentaire) throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        if(commentaire.compareTo("null")==0){
            req = "INSERT INTO AS_ADRESSE(ID,IDQUARTIER,IDUTILISATEUR,IDTYPEADRESSE,ADRESSE,COMMENTAIRE) VALUES ('"+id+"','"+idquartier+"','"+idutilisateur+"','"+idtypeadresse+"','"+adresse+"',null)";
        }
            req = "INSERT INTO AS_ADRESSE(ID,IDQUARTIER,IDUTILISATEUR,IDTYPEADRESSE,ADRESSE,COMMENTAIRE) VALUES ('"+id+"','"+idquartier+"','"+idutilisateur+"','"+idtypeadresse+"','"+adresse+"','"+commentaire+"')";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour inserer une table de type id-val-desce*/
    public static String inserer_id_val_desce(String table,String id, String val, String desce)throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        if(desce.compareTo("null")==0) desce=val;
        req = "INSERT INTO "+table+" (ID,VAL,DESCE) VALUES ('"+id+"','"+val+"','"+desce+"')";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            except = "["+e.getMessage()+"]";
        }
        if(res!=null){
            result = "[ok]";
        }else if(res==null) result = except;
        return result;
    }
    
    /*Fonction pour inserer un nouveau produit*/
    public static String inserer_produit(String id,String nom,String designation,String photo,String typeProduit,String calorie,String poids,String pa,String afficher,String commentaire) throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        req = "INSERT INTO AS_PRODUITS (ID,NOM,DESIGNATION,PHOTO,TYPEPRODUIT,CALORIE,POIDS,PA,AFFICHER,COMMENTAIRE) VALUES ('"+id+"','"+nom+"','"+designation+"','"+photo+"','"+typeProduit+"','"+calorie+"','"+poids+"','"+pa+"','"+afficher+"','"+commentaire+"')";
        System.out.println(req);
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "["+id+"�ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            except = "["+e.getMessage()+"]";
        }
        return result;
    }
    
    /*Fonction pour inserer un prix produit*/
    public static String inserer_prix_produit(String id, String produit, String dateApplication, String montant, String observation) throws Exception{
        System.out.println("id : "+id);
        System.out.println("produit : "+produit);
        System.out.println("dateApplication : "+dateApplication);
        System.out.println("montant : "+montant);
        System.out.println("observation : "+observation);

        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        dateApplication = "TO_DATE('"+dateApplication+"','YYYY-MM-DD')";
        if(observation==null){
            req = "INSERT INTO AS_PRIXPRODUIT (ID,PRODUIT,DATEAPPLICATION,MONTANT,OBSERVATION) VALUES ('"+id+"','"+produit+"',"+dateApplication+",'"+montant+"',null)";
        }else{
            req = "INSERT INTO AS_PRIXPRODUIT (ID,PRODUIT,DATEAPPLICATION,MONTANT,OBSERVATION) VALUES ('"+id+"','"+produit+"',"+dateApplication+",'"+montant+"','"+observation+"')";
        }
        System.out.println(req);
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            except = "["+e.getMessage()+"]";
        }
        return result;
    }
    
    /*Fonction pour inserer un nouveau tarif de livraison*/
    public static String inserer_TarifLivraison (String id, String quartier, String montant, String dateApplication, String observation) throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        dateApplication = "TO_DATE('"+dateApplication+"','YYYY-MM-DD')";
        if(observation==null){
            req = "INSERT INTO AS_TARIFLIVRAISON (ID,QUARTIER,MONTANT,DATEEFFECTIVITE,OBSERVATION) VALUES ('"+id+"','"+quartier+"',"+montant+","+dateApplication+",null)";
        }else{
            req = "INSERT INTO AS_TARIFLIVRAISON (ID,QUARTIER,MONTANT,DATEEFFECTIVITE,OBSERVATION) VALUES ('"+id+"','"+quartier+"',"+montant+","+dateApplication+",'"+observation+"')";
        }
        System.out.println(req);
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "["+e.getMessage()+"]";
        }
        return result;
    }
    
    /*Fonction pour inserer une commande client*/
    public static String inserer_commandeClient(String id, String dateSaisie, String numCommande, String responsable, String client, String typeCommande, String dateLivraison, String adresseLivraison, String heureLivraison, String etat, String secteur, String quartier, String idSession, String idUtilisateur, String remarque, String observation, String distance) throws Exception{
        String result = null;
        String req = null;
        dateSaisie = "TO_DATE('"+dateSaisie+"','YYYY-MM-DD HH24:MI')";
        Date dateDuJour = new Date(); 
        String formatDate = "YYYY-MM-dd"; 
        SimpleDateFormat formater = new SimpleDateFormat( formatDate );
        String dateCommande = formater.format( dateDuJour );
        dateCommande = "TO_DATE('"+dateCommande+"','YYYY-MM-DD')";
        dateLivraison = "TO_DATE('"+dateLivraison+"','DD-MM-YYYY')";
        if(numCommande==null || numCommande.compareTo("")==0) numCommande="null";
        if(observation==null) observation="null";
        if(distance==null) observation="null";
        req = "INSERT INTO AS_COMMANDECLIENT "
                + "(ID,DATESAISIE,DATECOMMANDE,NUMCOMMANDE,RESPONSABLE,CLIENT,TYPECOMMANDE,DATELIV,ADRESSELIV,HEURELIV,DISTANCE,REMARQUE,ETAT,SECTEUR,OBSERVATION,QUARTIER,IDUTILISATEUR,IDSESSION) VALUES"
                + "('"+id+"',"+dateSaisie+","+dateCommande+","+numCommande+",'"+responsable+"','"+client+"','"+typeCommande+"',"+dateLivraison+",'"+adresseLivraison+"','"+heureLivraison+"',"+distance+",'"+remarque+"',"+etat+",'"+secteur+"',"+observation+",'"+quartier+"','"+idUtilisateur+"','"+idSession+"')";
         try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour inserer un détail de commande*/
    public static String inserer_detailCommande(String id, String idMere, String produit, String quantite, String pu, String remise, String observation){
        String result = null;
        String req = new String();
        JSONArray res = null;
        String except = null;
        if(observation==null) observation="null";
        req = "INSERT INTO AS_DETAILSCOMMANDE "
                + "(ID,IDMERE,PRODUIT,QUANTITE,PU,REMISE,OBSERVATION) VALUES"
                + "('"+id+"','"+idMere+"','"+produit+"','"+quantite+"',"+pu+","+remise+","+observation+")";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour inserer un abonnement*/
    public static String inserer_abonnement(String id,String typeAbonnement,  String idUtilisateur, String debut, String fin, String statut, String idSession, String commentaire, String nbrMois) throws Exception{
        String result = null;
        /*Date de debut*/
        String dateDebut = "TO_DATE('"+debut+"','YYYY-MM-DD')";
        /*Date de fin*/
        String dateFin = "TO_DATE('"+fin+"','YYYY-MM-DD')";
        /*Commentaire*/
        if(commentaire==null) commentaire ="null";
        else commentaire="'"+commentaire+"'";
        String requete = "INSERT INTO AS_ABONNEMENT (ID,IDTYPEABONNEMENT,IDUTILISATEUR,DATEDEBUTABONNEMENT,DATEFINABONNEMENT,STATUTABONNEMENT,IDSESSION,COMMENTAIRE,NBRMOIS) VALUES ('"+id+"','"+typeAbonnement+"','"+idUtilisateur+"',"+dateDebut+","+dateFin+","+statut+",'"+idSession+"',"+commentaire+","+nbrMois+")";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
	//Fonction pour inserer une promotion
    public static String inserer_promotion(String id, String nomPromotion, String dateDebut, String dateFin, String descriptionPromotion, String idSession, String lienImage, String commentaire) throws Exception{
        String result = null;
        /*Date de debut*/
        String dateDebut_val = ExecuteFunction.to_dateTime(dateDebut);
        System.out.println("Date de d�but:"+dateDebut);
        System.out.println("Valeur de la date de d�but:"+dateDebut_val);
        /*Date de fin*/
        String dateFin_val = ExecuteFunction.to_dateTime(dateFin);
        System.out.println("Date de fin:"+dateFin);
        System.out.println("Valeur de la date de fin:"+dateFin_val);
        /*Commentaire*/
        if(commentaire==null) commentaire ="null";
        else commentaire="'"+commentaire+"'";
        String requete = "INSERT INTO AS_PROMOTION (ID,NOMPROMOTION,DATEDEBUT,DATEFIN,DESCRIPTIONPROMOTION,IDSESSION,LIENIMAGE,COMMENTAIRE) VALUES ('"+id+"','"+nomPromotion+"',TO_DATE('"+dateDebut+"','yyyy-mm-dd hh24:mi:ss'), TO_DATE('"+dateFin+"','yyyy-mm-dd hh24:mi:ss'),'"+descriptionPromotion+"','"+idSession+"','"+lienImage+"',"+commentaire+")";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    //Fonction pour inserer un type d'abonnement lié à une promotion
    public static String inserer_promotionTypeAbonnement(String idTypeAbonnement, String idPromotion, String commentaire) throws Exception{
        String result = null;
        /*Commentaire*/
        if(commentaire==null) commentaire ="null";
        else commentaire="'"+commentaire+"'";
        String requete = "INSERT INTO AS_PROMOTIONTYPEABONNEMENT (IDTYPEABONNEMENT,IDPROMOTION,COMMENTAIRE) VALUES ('"+idTypeAbonnement+"','"+idPromotion+"',"+commentaire+")";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
	
    //Fonction pour inserer une cause de promotion
    public static String inserer_promotionCause(String id, String idPromotion, String idProduit, String nbrProduitAchete, String commentaire) throws Exception{
        String result = null;
        /*Commentaire*/
        if(commentaire==null) commentaire ="null";
        else commentaire="'"+commentaire+"'";
        String requete = "INSERT INTO AS_PROMOTIONCAUSE (ID,IDPROMOTION,IDPRODUIT,NBRPRODUITACHETE,COMMENTAIRE) VALUES ('"+id+"','"+idPromotion+"','"+idProduit+"',"+nbrProduitAchete+","+commentaire+")";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }	

    //Fonction pour inserer une consequence de promotion
    public static String inserer_promotionConsequence(String id, String idPromotion, String idProduit, String nbrProduitOffert, String remise, String commentaire) throws Exception{
        String result = null;
        /*Commentaire*/
        if(commentaire==null) commentaire ="null";
        else commentaire="'"+commentaire+"'";
        String requete = "INSERT INTO AS_PROMOTIONCONSEQUENCE (ID,IDPROMOTION,IDPRODUIT,NBRPRODUITOFFERT, REMISEPRODUITOFFERT, COMMENTAIRE) VALUES ('"+id+"','"+idPromotion+"','"+idProduit+"',"+nbrProduitOffert+","+remise+","+commentaire+")";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }

    //Fonction pour inserer une promotion au panier
    public static String inserer_panier_promotion(String id, String idPromotion, String idUtilisateur, int quantite, float pu, int statutPanier, String observation) throws Exception{
        String result = null;
        /*Commentaire*/
        if(observation==null || observation.compareTo("")==0) observation ="null";
        else observation="'"+observation+"'";
        String requete = "INSERT INTO AS_PANIERPROMOTION (ID,IDPROMOTION,IDUTILISATEUR,QUANTITE,PU,STATUTPANIER,OBSERVATION) VALUES ('"+id+"','"+idPromotion+"','"+idUtilisateur+"',"+quantite+","+pu+","+statutPanier+","+observation+")";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }

    //Fonction pour inserer un paiement sur le site
     public static String inserer_paiementSite(String id, String idTypePaiement, String idUtilisateur, String idCommandeMere, String datePaiement, String montant, String commentaire) throws Exception{
        String result = null;
        /*Commentaire*/
        if(commentaire==null || commentaire.compareTo("")==0) commentaire ="null";
        else commentaire="'"+commentaire+"'";
        String requete = "INSERT INTO AS_PAIEMENTSITE (ID,IDTYPEPAIEMENT,IDUTILISATEUR,IDCOMMANDEMERE,DATEPAIEMENT,MONTANT,COMMENTAIRE) VALUES ('"+id+"','"+idTypePaiement+"','"+idUtilisateur+"','"+idCommandeMere+"',TO_DATE('"+datePaiement+"','YYYY-MM-DD'),"+montant+","+commentaire+")";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }

    //Fonction poru inserer une nouvelle image
    public static String inserer_image(String id, String idCategorieImage, String nomImage, String lienImage, String description, String alt, String commentaire) throws Exception{
        String result = null;
        /*Commentaire*/
        if(commentaire==null || commentaire.compareTo("")==0) commentaire ="null";
        else commentaire="'"+commentaire+"'";
        
        /*Description*/
        if(description==null || description.compareTo("")==0) description ="null";
        else description="'"+description+"'";
        
        /*Alt*/
        if(alt==null || alt.compareTo("")==0) alt ="null";
        else alt="'"+alt+"'";
        
        String requete = "INSERT INTO AS_IMAGES (ID,IDCATEGORIEIMAGE,NOMIMAGE,LIENIMAGE,DESCRIPTIONIMAGE,ALT,COMMENTAIRE) VALUES ('"+id+"','"+idCategorieImage+"','"+nomImage+"','"+lienImage+"',"+description+","+alt+","+commentaire+")";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }

    /*Fonction pour inserer un détail de produit*/
    public static String inserer_detailProduit(String id, String idProduit, String commentaire, String energie, String a_energie, String proteine, String a_proteine, String carbohytrateGlycemique, String a_carbohytrateGlycemique, String totalSucre, String a_totalSucre, String totalSodium, String a_totalSodium, String cholesterole, String a_cholesterole, String totalMatiereGrasse, String a_totalMatiereGrasse, String totalMatiereGrasseMonoSaturee, String a_totalMatiereGrasseMonoSaturee, String totalMatiereGrassePolyinsaturee, String a_totalMatiereGrassePolyinsaturee, String fibreDietetique, String a_fibreDietetique, String totalKJ, String a_totalKJ, String totalCal, String a_totalCal) throws Exception{
        String result = null;
        
        /*Commentaire*/
        if(commentaire==null || commentaire.compareTo("")==0) commentaire ="null"; else commentaire = "'"+commentaire+"'";
        System.out.println("COMMENTAIRE : "+commentaire);
        
        if(energie==null || energie.compareTo("")==0) energie ="null"; else energie = "'"+energie+"'";
        if(a_energie==null || a_energie.compareTo("")==0) a_energie ="null"; else a_energie = "'"+a_energie+"'";
        System.out.println("energie : "+commentaire+" /// afficher :"+a_energie);
        
        if(proteine==null || proteine.compareTo("")==0) proteine ="null"; else proteine = "'"+proteine+"'";
        if(a_proteine==null || a_proteine.compareTo("")==0) a_proteine ="null"; else a_proteine = "'"+a_proteine+"'";
        System.out.println("proteine : "+proteine+" /// afficher :"+a_proteine);
        
        if(carbohytrateGlycemique==null || carbohytrateGlycemique.compareTo("")==0) carbohytrateGlycemique ="null"; else carbohytrateGlycemique = "'"+carbohytrateGlycemique+"'";
        if(a_carbohytrateGlycemique==null || a_carbohytrateGlycemique.compareTo("")==0) a_carbohytrateGlycemique ="null"; else a_carbohytrateGlycemique = "'"+a_carbohytrateGlycemique+"'";
        System.out.println("carbohytrateGlycemique : "+carbohytrateGlycemique+" /// afficher :"+a_carbohytrateGlycemique);
        
        if(totalSucre==null || totalSucre.compareTo("")==0) totalSucre ="null"; else totalSucre = "'"+totalSucre+"'";
        if(a_totalSucre==null || a_totalSucre.compareTo("")==0) a_totalSucre ="null"; else a_totalSucre = "'"+a_totalSucre+"'";
        System.out.println("totalSucre : "+totalSucre+" /// afficher :"+a_totalSucre);
        
        if(totalSodium==null || totalSodium.compareTo("")==0) totalSodium ="null"; else totalSodium = "'"+totalSodium+"'";
        if(a_totalSodium==null || a_totalSodium.compareTo("")==0) a_totalSodium ="null"; else a_totalSodium = "'"+a_totalSodium+"'";
        System.out.println("totalSodium : "+totalSodium+" /// afficher :"+a_totalSodium);
        
        if(cholesterole==null || cholesterole.compareTo("")==0) cholesterole ="null"; else cholesterole = "'"+cholesterole+"'";
        if(a_cholesterole==null || a_cholesterole.compareTo("")==0) a_cholesterole ="null"; else a_cholesterole = "'"+a_cholesterole+"'";
        System.out.println("cholesterole : "+cholesterole+" /// afficher :"+a_cholesterole);
        
        if(totalMatiereGrasse==null || totalMatiereGrasse.compareTo("")==0) totalMatiereGrasse ="null"; else totalMatiereGrasse = "'"+totalMatiereGrasse+"'";
        if(a_totalMatiereGrasse==null || a_totalMatiereGrasse.compareTo("")==0) a_totalMatiereGrasse ="null"; else a_totalMatiereGrasse = "'"+a_totalMatiereGrasse+"'";
        System.out.println("totalMatiereGrasse : "+totalMatiereGrasse+" /// afficher :"+a_totalMatiereGrasse);
        
        if(totalMatiereGrasseMonoSaturee==null || totalMatiereGrasseMonoSaturee.compareTo("")==0) totalMatiereGrasseMonoSaturee ="null"; else totalMatiereGrasseMonoSaturee = "'"+totalMatiereGrasseMonoSaturee+"'";
        if(a_totalMatiereGrasseMonoSaturee==null || a_totalMatiereGrasseMonoSaturee.compareTo("")==0) a_totalMatiereGrasseMonoSaturee ="null"; else a_totalMatiereGrasseMonoSaturee = "'"+a_totalMatiereGrasseMonoSaturee+"'";
        System.out.println("totalMatiereGrasseMonoSaturee : "+totalMatiereGrasseMonoSaturee+" /// afficher :"+a_totalMatiereGrasseMonoSaturee);
        
        if(totalMatiereGrassePolyinsaturee==null || totalMatiereGrassePolyinsaturee.compareTo("")==0) totalMatiereGrassePolyinsaturee ="null"; else totalMatiereGrassePolyinsaturee = "'"+totalMatiereGrassePolyinsaturee+"'";
        if(a_totalMatiereGrassePolyinsaturee==null || a_totalMatiereGrassePolyinsaturee.compareTo("")==0) a_totalMatiereGrassePolyinsaturee ="null"; else a_totalMatiereGrassePolyinsaturee = "'"+a_totalMatiereGrassePolyinsaturee+"'";
        System.out.println("totalMatiereGrassePolyinsaturee : "+totalMatiereGrassePolyinsaturee+" /// afficher :"+a_totalMatiereGrassePolyinsaturee);
        
        if(fibreDietetique==null || fibreDietetique.compareTo("")==0) fibreDietetique ="null"; else fibreDietetique = "'"+fibreDietetique+"'";
        if(a_fibreDietetique==null || a_fibreDietetique.compareTo("")==0) a_fibreDietetique ="null"; else a_fibreDietetique = "'"+a_fibreDietetique+"'";
        System.out.println("fibreDietetique : "+fibreDietetique+" /// afficher :"+a_fibreDietetique);
        
        if(totalKJ==null || totalKJ.compareTo("")==0) totalKJ ="null"; else totalKJ = "'"+totalKJ+"'";
        if(a_totalKJ==null || a_totalKJ.compareTo("")==0) a_totalKJ ="null"; else a_totalKJ = "'"+a_totalKJ+"'";
        System.out.println("totalKJ : "+totalKJ+" /// afficher :"+a_totalKJ);
        
        if(totalCal==null || totalCal.compareTo("")==0) totalCal ="null"; else totalCal = "'"+totalCal+"'";
        if(a_totalCal==null || a_totalCal.compareTo("")==0) a_totalCal ="null"; else a_totalCal = "'"+a_totalCal+"'";
        System.out.println("totalCal : "+totalCal+" /// afficher :"+a_totalCal);
        
        
        String req = "INSERT INTO AS_DETAILPRODUIT(ID, IDPRODUIT, ENERGIE, A_ENERGIE, PROTEINE, A_PROTEINE, CARBOHYTRATEGLYCEMIQUE, A_CARBOHYTRATEGLYCEMIQUE, TOTALSUCRE, A_TOTALSUCRE, TOTALSODIUM, A_TOTALSODIUM, CHOLESTEROLE, A_CHOLESTEROLE, TOTALMATIEREGRASSE, A_TOTALMATIEREGRASSE, MATIEREGRASSEMONOSATUREE, A_MATIEREGRASSEMONOSATUREE, MATIEREGRASSEPOLYINSATUREE,A_MATIEREGRASSEPOLYINSATUREE, FIBREDIETETIQUE, A_FIBREDIETETIQUE, TOTALKJ, A_TOTALKJ, TOTALCAL, A_TOTALCAL, COMMENTAIRE)"
                + " VALUES ('"+id+"','"+idProduit+"',"+energie+","+a_energie+","+proteine+","+a_proteine+","+carbohytrateGlycemique+","+a_carbohytrateGlycemique+","+totalSucre+","+a_totalSucre+","+totalSodium+","+a_totalSodium+","+cholesterole+","+a_cholesterole+","+totalMatiereGrasse+","+a_totalMatiereGrasse+","+totalMatiereGrasseMonoSaturee+","+a_totalMatiereGrasseMonoSaturee+","+totalMatiereGrassePolyinsaturee+","+a_totalMatiereGrassePolyinsaturee+","+fibreDietetique+","+a_fibreDietetique+","+totalKJ+","+a_totalKJ+","+totalCal+","+a_totalCal+","+commentaire+")";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }

    //Fonction pour inserer un horaire de livraison 
    public static String inserer_horaireLivraison (String id, String debutCommande, String finCommande, String debutLivraison, String finLivraison, String commentaire) throws Exception{
        String result = null;
        debutCommande = "TO_DATE('"+debutCommande+"','HH24:MI')";
        finCommande = "TO_DATE('"+finCommande+"','HH24:MI')";
        debutLivraison = "TO_DATE('"+debutLivraison+"','HH24:MI')";
        finLivraison = "TO_DATE('"+finLivraison+"','HH24:MI')";
        String req = "INSERT INTO AS_HORAIRELIVRAISON(ID,DEBUTCOMMANDE,FINCOMMANDE,DEBUTLIVRAISON,FINLIVRAISON,COMMENTAIRE) VALUES ('"+id+"',"+debutCommande+","+finCommande+","+debutLivraison+","+finLivraison+",'"+commentaire+"')";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour inserer un ingr�dient*/
    public static String inserer_ingredient(String id, String libelle, String seuil, String unite, String quantiteParPack, String pu, String actif) throws Exception{
        String result = null;
        String req = "INSERT INTO AS_INGREDIENTS (ID, LIBELLE, SEUIL, UNITE, QUANTITEPARPACK, PU, ACTIF)"
                + " VALUES ('"+id+"','"+libelle+"',"+seuil+",'"+unite+"',"+quantiteParPack+","+pu+","+actif+")";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour inserer un tarif d'abonnement*/
    public static String inserer_tarifAbonnement(String id, String idDureeTarifAbonnement, String dateApplication, String montant, String commentaire) throws Exception{
        String result = null;
        String val_commentaire = "";
        String val_dateApplication = "";
        val_dateApplication = " TO_DATE('"+dateApplication+"','YYYY-MM-DD') ";
        if(commentaire.compareTo("")==0 || commentaire == null){
            val_commentaire = "null";
        }else val_commentaire = "'"+commentaire+"'";
        String req = "INSERT INTO AS_TARIFABONNEMENT (ID, IDDUREEABONNEMENT, MONTANT, DATEAPPLICATION, COMMENTAIRE)"
                + " VALUES ('"+id+"','"+idDureeTarifAbonnement+"',"+montant+","+val_dateApplication+" , "+val_commentaire+")";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    
    /*Fonction pour inserer une reinitialisation de mot de passe*/
    public static String inserer_reinitMdp(String id, String idUtilisateur, String cle, String statut, String debutValidite, String finValidite) throws Exception{
        String result = null;
        String val_debutValidite = " TO_DATE('"+debutValidite+"','YYYY-MM-DD HH24:MI:SS') ";
        String val_finValidite = " TO_DATE('"+finValidite+"','YYYY-MM-DD HH24:MI:SS') ";
        System.out.println("date debut : "+val_debutValidite);
        System.out.println("date fin : "+val_finValidite);
        String req = "INSERT INTO AS_REINITIALISATION_MDP (ID, IDUTILISATEUR, CLE, STATUT, DEBUTVALIDITE, FINVALIDITE)"
                + " VALUES ('"+id+"','"+idUtilisateur+"','"+cle+"','"+statut+"' ,"+val_debutValidite+" , "+val_finValidite+")";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    
    /*Fonction pour inserer une dur�e d'abonnement*/
    public static String inserer_dureeAbonnement(String id, String idTypeAbonnement, String nbrMois, String commentaire, String idSession) throws Exception{
        String result = null;
        String val_commentaire = null;
        if(commentaire.compareTo("")==0 || commentaire==null){
            val_commentaire = "null";
        }
        else val_commentaire = "'"+commentaire+"'";
        String req = "INSERT INTO AS_DUREEABONNEMENT (ID, IDTYPEABONNEMENT, NBRMOIS, COMMENTAIRE, IDSESSION)"
                + " VALUES ('"+id+"','"+idTypeAbonnement+"',"+nbrMois+","+val_commentaire+",'"+idSession+"')";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    
    /*Fonction pour inserer une demande d'abonnement*/
    public static String inserer_demandeAbonnement(String id, String idUtilisateur, String idDureeAbonnement, String dateDemande, String statut, String idSession) throws Exception{
        String result = null;
        String val_dateDemande = null;
        System.out.println("IDDUREEABONNEMENT"+idDureeAbonnement);
        val_dateDemande = "TO_DATE('"+dateDemande+"','YYYY-MM-DD')";
        String req = "INSERT INTO AS_DEMANDE_ABONNEMENT (ID, IDUTILISATEUR, IDDUREEABONNEMENT, DATEDEMANDE, STATUT, IDSESSION)"
                + " VALUES ('"+id+"','"+idUtilisateur+"','"+idDureeAbonnement+"',"+val_dateDemande+","+statut+" ,'"+idSession+"')";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
	/*--------------------UPDATE--------------------------*/
    /*Fonction pour modifier un utilisateur*/
    public static String modifier_utilisateur(String id, String civilite, String nom, String prenom, String dateNaissance, String email, String identifiant, String mdp, String statut, String idSession, String commentaire, String photo) throws Exception{
        String result = null;
        /*Date de naissance*/
        if(dateNaissance==null) dateNaissance = "null";
        else dateNaissance = "TO_DATE('"+dateNaissance+"','YYYY-MM-DD')";
        
        if(photo==null) photo="null"; else photo = "'"+photo+"'";
        
        String reqModif = "UPDATE AS_UTILISATEUR SET IDUTILISATEUR='"+id+"'";
        if(civilite!=null) reqModif = reqModif + ", CIVILITE="+civilite;
        if(nom!=null) reqModif = reqModif + ", NOMUTILISATEUR='"+nom+"'";
        if(prenom!=null) reqModif = reqModif + ", PRENOMUTILISATEUR='"+prenom+"'";
        if(dateNaissance!=null)  reqModif = reqModif + ", NAISSANCEUTILISATEUR="+dateNaissance;
        if(email!=null) reqModif = reqModif + ", EMAILUTILISATEUR='"+email+"'";
        if(identifiant!=null) reqModif = reqModif + ", IDENTIFIANT='"+identifiant+"'";
        if(mdp!=null) reqModif = reqModif + ", MOTDEPASSE='"+prenom+"'";
        if(statut!=null) reqModif = reqModif + ", STATUTUTILISATEUR='"+statut+"'";
        if(idSession!=null) reqModif = reqModif + ", IDSESSION='"+idSession+"'";
        if(commentaire!=null) reqModif = reqModif + ", COMMENTAIRE='"+commentaire+"'";
        reqModif = reqModif+" WHERE IDUTILISATEUR = '"+id+"'";
        try{
            System.out.println(reqModif);
            String startAction = ExecuteFunction.as_executeQuery(reqModif); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier un article au panier*/
    public static String modifier_panier_article(String id, String idProduit, String idUtilisateur, int quantite, float pu, float remise, int statut, String observation) throws Exception{
        String result = null;
        String req = "UPDATE AS_PANIER_ARTICLE SET ID='"+id+"',IDPRODUIT='"+idProduit+"',IDUTILISATEUR='"+idUtilisateur+"',QUANTITE="+quantite+",PU="+pu+",REMISE="+remise+",STATUTPANIER="+statut+",OBSERVATION='"+observation+"'";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "notok";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    public static String modifier_quantite_panier_article(String idProduit, int quantite) throws Exception{
        String result = null;
        String req = "UPDATE AS_PANIER_ARTICLE SET QUANTITE="+quantite+" WHERE IDPRODUIT='"+idProduit+"' AND STATUTPANIER = 0";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "notok";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour mettre à jour une ligne dans une table id val desce*/
    public static String update_id_val_desce(String table,String id, String val, String desce)throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        if(desce.compareTo("null")==0) desce=val;
        req = "UPDATE "+table+" SET VAL='"+val+"',DESCE='"+desce+"' WHERE ID='"+id+"'";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            except = "["+e.getMessage()+"]";
        }
        if(res!=null){
            result = "[ok]";
        }else if(res==null) result = except;
        return result;
    }
    
    /*Fonction pour modifier le statut d'un message*/
    public static String update_statut_message(String id, String etat)throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        req = "UPDATE AS_MESSAGE SET ETATMESSAGE='"+etat+"' WHERE ID='"+id+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            except = "["+e.getMessage()+"]";
        }
        return result;
    }

    /*Fonction pour modifier la quantit� d'un panier*/
    public static String update_quantiteCommande_2conditions(String table,String nomChampQuantite,int quantite,String nomChampCondition1,String valeurChampCondition1,String nomChampCondition2,String valeurChampCondition2) throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        req = "UPDATE "+table+" SET "+nomChampQuantite+"="+quantite+" WHERE "+nomChampCondition1+"='"+valeurChampCondition1+"' AND "+nomChampCondition2+"='"+valeurChampCondition2+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            except = "["+e.getMessage()+"]";
        }
        return result;
    }
    
    /*Fonction pour modifier le statut d'un message*/
    public static String update_statut_utilisateur(String id, String statut)throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        req = "UPDATE AS_UTILISATEUR SET STATUTUTILISATEUR='"+statut+"' WHERE IDUTILISATEUR='"+id+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            except = "["+e.getMessage()+"]";
        }
        return result;
    }
    
    /*Fonction pour modifier le statut d'un message*/
    public static String updateComs_promoTpeAbt(String idTypeAbonnement, String idPromotion, String commentaire)throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        req = "UPDATE AS_PROMOTIONTYPEABONNEMENT SET COMMENTAIRE='"+commentaire+"' WHERE IDTYPEABONNEMENT='"+idTypeAbonnement+"' AND IDPROMOTION='"+idPromotion+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            except = "["+e.getMessage()+"]";
        }
        if(res!=null){
            result = "[ok]";
        }else if(res==null) result = except;
        return result;
    }
    
    /*Fonction pour mettre à jour le statut d'un panier*/
    public static String update_statut_panier(String id, String statut)throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        req = "UPDATE AS_PANIER_ARTICLE SET STATUTPANIER='"+statut+"' WHERE ID='"+id+"'";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            except = "["+e.getMessage()+"]";
        }
        if(res!=null){
            result = "[ok]";
        }else if(res==null) result = except;
        return result;
    }
    
    /*Fonction pour mettre � jour l'horaire de livraison*/
    public static String modifier_horaireLivraison (String id, String debutCommande, String finCommande, String debutLivraison, String finLivraison, String commentaire) throws Exception{
        String result = null;
        debutCommande = "TO_DATE('"+debutCommande+"','HH24:MI')";
        finCommande = "TO_DATE('"+finCommande+"','HH24:MI')";
        debutLivraison = "TO_DATE('"+debutLivraison+"','HH24:MI')";
        finLivraison = "TO_DATE('"+finLivraison+"','HH24:MI')";
        String req = "UPDATE AS_HORAIRELIVRAISON SET DEBUTCOMMANDE= "+debutCommande+" ,FINCOMMANDE= "+finCommande+" , DEBUTLIVRAISON= "+debutLivraison+" ,FINLIVRAISON= "+finLivraison+" ,COMMENTAIRE= '"+commentaire+"' WHERE ID='"+id+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier le d�tail d'un produit*/
    public static String modifier_detailProduit(String id, String idProduit, String commentaire, String energie, String a_energie, String proteine, String a_proteine, String carbohytrateGlycemique, String a_carbohytrateGlycemique, String totalSucre, String a_totalSucre, String totalSodium, String a_totalSodium, String cholesterole, String a_cholesterole, String totalMatiereGrasse, String a_totalMatiereGrasse, String totalMatiereGrasseMonoSaturee, String a_totalMatiereGrasseMonoSaturee, String totalMatiereGrassePolyinsaturee, String a_totalMatiereGrassePolyinsaturee, String fibreDietetique, String a_fibreDietetique, String totalKJ, String a_totalKJ, String totalCal, String a_totalCal) throws Exception{
        String result = null;
        
        String reqModif = "UPDATE AS_DETAILPRODUIT SET ID='"+id+"' ";
        /*Commentaire*/
        if(commentaire==null || commentaire.compareTo("")==0) reqModif = reqModif+", COMMENTAIRE = null "; else reqModif = reqModif + ", COMMENTAIRE ='"+commentaire+"'";
        if(energie==null || energie.compareTo("")==0) reqModif = reqModif+", ENERGIE = null "; else reqModif = reqModif + ", ENERGIE ='"+energie+"'";
        if(a_energie==null || a_energie.compareTo("")==0) reqModif = reqModif+", A_ENERGIE = null "; else reqModif = reqModif + ", A_ENERGIE ='"+a_energie+"'";
        if(proteine==null || proteine.compareTo("")==0) reqModif = reqModif+", PROTEINE = null "; else reqModif = reqModif + ", PROTEINE ='"+proteine+"'";
        if(a_proteine==null || a_proteine.compareTo("")==0) reqModif = reqModif+", A_PROTEINE = null "; else reqModif = reqModif + ", A_PROTEINE ='"+a_proteine+"'";
        if(carbohytrateGlycemique==null || carbohytrateGlycemique.compareTo("")==0) reqModif = reqModif+", CARBOHYTRATEGLYCEMIQUE = null "; else reqModif = reqModif + ", CARBOHYTRATEGLYCEMIQUE ='"+carbohytrateGlycemique+"'";
        if(a_carbohytrateGlycemique==null || a_carbohytrateGlycemique.compareTo("")==0) reqModif = reqModif+", A_CARBOHYTRATEGLYCEMIQUE = null "; else reqModif = reqModif + ", A_CARBOHYTRATEGLYCEMIQUE ='"+a_carbohytrateGlycemique+"'";
        if(totalSucre==null || totalSucre.compareTo("")==0) reqModif = reqModif+", TOTALSUCRE = null "; else reqModif = reqModif + ", TOTALSUCRE ='"+totalSucre+"'";
        if(a_totalSucre==null || a_totalSucre.compareTo("")==0) reqModif = reqModif+", A_TOTALSUCRE = null "; else reqModif = reqModif + ", A_TOTALSUCRE ='"+a_totalSucre+"'";
        if(totalSodium==null || totalSodium.compareTo("")==0) reqModif = reqModif+", TOTALSODIUM = null "; else reqModif = reqModif + ", TOTALSODIUM ='"+totalSodium+"'";
        if(a_totalSodium==null || a_totalSodium.compareTo("")==0) reqModif = reqModif+", A_TOTALSODIUM = null "; else reqModif = reqModif + ", A_TOTALSODIUM ='"+a_totalSodium+"'";
        if(cholesterole==null || cholesterole.compareTo("")==0) reqModif = reqModif+", CHOLESTEROLE = null "; else reqModif = reqModif + ", CHOLESTEROLE ='"+cholesterole+"'";
        if(a_cholesterole==null || a_cholesterole.compareTo("")==0) reqModif = reqModif+", A_CHOLESTEROLE = null "; else reqModif = reqModif + ", A_CHOLESTEROLE ='"+a_cholesterole+"'";
        if(totalMatiereGrasse==null || totalMatiereGrasse.compareTo("")==0) reqModif = reqModif+", TOTALMATIEREGRASSE = null "; else reqModif = reqModif + ", TOTALMATIEREGRASSE ='"+totalMatiereGrasse+"'";
        if(a_totalMatiereGrasse==null || a_totalMatiereGrasse.compareTo("")==0) reqModif = reqModif+", A_TOTALMATIEREGRASSE = null "; else reqModif = reqModif + ", A_TOTALMATIEREGRASSE ='"+a_totalMatiereGrasse+"'";
        if(totalMatiereGrasseMonoSaturee==null || totalMatiereGrasseMonoSaturee.compareTo("")==0) reqModif = reqModif+", MATIEREGRASSEMONOSATUREE = null "; else reqModif = reqModif + ", MATIEREGRASSEMONOSATUREE ='"+totalMatiereGrasseMonoSaturee+"'";
        if(a_totalMatiereGrasseMonoSaturee==null || a_totalMatiereGrasseMonoSaturee.compareTo("")==0) reqModif = reqModif+", A_MATIEREGRASSEMONOSATUREE = null "; else reqModif = reqModif + ", A_MATIEREGRASSEMONOSATUREE ='"+a_totalMatiereGrasseMonoSaturee+"'";
        if(totalMatiereGrassePolyinsaturee==null || totalMatiereGrassePolyinsaturee.compareTo("")==0) reqModif = reqModif+", TOTALMATIEREGRASSEPOLYINSATUREE = null "; else reqModif = reqModif + ", TOTALMATIEREGRASSEPOLYINSATUREE ='"+totalMatiereGrassePolyinsaturee+"'";
        if(a_totalMatiereGrassePolyinsaturee==null || a_totalMatiereGrassePolyinsaturee.compareTo("")==0) reqModif = reqModif+", A_TOTALMATIEREGRASSEPOLYINSATUREE = null "; else reqModif = reqModif + ", A_TOTALMATIEREGRASSEPOLYINSATUREE ='"+a_totalMatiereGrassePolyinsaturee+"'";
        if(fibreDietetique==null || fibreDietetique.compareTo("")==0) reqModif = reqModif+", FIBREDIETETIQUE = null "; else reqModif = reqModif + ", FIBREDIETETIQUE ='"+fibreDietetique+"'";
        if(a_fibreDietetique==null || a_fibreDietetique.compareTo("")==0) reqModif = reqModif+", A_FIBREDIETETIQUE = null "; else reqModif = reqModif + ", A_FIBREDIETETIQUE ='"+a_fibreDietetique+"'";
        if(totalKJ==null || totalKJ.compareTo("")==0) reqModif = reqModif+", TOTALKJ = null "; else reqModif = reqModif + ", TOTALKJ ='"+totalKJ+"'";
        if(a_totalKJ==null || a_totalKJ.compareTo("")==0) reqModif = reqModif+", A_TOTALKJ = null "; else reqModif = reqModif + ", A_TOTALKJ ='"+a_totalKJ+"'";
        if(totalCal==null || totalCal.compareTo("")==0) reqModif = reqModif+", TOTALCAL = null "; else reqModif = reqModif + ", TOTALCAL ='"+totalCal+"'";
        if(a_totalCal==null || a_totalCal.compareTo("")==0) reqModif = reqModif+", A_TOTALCAL = null "; else reqModif = reqModif + ", A_TOTALCAL ='"+a_totalCal+"'";
        reqModif = reqModif + " WHERE ID='"+id+"'";
        
        try{
            System.out.println(reqModif);
            String startAction = ExecuteFunction.as_executeQuery(reqModif); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    public static String modifier_imageProduit(String idProduit, String image) throws Exception{
        String result = null;
        String req = "UPDATE AS_PRODUITS SET PHOTO='"+image+"' WHERE ID='"+idProduit+"'";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "notok";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    public static String modifier_champTable(String table, String champ, String valChamp, String conditionChamp, String conditionValChamp) throws Exception{
        String result = null;
        String req = "UPDATE "+table+" SET "+champ+"='"+valChamp+"' WHERE "+conditionChamp+" ='"+conditionValChamp+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "notok";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier un abonnement*/
    public static String modifier_abonnement(String id, String typeAbonnement, String idUtilisateur, String debut, String fin, String statut, String idSession, String commentaire, String nbrMois) throws Exception{
        String result = null;
        /*Date de debut*/
        String dateDebut = "TO_DATE('"+debut+"','YYYY-MM-DD')";
        /*Date de fin*/
        String dateFin = "TO_DATE('"+fin+"','YYYY-MM-DD')";
        /*Commentaire*/
        if(commentaire==null) commentaire ="null";
        else commentaire="'"+commentaire+"'";
        String requete = "UPDATE AS_ABONNEMENT SET IDTYPEABONNEMENT='"+typeAbonnement+"', IDUTILISATEUR='"+idUtilisateur+"', DATEDEBUTABONNEMENT = "+debut+" , DATEFINABONNEMENT = "+fin+", STATUTABONNEMENT ="+statut+ ", COMMENTAIRE = "+commentaire+" , IDSESSION='"+idSession+"', NBRMOIS="+nbrMois+" WHERE ID ='"+id+"'";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier un abonnement*/
    public static String modifier_statut_abonnement(String id, String statut) throws Exception{
        String result = null;
        String requete = "UPDATE AS_REINITIALISATION_MDP SET STATUT ="+statut+ " WHERE ID ='"+id+"'";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier d'une demande de r�initialisation de mot de passe*/
    public static String modifier_statutReinitMdp(String id, String statut) throws Exception{
        String result = null;
        String requete = "UPDATE AS_REINITIALISATION_MDP SET STATUT ="+statut+ " WHERE ID ='"+id+"'";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier la photo de prodil d'un utilisateur*/
    public static String modifier_photoProfilUtilisateur(String idUtilisateur, String image) throws Exception{
        String result = null;
        String requete = "UPDATE AS_UTILISATEUR SET PHOTO ='"+image+"' WHERE IDUTILISATEUR ='"+idUtilisateur+"'";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier l'�tat d'une commande client*/
    public static String modifier_etat_commandeClient(String id, String etat) throws Exception{
        String result = null;
        String requete = "UPDATE AS_COMMANDECLIENT SET ETAT ="+etat+ " WHERE ID ='"+id+"'";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    //Fonction pour modifier les informations d'une image
    public static String modifier_image(String id, String idCategorieImage, String nomImage, String lienImage, String description, String alt, String commentaire) throws Exception{
        String result = null;
        /*Commentaire*/
        if(commentaire==null || commentaire.compareTo("")==0) commentaire ="null";
        else commentaire="'"+commentaire+"'";
        
        /*Description*/
        if(description==null || description.compareTo("")==0) description ="null";
        else description="'"+description+"'";
        
        /*Alt*/
        if(alt==null || alt.compareTo("")==0) alt ="null";
        else alt="'"+alt+"'";
        
        String requete = "UPDATE AS_IMAGES SET IDCATEGORIEIMAGE = '"+idCategorieImage+"'";
        requete = requete + " , NOMIMAGE='"+nomImage+"'";
        requete = requete + " , LIENIMAGE='"+lienImage+"'";
        requete = requete + " , ALT="+alt;
        requete = requete + " , DESCRIPTIONIMAGE="+description;
        requete = requete + " , COMMENTAIRE="+commentaire;
        requete = requete + " WHERE ID='"+id+"'";
        try{
            System.out.println(requete);
            String startAction = ExecuteFunction.as_executeQuery(requete); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }

    /*Fonction pour modifier un ingr�dient*/
    public static String modifier_ingredient(String id, String libelle, String seuil, String unite, String quantiteParPack, String pu, String actif) throws Exception{
        String result = null;
        String req = "UPDATE AS_INGREDIENTS SET LIBELLE='"+libelle+"', SEUIL="+seuil+" , QUANTITEPARPACK="+quantiteParPack+" ,PU="+pu+", ACTIF="+actif+" WHERE ID='"+id+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier un tarif d'abonnement*/
    public static String modifier_tarifAbonnement(String id, String idDureeTarifAbonnement, String dateApplication, String montant, String commentaire) throws Exception{
        String result = null;
        String val_commentaire = "";
        String val_dateApplication = "";
        val_dateApplication = " TO_DATE('"+dateApplication+"','YYYY-MM-DD') ";
        if(commentaire.compareTo("")==0 || commentaire == null){
            val_commentaire = "null";
        }
        String req = "UPDATE AS_TARIFABONNEMENT SET IDDUREEABONNEMENT='"+idDureeTarifAbonnement+"', MONTANT="+montant+" , DATAPPLICATION="+val_dateApplication+" , COMMENTAIRE="+val_commentaire+" WHERE ID='"+id+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier une dur�e d'abonnement*/
    public static String modifier_dureeAbonnement(String id, String idTypeAbonnement, String nbrMois, String commentaire, String idSession) throws Exception{
        String result = null; 
        String val_commentaire = null;
        if(commentaire.compareTo("")==0 || commentaire==null){
            val_commentaire = "null";
        }
        else val_commentaire = "'"+commentaire+"'";
        String req = "UPDATE AS_DUREEABONNEMENT SET IDTYPEABONNEMENT='"+idTypeAbonnement+"', NBRMOIS="+nbrMois+" ,COMMENTAIRE="+val_commentaire+" ,idSession='"+idSession+"' WHERE ID='"+id+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier une demande d'abonnement*/
    public static String update_demandeAbonnement(String id, String idUtilisateur, String idDureeAbonnement, String dateDemande, String statut, String idSession) throws Exception{
        String result = null;
        String val_dateDemande = null;
        val_dateDemande = "TO_DATE('"+dateDemande+"','YYYY-MM-DD')";
        String req = "UPDATE AS_DEMANDE_ABONNEMENT SET IDUTILISATEUR='"+idUtilisateur+"', IDDUREEABONNEMENT='"+idDureeAbonnement+"', DATEDEMANDE="+val_dateDemande+", STATUT="+statut+", IDSESSION='"+idSession+"' WHERE ID='"+id+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour modifier une demande d'abonnement*/
    public static String update_statutDemandeAbonnement(String id, String statut) throws Exception{
        String result = null;
        String req = "UPDATE AS_DEMANDE_ABONNEMENT SET STATUT="+statut+" WHERE ID='"+id+"'";
        try{
            System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*--------------------DELETE--------------------------*/
    /*Fonction pour supprimer une ligne d'une table grâce à un paramètre*/
    public static String supprimer_ligne_table(String table,String champ, String valeur)  throws Exception{
        String result = null;
        String req = "DELETE FROM "+table+" WHERE "+champ+" = '"+valeur+"'";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "notok";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour supprimer une ligne dans une table id val desce*/
    public static String supprimer_id_val_desce(String table,String id)  throws Exception{
        String result = null;
        String req = null;
        JSONArray res = null;
        String except = null;
        req = "DELETE FROM "+table+" WHERE ID='"+id+"'";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            except = "["+e.getMessage()+"]";
        }
        if(res!=null){
            result = "[ok]";
        }else if(res==null) result = except;
        return result;
    }

    /*Fonction pour supprimer une ligne d'une table avec une seule condition*/
    public static String delete_1critere(String table,String colonne, String valeur)  throws Exception{
        String result = null;
        String req = "DELETE FROM "+table+" WHERE "+colonne+" = '"+valeur+"'";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }

    /*Fonction pour supprimer une ligne d'une table avec une seule condition*/
    public static String delete_2criteres(String table,String colonne1, String valeur1, String colonne2, String valeur2)  throws Exception{
        String result = null;
        String req = "DELETE FROM "+table+" WHERE "+colonne1+" = '"+valeur1+"' AND "+colonne2+" = '"+valeur2+"'";
        try{
			System.out.println(req);
            String startAction = ExecuteFunction.as_executeQuery(req); 
			if(startAction.compareTo("ok")==0){
				result = "[ok]";
			}else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }

    /*--------------------RECHERCHE--------------------------*/
    /*Remarque : Utilisateur 1 : client ; 2 : administrateur*/
    
    /*Fonction pour rechercher un abonnement*/
    public static JSONArray rechercher_abonnement(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_V_ABONNEMENT WHERE NOMABONNEMENT like "+motCle;
        req = req+" OR IDABONNEMENT like "+motCle;
        req = req+" OR IDTYPEABONNEMENT like "+motCle;
        req = req+" OR DATEDEBUTABONNEMENT like "+motCle;
        req = req+" OR DATEFINABONNEMENT like "+motCle;
        req = req+" OR COMMENTAIRE like "+motCle;
        req = req+" OR NOMUTILISATEUR like "+motCle;
        //req = req+" OR PRENOMUTILISATEUR like "+motCle;
        req = req+" OR IDENTIFIANT like "+motCle;
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour rechercher un administrateur*/
    public static JSONArray rechercher_administrateur(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_ADMIN WHERE 2<1";
            req = "SELECT * FROM AS_ADMIN WHERE NOM like "+motCle;
            req = req+" OR PRENOM like "+motCle;
            req = req+" OR IDENTIFIANT like "+motCle;
            req = req+" OR EMAIL like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour rechercher un adresse*/
    public static JSONArray rechercher_adresse(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_V_ADRESSE WHERE 2<1";
            req = "SELECT * FROM AS_V_ADRESSE WHERE IDQUARTIER like "+motCle;
            req = req+" OR IDUTILISATEUR like "+motCle;
            req = req+" OR IDTYPEADRESSE like "+motCle;
            req = req+" OR ADRESSE like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
            req = req+" OR NOMUTILISATEUR like "+motCle;
            req = req+" OR PRENOMUTILISATEUR like "+motCle;
            req = req+" OR IDENTIFIANT like "+motCle;
            req = req+" OR NOMTYPEADRESSE like "+motCle;
            req = req+" OR NOMQUARTIER like "+motCle;
            req = req+" OR NOMSECTEUR like "+motCle;
            req = req+" OR IDSECTEUR like "+motCle;
            req = req+" OR MONTANT like "+motCle;
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    /*Fonction pour rechercher dans une table id val desce*/
    public static JSONArray rechercher_idValDesce(String motCle, String table) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM "+table+" WHERE VAL like "+motCle+" OR DESCE like "+motCle;
            req = req+" OR ID like "+motCle;
        try {
             
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher l'historique d'un utilisateur
    public static JSONArray rechercher_historiqueUtilisateur(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_HISTORIQUE_UTILISATEUR WHERE 2<1";
            req = "SELECT * FROM AS_HISTORIQUE_UTILISATEUR WHERE IDUTILISATEUR like "+motCle;
            req = req+" OR NOMUTILISATEUR like "+motCle;
            req = req+" OR PRENOMUTILISATEUR like "+motCle;
            req = req+" OR NAISSANCEUTILISATEUR like "+motCle;
            req = req+" OR CIN like "+motCle;
            req = req+" OR DATEDELIVRANCECIN like "+motCle;
            req = req+" OR LIEUDELIVRANCECIN like "+motCle;
            req = req+" OR LIENCIN_RECTO like "+motCle;
            req = req+" OR LIENCIN_VERSO like "+motCle;
            req = req+" OR EMAILUTILISATEUR like "+motCle;
            req = req+" OR FONCTION like "+motCle;
            req = req+" OR SOCIETE like "+motCle;
            req = req+" OR IDENTIFIANT like "+motCle;
            req = req+" OR STATUTUTILISATEUR like "+motCle;
            req = req+" OR IDSESSION like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
            req = req+" OR PHOTO like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Rechercher une image
    public static JSONArray rechercher_image(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_V_IMAGES WHERE NOM like "+motCle;
            req = req+" OR ID like "+motCle;
            req = req+" OR IDCATEGORIE like "+motCle;
            req = req+" OR CATEGORIE like "+motCle;
            req = req+" OR LIEN like "+motCle;
            req = req+" OR DESCRIPTION like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
        try {
            System.out.println(req);
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Rechercher un ingrédient de produit
    public static JSONArray rechercher_ingredientProduit(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_RECETTE_LIBELLE WHERE PRODUITS like "+motCle;
            req = req+" OR ID like "+motCle;
            req = req+" OR IDPRODUITS like "+motCle;
            req = req+" OR IDINGREDIENTS like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Rechercher un ingrédient
    public static JSONArray rechercher_ingredient(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_INGREDIENTS WHERE LIBELLE like "+motCle;
            req = req+" OR ID like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Rechercher un message
    public static JSONArray rechercher_message(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_MESSAGE WHERE 2<1 ";
            req = "SELECT * FROM AS_MESSAGE WHERE ID like "+motCle;
            req = req+" OR IDTYPEMESSAGE like "+motCle;
            req = req+" OR NOM like "+motCle;
            req = req+" OR EMAIL like "+motCle;
            req = req+" OR CONTENU like "+motCle;
            req = req+" OR ETATMESSAGE like "+motCle;
            req = req+" OR DATEENVOI like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Rechercher un paiement
    public static JSONArray rechercher_paiement(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_PAIEMENT_LIBELLE WHERE 2<1 ";
            req = "SELECT * FROM AS_PAIEMENT WHERE ID like "+motCle;
            req = req+" OR DATY like "+motCle;
            req = req+" OR MONTANT like "+motCle;
            req = req+" OR MODEPAIEMENT like "+motCle;
            req = req+" OR IDOBJET like "+motCle;
            req = req+" OR OBSERVATION like "+motCle;
            req = req+" OR CLIENT like "+motCle;
            req = req+" OR CAISSE like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher un produit
    public static JSONArray rechercher_produit(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_PRODUITS_LIBELLEPRIX_CATEG WHERE NOM like "+motCle;
            req = req+" OR DESIGNATION like "+motCle;
            req = req+" OR ID like "+motCle;
            req = req+" OR PHOTO like "+motCle;
            req = req+" OR TYPEPRODUIT like "+motCle;
            req = req+" OR CALORIE like "+motCle;
            req = req+" OR PA like "+motCle;
            req = req+" OR POIDS like "+motCle;
            req = req+" OR PU like "+motCle;
            req = req+" OR ID_TYPEPRODUIT like "+motCle;
            req = req+" OR VAL_TYPEPRODUIT like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher une promotion
    public static JSONArray rechercher_promotion(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_V_PROMOTION WHERE NOMPROMOTION like "+motCle;
            req = req+" OR IDTYPEABONNEMENT like "+motCle;
            req = req+" OR NOMABONNEMENT like "+motCle;
            req = req+" OR ID like "+motCle;
            req = req+" OR DATEDEBUT like "+motCle;
            req = req+" OR DATEFIN like "+motCle;
            req = req+" OR DESCRIPTIONPROMOTION like "+motCle;
            req = req+" OR IDSESSION like "+motCle;
            req = req+" OR LIENIMAGE like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher une cause de promotion
    public static JSONArray rechercher_promotionCause(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_V_PROMOTIONCAUSE WHERE 1>2";
            req = "SELECT * FROM AS_V_PROMOTIONCAUSE WHERE IDPROMOTION like "+motCle;
            req = req+" OR ID like "+motCle;
            req = req+" OR IDPRODUIT like "+motCle;
            req = req+" OR NBRPRODUITACHETE like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
            req = req+" OR NOM like "+motCle;
            req = req+" OR PU like "+motCle;
            req = req+" OR NOMPROMOTION like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher une consequence de promotion
    public static JSONArray rechercher_promotionConsequence(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_V_PROMOTIONCONSEQUENCE WHERE 1>2";
        
            req = "SELECT * FROM AS_V_PROMOTIONCONSEQUENCE WHERE IDPROMOTION like "+motCle;
            req = req+" OR ID like "+motCle;
            req = req+" OR IDPROMOTION like "+motCle;
            req = req+" OR IDPRODUIT like "+motCle;
            req = req+" OR REMISEPRODUITOFFERT like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
            req = req+" OR NOM like "+motCle;
            req = req+" OR NOMPROMOTION like "+motCle;
        
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher une promotion liée à un type d'abonnement
    public static JSONArray rechercher_promotionTypeAbonnement(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_V_PROMOTIONTYPEABONNEMENT WHERE NOMPROMOTION like "+motCle;
        
            req = req+" OR IDTYPEABONNEMENT like "+motCle;
            req = req+" OR IDPROMOTION like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
        
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher un quartier
    public static JSONArray rechercher_quartier(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_V_QUARTIER WHERE 2<1";
            req = "SELECT * FROM AS_V_QUARTIER WHERE ID like "+motCle;
            req = req+" OR NOM like "+motCle;
            req = req+" OR SECTEUR like "+motCle;
            req = req+" OR NOMSECTEUR like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher un téléphone
    public static JSONArray rechercher_telephone(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_V_TELEPHONE WHERE 2<1";
            req = "SELECT * FROM AS_V_TELEPHONE WHERE ID like "+motCle;
            req = req+" OR IDUTILISATEUR like "+motCle;
            req = req+" OR NUMEROTELEPHONE like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
            req = req+" OR NOMUTILISATEUR like "+motCle;
            req = req+" OR PRENOMUTILISATEUR like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher une commande d'un client
    public static JSONArray rechercher_commandeClient(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_V_COMMANDECLIENT WHERE 2<1";
            req = "SELECT * FROM AS_V_TELEPHONE WHERE ID like "+motCle;
            req = req+" OR DATESAISIE like "+motCle;
            req = req+" OR DATECOMMANDE like "+motCle;
            req = req+" OR NUCOMMANDE like "+motCle;
            req = req+" OR RESPONSABLE like "+motCle;
            req = req+" OR CLIENT like "+motCle;
            req = req+" OR TYPECOMMANDE like "+motCle;
            req = req+" OR DATELIV like "+motCle;
            req = req+" OR ADRESSELIV like "+motCle;
            req = req+" OR HEURELIV like "+motCle;
            req = req+" OR DISTANCE like "+motCle;
            req = req+" OR REMARQUE like "+motCle;
            req = req+" OR SECTEUR like "+motCle;
            req = req+" OR OBSERVATION like "+motCle;
            req = req+" OR QUARTIER like "+motCle;
            req = req+" OR IDUTILISATEUR like "+motCle;
            req = req+" OR IDSESSION like "+motCle;
            req = req+" OR NOMUTILISATEUR like "+motCle;
            req = req+" OR PRENOMUTILISATEUR like "+motCle;
            req = req+" OR NOMRESPONSABLE like "+motCle;
            req = req+" OR PRENOMRESPONSABLE like "+motCle;
            req = req+" OR IDTYPERESPONSABLE like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher l'historique d'un utilisateur
    public static JSONArray rechercher_utilisateur(String motCle) throws Exception{
        JSONArray result = null;
        motCle = "'%"+motCle+"%'";
        String req = "SELECT * FROM AS_UTILISATEUR WHERE 2<1";
            req = "SELECT * FROM AS_UTILISATEUR WHERE IDUTILISATEUR like "+motCle;
            req = req+" OR NOMUTILISATEUR like "+motCle;
            req = req+" OR PRENOMUTILISATEUR like "+motCle;
            req = req+" OR NAISSANCEUTILISATEUR like "+motCle;
            req = req+" OR CIN like "+motCle;
            req = req+" OR DATEDELIVRANCECIN like "+motCle;
            req = req+" OR LIEUDELIVRANCECIN like "+motCle;
            req = req+" OR LIENCIN_RECTO like "+motCle;
            req = req+" OR LIENCIN_VERSO like "+motCle;
            req = req+" OR EMAILUTILISATEUR like "+motCle;
            req = req+" OR FONCTION like "+motCle;
            req = req+" OR SOCIETE like "+motCle;
            req = req+" OR IDENTIFIANT like "+motCle;
            req = req+" OR STATUTUTILISATEUR like "+motCle;
            req = req+" OR IDSESSION like "+motCle;
            req = req+" OR COMMENTAIRE like "+motCle;
            req = req+" OR PHOTO like "+motCle;
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher l'historique d'un utilisateur avec pagination
    public static JSONArray rechercher_produit_utilisateur_paginee(int limite,int debut,float prixMin,float prixMax,String nom,String champ,String ordre, String categorie) throws Exception{
        JSONArray result = null;
        String nom_avant = "'%"+nom+"'";
        String nom_apres = "'"+nom+"%'";
        String nom_avant_apres = "'%"+nom+"%'";
        String req = "SELECT * FROM AS_PRODUITS_LIBELLEPRIX WHERE AFFICHER!=0";
        if(nom!=null) req = req+" AND ( UPPER(NOM) LIKE UPPER("+nom_avant+")";
        if(nom!=null) req = req+" OR UPPER(NOM) LIKE UPPER("+nom_apres+")";
        if(nom!=null) req = req+" OR UPPER(NOM) LIKE UPPER("+nom_avant_apres+") )";
        if(prixMin!=0) req = req + " AND PU>="+prixMin;
        if(prixMax!=0) req = req + " AND PU<="+prixMax;
        if(categorie!=null && categorie.compareTo("")!=0) req = req + " AND TYPEPRODUIT='"+categorie+"' ";
        if(champ!=null && ordre!=null){
            if(champ.compareTo("")!=0 && ordre.compareTo("")!=0){
                req = req + " ORDER BY "+champ+" "+ordre;
            }    
        }
        debut = debut +1;
        int nouveauDebut = ((debut*limite)+1)-limite;
        int nouvelleFin = nouveauDebut + limite;
        String requeteFinale = "SELECT * FROM ( SELECT a.*, ROWNUM rnum FROM ("+req+") a WHERE ROWNUM <"+nouvelleFin+") WHERE rnum >="+nouveauDebut; 
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour rechercher l'historique d'un utilisateur sans pagination
    public static JSONArray rechercher_produit_utilisateur_tout(float prixMin,float prixMax,String nom,String champ,String ordre, String categorie) throws Exception{
        JSONArray result = null;
        String nom_avant = "'%"+nom+"'";
        String nom_apres = "'"+nom+"%'";
        String nom_avant_apres = "'%"+nom+"%'";
        String req = "SELECT * FROM AS_PRODUITS_LIBELLEPRIX WHERE AFFICHER!=0";
        if(nom!=null) req = req+" AND ( UPPER(NOM) LIKE UPPER("+nom_avant+")";
        if(nom!=null) req = req+" OR UPPER(NOM) LIKE UPPER("+nom_apres+")";
        if(nom!=null) req = req+" OR UPPER(NOM) LIKE UPPER("+nom_avant_apres+") )";
        if(prixMin!=0) req = req + " AND PU>="+prixMin;
        if(prixMax!=0) req = req + " AND PU<="+prixMax;
        if(categorie!=null && categorie.compareTo("")!=0) req = req + " AND TYPEPRODUIT='"+categorie+"' ";
        if(champ!=null && ordre!=null){
            if(champ.compareTo("")!=0 && ordre.compareTo("")!=0){
                req = req + " ORDER BY "+champ+" "+ordre;
            }    
        }
        try {
            System.out.println(req); 
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
    //Fonction pour avoir les produits au m�me prix
    
    public static JSONArray produitsMemePrix(String idProduit, int prix, int marge, int limite) throws Exception{
        JSONArray result = null;
        int prixMin = prix - marge;
        int prixMax = prix + marge;
        System.out.println("ok1");
        String req1 = "SELECT * FROM AS_PRODUITS_AFFICHE WHERE PU <= "+prixMax+" AND PU >="+prixMin;
        String req = "SELECT * FROM ("+req1+") WHERE ID != '"+idProduit+"'";
        String requeteFinale = "SELECT * FROM ( SELECT a.*, ROWNUM rnum FROM ("+req+") a WHERE ROWNUM <"+limite+") WHERE rnum >=1";
        try {
            result = ExecuteFunction.resultJSON(requeteFinale);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
            
    public static JSONArray liste_produitMemePrix(String idProduit, int prix, int marge, int limite) throws Exception{
        JSONArray result = null;
        int prixMin = prix - marge;
        int prixMax = prix + marge;
        System.out.println("ok1");
        String req1 = "SELECT * FROM AS_PRODUITS_AFFICHE WHERE PU <= "+prixMax+" AND PU >="+prixMin;
        System.out.println("ok req1");
        //String req = "SELECT * FROM ("+req1+") WHERE ID != '"+idProduit+"'";
        String requeteFinale = "SELECT * FROM ( SELECT a.*, ROWNUM rnum FROM ("+req1+") a WHERE ROWNUM <"+limite+") WHERE rnum >=1";
        System.out.println("ok requete finale");
        System.out.println(requeteFinale);
        try {
            System.out.println("try ok");
            result = ExecuteFunction.resultJSON(requeteFinale);
            System.out.println("try termin�");
        } catch (Exception e) {
            System.out.println("erreur");
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    
     //Fonction pour avoir les produits au m�me prix
    
    public static JSONArray select_abtActuel_idTypeAbt(String idTypeAbonnement) throws Exception{
        JSONArray result = null;
        String reqDateActuelle = "(SELECT SYSDATE FROM DUAL) ";
        String req = "SELECT * FROM AS_ABONNEMENT WHERE IDTYPEABONNEMENT = '"+idTypeAbonnement+"'";
        req = req+" AND DATEFINABONNEMENT>"+reqDateActuelle;
        req = req+" AND DATEDEBUTABONNEMENT<"+reqDateActuelle;
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    //----------------STATISTIQUE--------------------------
    //Fonction pour r�cup�rer l'�tat de vente d'un mois d'une ann�ee, regroup� par �tat
    public static JSONArray etatVente_moisAnneeEtat(String mois, String annee, String etat){
        JSONArray result = null;
        String req = "SELECT * FROM AS_V_ETATCOMMANDE_MENSUEL WHERE ANNEE_MOIS = '"+mois+"-"+annee+"' AND ETAT = '"+etat+"'";
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    //Fonction pour r�cup�rer le nombre total de produit vendu
    public static JSONArray produitVendu_etat(String etat) throws Exception{
        JSONArray result = null;
        String req = "SELECT SUM(QUANTITE) AS NBR FROM AS_V_ETATCOMMANDE_MENSUEL WHERE ETAT='"+etat+"'";
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    /*---------------------------------------VISITE--------------------------------------*/
    //Fonction pour inserer une visite
    public static String inserer_visite(String idVisite, String dateVisite, String idSession) throws Exception{
        String result = null;     
        String val_dateVisite =  to_date(dateVisite);   
        System.out.println("date de viste : "+val_dateVisite);
        String req = "INSERT INTO AS_VISITE (IDVISITE, DATEVISITE, IDSESSION)"
                + " VALUES ('"+idVisite+"',"+val_dateVisite+",'"+idSession+"')";
        try{
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }

    //Fonction pour compter le nombre de visite se reliant � un idSession et une date
    public static JSONArray select_count_nbrVisite_idSession_date(String idSession, String dateVisite ) throws Exception{
        JSONArray result = null;
        String val_dateVisite =  to_date(dateVisite);   
        String req = "SELECT COUNT(*) AS NBR FROM AS_VISITE WHERE IDSESSION = '"+idSession+"' AND DATEVISITE = "+val_dateVisite;
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    //Fonction pour compter le nombre de visite se reliant � un idSession et une date
    public static JSONArray select_count_nbrVisite_date(String dateVisite)  throws Exception{
        JSONArray result = null;
        String val_dateVisite =  to_date(dateVisite);   
        String req = "SELECT COUNT(*) AS NBR FROM AS_VISITE WHERE DATEVISITE = "+val_dateVisite;
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    //Fonction pour compter le nombre de visite se reliant � un idSession et une date
    public static JSONArray select_count_nbrVisite_2date(String debut, String fin)  throws Exception{
        JSONArray result = null;
        String val_debut =  to_date(debut);   
        String val_fin =  to_date(fin);   
        String req = "SELECT COUNT(*) AS NBR FROM AS_VISITE WHERE DATEVISITE >= "+val_debut+" AND DATEVISITE <="+val_fin;
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    //Fonction pour compter le nombre de visite durant un mois
    public static JSONArray select_count_nbrVisite_mois(String mois, String annee) throws Exception{
        JSONArray result = null;  
        String req = "SELECT QUANTITE AS NBR FROM AS_V_VISITE WHERE annee_mois = '"+mois+"-"+annee+"'";
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    //Fonction pour compter le nombre de message re�u durant un mois
    public static JSONArray messageRecu_mois_annee (String mois, String annee) throws Exception{
        JSONArray result = null;  
        String req = "SELECT * FROM AS_COUNT_MESSAGE WHERE annee_mois = '"+mois+"-"+annee+"'";
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }

    /*--------------------------PARTAGE FACEBOOK----------------------------------*/
    /*Fonction pour inserer un partage facebook*/
    public static String inserer_partage(String idPartage, String datePartage) throws Exception{
        String result = null;     
        String val_datePartage =  to_date(datePartage);   
        System.out.println("date de viste : "+val_datePartage);
        String req = "INSERT INTO AS_PARTAGE_FB (IDPARTAGE, DATEPARTAGE)"
                + " VALUES ('"+idPartage+"',"+val_datePartage+")";
        System.out.println(req);
        try{
            String startAction = ExecuteFunction.as_executeQuery(req); 
            if(startAction.compareTo("ok")==0){
                    result = "[ok]";
            }else result = "[notok]";
        }
        catch(Exception e){
            result = "[notok]";
            System.out.println("Erreur :"+e.getMessage());
        }
        return result;
    }
    //Fonction pour compter le nombre de partage se reliant � une date
    public static JSONArray select_count_nbrPartage_date(String date)  throws Exception{
        JSONArray result = null;
        String val_date =  to_date(date);   
        String req = "SELECT COUNT(*) AS NBR FROM AS_PARTAGE_FB WHERE DATEPARTAGE = "+val_date;
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    //Fonction pour compter le nombre de partage entre deux dates
    public static JSONArray select_count_nbrPartage_2date(String debut, String fin)  throws Exception{
        JSONArray result = null;
        String val_debut =  to_date(debut);   
        String val_fin =  to_date(fin);   
        String req = "SELECT COUNT(*) AS NBR FROM AS_PARTAGE_FB WHERE DATEPARTAGE >= "+val_debut+" AND DATEPARTAGE <="+val_fin;
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
    //Fonction pour compter le nombre de partage durant un mois d'une ann�e
    public static JSONArray select_count_nbrPartage_mois(String mois, String annee) throws Exception{
        JSONArray result = null;  
        String req = "SELECT QUANTITE AS NBR FROM AS_V_PARTAGE_FB WHERE annee_mois = '"+mois+"-"+annee+"'";
        try {
            result = ExecuteFunction.resultJSON(req);
        } catch (Exception e) {
            System.out.println("Erreur :" + e.getMessage());
        }
        return result;
    }
}