// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   Depense.java

package depense;

import bean.ClassMAPTable;
import java.sql.Connection;
import java.sql.Date;

public class Depense extends ClassMAPTable
{

    public Depense()
    {
        super.setNomTable("Depense");
    }

    public String getTuppleID()
    {
        return id;
    }

    public String getAttributIDName()
    {
        return "id";
    }

    public void construirePK(Connection c)
        throws Exception
    {
        super.setNomTable("Depense");
        preparePk("DEP", "getSeqDepense");
        setId(makePK(c));
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setMontant(double montant)
        throws Exception
    {
        if(montant <= 0.0D)
        {
            throw new Exception("montant invalide");
        } else
        {
            this.montant = montant;
            return;
        }
    }

    public double getMontant()
    {
        return montant;
    }

    public void setTypedepense(String typedepense)
    {
        this.typedepense = typedepense;
    }

    public String getTypedepense()
    {
        return typedepense;
    }

    public void setDesignation(String designation)
    {
        this.designation = designation;
    }

    public String getDesignation()
    {
        return designation;
    }

    public void setBeneficiaire(String beneficiaire)
    {
        this.beneficiaire = beneficiaire;
    }

    public String getBeneficiaire()
    {
        return beneficiaire;
    }

    public void setDaty(Date daty)
    {
        this.daty = daty;
    }

    public Date getDaty()
    {
        return daty;
    }

    private String id;
    private double montant;
    private String typedepense;
    private String designation;
    private String beneficiaire;
    private Date daty;
}
