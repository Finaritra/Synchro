package user;

import bean.AdminGen;
import bean.CGenUtil;
import bean.ClassEtat;
import bean.ClassMAPTable;
import mg.allosakafo.commande.*;
import bean.ClassUser;
import bean.ErreurDAO;
import bean.ListeColonneTable;
import bean.RejetTable;
import bean.ResultatEtSomme;
import bean.TypeObjet;
import bean.TypeObjetUtil;
import bean.UnionIntraTable;
import bean.UploadPj;
import config.Config;
import config.Table;
import historique.HistoriqueEJBClient;
import historique.HistoriqueLocal;
import historique.MapHistorique;
import historique.MapRoles;
import historique.MapUtilisateur;
import historique.UtilisateurUtil;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import lc.Direction;
import lc.DirectionUtil;
import mg.cnaps.configuration.Configuration;
import mg.cnaps.messagecommunication.MessageLibelle;
import mg.cnaps.utilisateur.CNAPSUser;

import mg.allosakafo.ded.OrdonnerPayement;
import mg.allosakafo.facturefournisseur.FactureFournisseur;
import mg.allosakafo.fin.EtatdeCaisseDate;
import mg.allosakafo.fin.MvtCaisse;
import mg.allosakafo.fin.Caisse;
import mg.allosakafo.fin.OrdreDePaiement;
import mg.allosakafo.fin.FactureClient;
import mg.allosakafo.commande.CommandeClientDetails;
import mg.allosakafo.commande.Paiement;
import mg.allosakafo.fin.FactureClientDetails;
import mg.allosakafo.stock.BonDeCommande;
import mg.allosakafo.stock.BonDeCommandeFille;
import mg.allosakafo.stock.BonDeLivraison;
import mg.allosakafo.stock.BonDeLivraisonFille;
import mg.allosakafo.stock.MvtStock;
import mg.allosakafo.stock.MvtStockFille;
import mg.allosakafo.produits.Recette;
import mg.allosakafo.produits.Ingredients;
import mg.allosakafo.stock.EtatdeStockDate;

import modules.GestionMessage;
import modules.GestionRole;

import utilisateur.ConstanteUtilisateur;
import utilisateur.HomePageURL;
import utilisateur.UtilisateurRole;
import utilitaire.Constante;
import utilitaire.ConstanteEtat;
import utilitaire.ConstanteUser;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;
import utilitaire.UtilitaireFormule;
import utilitaire.UtilitaireMetier;
import mg.allosakafo.commande.Livraison;
import mg.allosakafo.ded.SituationOp;
import mg.allosakafo.facturefournisseur.DetailsFactureFournisseur;
import mg.allosakafo.fin.EtatOP;
import mg.allosakafo.fin.Report;
import mg.allosakafo.produits.Indisponibilite;
import mg.allosakafo.livraison.LivraisonApresCommande;
import mg.allosakafo.produits.Produits;
import mg.allosakafo.tiers.ClientAlloSakafo;
import mg.allosakafo.secteur.TarifLivraison;
import mg.allosakafo.stock.InventaireMere;
import mg.allosakafo.stock.TransfertStock;
import service.AlloSakafoService;

@Stateful
public class UserEJBBean implements UserEJB, UserEJBRemote, SessionBean {

    SessionContext sessionContext;
    MapUtilisateur u;
    MapUtilisateur uVue;
    String type;
    String idDirection;
    String home_page = "notification/notification-liste.jsp";
    Config c;
    CNAPSUser cnapsUser;
    Configuration[] listeConfig;
    private int exercice;
    private HashMap<String, String> mapAutoComplete = new HashMap<>();

    public HashMap<String, String> getMapAutoComplete() {
        return mapAutoComplete;
    }

    public void setMapAutoComplete(HashMap<String, String> mapAutoComplete) {
        this.mapAutoComplete = mapAutoComplete;
    }

    public int getExercice() {
        return exercice;
    }

    public void setExercice(int exercice) {
        this.exercice = exercice;
    }

    public UserEJBBean() {
        u = null;
    }

    public void ejbRemove() {
//        try {
//            Config.vider();
//            MapHistorique histo = new MapHistorique("logout", "logout", String.valueOf(u.getRefuser()), String.valueOf(u.getRefuser()));
//            histo.insertToTable();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public void ejbActivate() {
    }

    public void ejbPassivate() {
    }

    @Override
    public int payerApayer(String idCommande, String idCaisse) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            CommandeClient crt = new CommandeClient();
            crt.setId(idCommande);
            CommandeClient[] liste = (CommandeClient[]) CGenUtil.rechercher(crt, null, null, c, "");
            if (liste.length == 0) {
                throw new Exception("commande non existante");
            }
            crt = liste[0];
            if(crt.getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0){
                throw new Exception("Action non permise");
            }
            return crt.payerApayer(idCaisse, this.getUser().getTuppleID(), c);
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    @Override
    public void annulerDetailsCommande(String[] id) throws Exception {
        if (u.isSuperUser() == false) {
            throw new Exception("pas de droit");
        }
        CommandeService cs = new CommandeService();
        cs.annulerDetailsCommande(id, null, this.getUser().getTuppleID());
    }

    public void setSessionContext(SessionContext sessionContext) {
        this.sessionContext = sessionContext;
    }

    /*
     * public TypeObjet[] findTypeObjet(String nomTable, String id, String typ)
     * throws Exception { try { TypeObjetUtil to = new TypeObjetUtil();
     * TypeObjet atypeobjet[] = to.findTypeObjet(nomTable, id, typ); return
     * atypeobjet; } catch(Exception ex) { throw new Exception(ex.getMessage());
     * } }
     */
    public CNAPSUser getCnapsUser() {
        return cnapsUser;
    }

    public Configuration[] getListeConfiguration() {
        return listeConfig;
    }

    @Override
    public String createUtilisateurs(String loginuser, String pwduser, String nomuser, String adruser, String teluser, String idrole) throws Exception {
        HistoriqueLocal rl = null;
        try {
            if (u.getIdrole().compareTo("dg") == 0) {
                rl = HistoriqueEJBClient.lookupHistoriqueEJBBeanLocal();
                String s = rl.createUtilisateurs(loginuser, pwduser, nomuser, adruser, teluser, idrole, u.getTuppleID());
                return s;
            } else {
                throw new Exception("Erreur de droit");
            }
        } catch (CreateException ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public String updateUtilisateurs(String refuser, String loginuser, String pwduser, String nomuser, String adruser, String teluser, String idrole) throws Exception {
        HistoriqueLocal rl = null;
        try {
            rl = HistoriqueEJBClient.lookupHistoriqueEJBBeanLocal();
            if (u.getIdrole().compareTo("dg") == 0) {
                return rl.updateUtilisateurs(refuser, loginuser, pwduser, nomuser, adruser, teluser, idrole, u.getTuppleID());
            } else if (String.valueOf(u.getRefuser()).compareTo(refuser) == 0) {
                return rl.updateUtilisateurs(refuser, loginuser, pwduser, nomuser, adruser, teluser, u.getIdrole(), u.getTuppleID());
            } else {
                throw new Exception("Erreur de droit");
            }
        } catch (CreateException ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public int deleteUtilisateurs(String refuser) throws Exception {
        HistoriqueLocal rl = null;

        try {
            if (u.getIdrole().compareTo("admin") == 0 || u.getIdrole().compareTo("dg") == 0 || u.getIdrole().compareTo("adminFacture") == 0) {
                rl = HistoriqueEJBClient.lookupHistoriqueEJBBeanLocal();
                int i = rl.deleteUtilisateurs(refuser, u.getTuppleID());
                return i;
            } else {
                throw new Exception("Erreur de droit");
            }
        } catch (CreateException ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public ClassMAPTable estIlExiste(ClassMAPTable o) throws Exception {
        try {
            ClassMAPTable[] liste = (ClassMAPTable[]) CGenUtil.rechercher(o, null, null, "");
            if (liste.length > 0) {
                return liste[0];
            } else {
                return null;
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public String createTypeObjet(String nomTable, String proc, String pref, String typ, String desc) throws Exception {
        try {
            if (u.getIdrole().compareTo("Chef") == 0 || u.getIdrole().compareTo("admin") == 0 || u.getIdrole().compareTo("dg") == 0 || u.getIdrole().compareTo("adminFacture") == 0) {
                TypeObjet to = new TypeObjet(nomTable, proc, pref, typ, desc);
                MapHistorique histo = new MapHistorique(nomTable, "insert", u.getTuppleID(), to.getId());
                histo.setObjet("bean.TypeObjet");
                to.insertToTable(histo);
                String s = to.getId();
                return s;
            } else {
                throw new Exception("Erreur de droit");
            }
        } catch (ErreurDAO ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public String updateMontantUnionIntra(String nomTable, String id1, String[] id2, String[] montant) throws Exception {
        Connection c = null;
        Statement st = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            st = c.createStatement();
            for (int i = 0; i < id2.length; i++) {
                bean.UnionIntraTableUtil uiti = new bean.UnionIntraTableUtil();
                uiti.setNomTable(nomTable);
                UnionIntraTable[] liefflc = (UnionIntraTable[]) uiti.rechercher(1, id2[i], c);
                if (liefflc.length == 0) {
                    throw new Exception("le lien n existe pas");
                }
                if (liefflc[0].estIlModifiable() == true) {
                    liefflc[0].setMontantMere(Double.parseDouble(montant[i]));
                    liefflc[0].setNomTable(nomTable);
                    String rek = "update " + nomTable + "  set montantMere = " + montant[i] + " where id1 = '" + liefflc[0].getId1() + "' and id2 = '" + liefflc[0].getId2() + "'";
                    st.executeUpdate(rek);
                }
            }
            c.commit();
            return "ok";
        } catch (SQLException ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw new Exception();
        } finally {
            if (c != null) {
                c.close();
            }
            if (st != null) {
                st.close();
            }
        }
    }

    @Override
    public String updateTypeObjet(String table, String id, String typ, String desc) throws Exception {
        try {
            if (u.getIdrole().compareTo("Chef") == 0 || u.getIdrole().compareTo("admin") == 0 || u.getIdrole().compareTo("dg") == 0 || u.getIdrole().compareTo("adminFacture") == 0) {
                TypeObjet to = new TypeObjet(table, id, typ, desc);
                MapHistorique histo = new MapHistorique(table, "update", u.getTuppleID(), to.getId());
                histo.setObjet("bean.TypeObjet");
                to.updateToTableWithHisto(histo);
                String s = to.getId();
                return s;
            } else {
                throw new Exception("Erreur de droit");
            }
        } catch (ErreurDAO ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public int deleteTypeObjet(String nomTable, String id) throws Exception {
        try {
            if (u.getIdrole().compareTo("Chef") == 0 || u.getIdrole().compareTo("admin") == 0 || u.getIdrole().compareTo("dg") == 0 || u.getIdrole().compareTo("adminFacture") == 0) {

                TypeObjet to = new TypeObjet(nomTable, id, "-", "-");
                MapHistorique h = new MapHistorique(nomTable, "delete", u.getTuppleID(), id);
                h.setObjet("bean.TypeObjet");
                to.deleteToTable(h);
                int i = 1;
                return i;
            } else {
                throw new Exception("Erreur de droit");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public MapUtilisateur[] findUtilisateurs(String refuser, String loginuser, String pwduser, String nomuser, String adruser, String teluser, String idrole) throws Exception {
        try {
            int[] a = {1, 2, 3, 4, 5, 6, 7}; //Donne le numero des champs sur lesquelles on va mettre des criteres
            String[] val = new String[a.length];
            val[0] = refuser;
            val[1] = loginuser;
            val[2] = pwduser;
            val[3] = nomuser;
            val[4] = adruser;
            val[5] = teluser;
            val[6] = idrole; //Affecte des valeurs aux criteres
            UtilisateurUtil cu = new UtilisateurUtil();
            return (MapUtilisateur[]) cu.rechercher(a, val);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public ResultatEtSomme findResultatFinalePage(String nomTable, String apresW, String colonne, String ordre) throws Exception {
        return null;
    }

    @Override
    public MapRoles[] findRole(String rol) {
        return (MapRoles[]) (new historique.RoleUtil().rechercher(1, rol));
    }

    @Override
    public MapUtilisateur getUser() {
        return u;
    }

    public int getMontantBilletage(String dix, String cinq, String deux, String un, String deuxCinq, String deuxCent, String cent, String cinquante, String vingt) {
        int retour1 = Utilitaire.stringToInt(dix) * 10000 + 5000 * Utilitaire.stringToInt(cinq) + 2000 * Utilitaire.stringToInt(deux) + 1000 * Utilitaire.stringToInt(un);
        int retour2 = Utilitaire.stringToInt(deuxCinq) * 500 + 200 * Utilitaire.stringToInt(deuxCent) + 100 * Utilitaire.stringToInt(cent) + 50 * Utilitaire.stringToInt(cinquante) + 20 * Utilitaire.stringToInt(vingt);
        return retour1 + retour2;
    }

    public int desactiveUtilisateur(String ref, String refUser) throws Exception {
        try {
            historique.AnnulationUtilisateur au = new historique.AnnulationUtilisateur(ref);
            historique.MapHistorique h = new historique.MapHistorique("Utilisateurs", "annule", refUser, ref);
            h.setObjet("historique.AnnulationUtilisateur");
            au.insertToTable(h);
            return 1;
        } catch (ErreurDAO ex) {
            throw new bean.ErreurDAO(ex.getMessage());
        }
    }

    @Override
    public int desactiveUtilisateur(String ref) throws Exception {

        try {
            if (u.getIdrole().compareTo("admin") == 0 || u.getIdrole().compareTo("dg") == 0 || u.getIdrole().compareTo("adminFacture") == 0) {

                int i = desactiveUtilisateur(ref, u.getTuppleID());
                return i;
            } else {
                throw new Exception("Erreur de droit");
            }
        } catch (CreateException ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public int activeUtilisateur(String ref, String refUser) throws bean.ErreurDAO {
        try {
            historique.AnnulationUtilisateur[] au = (historique.AnnulationUtilisateur[]) new historique.AnnulationUtilisateurUtil().rechercher(2, ref);
            historique.MapHistorique h = new historique.MapHistorique("Utilisateurs", "active", refUser, ref);
            h.setObjet("historique.AnnulationUtilisateur");
            for (int i = 0; i < au.length; i++) {
                au[i].deleteToTable(h);
            }
            return 1;
        } catch (ErreurDAO ex) {
            throw new bean.ErreurDAO(ex.getMessage());
        }
    }

    @Override
    public int activeUtilisateur(String ref) throws Exception {

        try {
            if (u.getIdrole().compareTo("admin") == 0 || u.getIdrole().compareTo("dg") == 0 || u.getIdrole().compareTo("adminFacture") == 0) {

                int i = activeUtilisateur(ref, u.getTuppleID());
                return i;
            } else {
                throw new Exception("Erreur de droit");
            }
        } catch (CreateException ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public MapUtilisateur testeValide(String user, String pass) throws Exception {
        try {
            historique.UtilisateurUtil uI = new UtilisateurUtil();
            return uI.testeValide(user, pass);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    public String findHomePageServices(String codeService) throws Exception {
        Connection connection = null;
        try {
            connection = (new UtilDB()).GetConn();
            HomePageURL[] hommePageList = (HomePageURL[]) CGenUtil.rechercher(new HomePageURL(), null, null, connection, "");
            if (hommePageList != null && hommePageList.length > 0) {
                this.home_page = hommePageList[0].getUrlpage();
            }
            return this.home_page;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public Configuration[] findConfiguration() throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            Configuration[] liste = (Configuration[]) CGenUtil.rechercher(new Configuration(), null, null, c, " order by typeconfig desc");
            return liste;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    @Override
    public Direction[] findDirection(String idDir, String libelledir, String descdir, String abbrevDir, String idDirecteur) throws Exception {

        try {
            String afterW = "";
            int[] numChamp = {1, 2, 3, 5};
            String[] val = {idDir, libelledir, descdir, abbrevDir};
            DirectionUtil du = new DirectionUtil();
            du.utiliserChampBase();
            //if(idDirecteur.compareToIgnoreCase("")==0 || idDirecteur.compareToIgnoreCase("%")==0) idDirecteur = "%";
            //afterW=" AND idDirecteur like  '" + idDirecteur +"'";
            return (Direction[]) du.rechercher(numChamp, val, "");

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }

    }

    @Override
    public TypeObjet[] findTypeObjet(String nomTable, String id, String typ) throws Exception {
        try {
            TypeObjetUtil to = new TypeObjetUtil();
            TypeObjet atypeobjet[] = to.findTypeObjet(nomTable, id, typ);
            return atypeobjet;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public void testLogin(String user, String pass) throws Exception {
        Connection c = null;
        try {
            u = testeValide(user, pass);
            UtilisateurUtil crt = new UtilisateurUtil();
            uVue = crt.testeValide("utilisateurVue", user, pass);
            type = u.getIdrole();
            if (type.compareToIgnoreCase(utilitaire.Constante.getIdRoleDirecteur()) == 0) {
                Direction d[] = findDirection("", "", "", "", String.valueOf(u.getRefuser()));
                if (d.length > 0) {
                    this.setIdDirection(d[0].getIdDir());
                } else {
                    type = u.getIdrole();
                }
            }

            //this.listeConfig = this.findConfiguration();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void ejbCreate() throws CreateException {
    }

    @Override
    public String getIdDirection() {
        return idDirection;
    }

    @Override
    public void setIdDirection(String idDirection) {
        this.idDirection = idDirection;
    }

    @Override
    public String mapperMereFille(ClassMAPTable e, String idMere, String[] idFille, String rem, String montant, String etat) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            mapperMereFille(e.getNomTable(), e.getNomProcedureSequence(), e.getINDICE_PK(), idMere, idFille, rem, montant, etat, c);
            c.commit();
        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    @Override
    public String mapperMereFille(ClassMAPTable e, String nomTable, String idMere, String[] idFille, String rem, String montant, String etat) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            String nomProcedure = "getSeq" + Utilitaire.convertDebutMajuscule(nomTable);
            String indicePK = nomTable.substring(0, 3).toUpperCase();
            mapperMereFille(nomTable, nomProcedure, indicePK, idMere, idFille, rem, montant, etat, c);
            c.commit();
        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    @Override
    public void deleteMereFille(ClassMAPTable e, String idMere, String[] liste_id_fille) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            deleteMereFille(e.getNomTable(), idMere, liste_id_fille, c);
            c.commit();
        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String mapperMereFille(String nomtableMappage, String nomProcedure, String suffixeMap, String idMere, String[] idFille, String rem, String montant, String etat, Connection c) throws Exception {
        try {
            for (int i = 0; i < idFille.length; i++) {
                UtilitaireMetier.mapperMereToFille(nomtableMappage, nomProcedure, suffixeMap, idMere, idFille[i], rem, montant, u.getTuppleID(), etat, c);
            }
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        }
        return null;
    }

    public void deleteMereFille(String nomtableMappage, String idMere, String[] idFille, Connection c) throws Exception {
        try {
            for (int i = 0; i < idFille.length; i++) {
                UtilitaireMetier.deleteMereToFille(nomtableMappage, idMere, idFille[i], u.getTuppleID(), c);
            }
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        }
    }

    @Override
    public void deleteMereFille(ClassMAPTable e, String nomTable, String idMere, String[] liste_id_fille) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            deleteMereFille(nomTable, idMere, liste_id_fille, c);

            c.commit();
        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public boolean testMemeUser(int userAutres) throws Exception {
        if (u.getRefuser() != (userAutres)) {
            throw new Exception("Utilisateur different de vous");
        }
        return true;
    }

    public boolean testMemeDirection(int userAutres) throws Exception {
        boolean retour = true;
        MapUtilisateur crt = new MapUtilisateur();
        MapUtilisateur[] t = (MapUtilisateur[]) (bean.CGenUtil.rechercher(crt, null, null, " and refuser=" + userAutres));
        if (t.length == 0) {
            throw new Exception("Utilisateur finale " + userAutres + " non existante");
        }
        //System.out.println("u adre user = "+t.length+" t[0] adr user = "+t[0].getAdruser()+" t[0] id user = "+userAutres);
        if (u.getAdruser().compareToIgnoreCase(t[0].getAdruser()) != 0) {
            throw new Exception("Utilisateur dans une autre direction");
        }
        return retour;
    }

    public String testRangUser(ClassMAPTable e, Connection c) throws Exception {
        String retour = "";
        /*UtilisateurRole util = new UtilisateurRole();
	UtilisateurRole[] utilEnCours = (UtilisateurRole[]) CGenUtil.rechercher(util, null, null, c, " and refuser=" + u.getTuppleID());
	if (utilEnCours.length != 0) {
	    UtilisateurRole[] liste_util = (UtilisateurRole[]) CGenUtil.rechercher(util, null, null, c, " and rang <= " + utilEnCours[0].getRang());
	    String[] users = new String[liste_util.length];

	    for (int i = 0; i < liste_util.length; i++) {
		users[i] = liste_util[i].getRefuser() + "";
	    }
	    if (e instanceof bean.ClassEtat || e instanceof bean.ClassUser) {
		if (e instanceof mg.cnaps.notification.NotificationLibelle) {
		    return NotificationService.conditionLectureNotification(utilEnCours[0]);
		} else if ((ListeColonneTable.getChamp(e, "direction", c) != null && ListeColonneTable.getChamp(e, "service", c) != null)) {
		    if (utilEnCours[0].getRang() < ConstanteUser.getRangChefDeService()) {
			return " and ((iduser = '" + utilEnCours[0].getRefuser() + "') OR (iduser like '%' AND service = '" + utilEnCours[0].getService() + "') OR (iduser like '%' AND direction like '%'))";
		    }
		    if (utilEnCours[0].getRang() >= ConstanteUser.getRangChefDeService() && utilEnCours[0].getRang() < ConstanteUser.getRangDG()) {
			return " and ((service = '" + utilEnCours[0].getService() + "') OR (direction like '%' AND service like '%' ) OR (service like '%' AND direction = '" + utilEnCours[0].getDirection() + "'))";
		    }
		    if (utilEnCours[0].getRang() >= ConstanteUser.getRangDG()) {
			return " and (direction like '%' OR direction is null)";
		    }
		} else {
		    retour = Utilitaire.tabToString(users, "'", ",");
		    retour = " and iduser in (" + retour + ",'0')";
		}
	    }
//            if ((e.getClass().getSuperclass().getSimpleName() != null) && (e.getClass().getSuperclass().getSimpleName().compareToIgnoreCase("ClassEtat") == 0 || e.getClass().getSuperclass().getSimpleName().compareToIgnoreCase("ClassUser") == 0)) {
//                retour = Utilitaire.tabToString(users, "'", ",");
//                retour = " and iduser in (" + retour + ",'0')";
//            }
	}*/
        return retour;
    }

    @Override
    public ResultatEtSomme getDataPageMax(ClassMAPTable e, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, Connection c) throws Exception {
        return getDataPageMax(e, colInt, valInt, numPage, apresWhere, nomColSomme, c, 0);
    }

    @Override
    public ResultatEtSomme getDataPageMax(ClassMAPTable e, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, Connection c, int npp) throws Exception {
        e.setMode("select");
        if (ListeColonneTable.getChamp(e, "iduser", c) != null) {
            ClassUser temp = (ClassUser) e;
            apresWhere = testRangUser(e, c) + apresWhere;
        }
        if (e.getNomTableSelect().compareToIgnoreCase("SIG_TRAVAILLEUR_INFO_COMPLET") == 0) {
            return CGenUtil.rechercherPageMaxSansRecap(e, colInt, valInt, numPage, apresWhere, nomColSomme, c, npp);
        }
        //System.out.println("apres where vaovao oooo "+apresWhere+ "user vaovao ooo"+u.getTuppleID());
        return CGenUtil.rechercherPageMax(e, colInt, valInt, numPage, apresWhere, nomColSomme, c, npp);
    }

    @Override
    public ResultatEtSomme getDataPage(ClassMAPTable e, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, Connection c) throws Exception {
        return getDataPage(e, colInt, valInt, numPage, apresWhere, nomColSomme, c, 0);
    }

    @Override
    public ResultatEtSomme getDataPage(ClassMAPTable e, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, Connection c, int npp) throws Exception {
        e.setMode("select");
        //if (ListeColonneTable.getChamp(e, "iduser", c) != null) {
        if (e instanceof bean.ClassUser) {
            ClassUser temp = (ClassUser) e;
            apresWhere = testRangUser(e, c) + apresWhere;
        }
        ResultatEtSomme rs = CGenUtil.rechercherPage(e, colInt, valInt, numPage, apresWhere, nomColSomme, c, npp);
        // if(u==null||u.isSuperUser()==false)rs.cacherMontant();
        if ((u != null && u.isSuperUser() == false && u.getNomuser().compareToIgnoreCase("front") != 0)) {
            rs.cacherMontant();
        }
        return rs;
    }

    @Override
    public ResultatEtSomme getDataPageGroupe(ClassMAPTable e, String[] groupe, String[] sommeGroupe, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, String ordre, Connection c) throws Exception {
        e.setMode("select");
        if (u.isSuperUser() == false) {
            return null;
        }
        if (ListeColonneTable.getChamp(e, "iduser", c) != null) {
            apresWhere = testRangUser(e, c) + apresWhere;
        }
        return CGenUtil.rechercherPageGroupe(e, groupe, sommeGroupe, colInt, valInt, numPage, apresWhere, nomColSomme, ordre, c);
    }

    @Override
    public ResultatEtSomme getDataPageGroupe(ClassMAPTable e, String[] groupe, String[] sommeGroupe, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, String ordre, Connection c, int npp) throws Exception {
        e.setMode("select");
        if (u.isSuperUser() == false) {
            return null;
        }
        if (ListeColonneTable.getChamp(e, "iduser", c) != null) {
            apresWhere = testRangUser(e, c) + apresWhere;
        }
        return CGenUtil.rechercherPageGroupe(e, groupe, sommeGroupe, colInt, valInt, numPage, apresWhere, nomColSomme, ordre, c, npp);
    }

    @Override
    public Object[] getData(ClassMAPTable e, String[] colInt, String[] valInt, Connection c, String apresWhere) throws Exception {
        e.setMode("select");
        if (ListeColonneTable.getChamp(e, "iduser", c) != null) {
            apresWhere = testRangUser(e, c) + apresWhere;
        }
        if (c == null) {
            return CGenUtil.rechercher(e, colInt, valInt, apresWhere);
        }
        return CGenUtil.rechercher(e, colInt, valInt, c, apresWhere);
    }

    public int getint() {
        return 0;
    }

    public String getMaxId(String table) throws Exception {
        Connection c = null;
        String retour = "---";
        try {
            c = new UtilDB().GetConn();
            java.sql.Statement sta = c.createStatement();
            java.sql.ResultSet res = sta.executeQuery("select max(id) from " + table);
            res.next();
            retour = res.getString(1);
        } catch (Exception e) {
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return retour;
    }

    public String getMaxId(String table, String[] colonne, String[] listeCritere) throws Exception {
        String retour = "----";
        int tailleCrt = listeCritere.length;
        Connection c = null;
        try {
            String temp = "select max(id) from " + table + " ";
            c = new UtilDB().GetConn();
            java.sql.Statement sta = c.createStatement();
            temp += "where " + colonne[0] + " ='" + listeCritere[0] + "'";
            //System.out.println("TY LE TEMP "+temp);
            if (tailleCrt > 1) {
                for (int i = 1; i < tailleCrt; i++) {
                    temp += " and " + colonne[i] + " = '" + listeCritere[i] + "'";
                }
            }
            java.sql.ResultSet res = sta.executeQuery(temp);
            res.next();
            retour = res.getString(1);
        } catch (Exception e) {
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return retour;
    }

    public static String getMaxColonne(String table, String colonne, String champCritere, String critere) throws Exception {
        String retour = "----";
        Connection c = null;
        try {
            String temp = "select max(" + colonne + ") from " + table + " where " + champCritere + "= " + critere;
            c = new UtilDB().GetConn();
            java.sql.Statement sta = c.createStatement();

            //System.out.println("TY LE TEMP "+temp);
            java.sql.ResultSet res = sta.executeQuery(temp);
            res.next();
            retour = res.getString(1);
        } catch (Exception e) {
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return retour;
    }

    public Object validerObject(ClassMAPTable o) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            Object ob = validerObject(o, c);
            c.commit();
            return ob;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    @Override
    public Object validerObject(ClassMAPTable o, Connection c) throws Exception {
        /*	if (u.getRang() < 4) 
	{
	    throw new Exception("	Erreur de droit");
	}*/
        try {
            return ValiderObject.validerObject(c, o, this, u, listeConfig);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        }
    }

    public Object cloturerObject(ClassMAPTable o) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            //System.out.println("oooo = " + o.toString());
            Object ob = cloturerObject(o, c);
            c.commit();
            return ob;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void testClotureEtat(ClassMAPTable o, Connection c) throws Exception {
        try {
            String id = o.getValInsert("id");
            ClassMAPTable cl = (ClassMAPTable) Class.forName(o.getClassName()).newInstance();
            ClassMAPTable[] liste = (ClassMAPTable[]) CGenUtil.rechercher(cl, null, null, c, " and id = '" + id + "'");
            if (liste.length == 0) {
                throw new Exception("Objet inexistante");
            }
            if (o.getClass().getSuperclass().getSimpleName().compareToIgnoreCase("ClassEtat") == 0) {
                if (Utilitaire.stringToInt(liste[0].getValInsert("etat")) == ConstanteEtat.getEtatAnnuler()) {
                    throw new Exception("Impossible de cloturer. Objet annul?");
                }

            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

//    public Object cloturerObject(ClassMAPTable o, Connection c) throws Exception {
//        if (testRestriction(u.getIdrole(), "ACT000008", o.getNomTable(), c) == 1) {
//            throw new Exception("Erreur de droit");
//        }
//
//        testClotureEtat(o, c);
//        try {
//            if (o.getClassName().compareToIgnoreCase("mg.allosakafo.commande.CommandeClient") == 0) {
//                CommandeClient cmd = (CommandeClient) o;
//                CommandeClient[] commande;
//                commande = (CommandeClient[]) CGenUtil.rechercher(cmd, null, null, c, " AND ID = '" + cmd.getId() + "'");
//
//                CommandeClientDetails[] listeDetails = (CommandeClientDetails[]) CGenUtil.rechercher(new CommandeClientDetails(), null, null, c, " AND idMere = '" + commande[0].getId() + "'");
//                for (int i = 0; i < listeDetails.length; i++) {
//                    listeDetails[i].setEtat(ConstanteEtat.getEtatCloture());
//                    listeDetails[i].updateToTableWithHisto(u.getTuppleID(), c);
//                }
//                commande[0].setEtat(ConstanteEtat.getEtatCloture());
//                commande[0].updateToTableWithHisto(u.getTuppleID(), c);
//            } else {
//                o.setMode("modif");
//
//                this.updateEtat(o, ConstanteEtat.getEtatCloture(), o.getValInsert("id"), c);
//            }
//            c.commit();
//            return o;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            if (c != null) {
//                c.rollback();
//            }
//            throw ex;
//        }
//    }

    public Object cloturerObject(ClassMAPTable o, Connection c) throws Exception {
//        if (testRestriction(u.getIdrole(), "ACT000008", o.getNomTable(), c) == 1) {
//            throw new Exception("Erreur de droit");
//        }

        testClotureEtat(o, c);
        try {
            Object obj = o;
            ClassEtat []lo=new ClassEtat[0];
            if(o instanceof ClassEtat){
                ClassEtat filtre=(ClassEtat)o.getClass().newInstance();
                filtre.setNomTable(o.getNomTable());
                
                lo=(ClassEtat [])CGenUtil.rechercher(filtre,null,null,c," and "+o.getAttributIDName()+"='"+o.getTuppleID()+"'");
                if(lo.length==0)throw new Exception("objet non existante");
                if (lo[0].getEtat()>=ConstanteEtat.getEtatCloture())throw new Exception("objet deja vise");
                o=lo[0];    
                obj = ((ClassEtat) o).cloturerObject(u.getTuppleID(), c);
            }
            c.commit();
            return obj;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        }
    }

    
    
    public Object rejeterObject(ClassMAPTable o) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            Object ob = rejeterObject(o, c);
            c.commit();
            return ob;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void testRejectEtat(ClassMAPTable o, Connection c) throws Exception {
        try {
            ClassMAPTable[] liste = (ClassMAPTable[]) CGenUtil.rechercher(o, null, null, c, "");
            if (liste.length == 0) {
                throw new Exception("Objet inexistante");
            }
            if (o.getClass().getSuperclass().getSimpleName().compareToIgnoreCase("ClassEtat") == 0) {
                if (Utilitaire.stringToInt(liste[0].getValInsert("etat")) == ConstanteEtat.getEtatAnnuler()) {
                    throw new Exception(" Impossible de rejeter. Objet annul?");
                }

                if (Utilitaire.stringToInt(liste[0].getValInsert("etat")) >= ConstanteEtat.getEtatValider()) {
                    throw new Exception(" Impossible de rejeter. Objet d?j? vis?");
                }
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public Object rejeterObject(ClassMAPTable o, Connection c) throws Exception {
        if (testRestriction(u.getIdrole(), "ACT000007", o.getNomTable(), c) == 1) {
            throw new Exception("Erreur de droit");
        }
        testRejectEtat(o, c);
        try {
            o.setMode("modif");

            c.commit();
            return o;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        }
    }

    public Object annulerObject(ClassMAPTable o) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            Object ob = annulerObject(o, c);
            c.commit();
            return ob;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public Object annulerObject(ClassMAPTable o, Connection c) throws Exception {
//        if (testRestriction(u.getIdrole(), "ACT000005", o.getNomTable(), c) == 1) {
//            throw new Exception("Erreur de droit");
//        }
        try {
            if (o instanceof ClassEtat) {
                ClassEtat filtre = (ClassEtat) o.getClass().newInstance();
                filtre.setNomTable(o.getNomTable());

                ClassEtat[] lo = (ClassEtat[]) CGenUtil.rechercher(filtre, null, null, c, " and " + o.getAttributIDName() + "='" + o.getTuppleID() + "'");

                if (lo.length == 0) {
                    throw new Exception("objet non existante");
                }
                
                //System.out.println("Je suis dans la fonction annulerObject");
                lo[0].annuler(c);
                return lo[0];
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        }
    }

    public void setEtat(ClassEtat o, Integer paramT) throws Exception {
        String nomChamp = "etat";
        o.setValChamp(nomChamp, paramT);
    }

    public Object createObject(ClassMAPTable o) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            Object ob = createObject(o, c);
            c.commit();
            return ob;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }

    }

    public Object[] createObjectMultiple(ClassMAPTable[] o, Connection c) throws Exception {
        try {
            Object[] ret = new Object[o.length];
            for (int i = 0; i < o.length; i++) {
                ret[i] = createObject(o[i], c);
            }

            return ret;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        }

    }

    public Object[] createObjectMultiple(ClassMAPTable[] o) throws Exception {
        Connection c = null;
        Object[] ret = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            ret = createObjectMultiple(o, c);
            c.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    public void updateEtat(ClassMAPTable e, int valeurEtat, String id) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            updateEtat(e, valeurEtat, id, c);
            c.commit();
        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void updateEtat(ClassMAPTable e, int valeurEtat, String id, Connection c) throws Exception {
        Statement cmd = null;
        try {
            String req = "update " + e.getNomTable() + " set etat=" + valeurEtat + " where " + e.getAttributIDName() + " = '" + id + "'";
            cmd = c.createStatement();
            cmd.executeUpdate(req);

        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            cmd.close();
        }
    }

    public int validerObjectMultiple(ClassEtat[] o, Connection c) throws Exception {
        try {
            for (int i = 0; i < o.length; i++) {
                ClassEtat map = ((ClassEtat[]) CGenUtil.rechercher(o[i], null, null, c, ""))[0];
                map.setMode("modif");
                setEtat(map, ConstanteEtat.getEtatValider());
                map.updateToTableWithHisto(u.getTuppleID(), c);
            }
            return 1;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
        }
        return 0;
    }

    public int viserObjectMultiple(ClassEtat o, String[] listeIdObjet, Connection c) throws Exception {
        try {

            String script = Utilitaire.tabToString(listeIdObjet, "'", ",");
            String apw = " and " + o.getAttributIDName() + " in (" + script + " )";
            ClassEtat[] mapTableListe = (ClassEtat[]) CGenUtil.rechercher(o, null, null, c, apw);
            for (int i = 0; i < mapTableListe.length; i++) {
                mapTableListe[i].setMode("modif");
                mapTableListe[i].setNomTable(o.getNomTable());
                this.validerObject(mapTableListe[i], c);
            }
            return 1;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }

        }
        return 0;
    }

    public void notifierObjectMultiple(String[] ids, String classe) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            String idString = Utilitaire.tabToString(ids, "'", ",");
            String where = " AND ID IN (" + idString + ")";

            c.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public int viserObjectMultiple(ClassEtat o, String[] listeIdObjet) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            //System.out.println("Tafiditra fct 1");
            int r = viserObjectMultiple(o, listeIdObjet, c);
            c.commit();
            return r;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    @Override
    public int retournerObjectMultiple(ClassEtat o, String[] listeIdObjet, String motif) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            int r = retournerObjectMultiple(o, listeIdObjet, motif, c);
            c.commit();
            return r;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    @Override
    public int recuObjectMultiple(ClassEtat o, String[] listeIdObjet) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            int r = recuObjectMultiple(o, listeIdObjet, c);
            c.commit();
            return r;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public int recuObjectMultiple(ClassEtat o, String[] listeIdObjet, Connection c) throws Exception {
        try {

            String script = Utilitaire.tabToString(listeIdObjet, "'", ",");
            String apw = " and " + o.getAttributIDName() + " in (" + script + " )";

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public int retournerObjectMultiple(ClassEtat o, String[] listeIdObjet, String motif, Connection c) throws Exception {
        try {

            String script = Utilitaire.tabToString(listeIdObjet, "'", ",");
            String apw = " and " + o.getAttributIDName() + " in (" + script + " )";

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public int rejeterObjectMultiple(ClassEtat o, String[] listeIdObjet, Connection c, String nomTableRejet, String nomTableCategorieRejet) throws Exception {
        try {
            String script = Utilitaire.tabToString(listeIdObjet, "'", ",");
            ClassEtat[] mapTableListe = (ClassEtat[]) CGenUtil.rechercher(o, null, null, c, " and upper(" + o.getAttributIDName() + ") in (" + script + " )");
            if (mapTableListe != null && testRestriction(u.getIdrole(), "ACT000006", mapTableListe[0].getNomTable(), c) == 1) {
                throw new Exception("Erreur de droit");
            }
            TypeObjet to = new TypeObjet();
            to.setNomTable(nomTableCategorieRejet);
            TypeObjet toVal = (TypeObjet) CGenUtil.rechercher(to, null, null, c, "")[0];
            for (int i = 0; i < mapTableListe.length; i++) {
                mapTableListe[i].setMode("modif");
                //setEtat(mapTableListe[i], ConstanteEtat.getEtatObjetRejeter());
                mapTableListe[i].updateToTableWithHisto(u.getTuppleID(), c);
                RejetTable rejet = new RejetTable(Utilitaire.dateDuJourSql(), "rejet multiple", listeIdObjet[i], "rejet multiple", toVal.getId());
                rejet.setNomTable(nomTableRejet);
                this.createObject(rejet, c);
            }
            return 1;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
        }
        return 0;
    }

    public int rejeterObjectMultiple(ClassEtat o, String[] listeIdObjet, String nomTableRejet, String nomTableCategorieRejet) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            int r = rejeterObjectMultiple(o, listeIdObjet, c, nomTableRejet, nomTableCategorieRejet);
            c.commit();
            return r;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public int validerObjectMultiple(ClassEtat[] o) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            int r = validerObjectMultiple(o, c);
            c.commit();
            return r;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String genererEnfantMatricule(String motherName, String sexe, Date dateNaissance, String nationalite, String numActe) throws Exception {
        try {
            //System.out.println("----------------------motherName = " + motherName);
            String anneeDeNaissance = Utilitaire.getAnnee(dateNaissance) + "";
            int moisDeNaissance = Utilitaire.getMois(dateNaissance);
            String jour = Utilitaire.getJour(Utilitaire.datetostring(dateNaissance));

            String premierChiffreAnnee = anneeDeNaissance.substring(0, 1);
            String rangMoisLettre = "";
            String millenismeAnnee = anneeDeNaissance.substring(2, 4);
            int rangMoisNaissance;
            int rangJourNaissance;

            if (sexe.compareToIgnoreCase("0") == 0) { //1989
                rangMoisLettre = Utilitaire.getRangMoisLettre(moisDeNaissance + 12);
            } else {
                rangMoisLettre = Utilitaire.getRangMoisLettre(moisDeNaissance);
            }

            if (nationalite.compareToIgnoreCase("1") == 0) { //1989
                rangJourNaissance = Integer.parseInt(jour) + 40;
            } else {
                rangJourNaissance = Integer.parseInt(jour);
            }

            // //System.out.println("premierChiffreAnnee == " + premierChiffreAnnee + "millenismeAnnee = " + millenismeAnnee + " rangMoisLettre = " + rangMoisLettre + " rangJourNaissance = " + rangJourNaissance + " numActe = " + numActe);
            // //System.out.println(premierChiffreAnnee + "" + millenismeAnnee + "" + rangMoisLettre + "" + rangJourNaissance + "" + numActe);
            String temp = premierChiffreAnnee + "" + millenismeAnnee + "" + rangMoisLettre + "" + String.format("%02d", rangJourNaissance) + "" + numActe;
            // cryptage du mere

            // dechifrer
            double nom_dechiffrer = Utilitaire.dechiffrer(motherName);

            //System.out.println(" dechiffrement ========= " + nom_dechiffrer);
            String val_hex_nom = Utilitaire.completerInt(6, (int) nom_dechiffrer);

            //System.out.println(" valeur hexa ========= " + val_hex_nom);

            temp += val_hex_nom;
            return temp;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public String genererTravailleurMatricule(String idTravailleur, String sexe, Date dateNaissance) throws Exception {
        try {
            String anneeDeNaissance = Utilitaire.getAnnee(dateNaissance) + "";
            int moisDeNaissance = Utilitaire.getMois(dateNaissance);
            String jour = Utilitaire.getJour(Utilitaire.datetostring(dateNaissance));

            String millenismeAnnee = anneeDeNaissance.substring(2, 4);
            int rangMoisNaissance;
            int rangJourNaissance = Integer.parseInt(jour);
            String numeroSequence = idTravailleur.substring(6, 10);

            if (sexe.compareToIgnoreCase("0") == 0) { //1989
                rangMoisNaissance = moisDeNaissance + 20;
            } else {
                rangMoisNaissance = moisDeNaissance;
            }

            rangMoisNaissance = Integer.parseInt(String.format("%02d", rangMoisNaissance));

            String numTemp = millenismeAnnee + "" + rangMoisNaissance + "" + String.format("%02d", rangJourNaissance) + "" + numeroSequence;

            long randumNumber = Long.parseLong(numTemp) % 97;

            //System.out.println("millenismeAnnee = " + millenismeAnnee + " rangMoisNaissance = " + rangMoisNaissance + " rangJourNaissance = " + rangJourNaissance + " numeroSequence = " + numeroSequence + " randumNumber = " + randumNumber);
            numTemp += randumNumber + "";

            return numTemp;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public Object finaliser(ClassMAPTable o, Connection c) throws Exception {
        try {
            o.setMode("modif");

            //this.updateEtat(o, ConstanteEtat.getConstanteEtatFinaliser(), o.getTuppleID(), c);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        return null;
    }

    public Object finaliser(ClassMAPTable map) throws Exception {
        Connection c = null;
        try {
            c = (new UtilDB()).GetConn();
            Object ret = finaliser(map, c);
            //c.commit();
            return ret;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public Object createObject(ClassMAPTable o, Connection c) throws Exception {
        /*if (testRestriction(u.getIdrole(), "ACT000001", o.getNomTable(), c) == 1) {
	    throw new Exception("Erreur de droit");
	}*/
        try {
            return CreateObject.createObject(o, c, u, this, listeConfig);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        }
    }

    public Object updateEtatCommande(ClassMAPTable o, String etat) throws Exception {
        Connection conn = null;
        try {
            conn = (new UtilDB()).GetConn();
            if (o.getClassName().compareToIgnoreCase("mg.allosakafo.commande.CommandeClient") == 0) {
                CommandeClient cmd = (CommandeClient) o;
                CommandeClient[] liste = (CommandeClient[]) CGenUtil.rechercher(cmd, null, null, conn, "");
                cmd = liste[0];
                if (etat.equals("payer")) {
                    cmd.setEtat(ConstanteEtat.getEtatPaye());
                }
                if (etat.equals("livrer")) {
                    cmd.setEtat(ConstanteEtat.getEtatLivraison());
                }
                cmd.updateToTable(conn);
                return cmd;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (conn != null) {
                conn.rollback();
            }
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /*public Object payerObject(ClassMAPTable o)throws Exception {
        Connection conn = null;
        try{
            conn = (new UtilDB()).GetConn();
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.commande.CommandeClient") == 0){
                CommandeClient cmd = (CommandeClient)o;
                CommandeClient[] liste = (CommandeClient[]) CGenUtil.rechercher(cmd, null, null, conn, "");
                cmd = liste[0];
                cmd.setEtat(ConstanteEtat.getEtatPaye());
                cmd.updateToTable(conn);
                return cmd;
            }
            return null;
        } catch (Exception ex) {
	    ex.printStackTrace();
	    throw ex;
	}finally{
            if(conn != null){
                conn.close();
            }
        }
    }
    public Object livrerObject(ClassMAPTable o)throws Exception {
        Connection conn = null;
        try{
            conn = (new UtilDB()).GetConn();
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.commande.CommandeClient") == 0){
                CommandeClient cmd = (CommandeClient)o;
                CommandeClient[] liste = (CommandeClient[]) CGenUtil.rechercher(cmd, null, null, conn, "");
                cmd = liste[0];
                cmd.setEtat(ConstanteEtat.getEtatLivraison());
                cmd.updateToTable(conn);
                return cmd;
            }
            return null;
        } catch (Exception ex) {
	    ex.printStackTrace();
	    throw ex;
	}finally{
            if(conn != null){
                conn.close();
            }
        }
    }*/
    public Object updateEtatFait(ClassMAPTable o) throws Exception {
        Connection conn = null;
        try {
            conn = (new UtilDB()).GetConn();

//            //System.out.println("Upadte ************ 0 ---- "+o.getClassName());            
            if (o.getClassName().compareToIgnoreCase("mg.allosakafo.commande.CommandeClient") == 0) {
                CommandeClient cmd = (CommandeClient) o;
                CommandeClient[] commande;
                if (cmd.getNomTable().compareToIgnoreCase("as_commandepoint") != 0) {
                    commande = (CommandeClient[]) CGenUtil.rechercher(new CommandeClient(), null, null, conn, " AND ID = '" + cmd.getId() + "'");
                } else {
                    CommandeClient c = new CommandeClient();
                    c.setNomTable("as_commandepoint");
                    commande = (CommandeClient[]) CGenUtil.rechercher(c, null, null, conn, " AND ID = '" + cmd.getId() + "'");
                }
                if (commande != null && commande.length != 0) {
                    CommandeClientDetails[] listeDetails = (CommandeClientDetails[]) CGenUtil.rechercher(new CommandeClientDetails(), null, null, conn, " AND idMere = '" + commande[0].getId() + "'");
                    for (int i = 0; i < listeDetails.length; i++) {
                        listeDetails[i].setEtat(ConstanteEtat.getEtatFait());
                        listeDetails[i].updateToTableWithHisto(u.getTuppleID(), conn);
                    }
                    commande[0].setEtat(ConstanteEtat.getEtatFait());
                    commande[0].updateToTableWithHisto(u.getTuppleID(), conn);
                }
                return cmd;
            }
            if (o.getClassName().compareToIgnoreCase("mg.allosakafo.commande.CommandeMobile") == 0) {
                CommandeMobile cmd = (CommandeMobile) o;
                CommandeMobile[] commande;
//                //System.out.println("Upadte ************ 1 ---- " + cmd.getId() );
                if (cmd.getNomTable().compareToIgnoreCase("as_commandemobile") != 0) {
                    commande = (CommandeMobile[]) CGenUtil.rechercher(new CommandeMobile(), null, null, conn, " AND ID = '" + cmd.getId() + "'");
                } else {
                    CommandeMobile c = new CommandeMobile();
                    c.setNomTable("as_commandemobile");
                    commande = (CommandeMobile[]) CGenUtil.rechercher(c, null, null, conn, " AND ID = '" + cmd.getId() + "'");
                }
                if (commande != null && commande.length != 0) {

                    commande[0].setEtat(ConstanteEtat.getEtatFait());
                    commande[0].updateToTableWithHisto(u.getTuppleID(), conn);
                }
                return cmd;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            if (conn != null) {
                conn.rollback();
            }
            throw new Exception();
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public Object updateObject(ClassMAPTable o) throws Exception {
        Connection c = null;

        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            updateObject(o, c);
            c.commit();
            return o;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void testUpdate(ClassMAPTable o, Connection c) throws Exception {
        try {
            if (o.getClass().getSuperclass().getSimpleName().compareToIgnoreCase("ClassEtat") == 0) {
                String id = o.getValInsert("id");
                //System.out.println("affichage id : " + id);
                ClassMAPTable cl = (ClassMAPTable) Class.forName(o.getClassName()).newInstance();
                if (o.getNomTable().compareTo("as_commandepoint") == 0) {
                    cl.setNomTable("as_commandepoint");
                }
                ClassMAPTable[] liste = (ClassMAPTable[]) CGenUtil.rechercher(cl, null, null, c, " and id = '" + id + "'");
                if (Utilitaire.stringToInt(liste[0].getValInsert("etat")) == ConstanteEtat.getEtatAnnuler()) {
                    throw new Exception(" Impossible de modifier. Objet annul?");
                }
                if (Utilitaire.stringToInt(liste[0].getValInsert("etat")) >= ConstanteEtat.getEtatValider()) {
                    //throw new Exception(" Impossible de modifier. Objet d?j? vis?");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public Object updateObject(ClassMAPTable o, Connection c) throws Exception {

        testUpdate(o, c);
        try {
            return UpdateObject.updateObject(o, c, u, this);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        }

    }

    public void testDelete(ClassMAPTable o, Connection c) throws Exception {
        try {
            //System.out.println("miditra delete");
            if (o.getClass().getSuperclass().getSimpleName().compareToIgnoreCase("ClassEtat") == 0) {
                String id = o.getValInsert("id");
                ClassMAPTable[] liste = (ClassMAPTable[]) CGenUtil.rechercher(o, null, null, c, "");
                // //System.out.println("ffffffffffffffffffffff=="+liste[0].getValInsert("etat"));
                if (liste.length == 0) {
                    throw new Exception("Objet inexistant");
                }
                if (Utilitaire.stringToInt(liste[0].getValInsert("etat")) >= ConstanteEtat.getEtatValider()) {
                    throw new Exception("Impossible de supprimer. Objet d?j? vis?");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void deleteObjetFille(ClassMAPTable o) throws Exception {
        Connection conn = null;
        try {
            conn = new UtilDB().GetConn();
            conn.setAutoCommit(false);
            deleteObjetFille(o, conn);
            conn.commit();
        } catch (Exception ex) {
            if (conn != null) {
                conn.rollback();
            }
            ex.printStackTrace();
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void deleteObjetFille(ClassMAPTable o, Connection conn) throws Exception {
        try {

            o.deleteToTableWithHisto(u.getTuppleID(), conn);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (conn != null) {
                conn.rollback();
            }
        }
    }

    public void deleteObject(ClassMAPTable o) throws Exception {
        this.deleteObject(o, null);
    }

    public void deleteObject(ClassMAPTable o, Connection c) throws Exception {
        int verif = 0;
        try {
            /*
             * if (u.isSuperUser() == false) { throw new Exception("Pas de
             * droit"); }
             */
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
//            if (testRestriction(u.getIdrole(), "ACT000003", o.getNomTable(), c) == 1) {
//                throw new Exception("Erreur de droit");
//            }
            testDelete(o, c);
            ClassMAPTable[] liste = (ClassMAPTable[]) CGenUtil.rechercher(o, null, null, c, "");
            if (liste.length > 0) {
                //System.out.println("iddd !" + liste[0].getTuppleID());
                liste[0].deleteToTableWithHisto(u.getTuppleID(), c);
            } else {
                throw new Exception("Erreur durant la suppression, Objet introuvable");
            }
            if (verif == 1) {
                c.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw e;
        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            ex.printStackTrace();
            throw ex;
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
    }

    public void updateOneColonne(String nomTable, String colonne, String colonneCritere, String valeurCritere, String valeur, Connection c) throws Exception {
        PreparedStatement pst = null;
        try {
            String sql = "UPDATE " + nomTable + " SET " + colonne + "=? WHERE " + colonneCritere + "=?";
            pst = c.prepareStatement(sql);
            pst.setString(1, valeur);
            pst.setString(2, valeurCritere);
            pst.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (pst != null) {
                pst.close();
            }
        }
    }

    public boolean isSuperUser() {
        return u.isSuperUser();
    }

    @Override
    public int testRestriction(String user, String permission, String table, Connection con) throws Exception {
        try {
            GestionRole g = new GestionRole();
            return g.testRestriction(user, permission, table, u.getAdruser(), con);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    @Override
    public String[] getAllTable() throws Exception {
        Connection con = null;
        try {
            con = new UtilDB().GetConn();
            GestionRole g = new GestionRole();
            return g.getAllTAble(con);
        } catch (Exception e) {
            throw e;
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    @Override
    public void ajoutrestriction(String[] val, String idrole, String act, String direc) throws Exception {
        Connection con = null;
        try {
            con = new UtilDB().GetConn();
            ajoutrestriction(val, idrole, act, direc, con);
        } catch (Exception e) {
            if (con != null) {
                con.rollback();
            }
            throw e;
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    @Override
    public void ajoutrestriction(String[] val, String idrole, String act, String direc, Connection c) throws Exception {
        try {
            GestionRole g = new GestionRole();
            g.ajoutrestriction(val, idrole, act, direc, c);
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw e;
        }
    }

    @Override
    public config.Table[] getListeTable(String role, String adr) throws Exception {
        try {
            GestionRole g = new GestionRole();
            return g.getListeTable(role, adr);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public int deletePJ(String idDossier, String idPj) throws Exception {
        return 0;
    }

    @Override
    public int deletePJ(String idDossier, String idPj, Connection con) throws Exception {
        return 0;
    }

    @Override
    public int ajouterPJInfo(String info_valeur, String pieces_id, String info_id, String info_ok) throws Exception {
        return 0;
    }

    public int ajouterPJInfo(String info_valeur, String pieces_id, String info_id, String info_ok, Connection con) throws Exception {
        return 0;
    }

    @Override
    public int ajouterPJTiers(String idPiece, List<String[]> listeTiers) throws Exception {
        Connection con = null;
        try {
            con = new UtilDB().GetConn();
            con.setAutoCommit(false);
            int ret = ajouterPJTiers(idPiece, listeTiers, con);
            con.commit();
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            if (con != null) {
                con.rollback();
            }
            throw e;
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    @Override
    public int ajouterPJTiers(String idPiece, List<String[]> listeTiers, Connection con) throws Exception {

        return 1;
    }

    @Override
    public int detacherTiers(String idtiers) throws Exception {

        return -1;
    }

    @Override
    public void createUploadedPj(String nomtable, String nomprocedure, String libelle, String chemin, String mere) throws Exception {
        Connection con = null;
        try {
            con = new UtilDB().GetConn();
            UploadPj fichier = new UploadPj(nomtable, nomprocedure, "FLE", libelle, chemin, mere);
            fichier.construirePK(con);
            fichier.insertToTableWithHisto(u.getTuppleID(), con);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (con != null) {
                con.rollback();
            }
            throw ex;
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    @Override
    public void deleteUploadedPj(String nomtable, String id) throws Exception {
        Connection con = null;
        try {
            con = new UtilDB().GetConn();
            con.setAutoCommit(false);
            UploadPj up = new UploadPj(nomtable);
            UploadPj[] fichiers = (UploadPj[]) CGenUtil.rechercher(up, null, null, con, " AND ID = '" + id + "'");
            //  Utilitaire.deleteFileFromCdn(fichiers[0].getChemin());
            MapHistorique h = new MapHistorique(nomtable, "delete", u.getTuppleID(), id);
            h.setObjet("bean.UploadPj");
            fichiers[0].deleteToTable(con);
            con.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (con != null) {
                con.rollback();
            }
            throw ex;
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public String sendSms(String[] idDest, String idmodele, String message, String service, String numov) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            String retour = "";//SmsService.sendSms(c, idDest, idmodele, message, service, u, numov);
            return retour;
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public Object dupliquerObject(ClassMAPTable o, String mapFille, String nomColonneMere, Connection c) throws Exception {
        if (testRestriction(u.getIdrole(), "ACT000010", o.getNomTable(), c) == 1) {
            throw new Exception("Erreur de droit");
        }
        try {
            o.setMode("modif");
            if (o.getClass().getName().compareTo("mg.allosakafo.produits.Produits") == 0) {
                Produits p = (Produits) o;
                p.dupliquer(this, c);
                return p.getId();
            }
            String id = o.getValInsert("id");

        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        }
        return null;
    }

    public Object dupliquerObject(ClassMAPTable o, String mapFille, String nomColonneMere) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            Object ob = dupliquerObject(o, mapFille, nomColonneMere, c);
            c.commit();
            return ob;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    @Override
    public void annulerVisa(ClassMAPTable o) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            this.annulerVisa(o, c);
            c.commit();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public ResultatEtSomme getDataPageGroupeMultiple(ClassMAPTable e, String[] groupe, String[] sommeGroupe, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, String ordre, Connection c) throws Exception {
        return CGenUtil.rechercherPageGroupeM(e, groupe, sommeGroupe, colInt, valInt, numPage, apresWhere, nomColSomme, ordre, c);
    }

    public ResultatEtSomme getDataPageGroupeMultiple(ClassMAPTable e, String[] groupe, String[] sommeGroupe, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, String ordre, Connection c, int npp) throws Exception {
        return CGenUtil.rechercherPageGroupeM(e, groupe, sommeGroupe, colInt, valInt, numPage, apresWhere, nomColSomme, ordre, c, npp);
    }

    public ResultatEtSomme getDataPageGroupeMultiple(ClassMAPTable e, String[] groupe, String[] sommeGroupe, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, String ordre, Connection c, int npp, String count) throws Exception {
        return CGenUtil.rechercherPageGroupeM(e, groupe, sommeGroupe, colInt, valInt, numPage, apresWhere, nomColSomme, ordre, c, npp, count);
    }

    public void testAnnulerVisaEtat(ClassMAPTable o, Connection c) throws Exception {
        try {
            String id = o.getValInsert("id");
            ClassMAPTable cl = (ClassMAPTable) Class.forName(o.getClassName()).newInstance();
            ClassMAPTable[] liste = (ClassMAPTable[]) CGenUtil.rechercher(cl, null, null, c, " and id = '" + id + "'");
            if (liste.length == 0) {
                throw new Exception("Objet inexistante");
            }
            if (o.getClass().getSuperclass().getSimpleName().compareToIgnoreCase("ClassEtat") == 0) {
                if (Utilitaire.stringToInt(liste[0].getValInsert("etat")) < ConstanteEtat.getEtatValider()) {
                    throw new Exception("Impossible Annuler VISA. Objet non vis?");
                }
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void annulerVisa(ClassMAPTable o, Connection c) throws Exception {
        try {
            /*  if (u.getRang() < ConstanteUser.rangDir) {
		throw new Exception("Erreur de droit");
	    }*/
            boolean defaultVisa = true;
            testAnnulerVisaEtat(o, c);
            String id = o.getValInsert("id");
            o.setValChamp("id", id);
            ClassMAPTable[] liste = (ClassMAPTable[]) CGenUtil.rechercher(o, null, null, c, "");

            if (defaultVisa) {
                //this.updateEtat(o, ConstanteEtat.getEtatCreer(), id, c);
                ((ClassEtat)o).annulerVisa(u.getTuppleID(), c);
            }

        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        }
    }

    public String mapperMereToFilleMetier(String nomtableMappage, String nomProcedure, String suffixeMap, String idMere, String[] idFille, String rem, String montant, String etat, Connection c) throws Exception {
        try {
            for (int i = 0; i < idFille.length; i++) {
                UtilitaireMetier.mapperMereToFilleMetier(nomtableMappage, nomProcedure, suffixeMap, idMere, idFille[i], rem, montant, u.getTuppleID(), etat, c);
            }
        } catch (Exception e) {
            throw e;
        }
        return null;
    }

    @Override
    public String mapperMereToFilleMetier(ClassMAPTable e, String nomTable, String idMere, String[] idFille, String rem, String montant, String etat) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            String nomProcedure = "getSeq" + Utilitaire.convertDebutMajuscule(nomTable);
            String indicePK = nomTable.substring(0, 3).toUpperCase();
            if (nomTable.compareTo("log_vehicule_entretien") == 0) {
                nomProcedure = "GETSEQLogVehiculeEntretien";
                indicePK = "LVE";
            }
            mapperMereToFilleMetier(nomTable, nomProcedure, indicePK, idMere, idFille, rem, montant, etat, c);
            c.commit();
        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    @Override
    public String mapperMereToFilleMetier(ClassMAPTable e, String idMere, String[] idFille, String rem, String montant, String etat) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            mapperMereToFilleMetier(e.getNomTable(), e.getNomProcedureSequence(), e.getINDICE_PK(), idMere, idFille, rem, montant, etat, c);
            c.commit();
        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    public Object createCommande(ClassMAPTable f, String[] produit, String[] qtte, String[] pu, String[] rmq) throws Exception {
        Connection conn = null;
        try {
            conn = (new UtilDB()).GetConn();
            conn.setAutoCommit(false);

            //search client
            CommandeClient cct = (CommandeClient) f;
            String testClient = searchClient(cct.getClient(), conn);
            if (testClient.compareTo("") == 0) {
                ClientAlloSakafo client = new ClientAlloSakafo(cct.getRemarque(), cct.getClient(), cct.getAdresseliv());
                createObject(client, conn);
            }

            ClassMAPTable o = (ClassMAPTable) createObject(f, conn);

            for (int i = 0; i < produit.length; i++) {
                CommandeClientDetails p = new CommandeClientDetails();
                //System.out.println(" =====" + produit[i] + " =====");
                p.construirePK(conn);
                p.setIdmere(o.getTuppleID());
                p.setProduit(produit[i]);
                p.setQuantite(Utilitaire.stringToDouble(qtte[i]));
                //Bonus
                if (cct.getTypecommande().compareTo("TPC00001") == 0) {
                    p.setPu(0);
                } else {
                    p.setPu(Utilitaire.stringToDouble(pu[i]));
                }
                p.setObservation(rmq[i]);
                p.insertToTableWithHisto(u.getTuppleID(), conn);
            }

            conn.commit();
            return o;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (conn != null) {
                conn.rollback();
            }
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public Object createCommandeMobile(ClassMAPTable f, String[] produit, String[] qtte, String[] pu, String[] rmq) throws Exception {
        Connection conn = null;
        try {
            conn = (new UtilDB()).GetConn();
            conn.setAutoCommit(false);

            CommandeMobile cct = (CommandeMobile) f;
            ClassMAPTable o = (ClassMAPTable) createObject(cct, conn);

            for (int i = 0; i < produit.length; i++) {
                CommandeMobileDetails p = new CommandeMobileDetails();
                p.construirePK(conn);
                p.setIdmere(o.getTuppleID());
                p.setProduit(produit[i]);
                p.setQuantite(Utilitaire.stringToDouble(qtte[i]));
                p.setObservation(rmq[i]);
                p.setPu(Utilitaire.stringToDouble(pu[i]));
                p.insertToTableWithHisto(u.getTuppleID(), conn);
            }

            conn.commit();
            return o;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (conn != null) {
                conn.rollback();
            }
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public Object updateCommande(ClassMAPTable f, String[] produit, String[] qtte, String[] pu, String[] rmq) throws Exception {
        Connection conn = null;
        try {
            conn = (new UtilDB()).GetConn();
            conn.setAutoCommit(false);
            CommandeClient cmd = (CommandeClient) f;
            CommandeClient[] lsc = (CommandeClient[]) CGenUtil.rechercher(cmd, null, null, conn, "");

            ClassMAPTable o = (ClassMAPTable) updateObject(cmd, conn);

            String req = Utilitaire.tabToString(produit, "'", ",");

            for (int i = 0; i < produit.length; i++) {
                CommandeClientDetails p = new CommandeClientDetails();
                p.setId(produit[i]);
                CommandeClientDetails[] lsd = (CommandeClientDetails[]) CGenUtil.rechercher(p, null, null, conn, " AND IDMERE = '" + f.getTuppleID() + "'");
                //System.out.println(produit[i] + " - " + qtte[i] + " - " + pu[i]);
                lsd[0].setQuantite(Utilitaire.stringToDouble(qtte[i]));
                //Bonus
                if (cmd.getTypecommande().compareTo("TPC00001") == 0) {
                    lsd[0].setPu(0);
                } else {
                    lsd[0].setPu(Utilitaire.stringToDouble(pu[i]));
                }
                lsd[0].setObservation(rmq[i]);
                lsd[0].updateToTableWithHisto(u.getTuppleID(), conn);
            }

            conn.commit();
            return o;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (conn != null) {
                conn.rollback();
            }
            throw new Exception(ex.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public String createOpDirect(String daty, String montantTTC, String idTVA, String idFournisseur, String idDevise, String remarque, String designation, String datyecheance) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            String id = createOpDirect(daty, montantTTC, idTVA, idFournisseur, idDevise, remarque, designation, datyecheance, c);

            c.commit();
            return id;
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw new Exception("Erreur : " + e.getMessage());
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String createOpDirect(String daty, String montantTTC, String idTVA, String idFournisseur, String idDevise, String remarque, String designation, String datyecheance, Connection c) throws Exception {
        String refU = u.getTuppleID();

        OrdonnerPayement op = new OrdonnerPayement("", "", daty, montantTTC, remarque);

        FactureFournisseur ff = new FactureFournisseur(Constante.getObjetFactureFournisseur(), "", remarque, daty, daty, idFournisseur, idTVA, montantTTC, "opAuto", idDevise);
        /* private String id,idingredient,idmere,compte;
    private double qte,pu;*/
        DetailsFactureFournisseur dts = new DetailsFactureFournisseur(ff.getId(), 1, Utilitaire.stringToDouble(montantTTC));
        dts.construirePK(c);
        //System.out.println("dts facture fournissseur : " + dts.getId() + " facture mere : " + ff.getId());
        op.setDed_Id(ff.getTuppleID());

        dts.insertToTableWithHisto(refU, c);
        ff.insertToTableWithHisto(refU, c);
        op.insertToTableWithHisto(refU, c);

        return op.getTuppleID();
    }

    public void validerOP(ClassMAPTable idOP) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            validerOP(idOP, c);

            c.commit();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void validerOP(ClassMAPTable idOP, Connection c) throws Exception {

        OrdonnerPayement cmd = (OrdonnerPayement) idOP;
        OrdonnerPayement[] ops = (OrdonnerPayement[]) CGenUtil.rechercher(cmd, null, null, c, "");
        if (ops.length == 0) {
            throw new Exception("OP n'existe pas");
        }
        validerObject(ops[0], c);

        FactureFournisseur ff = new FactureFournisseur();
        ff.setId(ops[0].getDed_Id());
        FactureFournisseur[] ffs = (FactureFournisseur[]) CGenUtil.rechercher(ff, null, null, c, "");
        if (ffs.length == 0) {
            throw new Exception("Aucune Facture fournisseur n'est lie a ce OP");
        }
        if (ffs[0].getEtat() < ConstanteEtat.getEtatValider()) {
            validerObject(ffs[0], c);
        }

    }

    public EtatdeCaisseDate[] traiteEtatCaisse(String daty1, String daty2, String caisse, String devise) throws Exception {
        Connection c = null;
        try {
            String req = "select c.DESCCAISSE,  d.val, "
                    + " cast(nvl((select sum(mv.debit) from mvtCaisse mv where mv.IDCAISSE=c.IDCAISSE and mv.daty>='" + daty1 + "' and mv.daty<='" + daty2 + "' and mv.idCaisse like '" + caisse + "' and mv.iddevise='" + devise + "'),0) as number(20,2)) as debit, "
                    + " cast(nvl((select sum(mv.credit) from mvtCaisse mv where mv.IDCAISSE=c.IDCAISSE and mv.daty>='" + daty1 + "' and mv.daty<='" + daty2 + "' and mv.idCaisse like '" + caisse + "'  and mv.iddevise='" + devise + "'),0) as number(20,2)) as credit, "
                    + " cast(nvl((select distinct r.montant from report r where r.CAISSE=c.IDCAISSE and r.DATY='" + daty1 + "' and r.devise = d.id),0) as number(20,2)) as report from caisse c, devise d where d.id = '" + devise + "'";

            //System.out.println(req);

            c = new UtilDB().GetConn();
            Statement st = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = st.executeQuery(req);
            EtatdeCaisseDate temp = null;
            Vector vect = new Vector();
            int i = 0;

            while (rs.next()) {
                temp = new EtatdeCaisseDate(Utilitaire.dateDuJourSql(), rs.getString(1), rs.getString(2), rs.getDouble(5), rs.getDouble(4), rs.getDouble(3));
                vect.add(i, temp);
                i++;
            }
            EtatdeCaisseDate[] resultats = new EtatdeCaisseDate[i];
            vect.copyInto(resultats);
            return resultats;
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public Object createMvtCaisse(MvtCaisse mvt) throws Exception {
        Connection c = null;
        try {
            //test efa misy report @io daty io ve??
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            Object o = createMvtCaisse(mvt, c);
            c.commit();
            return o;

        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public Object createMvtCaisse(MvtCaisse mvt, Connection c) throws Exception {
        Report report = new Report();
        Date dateavant = Utilitaire.ajoutJourDate(mvt.getDaty(), 1);
        //Report[] repts = (Report[]) CGenUtil.rechercher(report, null, null, c, " and caisse like '" + mvt.getIdcaisse() + "' and daty='" + Utilitaire.datetostring(dateavant) + "'");
        //if (repts.length > 0) {
        //  throw new Exception("Impossible d effectuer ce mouvement, la caisse est deja cloturee pour cette date");
        //}
        mvt.controler(c);
        /*EtatOP[] sop = (EtatOP[])CGenUtil.rechercher(new EtatOP(), null, null, " AND ID = '"+mvt.getIdordre()+"'");
                if(sop.length>0){
                    //System.out.println("OPOPOPOPO "+sop[0].getId()+" RESTE === "+sop[0].getReste());
                    if(sop[0].getReste() == 0) throw new Exception("OP DEJA PAYE");
                    if(mvt.getDebit() > sop[0].getReste()) throw new Exception("Montant superieur reste a payer");
                }*/
        return createObject(mvt, c);
    }

    public void createLivraison(Livraison liv) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            String[] lstCmd = Utilitaire.split(liv.getCommande(), ";");
            for (int a = 0; a < lstCmd.length; a++) {
                CommandeClient cml = new CommandeClient();
                CommandeClient[] lst = (CommandeClient[]) CGenUtil.rechercher(cml, null, null, c, " AND ID = '" + lstCmd[a] + "'");
                if (lst != null && lst.length > 0) {
                    lst[0].setEtat(20);
                    lst[0].updateToTableWithHisto(u.getTuppleID(), c);
                    enregistrerSortieCommande(lst[0], c);
                }
            }

            if (liv.getCarburant() > 0) {
                String datyS = Utilitaire.datetostring(liv.getDaty());
                createOpDirect(datyS, new Double(liv.getCarburant()).toString(), "0", "Station service", "Ar", "Carburant livraison " + liv.getDescription(), "Carburant livraison " + liv.getDescription(), null, c);
            }
            liv.construirePK(c);
            liv.insertToTableWithHisto(u.getTuppleID(), c);
            c.commit();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void createLivraison(Livraison liv, String table) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            String[] lstCmd = Utilitaire.split(liv.getCommande(), ";");
            for (int a = 0; a < lstCmd.length; a++) {
                CommandeClient cml = new CommandeClient();
                if (table != null && table.compareToIgnoreCase("") != 0) {
                    cml.setNomTable(table);
                }
                CommandeClient[] lst = (CommandeClient[]) CGenUtil.rechercher(cml, null, null, c, " AND ID = '" + lstCmd[a] + "'");
                if (lst != null && lst.length > 0) {
                    lst[0].setEtat(20);
                    lst[0].updateToTableWithHisto(u.getTuppleID(), c);
                    enregistrerSortieCommande(lst[0], c);
                }
            }

            if (liv.getCarburant() > 0) {
                String datyS = Utilitaire.datetostring(liv.getDaty());
                createOpDirect(datyS, new Double(liv.getCarburant()).toString(), "0", "Station service", "Ar", "Carburant livraison " + liv.getDescription(), "Carburant livraison " + liv.getDescription(), null, c);
            }
            liv.construirePK(c);
            liv.insertToTableWithHisto(u.getTuppleID(), c);
            c.commit();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    private void enregistrerSortieCommande(CommandeClient commande, Connection c) throws Exception {
        CommandeClientDetails aliment = new CommandeClientDetails();
        aliment.setNomTable("as_detailscommande_aliment2");
        CommandeClientDetails[] listesakafo = (CommandeClientDetails[]) CGenUtil.rechercher(aliment, null, null, c, " and idmere = '" + commande.getId() + "'");

        MvtStock mvtstocktemp = new MvtStock("0", "Sortie en stock commande", "", commande.getClient(), commande.getId(), Utilitaire.dateDuJourSql());
        TypeObjet o = new TypeObjet();
        o.setNomTable("point");
        TypeObjet liste[] = (TypeObjet[]) CGenUtil.rechercher(o, null, null, c, " ");
        if (commande.getNomTable().compareTo("as_commandeclient") == 0) {

            mvtstocktemp.setDepot(commande.getPoint());
        } else {
            for (int i = 0; i < liste.length; i++) {
                if (commande.getClient().compareToIgnoreCase(liste[i].getDesce()) == 0) {
                    mvtstocktemp.setDepot(liste[i].getId());
                }
            }

        }

        for (int i = 0; i < listesakafo.length; i++) {
            sortirStockProduit(listesakafo[i].getProduit(), listesakafo[i].getQuantite(), mvtstocktemp, c);
        }

    }

    private int sortirStockProduit(String produit, double quantite, MvtStock mvtstockmere, Connection c) throws Exception {
        Recette p = new Recette();
        Recette[] liste = (Recette[]) CGenUtil.rechercher(p, null, null, c, " and idproduits = '" + produit + "'");
        if (liste.length > 0) {
            createObject(mvtstockmere, c);
            validerObject(mvtstockmere, c);
        }
        for (int i = 0; i < liste.length; i++) {
            MvtStockFille fille = new MvtStockFille(mvtstockmere.getId(), liste[i].getIdingredients(), 0, liste[i].getQuantite() * quantite);
            createObject(fille, c);
        }
        return liste.length;
    }

    public String searchClient(String numero) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();

            return searchClient(numero, c);
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String searchClient(String numero, Connection c) throws Exception {
        Statement st = null;
        try {

            st = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = st.executeQuery("select nom from as_client where telephone like '" + numero + "'");

            if (rs.next()) {
                //System.out.println("test " + rs.getString(1));
                return rs.getString(1);
            } else {
                return "";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    public ClientAlloSakafo searchClientComplet(String numero) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();

            ClientAlloSakafo cml = new ClientAlloSakafo();
            ClientAlloSakafo[] lst = (ClientAlloSakafo[]) CGenUtil.rechercher(cml, null, null, c, " AND telephone like '%" + numero + "%'");

            if (lst.length > 0) {
                return lst[0];
            } else {
                return null;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    private String createFactureByPaiement(Paiement pmt, Connection c) throws Exception {

        //String client = searchClient (pmt.getClient(), c);
        FactureClient fc = new FactureClient();
        fc.setDesignation(pmt.getObservation());
        fc.setIdobjet(pmt.getIdobjet());
        fc.setTva(pmt.getTva());
        fc.setDaty(pmt.getDaty());
        fc.setClient(pmt.getClient());
        fc.construirePK(c);
        fc.insertToTableWithHisto(u.getTuppleID(), c);

        CommandeClientDetails cmll = new CommandeClientDetails();
        cmll.setNomTable("as_detailscommande_lib");
        CommandeClientDetails[] lsts = (CommandeClientDetails[]) CGenUtil.rechercher(cmll, null, null, c, " AND IDMERE = '" + pmt.getIdobjet() + "'");

        for (int i = 0; i < lsts.length; i++) {
            FactureClientDetails fcd = new FactureClientDetails();
            fcd.setIdmere(fc.getId());
            fcd.setDesignation(lsts[i].getProduit());
            fcd.setQuantite(lsts[i].getQuantite());
            fcd.setPu(lsts[i].getPu());
            fcd.setMontant(lsts[i].getMontant());
            fcd.construirePK(c);
            fcd.insertToTableWithHisto(u.getTuppleID(), c);
        }

        return fc.getId();
    }

    public String createFactureClient(ClassMAPTable factureclient) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            FactureClient fc = (FactureClient) factureclient;
            fc.construirePK(c);
            fc.insertToTableWithHisto(u.getTuppleID(), c);

            CommandeClientDetails cmll = new CommandeClientDetails();
            cmll.setNomTable("as_detailscommande_lib");
            CommandeClientDetails[] lsts = (CommandeClientDetails[]) CGenUtil.rechercher(cmll, null, null, c, " AND IDMERE = '" + fc.getIdobjet() + "'");

            for (int i = 0; i < lsts.length; i++) {
                FactureClientDetails fcd = new FactureClientDetails();
                fcd.setIdmere(fc.getId());
                fcd.setDesignation(lsts[i].getProduit());
                fcd.setQuantite(lsts[i].getQuantite());
                fcd.setPu(lsts[i].getPu());
                fcd.setMontant(lsts[i].getMontant());
                fcd.construirePK(c);
                fcd.insertToTableWithHisto(u.getTuppleID(), c);
            }

            CommandeClient cml = new CommandeClient();
            CommandeClient[] lst = (CommandeClient[]) CGenUtil.rechercher(cml, null, null, c, " AND ID = '" + fc.getIdobjet() + "'");
            if (lst != null && lst.length > 0) {
                lst[0].setEtat(30);
                lst[0].updateToTableWithHisto(u.getTuppleID(), c);
            }

            c.commit();
            return fc.getId();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String createFactureClient(ClassMAPTable factureclient, String nomtable) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            FactureClient fc = (FactureClient) factureclient;
            fc.construirePK(c);
            fc.insertToTableWithHisto(u.getTuppleID(), c);

            CommandeClientDetails cmll = new CommandeClientDetails();
            cmll.setNomTable("as_detailscommande_lib");
            CommandeClientDetails[] lsts = (CommandeClientDetails[]) CGenUtil.rechercher(cmll, null, null, c, " AND IDMERE = '" + fc.getIdobjet() + "'");

            for (int i = 0; i < lsts.length; i++) {
                FactureClientDetails fcd = new FactureClientDetails();
                fcd.setIdmere(fc.getId());
                fcd.setDesignation(lsts[i].getProduit());
                fcd.setQuantite(lsts[i].getQuantite());
                fcd.setPu(lsts[i].getPu());
                fcd.setMontant(lsts[i].getMontant());
                fcd.construirePK(c);
                fcd.insertToTableWithHisto(u.getTuppleID(), c);
            }

            CommandeClient cml = new CommandeClient();
            if (nomtable != null && nomtable.compareToIgnoreCase("") != 0) {
                cml.setNomTable(nomtable);
            }
            CommandeClient[] lst = (CommandeClient[]) CGenUtil.rechercher(cml, null, null, c, " AND ID = '" + fc.getIdobjet() + "'");
            if (lst != null && lst.length > 0) {
                lst[0].setEtat(30);
                lst[0].updateToTableWithHisto(u.getTuppleID(), c);
            }

            c.commit();
            return fc.getId();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void createPaiement(Paiement pmt, String nomtable) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            CommandeClientDetails cmll = new CommandeClientDetails();
            CommandeClientDetails[] lsts = (CommandeClientDetails[]) CGenUtil.rechercher(cmll, null, null, c, " AND IDMERE = '" + pmt.getIdobjet() + "'");

            Paiement[] lp = (Paiement[]) CGenUtil.rechercher(new Paiement(), null, null, c, " AND IDOBJET = '" + pmt.getIdobjet() + "'");

            double montant = 0;
            double paye = pmt.getMontant();
            for (int i = 0; i < lp.length; i++) {
                paye += lp[i].getMontant();
            }

            for (int i = 0; i < lsts.length; i++) {
                montant = montant + (lsts[i].getPu() * lsts[i].getQuantite());
            }

            if (paye > montant) {
                throw new Exception("Somme pay� sup�rieur � la commande");
            }

            pmt.construirePK(c);
            pmt.insertToTableWithHisto(u.getTuppleID(), c);

            // cr�ation facture auto
            String factureId = createFactureByPaiement(pmt, c);

            // cr�ation ordre de paiement
            OrdreDePaiement op = new OrdreDePaiement();
            op.setDed_id(factureId);
            op.setRemarque(pmt.getObservation());
            op.setModepaiement(pmt.getModepaiement());
            op.setMontant(pmt.getMontant());
            op.setIdligne(pmt.getNumpiece());
            op.setDaty(pmt.getDaty());
            op.construirePK(c);
            createObject(op, c);
            validerOR(op, c);

            //mouvement de caisse
            MvtCaisse mvt = new MvtCaisse(pmt.getObservation(), "Ar", pmt.getModepaiement(), pmt.getCaisse(), pmt.getObservation(), pmt.getClient(), pmt.getIdobjet(), pmt.getMontant(), 0, pmt.getDaty(), "1");
            createMvtCaisse(mvt, c);
            validerObject(mvt, c);

            // mise a jour etat commande
            CommandeClient cml = new CommandeClient();
            if (nomtable != null && nomtable.compareToIgnoreCase("") != 0) {
                cml.setNomTable(nomtable);
            }
            CommandeClient[] lst = (CommandeClient[]) CGenUtil.rechercher(cml, null, null, c, " AND ID = '" + pmt.getIdobjet() + "'");

            if (lst != null && lst.length > 0) {
                lst[0].setEtat(40);
                lst[0].updateToTableWithHisto(u.getTuppleID(), c);
            }

            c.commit();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void createPaiement(Paiement pmt) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            CommandeClientDetails cmll = new CommandeClientDetails();
            CommandeClientDetails[] lsts = (CommandeClientDetails[]) CGenUtil.rechercher(cmll, null, null, c, " AND IDMERE = '" + pmt.getIdobjet() + "'");

            Paiement[] lp = (Paiement[]) CGenUtil.rechercher(new Paiement(), null, null, c, " AND IDOBJET = '" + pmt.getIdobjet() + "'");

            double montant = 0;
            double paye = pmt.getMontant();
            for (int i = 0; i < lp.length; i++) {
                paye += lp[i].getMontant();
            }

            for (int i = 0; i < lsts.length; i++) {
                montant = montant + (lsts[i].getPu() * lsts[i].getQuantite());
            }

            if (paye > montant) {
                throw new Exception("Somme pay� sup�rieur � la commande");
            }

            pmt.construirePK(c);
            pmt.insertToTableWithHisto(u.getTuppleID(), c);

            // cr�ation facture auto
            String factureId = createFactureByPaiement(pmt, c);

            // cr�ation ordre de paiement
            OrdreDePaiement op = new OrdreDePaiement();
            op.setDed_id(factureId);
            op.setRemarque(pmt.getObservation());
            op.setModepaiement(pmt.getModepaiement());
            op.setMontant(pmt.getMontant());
            op.setIdligne(pmt.getNumpiece());
            op.setDaty(pmt.getDaty());
            op.construirePK(c);
            createObject(op, c);
            validerOR(op, c);

            //mouvement de caisse
            MvtCaisse mvt = new MvtCaisse(pmt.getObservation(), "Ar", pmt.getModepaiement(), pmt.getCaisse(), pmt.getObservation(), pmt.getClient(), pmt.getIdobjet(), pmt.getMontant(), 0, pmt.getDaty(), "1");
            createMvtCaisse(mvt, c);
            validerObject(mvt, c);

            // mise a jour etat commande
            CommandeClient cml = new CommandeClient();
            CommandeClient[] lst = (CommandeClient[]) CGenUtil.rechercher(cml, null, null, c, " AND ID = '" + pmt.getIdobjet() + "'");

            if (lst != null && lst.length > 0) {
                lst[0].setEtat(40);
                lst[0].updateToTableWithHisto(u.getTuppleID(), c);
            }

            c.commit();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void validerOR(ClassMAPTable idOR) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            validerOR(idOR, c);

            c.commit();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void validerOR(ClassMAPTable idOR, Connection c) throws Exception {
        OrdreDePaiement cmd = (OrdreDePaiement) idOR;
        OrdreDePaiement test = new OrdreDePaiement();
        test.setId(cmd.getId());
        OrdreDePaiement[] ops = (OrdreDePaiement[]) CGenUtil.rechercher(test, null, null, c, "");
        if (ops.length == 0) {
            throw new Exception("OR n'existe pas");
        }
        //System.out.println("id OR avant valider " + ops[0].getId() + " etat= " + ops[0].getEtat());
        validerObject(ops[0], c);

        FactureClient ff = new FactureClient();
        ff.setId(ops[0].getDed_id());
        FactureClient[] ffs = (FactureClient[]) CGenUtil.rechercher(ff, null, null, c, "");
        if (ffs.length == 0) {
            throw new Exception("Aucune Facture client n'est lie a ce OR");
        }
        if (ffs[0].getEtat() < ConstanteEtat.getEtatValider()) {
            validerObject(ffs[0], c);
        }
    }

    public String saveBCAvecDetails(BonDeCommande bc, String[] id, String[] quantiteparpack, String[] pu, String[] quantite) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            createObject(bc, c);
            for (int i = 0; i < id.length; i++) {
                double qte = Utilitaire.stringToDouble(quantite[i]);
                //System.out.println(id[i] + " - " + qte);
                if (qte > 0) {
                    double prixu = Utilitaire.stringToDouble(pu[i]);
                    double qtepack = Utilitaire.stringToDouble(quantiteparpack[i]);
                    BonDeCommandeFille fille = new BonDeCommandeFille(id[i], bc.getTuppleID(), "", prixu, qtepack, qte);
                    createObject(fille, c);
                }
            }
            c.commit();
            return bc.getTuppleID();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String generateOPByBC(String idbc) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            String id = "";
            //rechercher BC
            BonDeCommande bctemp = new BonDeCommande();
            BonDeCommande[] bcs = (BonDeCommande[]) CGenUtil.rechercher(bctemp, null, null, c, " and id='" + idbc + "'");
            if (bcs.length == 0) {
                throw new Exception("Bon de commande inexistant");
            }
            BonDeCommande bc = bcs[0];

            //test sao efa misy OP 
            OrdonnerPayement optemp = new OrdonnerPayement();
            OrdonnerPayement[] ops = (OrdonnerPayement[]) CGenUtil.rechercher(optemp, null, null, c, " and remarque='" + idbc + "'");
            if (ops.length == 0) {
                id = createOpDirect(Utilitaire.datetostring(bc.getDaty()), new Double(bc.getMontantTTC()).toString(), new Integer(bc.getTva()).toString(), bc.getFournisseur(), "Ar", idbc, bc.getDesignation(), null, c);
            } else {
                id = ops[0].getId();
            }

            c.commit();
            return id;
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String saveBLAvecDetails(BonDeLivraison bl, String[] id, String[] quantiteparpack, String[] quantite) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            createObject(bl, c);

            for (int i = 0; i < id.length; i++) {
                double qte = Utilitaire.stringToDouble(quantite[i]);
                //System.out.println(id[i] + " - " + qte);
                if (qte > 0) {
                    double qtepack = Utilitaire.stringToDouble(quantiteparpack[i]);
                    BonDeLivraisonFille fille = new BonDeLivraisonFille(id[i], bl.getTuppleID(), qtepack, qte);
                    createObject(fille, c);
                }
            }
            c.commit();
            return bl.getTuppleID();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String validerBL(String idbl) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            //rechercher BL
            BonDeLivraison bltemp = new BonDeLivraison();
            BonDeLivraison[] bls = (BonDeLivraison[]) CGenUtil.rechercher(bltemp, null, null, c, " and id='" + idbl + "'");
            if (bls.length == 0) {
                throw new Exception("Bon de livraison inexistant");
            }
            BonDeLivraison bl = bls[0];
            //viser BL
            validerObject(bl, c);

            BonDeCommande bctemp = new BonDeCommande();
            BonDeCommande[] bcs = (BonDeCommande[]) CGenUtil.rechercher(bctemp, null, null, c, " and id='" + bl.getIdbc() + "'");
            if (bcs.length == 0) {
                throw new Exception("Bon de commande correspondant inexistant");
            }
            //marquer BC livre
            BonDeCommande bc = bcs[0];
            bc.setEtat(ConstanteEtat.getEtatLivraison());
            bc.updateToTableWithHisto(u.getTuppleID(), c);

            //rechercher BLF
            BonDeLivraisonFille blftemp = new BonDeLivraisonFille();
            BonDeLivraisonFille[] blfs = (BonDeLivraisonFille[]) CGenUtil.rechercher(blftemp, null, null, c, " and numbl='" + idbl + "'");

            //enregistrer mvtstock & mvtstockfille
            //MvtStock(String typemvt, String designation, String observation, String fournisseur, String numbc, Date daty)
            MvtStock mvtstocktemp = new MvtStock("1", bc.getDesignation(), bl.getRemarque(), bc.getFournisseur(), bc.getId(), Utilitaire.dateDuJourSql());
            MvtStock mvtstock = (MvtStock) createObject(mvtstocktemp, c);
            validerObject(mvtstock, c);

            for (int i = 0; i < blfs.length; i++) {
                //MvtStockFille(String idmvtstock, String ingredients, double entree, double sortie) 
                MvtStockFille fille = new MvtStockFille(mvtstock.getId(), blfs[i].getProduit(), blfs[i].getTotal(), 0);
                createObject(fille, c);
            }

            c.commit();
            return bl.getTuppleID();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String updateBCAvecDetails(BonDeCommande bc, String[] id, String[] quantiteparpack, String[] pu, String[] quantite) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            bc.setNomTable("AS_BONDECOMMANDE");
            bc.updateToTableWithHisto(u.getTuppleID(), c);
            for (int i = 0; i < id.length; i++) {
                double qte = Utilitaire.stringToDouble(quantite[i]);
                //System.out.println(id[i] + " - " + qte);
                if (qte >= 0) {
                    BonDeCommandeFille p = new BonDeCommandeFille();
                    p.setId(id[i]);
                    BonDeCommandeFille[] lsd = (BonDeCommandeFille[]) CGenUtil.rechercher(p, null, null, c, " AND IDBC = '" + bc.getTuppleID() + "'");
                    BonDeCommandeFille fille = lsd[0];
                    fille.setPu(Utilitaire.stringToDouble(pu[i]));
                    fille.setQuantiteparpack(Utilitaire.stringToDouble(quantiteparpack[i]));
                    fille.setQuantite(qte);
                    fille.updateToTableWithHisto(u.getTuppleID(), c);
                }
            }
            c.commit();
            return bc.getTuppleID();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String updateBLAvecDetails(BonDeLivraison bl, String[] id, String[] quantiteparpack, String[] quantite) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            bl.setNomTable("AS_BONDELIVRAISON");
            this.updateObject(bl, c);
            //bl.updateToTableWithHisto(u.getTuppleID(), c);
            for (int i = 0; i < id.length; i++) {
                double qte = Utilitaire.stringToDouble(quantite[i]);
                if (qte >= 0) {
                    BonDeLivraisonFille p = new BonDeLivraisonFille();
                    p.setId(id[i]);
                    BonDeLivraisonFille[] lsd = (BonDeLivraisonFille[]) CGenUtil.rechercher(p, null, null, c, " AND numbl = '" + bl.getTuppleID() + "'");
                    BonDeLivraisonFille fille = lsd[0];
                    fille.setQuantiteparpack(Utilitaire.stringToDouble(quantiteparpack[i]));
                    fille.setQuantite(qte);
                    fille.updateToTableWithHisto(u.getTuppleID(), c);
                }
            }
            c.commit();
            return bl.getTuppleID();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public EtatdeStockDate[] traiteEtatStock(String dateDebut, String dateFin, String idIngredients, String unite) throws Exception {
        Connection c = null;
        try {
            String req = "select c.libelle,  d.val, cast(nvl((select sum(mv.entree) from as_mvtstock_filledetail mv where mv.INGREDIENTS=c.id and mv.daty>='" + dateDebut + "' and mv.daty<='" + dateFin + "' and mv.ingredients like '" + idIngredients + "' and mv.unite like '" + unite + "'),0) as number(20,2)) as entree, cast(nvl((select sum(mv.sortie) from as_mvtstock_filledetail mv where mv.INGREDIENTS=c.id and mv.daty>='" + dateDebut + "' and mv.daty<='" + dateFin + "' and mv.INGREDIENTS like '" + idIngredients + "' and mv.unite like '" + unite + "'),0) as number(20,2)) as sortie, cast(0 as number(20,2)) as report from as_ingredients c, as_unite d where d.id = c.unite";

            c = new UtilDB().GetConn();
            Statement st = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = st.executeQuery(req);
            EtatdeStockDate temp = null;
            Vector vect = new Vector();
            int i = 0;

            while (rs.next()) {
                temp = new EtatdeStockDate(Utilitaire.dateDuJourSql(), rs.getString(1), rs.getString(2), rs.getDouble(5), rs.getDouble(3), rs.getDouble(4));
                vect.add(i, temp);
                i++;
            }
            EtatdeStockDate[] resultats = new EtatdeStockDate[i];
            vect.copyInto(resultats);
            return resultats;
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void modifierRecette(String[] actionligne, String[] idIngredients, String[] libs, String[] qte, String idproduit) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            for (int i = 0; i < actionligne.length; i++) {
                if (actionligne[i].compareTo("ajout") == 0) {
                    Ingredients temp = new Ingredients();
                    temp.setId(idIngredients[i]);
                    Ingredients[] ingredients = (Ingredients[]) CGenUtil.rechercher(temp, null, null, c, "");
                    //test doublon
                    //search id dans recette de produit 
                    Recette tempR = new Recette();
                    Recette[] recettes = (Recette[]) CGenUtil.rechercher(tempR, null, null, c, " and idproduits='" + idproduit + "' and idingredients='" + ingredients[0].getId() + "'");
                    if (recettes.length > 0) {
                        throw new Exception("Cet ingredient est d�j� dans la recette " + libs[i]);
                    }
                    Recette recette = new Recette();
                    recette.setIdproduits(idproduit);
                    recette.setIdingredients(ingredients[0].getId());
                    recette.setUnite(ingredients[0].getUnite());
                    recette.setQuantite(Utilitaire.stringToDouble(qte[i]));
                    createObject(recette, c);
                } else if (actionligne[i].compareTo("modif") == 0) {
                    Recette temp = new Recette();
                    temp.setId(idIngredients[i]);
                    Recette[] recettes = (Recette[]) CGenUtil.rechercher(temp, null, null, c, "");
                    Recette recette = recettes[0];
                    recette.setQuantite(Utilitaire.stringToDouble(qte[i]));
                    recette.updateToTableWithHisto(u.getTuppleID(), c);
                } else if (actionligne[i].compareTo("supp") == 0) {
                    Recette temp = new Recette();
                    temp.setId(idIngredients[i]);
                    Recette[] recettes = (Recette[]) CGenUtil.rechercher(temp, null, null, c, "");
                    recettes[0].deleteToTable(c);
                }
            }

            c.commit();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String saveInventaire(MvtStock mvt, String[] id, String[] quantiteparpack, String[] quantite) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            MvtStock mvtstock = (MvtStock) createObject(mvt, c);
            validerObject(mvtstock, c);

            Ingredients p = new Ingredients();
            p.setNomTable("as_etatstock_term");
            Ingredients[] listef = (Ingredients[]) CGenUtil.rechercher(p, null, null, " order by id asc");
            double si = 0;
            double cumul = 0;
            for (int i = 0; i < id.length; i++) {

                //MvtStockFille(String idmvtstock, String ingredients, double entree, double sortie) 
                double total = Utilitaire.stringToDouble(quantiteparpack[i]) * Utilitaire.stringToDouble(quantite[i]);
                double difference = listef[i].getSeuil() - total;
                if (difference > 0) {
                    //sortie
                    MvtStockFille fille = new MvtStockFille(mvtstock.getId(), id[i], 0, difference);
                    createObject(fille, c);
                } else if (difference < 0) {
                    //entree
                    Date dateAvant = Utilitaire.getDateAvant(mvtstock.getDaty(), -1);
                    String daty = Utilitaire.datetostring(dateAvant);
                    EtatdeStockDate etat[] = (EtatdeStockDate[]) CGenUtil.rechercher(new EtatdeStockDate(), null, null, c, " and ingredients='" + id[i] + "' and daty='" + daty + "'");
                    MvtStockFille fille = new MvtStockFille(mvtstock.getId(), id[i], difference * -1, 0);
                    if (etat.length > 0) {
                        if (etat[0].getReport() == 0) {
                            fille.setStock_init(difference * -1);
                        } else {
                            cumul = etat[0].getReste();
                            fille.setStock_init(cumul);
                        }
                    }
                    createObject(fille, c);
                }
            }
            c.commit();
            return mvtstock.getId();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String saveInventaire(MvtStock mvt, String[] id, String[] quantiteparpack, String[] quantite, String[] remarque) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            MvtStock mvtstock = (MvtStock) createObject(mvt, c);
            validerObject(mvtstock, c);

            Ingredients p = new Ingredients();
            p.setNomTable("as_etatstock_term");
            Ingredients[] listef = (Ingredients[]) CGenUtil.rechercher(p, null, null, " order by id asc");

            for (int i = 0; i < id.length; i++) {
                //MvtStockFille(String idmvtstock, String ingredients, double entree, double sortie) 

                double total = Utilitaire.stringToDouble(quantiteparpack[i]) * Utilitaire.stringToDouble(quantite[i]);
                double difference = listef[i].getSeuil() - total;

                if (difference > 0) {
                    //sortie
                    MvtStockFille fille = new MvtStockFille(mvtstock.getId(), id[i], 0, difference);
                    createObject(fille, c);
                } else if (difference < 0) {
                    //entree
                    MvtStockFille fille = new MvtStockFille(mvtstock.getId(), id[i], difference * -1, 0);
                    createObject(fille, c);
                }
            }
            c.commit();
            return mvtstock.getId();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String saveInventaire(MvtStock mvt, String[] id, String[] quantiteparpack, String[] quantite, String typemvt) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            MvtStock mvtstock = (MvtStock) createObject(mvt, c);
            validerObject(mvtstock, c);
            double si = 0;
            double cumul = 0;
            for (int i = 0; i < id.length; i++) {
                //MvtStockFille(String idmvtstock, String ingredients, double entree, double sortie) 
                double total = Utilitaire.stringToDouble(quantiteparpack[i]) * Utilitaire.stringToDouble(quantite[i]);
                if (typemvt.compareTo("1") == 0 && total > 0) {
                    //sortie
                    MvtStockFille fille = new MvtStockFille(mvtstock.getId(), id[i], 0, total);
                    createObject(fille, c);
                }
                if (typemvt.compareTo("0") == 0 && total > 0) {
                    //entree
                    Date dateAvant = Utilitaire.getDateAvant(mvtstock.getDaty(), -1);
                    String daty = Utilitaire.datetostring(dateAvant);
                    EtatdeStockDate etat[] = (EtatdeStockDate[]) CGenUtil.rechercher(new EtatdeStockDate(), null, null, c, " and ingredients='" + id[i] + "' and daty='" + daty + "'");
                    MvtStockFille fille = new MvtStockFille(mvtstock.getId(), id[i], total, 0);
                    if (etat.length > 0) {
                        if (etat[0].getReport() == 0) {
                            fille.setStock_init(total);
                        } else {
                            cumul = etat[0].getReste();
                            fille.setStock_init(cumul);
                        }
                    }
                    createObject(fille, c);
                }
            }
            c.commit();
            return mvtstock.getId();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public String transfertInventaire(TransfertStock mvt, String[] id, String[] quantite, Connection c) throws Exception {
        String rep = null;
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            if (mvt.getDepot().compareTo(mvt.getDepotfinal()) == 0) {
                throw new Exception("le magasin initial et destination doivent etre differents");
            }
            //sortie
            MvtStock mvt_debut = mvt;
            mvt_debut.setNomTable("AS_MVT_STOCK");
            rep = saveInventaire(mvt_debut, id, quantite, "1", c);

            //entree
            MvtStock mvt_fin = mvt;
            mvt_fin.setNomTable("AS_MVT_STOCK");
            mvt_fin.construirePK(c);
            mvt_fin.setDepot(mvt.getDepotfinal());
            saveInventaire(mvt_fin, id, quantite, "0", c);
            if (verif == 1) {
                c.commit();
            }
            return rep;
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
    }

    public String saveInventaire(MvtStock mvt, String[] id, String[] quantite, String typemvt) throws Exception {
        Connection c = null;
        String rep = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            rep = saveInventaire(mvt, id, quantite, typemvt, c);
            c.commit();
            return rep;

        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    @Override
    public String saveInventaire(MvtStock mvt, String[] id, String[] quantite, String typemvt, Connection c) throws Exception {
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }

            MvtStock mvtstock = (MvtStock) createObject(mvt, c);

            double si = 0;
            double cumul = 0;
            for (int i = 0; i < id.length; i++) {
                //MvtStockFille(String idmvtstock, String ingredients, double entree, double sortie) 
                double total = Utilitaire.stringToDouble(quantite[i]);
                if (typemvt.compareTo("1") == 0 && total > 0) {
                    //sortie
                    MvtStockFille fille = new MvtStockFille(mvtstock.getId(), id[i], 0, total);
                    createObject(fille, c);
                }
                if (typemvt.compareTo("0") == 0 && total > 0) {
                    //entree
                    Date dateAvant = Utilitaire.getDateAvant(mvtstock.getDaty(), -1);
                    String daty = Utilitaire.datetostring(dateAvant);
                    EtatdeStockDate etat[] = (EtatdeStockDate[]) CGenUtil.rechercher(new EtatdeStockDate(), null, null, c, " and ingredients='" + id[i] + "' and daty='" + daty + "'");
                    MvtStockFille fille = new MvtStockFille(mvtstock.getId(), id[i], total, 0);
                    if (etat.length > 0) {
                        if (etat[0].getReport() == 0) {
                            fille.setStock_init(total);
                        } else {
                            cumul = etat[0].getReste();
                            fille.setStock_init(cumul);
                        }
                    }
                    createObject(fille, c);
                }
            }
            if (verif == 1) {
                c.commit();
            }
            return mvtstock.getId();
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
    }

    public TarifLivraison searchTarifByAdresse(String adresse) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();

            TarifLivraison tarif = new TarifLivraison();
            tarif.setNomTable("as_livraison_prixlibelle");
            TarifLivraison[] liste = (TarifLivraison[]) CGenUtil.rechercher(tarif, null, null, c, " and upper(quartier) like upper('%" + adresse + "%')");

            if (liste.length > 0) {
                return liste[0];
            } else {
                return null;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public Object insertReportAuto(ClassMAPTable o) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            Report report = (Report) o;

            //System.out.println(report.getDaty());

            String req1 = "select max(daty) as daty from mvtcaisse where idcaisse = '" + report.getCaisse() + "' and iddevise='" + report.getDevise() + "' and daty<'" + Utilitaire.datetostring(report.getDaty()) + "'";
            Statement st1 = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs1 = st1.executeQuery(req1);
            Date datydebut = null;
            if (rs1.next()) {
                datydebut = rs1.getDate(1);
            }

            Caisse caisse = new Caisse();
            caisse.setIdcaisse(report.getCaisse());
            Caisse[] listeC = (Caisse[]) CGenUtil.rechercher(caisse, null, null, c, "");

            //System.out.println(Utilitaire.datetostring(datydebut));
            EtatdeCaisseDate[] etats = traiteEtatCaisse(Utilitaire.datetostring(datydebut), Utilitaire.datetostring(datydebut), report.getCaisse(), report.getDevise());
            for (int i = 0; i < etats.length; i++) {
                if (etats[i].getCaisse().compareTo(listeC[0].getDesccaisse()) == 0) {
                    report.setMontant(etats[i].getDisponible());
                }
            }

            Report response = (Report) createObject(report, c);
            c.commit();
            return response;
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public EtatdeStockDate[] genererEtatStock(String daty1, String daty2) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            Date dateAvant = Utilitaire.getDateAvant(Utilitaire.stringDate(daty1), -1);
            String daty = Utilitaire.datetostring(dateAvant);
            double r1 = 0;
            EtatdeStockDate etat = new EtatdeStockDate();
            etat.setNomTable("AS_ETATSTOCK_LIBELLE1");
            // double entree=0;
            EtatdeStockDate[] reste = (EtatdeStockDate[]) CGenUtil.rechercher(etat, null, null, c, " and daty='" + daty + "'");
            EtatdeStockDate[] entre2Date = (EtatdeStockDate[]) CGenUtil.rechercher(etat, null, null, c, " and  daty between '" + daty1 + "' and '" + daty2 + "'");
            //System.out.println("l1" + reste.length);
            //System.out.println("l2" + entre2Date.length);
            for (int i = 0; i < entre2Date.length; i++) {
                r1 = r1 + reste[i].getReste();
                r1 = r1 + entre2Date[i].getEntree() - entre2Date[i].getSortie();
                entre2Date[i].setReste(r1);

            }
            return entre2Date;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public Object livrerCommande(String idCommande) throws Exception {
        Connection c = null;
        try {
            Livraison liv = new Livraison();
            liv.setCommande(idCommande);
            liv.setDaty(Utilitaire.dateDuJourSql());
            liv.setHeure(Utilitaire.heureCouranteHMS());
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            CommandeClient cml = new CommandeClient();
            CommandeClient[] lst = (CommandeClient[]) CGenUtil.rechercher(cml, null, null, c, " AND ID = '" + idCommande + "'");
            if (lst != null && lst.length > 0) {
                CommandeClientDetails[] listeDetails = (CommandeClientDetails[]) CGenUtil.rechercher(new CommandeClientDetails(), null, null, c, " AND idMere = '" + lst[0].getId() + "'");
                for (int i = 0; i < listeDetails.length; i++) {
                    listeDetails[i].setEtat(ConstanteEtat.getLivrer());
                    listeDetails[i].updateToTableWithHisto(u.getTuppleID(), c);
                }
                lst[0].setEtat(ConstanteEtat.getLivrer());
                lst[0].updateToTableWithHisto(u.getTuppleID(), c);
                enregistrerSortieCommande(lst[0], c);
            }

            liv.construirePK(c);
            liv.insertToTableWithHisto(u.getTuppleID(), c);
            c.commit();
            return cml;
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public Object payerCommande(String idCommande) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            Paiement pmt = new Paiement();
            pmt.setDaty(Utilitaire.string_date("dd/MM/yyyy", Utilitaire.dateDuJour()));
            pmt.setTva(0);
            pmt.setIdobjet(idCommande);
            double montantC = CommandeService.calculerMontantCommande(idCommande);
            CommandeClient cmc = CommandeService.getInfoCommandeClient(idCommande, "");
            pmt.setClient(cmc.getClient());
            pmt.setMontant(montantC);
            TypeObjet modeP = new TypeObjet();
            modeP.setNomTable("MODEPAIEMENT");
            TypeObjet[] tMode = (TypeObjet[]) CGenUtil.rechercher(modeP, null, null, c, " AND VAL = 'espece'");
            if (tMode.length > 0) {
                pmt.setModepaiement(tMode[0].getId());
            }
            Caisse[] tCaisse = (Caisse[]) CGenUtil.rechercher(new Caisse(), null, null, c, " AND desccaisse = 'caisse allo sakafo'");
            if (tCaisse.length > 0) {
                pmt.setCaisse(tCaisse[0].getIdcaisse());
            }

            CommandeClientDetails cmll = new CommandeClientDetails();
            CommandeClientDetails[] lsts = (CommandeClientDetails[]) CGenUtil.rechercher(cmll, null, null, c, " AND IDMERE = '" + pmt.getIdobjet() + "'");

            Paiement[] lp = (Paiement[]) CGenUtil.rechercher(new Paiement(), null, null, c, " AND IDOBJET = '" + pmt.getIdobjet() + "'");

            double montant = 0;
            double paye = pmt.getMontant();
            for (int i = 0; i < lp.length; i++) {
                paye += lp[i].getMontant();
            }

            for (int i = 0; i < lsts.length; i++) {
                montant = montant + (lsts[i].getPu() * lsts[i].getQuantite());
            }

            if (paye > montant) {
                throw new Exception("Somme pay� sup�rieur � la commande");
            }

            pmt.construirePK(c);
            pmt.insertToTableWithHisto(u.getTuppleID(), c);

            // cr�ation facture auto
            String factureId = createFactureByPaiement(pmt, c);

            // cr�ation ordre de paiement
            OrdreDePaiement op = new OrdreDePaiement();
            op.setDed_id(factureId);
            op.setRemarque(pmt.getObservation());
            op.setModepaiement(pmt.getModepaiement());
            op.setMontant(pmt.getMontant());
            op.setIdligne(pmt.getNumpiece());
            op.setDaty(pmt.getDaty());
            op.construirePK(c);
            createObject(op, c);
            validerOR(op, c);

            MvtCaisse mvt = new MvtCaisse(pmt.getObservation(), "Ar", pmt.getModepaiement(), pmt.getCaisse(), pmt.getObservation(), pmt.getClient(), pmt.getIdobjet(), pmt.getMontant(), 0, pmt.getDaty(), "1");
            createMvtCaisse(mvt, c);
            validerObject(mvt, c);

            CommandeClient cml = new CommandeClient();
            CommandeClient[] lst = (CommandeClient[]) CGenUtil.rechercher(cml, null, null, c, " AND ID = '" + pmt.getIdobjet() + "'");

            if (lst != null && lst.length > 0) {
                CommandeClientDetails[] listeDetails = (CommandeClientDetails[]) CGenUtil.rechercher(new CommandeClientDetails(), null, null, c, " AND idMere = '" + lst[0].getId() + "'");
                for (int i = 0; i < listeDetails.length; i++) {
                    listeDetails[i].setEtat(ConstanteEtat.getPayer());
                    listeDetails[i].updateToTableWithHisto(u.getTuppleID(), c);
                }
                lst[0].setEtat(ConstanteEtat.getPayer());
                lst[0].updateToTableWithHisto(u.getTuppleID(), c);
            }
            c.commit();
            return cml;
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void fabricationproduit(String idproduit, int quantite, Connection c) throws Exception {
        int indice = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                indice = 1;
            }

            Produits[] produits = (Produits[]) CGenUtil.rechercher(new Produits(), null, null, c, " AND ID = '" + idproduit + "'");
            if (produits.length == 0) {
                throw new Exception("Produit fabrication introuvable");
            }

            MvtStock mvtstocktemp = new MvtStock("0", "Sortie en stock commande", "", u.getTuppleID(), "", Utilitaire.dateDuJourSql());

            sortirStockProduit(idproduit, quantite, mvtstocktemp, c);

            MvtStock mvtstocktemp_entree = new MvtStock("", "Entree ", "", u.getTuppleID(), "", Utilitaire.dateDuJourSql());
            MvtStock mvtstock_entree = (MvtStock) createObject(mvtstocktemp_entree, c);
            validerObject(mvtstock_entree, c);

            MvtStockFille fille = new MvtStockFille(mvtstock_entree.getId(), produits[0].getIdIngredient(), quantite, 0);
            createObject(fille, c);

            if (indice == 1) {
                c.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && indice == 1) {
                c.close();
            }
        }
    }

    private void enregistrerSortieDetailsCommande(CommandeClientDetailsWithPoint commande, Connection c) throws Exception {

        MvtStock mvtstocktemp = new MvtStock("0", "Sortie en stock commande", "", "", commande.getId(), Utilitaire.dateDuJourSql(), commande.getPoint());
        sortirStockProduit(commande.getProduit(), commande.getQuantite(), mvtstocktemp, c);
    }

    public void insertlivrerCommandeDetails(String[] idCommande, Connection c) throws Exception {
        try {
            String id_str = Utilitaire.tabToString(idCommande, "'", ",");
            //System.out.println("Commande reshetr " + id_str);
            CommandeClientDetailsWithPoint[] listeDetails = (CommandeClientDetailsWithPoint[]) CGenUtil.rechercher(new CommandeClientDetailsWithPoint(), null, null, c, " and id in (" + id_str + ")");
            Livraison[] livraisons = (Livraison[]) CGenUtil.rechercher(new Livraison(), null, null, c, " and commande in (" + id_str + ") and etat = " + ConstanteEtat.getEtatValider());
            LivraisonApresCommande[] livApCmd = (LivraisonApresCommande[]) CGenUtil.rechercher(new LivraisonApresCommande(), null, null, c, " and idcommnademere in (" + id_str + ") and etat = " + ConstanteEtat.getEtatValider());
            if(livraisons.length!=0 || livApCmd.length!=0){
                throw new Exception("Livraison deja effectuee");
            }
          
            for (int i = 0; i < listeDetails.length; i++) {
                
                Livraison liv = new Livraison();
                liv.setCommande(listeDetails[i].getId());
                liv.setDaty(Utilitaire.dateDuJourSql());
                liv.setHeure(Utilitaire.heureCouranteHMS());
                liv.construirePK(c);
                liv.insertToTableWithHisto(u.getTuppleID(), c);
                //enregistrerSortieDetailsCommande(listeDetails[i], c);
                MvtStock mvt = listeDetails[i].creerMvtStock(c);
                mvt.insertAvecFille(this.getUser().getTuppleID(), c);
                mvt.validerObject(this.getUser().getTuppleID(), c);
            }
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        }
    }

    /*public void insertlivrerCommandeDetails2 (String[] idCommande, Connection c) throws Exception{
        try{
            String id_str = Utilitaire.tabToString(idCommande, "'", ",");
            CommandeClientDetails[] listeDetails=(CommandeClientDetails[])CGenUtil.rechercher(new CommandeClientDetails(), null, null, c, " and id in (" + id_str + ")");
            for(int i=0;i<listeDetails.length;i++){
                Livraison liv = new Livraison();
                liv.setCommande(listeDetails[i].getId());
                liv.setDaty(Utilitaire.dateDuJourSql());
                liv.setHeure(Utilitaire.heureCouranteHMS());
                liv.construirePK(c);
                liv.insertToTableWithHisto(u.getTuppleID(),c);
                enregistrerSortieDetailsCommande(listeDetails[i], c);
            }
        }
        catch(Exception e){
            throw e;
        }
    }*/
    public void livrerDetailsCommande(String[] id) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            if (id == null) {
                throw new Exception("Aucune commande selectione");
            }
            ArrayList<Object> mere = new CommandeService().setEtatDetailCommande(id, ConstanteEtat.getEtatLivraison(), "Commande d�j� livr�", c);

            for (int i = 0; i < mere.size(); i++) {
                new CommandeService().modifierEtatCommandeMere(mere.get(i).toString(), ConstanteEtat.getEtatLivraison(), c);
            }
            insertlivrerCommandeDetails(id, c);
            c.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw new Exception(ex.getMessage());
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    @Override
    public void saisieInventaireCategorie(InventaireMere inventaireMere, String[] idingredients, String[] quantites, Connection c) throws Exception {
        boolean newCon = false;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                newCon = true;
            }

            this.saisieInventaireCategorieTheorique(inventaireMere, idingredients, quantites, null, c);

            if (newCon && c != null) {
                c.commit();
            }
        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (newCon && c != null) {
                c.close();
            }
        }
    }

    @Override
    public void saisieInventaireCategorieTheorique(InventaireMere inventaireMere, String[] idingredients, String[] quantites, String[] qtetheorique, Connection c) throws Exception {
        boolean newCon = false;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                newCon = true;
            }

            inventaireMere.setId(null);
            inventaireMere = (InventaireMere) this.createObject(inventaireMere, c);
            inventaireMere.insertInventaireFille(idingredients, quantites, qtetheorique, this.getUser().getTuppleID(), c);

            if (newCon && c != null) {
                c.commit();
            }
        } catch (Exception ex) {
            if (c != null) {
                c.rollback();
            }
            throw ex;
        } finally {
            if (newCon && c != null) {
                c.close();
            }
        }
    }

    public void cloturerDetailsCommandeWithUser(String[] id, String caisse, Connection c) throws Exception {
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            new CommandeService().cloturerDetailsCommandeWithUser(id, caisse, this.getUser().getTuppleID(), c);
            for (int j = 0; j < id.length; j++) {
                //savesaPaiementDetailsCommande(id[j], c);
                savePaiementDetailsCommande(id[j], Constante.typeEspece, caisse, c);
            }
            if (verif == 1) {
                c.commit();
            }
        } catch (Exception e) {
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (verif == 1 && c != null) {
                c.close();
            }
        }

    }

    public void savePaiementDetailsCommande(String idCommande, String idcaisse, Connection c) throws Exception {
        savePaiementDetailsCommande(idCommande, Constante.typeEspece, idcaisse, c);
    }

    public void savePaiementCommande(String idCommande, Connection c) throws Exception {
        CommandeClient cmd = (CommandeClient) new CommandeClient().getById(idCommande, null, c);
        CommandeClientDetails[] lDet = cmd.getDetailsCommandeClient(null, c);
        for (int i = 0; i < lDet.length; i++) {
            savePaiementDetailsCommande(lDet[i].getId(), c);
        }
    }

    public void savePaiementDetailsCommande(String idCommande, Connection c) throws Exception {
        savePaiementDetailsCommande(idCommande, Constante.typeEspece, AlloSakafoService.getCaisseDefaut(), c);
    }

    public void savePaiementDetailsCommande(String idCommande, String modePayement, String caisse, Connection c) throws Exception {
        boolean estOuvert = false;
        try {
            if (c == null) {
                c = new utilitaire.UtilDB().GetConn();
                estOuvert = true;
            }
            Paiement pmt = new Paiement();
            pmt.setDaty(Utilitaire.dateDuJourSql());
            pmt.setTva(0);
            pmt.setIdobjet(idCommande);
            CommandeClientDetails cmll = new CommandeClientDetails();
            cmll.setId(idCommande);
            cmll.setNomTable("AS_DETAILSCOMMANDE_LETTRE");
            CommandeClientDetails[] tCommandeClientDetails = (CommandeClientDetails[]) CGenUtil.rechercher(cmll, null, null, c, " ");

            if (tCommandeClientDetails.length == 0) {
                throw new Exception("Commande detail introuvable");
            }
            CommandeClient cc = new CommandeClient();
            cc.setId(tCommandeClientDetails[0].getIdmere());
            CommandeClient[] tCommandeClient = (CommandeClient[]) CGenUtil.rechercher(cc, null, null, c, " ");
            if (tCommandeClient.length == 0) {
                throw new Exception("Commande introuvable");
            }
            pmt.setObservation(tCommandeClientDetails[0].getProduit());
            pmt.setClient(tCommandeClient[0].getClient());
            pmt.setMontant(tCommandeClientDetails[0].getQuantite() * tCommandeClientDetails[0].getPu());

            /*TypeObjet modeP = new TypeObjet();
            modeP.setNomTable("MODEPAIEMENT");
            TypeObjet[] tMode = (TypeObjet[])CGenUtil.rechercher(modeP, null, null, c, " AND VAL = 'espece'");
            if(tMode.length > 0){
                pmt.setModepaiement(tMode[0].getId());
            }
            Caisse[] tCaisse = (Caisse[])CGenUtil.rechercher(new Caisse(), null, null, c, " AND desccaisse = 'caisse allo sakafo'");
            if(tCaisse.length > 0){
                pmt.setCaisse(tCaisse[0].getIdcaisse());
            }*/
            pmt.setModepaiement(modePayement);
            pmt.setCaisse(caisse);

            //pmt.makePK(c);
            //pmt.insertToTableWithHisto(u.getTuppleID(), c);
            // cr�ation facture auto
            String factureId = createFactureByPaiement(pmt, c);

            // cr�ation ordre de paiement
            OrdreDePaiement op = new OrdreDePaiement();
            op.setDed_id(factureId);
            op.setRemarque(pmt.getObservation());
            op.setModepaiement(pmt.getModepaiement());
            op.setMontant(pmt.getMontant());
            op.setIdligne(pmt.getNumpiece());
            op.setDaty(pmt.getDaty());
            op.setEtat(ConstanteEtat.getEtatCreer());
            //op.construirePK(c);
            //System.out.println("id OR avant create "+op.getId()+ " etat= "+op.getEtat());
            createObject(op, c);
            //System.out.println("id ESSAIOP apres create "+essaiOP.getId()+ " etat= "+essaiOP.getEtat());
            validerOR(op, c);

            MvtCaisse mvt = new MvtCaisse(pmt.getObservation(), "Ar", pmt.getModepaiement(), pmt.getCaisse(), pmt.getObservation(), pmt.getClient(), pmt.getIdobjet(), pmt.getMontant(), 0, pmt.getDaty(), "1");
            createMvtCaisse(mvt, c);
            validerObject(mvt, c);
            if (c != null && estOuvert == true) {
                c.commit();
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw e;
        } finally {
            if (c != null && estOuvert == true) {
                c.close();
            }
        }
    }

    public void payerDetailsCommande(String[] id, String idcaisse, Connection c) throws Exception {
        int verif = 0;
        try {
            if (c == null) {
                c = new UtilDB().GetConn();
                c.setAutoCommit(false);
                verif = 1;
            }
            if (id == null) {
                throw new Exception("Aucune commande selectione");
            }
            ArrayList<Object> mere = new CommandeService().setEtatDetailCommande(id, ConstanteEtat.getEtatPaye(), "Commande d�j� pay�", c);

            for (int i = 0; i < mere.size(); i++) {
                CommandeClient clTemp = (CommandeClient) mere.get(i);
                if (clTemp.getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0) {
                    throw new Exception("Action non permise");
                }

                new CommandeService().modifierEtatCommandeMere(mere.get(i).toString(), ConstanteEtat.getEtatPaye(), c);
            }

            for (int j = 0; j < id.length; j++) {
                savePaiementDetailsCommande(id[j], idcaisse, c);
            }

            if (verif == 1) {
                c.commit();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw new Exception(ex.getMessage());
        } finally {
            if (c != null && verif == 1) {
                c.close();
            }
        }
    }

    public void payerDetailsCommande(String[] id, Connection c) throws Exception {
        try {
            payerDetailsCommande(id, AlloSakafoService.getCaisseDefaut(), c);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public void produitDisponible(String idProduit, String isDispo) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            String point = AlloSakafoService.getNomRestau();
            Indisponibilite indisp = new Indisponibilite(idProduit, point);
            Indisponibilite[] indispo = (Indisponibilite[]) CGenUtil.rechercher(indisp, null, null, c, " and idproduit like '" + idProduit + "' and idpoint like '" + point + "'");
            if (isDispo.compareToIgnoreCase("false") == 0) {
                //manao insert anaty indisponibilite
                if (indispo.length == 0) {
                    indisp.insertToTableWithHisto("" + this.getUser().getRefuser(), c);
                }
            } else {
                // delete
                if (indispo.length != 0) {
                    indispo[0].deleteToTableWithHisto("" + this.getUser().getRefuser(), c);
                }
                
            }
            c.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw new Exception(e.getMessage());
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void livrerDetailsCommandeLivraison(String[] id) throws Exception {
        Connection c = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            if (id == null) {
                throw new Exception("Aucune commande selectione");
            }
            int etat = ConstanteEtat.getEtatLivraison();
            ArrayList<Object> mere = new CommandeService().setEtatDetailCommandeLivraison(id, etat, "Commande d�j� livr�", c);

            for (int i = 0; i < mere.size(); i++) {
                CommandeClient clTemp = (CommandeClient) mere.get(i);
                if (clTemp.getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0) {
                    throw new Exception("Action non permise");
                }
            }

            for (int i = 0; i < mere.size(); i++) {
                CommandeClient clTemp = (CommandeClient) mere.get(i);
                if (clTemp.getPoint().compareToIgnoreCase(AlloSakafoService.getNomRestau()) != 0) {
                    throw new Exception("Action non permise");
                }
            }

            insertlivrerCommandeDetails(id, c);
            c.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (c != null) {
                c.rollback();
            }
            throw new Exception(ex.getMessage());
        } finally {
            c.close();
        }
    }

    public Object[] createObjectMultiple(ClassMAPTable[] o, String colonneMere, String idmere, Connection c) throws Exception {
        try {
            Object[] ret = new Object[o.length];
            for (int i = 0; i < o.length; i++) {
                o[i].setValChamp(colonneMere, idmere);
                ret[i] = createObject(o[i], c);
            }
            return ret;
        } catch (Exception ex) {
            ex.printStackTrace();
            c.rollback();
            throw ex;
        }

    }

    @Override
    public Object createObjectMultiple(ClassMAPTable mere, String colonneMere, ClassMAPTable[] fille) throws Exception {
        Connection c = null;
        Object ret = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);

            Object idmere = createObject(mere, c);
            //System.out.println(" ((ClassMAPTable)idmere).getTuppleID() === " + ((ClassMAPTable) idmere).getTuppleID());
            ret = createObjectMultiple(fille, colonneMere, ((ClassMAPTable) idmere).getTuppleID(), c);
            ret = idmere;

            c.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            c.rollback();
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    @Override
    public Object[] updateObjectMultiple(ClassMAPTable[] o) throws Exception {
        Connection c = null;
        Object[] ret = null;
        try {
            c = new UtilDB().GetConn();
            c.setAutoCommit(false);
            ret = updateObjectMultiple(o, c);
            c.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            c.rollback();
            throw ex;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    public Object[] updateObjectMultiple(ClassMAPTable[] o, Connection c) throws Exception {
        try {
            Object[] ret = new Object[o.length];

            for (int i = 0; i < o.length; i++) {
                ret[i] = updateObject(o[i], c);
            }
            return ret;
        } catch (Exception ex) {
            ex.printStackTrace();
            c.rollback();
            throw ex;
        }

    }

}
