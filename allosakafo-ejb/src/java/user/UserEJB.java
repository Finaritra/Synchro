package user;

import bean.ClassEtat;
import bean.ClassMAPTable;
import bean.ResultatEtSomme;
import bean.TypeObjet;
import historique.MapRoles;
import historique.MapUtilisateur;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import javax.ejb.Local;
import lc.Direction;
import mg.cnaps.configuration.Configuration;
import mg.cnaps.messagecommunication.MessageLibelle;
import mg.cnaps.utilisateur.CNAPSUser;
import mg.allosakafo.fin.EtatdeCaisseDate;
import mg.allosakafo.fin.MvtCaisse;
import mg.allosakafo.commande.*;
import mg.allosakafo.tiers.ClientAlloSakafo;
import mg.allosakafo.stock.BonDeCommande;
import mg.allosakafo.stock.BonDeLivraison;
import mg.allosakafo.stock.EtatdeStockDate;
import mg.allosakafo.stock.MvtStock;
import mg.allosakafo.secteur.TarifLivraison;
import mg.allosakafo.stock.InventaireMere;
import mg.allosakafo.stock.TransfertStock;



@Local
public interface UserEJB {
    public HashMap<String, String> getMapAutoComplete();
    public void savePaiementCommande(String idCommande,Connection c)throws Exception;
    public void savePaiementDetailsCommande(String idCommande,Connection c)throws Exception;
    public void saisieInventaireCategorie(InventaireMere inventaireMere, String[] idingredients, String[] quantites, Connection c) throws Exception;
     public void saisieInventaireCategorieTheorique(InventaireMere inventaireMere, String[] idingredients, String[] quantites,String[] qtetheorique, Connection c) throws Exception;
    public int payerApayer(String idCommande,String idCaisse) throws Exception;
    public void savePaiementDetailsCommande(String idCommande,String modePayement,String caisse, Connection c) throws Exception;
    public void cloturerDetailsCommandeWithUser(String []id,String caisse,Connection c) throws Exception;
    public ResultatEtSomme getDataPageMax(ClassMAPTable e, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, Connection c) throws Exception;

    public ResultatEtSomme getDataPageMax(ClassMAPTable e, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, Connection c, int npp) throws Exception;

    //public Object createRemisemagasinMultiple(String[] immo, String magasin) throws Exception;


    public void ejbRemove();

    public void annulerDetailsCommande(String[] id)throws Exception;
    public String sendSms(String[] idDest, String idmodele, String message, String service, String numov) throws Exception;

    public void deleteObjetFille(ClassMAPTable o, Connection conn) throws Exception;

    public void deleteObjetFille(ClassMAPTable o) throws Exception;

    
    public Object annulerObject(ClassMAPTable o) throws Exception;

    public Object annulerObject(ClassMAPTable o, Connection c) throws Exception;

    public String getMaxId(String table, String[] colonne, String[] listeCritere) throws Exception;

    public String getMaxId(String table) throws Exception;

    public void updateEtat(ClassMAPTable e, int valeurEtat, String id) throws Exception;

    public String createTypeObjet(String nomTable, String proc, String pref, String typ, String desc) throws Exception;

    public String updateTypeObjet(String table, String id, String typ, String desc) throws Exception;

    public TypeObjet[] findTypeObjet(String nomTable, String id, String typ) throws Exception;

    public int deleteTypeObjet(String nomTable, String id) throws Exception;

    public MapUtilisateur[] findUtilisateurs(String refuser, String loginuser, String pwduser, String nomuser, String adruser, String teluser, String idrole) throws Exception;

    public MapRoles[] findRole(String rol);

    public MapUtilisateur getUser();

    public int rejeterObjectMultiple(ClassEtat o, String[] listeIdObjet, String nomTableRejet, String nomTableCategorieRejet) throws Exception;

    public int viserObjectMultiple(ClassEtat o, String[] listeIdObjet) throws Exception;

    public int retournerObjectMultiple(ClassEtat o, String[] listeIdObjet, String motif) throws Exception;

    public int recuObjectMultiple(ClassEtat o, String[] listeIdObjet) throws Exception;

    public int validerObjectMultiple(ClassEtat[] o) throws Exception;

    public Object finaliser(ClassMAPTable o, Connection c) throws Exception;

    public Object finaliser(ClassMAPTable o) throws Exception;

    public String updateUtilisateurs(String refuser, String loginuser, String pwduser, String nomuser, String adruser, String teluser, String idrole) throws Exception;

    public int deleteUtilisateurs(String refuser) throws Exception;

    public String createUtilisateurs(String loginuser, String pwduser, String nomuser, String adruser, String teluser, String idrole) throws Exception;

    public int desactiveUtilisateur(String ref) throws Exception;

    public int activeUtilisateur(String ref) throws Exception;

    public void testLogin(String user, String pass) throws Exception;

    public Configuration[] findConfiguration() throws Exception;

    public String findHomePageServices(String codeService) throws Exception;

    public String mapperMereFille(ClassMAPTable e, String idMere, String[] idFille, String rem, String montant, String etat) throws Exception;

    public String mapperMereFille(ClassMAPTable e, String nomTable, String idMere, String[] idFille, String rem, String montant, String etat) throws Exception;

    public void deleteMereFille(ClassMAPTable e, String idMere, String[] liste_id_fille) throws Exception;

    public void annulerVisa(ClassMAPTable o) throws Exception; //

    public void deleteMereFille(ClassMAPTable e, String nomTable, String idMere, String[] liste_id_fille) throws Exception;

    public ResultatEtSomme getDataPage(ClassMAPTable e, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, Connection c) throws Exception;

    public ResultatEtSomme getDataPage(ClassMAPTable e, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, Connection c, int npp) throws Exception;

    public ResultatEtSomme getDataPageGroupe(ClassMAPTable e, String[] groupe, String[] sommeGroupe, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, String ordre, Connection c) throws Exception;

    public ResultatEtSomme getDataPageGroupe(ClassMAPTable e, String[] groupe, String[] sommeGroupe, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, String ordre, Connection c, int npp) throws Exception;

    public Object[] getData(ClassMAPTable e, String[] colInt, String[] valInt, Connection c, String apresWhere) throws Exception;

    public Object createObject(ClassMAPTable o) throws Exception;

    
    public Object dupliquerObject(ClassMAPTable o, String mapFille, String nomColonneMere) throws Exception;
    public Object dupliquerObject(ClassMAPTable o, String mapFille, String nomColonneMere, Connection c) throws Exception;
    public Object createObject(ClassMAPTable o, Connection c) throws Exception;
    public Object updateObject(ClassMAPTable o) throws Exception;
    public Object validerObject(ClassMAPTable o) throws Exception;
    /*public Object payerObject(ClassMAPTable o) throws Exception;
    public Object livrerObject(ClassMAPTable o) throws Exception;*/
    public Object updateEtatCommande(ClassMAPTable o, String etat) throws Exception;
    
    
    public Object rejeterObject(ClassMAPTable o) throws Exception;
    public Object cloturerObject(ClassMAPTable o) throws Exception;
    public void deleteObject(ClassMAPTable o) throws Exception;
    public void deleteObject(ClassMAPTable o,Connection c) throws Exception ;
    public boolean isSuperUser();
    public void setIdDirection(String idDirection);
    public String getIdDirection();
    public Direction[] findDirection(String idDir, String libelledir, String descdir, String abbrevDir, String idDirecteur) throws Exception;
    public ResultatEtSomme findResultatFinalePage(String nomTable, String apresW, String colonne, String ordre) throws Exception;
    
    public int testRestriction(String user, String permission, String table, Connection con) throws Exception;
    public String[] getAllTable() throws Exception;
    public config.Table[] getListeTable(String role, String adruser) throws Exception;
    public int deletePJ(String idDossier, String idPj) throws Exception;
    public int deletePJ(String idDossier, String idPj, Connection con) throws Exception;
    public int ajouterPJInfo(String info_valeur, String pieces_id, String info_id, String info_ok) throws Exception;
    public int ajouterPJInfo(String info_valeur, String pieces_id, String info_id, String info_ok, Connection con) throws Exception;
    public int ajouterPJTiers(String idPiece, List<String[]> listeTiers) throws Exception;
    public int ajouterPJTiers(String idPiece, List<String[]> listeTiers, Connection con) throws Exception;
    public int detacherTiers(String idtiers) throws Exception;
    
    
    public Object createObjectMultiple(ClassMAPTable[] o) throws Exception;
    public void createUploadedPj(String nomtable, String nomprocedure, String libelle, String chemin, String mere) throws Exception;//Upload generaliser
    public void deleteUploadedPj(String nomtable, String id) throws Exception;//delete d'upload generaliser
    public CNAPSUser getCnapsUser();
    public void ajoutrestriction(String[] table, String idrole, String idaction, String direction) throws Exception;
    public void ajoutrestriction(String[] val, String idrole, String idaction, String direction, Connection c) throws Exception;
    
    public String mapperMereToFilleMetier(ClassMAPTable e, String idMere, String[] idFille, String rem, String montant, String etat) throws Exception;
    public String mapperMereToFilleMetier(ClassMAPTable e, String nomTable, String idMere, String[] idFille, String rem, String montant, String etat) throws Exception;
    
    public void notifierObjectMultiple(String[] ids, String classe) throws Exception;
    public void updateEtat(ClassMAPTable e, int valeurEtat, String id, Connection c) throws Exception;
    public Object updateObject(ClassMAPTable o, Connection c) throws Exception;

    public Object createCommande(ClassMAPTable f, String[] produit, String[] qtte, String[] pu, String[] rmq) throws Exception;    
    public Object createCommandeMobile(ClassMAPTable f, String[] produit, String[] qtte, String[] pu, String[] rmq) throws Exception;
    public Object updateEtatFait(ClassMAPTable o) throws Exception;
    public Object updateCommande(ClassMAPTable f, String[] produit, String[] qtte, String[] pu, String[] rmq) throws Exception;
	
	public String createOpDirect(String daty, String montantTTC, String idTVA, String idFournisseur, String idDevise, String remarque,
				String designation, String datyecheance) throws Exception;
	
	public void validerOP(ClassMAPTable idOP) throws Exception;
	
	public EtatdeCaisseDate[] traiteEtatCaisse(String dateDebut,String dateFin,String idCaisse,String devise) throws Exception;
	
	public Object createMvtCaisse(MvtCaisse mvt) throws Exception;
	public void createLivraison (Livraison liv) throws Exception;
	public String searchClient (String numero) throws Exception;
	public ClientAlloSakafo searchClientComplet (String numero) throws Exception;
	public void createPaiement (Paiement paie) throws Exception;
	public void validerOR(ClassMAPTable idOR) throws Exception;

	public String saveBCAvecDetails(BonDeCommande bc, String[] id, String[] quantiteparpack, String[] pu , String[] quantite) throws Exception;
	
	public String generateOPByBC(String idbc) throws Exception;
	public String saveBLAvecDetails(BonDeLivraison bl, String[] id, String[] quantiteparpack, String[] quantite) throws Exception;
	public String validerBL (String idbl) throws Exception;
	public String updateBCAvecDetails(BonDeCommande bc, String[] id, String[] quantiteparpack, String[] pu , String[] quantite) throws Exception;
	public String updateBLAvecDetails(BonDeLivraison bl, String[] idFille, String[] quantiteparpack, String[] quantite) throws Exception;
	
	public EtatdeStockDate[] traiteEtatStock(String dateDebut,String dateFin,String idIngredients, String unite) throws Exception;
	
	public void modifierRecette(String[] actionligne, String[] idIngredients, String[] libs, String[] qte, String idproduit) throws Exception;
	
	public String saveInventaire(MvtStock mvt, String[] id, String[] quantiteparpack, String[] quantite) throws Exception;
	
	public String saveInventaire(MvtStock mvt, String[] id, String[] quantiteparpack, String[] quantite, String typemvt) throws Exception;
	
        
	public TarifLivraison searchTarifByAdresse (String adresse) throws Exception;
	
	public Object insertReportAuto(ClassMAPTable o) throws Exception;
	
	public String createFactureClient(ClassMAPTable factureclient,String nomtable) throws Exception;
        
        public String createFactureClient(ClassMAPTable factureclient) throws Exception;
        
        public String saveInventaire(MvtStock mvt, String[] id, String[] quantiteparpack, String[] quantite,String [] remarque) throws Exception;
        
        public void createLivraison (Livraison liv,String table) throws Exception;
        
        public void createPaiement (Paiement pmt,String nomtable) throws Exception;
        
        public EtatdeStockDate [] genererEtatStock(String daty1,String daty2) throws Exception;
        
        public String saveInventaire(MvtStock mvt, String[] id,String[] quantite, String typemvt) throws Exception;
        public String saveInventaire(MvtStock mvt, String[] id, String[] quantite, String typemvt,Connection c) throws Exception;
        
         public String transfertInventaire(TransfertStock mvt, String[] id,String[] quantite, Connection c) throws Exception;
        
        public Object livrerCommande (String idCommande) throws Exception;
        
        public Object payerCommande (String idCommande) throws Exception;
        
        public void livrerDetailsCommande(String[] id)throws Exception;
        
        public void payerDetailsCommande(String[] id, Connection c)throws Exception;
        
        public void produitDisponible(String idProduit, String isDispo) throws Exception;
        
        public String updateMontantUnionIntra(String nomTable, String id1, String[] id2, String[] montant) throws Exception;
        
        public ResultatEtSomme getDataPageGroupeMultiple(ClassMAPTable e, String[] groupe, String[] sommeGroupe, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, String ordre, Connection c) throws Exception;

        public ResultatEtSomme getDataPageGroupeMultiple(ClassMAPTable e, String[] groupe, String[] sommeGroupe, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, String ordre, Connection c, int npp) throws Exception;

        public ResultatEtSomme getDataPageGroupeMultiple(ClassMAPTable e, String[] groupe, String[] sommeGroupe, String[] colInt, String[] valInt, int numPage, String apresWhere, String[] nomColSomme, String ordre, Connection c, int npp, String count) throws Exception;
        public void livrerDetailsCommandeLivraison(String[] id) throws Exception ;
        public Object createObjectMultiple(ClassMAPTable mere, String colonneMere, ClassMAPTable[] fille) throws Exception;
         public Object[] updateObjectMultiple(ClassMAPTable[] o) throws Exception ;
         public Object validerObject(ClassMAPTable o, Connection c) throws Exception;
          public void payerDetailsCommande(String[] id, String idcaisse,Connection c) throws Exception;
}
