/*
 * To change user license header, choose License Headers in Project Properties.
 * To change user template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import bean.ClassMAPTable;
import historique.MapUtilisateur;
import java.sql.Connection;

import utilitaire.Constante;
import utilitaire.ConstanteEtat;
import utilitaire.ConstanteUser;
import bean.AdminGen;
import bean.CGenUtil;
import bean.ClassEtat;
import bean.ClassMAPTable;
import bean.ClassUser;
import bean.ErreurDAO;
import bean.ListeColonneTable;
import bean.RejetTable;
import bean.ResultatEtSomme;
import bean.TypeObjet;
import bean.TypeObjetUtil;
import bean.UnionIntraTable;
import bean.UploadPj;
import config.Config;
import config.Table;
import historique.HistoriqueEJBClient;
import historique.HistoriqueLocal;
import historique.MapHistorique;
import historique.MapRoles;
import historique.MapUtilisateur;
import historique.UtilisateurUtil;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import lc.Direction;
import lc.DirectionUtil;
import mg.cnaps.configuration.Configuration;
import utilisateur.ConstanteUtilisateur;
import utilisateur.HomePageURL;
import utilisateur.UtilisateurRole;
import utilitaire.Constante;
import utilitaire.ConstanteEtat;
import utilitaire.ConstanteUser;
import utilitaire.UtilDB;
import utilitaire.Utilitaire;
import utilitaire.UtilitaireFormule;
import utilitaire.UtilitaireMetier;
import mg.allosakafo.commande.*;
import mg.allosakafo.produits.*;
import mg.allosakafo.fin.*;
import mg.allosakafo.stock.MvtStock;
import mg.allosakafo.stock.MvtStockFille;
import mg.allosakafo.ded.*;
import mg.allosakafo.facture.Client;
import mg.allosakafo.facturefournisseur.*;
import mg.allosakafo.reservation.Reservation;
import mg.allosakafo.facturefournisseur.FactureFournisseur;
import mg.allosakafo.livraison.LivraisonApresCommande;
import service.AlloSakafoService;

public class CreateObject {
    public static Object createObject(ClassMAPTable o, Connection c, UserEJBBean u)throws Exception
    {
        return createObject(o,c,u.getUser(),u,u.getListeConfiguration());
    }
            

    public static Object createObject(ClassMAPTable o, Connection c, MapUtilisateur u, UserEJBBean user, Configuration[] listeConfig) throws Exception {
	try {
	    o.setMode("modif");
	    //RapportService.autoSaveRapport(o, u.getTuppleID(), c);
	    //if (o.getClass().getSuperclass().getSimpleName().compareToIgnoreCase("ClassEtat") == 0)
	    if (o instanceof bean.ClassEtat) {
		o.setValChamp("etat", Integer.valueOf(1));
		if(u!=null)o.setValChamp("iduser", u.getTuppleID());
	    }
	    //if (o.getClass().getSuperclass().getSimpleName().compareToIgnoreCase("ClassUser") == 0)
	    if (o instanceof bean.ClassUser) {
		o.setValChamp("iduser", u.getTuppleID());
	    }
            
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.commande.CommandeClient") == 0){
                CommandeClient cmd = (CommandeClient)o;
                cmd.setClient(cmd.getClient().trim());
            }if(o.getClassName().compareToIgnoreCase("mg.allosakafo.livraison.LivraisonApresCommande") == 0){
                LivraisonApresCommande lac = (LivraisonApresCommande)o;
                String idpoint = lac.getIdpoint();
               
                String idCommandeMere=lac.getIdcommnademere();
                String[] liste=Utilitaire.stringToTab(idCommandeMere, ";");
                String apres=Utilitaire.tabToString(liste, "'", ",");
                lac.setIdcommnademere(apres);
                lac.controler(c);
                CommandeClientDetailsFM [] listeDetails=(CommandeClientDetailsFM[]) CGenUtil.rechercher(new CommandeClientDetailsFM(), null, null, c, " and id in ("+apres+")");
                if(listeDetails.length>0){
                    for(int i=0;i<liste.length;i++){
                        CommandeClient cc = listeDetails[i].getCommandeClient();
                        CommandeClientDetails  ccd = listeDetails[i].getCommandeClientDetails();
                        
                        //Verification point
                        cc.verifPoint();
                        
                        lac.setIdcommnademere(liste[i]);
                        lac.construirePK(c);
                        lac.insertToTableWithHisto(u.getTuppleID(), c);
                        ccd.setEtat(ConstanteEtat.getEtatLivraison() );
                        ccd.updateToTableWithHisto(u.getTuppleID(), c);
                    }
                }
                return lac;
            }
             if(o.getClassName().compareToIgnoreCase("mg.allosakafo.produit.IngredientsPrecuit") == 0){
                IngredientsPrecuit cmd = (IngredientsPrecuit)o;
                // sortie stock produit initial
                MvtStock stock=new MvtStock("0", "Sortie en stock ingredient initial", "","", "", Utilitaire.dateDuJourSql());
                stock.construirePK(c);
                stock.insertToTableWithHisto(u.getTuppleID(), c);
                stock.setEtat(ConstanteEtat.getEtatValider());
                stock.updateToTableWithHisto(u.getTuppleID(), c);
                MvtStockFille fille=new MvtStockFille(stock.getId(),cmd.getId1(),0,cmd.getQuantite1());
                fille.construirePK(c);
                fille.insertToTableWithHisto(u.getTuppleID(), c);
                // entree stock produit final
                MvtStock stock1=new MvtStock("0", "Entree en stock ingredient precuit", "","", "", Utilitaire.dateDuJourSql());
                stock1.construirePK(c);
                stock1.insertToTableWithHisto(u.getTuppleID(), c);
                stock1.setEtat(ConstanteEtat.getEtatValider());
                stock1.updateToTableWithHisto(u.getTuppleID(), c);
                MvtStockFille fille1=new MvtStockFille(stock1.getId(),cmd.getId2(),cmd.getQuantite2(),0);
                fille1.construirePK(c);
                fille1.insertToTableWithHisto(u.getTuppleID(), c);
            }
             if(o.getClassName().compareToIgnoreCase("mg.allosakafo.fin.MvtCaisse")==0)
             {
                 MvtCaisse mvt=(MvtCaisse)o;
                 if(mvt.getTypemvt().compareToIgnoreCase(Constante.financeTypeAnnulation)==0&&mvt.getDebit()>0)
                 {
                    //OrdonnerPayement or=mvt.getOP(c);
                    MvtCaisse crtM=new MvtCaisse();
                    crtM.setIdordre(mvt.getIdordre());
                    MvtCaisse aAnnul=(MvtCaisse)CGenUtil.rechercher(crtM, null,null,c," order by daty desc")[0];
                    mg.allosakafo.ded.Annulation a=new mg.allosakafo.ded.Annulation();
                    a.setNomTable("annulationMvt");
                    a.setIdobjet(aAnnul.getId());
                    a.setDaty(Utilitaire.dateDuJourSql());
                    a.setMontant(mvt.getDebit());
                    a.setMotif("annulation du mvt "+aAnnul.getId());
                    a.setEtat(ConstanteEtat.getEtatCreer());
                    a.insertToTableWithHisto(u.getTuppleID(), c);
                    return a;
                 }
             }
             
			
	if(o.getClassName().compareToIgnoreCase("mg.allosakafo.produits.ProduitsPrix") == 0){
                ProduitsPrix produitPrix = (ProduitsPrix)o;
                
                /* String nom, String designation, String photo, String typeproduit, String calorie, String pa, double poids */
                Produits produit = new Produits(produitPrix.getNom(), produitPrix.getDesignation(), produitPrix.getPhoto(), produitPrix.getTypeproduit(), produitPrix.getCalorie(), produitPrix.getPa(), produitPrix.getPoids());
                produit.setPrixl(produitPrix.getPrixl());
                produit.construirePK(c);
                produit.insertToTableWithHisto(u.getTuppleID(), c);
                produit.insertIngredientRecetteBoisson(0,u.getTuppleID(),c);

                TarifProduits tarif = new TarifProduits(produit.getTuppleID(), produitPrix.getObservation(), produitPrix.getDateapplication(), produitPrix.getMontant());
                tarif.construirePK(c);
                tarif.insertToTableWithHisto(u.getTuppleID(), c);
                
                return produitPrix;
            }
            
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.facturefournisseur.FactureFournisseur") == 0){
                FactureFournisseur frs = (FactureFournisseur)o;
                frs.gererFournisseur(u.getTuppleID(), c);
                frs.construirePK(c);
                frs.insertToTableWithHisto(u.getTuppleID(), c);
                return frs;
            }
            //o.gerer(u.getTuppleID(),c);
            if (o.getClassName().compareToIgnoreCase("historique.MapUtilisateur") == 0) {
                MapUtilisateur util=(MapUtilisateur)o;
                user.createUtilisateurs(util.getLoginuser(), util.getPwduser(), util.getNomuser(), util.getAdruser(), util.getTeluser(), util.getIdrole());
               
                return util;
            }
             if (o.getClassName().compareToIgnoreCase("mg.allosakafo.produits.Recette") == 0) {
                Recette recette=(Recette)o;
                recette.inserer(u.getTuppleID(), c);
                return recette;
            } 
            /*if (o.getClassName().compareToIgnoreCase("mg.allosakafo.produits.Ingredients") == 0) {
                Ingredients ing=(Ingredients)o;
                ing.inserer(u.getTuppleID(), c);
                return ing;
            }*/
	    /*o.controler(c);
            
	    if (o.getTuppleID()==null||o.getTuppleID().compareToIgnoreCase("")==0)o.construirePK(c);
	    o.insertToTableWithHisto(u.getTuppleID(), c);*/
            o.controler(c);
            return o.createObject(u.getTuppleID(), c);
	} catch (Exception e) {
            c.rollback();
            e.printStackTrace();
	    throw e;
	}
    }
}
