/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import bean.CGenUtil;
import bean.ClassEtat;
import bean.ClassMAPTable;
import historique.MapUtilisateur;
import java.sql.Connection;
import mg.allosakafo.ded.Annulation;
import mg.allosakafo.commande.CommandeClient;
import mg.allosakafo.commande.CommandeClientDetails;
import mg.allosakafo.ded.OrdonnerPayement;
import mg.allosakafo.facturefournisseur.FactureFournisseur;
import mg.allosakafo.fin.MvtCaisse;
import mg.allosakafo.fin.MvtIntraCaisse;
import mg.allosakafo.produits.Fabrication;
import mg.allosakafo.reservation.Reservation;
import mg.allosakafo.reservation.ReservationTable;
import mg.allosakafo.stock.BonDeLivraison;
import mg.allosakafo.stock.InventaireMere;
import mg.cnaps.configuration.Configuration;

import service.AlloSakafoService;
import utilitaire.Constante;
import utilitaire.ConstanteEtat;
import utilitaire.Utilitaire;

public class ValiderObject {
    public static Object validerObject(Connection c,ClassMAPTable o,UserEJBBean user)throws Exception
    {
        return validerObject(c,o,user,user.getUser(),user.getListeConfiguration());
    }
    public static Object validerObject(Connection c, ClassMAPTable o, UserEJBBean user, MapUtilisateur u, Configuration[] listeConfig) throws Exception{
        try{
            ClassEtat []lo=null;
            if (o instanceof ClassEtat)
            {
                ClassEtat filtre=(ClassEtat)o.getClass().newInstance();
                filtre.setNomTable(o.getNomTable());
                
                lo=(ClassEtat [])CGenUtil.rechercher(filtre,null,null,c," and "+o.getAttributIDName()+"='"+o.getTuppleID()+"'");
                if(lo.length==0)throw new Exception("objet non existante");
                if (lo[0].getEtat()>=ConstanteEtat.getEtatValider())throw new Exception("objet deja vise");
                o=lo[0];
              
            }
            
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.stock.InventaireMere") == 0){
                InventaireMere ord = (InventaireMere)lo[0];
                ord.controlerDoublon(c);                
            }
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.fin.Report") == 0){
                  AlloSakafoService.saisie2Mvt(o,user,c);
            }
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.commande.CommandeClient") == 0){
                CommandeClient cmd = (CommandeClient)o; 
                CommandeClient[] commande;
                commande = (CommandeClient[])CGenUtil.rechercher(cmd, null, null, c, " AND ID = '"+cmd.getId()+"'");
                
                CommandeClientDetails[] listeDetails=(CommandeClientDetails[])CGenUtil.rechercher(new CommandeClientDetails(), null, null, c, " AND idMere = '" + commande[0].getId() + "'");
                for(int i=0;i<listeDetails.length;i++){
                    listeDetails[i].setEtat(ConstanteEtat.getEtatValider());
                    listeDetails[i].updateToTableWithHisto(u.getTuppleID(), c);
                }
                commande[0].setEtat(ConstanteEtat.getEtatValider());
                commande[0].updateToTableWithHisto(u.getTuppleID(), c);
                return commande[0];
            }
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.ded.OrdonnerPayement") == 0){
                OrdonnerPayement ord = (OrdonnerPayement)o;
                o.controler(c);                
            }
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.fin.MvtCaisse")==0)
            {
                MvtCaisse mvt = (MvtCaisse)o;
                
                //verification caisse
                MvtCaisse nv=new MvtCaisse();
                nv.setId(mvt.getId());
                mvt=(MvtCaisse)CGenUtil.rechercher(nv, null,null, c, "")[0];
                mvt.verifCaisse();
                
                if(mvt.getTypemvt().compareToIgnoreCase(Constante.financeTypeAnnulation)==0)//annulation mvt
                {
                    //Annulation an=(Annulation)CGenUtil.rechercher(new Annulation("%",mvt.getIdordre()), null,null,c,"")[0];
                    OrdonnerPayement crtOrd=new OrdonnerPayement();
                    crtOrd.setId(mvt.getIdordre());
                    crtOrd=(OrdonnerPayement)CGenUtil.rechercher(crtOrd, null,null,c,"")[0];
                    MvtCaisse crtM=new MvtCaisse();
                    crtM.setIdordre(crtOrd.getId());
                    MvtCaisse lMvt=(MvtCaisse)CGenUtil.rechercher(crtM, null,null,c," and id !='"+mvt.getId()+ "' order by daty desc")[0];
                    lMvt.setDebit(lMvt.getDebit()-mvt.getDebit());
                    lMvt.setCredit(lMvt.getCredit()-mvt.getCredit());
                    lMvt.updateToTableWithHisto(u.getTuppleID(), c);
                    Annulation an=new Annulation(lMvt.getId(),"Annulation mvt",mvt.getDebit(),Utilitaire.dateDuJourSql(),"AnnulationMvt");
                    an.setEtat(ConstanteEtat.getEtatValider());
                    an.insertToTableWithHisto(u.getTuppleID(), c);
                }
            }
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.ded.Annulation")==0)
            {
                Annulation an = (Annulation)o;
                //an=(Annulation)CGenUtil.rechercher(an,null, null, c, "")[0];
                if(an.getNomTable().compareToIgnoreCase("AnnulationOP")==0)
                {
                    OrdonnerPayement crt=new OrdonnerPayement();
                    crt.setId(an.getIdobjet());
                    OrdonnerPayement ord=(OrdonnerPayement)CGenUtil.rechercher(crt, null, null, c, "")[0];
                    ord.setMontant(ord.getMontant()-an.getMontant());
                    ord.updateToTableWithHisto(u.getTuppleID(), c);                    
                }
                if(an.getNomTable().compareToIgnoreCase("AnnulationMvt")==0)
                {
                    MvtCaisse crtM=new MvtCaisse();
                    crtM.setId(an.getIdobjet());
                    MvtCaisse lMvt=(MvtCaisse)CGenUtil.rechercher(crtM, null,null,c," order by daty desc")[0];
                    lMvt.setDebit(lMvt.getDebit()-an.getMontant());
                    lMvt.updateToTableWithHisto(u.getTuppleID(), c);   
                }
                
            }
            
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.fin.MvtIntraCaisse")==0)
            {
                MvtIntraCaisse mic = (MvtIntraCaisse)o;
                AlloSakafoService.viserMvtIntraCaisse(mic,user,c);
            }
            if(o.getClassName().compareToIgnoreCase("mg.allosakafo.produits.Fabrication")==0)
            {
               //Fabrication fab = (Fabrication)o; 
               //fab=(Fabrication)CGenUtil.rechercher(fab,null, null, c, "")[0];
               Fabrication fab=(Fabrication)lo[0];
//               fab.fabriquer(AlloSakafoService.getNomRestau(), user,c);
               fab.fabriquer(fab.getDepot(), user,c);
            }
             if(o.getClassName().compareToIgnoreCase("mg.allosakafo.facturefournisseur.FactureFournisseur")==0)
            {
                FactureFournisseur ff=(FactureFournisseur)lo[0];
                ff.controlerVisa(c); 
            }
              if(o.getClassName().compareToIgnoreCase("mg.allosakafo.stock.BonDeLivraison")==0)
            {
                    BonDeLivraison bon=(BonDeLivraison)lo[0];
                    bon.controlerVisa(c);
                    bon.saveMvtStock(user,c);
                   
            }
                    
            if(o instanceof ClassEtat)
            {
                ((ClassEtat) o).validerObject(u.getTuppleID(), c);
            }
            else
            {
                o.setMode("modif");
                o.controler(c);
                user.updateEtat(o, ConstanteEtat.getEtatValider(), o.getValInsert(o.getAttributIDName()), c);
            }

        }
        catch(Exception e){
            if(c!=null)c.rollback();
            throw e;
        }
        return o;
    }
}
