/*
 * To change user license header, choose License Headers in Project Properties.
 * To change user template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import bean.CGenUtil;
import bean.ClassEtat;
import bean.ClassMAPTable;
import bean.UnionIntraTable;
import historique.MapUtilisateur;
import historique.ParamCrypt;
import java.sql.Connection;
import mg.allosakafo.commande.CommandeClient;
import mg.allosakafo.facture.Client;
import mg.allosakafo.facture.Client2;
import utilisateur.Utilisateur;

import utilitaire.ConstanteEtat;
import utilitaire.Utilitaire;

public class UpdateObject {

    public static Object updateObject(ClassMAPTable o, Connection c, MapUtilisateur u, UserEJBBean user) throws Exception {
	try {
            
	    if (o instanceof bean.ClassEtat) {
                ClassEtat filtre=(ClassEtat)o.getClass().newInstance();
                filtre.setNomTable(o.getNomTable());
                
                ClassEtat[] lo=(ClassEtat [])CGenUtil.rechercher(filtre,null,null,c," and "+o.getAttributIDName()+"='"+o.getTuppleID()+"'");
                if(lo.length==0)throw new Exception("objet non existante");
                if (lo[0].getEtat()>=ConstanteEtat.getEtatValider())throw new Exception("objet deja vise");
                
                /*
                ClassEtat ce=(ClassEtat)o;
                if(ce.getEtat()>=ConstanteEtat.getValider())throw new Exception("Objet deja valide, non modifiable");
		o.setValChamp("iduser", u.getTuppleID());*/
	    }
	    if (o instanceof bean.ClassUser) {
		o.setValChamp("iduser", u.getTuppleID());
	    }if(o instanceof mg.allosakafo.commande.CommandeClient){
                CommandeClient cc=(CommandeClient)o;
                Client2 cl=(Client2)CGenUtil.rechercher(new Client2(),null,null,c," and id='"+cc.getClient()+"'")[0];
                if(cl!=null){
                    cl.setPrenom(cc.getObservation());
                    cl.updateToTableWithHisto(u.getTuppleID(), c);
                }
            }
           /* if(o instanceof utilisateur.Utilisateur){
                System.out.println("ATOOOOOOOO");
                Utilisateur cc=(Utilisateur)o;
                user.updateUtilisateurs(cc.getRefuser(), cc.getLoginuser(), cc.getPwduser(), cc.getNomuser(), cc.getAdruser(), cc.getTeluser(), cc.getIdrole());
            }   
            */
            
            if(o instanceof utilisateur.Utilisateur){
                Utilisateur util=(Utilisateur)o;
                ParamCrypt crt = new ParamCrypt();
                crt.setIdUtilisateur(String.valueOf(util.getRefuser()));
                ParamCrypt[] pc = (ParamCrypt[]) CGenUtil.rechercher(crt, null, null, c, "");
                if (pc.length == 0) {
                    throw new Exception("Pas de cryptage associe");
                }
                String passCrypt = Utilitaire.cryptWord(util.getPwduser(), pc[0].getNiveau(), pc[0].getCroissante());
                util.setPwduser(passCrypt);
                System.out.println("PWD** "+util.getPwduser());
                System.out.println("REFUSER** "+util.getRefuser());
                //return null;
                o = util;
            }
            
            
	    o.controlerUpdate(c);
	    o.updateToTableWithHisto(u.getTuppleID(), c);
	    return o;
	} catch (Exception e) {
	    throw e;
	}
    }
}
