
CREATE OR REPLACE VIEW "LIVRAISONLIB_CREER" as 
select * from LIVRAISONLIB 
WHERE ETAT=1;

CREATE OR REPLACE VIEW "LIVRAISONLIB_VISER" as 
select * from LIVRAISONLIB 
WHERE ETAT=11;

CREATE OR REPLACE VIEW LIVRAISONLIBCOMPLET
(ID, DATY, HEURE, IDCOMMNADEMERE, IDPOINT, 
 ADRESSE, IDLIVREUR, ETAT, CLIENT, MONTANT)
AS 
select

            liv.id,

           liv.daty,

           liv.heure,

           dc.produit||'--'||dc.IDACCOMPAGNEMENTSAUCE as idcommnademere,

            pt.VAL as idpoint,

           liv.adresse,

           l.nom||'--'|| l.contact as idlivreur,

           liv.etat,
           clt.nom||'--'||clt.telephone as client,
           (dc.quantite*dc.pu) as montant


        from livraison liv

        join AS_DETAILSCOMMANDELIB dc on liv.idcommnademere = dc.ID

        join POINT pt on pt.ID=liv.idpoint
        join as_commandeclient cmd on dc.idmere=cmd.id
        join as_client clt on cmd.client=clt.id
        join livreur l on liv.idlivreur = l.id
        
/

create or replace view LIVRAISONLIBVALIDECOMPLET as select * from LIVRAISONLIBCOMPLET where etat>=1;