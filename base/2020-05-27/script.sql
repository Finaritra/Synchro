

  CREATE OR REPLACE VIEW "AS_BONDELIVRAISON_FILLE_LIB" AS 
  select bcf.id, prd.libelle as produit, bcf.numbl, bcf.quantite,  bcf.quantiteparpack ,bcf.iddetailsfacturefournisseur
from as_bondelivraison_fille bcf join as_ingredients prd on bcf.produit = prd.id ;

create or replace view  as_bondelivraison_fille_date as
select blf.* , bl.daty from as_bondelivraison_fille_lib blf join as_bondelivraison bl on  blf.numbl = bl.id ;

create or replace view detailsffbonlivraisonid as
select  
dts.*,
f.quantite,
(dts.reste-f.quantite) as restelivre,
f.id as idblfille,
f.numbl as idblmere
from
detailsffbonlivraisonlib dts join
AS_BONDELIVRAISON_FILLE f on dts.id=f.iddetailsfacturefournisseur;



 
