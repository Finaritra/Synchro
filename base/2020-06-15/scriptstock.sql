 CREATE OR REPLACE VIEW "BESOINETINGREDIENT" ("QUANTITE", "LIBELLEPRODUIT", "IDPRODUITS", "LIBELLEINGREDIENT", "IDINGREDIENTS") AS 
  select comp.quantite*comp.qteav*b.QUANTITE as quantite,p.nom ||' ('||b.QUANTITE||')' as libelleproduit ,p.id as idproduits,ing.nom  as libelleingredient ,ing.id as idingredients  from 
  as_recettebesoincompose comp, produitEtIngredient p,produitEtIngredient ing, besoin b
  where comp.MERE=p.ID and comp.IDINGREDIENTS=ing.ID 
  and b.IDPRODUIT=p.id;