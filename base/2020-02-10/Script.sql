create table facture(
    id varchar2(100) constraint facture_pk primary key ,
    client varchar2(100)constraint facturefou_fk references CLIENT,
    modepaiement varchar2(100) constraint facturemod_pk references MODEPAIEMENT,
    adresseclient varchar2(100),
    datefacture date,
    datesaisie date,
    montant number(10,2),
    etat number(10)
);

CREATE SEQUENCE seqfacture
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION GETSEQfacture
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqfacture.nextval INTO retour FROM dual;
  return retour;
END;

create table Detailsfacture (
    id varchar2(100) constraint details_facture_pk primary key ,
    idfacture varchar2(100) constraint details_facture_fac_fk references  facture,
    idproduit varchar2(100) ,
    qte number(10,2),
    pu number(10,2),
    montant number(10,2)

);

CREATE SEQUENCE seqDetailsfacture
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION GETSEQDetailsfacture
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqfacture.nextval INTO retour FROM dual;
  return retour;
END;

/*create or replace view as 
	select
		detFac.id,
		detFac.idfacture,
		detFac.nom as idproduit,
		detFac.qte,
		detFac.pu,
		detFac.qte*detFac.pu as montant
		from Detailsfacture detFac group by produit;*/

create or replace view  facture_lib as
select f.id , 
    f.client ,
    mp.val as modepaiement, 
    f.adresseclient , 
    f.datefacture , 
    f.datesaisie , 
    f.montant , 
    f.etat  
    from facture f
join modepaiement mp on f.modepaiement=mp.id ;

create or replace view detailsfacture_lib as
select
    df.id ,
    f.id as idfacture ,
    p.nom as idproduit ,
    df.qte ,
    pu as pu,
    df.qte*pu as montant
    from facture f
    join detailsfacture df on f.id = df.idfacture
    join as_produits p on df.idproduit = p.id ;
create or replace view detailsfacture_grop as
select
       idproduit,
       pu ,
       f.id,
       f.modepaiement,
       f.adresseclient,
       f.datefacture,
        sum(detailsfacture_lib.montant) as montant,
       sum(qte) as qte
from detailsfacture_lib
join facture f on f.id = detailsfacture_lib.idfacture
group by idproduit,pu,f.id,
       f.modepaiement,
       f.adresseclient,
       f.datefacture;