  CREATE OR REPLACE FORCE VIEW AS_COMMANDECLIENT_LIBELLE_ID ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "VENTE", "ETAT", "SECT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM as responsable, cmd.client as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    vente 
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur 
    and cmd.vente=vente.id(+);