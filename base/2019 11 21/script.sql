create or replace view mvtintracaisselibcomplet as
    select
        mvt.*,
        c1.DESCCAISSE as desccaissedepart, c1.RESPCAISSE as respcaissedepart,c1.IDETATCAISSE as idetatcaissedepart,etatdep.VAL as valetatcaissedepart,etatdep.DESCE as desceetatcaissedepart,
        c2.DESCCAISSE as desccaissearrive, c2.RESPCAISSE as respcaissearrive,c2.IDETATCAISSE as idetatcaissearrive,etatarr.VAL as valetatcaissearrive,etatarr.DESCE as desceetatcaissearrive
    from MVTINTRACAISSE mvt
        join caisse c1 on c1.IDCAISSE = mvt.CAISSEDEPART
        join etatcaisse etatdep on etatdep.ID = c1.IDETATCAISSE
        join caisse c2 on c2.IDCAISSE = mvt.CAISSEARRIVE
        join etatcaisse etatarr on etatarr.ID = c2.IDETATCAISSE;
		
create or replace view mvtintracaisselib as
	select
		id,
		daty,
		desccaissedepart as caissedepart,
		desccaissearrive as caissearrive,
		montant,
		remarque,
		iduser,
		etat
	from mvtintracaisselibcomplet;
	
create or replace view reportlibcomplet as
	select
		rep.*,
		c.*,
		etat.val as valetatcaisse, etat.desce as desceetatcaisse
	from report rep
		join caisse c on c.idcaisse = rep.caisse
		join etatcaisse etat on etat.id = c.idetatcaisse;
		
create or replace view reportlib as
	select
		id,
		daty,
		montant,
		devise,
		desccaisse as caisse,
		etat
	from reportlibcomplet;


CREATE OR REPLACE VIEW MVTINTRACAISSELIB
(ID, DATY, CAISSEDEPART, CAISSEARRIVE, MONTANT, 
 REMARQUE, IDUSER, ETAT)
AS 
select m.id,m.daty,c1.DESCCAISSE as caissedepart ,c2.desccaisse as caisseArrive,m.MONTANT,m.REMARQUE,m.IDUSER,m.etat from MVTINTRACAISSE m,caisse c1,caisse c2
where m.CAISSEDEPART=c1.IDCAISSE and m.caisseArrive=c2.IDCAISSE
/

CREATE OR REPLACE VIEW MVTINTRACAISSELIBCREE
(ID, DATY, CAISSEDEPART, CAISSEARRIVE, MONTANT, 
 REMARQUE, IDUSER, ETAT)
AS 
select m."ID",m."DATY",m."CAISSEDEPART",m."CAISSEARRIVE",m."MONTANT",m."REMARQUE",m."IDUSER",m."ETAT" from MVTINTRACAISSELIB m where m.etat=1
/

CREATE OR REPLACE VIEW MVTINTRACAISSELIBVALIDE
(ID, DATY, CAISSEDEPART, CAISSEARRIVE, MONTANT, 
 REMARQUE, IDUSER, ETAT)
AS 
select m."ID",m."DATY",m."CAISSEDEPART",m."CAISSEARRIVE",m."MONTANT",m."REMARQUE",m."IDUSER",m."ETAT" from MVTINTRACAISSELIB m where m.etat>=11
/

