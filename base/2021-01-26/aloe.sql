
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00012', 'Aloe CITRON - NATURE 350 ml', 'Aloe CITRON - NATURE 350 ml', null,'TPD00001','1','0',null,null,null,null,'9000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00014', 'PRDTBOI00012', TO_DATE('10-01-2021','DD-MM-YYYY'), 7500);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0020', 'Aloe CITRON - NATURE 35 cl', 'UNT00004', 1, 0, 'CI00001');
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00013', 'Aloe CITRON - NATURE 500 ml', 'Aloe CITRON - NATURE 50 cl', null,'TPD00001','1','0',null,null,null,null,'11000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00015', 'PRDTBOI00013', TO_DATE('10-01-2021','DD-MM-YYYY'), 9000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0021', 'Aloe CITRON - NATURE 500 ml', 'UNT00004', 1, 0, 'CI00001');


INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00014', 'Aloe Drink mangue 350 ml', 'Aloe DRINK Mangue 350 ml', null,'TPD00001','1','0',null,null,null,null,'9000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00016', 'PRDTBOI00014', TO_DATE('10-01-2021','DD-MM-YYYY'), 7500);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0022', 'Aloe mangue 35 cl', 'UNT00004', 1, 0, 'CI00001');
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00015', 'Aloe Drink Mangue 500 ml', 'Aloe Drink Mangue 500 ml', null,'TPD00001','1','0',null,null,null,null,'11000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00017', 'PRDTBOI00015', TO_DATE('10-01-2021','DD-MM-YYYY'), 9000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0023', 'Aloe Drink Mangue 500 ml', 'UNT00004', 1, 0, 'CI00001');

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00016', 'Aloe Drink litchi 350 ml', 'Aloe DRINK litchi 350 ml', null,'TPD00001','1','0',null,null,null,null,'9000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00018', 'PRDTBOI00016', TO_DATE('10-01-2021','DD-MM-YYYY'), 7500);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0025', 'Aloe litchi 35 cl', 'UNT00004', 1, 0, 'CI00001');
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00017', 'Aloe Drink Litchi 500 ml', 'Aloe Drink Litchi 500 ml', null,'TPD00001','1','0',null,null,null,null,'11000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00019', 'PRDTBOI00017', TO_DATE('10-01-2021','DD-MM-YYYY'), 9000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0026', 'Aloe Drink Litchi 500 ml', 'UNT00004', 1, 0, 'CI00001');


INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00018', 'Aloe Drink Passion 350 ml', 'Aloe DRINK Passion 350 ml', null,'TPD00001','1','0',null,null,null,null,'9000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00019', 'PRDTBOI00018', TO_DATE('10-01-2021','DD-MM-YYYY'), 7500);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0027', 'Aloe Passion 35 cl', 'UNT00004', 1, 0, 'CI00001');
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00019', 'Aloe Drink Passion 500 ml', 'Aloe Drink Passion 500 ml', null,'TPD00001','1','0',null,null,null,null,'11000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00020', 'PRDTBOI00019', TO_DATE('10-01-2021','DD-MM-YYYY'), 9000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0028', 'Aloe Drink Passion 500 ml', 'UNT00004', 1, 0, 'CI00001');


INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00020', 'Aloe Drink Peche 350 ml', 'Aloe DRINK Peche 350 ml', null,'TPD00001','1','0',null,null,null,null,'9000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00021', 'PRDTBOI00020', TO_DATE('10-01-2021','DD-MM-YYYY'), 7500);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0029', 'Aloe Peche 35 cl', 'UNT00004', 1, 0, 'CI00001');
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00021', 'Aloe Drink Peche 500 ml', 'Aloe Drink Peche 500 ml', null,'TPD00001','1','0',null,null,null,null,'11000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00022', 'PRDTBOI00021', TO_DATE('10-01-2021','DD-MM-YYYY'), 9000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0030', 'Aloe Drink Peche 500 ml', 'UNT00004', 1, 0, 'CI00001');

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00022', 'Aloe Drink Original 350 ml', 'Aloe DRINK Original 350 ml', null,'TPD00001','1','0',null,null,null,null,'9000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00023', 'PRDTBOI00022', TO_DATE('10-01-2021','DD-MM-YYYY'), 7500);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0031', 'Aloe Original 35 cl', 'UNT00004', 1, 0, 'CI00001');
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00023', 'Aloe Drink Original 500 ml', 'Aloe Drink Original 500 ml', null,'TPD00001','1','0',null,null,null,null,'11000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00024', 'PRDTBOI00023', TO_DATE('10-01-2021','DD-MM-YYYY'), 9000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0032', 'Aloe Drink Original 500 ml', 'UNT00004', 1, 0, 'CI00001');

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00030', 'Aloe Drink Passion 500 ml', 'Aloe Drink Passion 500 ml', null,'TPD00001','1','0',null,null,null,null,'11000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00030', 'PRDTBOI00030', TO_DATE('10-01-2021','DD-MM-YYYY'), 9000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGB0032', 'Aloe Drink Passion 500 ml', 'UNT00004', 1, 0, 'CI00001');

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00031', 'Aloe Drink Passion 350 ml', 'Aloe DRINK Passion 350 ml', null,'TPD00001','1','0',null,null,null,null,'9000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00031', 'PRDTBOI00031', TO_DATE('10-01-2021','DD-MM-YYYY'), 9000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0032', 'Aloe Drink Passion 500 ml', 'UNT00004', 1, 0, 'CI00001');
