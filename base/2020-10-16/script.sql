CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_TYPEP_REST_NOT
(
   ID,
   NOMTABLE,
   TYPEPRODUIT,
   DATECOMMANDE,
   HEURELIV,
   RESTAURANT,
   PRODUIT,
   QUANTITE,
   PU,
   MONTANT,
   ETAT,
   ACCO_SAUCE,
   IDCLIENT
)
AS
   SELECT id,
          nomtable,
          typeproduit,
          datecommande,
          heureliv,
          restaurant,
          produit,
          quantite,
          pu,
          montant,
          etat,
          acco_sauce,
          idclient
     FROM VUE_CMD_CLT_DETAIL_TYPEP_REST cmd
    WHERE not exists (select * from mvtCaisse mvt where MVT.IDORDRE=cmd.id and mvt.etat>=11) and cmd.etat>0;


CREATE OR REPLACE FORCE VIEW VUE_CMD_ADDITION_LIVRAISON
(
   ID,
   NOMTABLE,
   TYPEPRODUIT,
   DATECOMMANDE,
   HEURELIV,
   RESTAURANT,
   PRODUIT,
   QUANTITE,
   PU,
   MONTANT,
   ETAT,
   ACCO_SAUCE
)
AS
     SELECT det.id,
            cmd.client AS nomtable,
            tp.val AS typeproduit,
            cmd.datecommande,
            cmd.heureliv,
            cmd.point AS restaurant,
            prod.nom AS produit,
            det.quantite,
            det.pu,
            (det.quantite * det.pu) AS montant,
            det.etat,
            accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT AS acco_sauce
       FROM as_commandeclient cmd
            JOIN as_detailscommande det ON det.idmere = cmd.id
            JOIN as_produits prod ON prod.id = det.produit
            JOIN as_typeproduit tp ON tp.id = prod.typeproduit
            LEFT JOIN AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce
               ON det.IDACCOMPAGNEMENTSAUCE = accsauce.id
      WHERE det.etat >= 1 AND det.etat < 40 and not exists (select * from mvtCaisse mvt where MVT.IDORDRE=det.id and mvt.etat>=11)
   ORDER BY cmd.datecommande DESC, cmd.heureliv DESC, det.etat DESC;
   
Insert into UTILISATEUR
   (REFUSER, LOGINUSER, PWDUSER, NOMUSER, ADRUSER, 
    TELUSER, IDROLE)
 Values
   (3456, 'front', 'm7lm', 'front', 'DIR000006', 
    '-', 'drl');

   Insert into PARAMCRYPT
   (ID, NIVEAU, CROISSANTE, IDUTILISATEUR)
 Values
   ('CRYM0004', 7, 1, '3456');
COMMIT;



