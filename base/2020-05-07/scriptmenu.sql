create table detailsfacturefournisseur(
Id varchar2(100) constraint Ajoutdetailsfacture_pk PRIMARY KEY,
Idingredient varchar(100) constraint ajout_ingredient_fk references as_ingredients,
Qte number(10,2),
Pu number(10,2),
Idmere varchar2(100) constraint Ajoutdetailsfacture_fk  references facturefournisseur
);

CREATE SEQUENCE seqDetailsfacturefourni
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getseqDetailsfacturefourni
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqDetailsfacturefourni.nextval INTO retour FROM dual;
  return retour;
END;
/

CREATE OR REPLACE VIEW "AS_ETATSTOCKVIDE" AS 
select 
'-' as ingredients,
'-' as unite,
CAST(0 as NUMBER(10,2)) as entree,
CAST(0 as NUMBER(10,2)) as sortie,
CAST(0 as NUMBER(10,2)) as reste,
CAST(0 as NUMBER(10,2)) as report,
'' as daty,
'' as idcategorieingredient,
'-' as point,
CAST(0 as NUMBER(10,2)) as besoin
 from dual;

create or replace view facturefournisseurglobal as
    select 
        detfacfou.id,
        detfacfou.idingredient,
        detfacfou.qte,
        detfacfou.pu,
        detfacfou.idmere,
        facfou.daty,
        facfou.idfournisseur
        from detailsfacturefournisseur detfacfou
        join facturefournisseur facfou on facfou.id=detfacfou.idmere;


create or replace view FACTUREFOURNISSEURGLOBALMAX as
    (select idfournisseur,pu,idingredient,daty
    from facturefournisseurglobal factF
    where factF.daty=(select max(daty) from facturefournisseurglobal factfI  where factfI.IDINGREDIENT=factF.IDINGREDIENT))
/


create or replace view as_ingredient_besoin as
    select 
        asing.id,
        asing.libelle,
        asing.seuil,
        asing.unite,
        asing.pu,
        asing.actif,
        asing.photo,
        asing.calorie,
        asing.durre,
        asing.compose,
        asing.categorieingredient,
        be.quantite as quantiteparpack,
        be.remarque
        from AS_INGREDIENTS_LIB_SAISIE asing
        left join besoin be on be.idproduit=asing.id;

create or replace view besoinlib as
select 
    bes.id,
    prd.LIBELLE as idproduit,
    bes.remarque,
    bes.quantite
    from besoin bes 
    join as_ingredients prd on prd.id=bes.idproduit;