--accompagnements crepe sucrée miala
update as_accompagnement_sauce set etat=0 where remarque in ('PRDTMP000042', 'PRDTMP000043', 'PRDTMP000044', 'PRDTMP000045');

--Crepe 42 43 44=> Crepe chocolat
update as_produits set nom='Crepe chocolat', designation='Crepe chocolat' where id = 'PRDTMP000042';


INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTMP000255', 'Crepe miel orange', 'Crepe miel orange', null,'ASTP000010','1','0',null,null,null,null,'10000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRP000255', 'PRDTMP000255', TO_DATE('10-01-2021','DD-MM-YYYY'), 6000);
INSERT INTO AS_RECETTE(ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, UNITE) VALUES ('RCE00020','PRDTMP000255','INGS000838','2','UNT00005');

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTMP000256', 'Crepe caramel', 'Crepe caramel', null,'ASTP000010','1','0',null,null,null,null,'10000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRP000256', 'PRDTMP000256', TO_DATE('10-01-2021','DD-MM-YYYY'), 6000);
INSERT INTO AS_RECETTE(ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, UNITE) VALUES ('RCE00021','PRDTMP000256','INGS000838','2','UNT00005');



commit;