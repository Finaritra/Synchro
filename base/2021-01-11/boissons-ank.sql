INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0001', 'Lazan`i Fisakana', 'Lazan`i Fisakana', null,'TPD00001','1','0',null,null,null,null,'26000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB0001', 'IGB00012', TO_DATE('10-01-2021','DD-MM-YYYY'), 24000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0002', 'Chateau du Pere', 'Chateau du Pere', null,'TPD00001','1','0',null,null,null,null,'26000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB0002', 'IGB0001', TO_DATE('10-01-2021','DD-MM-YYYY'), 24000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0003', 'Corbieres', 'Corbieres', null,'TPD00001','1','0',null,null,null,null,'42000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB0003', 'IGB0002', TO_DATE('10-01-2021','DD-MM-YYYY'), 38000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0004', 'Croix Saint Salvy Vin rouge', 'Croix Saint Salvy Vin rouge', null,'TPD00001','1','0',null,null,null,null,'42000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB0004', 'IGB0003', TO_DATE('10-01-2021','DD-MM-YYYY'), 40000);
