ALTER TABLE ALLOSAKAFO.AS_INGREDIENTS
ADD (niveau VARCHAR2(50 BYTE));

CREATE OR REPLACE FORCE VIEW ALLOSAKAFO.AS_RECETTECOMPOSE
(
   ID,
   IDPRODUITS,
   IDINGREDIENTS,
   QUANTITE,
   UNITE,
   COMPOSE,
   niveau
)
AS
   SELECT r."ID",
          r."IDPRODUITS",
          r."IDINGREDIENTS",
          r."QUANTITE",
          r."UNITE",
          ing.COMPOSE,
          ing.niveau
     FROM as_recette r, as_ingredients ing
    WHERE r.IDINGREDIENTS = ing.id;