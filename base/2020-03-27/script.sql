
create or replace view mvtCaisseLettreComandeMere as
select m.ID, m.daty, m.designation, m.debit, m.credit, m.iddevise,
          m.idmode, m.idcaisse, m.remarque,
          m.tiers AS tiers, dcmd.IDMERE as numpiece, m.numcheque, m.typemvt,
          m.datyvaleur, m.idordre,m.etat,m.agence,m.IDMVTOR,m.ETABLISSEMENT
from mvtCaisse m, as_detailscommande dcmd where m.IDORDRE=dcmd.ID;

create or replace view mvtCaisseLettreCmdMereGroup as
select max(m.ID) as id, max(m.daty) as daty, '-' as designation, sum(m.debit) as debit, sum(m.credit) as credit, max(m.iddevise) as iddevise,
          max(m.idmode) as idmode, max(c.DESCCAISSE) as idcaisse, max(m.remarque) as remarque,
          max(m.tiers) AS tiers, numpiece, max(m.numcheque) as numcheque, max(m.typemvt) as typemvt,
          max(m.datyvaleur) as datyvaleur, '-' as idordre,max (m.etat) as etat, max(m.agence) as agence,
          max(m.IDMVTOR) as idmvtor, max(m.ETABLISSEMENT) as etablissement
from mvtCaisseLettreComandeMere m, caisse c where m.IDCAISSE=c.IDCAISSE group by numpiece;


CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, VENTE, ETAT, SECT, 
 RESTAURANT, PRENOM)
AS 
select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, tbl.TELEPHONE as responsable, tbl.nom as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect, cmd.point , 
    mvt.idcaisse
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    as_client tbl,
    vente,mvtCaisseLettreCmdMereGroup mvt
    
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur
    and cmd.client = tbl.id
    and cmd.vente=vente.id(+)
    and cmd.id=mvt.NUMPIECE(+);






CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE2
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, ETAT, COURSIER, MONTANT, 
 SECTEUR, VENTE, RESTAURANT, PRENOM)
AS 
select 
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE", mcmd.ADRESSE as "ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",cmd."ETAT",  '-', mtnt.montant, sect.val as secteur ,cmd.vente,cmd.restaurant,cmd.prenom
from
    as_commandeclient_libelle cmd,
    montantparcommande mtnt,
    as_secteur sect,
    commandelivraisonMax mcmd
where
    mtnt.id = cmd.id and sect.id(+)=cmd.sect
    and cmd.ID=mcmd.idclient(+)
/




create or replace view as_commandeClientLibLivraison as
select * from as_commandeclient_libelle2 cmd where cmd.ADRESSELIV is not null;


create or replace view as_commandeClientLibTable
 as select cmd.* from as_commandeclient_libelle2 cmd 
 where  cmd.ADRESSELIV is null or trim(cmd.ADRESSELIV)='';