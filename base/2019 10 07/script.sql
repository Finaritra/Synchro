create table as_reservation(
	id varchar2(100) primary key,
	source blob,
	nbPersonne int,
	dateSaisie date,
	heureSaisie varchar(50),
	dateReservation date,
	heureDebutReservation varchar(50),
	heureFinReservation varchar(50)
);

create sequence seq_as_reservation minvalue 1 increment by 1;

create function get_seq_as_reservation return number is retour number;
	begin
		select seq_as_reservation.nextval into retour from dual;
		return retour;
	end;

create view vue_commande_client as
	select 
	  cmd.id, cmd.datesaisie, cmd.datecommande, cmd.numcommande, cmd.responsable, cmd.client, cmd.typecommande,
	  cmd.dateliv, cmd.adresseliv, cmd.heureliv, cmd.distance, cmd.remarque, cmd.etat, cmd.secteur,
	  cli.prenom as observation, cmd.quartier, cmd.vente, cmd.point
	from
	  as_commandeclient cmd
	  join as_client cli on cli.id=cmd.client;
	  

update direction set libelledir='PHO1', descdir='Ampandrana' where iddir='DIR000006';
update direction set libelledir='PHO2', descdir='Ankorondrano' where iddir='DIR000008';

create view restaurant as 
  select * from direction where libelledir like 'PHO%'