
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0012', 'Mille feuilles', 'Mille Feuilles', null,'TPD00001','1','0',null,null,null,null,'9000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB0012', 'PRDTBOI0012', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0018', 'Mille feuilles', 'UNT00005', 1, 0, 'CI00001');

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0013', 'Gateau citron vanille', 'Gateau citron vanille', null,'TPD00001','1','0',null,null,null,null,'9000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB0013', 'PRDTBOI0013', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0017', 'Gateau citron vanille', 'UNT00005', 1, 0, 'CI00001');

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0014', 'Gateau mangue passion coco', 'Gateau mangue passion coco', null,'TPD00001','1','0',null,null,null,null,'9000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB0014', 'PRDTBOI0014', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0018', 'Gateau mangue passion coco', 'UNT00005', 1, 0, 'CI00001');