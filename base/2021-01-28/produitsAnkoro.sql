
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00025', 'Salade de fruit et glace', 'Salade de fruit et glace', null,'TPD00001','1','0',null,null,null,null,'7000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00025', 'PRDTBOI00025', TO_DATE('10-01-2021','DD-MM-YYYY'), 5000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI00026', 'Crudité', 'Crudité', null,'TPD00001','1','0',null,null,null,null,'5000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00026', 'PRDTBOI00026', TO_DATE('10-01-2021','DD-MM-YYYY'), 3000);