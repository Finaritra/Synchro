CREATE OR REPLACE VIEW AS_ETATSTOCKVIDE
(INGREDIENTS, UNITE, ENTREE, SORTIE, RESTE, 
 REPORT, DATY, IDCATEGORIEINGREDIENT, POINT, BESOIN, 
 IDFOURNISSEUR, PU, IDINGREDIENT, FOURNISSEUR,MONTANT,montantStock)
AS 
select 
'-' as ingredients,
'-' as unite,
CAST(0 as NUMBER(10,2)) as entree,
CAST(0 as NUMBER(10,2)) as sortie,
CAST(0 as NUMBER(10,2)) as reste,
CAST(0 as NUMBER(10,2)) as report,
'' as daty,
'' as idcategorieingredient,
'-' as point,
CAST(0 as NUMBER(10,2)) as besoin,
'-' as idfournisseur,
CAST(0 as NUMBER(10,2)) as pu,
CAST(0 as NUMBER(10,2)) as montant,
CAST(0 as NUMBER(10,2)) as montantStock,
'-' as idingredient,
'-' as fournisseur
 from dual
/

INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM0000000986', 'Saisie Besoin multiple','fa fa-plus', '/phobo/pages/module.jsp?but=stock/besoin/saisie-besoin-multiple.jsp', 5, 2, 'ASM00014');
INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM0000000987', 'Modif Besoin multiple','fa fa-list', '/phobo/pages/module.jsp?but=stock/besoin/modif-besoin-multiple.jsp', 6, 2, 'ASM00014');
commit;


