create or replace view as_recetteproduit as 
select * from as_recette where idproduits in (select id from as_produits);

CREATE OR REPLACE VIEW "BESOINETRECETTE" ("IDINGREDIENTS", "BESOIN") AS 
  select 
    rec.idingredients,
    sum(rec.quantite * be.quantite)as besoin
    from as_recetteproduit rec 
   left join besoin be on be.idproduit=rec.idproduits
    group by rec.idingredients;