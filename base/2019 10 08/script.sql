CREATE OR REPLACE FORCE VIEW "SAKAFO"."AS_COMMANDECLIENT_LIBELLE" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "VENTE", "ETAT", "SECT", "RESTAURANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM as responsable, tbl.val as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect, cmd.point 
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    tableclientvue2 tbl,
    vente 
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur 
    and cmd.client = tbl.id
    and cmd.vente=vente.id(+);
	
CREATE OR REPLACE FORCE VIEW "SAKAFO"."AS_COMMANDECLIENT_LIBELLE2" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR", "VENTE", "RESTAURANT") AS 
  select distinct
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE",cmd."ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",cmd."ETAT",  liv.responsable, mtnt.montant, sect.val as secteur ,cmd.vente,cmd.restaurant
from
    as_commandeclient_libelle cmd,
    (select cmd.id as commande, liv.id, veh.nom as responsable 
    from as_commande_livre cmd, as_livraison liv, as_vehicule veh 
    where   liv.commande like '%'||cmd.id||'%' and veh.id(+)=liv.cuisinier) liv,
    montantparcommande mtnt,
	as_secteur sect
where
    mtnt.id = cmd.id and liv.commande(+)=cmd.id and sect.id(+)=cmd.sect;
	
create view as_paiement_libelle_restaurant as
	select
		pmt.id, pmt.daty, pmt.montant, pmt.modepaiement, pmt.idobjet, pmt.observation, pmt.tva, pmt.client, pmt.numpiece, pmt.caisse,
		cmd.point as restaurant
	from
		as_paiement_libelle_table pmt
		join as_commandeclient cmd on cmd.id = pmt.idobjet;

CREATE OR REPLACE FORCE VIEW "SAKAFO"."LIVRAISONDETAIL" ("ID", "NOMTABLE", "PRODUIT", "RESTAURANT", "SAUCEACOOMPAGNEMENT", "DATECOMMANDE", "HEURECOMMANDE", "DATELIVRAISON", "HEURELIVRAISON", "DIFFERENCE") AS 
  select 
	cmdcl.id , tcv.val as NomTable , pro.nom as Produit, cmdcl.point, ac.idsauce as SauceAcoompagnement , cmdcl.DATECOMMANDE as DateCommande,
	cmdcl.HEURELIV as HeureCommande , liv.DATY as DateLivraison, liv.HEURE as HeureLivraison,
	cast( ( 60 *( to_number( substr(liv.HEURE,1,2) ) - to_number( substr(cmdcl.HEURELIV,1,2) ) ) ) + (  to_number( substr(liv.HEURE,4,2) ) - to_number( substr(cmdcl.HEURELIV,4,2) ) )  as number(20,2) )  as difference 
  from 
	AS_LIVRAISON liv join AS_DETAILSCOMMANDE cmdcld  on liv.COMMANDE = cmdcld.id 
	join  AS_COMMANDECLIENT cmdcl on cmdcl.id = cmdcld.IDMERE
	join TABLECLIENTVUE2 tcv on cmdcl.client = tcv.id
	join AS_PRODUITS pro on cmdcld.PRODUIT = pro.id   
	left join AS_ACCOMPAGNEMENT_SAUCE_LIB ac on cmdcld.IDACCOMPAGNEMENTSAUCE  = ac.id;
	
create view vue_cmd_client_details_rest as
	select
        det.id ,
        tab.val as nomtable,
        cmd.point as restaurant,
        tp.val as typeproduit,cmd.datecommande,cmd.heureliv,
        prod.nom as produit,
        det.quantite,det.pu,
        (det.quantite*det.pu) as montant,
        det.etat,
        accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
      from as_commandeclient cmd
      join as_detailscommande det on det.idmere = cmd.id
      join as_produits prod on prod.id=det.produit
      join as_typeproduit tp on tp.id=prod.typeproduit
      join tableclientvue2 tab on tab.id=cmd.client
      left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
      order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;