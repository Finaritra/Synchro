 CREATE OR REPLACE  VIEW "COMMANDELIVRAISONLIB" ("ID", "IDCLIENT", "ADRESSE", "IDPOINT", "DATY", "HEURE", "FRAIS", "ETAT") AS 
  select
            cmdliv.id,
            cmd.ID as idclient,
            cmdliv.adresse,
            pt.VAL as idpoint,
            cmdliv.daty,
           cmdliv.heure,
           cmdliv.frais,
           cmdliv.etat
        from commandeLivraison cmdliv
        join AS_COMMANDECLIENT cmd on cmd.id =cmdliv.idclient
        left join POINT pt on pt.id=cmdliv.idPoint;

insert into caisse values('CE000604','Airtel Money','3','etaCs1');
insert into caisse values('CE000605','Orange Money','3','etaCs1');