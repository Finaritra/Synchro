CREATE TABLE Besoinquotidien (
    ID        VARCHAR2(100) constraint besoinqoutidien_pk primary key ,
    IDPRODUIT VARCHAR2(100),
    REMARQUE  VARCHAR2(500),
    QUANTITE  NUMBER(10, 2)
)
/

CREATE SEQUENCE seqBesoinquotidien
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION GETseqBesoinquotidien
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqBesoinquotidien.nextval INTO retour FROM dual;
  return retour;
END;
--
CREATE TABLE Besoinhebdo(
    ID        VARCHAR2(100) constraint Besoinhebdo_pk primary key ,
    IDPRODUIT VARCHAR2(100),
    REMARQUE  VARCHAR2(500),
    QUANTITE  NUMBER(10, 2)
)
/

CREATE SEQUENCE seqBesoinhebdo
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION GETseqBesoinhebdo
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqBesoinhebdo.nextval INTO retour FROM dual;
  return retour;
END;
    --

create view Besoinquotidienlib as
select
    bq.ID,
    bq.idproduit,
    bq.remarque,
    bq.quantite,
    p.nom as libelleproduit
from Besoinquotidien bq,
    as_produits p where bq.IDPRODUIT=p.id;

create view Besoinhebdolib as
select
    bh.ID,
    bh.idproduit,
    bh.remarque,
    bh.quantite,
    p.nom as libelleproduit
from Besoinhebdo bh,
    as_produits p where bh.IDPRODUIT=p.id;