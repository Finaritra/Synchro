

update MENUDYNAMIQUE set LIBELLE='Besoin' where id = 'ASM00016';

INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM0001700','Besoin hebdo','fa fa-list-alt',null,7, 2,'ASM00014');
INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM0001701','Liste Update Multiple','fa fa-list','/phobo/pages/module.jsp?but=stock/besoin/uptade-multiple-besoin-hebdo.jsp',1, 3,'ASM0001700');
INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM0001702','Saisie','fa fa-plus','/phobo/pages/module.jsp?but=stock/besoin/saisie-besoin-hebdo.jsp',2, 3,'ASM0001700');

INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM0001703','Besoin Quotidien','fa fa-list-alt',null,8, 2,'ASM00014');
INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM0001704','Liste Update Multiple','fa fa-list','/phobo/pages/module.jsp?but=stock/besoin/uptade-multiple-besoin-quotidien.jsp',1, 3,'ASM0001703');
INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM0001705','Saisie','fa fa-plus','/phobo/pages/module.jsp?but=stock/besoin/saisie-besoin-quotidien.jsp',2, 3,'ASM0001703');

INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM000080230','Analyse BL Fille','fa fa-line-chart', '/phobo/pages/module.jsp?but=stock/bl/analyse-bondelivraisonfille.jsp',3, 3,'ASM0000802');
INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM000080231','Liste BL Fille','fa fa-list','/phobo/pages/module.jsp?but=stock/bl/asBondeLivraisonFille-liste.jsp',4, 3,'ASM0000802');



update MENUDYNAMIQUE set HREF='/phobo/pages/module.jsp?but=stock/besoin/saisie-besoin-quotidien.jsp'||'&'||'ciblename=quotidien' where id = 'ASM0001705';
update MENUDYNAMIQUE set HREF='/phobo/pages/module.jsp?but=stock/besoin/saisie-besoin-quotidien.jsp'||'&'||'ciblename=hebdo' where id = 'ASM0001702';

update MENUDYNAMIQUE set HREF='/phobo/pages/module.jsp?but=stock/besoin/modif-besoin-multiple-quotidien.jsp'||'&'||'cible=besoinquotidienlib' where id = 'ASM0001704';
update MENUDYNAMIQUE set HREF='/phobo/pages/module.jsp?but=stock/besoin/modif-besoin-multiple-quotidien.jsp'||'&'||'cible=besoinhebdolib' where id = 'ASM0001701';
commit;