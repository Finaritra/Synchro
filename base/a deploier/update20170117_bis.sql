create table as_secteur (
	ID VARCHAR(50) not null,
	VAL VARCHAR(100) not null,
	DESCE VARCHAR(250) not null,
	constraint PK_ASSECTEUR primary key (ID)
);

CREATE SEQUENCE  seqassecteur  MINVALUE 1 INCREMENT BY 1  ;

create or replace 
FUNCTION         getSeqASsecteur RETURN NUMBER IS retour NUMBER;
BEGIN
SELECT seqassecteur.nextval INTO retour FROM dual;
return retour;
END; 

alter table as_commandeclient add secteur varchar(50);

CREATE OR REPLACE FORCE VIEW "AS_COMMANDECLIENT_LIBELLE" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "SECTEUR") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.client, tp.VAL,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, sct.val as sect
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur ;
 

CREATE OR REPLACE FORCE VIEW "AS_COMMANDECLIENT_LIBANNULE" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant, cmd.secteur
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id 
     and liv.commande (+)=cmd.id
    and cmd.etat =0;
	
	
CREATE OR REPLACE FORCE VIEW "AS_COMMANDECLIENT_LIBENCOURS" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT,
    liv.responsable, mtnt.montant, cmd.secteur
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id 
    and liv.commande (+)=cmd.id
    and cmd.etat =1;
	
CREATE OR REPLACE FORCE VIEW "AS_COMMANDECLIENT_LIBFAIT" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant, cmd.secteur
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and liv.commande (+)=cmd.id
    and mtnt.id = cmd.id
    and cmd.etat < 20
    and cmd.etat >= 5;
	
CREATE OR REPLACE FORCE VIEW "AS_COMMANDECLIENT_LIVNP" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.client,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant, cmd.secteur
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id
     and liv.commande (+)=cmd.id
    and cmd.etat = 20;
	
CREATE OR REPLACE FORCE VIEW "AS_COMMANDECLIENT_LIVP" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM,
    cmd.client, tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant, cmd.secteur
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id 
     and liv.commande (+)=cmd.id
    and cmd.etat = 40;
	
CREATE OR REPLACE FORCE VIEW "AS_COMMANDECLIENT_LIBELLE2" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE",cmd."ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",cmd."ETAT",  liv.responsable, mtnt.montant, cmd.secteur
from
    as_commandeclient_libelle cmd,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    liv.commande (+)=cmd.id and
    mtnt.id = cmd.id;
	
insert into as_secteur values ('SCT000001', 'Centre', 'Centre');
insert into as_secteur values ('SCT000002', 'Sud loin', 'Sud');
insert into as_secteur values ('SCT000003', 'Sud moyen', 'Sud');
insert into as_secteur values ('SCT000004', 'Axe Nanisana', 'Axe Nanisana');
insert into as_secteur values ('SCT000005', 'Axe Analamahitsy', 'Axe Analamahitsy');
insert into as_secteur values ('SCT000006', 'Axe Analamahitsy loin', 'Axe Analamahitsy');
insert into as_secteur values ('SCT000007', 'Axe Ivato moyen', 'Axe ivato');
insert into as_secteur values ('SCT000008', 'Axe Ivato loin', 'Axe ivato');
insert into as_secteur values ('SCT000009', 'Axe Ankorondrano', 'Axe Ankorondrano');
