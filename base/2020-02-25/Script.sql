CREATE OR REPLACE VIEW annulationmvtlib (id,idobjet,daty,motif,montant,etat) as
    select mvt.ID,
           mvtc.DESIGNATION as IDOBJET,
           mvt.DATY,
           mvt.MOTIF,
           mvt.MONTANT,
           mvt.ETAT
from ANNULATIONMVT mvt,
     mvtcaisse mvtc where mvt.IDOBJET=mvtc.ID;
	 
CREATE OR REPLACE VIEW annulationoplib (id,idobjet,daty,motif,montant,etat) as
    select op.ID,
           mvtc.DESIGNATION as IDOBJET,
           op.DATY,
           op.MOTIF,
           op.MONTANT,
           op.ETAT
from Annulationop op,
     mvtcaisse mvtc where op.IDOBJET=mvtc.ID;


CREATE OR REPLACE VIEW reservationlibvalide 
AS
   SELECT   * from RESERVATIONLIBTOUS where etat = 11;

CREATE OR REPLACE VIEW reservationlibcree 
AS
   SELECT   * from RESERVATIONLIBTOUS where etat = 1;