ALTER TABLE commandeLivraison DROP CONSTRAINT commandeLiv_cli ;

CREATE OR REPLACE FORCE VIEW "VUE_CMD_CLT_DETAIL_TYPEP_REST" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "RESTAURANT", "IDCLIENT", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  select
det.id,
tab.nom as nomtable,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,cmd.CLIENT as idclient,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,
det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join as_client tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;

 CREATE OR REPLACE VIEW "DETAILSCOMMANDECLIENT" ("ID", "IDMERE", "PRODUIT", "QUANTITE", "PU", "REMISE", "OBSERVATION", "ETAT", "IDACCOMPAGNEMENTSAUCE", "PRIORITER", "IDCLIENT") AS 
  select
    det."ID",det."IDMERE",det."PRODUIT",det."QUANTITE",det."PU",det."REMISE",det."OBSERVATION",det."ETAT",det."IDACCOMPAGNEMENTSAUCE",det."PRIORITER",
    clt.id as idclient
    from AS_DETAILSCOMMANDE det
    join AS_COMMANDECLIENT cml on cml.id=det.IDMERE
    join AS_CLIENT clt on clt.id=cml.CLIENT;

 CREATE OR REPLACE VIEW "VUE_CMD_ADDITION_LIVRAISON" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "RESTAURANT", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  select
det.id,
cmd.client as nomtable,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,
det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    where det.etat>=1 and det.etat<40
    order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;
 
