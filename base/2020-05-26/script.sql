alter table AS_BONDELIVRAISON_FILLE add iddetailsfacturefournisseur VARCHAR2(100) constraint detailsbondelivraison_fk references detailsfacturefournisseur;
create or replace view  as_bondelivraison_fille_date as
select blf.* , bl.daty from as_bondelivraison_fille_lib blf join as_bondelivraison bl on  blf.numbl = bl.id ;

create or replace view Detailsffbonlivraison as
select
    Detfac.id,
    Detfac.QTE,
    Detfac.pu,
    Detfac.IDMERE,
    Detfac.COMPTE,
       Detfac.IDINGREDIENT,
    NVL((select sum(bonF.quantite) from AS_BONDELIVRAISON_fille bonF join as_bondelivraison bon  on bon.id=bonF.NUMBL where etat=11 and bonF.iddetailsfacturefournisseur=Detfac.id group by iddetailsfacturefournisseur),0) as livre,
    QTE- NVL((select sum(bonF.quantite)from AS_BONDELIVRAISON_fille bonF join as_bondelivraison bon  on bon.id=bonF.NUMBL where etat=11 and bonF.iddetailsfacturefournisseur=Detfac.id group by iddetailsfacturefournisseur),0) as reste
    from detailsfacturefournisseur Detfac;

create or replace view Detailsffbonlivraisonlib as
    select
    dl.id,
    dl.QTE,
    dl.pu,
    dl.IDMERE,
    dl.COMPTE,
    i.LIBELLE as IDINGREDIENT,
    dl.livre,
    dl.reste
    from
        Detailsffbonlivraison dl
    join AS_INGREDIENTS i on i.id=dl.IDINGREDIENT;