create or replace 
FUNCTION "GETSEQASLIVRAISON" RETURN NUMBER IS retour NUMBER;
BEGIN
SELECT SEQ_ASLIVRAISON.nextval INTO retour FROM dual;
return retour;
END; 


create view as_livraison_complet as 
select distinct cmd_id as commande, id, daty, heure, adresse, secteur, distance, responsable, cuisinier, etat, carburant  from as_livraison , (
select  regexp_substr(commande,'[^;]+', 1, level) as cmd_id, id as liv_id from as_livraison
   connect by regexp_substr(commande, '[^;]+', 1, level) is not null) liv_fait where id = liv_id and cmd_id is not null;
   
CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBANNULE" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id 
     and liv.commande (+)=cmd.id
    and cmd.etat =0;
	
	
CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBENCOURS" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT,
    liv.responsable, mtnt.montant
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id 
    and liv.commande (+)=cmd.id
    and cmd.etat =1;
	
CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBFAIT" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and liv.commande (+)=cmd.id
    and mtnt.id = cmd.id
    and cmd.etat < 20
    and cmd.etat >= 5;
	
CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIVNP" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.client,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id
     and liv.commande (+)=cmd.id
    and cmd.etat = 20;
	
CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIVP" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM,
    cmd.client, tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id 
     and liv.commande (+)=cmd.id
    and cmd.etat = 40;
	
CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBELLE2" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE",cmd."ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",cmd."ETAT",  liv.responsable, mtnt.montant
from
    as_commandeclient_libelle cmd,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    liv.commande (+)=cmd.id and
    mtnt.id = cmd.id;


   
