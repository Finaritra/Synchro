CREATE OR REPLACE VIEW OPPAYE
(ID, APAYER, PAYER, RESTE)
AS 
select id, apayer, payer, reste from(
        select 
            op.id as id, op.MONTANT as apayer, 
            sum(mc.debit) as payer, (op.MONTANT - sum(mc.debit)) as reste
        from
            ordonnerpayement op, mvtcaisse mc
        where
            op.ID = mc.IDORDRE
        group by
            op.id, op.montant
    )
where reste = 0
/
INSERT INTO MENUDYNAMIQUE VALUES ('ASM00006023', 'Situation OP', 'fa fa-list-alt', '/allosakafo-war/pages/module.jsp?but=ded/situationop-liste.jsp', 3, 3, 'ASM0000602');

CREATE OR REPLACE VIEW SITUATION_OP
(ID, REMARQUE, DATY, APAYER, PAYER, 
 RESTE)
AS 
select id, remarque, daty, cast(apayer as number(20,2)), cast(payer  as number(20,2)), cast(reste  as number(20,2)) from(
        select 
            op.id as id,op.remarque,op.daty, op.MONTANT as apayer, 
           nvl(sum(mc.debit),0) as payer, nvl((op.MONTANT - sum(mc.debit)), op.MONTANT) as reste
        from
            ordonnerpayement op
        left join mvtcaisse mc on op.ID = mc.IDORDRE
        group by
            op.id, op.montant, op.remarque, op.daty
    )
  order by daty desc
/

create or replace view as_produit_prix as 
select as_produits.id, nom, designation, photo, typeproduit, calorie, poids, pa, dateapplication, montant, observation from as_produits join as_prixproduit on as_produits.id = as_prixproduit.PRODUIT;

update menudynamique set href='/allosakafo-war/pages/module.jsp?but=produits/as-produitsprix-saisie.jsp' where id='ASM0000401';


