  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."SITUATION_OP" ("ID", "REMARQUE", "DATY", "APAYER", "PAYER", "RESTE") AS 
  select id, remarque, daty, cast(apayer as number(20,2)), cast(payer  as number(20,2)), cast(reste  as number(20,2)) from(
        select 
            op.id as id,op.remarque,op.daty, op.MONTANT as apayer, 
           nvl(sum(mc.debit),0) as payer, nvl((op.MONTANT - sum(mc.debit)), op.MONTANT) as reste
        from
            ordonnerpayement op
        left join mvtcaisse mc on op.ID = mc.IDORDRE
		where op.etat=11
        group by
            op.id, op.montant, op.remarque, op.daty
    )
  order by daty desc;