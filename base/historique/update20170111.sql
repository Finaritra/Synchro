ALTER TABLE facturefournisseur RENAME COLUMN idfacturefournisseur to id;

 CREATE OR REPLACE FORCE VIEW "VVLIGNE"."FACTUREFOURNISSEURLETTRE" ("ID", "DATY", "IDFOURNISSEUR", "IDTVA", "MONTANTTTC", "REMARQUE", "DATEEMISSION", "DESIGNATION", "IDDEVISE", "NUMFACT", "DATYECHEANCE", "ETAT") AS 
  SELECT ff.id, ff.daty, c.nom as idFournisseur, ff.idtva, ff.montantttc,ff.remarque, 
  ff.dateemission, ff.designation, ff.iddevise, ff.numfact, ff.datyecheance, ff.etat
FROM facturefournisseur ff,client c where ff.IDFOURNISSEUR=c.IDCLIENT;

Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM00006', 'D�pense', 'fa fa-home', '', 6, 1, null);
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM0000601', 'Facture fournisseur', 'fa fa-home', '', 1, 2, 'ASM00006');
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM00006011', 'Cr�ation', 'fa fa-plus', '/allosakafo-war/pages/module.jsp?but=facturefournisseur/facturefournisseur-saisie.jsp', 1, 3, 'ASM0000601');
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM00006012', 'Liste', 'fa fa-list', '/allosakafo-war/pages/module.jsp?but=facturefournisseur/facturefournisseur-liste.jsp', 2, 3, 'ASM0000601');
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM0000602', 'Ordre de paiement', 'fa fa-home', '', 2, 2, 'ASM00006');
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM00006021', 'Cr�ation', 'fa fa-plus', '/allosakafo-war/pages/module.jsp?but=ded/ordonnerpayement-saisie.jsp', 1, 3, 'ASM0000602');
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM00006022', 'Liste', 'fa fa-list', '/allosakafo-war/pages/module.jsp?but=ded/ordonnerpayement-liste.jsp', 2, 3, 'ASM0000602');

Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM00007', 'Caisse', 'fa fa-home', '', 7, 1, null);
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM0000701', 'Entr�e', 'fa fa-plus', '/allosakafo-war/pages/module.jsp?but=fin/entreecaisse-saisie.jsp', 1, 2, 'ASM00007');
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM0000702', 'Sortie', 'fa fa-plus', '/allosakafo-war/pages/module.jsp?but=fin/sortiecaisse-saisie.jsp', 2, 2, 'ASM00007');
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM0000703', 'Liste', 'fa fa-list', '/allosakafo-war/pages/module.jsp?but=fin/mvtcaisse-liste.jsp', 3, 2, 'ASM00007');
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM0000704', 'Etat de caisse', 'fa fa-list', '/allosakafo-war/pages/module.jsp?but=fin/etatcaisse-liste.jsp', 4, 2, 'ASM00007');
Insert into MENUDYNAMIQUE (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE)	Values ('ASM0000705', 'Report', 'fa fa-plus', '/allosakafo-war/pages/module.jsp?but=ded/report-saisie.jsp', 5, 2, 'ASM00007');

ALTER TABLE mvtcaisse add etat integer default 1;

CREATE SEQUENCE  "VVLIGNE"."SEQMVTCAISSE"  MINVALUE 1 INCREMENT BY 1  ;

create or replace 
FUNCTION         getSeqMvtcaisse RETURN NUMBER IS retour NUMBER;
BEGIN
SELECT seqMvtcaisse.nextval INTO retour FROM dual;
return retour;
END; 

CREATE OR REPLACE FORCE VIEW "VVLIGNE"."MVTCAISSELETTRE" ("ID", "DATY", "DESIGNATION", "DEBIT", "CREDIT", "IDDEVISE", 
"IDMODE", "IDCAISSE", "REMARQUE",  "TIERS", "NUMPIECE", "NUMCHEQUE", "TYPEMVT", "DATYVALEUR", "IDORDRE") AS 
  select m.ID, m.daty, m.designation, m.debit, m.credit, m.iddevise, mp.VAL as idmode,
       c.DESCCAISSE as idcaisse, m.remarque, m.tiers as tiers, m.numpiece, m.numcheque,t.VAL as typemvt ,
       m.datyvaleur, m.idordre from mvtCaisse m,typeMvt t,caisse c,modePaiement mp where 
        m.TYPEMVT=t.ID  and m.IDCAISSE=c.IDCAISSE and m.IDMODE=mp.id ;
		