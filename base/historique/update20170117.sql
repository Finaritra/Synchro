create table as_clientdouteux (
	ID VARCHAR(50) not null,
	IDCLIENT VARCHAR(100) not null,
	REMARQUE VARCHAR(250) not null,
	constraint PK_CLIENTDTX primary key (ID)
);


CREATE SEQUENCE  seqasclientdouteux  MINVALUE 1 INCREMENT BY 1  ;

create or replace 
FUNCTION         getSeqASclientdouteux RETURN NUMBER IS retour NUMBER;
BEGIN
SELECT seqasclientdouteux.nextval INTO retour FROM dual;
return retour;
END; 

create or replace view as_clientdouteux_libelle as 
select distinct * from as_client where id in (select idclient from as_clientdouteux);	

create or replace view as_clientnondouteux_libelle as 
select distinct * from as_client where id not in (select idclient from as_clientdouteux) ;

