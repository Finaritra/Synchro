insert into caisse values ('CE000600', 'caisse allo sakafo', 2, 'etaCs1');


  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."ETATCAISSEPARDATE" ("CAISSE", "DEVISE", "DATY", "DEBIT", "CREDIT", "DISPONIBLE", "REPORT") AS 
  select 
    mvt.idcaisse,
    mvt.iddevise,
    mvt.DATY,
    sum(mvt.DEBIT),
    sum(mvt.CREDIT),
    0,
    0
from
MVTCAISSE mvt 
group by mvt.idcaisse,  mvt.iddevise, mvt.DATY
order by mvt.DATY asc;

 

  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_ANALYSECOMMANDE_DETAILS" ("DATECOMMANDE", "DATELIV", "CLIENT", "PRODUIT", "PU", "QUANTITE", "MONTANT", "REMARQUE", "HEURELIV", "NUMCOMMANDE", "ADRESSELIV", "ETAT") AS 
select
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, sum(dets.PU), sum(dets.QUANTITE), 
    sum(dets.PU * dets.QUANTITE), comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat
from
    as_commandeclient comm,
    as_detailscommande dets,
    as_produits prod
where
    dets.IDMERE = comm.ID
    and dets.PRODUIT = prod.ID
group by
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat ;
    
     CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_ANALYSECOMMANDE_LIBANNULE" ("DATECOMMANDE", "DATELIV", "CLIENT", "PRODUIT", "PU", "QUANTITE", "MONTANT", "REMARQUE", "HEURELIV", "NUMCOMMANDE", "ADRESSELIV", "ETAT") AS 
  select
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, sum(dets.PU), sum(dets.QUANTITE), 
    sum(dets.PU * dets.QUANTITE), comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat
    
from
    as_commandeclient comm,
    as_detailscommande dets,
    as_produits prod
where
    dets.IDMERE = comm.ID
    and dets.PRODUIT = prod.ID
	and comm.ETAT = 0
group by
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat;
    
    
  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_ANALYSECOMMANDE_LIBENCOURS" ("DATECOMMANDE", "DATELIV", "CLIENT", "PRODUIT", "PU", "QUANTITE", "MONTANT", "REMARQUE", "HEURELIV", "NUMCOMMANDE", "ADRESSELIV", "ETAT") AS 
  select
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, sum(dets.PU), sum(dets.QUANTITE), 
    sum(dets.PU * dets.QUANTITE), comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat
    
from
    as_commandeclient comm,
    as_detailscommande dets,
    as_produits prod
where
    dets.IDMERE = comm.ID
    and dets.PRODUIT = prod.ID
	and comm.ETAT = 1
group by
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat;
 


  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_ANALYSECOMMANDE_LIBFAIT" ("DATECOMMANDE", "DATELIV", "CLIENT", "PRODUIT", "PU", "QUANTITE", "MONTANT", "REMARQUE", "HEURELIV", "NUMCOMMANDE", "ADRESSELIV", "ETAT") AS 
  select
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, sum(dets.PU), sum(dets.QUANTITE), 
    sum(dets.PU * dets.QUANTITE), comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat
    
from
    as_commandeclient comm,
    as_detailscommande dets,
    as_produits prod
where
    dets.IDMERE = comm.ID
    and dets.PRODUIT = prod.ID
	and comm.ETAT = 5
group by
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV , comm.etat;
    
    
  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_ANALYSECOMMANDE_LIBVNP" ("DATECOMMANDE", "DATELIV", "CLIENT", "PRODUIT", "PU", "QUANTITE", "MONTANT", "REMARQUE", "HEURELIV", "NUMCOMMANDE", "ADRESSELIV", "ETAT") AS 
  select
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, sum(dets.PU), sum(dets.QUANTITE), 
    sum(dets.PU * dets.QUANTITE), comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat
    
from
    as_commandeclient comm,
    as_detailscommande dets,
    as_produits prod
where
    dets.IDMERE = comm.ID
    and dets.PRODUIT = prod.ID
	and comm.ETAT = 20
group by
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat;
 

  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_ANALYSECOMMANDE_LIBVP" ("DATECOMMANDE", "DATELIV", "CLIENT", "PRODUIT", "PU", "QUANTITE", "MONTANT", "REMARQUE", "HEURELIV", "NUMCOMMANDE", "ADRESSELIV", "ETAT") AS 
  select
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, sum(dets.PU), sum(dets.QUANTITE), 
    sum(dets.PU * dets.QUANTITE), comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat
    
from
    as_commandeclient comm,
    as_detailscommande dets,
    as_produits prod
where
    dets.IDMERE = comm.ID
    and dets.PRODUIT = prod.ID
	and comm.ETAT = 40
group by
    comm.DATECOMMANDE, comm.DATELIV, comm.CLIENT, prod.NOM, comm.REMARQUE, comm.HEURELIV, comm.NUMCOMMANDE , comm.ADRESSELIV, comm.etat;
 

create view montantparcommande as 
select idmere as id, sum(quantite*pu - (quantite*pu)*remise/100) as montant from as_detailscommande group by idmere;

CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBELLE2" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE",cmd."ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",cmd."ETAT",  liv.responsable, mtnt.montant
from
    as_commandeclient_libelle cmd,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv,
    montantparcommande mtnt
where
    liv.commande (+)=cmd.id and
    mtnt.id = cmd.id;

 CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBFAIT" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and liv.commande (+)=cmd.id
    and mtnt.id = cmd.id
    and cmd.etat < 20
    and cmd.etat >= 5;
	

   CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBENCOURS" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT,
    liv.responsable, mtnt.montant
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id 
    and liv.commande (+)=cmd.id
    and cmd.etat =1;
 
 
CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIVNP" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.client,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id
     and liv.commande (+)=cmd.id
    and cmd.etat = 20;
 
 
 CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIVP" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM,
    cmd.client, tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id 
     and liv.commande (+)=cmd.id
    and cmd.etat = 40;
 

    CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBANNULE" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id 
     and liv.commande (+)=cmd.id
    and cmd.etat =0;
	
	CREATE OR REPLACE FORCE VIEW "VVLIGNE"."FACTUREFOURNISSEURLETTRE" ("ID", "DATY", "IDFOURNISSEUR", "IDTVA", "MONTANTTTC", "REMARQUE", "DATEEMISSION", "DESIGNATION", "IDDEVISE", "NUMFACT", "DATYECHEANCE", "ETAT") AS 
  SELECT ff.id, ff.daty, c.nom as idFournisseur, ff.idtva, ff.montantttc,ff.remarque, 
  ff.dateemission, ff.designation, ff.iddevise, ff.numfact, ff.datyecheance, ff.etat
FROM facturefournisseur ff,client c where c.IDCLIENT (+)= ff.IDFOURNISSEUR;