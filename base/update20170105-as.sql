CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBANNULE
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, ETAT, COURSIER)
AS 
select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT , tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable
from
    as_commandeclient cmd,
    as_responsable resp, 
    as_typecommande tp,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
	 and liv.commande (+)=cmd.id 
    and cmd.etat =0;
	
CREATE OR REPLACE FORCE VIEW "ALLOSAKAFO"."as_commandeclient_libelle2" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", 
"RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cl.NOM||' '||cl.PRENOM, tp.VAL,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_client cl,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv
where
    cmd.RESPONSABLE = resp.ID
    and cmd.CLIENT = cl.ID
    and liv.commande (+)=cmd.id  
    and cmd.TYPECOMMANDE = tp.ID ;
	
CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBENCOURS
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, ETAT, COURSIER)
AS
select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT , 
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT,
    liv.responsable
from
    as_commandeclient cmd,
    as_responsable resp, 
    as_typecommande tp,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID 
    and liv.commande (+)=cmd.id  
    and cmd.etat < 5;
	
CREATE OR REPLACE FORCE VIEW "ALLOSAKAFO"."AS_COMMANDECLIENT_LIBFAIT" ("ID", "NUMCOMMANDE", "DATESAISIE",
"DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE",
"REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT , 
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and liv.commande (+)=cmd.id 
    and cmd.etat < 20
    and cmd.etat >= 5;
	
CREATE OR REPLACE FORCE VIEW "ALLOSAKAFO"."AS_COMMANDECLIENT_LIVNP" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", 
 "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cl.NOM||' '||cl.PRENOM, 
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_client cl,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv
where
    cmd.RESPONSABLE = resp.ID
    and cmd.CLIENT = cl.ID
    and cmd.TYPECOMMANDE = tp.ID
     and liv.commande (+)=cmd.id 
    and cmd.etat = 20 ;
	
  CREATE OR REPLACE FORCE VIEW "ALLOSAKAFO"."AS_COMMANDECLIENT_LIVP" ("ID", "NUMCOMMANDE", "DATESAISIE", 
  "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, 
    cl.NOM||' '||cl.PRENOM, tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_client cl,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv
where
    cmd.RESPONSABLE = resp.ID
    and cmd.CLIENT = cl.ID
    and cmd.TYPECOMMANDE = tp.ID
     and liv.commande (+)=cmd.id 
    and cmd.etat >= 40 ;
	
 CREATE OR REPLACE FORCE VIEW "ALLOSAKAFO"."AS_COMMANDECLIENT_LIBANNULE" ("ID", "NUMCOMMANDE",
 "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", 
 "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable
from
    as_commandeclient cmd,
    as_responsable resp, 
    as_typecommande tp,
    (select as_livraison.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison join as_responsable res on as_livraison.responsable = res.id) liv
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
     and liv.commande (+)=cmd.id 
    and cmd.etat =0;
 
 CREATE OR REPLACE FORCE VIEW "ALLOSAKAFO"."as_commandeclient_libelle" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", 
"RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cl.NOM||' '||cl.PRENOM, tp.VAL, 
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_client cl
where
    cmd.RESPONSABLE = resp.ID
    and cmd.CLIENT = cl.ID
    and cmd.TYPECOMMANDE = tp.ID ;