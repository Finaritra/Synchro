CREATE OR REPLACE  VIEW AS_ING_PROD_LIBELLE
            (ID, LIBELLE, SEUIL, UNITE, QUANTITEPARPACK, PU, CATEGORIEINGREDIENT,idunite) AS
SELECT as_ingredients.id,
       as_ingredients.libelle,
       as_ingredients.seuil,
       u.val                   AS unite,
       as_ingredients.quantiteparpack,
       as_ingredients.pu,
       CATEGORIEINGREDIENT.val AS CATEGORIEINGREDIENT,
       AS_INGREDIENTS.UNITE as idunite
FROM as_ingredients
         JOIN as_unite u ON as_ingredients.unite = u.id
         JOIN CATEGORIEINGREDIENT
              ON as_ingredients.CATEGORIEINGREDIENT = CATEGORIEINGREDIENT.id
UNION
SELECT p.id,
       p.nom     AS libelle,
       0         as seuil,
       'unite'   as unite,
       0         as quantiteparpack,
       0         as pu,
       'produit' as CATEGORIEINGREDIENT,
       '' as idunite
FROM AS_PRODUITS_LIBELLE p;