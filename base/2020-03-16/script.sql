
create or replace view commandelivraisonliblead as
    select *
    from commandelivraisonlible where adresse='' or adresse is null;


create  or replace view VUE_CMD_DTLS_VALIDE_REST_RESP as
select
        id,
        nomtable,
       typeproduit,
       datecommande,
       heureliv,
       restaurant,
       produit,
       quantite,
       pu,
       montant,
       etat,
       acco_sauce,
       responsable,
       TELEPHONE,
       IDCLIENT
from  VUE_CMD_CLT_DETS_REST_RESP
where etat = 11
order by datecommande asc, heureliv asc;


CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_TOUS
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL)
AS 
select

        vccdrr.id,

       vccdrr.nomtable,

       vccdrr.typeproduit,

       vccdrr.datecommande,

       vccdrr.heureliv,

       vccdrr.restaurant,

      vccdrr.produit,

       vccdrr.quantite,

       vccdrr.pu,

       vccdrr.montant,

      vccdrr.etat,

       vccdrr.acco_sauce,

       vccdrr.prioriter,

       vccdrr.observation,

       vccdrr.idtype,

       vccdrr.responsable,

       '-' as idcl,

       vccdrr.datecommande as datycl,

       vccdrr.telephone,

     '-' as heurecl

from VUE_CMD_CLT_DETS_REST_RESP vccdrr

order by vccdrr.DATECOMMANDE , vccdrr.HEURELIV ASC, vccdrr.prioriter asc;