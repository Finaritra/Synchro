﻿ CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBELLE2" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE",
"ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") as
 select distinct
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE",cmd."ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",cmd."ETAT",  liv.responsable, mtnt.montant, sect.val as secteur 
from
    as_commandeclient_libelle cmd,
    (select cmd.id as commande, liv.id, veh.nom as responsable 
    from as_commande_livre cmd, as_livraison liv, as_vehicule veh 
    where   liv.commande like '%'||cmd.id||'%' and veh.id(+)=liv.cuisinier) liv,
    montantparcommande mtnt,
	as_secteur sect
where
    mtnt.id = cmd.id and liv.commande(+)=cmd.id and sect.id(+)=cmd.secteur;
    
    
    CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBFAIT" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select distinct
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant, sect.val as secteur
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
	(select cmd.id as commande, liv.id, veh.nom as responsable 
    from as_commande_livre cmd, as_livraison liv, as_vehicule veh 
    where   liv.commande like '%'||cmd.id||'%' and veh.id(+)=liv.cuisinier) liv,
    montantparcommande mtnt,
	as_secteur sect
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id
	and liv.commande(+)=cmd.id  
    and cmd.etat < 20
    and cmd.etat >= 5
	 and sect.id(+)=cmd.secteur;
 

  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBENCOURS" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT,
    liv.responsable, mtnt.montant, sect.val as secteur
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
	(select cmd.id as commande, liv.id, veh.nom as responsable 
    from as_commande_livre cmd, as_livraison liv, as_vehicule veh 
    where   liv.commande like '%'||cmd.id||'%' and veh.id(+)=liv.cuisinier) liv,
    montantparcommande mtnt,
	as_secteur sect
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
	and liv.commande(+)=cmd.id  
    and mtnt.id = cmd.id
    and cmd.etat =1
	 and sect.id(+)=cmd.secteur;
 

  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIVNP" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select distinct
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.client,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant, sect.val as secteur 
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
	(select cmd.id as commande, liv.id, veh.nom as responsable 
    from as_commande_livre cmd, as_livraison liv, as_vehicule veh 
    where   liv.commande like '%'||cmd.id||'%' and veh.id(+)=liv.cuisinier) liv,
    montantparcommande mtnt,
	as_secteur sect
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
	and liv.commande(+)=cmd.id  
    and mtnt.id = cmd.id
    and cmd.etat = 20
	 and sect.id(+)=cmd.secteur;
 

  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIVP" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM,
    cmd.client, tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant, sect.val as secteur 
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
	(select cmd.id as commande, liv.id, veh.nom as responsable 
    from as_commande_livre cmd, as_livraison liv, as_vehicule veh 
    where   liv.commande like '%'||cmd.id||'%' and veh.id(+)=liv.cuisinier) liv,
    montantparcommande mtnt,
	as_secteur sect
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
	and liv.commande(+)=cmd.id  
    and mtnt.id = cmd.id
    and cmd.etat = 40
	 and sect.id(+)=cmd.secteur;
 

  CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_COMMANDECLIENT_LIBANNULE" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT,liv.responsable, mtnt.montant, sect.val as secteur 
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
	(select cmd.id as commande, liv.id, veh.nom as responsable 
    from as_commande_livre cmd, as_livraison liv, as_vehicule veh 
    where   liv.commande like '%'||cmd.id||'%' and veh.id(+)=liv.cuisinier) liv,
    montantparcommande mtnt,
	as_secteur sect
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
	and liv.commande(+)=cmd.id  
    and mtnt.id = cmd.id
    and cmd.etat =0
	 and sect.id(+)=cmd.secteur;


CREATE OR REPLACE FORCE VIEW "VVLIGNE"."AS_LIVRAISON_PRIXLIBELLE" ("ID", "QUARTIER", "OBSERVATION", "DATEEFFECTIVITE", "MONTANT") AS 
  select
    prd.id, prd.nom, prd.secteur, (select current_date from dual),  sum(trf.montant) keep (dense_rank first order by trf.dateeffectivite desc)
from
    as_quartier_libelle prd,
    as_tariflivraison trf
where
    trf.quartier (+)= prd.id
group by prd.id, prd.nom, prd.secteur;





 