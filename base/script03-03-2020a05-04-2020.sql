--2020-03-03
create or replace view VUE_CMD_CLT_DETS_REST_RESP as
select
det.id,
tab.nom as nomtable,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
det.prioriter,
det.observation,
tp.id as idtype,
resp.NOM as responsable
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
join AS_RESPONSABLE resp on resp.id=cmd.RESPONSABLE
order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;

create view Vue_cmd_dtls_clot_rest_resp as
select
id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
prioriter,
observation,
idtype,
responsable
from VUE_CMD_CLT_DETS_REST_RESP
where etat = 9 and idtype != 'TPD00001' and produit != 'Barquette'
order by DATECOMMANDE , HEURELIV ASC,prioriter asc;

create view Vue_cmd_dtls_valide_rest_resp as
select
id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
responsable
from  VUE_CMD_CLT_DETS_REST_RESP
where etat = 11
order by datecommande asc, heureliv asc;

create view Vue_cmd_dtls_livre_rest_resp as
select
id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
responsable
from  VUE_CMD_CLT_DETS_REST_RESP
where etat > 0 and etat <= 20;

	
create view Vue_cmd_dtls_bois_rest_resp as
select
id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
prioriter,
observation,
idtype,
responsable
from VUE_CMD_CLT_DETS_REST_RESP where etat = 9 and (idtype = 'TPD00001' or produit like '%Barquet%');

create view Vue_cmd_dtls_paye_rest_resp as
select
id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
responsable
from  VUE_CMD_CLT_DETS_REST_RESP
where  etat =40 ;

alter table AS_RESPONSABLE add mdp varchar2(100);





--script 12-03-2020
create table commandeLivraison
(
id varchar2(100) constraint commandeLivraison_pk primary key ,
idclient varchar2(100)constraint commandeLiv_cli references as_client,
adresse varchar2(100),
idPoint varchar2(100) constraint commandeliv_poi references POINT,
daty date,
heure varchar2(100),
frais number(10,2),
etat number(10)
);

CREATE SEQUENCE SeqcommandeLivraison
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getSeqcommandeLivraison
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SeqcommandeLivraison.nextval INTO retour FROM dual;
  return retour;
END;
/
create view commandeLivraisonlib as
select
 cmdliv.id,
 cli.TELEPHONE as idclient,
 cmdliv.adresse,
 pt.VAL as idpoint,
 cmdliv.daty,
cmdliv.heure,
cmdliv.frais,
cmdliv.etat
 from commandeLivraison cmdliv
 join AS_CLIENT cli on cli.id =cmdliv.idclient
 join POINT pt on pt.id=cmdliv.idPoint;

create table livreur
(
id varchar2(100) constraint livreur_pk primary key ,
 nom varchar2(100) ,
 contact varchar2(100),
 etat number(10)
);

CREATE SEQUENCE Seqlivreur
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getSeqlivreur
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT Seqlivreur.nextval INTO retour FROM dual;
  return retour;
END;
/

create table livraison
(
id varchar2(100) constraint livraison_pk primary key ,
daty date,
heure varchar2(100),
idcommnademere varchar2(100) constraint livraison_mere_fk references AS_COMMANDECLIENT,
idpoint  varchar2(100) constraint livraison_poi references POINT,
adresse varchar2(100),
idlivreur varchar2(100) constraint  livraison_livreur references  livreur,
etat number(10)
);

CREATE SEQUENCE Seqlivraison
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getSeqlivraison
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT Seqlivraison.nextval INTO retour FROM dual;
  return retour;
END;
/

create view livraisonlib as
select
 liv.id,
liv.daty,
liv.heure,
liv.idcommnademere,
 pt.VAL as idpoint,
liv.adresse,
l.nom as idlivreur,
liv.etat
 from livraison liv
 join AS_COMMANDECLIENT AC on liv.idcommnademere = AC.ID
 join POINT pt on pt.ID=liv.idpoint
 join livreur l on liv.idlivreur = l.id;

create view commandeLivraisonCloture as
select
 *
 from commandeLivraisonlib cmdlib where etat=9;

create view commandeLivraisonAnnulerPayer as
select *
from commandeLivraisonlib where etat=6;


create view commandeLivraisonACloture as
select
 *
from commandeLivraisonlib  where etat=8;

create view LIVRAISONLIB_CREE as
select
 *from LIVRAISONLIB where etat=1;

create view LIVRAISONLIB_LIVREE as
select
*from LIVRAISONLIB where etat=20;


create view commandelivraisonlibl as
select
 cd.id,
 cd.IDMERE,
 comLi.id as idlivraison,
 prod.NOM as produit,
 cd.QUANTITE,
 cd.pu,
 cd.REMISE,
 cd.OBSERVATION,
 cd.etat,
 lib.IDSAUCE ||' '||lib.IDACCOMPAGNEMENT as IDACCOMPAGNEMENTSAUCE,
 cd.PRIORITER
 from commandeLivraison comLi
 join AS_CLIENT AC on comLi.idclient = AC.ID
 join AS_COMMANDECLIENT cmcli on cmcli.CLIENT=ac.id
 join AS_DETAILSCOMMANDE cd on cd.IDMERE=cmcli.id
 left join AS_ACCOMPAGNEMENT_SAUCE_lib lib on lib.id=cd.IDACCOMPAGNEMENTSAUCE
 left join AS_PRODUITS prod on prod.id=cd.PRODUIT;


create view commandelivraisonliblAcloturer as
select
 *from commandelivraisonlibl where etat=8;

create view commandelivraisonlannulerPayer as
select
 *from commandelivraisonlibl where etat=6;

create view commandelivraisonlible as
select
 cd.id,
 cd.IDMERE,
 comLi.id as idlivraison,
 prod.NOM as produit,
 cd.QUANTITE,
 cd.pu,
 cd.REMISE,
 cd.OBSERVATION,
 cd.etat,
 lib.IDSAUCE ||' '||lib.IDACCOMPAGNEMENT as IDACCOMPAGNEMENTSAUCE,
 cd.PRIORITER,
 comli.adresse,
 pt.VAL as idpoint
 from commandeLivraison comLi
 join AS_CLIENT AC on comLi.idclient = AC.ID
 join AS_COMMANDECLIENT cmcli on cmcli.CLIENT=ac.id
 join AS_DETAILSCOMMANDE cd on cd.IDMERE=cmcli.id
 left join AS_ACCOMPAGNEMENT_SAUCE_lib lib on lib.id=cd.IDACCOMPAGNEMENTSAUCE
 left join AS_PRODUITS prod on prod.id=cd.PRODUIT
 join POINT  pt on pt.id=comLi.idPoint ;

create view commandelivraisonliblead as
select *
from commandelivraisonlible where adresse='';
	
	
	create view commandelivraisonliblAlivrer as
select *
from commandelivraisonlibl where etat=12;



--13-03-2020
create view  AS_DETAILSCOMMANDELIB as
select
detCom.id,
detCom.IDMERE,
prd.NOM as PRODUIT,
detCom.QUANTITE,
detCom.pu,
detCom.REMISE,
detCom.OBSERVATION,
detCom.ETAT,
acc.IDSAUCE||' '||acc.IDACCOMPAGNEMENT as IDACCOMPAGNEMENTSAUCE,
detCom.PRIORITER
from AS_DETAILSCOMMANDE detCom
join AS_PRODUITS prd on prd.id=detCom.PRODUIT
left join AS_ACCOMPAGNEMENT_SAUCE_LIB acc on acc.id=detCom.IDACCOMPAGNEMENTSAUCE;
	
alter table livraison modify idcommnademere  constraint livraison_s_fk references AS_DETAILSCOMMANDE;

create or replace view livraisonlib as
select
 liv.id,
liv.daty,
liv.heure,
liv.idcommnademere,
 pt.VAL as idpoint,
liv.adresse,
l.nom as idlivreur,
liv.etat
 from livraison liv
 join AS_DETAILSCOMMANDE dc on liv.idcommnademere = dc.ID
 join POINT pt on pt.ID=liv.idpoint
 join livreur l on liv.idlivreur = l.id;

create view commandeLivraisonlibp as
select cd.id,
 cd.IDMERE,
 comLi.id as idlivraison1,
 prod.NOM as produit,
 cd.QUANTITE,
 cd.pu,
 cd.REMISE,
 cd.OBSERVATION,
 cd.etat,
 lib.IDSAUCE ||' '||lib.IDACCOMPAGNEMENT as IDACCOMPAGNEMENTSAUCE,
 cd.PRIORITER,
 comLi.idclient,
 comLi.adresse,
 comLi.idPoint,
 comLi.daty,
 comLi.heure,
 comLi.frais
 from commandeLivraison comLi
 join AS_CLIENT AC on comLi.idclient = AC.ID
 join AS_COMMANDECLIENT cmcli on cmcli.CLIENT=ac.id
 join AS_DETAILSCOMMANDE cd on cd.IDMERE=cmcli.id
 left join AS_ACCOMPAGNEMENT_SAUCE_lib lib on lib.id=cd.IDACCOMPAGNEMENTSAUCE
 left join AS_PRODUITS prod on prod.id=cd.PRODUIT;

create or replace view commandeLivraisonlibpoint as
select
p.* ,
POINT.VAL,
POINT.DESCE
from commandeLivraisonlibp  p, point where p.idpoint = point.id(+) and p.idpoint is null;


CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_DETS_REST_RESP as 
 select
det.id,
tab.nom as nomtable,
tab.telephone,
tab.id as idclient,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
det.prioriter,
det.observation,
tp.id as idtype,
resp.NOM as responsable
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
join AS_RESPONSABLE resp on resp.id=cmd.RESPONSABLE
order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;
 
 
CREATE OR REPLACE FORCE VIEW VUE_CMD_LIV_DTLS_TOUS as 
select
 vccdrr.id,
vccdrr.nomtable,
vccdrr.typeproduit,
vccdrr.datecommande,
vccdrr.heureliv,
vccdrr.restaurant,
vccdrr.produit,
vccdrr.quantite,
vccdrr.pu,
vccdrr.montant,
vccdrr.etat,
vccdrr.acco_sauce,
vccdrr.prioriter,
vccdrr.observation,
vccdrr.idtype,
vccdrr.responsable,
cl.id as idcl,
cl.daty as datycl,
vccdrr.telephone,
 cl.HEURE as heurecl
from VUE_CMD_CLT_DETS_REST_RESP vccdrr join commandelivraison cl on vccdrr.idclient = cl.idclient
where vccdrr.idtype != 'TPD00001' and vccdrr.produit != 'Barquette'
order by vccdrr.DATECOMMANDE , vccdrr.HEURELIV ASC, vccdrr.prioriter asc;

CREATE OR REPLACE FORCE VIEW VUE_CMD_LIV_DTLS_ACLOT as 
select * from VUE_CMD_LIV_DTLS_TOUS where etat = 8;

CREATE OR REPLACE FORCE VIEW VUE_CMD_LIV_DTLS_ANNULERPAY as 
select * from VUE_CMD_LIV_DTLS_TOUS where etat = 6;


CREATE OR REPLACE FORCE VIEW VUE_CMD_LIV_DTLS_ALIVRER as 
select * from VUE_CMD_LIV_DTLS_TOUS where etat = 12;



--16-03-2020

create or replace view commandelivraisonliblead as
select *
from commandelivraisonlible where adresse='' or adresse is null;


create  or replace view VUE_CMD_DTLS_VALIDE_REST_RESP as
select
 id,
 nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
responsable,
TELEPHONE,
IDCLIENT
from  VUE_CMD_CLT_DETS_REST_RESP
where etat = 11
order by datecommande asc, heureliv asc;


CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_TOUS
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL)
AS 
select
 vccdrr.id,
vccdrr.nomtable,
vccdrr.typeproduit,
vccdrr.datecommande,
vccdrr.heureliv,
vccdrr.restaurant,
  vccdrr.produit,
vccdrr.quantite,
vccdrr.pu,
vccdrr.montant,
  vccdrr.etat,
vccdrr.acco_sauce,
vccdrr.prioriter,
vccdrr.observation,
vccdrr.idtype,
vccdrr.responsable,
'-' as idcl,
vccdrr.datecommande as datycl,
vccdrr.telephone,
 '-' as heurecl
from VUE_CMD_CLT_DETS_REST_RESP vccdrr
order by vccdrr.DATECOMMANDE , vccdrr.HEURELIV ASC, vccdrr.prioriter asc;


INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('MEN00001000', 'A Cloturer','fa fa-list', '/phobo/pages/module.jsp?but=commande/commande-livraison-liste.jsp', '1', '3', 'ASM0000907');
INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('MEN00001001', 'A livrer','fa fa-list', '/phobo/pages/module.jsp?but=commande/commandeAlivrer.jsp', '3', '3', 'ASM0000907');

update MENUDYNAMIQUE set RANG=1 where id ='MEN00001000';
update MENUDYNAMIQUE set RANG=3 where id ='MEN00001001';
update MENUDYNAMIQUE set RANG=5 where id ='ASM0000915';
update MENUDYNAMIQUE set RANG=2 where id ='ASM0000910';
update MENUDYNAMIQUE set RANG=6 where id ='ASM0000918';
update MENUDYNAMIQUE set RANG=7 where id ='ASM0000912';
update MENUDYNAMIQUE set RANG=8 where id ='ASM0000917';



update MENUDYNAMIQUE set id='MEN00001002' where id ='ASM0000910';
update MENUDYNAMIQUE set id='ASM0000910' where id ='MEN00001000';
update MENUDYNAMIQUE set id='MEN00001000' where id ='ASM0000912';
update MENUDYNAMIQUE set id='ASM0000912' where id ='MEN00001002';


update MENUDYNAMIQUE set id='MEN00001003' where id ='ASM0000913';
update MENUDYNAMIQUE set id='ASM0000913' where id ='MEN00001001';
update MENUDYNAMIQUE set id='MEN00001004' where id ='ASM0000915';
update MENUDYNAMIQUE set id='ASM0000915' where id ='MEN00001003';


update MENUDYNAMIQUE set id='MEN00001005' where id ='ASM0000913';
update MENUDYNAMIQUE set id='ASM0000913' where id ='MEN00001000';
update MENUDYNAMIQUE set id='MEN00001006' where id ='ASM0000915';
update MENUDYNAMIQUE set id='ASM0000915' where id ='MEN00001005';
update MENUDYNAMIQUE set id='MEN00001007' where id ='ASM0000917';
update MENUDYNAMIQUE set id='ASM0000917' where id ='MEN00001006';


--17-03-2020
CREATE OR REPLACE FORCE VIEW "VUE_CMD_CLT_DETAIL_TYPEP_REST" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "RESTAURANT", "IDCLIENT", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  select
det.id,
tab.nom as nomtable,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,cmd.CLIENT as idclient,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,
det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;

create or replace view VUE_CMD_LIV_DTLS_ALIVRER_PT as
select
 liv.*,
 cli.telephone as idclient,
 cl.adresse ,
 pt.val as idpoint
 from VUE_CMD_LIV_DTLS_ALIVRER liv
 join commandeLivraison cl on cl.id=liv.idcl
 left join as_client cli on cli.id = cl.idclient
 left join point pt on pt.id=cl.idPoint;

create or replace view VUE_CMD_LIV_DTLS_ALIVRER_PT_AD AS
SELECT *
 FROM VUE_CMD_LIV_DTLS_ALIVRER_PT WHERE adresse IS NULL OR adresse='';


create  or replace view VUE_CMD_CLT_DETS_REST_RESP as
select
det.id,
tab.nom as nomtable,
tab.telephone,
tab.id as idclient,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
det.prioriter,
det.observation,
tp.id as idtype,
resp.NOM as responsable,
cmd.id as idmere
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
join AS_RESPONSABLE resp on resp.id=cmd.RESPONSABLE
order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;


CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_TOUS
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV,
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT,
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE,
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL,idmere)
AS
select
 vccdrr.id,
vccdrr.nomtable,
vccdrr.typeproduit,
vccdrr.datecommande,
vccdrr.heureliv,
vccdrr.restaurant,
   vccdrr.produit,
vccdrr.quantite,
vccdrr.pu,
vccdrr.montant,
   vccdrr.etat,
vccdrr.acco_sauce,
vccdrr.prioriter,
vccdrr.observation,
vccdrr.idtype,
vccdrr.responsable,
'-' as idcl,
vccdrr.datecommande as datycl,
vccdrr.telephone,
  '-' as heurecl,
vccdrr.idmere
from VUE_CMD_CLT_DETS_REST_RESP vccdrr
order by vccdrr.DATECOMMANDE , vccdrr.HEURELIV ASC, vccdrr.prioriter asc;



create or replace view VUE_CMD_LIV_DTLS_ACLOT as
select "ID",
"NOMTABLE",
"TYPEPRODUIT",
"DATECOMMANDE",
"HEURELIV",
"RESTAURANT",
"PRODUIT","QUANTITE","PU","MONTANT","ETAT","ACCO_SAUCE","PRIORITER","OBSERVATION","IDTYPE","RESPONSABLE","IDCL","DATYCL","TELEPHONE","HEURECL",idmere
from VUE_CMD_LIV_DTLS_TOUS where etat = 8;

create or replace view VUE_CMD_LIV_DTLS_ANNULERPAY as
select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","RESTAURANT","PRODUIT",
"QUANTITE","PU","MONTANT","ETAT","ACCO_SAUCE","PRIORITER","OBSERVATION","IDTYPE","RESPONSABLE","IDCL","DATYCL","TELEPHONE","HEURECL" ,idmere
from VUE_CMD_LIV_DTLS_TOUS where etat = 6;



create or replace view VUE_CMD_CLT_TYPEP_REST_NOT AS
select
id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
idclient
from  VUE_CMD_CLT_DETAIL_TYPEP_REST where etat > 0 and etat <= 20 and idclient like 'CASM%';


create or replace view VUE_CMD_LIV_DTLS_ALIVRER  as
select "ID",
"NOMTABLE",
"TYPEPRODUIT",
"DATECOMMANDE",
"HEURELIV",
"RESTAURANT",
"PRODUIT",
"QUANTITE",
"PU",
"MONTANT",
"ETAT",
"ACCO_SAUCE",
"PRIORITER",
"OBSERVATION",
"IDTYPE",
"RESPONSABLE",
"IDCL",
"DATYCL",
"TELEPHONE",
"HEURECL",
idmere 
from VUE_CMD_LIV_DTLS_TOUS where etat = 12;

create or replace view VUE_CMD_LIV_DTLS_ALIVRER_PT as
select
 liv.*,
 pt.VAL as idpoint,
cl.adresse
 from VUE_CMD_LIV_DTLS_ALIVRER liv
 join commandeLivraison cl on cl.idclient=liv.idmere
 left join point pt on pt.id=cl.idPoint;

create or replace view VUE_CMD_LIV_DTLS_ALIVRER_PT_AD AS
SELECT *
 FROM VUE_CMD_LIV_DTLS_ALIVRER_PT WHERE adresse IS NULL OR adresse='';

create or replace view VUE_CMD_LIV_DTLS_ALIVRER_PTVID AS
SELECT *
 FROM VUE_CMD_LIV_DTLS_ALIVRER_PT WHERE idpoint IS NULL OR idpoint='';
		

INSERT INTO menudynamique (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('MEN00001006', 'Livraison apres Commande','fa fa-list', '/phobo/pages/module.jsp?but=commande/livrasionApresCommande-liste.jsp', '5', '2', 'ASM00001');

update MENUDYNAMIQUE set RANG=5 where id ='MEN00001006';
update MENUDYNAMIQUE set RANG=9 where id ='ASC0000105';

update MENUDYNAMIQUE set id='MEN00001007' where id ='ASC0000105';
update MENUDYNAMIQUE set id='ASC0000105' where id ='MEN00001006';



--20-03-2020
ALTER TABLE commandeLivraison DROP CONSTRAINT commandeLiv_cli ;

CREATE OR REPLACE FORCE VIEW "VUE_CMD_CLT_DETAIL_TYPEP_REST" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "RESTAURANT", "IDCLIENT", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  select
det.id,
tab.nom as nomtable,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,cmd.CLIENT as idclient,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,
det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;

 CREATE OR REPLACE VIEW "DETAILSCOMMANDECLIENT" ("ID", "IDMERE", "PRODUIT", "QUANTITE", "PU", "REMISE", "OBSERVATION", "ETAT", "IDACCOMPAGNEMENTSAUCE", "PRIORITER", "IDCLIENT") AS 
  select
det."ID",det."IDMERE",det."PRODUIT",det."QUANTITE",det."PU",det."REMISE",det."OBSERVATION",det."ETAT",det."IDACCOMPAGNEMENTSAUCE",det."PRIORITER",
clt.id as idclient
from AS_DETAILSCOMMANDE det
join AS_COMMANDECLIENT cml on cml.id=det.IDMERE
join AS_CLIENT clt on clt.id=cml.CLIENT;

 CREATE OR REPLACE VIEW "VUE_CMD_ADDITION_LIVRAISON" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "RESTAURANT", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  select
det.id,
cmd.client as nomtable,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,
det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
where det.etat>=1
order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;
 

CREATE OR REPLACE FORCE VIEW "VUE_CMD_ADDITION_LIVRAISON" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "RESTAURANT", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  select
det.id,
cmd.client as nomtable,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,
det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
where det.etat>=1 and det.etat<40
order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;

CREATE OR REPLACE VIEW "LIVRAISONLIB_CREER" as 
select * from LIVRAISONLIB 
WHERE ETAT=1;

CREATE OR REPLACE VIEW "LIVRAISONLIB_VISER" as 
select * from LIVRAISONLIB 
WHERE ETAT=11;



--script21-03-2020

CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_ALIVRER_PT_ID
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE, IDPOINT, ADRESSE)
AS 
select
 liv.*,
 cl.idpoint as idpoint,
cl.adresse
from VUE_CMD_LIV_DTLS_ALIVRER liv
join commandeLivraison cl on cl.idclient=liv.idmere
/

insert into livreur values ('LIV001','Donne','0337408645',1);
insert into livreur values ('LIV002','Fidy','0336821901',1);
insert into livreur values ('LIV004','Tanjona','0346604962',1);
insert into livreur values ('LIV005','Naina','0337212147',1);
insert into livreur values ('LIV006','Mimi','0339052300',1);
insert into livreur values ('LIV007','Libre 1','-',1);
insert into livreur values ('LIV008','Libre 2','-',1);
insert into livreur values ('LIV009','Libre 3','-',1);
insert into livreur values ('LIV010','Libre 4','-',1);
commit;

alter table livraison drop constraint LIVRAISON_MERE_FK;


CREATE OR REPLACE VIEW LIVRAISONLIB
(ID, DATY, HEURE, IDCOMMNADEMERE, IDPOINT, 
 ADRESSE, IDLIVREUR, ETAT)
AS 
select
liv.id,
liv.daty,
liv.heure,
liv.idcommnademere,
pt.VAL as idpoint,
liv.adresse,
l.nom||'--'|| l.contact as idlivreur,
liv.etat
from livraison liv
join AS_DETAILSCOMMANDE dc on liv.idcommnademere = dc.ID
join POINT pt on pt.ID=liv.idpoint
join livreur l on liv.idlivreur = l.id
/

CREATE OR REPLACE VIEW LIVRAISONLIBValide
(ID, DATY, HEURE, IDCOMMNADEMERE, IDPOINT, 
 ADRESSE, IDLIVREUR, ETAT)
AS 
select * from  LIVRAISONLIB liv where liv.etat>=1
/

CREATE OR REPLACE VIEW LIVRAISONLIBVALIDECOMPLET
AS 
select
   liv.id,
liv.daty,
liv.heure,
dc.produit||'--'||dc.IDACCOMPAGNEMENTSAUCE as idcommnademere,
 pt.VAL as idpoint,
liv.adresse,
l.nom||'--'|| l.contact as idlivreur,
liv.etat,clt.nom||'--'||clt.telephone as client,(dc.quantite*dc.pu) as montant
from livraison liv
join AS_DETAILSCOMMANDELIB dc on liv.idcommnademere = dc.ID
join POINT pt on pt.ID=liv.idpoint
join as_commandeclient cmd on dc.idmere=cmd.id
join as_client clt on cmd.client=clt.id
join livreur l on liv.idlivreur = l.id
where liv.etat>=1
/

CREATE TABLE as_restriction
(
  id    VARCHAR2(100),
  idrole VARCHAR2(100),
  idaction      VARCHAR2(100),
  tablename     VARCHAR2(100),
  autorisation  VARCHAR2(100),
  descritpion   VARCHAR2(100),
  iddirection   VARCHAR2(100)
);


ALTER TABLE as_restriction ADD (
  CONSTRAINT as_restriction_PK
 PRIMARY KEY
 (id));

insert into point values ('-','-','-');
commit;

create or replace view pointtrie as select * from point order by id asc;


--23-03-2020

CREATE OR REPLACE VIEW "LIVRAISONLIB_CREER" as 
select * from LIVRAISONLIB 
WHERE ETAT=1;

CREATE OR REPLACE VIEW "LIVRAISONLIB_VISER" as 
select * from LIVRAISONLIB 
WHERE ETAT=11;

CREATE OR REPLACE VIEW LIVRAISONLIBCOMPLET
(ID, DATY, HEURE, IDCOMMNADEMERE, IDPOINT, 
 ADRESSE, IDLIVREUR, ETAT, CLIENT, MONTANT)
AS 
select
            liv.id,
           liv.daty,
           liv.heure,
           dc.produit||'--'||dc.IDACCOMPAGNEMENTSAUCE as idcommnademere,
            pt.VAL as idpoint,
           liv.adresse,
           l.nom||'--'|| l.contact as idlivreur,
           liv.etat,
           clt.nom||'--'||clt.telephone as client,
           (dc.quantite*dc.pu) as montant
        from livraison liv
        join AS_DETAILSCOMMANDELIB dc on liv.idcommnademere = dc.ID
        join POINT pt on pt.ID=liv.idpoint
        join as_commandeclient cmd on dc.idmere=cmd.id
        join as_client clt on cmd.client=clt.id
        join livreur l on liv.idlivreur = l.id
/

create or replace view LIVRAISONLIBVALIDECOMPLET as select * from LIVRAISONLIBCOMPLET where etat>=1;

--24-03-2020
insert into as_produits values('LIVR1','Livraison','Livraison','THB-65cl.jpg','TPD00001',
1,0,3000,'INGRBS0026','',99);

insert into as_prixproduit values('PLIVR1','LIVR1','25/09/2019',3000,'');




insert into as_produits values('LIVR2','Livraison domicile','Livraison domicile','THB-65cl.jpg','TPD00001',
1,0,4000,'INGRBS0026','',99);

insert into as_prixproduit values('PLIVR2','LIVR2','25/09/2019',4000,'');


create or replace view as_produits_avecPrix as
select p.id,p.nom,p.designation,p.photo,
p.typeproduit,p.calorie,p.poids,prix.MONTANT as pa,p.idingredient,
p.idsouscategorie,p.numeroplat from as_produits p,as_prixproduit prix
where p.id=prix.PRODUIT;


CREATE OR REPLACE VIEW VUE_CMD_DTLS_BOIS_REST_RESP
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE)
AS 
select
        id,
       nomtable,
       typeproduit,
       datecommande,
      heureliv,
       restaurant,
       produit,
       quantite,
       pu,
       montant,
       etat,
       acco_sauce,
       prioriter,
       observation,
       idtype,
       responsable
from VUE_CMD_CLT_DETS_REST_RESP where etat = 9 and (idtype = 'TPD00001' or produit like '%Barquet%')
and produit not in (select p2.nom from as_produits p2 where p2.id like 'LIVR%');


create or replace view commandeLivraisonMax as
select * from commandelivraison l1 where l1.id=
(select max(l2.id) from commandelivraison l2 where l2.idclient=l1.idClient);

CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_ALIVRER_PT
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE, IDPOINT, ADRESSE)
AS 
select
 liv."ID",liv."NOMTABLE",liv."TYPEPRODUIT",liv."DATECOMMANDE",liv."HEURELIV",liv."RESTAURANT",liv."PRODUIT",liv."QUANTITE",liv."PU",liv."MONTANT",liv."ETAT",liv."ACCO_SAUCE",liv."PRIORITER",liv."OBSERVATION",liv."IDTYPE",liv."RESPONSABLE",liv."IDCL",liv."DATYCL",liv."TELEPHONE",liv."HEURECL",liv."IDMERE",
 pt.VAL as idpoint,
cl.adresse
 from VUE_CMD_LIV_DTLS_ALIVRER liv
 join commandeLivraisonMax cl on cl.idclient=liv.idmere
 left join point pt on pt.id=cl.idPoint;


 CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_ALIVRER_PT_ID
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE, IDPOINT, ADRESSE)
AS 
select
 liv."ID",liv."NOMTABLE",liv."TYPEPRODUIT",liv."DATECOMMANDE",liv."HEURELIV",liv."RESTAURANT",liv."PRODUIT",liv."QUANTITE",liv."PU",liv."MONTANT",liv."ETAT",liv."ACCO_SAUCE",liv."PRIORITER",liv."OBSERVATION",liv."IDTYPE",liv."RESPONSABLE",liv."IDCL",liv."DATYCL",liv."TELEPHONE",liv."HEURECL",liv."IDMERE",
 cl.idpoint as idpoint,
cl.adresse
from VUE_CMD_LIV_DTLS_ALIVRER liv
join commandeLivraisonMax cl on cl.idclient=liv.idmere;


CREATE OR REPLACE VIEW COMMANDELIVRAISONLIB
(ID, IDCLIENT, ADRESSE, IDPOINT, DATY, 
 HEURE, FRAIS, ETAT)
AS 
select
			cmdliv.id,
            cmdliv.idclient,
            cmdliv.adresse,
            pt.VAL as idpoint,
            cmdliv.daty,
           cmdliv.heure,
           cmdliv.frais,
           cmdliv.etat
        from commandeLivraison cmdliv
        left join as_commandeclient cmd on cmd.id=cmdliv.idclient
        left join POINT pt on pt.id=cmdliv.idPoint
/


CREATE OR REPLACE VIEW VUE_CMD_DTLS_CLOT_REST_RESP
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE)
AS 
select
id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
prioriter,
observation,
idtype,
idmere as responsable
from VUE_CMD_CLT_DETS_REST_RESP
where etat = 9 and idtype != 'TPD00001' and produit != 'Barquette'
order by DATECOMMANDE , HEURELIV ASC,prioriter asc
/


CREATE OR REPLACE VIEW VUE_CMD_DTLS_BOIS_REST_RESP
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE)
AS 
select
        id,
       nomtable,
       typeproduit,
       datecommande,
       heureliv,
       restaurant,
       produit,
       quantite,
       pu,
       montant,
       etat,
       acco_sauce,
       prioriter,
       observation,
       idtype,
       idmere as responsable
from VUE_CMD_CLT_DETS_REST_RESP where etat = 9 and (idtype = 'TPD00001' or produit like '%Barquet%')
and produit not in (select p2.nom from as_produits p2 where p2.id like 'LIVR%')
/

CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, VENTE, ETAT, SECT, 
 RESTAURANT, PRENOM)
AS 
select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, tbl.TELEPHONE as responsable, tbl.nom as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect, cmd.point , tbl.prenom
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    as_client tbl,
    vente
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur
    and cmd.client = tbl.id
    and cmd.vente=vente.id(+);


CREATE OR REPLACE VIEW VUE_CMD_DTLS_VALIDE_REST_RESP
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, RESPONSABLE, TELEPHONE, IDCLIENT)
AS 
select
 id,
 nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
idmere as responsable,
TELEPHONE,
IDCLIENT
from  VUE_CMD_CLT_DETS_REST_RESP
where etat = 11
order by datecommande asc, heureliv asc;

--25-03-2020
 CREATE OR REPLACE  VIEW "COMMANDELIVRAISONLIB" ("ID", "IDCLIENT", "ADRESSE", "IDPOINT", "DATY", "HEURE", "FRAIS", "ETAT") AS 
  select
            cmdliv.id,
            cmd.ID as idclient,
            cmdliv.adresse,
            pt.VAL as idpoint,
            cmdliv.daty,
           cmdliv.heure,
           cmdliv.frais,
           cmdliv.etat
        from commandeLivraison cmdliv
        join AS_COMMANDECLIENT cmd on cmd.id =cmdliv.idclient
        left join POINT pt on pt.id=cmdliv.idPoint;

insert into caisse values('CE000604','Airtel Money','3','etaCs1');
insert into caisse values('CE000605','Orange Money','3','etaCs1');

--26-03-2020

insert into caisse values ('-','-','2','etaCs1');
insert into caisse values ('CE000606','A payer','2','etaCs1');
commit;
CREATE OR REPLACE VIEW MVTCAISSELETTRERESTE
(ID, DATY, DESIGNATION, DEBIT, CREDIT, 
 IDDEVISE, IDMODE, IDCAISSE, REMARQUE, TIERS, 
 NUMPIECE, NUMCHEQUE, TYPEMVT, DATYVALEUR, IDORDRE, 
 ETAT, AGENCE, IDMVTOR, ETABLISSEMENT, RESTE)
AS 
select m."ID",m."DATY",m."DESIGNATION",m."DEBIT",m."CREDIT",m."IDDEVISE",
m."IDMODE",m."IDCAISSE",m."REMARQUE",m."TIERS",l.idlivreur,m."NUMCHEQUE",
m."TYPEMVT",m."DATYVALEUR",m."IDORDRE",m."ETAT",m."AGENCE",m."IDMVTOR",
m."ETABLISSEMENT", cast((m.credit-m.debit) as NUMBER(10,2)) as reste from MVTCAISSELETTRE m,
livraisonlib l where m.idordre=l.IDCOMMNADEMERE(+);
CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_ALIVRER_PT
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE, IDPOINT, ADRESSE)
AS 
select
 liv."ID",liv."NOMTABLE",liv."TYPEPRODUIT",liv."DATECOMMANDE",liv."HEURELIV",liv."RESTAURANT",liv."PRODUIT",liv."QUANTITE",liv."PU",liv."MONTANT",
 liv."ETAT",liv."ACCO_SAUCE",liv."PRIORITER",liv."OBSERVATION",liv."IDTYPE",liv."IDMERE",liv."IDCL",liv."DATYCL",liv."TELEPHONE",liv."HEURECL",liv."IDMERE",
 pt.VAL as idpoint,
cl.adresse
 from VUE_CMD_LIV_DTLS_ALIVRER liv
 join commandeLivraisonMax cl on cl.idclient=liv.idmere
 left join point pt on pt.id=cl.idPoint
/


create or replace view mvtCaisseLettreComandeMere as
select m.ID, m.daty, m.designation, m.debit, m.credit, m.iddevise,
          m.idmode, m.idcaisse, m.remarque,
          m.tiers AS tiers, dcmd.IDMERE as numpiece, m.numcheque, m.typemvt,
          m.datyvaleur, m.idordre,m.etat,m.agence,m.IDMVTOR,m.ETABLISSEMENT
from mvtCaisse m, as_detailscommande dcmd where m.IDORDRE=dcmd.ID;

create or replace view mvtCaisseLettreCmdMereGroup as
select max(m.ID) as id, max(m.daty) as daty, '-' as designation, sum(m.debit) as debit, sum(m.credit) as credit, max(m.iddevise) as iddevise,
          max(m.idmode) as idmode, max(c.DESCCAISSE) as idcaisse, max(m.remarque) as remarque,
          max(m.tiers) AS tiers, numpiece, max(m.numcheque) as numcheque, max(m.typemvt) as typemvt,
          max(m.datyvaleur) as datyvaleur, '-' as idordre,max (m.etat) as etat, max(m.agence) as agence,
          max(m.IDMVTOR) as idmvtor, max(m.ETABLISSEMENT) as etablissement
from mvtCaisseLettreComandeMere m, caisse c where m.IDCAISSE=c.IDCAISSE group by numpiece;


CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, VENTE, ETAT, SECT, 
 RESTAURANT, PRENOM)
AS 
select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, tbl.TELEPHONE as responsable, tbl.nom as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect, cmd.point , 
    mvt.idcaisse
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    as_client tbl,
    vente,mvtCaisseLettreCmdMereGroup mvt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur
    and cmd.client = tbl.id
    and cmd.vente=vente.id(+)
    and cmd.id=mvt.NUMPIECE(+);
	
	insert into menudynamique values('MEN00116','Livreur','fa fa-home',null,'7','2','ASM00009');
insert into menudynamique values('MEN00117','Liste Livreur','fa fa-list','/phobo/pages/module.jsp?but=livraison/livreur-liste.jsp','1','3','MEN00116');
insert into menudynamique values('MEN00118','Saisie Livreur','fa fa-plus','/phobo/pages/module.jsp?but=livraison/livreur-saisie.jsp','2','3','MEN00116');

--27-03-2020

create or replace view mvtCaisseLettreComandeMere as
select m.ID, m.daty, m.designation, m.debit, m.credit, m.iddevise,
          m.idmode, m.idcaisse, m.remarque,
          m.tiers AS tiers, dcmd.IDMERE as numpiece, m.numcheque, m.typemvt,
          m.datyvaleur, m.idordre,m.etat,m.agence,m.IDMVTOR,m.ETABLISSEMENT
from mvtCaisse m, as_detailscommande dcmd where m.IDORDRE=dcmd.ID;

create or replace view mvtCaisseLettreCmdMereGroup as
select max(m.ID) as id, max(m.daty) as daty, '-' as designation, sum(m.debit) as debit, sum(m.credit) as credit, max(m.iddevise) as iddevise,
          max(m.idmode) as idmode, max(c.DESCCAISSE) as idcaisse, max(m.remarque) as remarque,
          max(m.tiers) AS tiers, numpiece, max(m.numcheque) as numcheque, max(m.typemvt) as typemvt,
          max(m.datyvaleur) as datyvaleur, '-' as idordre,max (m.etat) as etat, max(m.agence) as agence,
          max(m.IDMVTOR) as idmvtor, max(m.ETABLISSEMENT) as etablissement
from mvtCaisseLettreComandeMere m, caisse c where m.IDCAISSE=c.IDCAISSE group by numpiece;


CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, VENTE, ETAT, SECT, 
 RESTAURANT, PRENOM)
AS 
select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, tbl.TELEPHONE as responsable, tbl.nom as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect, cmd.point , 
    mvt.idcaisse
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    as_client tbl,
    vente,mvtCaisseLettreCmdMereGroup mvt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur
    and cmd.client = tbl.id
    and cmd.vente=vente.id(+)
    and cmd.id=mvt.NUMPIECE(+);



CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE2
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, ETAT, COURSIER, MONTANT, 
 SECTEUR, VENTE, RESTAURANT, PRENOM)
AS 
select 
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE", mcmd.ADRESSE as "ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",cmd."ETAT",  '-', mtnt.montant, sect.val as secteur ,cmd.vente,cmd.restaurant,cmd.prenom
from
    as_commandeclient_libelle cmd,
    montantparcommande mtnt,
    as_secteur sect,
    commandelivraisonMax mcmd
where
    mtnt.id = cmd.id and sect.id(+)=cmd.sect
    and cmd.ID=mcmd.idclient(+)
/

create or replace view as_commandeClientLibLivraison as
select * from as_commandeclient_libelle2 cmd where cmd.ADRESSELIV is not null;


create or replace view as_commandeClientLibTable
 as select cmd.* from as_commandeclient_libelle2 cmd 
 where  cmd.ADRESSELIV is null or trim(cmd.ADRESSELIV)='';
 
 --02-04-2020
 
 CREATE OR REPLACE VIEW "AS_PRODUITS_LIBELLEPRIX" ("ID", "NOM", "DESIGNATION", "PHOTO", "TYPEPRODUIT", "CALORIE", "PA", "POIDS", "PU") AS
 select
   prd.id, prd.nom, prd.designation, prd.photo, tpd.val,
   case when prd.calorie = '1' then 'DISPONIBLE'
   else 'INDISPONIBLE' end,
   prd.pa, prd.poids, sum(trf.montant) keep (dense_rank first order by trf.dateapplication desc)
from
   as_produits prd,
   as_typeproduit tpd,
   as_prixproduit trf
where
   prd.typeproduit = tpd.id
   and trf.produit = prd.id
group by prd.id, prd.nom, prd.designation,
prd.photo, tpd.val, prd.calorie, prd.pa,
prd.poids;


   CREATE OR REPLACE VIEW AS_NOMBRECOMMANDELIVRE AS
   select t1.idmere, t1.nombrelivre, t2.total as totalcommande from
   (
   select det.idmere, count(*) as nombrelivre,0 as total  from  as_commandeclient mere inner join as_detailscommande det on mere.id=det.idmere
   where  det.produit NOT LIKE '%LIVR%' and det.etat>=20 and det.etat>0
   group by idmere
   ) t1
   inner join
   (
   select det.idmere,0 nombrelivre, count(*) as total  from  as_commandeclient mere inner join as_detailscommande det on mere.id=det.idmere
   where  det.produit  NOT LIKE '%LIVR%' and det.etat>0
   group by idmere
   )
   t2
   on t1.idmere=t2.idmere;
   
   CREATE OR REPLACE  VIEW "AS_COMMANDECLIENT_LIBELLE2" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR", "VENTE", "RESTAURANT", "PRENOM")
   as
  select
      cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
      cmd."REMARQUE", mcmd.ADRESSE as "ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",
         case  cmdlivre.nombrelivre WHEN cmdlivre.totalcommande then 20
         else (select min(etat) from as_detailscommande det where det.idmere=cmd.id)
         end  "ETAT" ,  '-', mtnt.montant, sect.val as secteur ,cmd.vente,cmd.restaurant,cmd.prenom
  from
      as_commandeclient_libelle cmd,
      montantparcommande mtnt,
      as_secteur sect,
      commandelivraisonMax mcmd,
      AS_NOMBRECOMMANDELIVRE  cmdlivre
  where
      mtnt.id = cmd.id and sect.id(+)=cmd.sect
      and cmd.ID=mcmd.idclient(+)
      and cmd.id=cmdlivre.idmere(+)
      ORDER BY  DATELIV,HEURELIV asc;
	  
	  
--04-04-2020
update as_produits set typeproduit='TPD00001' where id ='PRDTBKTP000039';
--05-04-2020
CREATE OR REPLACE VIEW VUE_CMD_DTLS_VALIDE_REST_RESP

AS 
select
 id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
prioriter,
observation,
idtype,
idmere as responsable
from  VUE_CMD_CLT_DETS_REST_RESP
where etat = 11
order by datecommande asc, heureliv asc;


CREATE OR REPLACE VIEW LIVRAISONLIBCOMPLETMERE
(ID, DATY, HEURE, IDCOMMNADEMERE, IDPOINT, 
 ADRESSE, IDLIVREUR, ETAT, CLIENT, MONTANT,idmere)
AS 
select
         liv.id,
           liv.daty,
           liv.heure,
           dc.produit||'--'||dc.IDACCOMPAGNEMENTSAUCE as idcommnademere,
            pt.VAL as idpoint,
           liv.adresse,
           l.nom||'--'|| l.contact as idlivreur,
           liv.etat,
           clt.nom||'--'||clt.telephone as client,
           (dc.quantite*dc.pu) as montant,
           cmd.ID
        from livraison liv
        join AS_DETAILSCOMMANDELIB dc on liv.idcommnademere = dc.ID
        join POINT pt on pt.ID=liv.idpoint
        join as_commandeclient cmd on dc.idmere=cmd.id
        join as_client clt on cmd.client=clt.id
        join livreur l on liv.idlivreur = l.id;

CREATE OR REPLACE VIEW LIVRAISONLIBCOMPLETMEREGROUPE
AS 
select max (liv.id) as id,max(liv.daty) as daty,max(liv.heure) as heure,liv.idmere as idcommnademere,
max(cmd.prenom) as idpoint,max(liv.adresse) as adresse,liv.idlivreur,
liv.etat,max(liv.client) as client,sum(liv.montant) as montant,liv.idmere from LIVRAISONLIBCOMPLETMERE liv,
as_commandeclient_libelle cmd where cmd.id=liv.idmere
 group by liv.idlivreur,liv.etat,liv.idmere

 CREATE OR REPLACE VIEW VUE_CMD_CLT_DETS_REST_RESP
(ID, NOMTABLE, TELEPHONE, IDCLIENT, TYPEPRODUIT, 
 DATECOMMANDE, HEURELIV, RESTAURANT, PRODUIT, QUANTITE, 
 PU, MONTANT, ETAT, ACCO_SAUCE, PRIORITER, 
 OBSERVATION, IDTYPE, RESPONSABLE, IDMERE)
AS 
select
det.id,
tab.nom as nomtable,
tab.telephone,
tab.id as idclient,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
det.prioriter,
det.observation,
tp.id as idtype,
prod.id as responsable,
cmd.id as idmere
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
join AS_RESPONSABLE resp on resp.id=cmd.RESPONSABLE
order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;

CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_TOUS
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE)
AS 
select
 vccdrr.id,
vccdrr.nomtable,
vccdrr.typeproduit,
vccdrr.datecommande,
vccdrr.heureliv,
vccdrr.restaurant,
   vccdrr.produit,
vccdrr.quantite,
vccdrr.pu,
vccdrr.montant,
   vccdrr.etat,
vccdrr.acco_sauce,
vccdrr.prioriter,
vccdrr.observation,
vccdrr.idtype,
vccdrr.responsable,
'-' as idcl,
vccdrr.datecommande as datycl,
vccdrr.telephone,
  vccdrr.responsable as heurecl,
vccdrr.idmere
from VUE_CMD_CLT_DETS_REST_RESP vccdrr
order by vccdrr.DATECOMMANDE , vccdrr.HEURELIV ASC, vccdrr.prioriter asc;


CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_ALIVRER
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE)
AS 
select "ID",
"NOMTABLE",
"TYPEPRODUIT",
"DATECOMMANDE",
"HEURELIV",
"RESTAURANT",
"PRODUIT",
"QUANTITE",
"PU",
"MONTANT",
"ETAT",
"ACCO_SAUCE",
"PRIORITER",
"OBSERVATION",
"IDTYPE",
"RESPONSABLE",
"IDCL",
"DATYCL",
"TELEPHONE",
"HEURECL",
idmere
from VUE_CMD_LIV_DTLS_TOUS where (etat = 12 or (responsable like 'LIVR%' and etat>8 and etat<20)) order by heureLiv desc;







