CREATE OR REPLACE FORCE VIEW AS_BONDELIVRASION_FILLE_GRP
(
   ID,
   PRODUIT,
   NUMBL,
   QUANTITE,
   QUANTITEPARPACK,
   IDDETAILSFACTUREFOURNISSEUR,
   DATY,
   IDFOURNISSEUR,
   NOMFOURNISSEUR,
   etat
)
AS
   SELECT '-' as id,
          blfd."PRODUIT",
          blfd."NUMBL",
          sum(blfd."QUANTITE"),
          avg(blfd."QUANTITEPARPACK"),
          blfd."IDDETAILSFACTUREFOURNISSEUR",
          blfd."DATY",
          fp1.id AS idfournisseur,
          fp1.NOM AS nomfournisseur,
          blfd.etat
     FROM AS_BONDELIVRAISON_FILLE_DATE blfd
          JOIN DETAILSFACTUREFOURNISSEUR ff
             ON ff.id = blfd.IDDETAILSFACTUREFOURNISSEUR
          LEFT JOIN FACTUREFOURNISSEUR fp ON fp.id = ff.IDMERE
          LEFT JOIN FOURNISSEURPRODUITS fp1 ON fp1.id = fp.IDFOURNISSEUR
          group by
          blfd."PRODUIT",
          blfd."NUMBL",
          blfd."IDDETAILSFACTUREFOURNISSEUR",
          blfd."DATY",
          fp1.id,
          fp1.NOM,
          blfd.etat;
          
          
CREATE OR REPLACE FORCE VIEW DETAILSFFLIBCOMPLET
(
   ID,
   IDINGREDIENT,
   QTE,
   PU,
   IDMERE,
   COMPTE,
   DATY,
   IDFOURNISSEUR,
   IDTVA,
   MONTANTTTC,
   REMARQUE,
   DATEEMISSION,
   DESIGNATION,
   IDDEVISE,
   NUMFACT,
   DATYECHEANCE,
   ETAT,
   IDTYPEFACTURELIB,
   livre,
   reste
)
AS
   SELECT dt."ID",
          dt."IDINGREDIENT",
          dt."QTE",
          dt."PU",
          dt."IDMERE",
          dt."COMPTE",
          ff.DATY,
          ff.IDFOURNISSEUR,
          ff.IDTVA,
          dt.pu * dt.qte AS MONTANTTTC,
          ff.REMARQUE,
          ff.DATEEMISSION,
          ff.DESIGNATION,
          ff.IDDEVISE,
          ff.NUMFACT,
          ff.DATYECHEANCE,
          ff.ETAT,
          dt.idtypefacturelib,
          bl.quantite as livre,
          dt.qte-bl.quantite as reste
     FROM DETAILSFFLIB dt, facturefournisseurlettre ff,AS_BONDELIVRASION_FILLE_GRP bl
          where ff.id = dt.idmere
          and bl.IDDETAILSFACTUREFOURNISSEUR(+)=dt.id  
    and FF.ETAT = 'VALIDER';