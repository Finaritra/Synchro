CREATE OR REPLACE FORCE VIEW DETAILSFFLIB
(
   ID,
   IDINGREDIENT,
   QTE,
   PU,
   IDMERE,
   COMPTE,
   idtypefacturelib
)
AS
   SELECT df.id,
          p.libelle AS idingredient,
          df.QTE,
          df.PU,
          df.IDMERE,
          df.COMPTE,
          f.val as idtypefacturelib
     FROM detailsfacturefournisseur df
          JOIN as_ingredients p ON df.idingredient = p.id
          left join typefacture f on DF.IDTYPEFACTURE=f.id;
          
CREATE OR REPLACE FORCE VIEW DETAILSFFLIBCOMPLET
(
   ID,
   IDINGREDIENT,
   QTE,
   PU,
   IDMERE,
   COMPTE,
   DATY,
   IDFOURNISSEUR,
   IDTVA,
   MONTANTTTC,
   REMARQUE,
   DATEEMISSION,
   DESIGNATION,
   IDDEVISE,
   NUMFACT,
   DATYECHEANCE,
   ETAT,
   idtypefacturelib
)
AS
   SELECT dt."ID",
          dt."IDINGREDIENT",
          dt."QTE",
          dt."PU",
          dt."IDMERE",
          dt."COMPTE",
          ff.DATY,
          ff.IDFOURNISSEUR,
          ff.IDTVA,
          dt.pu * dt.qte AS MONTANTTTC,
          ff.REMARQUE,
          ff.DATEEMISSION,
          ff.DESIGNATION,
          ff.IDDEVISE,
          ff.NUMFACT,
          ff.DATYECHEANCE,
          ff.ETAT,
          dt.idtypefacturelib
     FROM DETAILSFFLIB dt
          JOIN facturefournisseurlettre ff ON ff.id = dt.idmere where FF.ETAT='VALIDER' ;
CREATE OR REPLACE FORCE VIEW ETATFACTUREMONTANT
(
   ID,
   DATY,
   IDFOURNISSEUR,
   IDTVA,
   MONTANTTTC,
   REMARQUE,
   DATEEMISSION,
   DESIGNATION,
   IDDEVISE,
   NUMFACT,
   DATYECHEANCE,
   ETAT,
   MONTANTPAYE,
   RESTE,
   ENTITE
)
AS
   SELECT f."ID",
          f."DATY",
          f."IDFOURNISSEUR",
          f."IDTVA",
          CAST (f."MONTANTTTC" AS NUMBER (18, 2)) AS MONTANTTTC,
          f."REMARQUE",
          f."DATEEMISSION",
          f."DESIGNATION",
          f."IDDEVISE",
          f."NUMFACT",
          f."DATYECHEANCE",
          f."ETAT",
          CAST (NVL (op.montant, 0) AS NUMBER (18, 2)) AS montantPaye,
          CAST ( (f.montantTTC - NVL (op.montant, 0)) AS NUMBER (18, 2))
             AS reste,
          f.entite
     FROM FACTUREFOURNISSEURLETTREMTT f, ORDONNERPAYEMENTGROUPEFACT op
    WHERE f.ID = op.DED_ID(+);


CREATE OR REPLACE FORCE VIEW AS_BONDELIVRAISON_FILLE_DATE
(
   ID,
   PRODUIT,
   NUMBL,
   QUANTITE,
   QUANTITEPARPACK,
   IDDETAILSFACTUREFOURNISSEUR,
   DATY,
   etat
)
AS
   SELECT blf."ID",
          blf."PRODUIT",
          blf."NUMBL",
          blf."QUANTITE",
          blf."QUANTITEPARPACK",
          blf."IDDETAILSFACTUREFOURNISSEUR",
          bl.daty,
          BL.ETAT
     FROM as_bondelivraison_fille_lib blf
          JOIN as_bondelivraison bl ON blf.numbl = bl.id;

CREATE OR REPLACE FORCE VIEW AS_BONDELIVRASION_FILLE_FRS
(
   ID,
   PRODUIT,
   NUMBL,
   QUANTITE,
   QUANTITEPARPACK,
   IDDETAILSFACTUREFOURNISSEUR,
   DATY,
   IDFOURNISSEUR,
   NOMFOURNISSEUR,
   etat
)
AS
   SELECT blfd."ID",
          blfd."PRODUIT",
          blfd."NUMBL",
          blfd."QUANTITE",
          blfd."QUANTITEPARPACK",
          blfd."IDDETAILSFACTUREFOURNISSEUR",
          blfd."DATY",
          fp1.id AS idfournisseur,
          fp1.NOM AS nomfournisseur,
          blfd.etat
     FROM AS_BONDELIVRAISON_FILLE_DATE blfd
          JOIN DETAILSFACTUREFOURNISSEUR ff
             ON ff.id = blfd.IDDETAILSFACTUREFOURNISSEUR
          LEFT JOIN FACTUREFOURNISSEUR fp ON fp.id = ff.IDMERE
          LEFT JOIN FOURNISSEURPRODUITS fp1 ON fp1.id = fp.IDFOURNISSEUR;


