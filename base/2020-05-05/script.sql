create view besoinlib as
select 
    bes.id,
    prd.nom as idproduit,
    bes.remarque,
    bes.quantite
    from besoin bes 
    join as_produits prd on prd.id=bes.idproduit;



CREATE OR REPLACE VIEW "AS_ETATSTOCKVIDE" AS 
select 
'-' as ingredients,
'-' as unite,
CAST(0 as NUMBER(10,2)) as entree,
CAST(0 as NUMBER(10,2)) as sortie,
CAST(0 as NUMBER(10,2)) as reste,
CAST(0 as NUMBER(10,2)) as report,
'' as daty,
'' as idcategorieingredient,
'-' as point,
CAST(0 as NUMBER(10,2)) as besoin
 from dual;

create or replace view besoinrecette as
select 
    rec.idingredients,
    sum(rec.quantite * be.quantite)as besoin
    from as_recette rec 
    left join besoin be on be.idproduit=rec.idproduits
    group by rec.idingredients;

insert into menudynamique values('MEN00108','Saisie besoin','fa fa-plus','/phobo/pages/module.jsp?but=stock/besoin/besoin-saisie.jsp','4','2','ASM00014');
commit;