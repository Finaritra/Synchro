CREATE OR REPLACE VIEW VUE_CMD_DTLS_VALIDE_REST_RESP

AS 
select
 id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
prioriter,
observation,
idtype,
idmere as responsable
from  VUE_CMD_CLT_DETS_REST_RESP
where etat = 11
order by datecommande asc, heureliv asc;


CREATE OR REPLACE VIEW LIVRAISONLIBCOMPLETMERE
(ID, DATY, HEURE, IDCOMMNADEMERE, IDPOINT, 
 ADRESSE, IDLIVREUR, ETAT, CLIENT, MONTANT,idmere)
AS 
select
         liv.id,
           liv.daty,
           liv.heure,
           dc.produit||'--'||dc.IDACCOMPAGNEMENTSAUCE as idcommnademere,
            pt.VAL as idpoint,
           liv.adresse,
           l.nom||'--'|| l.contact as idlivreur,
           liv.etat,
           clt.nom||'--'||clt.telephone as client,
           (dc.quantite*dc.pu) as montant,
           cmd.ID
        from livraison liv
        join AS_DETAILSCOMMANDELIB dc on liv.idcommnademere = dc.ID
        join POINT pt on pt.ID=liv.idpoint
        join as_commandeclient cmd on dc.idmere=cmd.id
        join as_client clt on cmd.client=clt.id
        join livreur l on liv.idlivreur = l.id;

CREATE OR REPLACE VIEW LIVRAISONLIBCOMPLETMEREGROUPE
AS 
select max (liv.id) as id,max(liv.daty) as daty,max(liv.heure) as heure,liv.idmere as idcommnademere,
max(cmd.prenom) as idpoint,max(liv.adresse) as adresse,liv.idlivreur,
liv.etat,max(liv.client) as client,sum(liv.montant) as montant,liv.idmere from LIVRAISONLIBCOMPLETMERE liv,
as_commandeclient_libelle cmd where cmd.id=liv.idmere
 group by liv.idlivreur,liv.etat,liv.idmere

 CREATE OR REPLACE VIEW VUE_CMD_CLT_DETS_REST_RESP
(ID, NOMTABLE, TELEPHONE, IDCLIENT, TYPEPRODUIT, 
 DATECOMMANDE, HEURELIV, RESTAURANT, PRODUIT, QUANTITE, 
 PU, MONTANT, ETAT, ACCO_SAUCE, PRIORITER, 
 OBSERVATION, IDTYPE, RESPONSABLE, IDMERE)
AS 
select
det.id,
tab.nom as nomtable,
tab.telephone,
tab.id as idclient,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
det.prioriter,
det.observation,
tp.id as idtype,
prod.id as responsable,
cmd.id as idmere
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
join AS_RESPONSABLE resp on resp.id=cmd.RESPONSABLE
order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;

CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_TOUS
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE)
AS 
select
 vccdrr.id,
vccdrr.nomtable,
vccdrr.typeproduit,
vccdrr.datecommande,
vccdrr.heureliv,
vccdrr.restaurant,
   vccdrr.produit,
vccdrr.quantite,
vccdrr.pu,
vccdrr.montant,
   vccdrr.etat,
vccdrr.acco_sauce,
vccdrr.prioriter,
vccdrr.observation,
vccdrr.idtype,
vccdrr.responsable,
'-' as idcl,
vccdrr.datecommande as datycl,
vccdrr.telephone,
  vccdrr.responsable as heurecl,
vccdrr.idmere
from VUE_CMD_CLT_DETS_REST_RESP vccdrr
order by vccdrr.DATECOMMANDE , vccdrr.HEURELIV ASC, vccdrr.prioriter asc;


CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_ALIVRER
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE)
AS 
select "ID",
"NOMTABLE",
"TYPEPRODUIT",
"DATECOMMANDE",
"HEURELIV",
"RESTAURANT",
"PRODUIT",
"QUANTITE",
"PU",
"MONTANT",
"ETAT",
"ACCO_SAUCE",
"PRIORITER",
"OBSERVATION",
"IDTYPE",
"RESPONSABLE",
"IDCL",
"DATYCL",
"TELEPHONE",
"HEURECL",
idmere
from VUE_CMD_LIV_DTLS_TOUS where (etat = 12 or (responsable like 'LIVR%' and etat>8 and etat<20)) order by heureLiv desc;