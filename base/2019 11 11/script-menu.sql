INSERT INTO "MENUDYNAMIQUE" (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM00006014', 'Saisie fournisseur', 'fa fa-plus', '/phobo/pages/module.jsp?but=facturefournisseur/client-ajout.jsp', '4', '3', 'ASM0000601');
INSERT INTO "MENUDYNAMIQUE" (ID, LIBELLE, ICONE, HREF, RANG, NIVEAU, ID_PERE) VALUES ('ASM00006015', 'Liste fournisseur', 'fa fa-list', '/phobo/pages/module.jsp?but=facturefournisseur/client-liste.jsp', '5', '3', 'ASM0000601');

create or replace view as_detailscommandevalide as select * from as_detailscommande det where det.etat>0;

CREATE OR REPLACE VIEW MONTANTPARCOMMANDE
(ID, MONTANT)
AS 
select idmere as id, sum(quantite*pu - (quantite*pu)*remise/100) as montant from as_detailscommandevalide
 group by idmere;


CREATE OR REPLACE VIEW VUE_CMD_CLIENT_DETAILS_REST
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE)
AS 
select
det.id,
tab.nom as nomtable,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,
det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
    from as_commandeclient cmd
    join as_detailscommandevalide det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join as_client tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    where cmd.datecommande='17/11/2019'
    order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;