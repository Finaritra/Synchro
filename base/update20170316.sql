update as_ingredients set unite='UNT00001'where unite='g';
update as_ingredients set unite='UNT00001'where unite='kg';
update as_ingredients set unite='UNT00004'where unite='ml';
update as_ingredients set unite='UNT00004'where unite='l';
/*mette a jour kg en g*/
update as_ingredients set unite='UNT00001'where unite='UNT00002';
/*mette a jour l en ml*/
update as_ingredients set unite='UNT00004'where unite='UNT00003';
/*Verification que le tige n'existe pas dans ingredients*/
select * from as_ingredients where unite='UNT00006';
/*Supprimer tige*/
delete as_unite where id='UNT00006';
/*Verification que le Kg n'existe pas dans ingredients*/
select * from as_ingredients where unite='UNT00002';
/*Supprimer Kg*/
delete as_unite where id='UNT00002';
/*Verification que le l n'existe pas dans ingredients*/
select * from as_ingredients where unite='UNT00003';
/*Supprimer l*/
delete as_unite where id='UNT00003';

CREATE OR REPLACE View "AS_ORDONNERPAYEMENTLIBELLE"("ID", "DED_ID", "IDLIGNE", "DATY", "MONTANT", "REMARQUE", "ETAT") as
  select id,DED_ID,idligne,daty,montant,remarque, 
  CASE 
    WHEN etat=1 THEN 'CREER' 
    WHEN etat=11 THEN 'VISÉ(E)' 
    ELSE 'AUTRE' END 
    from ordonnerpayement;
	
CREATE OR REPLACE FORCE VIEW "AS_FACTUREFOURNISSEURLETTRE" ("ID", "DATY", "IDFOURNISSEUR", "IDTVA", "MONTANTTTC", "REMARQUE", "DATEEMISSION", "DESIGNATION", "IDDEVISE", "NUMFACT", "DATYECHEANCE", "ETAT") AS 
  SELECT ff.id, ff.daty, c.nom as idFournisseur, ff.idtva, ff.montantttc,ff.remarque, 
  ff.dateemission, ff.designation, ff.iddevise, ff.numfact, ff.datyecheance, CASE
   WHEN ff.etat=1 THEN 'CREER'
   WHEN ff.etat=11 THEN 'VISÉ(E)'
   ELSE 'AUTRE' 
  END
FROM facturefournisseur ff,client c where c.IDCLIENT (+)= ff.IDFOURNISSEUR;

commit;