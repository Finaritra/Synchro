create or replace view as_produits_type_libelleprix ("ID", "NOM", "DESIGNATION", "PHOTO", "TYPEPRODUIT", "VAL_CALORIE", "CALORIE", "PA", "POIDS", "PU", "PRIORITE") AS 
	select
		prd.id, prd.nom, prd.designation, prd.photo, prd.typeproduit,
		cast(prd.calorie as int),
		case when prd.calorie = '1' then '<b style="color:green">DISPONIBLE</b>'
		else '<b style="color:red">INDISPONIBLE</b>' end, 
		prd.pa, prd.poids, sum(trf.montant) keep (dense_rank first order by trf.dateapplication desc),
		cast(tpd.desce as int)
	from
		as_produits prd,
		as_typeproduit tpd,
		as_prixproduit trf
	where
		prd.typeproduit = tpd.id
		and trf.produit = prd.id
	group by prd.id, prd.nom, prd.designation, 
	prd.photo, prd.typeproduit, prd.calorie, prd.pa, 
	prd.poids,tpd.desce;


	CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_DETS_TYPP_PRIO AS 
  select
      det.id ,
      tab.val as nomtable,
      tp.val as typeproduit,cmd.datecommande,cmd.heureliv,
      prod.nom as produit,
      det.quantite,det.pu,
      (det.quantite*det.pu) as montant,
      det.etat,
      accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
      det.prioriter,
      det.observation,
      tp.id as idtype
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join tableclientvue2 tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;


  	CREATE OR REPLACE FORCE VIEW "VUE_CMD_DTLS_TYPEP_CLOT_PRIO" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE", "PRIORITER", "OBSERVATION", "IDTYPE") AS 
  	select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE", "PRIORITER", "OBSERVATION", "IDTYPE"
	from VUE_CMD_CLT_DETS_TYPP_PRIO where etat = 9 and idtype != 'TPD00001';

	insert into menudynamique values('ASM0000917','Boisson','fa fa-liste','/phobo/pages/module.jsp?but=commande/as-commande-liste-boisson.jsp'||'&'||'table=',1,3,'ASM0000907');

	CREATE OR REPLACE FORCE VIEW "VUE_CMD_DTLS_CLOT_PRIO_BOIS" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE", "PRIORITER", "OBSERVATION", "IDTYPE") AS 
  	select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE", "PRIORITER", "OBSERVATION", "IDTYPE"
	from VUE_CMD_CLT_DETS_TYPP_PRIO where etat = 9 and idtype = 'TPD00001';


	insert into menudynamique values('ASM0000918','Cloture update','fa fa-liste','/phobo/pages/module.jsp?but=commande/as-commande-liste-cloture.jsp'||'&'||'table=',1,3,'ASM0000907');


	CREATE OR REPLACE FORCE VIEW "VUE_CMD_CLT_DTLS_TYPEP_VALIDE" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  	select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE" from vue_cmd_client_details_typeP where etat = 11 order by datecommande asc, heureliv asc;