drop table as_reservation ;

CREATE TABLE "AS_RESERVATION"
   (	"ID" VARCHAR2(100 BYTE) primary key,
	"SOURCE" VARCHAR2(50 BYTE),
	"NBPERSONNE" NUMBER(38,0),
	"DATESAISIE" DATE,
	"HEURESAISIE" VARCHAR2(50 BYTE),
	"DATERESERVATION" DATE,
	"HEUREDEBUTRESERVATION" VARCHAR2(50 BYTE),
	"HEUREFINRESERVATION" VARCHAR2(50 BYTE),
	"IDCLIENT" VARCHAR2(50 BYTE) ,
	 etat number(10)
   );

CREATE SEQUENCE seqreservation
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1061
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getseqreservation
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqreservation.nextval INTO retour FROM dual;
  return retour;
END;
	create table Resa_table
(
    ID            VARCHAR2(100) not null
        constraint Resa_table_PK
            primary key,
    idReservation VARCHAR2(100)
        constraint FK_resa
            references AS_RESERVATION,
    Idtable      VARCHAR2(100)
        constraint FK_table
            references TABLES,
    ETAT NUMBER(10)
);
CREATE SEQUENCE seqresa_table
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1061
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getseqresa_table
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqresa_table.nextval INTO retour FROM dual;
  return retour;
END;
    select *from AS_RESERVATION;

    create or replace view reservationlib as
        select
            reserv.id,
            reserv.SOURCE,
            reserv.NBPERSONNE,
            reserv.DATESAISIE,
            reserv.HEURESAISIE,
            reserv.DATERESERVATION,
            reserv.HEUREDEBUTRESERVATION,
            reserv.HEUREFINRESERVATION,
            clt.TELEPHONE as idclient
        from AS_RESERVATION reserv
        left join AS_CLIENT clt on clt.id=reserv.idClient;
create or replace view reservationlibcomplet as
        select
            reserv.id,
            reserv.SOURCE,
            reserv.NBPERSONNE,
            reserv.DATESAISIE,
            reserv.HEURESAISIE,
            reserv.DATERESERVATION,
            reserv.HEUREDEBUTRESERVATION,
            reserv.HEUREFINRESERVATION,
            reserv.idClient,
            clt.NOM,
        clt.PRENOM,
               clt.SEXE,
               clt.DATENAISSANCE,
               clt.ADRESSE,
               clt.TELEPHONE,
               clt.fb
        from AS_RESERVATION reserv
        left join AS_CLIENT clt on clt.id=reserv.idClient;
		

	

  
create or replace view resa_table_lib as
    select
        resTabl.ID,
        ar.DATERESERVATION,
        ar.HEUREDEBUTRESERVATION,
        ar.HEUREFINRESERVATION,
        ar.NBPERSONNE,
        tab.val as idtable,
        resTabl.ETAT
        from Resa_table resTabl
        left join AS_RESERVATION AR on resTabl.idReservation = AR.ID
        left join  tableclientvue2 tab on tab.id=resTabl.IDTABLE;
		
create or replace view resa_table_libcomplet as
    select
        resTabl.ID,
        resTabl.idReservation,
        ar.DATERESERVATION,
        ar.HEUREDEBUTRESERVATION,
        ar.HEUREFINRESERVATION,
        ar.NBPERSONNE,
        tab.val as idtable,
        resTabl.ETAT
        from Resa_table resTabl
        left join AS_RESERVATION AR on resTabl.idReservation = AR.ID
        left join  tableclientvue2 tab on tab.id=resTabl.IDTABLE;