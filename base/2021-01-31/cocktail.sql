
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0025', 'Mojito', 'Mojito', null,'TPD00001','1','0',null,null,null,null,'7000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRC00025', 'PRDTBOI0025', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0026', 'Virgin Mojito', 'Virgin Mojito', null,'TPD00001','1','0',null,null,null,null,'7000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRC00026', 'PRDTBOI0026', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0027', 'Caipirina', 'Caipirina', null,'TPD00001','1','0',null,null,null,null,'7000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRC00027', 'PRDTBOI0027', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0028', 'Cuba Libre', 'Cuba Libre', null,'TPD00001','1','0',null,null,null,null,'7000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRC00028', 'PRDTBOI0028', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0029', 'Planteur', 'Planteur', null,'TPD00001','1','0',null,null,null,null,'7000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRC00029', 'PRDTBOI0029', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0030', 'Pink', 'Pink', null,'TPD00001','1','0',null,null,null,null,'7000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRC00030', 'PRDTBOI0030', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0031', 'Bora', 'Bora', null,'TPD00001','1','0',null,null,null,null,'7000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRC00031', 'PRDTBOI0031', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTBOI0032', 'Soda Float', 'Soda Float', null,'TPD00001','1','0',null,null,null,null,'7000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRC00032', 'PRDTBOI0032', TO_DATE('10-01-2021','DD-MM-YYYY'), 7000);