create view VUE_CMD_CLT_DETS_REST_RESP as
select
det.id,
tab.nom as nomtable,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
det.prioriter,
det.observation,
tp.id as idtype,
resp.NOM as responsable
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
join AS_RESPONSABLE resp on resp.id=cmd.RESPONSABLE
order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;

create view Vue_cmd_dtls_clot_rest_resp as
select
        id,
       nomtable,
       typeproduit,
       datecommande,
       heureliv,
       restaurant,
       produit,
       quantite,
       pu,
       montant,
       etat,
       acco_sauce,
       prioriter,
       observation,
       idtype,
       responsable

from VUE_CMD_CLT_DETS_REST_RESP
where etat = 9 and idtype != 'TPD00001' and produit != 'Barquette'
order by DATECOMMANDE , HEURELIV ASC,prioriter asc;

create view Vue_cmd_dtls_valide_rest_resp as
select
        id,
        nomtable,
       typeproduit,
       datecommande,
       heureliv,
       restaurant,
       produit,
       quantite,
       pu,
       montant,
       etat,
       acco_sauce,
       responsable
from  VUE_CMD_CLT_DETS_REST_RESP
where etat = 11
order by datecommande asc, heureliv asc;

create view Vue_cmd_dtls_livre_rest_resp as
select
        id,
       nomtable,
       typeproduit,
       datecommande,
       heureliv,
       restaurant,
       produit,
       quantite,
       pu,
       montant,
       etat,
       acco_sauce,
       responsable
from  VUE_CMD_CLT_DETS_REST_RESP
where etat > 0 and etat <= 20;

	
create view Vue_cmd_dtls_bois_rest_resp as
select
        id,
       nomtable,
       typeproduit,
       datecommande,
       heureliv,
       restaurant,
       produit,
       quantite,
       pu,
       montant,
       etat,
       acco_sauce,
       prioriter,
       observation,
       idtype,
       responsable
from VUE_CMD_CLT_DETS_REST_RESP where etat = 9 and (idtype = 'TPD00001' or produit like '%Barquet%');

create view Vue_cmd_dtls_paye_rest_resp as
select
        id,
       nomtable,
       typeproduit,
       datecommande,
       heureliv,
       restaurant,
       produit,
       quantite,
       pu,
       montant,
       etat,
       acco_sauce,
       responsable
from  VUE_CMD_CLT_DETS_REST_RESP
where  etat =40 ;

alter table AS_RESPONSABLE add mdp varchar2(100);