create or replace view FactureVue as select facture.*,'' as detail from facture;

CREATE OR REPLACE VIEW DETAILSFACTURE_GROP as
  select
       idproduit,
       pu ,
       clt.nom as CLIENT,
       f.id,
       m.val as modepaiement,
       f.adresseclient,
       f.datefacture,
       sum(detailsfacture_lib.montant) as montant,
       sum(qte) as qte
from detailsfacture_lib
join facture f on f.id = detailsfacture_lib.idfacture
join modepaiement m on m.id=f.MODEPAIEMENT
join client clt on clt.idclient = f.client
group by idproduit,pu,f.id,
       m.val,
       f.adresseclient,
       clt.nom,
       f.datefacture;