create or replace view ANNULATIONFACTFOURNISSEURLIB as
    select
        anul.id,
        mvtc.DESIGNATION as idobjet,
        anul.DATY,
        anul.MOTIF,
        anul.MONTANT,
        anul.ETAT
        from ANNULATIONFACTUREF anul
        join MVTCAISSE mvtc on mvtc.id=anul.IDOBJET;