  CREATE OR REPLACE VIEW "AS_RECETTE_LIBCOMPLET_CAT"  AS 
  select rec."ID",rec."IDPRODUITS",rec."IDINGREDIENTS",rec."QUANTITE",ing."UNITE",
ing.LIBELLE as libelleingredient,
       prod.libelleproduit,
un.id as idunite,
un.val as valunite,
ing.pu,
ing.categorieingredient
from as_recette rec
join as_ingredients ing on
ing.id=rec.idingredients
left join as_unite un on
un.id=ing.unite
join as_ing_prod prod on prod.id = rec.IDPRODUITS
order by rec.id asc;
 

 CREATE OR REPLACE FORCE VIEW "ETATFACTUREMONTANT" ("ID", "DATY", "IDFOURNISSEUR", "IDTVA", "MONTANTTTC", "REMARQUE", "DATEEMISSION", "DESIGNATION", "IDDEVISE", "NUMFACT", "DATYECHEANCE", "ETAT", "MONTANTPAYE", "RESTE") AS 
  select f."ID",f."DATY",f."IDFOURNISSEUR",f."IDTVA",cast (f."MONTANTTTC" as NUMBER(18,2)) as MONTANTTTC,f."REMARQUE",f."DATEEMISSION",f."DESIGNATION",f."IDDEVISE",f."NUMFACT",f."DATYECHEANCE",f."ETAT" ,cast (nvl(op.montant,0) as NUMBER(18,2)) as montantPaye,cast ((f.montantTTC-nvl(op.montant,0)) as NUMBER(18,2)) as reste from FACTUREFOURNISSEURLETTREMTT f,ORDONNERPAYEMENTGROUPEFACT op
where f.ID=op.DED_ID(+);
 



  CREATE OR REPLACE VIEW "FACTUREFOURNISSEURCOMPTA" ("ID", "DATY", "IDFOURNISSEUR", "IDTVA", "MONTANTTTC", "REMARQUE", "DATEEMISSION", "DESIGNATION", "IDDEVISE", "NUMFACT", "RESP", "DATYECHEANCE", "ETAT", "IDFACTUREFOURNISSEUR") AS 
  select 
    ff.id,
    ff.daty,
    ff.idfournisseur,
    ff.montantttc as idtva,
    (select sum (pu*qte) from detailsfacturefournisseur where ff.id=idmere) as montantttc,
    ff.remarque,
    ff.dateemission,
    ff.designation,
    ff.iddevise,
    ff.numfact,
    ff.resp,
    ff.datyecheance,
    ff.etat,
    ff.idfacturefournisseur
from facturefournisseur ff;
