insert into as_produits values('LIVR1','Livraison','Livraison','THB-65cl.jpg','TPD00001',
1,0,3000,'INGRBS0026','',99);

insert into as_produits values('LIVR3','Livraison Lavitra','Livraison Lavitra','THB-65cl.jpg','TPD00001',
1,0,5000,'INGRBS0026','',99);

insert into as_prixproduit values('PLIVR1','LIVR1','25/09/2019',3000,'');




insert into as_produits values('LIVR2','Livraison domicile','Livraison domicile','THB-65cl.jpg','TPD00001',
1,0,4000,'INGRBS0026','',99);

insert into as_prixproduit values('PLIVR2','LIVR2','25/09/2019',4000,'');


create or replace view as_produits_avecPrix as
select p.id,p.nom,p.designation,p.photo,
p.typeproduit,p.calorie,p.poids,prix.MONTANT as pa,p.idingredient,
p.idsouscategorie,p.numeroplat from as_produits p,as_prixproduit prix
where p.id=prix.PRODUIT;


CREATE OR REPLACE VIEW VUE_CMD_DTLS_BOIS_REST_RESP
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE)
AS 
select

        id,

       nomtable,

       typeproduit,

       datecommande,

       heureliv,

       restaurant,

       produit,

       quantite,

       pu,

       montant,

       etat,

       acco_sauce,

       prioriter,

       observation,

       idtype,

       responsable

from VUE_CMD_CLT_DETS_REST_RESP where etat = 9 and (idtype = 'TPD00001' or produit like '%Barquet%')
and produit not in (select p2.nom from as_produits p2 where p2.id like 'LIVR%');






















create or replace view commandeLivraisonMax as
select * from commandelivraison l1 where l1.id=
(select max(l2.id) from commandelivraison l2 where l2.idclient=l1.idClient);

CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_ALIVRER_PT
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE, IDPOINT, ADRESSE)
AS 
select
 liv."ID",liv."NOMTABLE",liv."TYPEPRODUIT",liv."DATECOMMANDE",liv."HEURELIV",liv."RESTAURANT",liv."PRODUIT",liv."QUANTITE",liv."PU",liv."MONTANT",liv."ETAT",liv."ACCO_SAUCE",liv."PRIORITER",liv."OBSERVATION",liv."IDTYPE",liv."RESPONSABLE",liv."IDCL",liv."DATYCL",liv."TELEPHONE",liv."HEURECL",liv."IDMERE",
 pt.VAL as idpoint,
cl.adresse
 from VUE_CMD_LIV_DTLS_ALIVRER liv
 join commandeLivraisonMax cl on cl.idclient=liv.idmere
 left join point pt on pt.id=cl.idPoint;


 CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_ALIVRER_PT_ID
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE, IDPOINT, ADRESSE)
AS 
select
 liv."ID",liv."NOMTABLE",liv."TYPEPRODUIT",liv."DATECOMMANDE",liv."HEURELIV",liv."RESTAURANT",liv."PRODUIT",liv."QUANTITE",liv."PU",liv."MONTANT",liv."ETAT",liv."ACCO_SAUCE",liv."PRIORITER",liv."OBSERVATION",liv."IDTYPE",liv."RESPONSABLE",liv."IDCL",liv."DATYCL",liv."TELEPHONE",liv."HEURECL",liv."IDMERE",
 cl.idpoint as idpoint,
cl.adresse
from VUE_CMD_LIV_DTLS_ALIVRER liv
join commandeLivraisonMax cl on cl.idclient=liv.idmere;


CREATE OR REPLACE VIEW COMMANDELIVRAISONLIB
(ID, IDCLIENT, ADRESSE, IDPOINT, DATY, 
 HEURE, FRAIS, ETAT)
AS 
select

            cmdliv.id,

            cmdliv.idclient,

            cmdliv.adresse,

            pt.VAL as idpoint,

            cmdliv.daty,

           cmdliv.heure,

           cmdliv.frais,

           cmdliv.etat



        from commandeLivraison cmdliv

        left join as_commandeclient cmd on cmd.id=cmdliv.idclient

        left join POINT pt on pt.id=cmdliv.idPoint
/


CREATE OR REPLACE VIEW VUE_CMD_DTLS_CLOT_REST_RESP
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE)
AS 
select
id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
prioriter,
observation,
idtype,
idmere as responsable
from VUE_CMD_CLT_DETS_REST_RESP
where etat = 9 and idtype != 'TPD00001' and produit != 'Barquette'
order by DATECOMMANDE , HEURELIV ASC,prioriter asc
/


CREATE OR REPLACE VIEW VUE_CMD_DTLS_BOIS_REST_RESP
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE)
AS 
select
        id,
       nomtable,
       typeproduit,
       datecommande,
       heureliv,
       restaurant,
       produit,
       quantite,
       pu,
       montant,
       etat,
       acco_sauce,
       prioriter,
       observation,
       idtype,
       idmere as responsable
from VUE_CMD_CLT_DETS_REST_RESP where etat = 9 and (idtype = 'TPD00001' or produit like '%Barquet%')
and produit not in (select p2.nom from as_produits p2 where p2.id like 'LIVR%')
/

CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, VENTE, ETAT, SECT, 
 RESTAURANT, PRENOM)
AS 
select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, tbl.TELEPHONE as responsable, tbl.nom as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect, cmd.point , tbl.prenom
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    as_client tbl,
    vente
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur
    and cmd.client = tbl.id
    and cmd.vente=vente.id(+);


    CREATE OR REPLACE VIEW VUE_CMD_DTLS_VALIDE_REST_RESP
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, RESPONSABLE, TELEPHONE, IDCLIENT)
AS 
select
 id,
 nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
idmere as responsable,
TELEPHONE,
IDCLIENT
from  VUE_CMD_CLT_DETS_REST_RESP
where etat = 11
order by datecommande asc, heureliv asc;



