create or replace view recettemontant as 
select 
rec.*,
cast(0 as number(12,2)) as qteav,
cast(0 as number(12,2)) as qtetotal
from as_recette rec;

create or replace view as_ingredients_ff as
select 
ing.ID,
ing.LIBELLE,
ing.SEUIL,
ing.UNITE,
ing.QUANTITEPARPACK,
NVL(ff.PU,0) as PU,
ing.ACTIF,
ing.PHOTO,
ing.CALORIE,
ing.DURRE,
ing.COMPOSE,
ing.CATEGORIEINGREDIENT,
ing.IDCATEGORIE,
ff.idfournisseur,
ff.daty
from as_ingredients_lib ing
left join FACTUREFOURNISSEURGLOBALMAX ff on 
ing.id=ff.idingredient; 

--probleme anle supprimer
create table as_restriction(
     id varchar(100),
     idrole varchar(100),
    idaction varchar(100),
     tablename varchar(500),
     autorisation varchar(100),
     description varchar(900),
    iddirection varchar(100)
    );

