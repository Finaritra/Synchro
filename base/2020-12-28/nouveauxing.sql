insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000698', 'Composants Tsock Special Prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000699', 'Composants Tsock Sans Porc Prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients 
(ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) 
VALUES
('IG000700', 'Composants bol renversé', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients 
(ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) 
VALUES
('IG000701', 'Composants riz cantonnais Prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients 
(ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) 
VALUES
('IG000702', 'Assiette Tsa Siou Prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients 
(ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) 
VALUES
('IG000704', 'Composants Van tan Prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients 
(ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) 
VALUES
('IG000705', 'Composants crepes poulet tsa siou Prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES ('IG000706', 'Misao special tsasiou Prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000707', 'Misao special tsasiou prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000708', 'Misao poulet prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000709', 'Van tan garnie(sans mine) prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000710', 'Van tan mine garnie prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000711', 'Pho sans porc prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000712', 'Pho prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000713', 'Spaghetti carbonara prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000714', 'Spaghetti bolognaise prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000715', 'Soupe Delice Pate Jaune prepare', 'UNT00005', 1, 1, 'CI00007');

insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IG000716', 'Assiette Boulette de poisson prepare', 'UNT00005', 1, 1, 'CI00007');

------A insérer
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES ('IG000718', 'Composants crepe poisson', 'UNT00005', 1, 1, 'CI00007');


Insert into AS_PRODUITS (ID,NOM,DESIGNATION,PHOTO,TYPEPRODUIT,CALORIE,POIDS,PA,IDINGREDIENT,IDSOUSCATEGORIE,NUMEROPLAT,PRIXL,POIDSPRECUIT) values ('APS000328','Misao special crevette','Misao special crevette','2328460628-12-2020','ASTP000005','1','0',null,null,null,null,'0','0');

Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001060','APS000328','IG000707','1',null);
Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001068','APS000328','ING000208','1',null);
Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001069','APS000328','ING000209','1',null);
Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001070','APS000328','ING000210','1',null);
Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001065','APS000328','INGS000618','15',null);
Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001064','APS000328','INGS000629','10',null);
Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001061','APS000328','INGS000662','20',null);
Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001067','APS000328','INGS000674','60',null);
Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001062','APS000328','INGS000727','15',null);
Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001063','APS000328','INGS000746','1',null);
Insert into AS_RECETTE (ID,IDPRODUITS,IDINGREDIENTS,QUANTITE,UNITE) values ('RTT001066','APS000328','INGS000752','2',null);
