CREATE OR REPLACE VIEW FABRICATION_LIB AS 
  select
            fab.id,
            fab.Daty,
            ing.LIBELLE as ingredient,
            fab.qte,
            point.val AS depot,
            fab.Remarque,
            fab.Etat
        from fabrication fab
        join AS_INGREDIENTS ing on ing.ID=fab.Ingredient
        JOIN point ON fab.depot = point.ID;

  create or replace view INVENTAIREDETAILSLIBINVENTCPS as
select
            invdet.id,
           invDet.idmere,
           ing.LIBELLE as idingredient,
           invDet.idUser,
           invDet.quantiter,
            im.depot as explication,
           im.etat,
           catIng.VAL as idcategorieingredient,
           im.DATY,
           invDet.IDINGREDIENT as IDING,
           uni.val as unite,
           ing.compose,
           catIng.ID as IDCATING,
            p.VAL as POINT
        from inventairedetails invDet
        left join AS_INGREDIENTS ing on ing.id=invDet.idingredient
        left join CATEGORIEINGREDIENT catIng on catIng.ID=invDet.IDCATEGORIEINGREDIENT
        join inventaireMere im on invDet.idmere=im.id
        join as_unite uni on ing.unite=uni.id
        left join point p on p.id=im.DEPOT
        where im.etat>=11
/
