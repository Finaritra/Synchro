create view Vue_etat_de_stock_report as (
	select as_ingredients.id as idingredient, as_mvt_stock.depot as idmagasin, as_mvt_stock.depot, as_ingredients.calorie as type, (entree - sortie) as theorique, 0 as inventaire, null as observation from as_ingredients
	join as_mvt_stock_fille on as_ingredients.id = as_mvt_stock_fille.ingredients
	join as_mvt_stock on as_mvt_stock_fille.idmvtstock = as_mvt_stock.id
);