insert into as_ingredients (id,libelle,seuil,unite,quantiteparpack,pu,actif,photo,calorie,durre,compose,categorieingredient)
select * from ampasamadinika.as_ingredients p where p.id not in (select a.id from as_ingredients a);


insert into as_produits (id,nom,designation,photo,typeproduit,calorie,poids,pa,idingredient,idsouscategorie,numeroplat)
select * from ampasamadinika.as_produits p where p.id not in (select a.id from as_produits a);

insert into as_recette (id,idproduits,idingredients,quantite,unite)
select * from ampasamadinika.as_recette p where p.id not in (select a.id from as_recette a);

update as_recette loc set loc.quantite=(select a.quantite from ampasamadinika.as_recette a where a.id=loc.id);


insert into fournisseurproduits (id,nom,prenom,sexe,datenaissance,adresse,telephone,fb)
select * from ampasamadinika.fournisseurproduits f where f.id not in(select id from fournisseurproduits);

insert into facturefournisseur (id,daty,idfournisseur,idtva,montantttc,remarque,dateemission,designation,iddevise,numfact,resp,datyecheance,etat,idfacturefournisseur,entite)
select * from ampasamadinika.facturefournisseur a where a.id not in (select id from facturefournisseur) and daty<='02/01/2020';


insert into detailsfacturefournisseur (id,idingredient,qte,pu,idmere,compte)
select * from ampasamadinika.detailsfacturefournisseur mer where mer.idmere in
(select id from facturefournisseur where daty<='02/01/2020');