CREATE SEQUENCE seqdetailsfacturefournisseur
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getseqdfacturefournisseur
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqdetailsfacturefournisseur.nextval INTO retour FROM dual;
  return retour;
END;
/

create or replace view detailsfflib as
select df.id,
df.qte,
df.pu,
df.idmere,
ing.libelle as idingredient
from DETAILSFACTUREFOURNISSEUR df join
as_ingredients ing on ing.id=df.idingredient;