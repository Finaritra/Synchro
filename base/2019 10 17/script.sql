update AS_TYPEPRODUIT
set  DESCE = '12'
where id = 'TPD00004';
update AS_TYPEPRODUIT
set  DESCE = '2'
where id = 'ASTP000009';
update AS_TYPEPRODUIT
set  DESCE = '7'
where id = 'ASTP000007';
update AS_TYPEPRODUIT
set  DESCE = '3'
where id = 'ASTP000005';
update AS_TYPEPRODUIT
set  DESCE = '4'
where id = 'ASTP000001';
update AS_TYPEPRODUIT
set  DESCE = '8'
where id = 'ASTP000002';
update AS_TYPEPRODUIT
set  DESCE = '9'
where id = 'ASTP000003';
update AS_TYPEPRODUIT
set  DESCE = '6'
where id = 'ASTP000004';
update AS_TYPEPRODUIT
set  DESCE = '5'
where id = 'ASTP000006';
update AS_TYPEPRODUIT
set  DESCE = '10'
where id = 'ASTP000008';
update AS_TYPEPRODUIT
set  DESCE = '11'
where id = 'ASTP000010';



 CREATE OR REPLACE FORCE VIEW "AS_INGREDIENTS_LIBELLE" ("ID", "LIBELLE", "SEUIL", "UNITE", "QUANTITEPARPACK", "PU") AS 
  select as_ingredients.id, as_ingredients.libelle, as_ingredients.seuil, unite.val as unite, as_ingredients.quantiteparpack, as_ingredients.pu
from as_ingredients join unite on as_ingredients.unite = unite.id ;
 

CREATE OR REPLACE FORCE VIEW "AS_ETATSTOCK_TERM" ("ID", "LIBELLE", "SEUIL", "UNITE", "QUANTITEPARPACK", "PU") AS 
  select as_etatstock.ingredients as id, as_ingredients.libelle,
case
  when sum(as_etatstock.entree) - sum(as_etatstock.sortie) < 0 then 0
  else  sum(as_etatstock.entree) - sum(as_etatstock.sortie)
 end as seuil,
 unite.val as unite, as_ingredients.quantiteparpack, as_ingredients.pu
 from as_etatstock
  join as_ingredients on as_etatstock.ingredients = as_ingredients.id
  join unite on as_etatstock.unite = unite.id
 group by as_etatstock.ingredients, as_ingredients.libelle,
 unite.val, as_ingredients.quantiteparpack, as_ingredients.pu;create view vue_achat_fournisseur as
	select 
		besoin.idingredient, ingr.idingredient as ingredient,
		besoin.quantite,besoin.idunite,ingr.idunite as unite,
		ingr.prixunitaire,besoin.daty,besoin.iduser,ingr.idfournisseur,ingr.fournisseur
	from besoinquotidien besoin 
	join vue_ingredient_fournisseur ingr on ingr.id = besoin.idingredient
	
create or replace FUNCTION getseq_fournisseur RETURN NUMBER IS retour NUMBER;
	BEGIN
		SELECT seq_ffournisseur.nextval INTO retour FROM dual;
		return retour;
	END; 
	
CREATE SEQUENCE  SEQ_ACHAT  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;

create or replace FUNCTION getseq_achat RETURN NUMBER IS retour NUMBER;
	BEGIN
		SELECT SEQ_ACHAT.nextval INTO retour FROM dual;
		return retour;
	END;

CREATE TABLE ACHAT
   (	"ID" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"IDINGREDIENT" VARCHAR2(50 BYTE), 
	"QUANTITE" NUMBER, 
	"IDUNITE" VARCHAR2(50 BYTE), 
	"PRIXUNITAIRE" NUMBER, 
	"MONTANT" NUMBER, 
	"DATY" DATE, 
	"IDUSER" NUMBER, 
	"IDFOURNISSEUR" VARCHAR2(50 BYTE), 
	 CONSTRAINT "ACHAT_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE, 
	 CONSTRAINT "ACHAT_FK2" FOREIGN KEY ("IDUNITE")
	  REFERENCES "SAKAFO"."UNITE" ("ID") ENABLE, 
	 CONSTRAINT "ACHAT_FK3" FOREIGN KEY ("IDUSER")
	  REFERENCES "SAKAFO"."UTILISATEUR" ("REFUSER") ENABLE, 
	 CONSTRAINT "ACHAT_FK4" FOREIGN KEY ("IDFOURNISSEUR")
	  REFERENCES "SAKAFO"."FFOURNISSEUR" ("IDFOURNISSEUR") ENABLE, 
	 CONSTRAINT "ACHAT_FK1" FOREIGN KEY ("IDINGREDIENT")
	  REFERENCES "SAKAFO"."AS_INGREDIENTS" ("ID") ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;	