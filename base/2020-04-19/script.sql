CREATE OR REPLACE VIEW AS_MVTSTOCK_FILLE_LIB
(ID, IDMVTSTOCK, INGREDIENTS, ENTREE, SORTIE, 
 SOLDE, PU, IDMVT)
AS 
select mvt.id, mvt.idmvtstock, igd.libelle as ingredients, mvt.entree, mvt.sortie,
mvt.solde, mvt.pu, mvt.idmvt from as_mvt_stock_fille mvt, as_ingredients igd where mvt.ingredients = igd.id(+);




insert into menudynamique values ('ASMST00410','Liste details', 'fa fa-plus','/phobo/pages/module.jsp?but=stock/mvtStock/mvtStockFilleComplet-liste.jsp',5,3,'ASM0000803');
commit;

create or replace view as_mvt_stockLibelle as 
select mvt.id,mvt.daty,mvt.designation,mvt.typemvt,mvt.observation,mvt.depot,mvt.typebesoin,mvt.etat,mvt.fournisseur,
mvt.beneficiaire,det.PRODUIT as numbc,mvt.numbs
 from as_mvt_stock mvt,as_detailscommande_lib det
where mvt.NUMBC=det.id(+);

create or replace view MvtStockDetLibComplet as 
select det.*,
  mere.DATY,
  mere.DESIGNATION,
  mere.TYPEMVT,
  mere.OBSERVATION,
  mere.DEPOT,
  mere.TYPEBESOIN,
  mere.ETAT,
  mere.FOURNISSEUR,
  mere.BENEFICIAIRE,
  mere.NUMBC,
  mere.NUMBS 
from  AS_MVTSTOCK_FILLE_LIB det, as_mvt_stockLibelle mere where det.IDMVTSTOCK=mere.id(+);

CREATE OR REPLACE VIEW AS_RECETTE_LIBCOMPLET
(ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, UNITE, 
 LIBELLEINGREDIENT, LIBELLEPRODUIT, IDUNITE, VALUNITE, PU)
AS 
select rec."ID",rec."IDPRODUITS",rec."IDINGREDIENTS",rec."QUANTITE",ing."UNITE",
ing.LIBELLE as libelleingredient,
       prod.libelleproduit,
un.id as idunite,
un.val as valunite,
ing.pu
from as_recette rec
join as_ingredients ing on
ing.id=rec.idingredients
left join as_unite un on
un.id=ing.unite
join as_ing_prod prod on prod.id = rec.IDPRODUITS
order by rec.id asc;
