CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_ALIVRER_PT_ID
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE, IDPOINT, ADRESSE)
AS 
select

            liv.*,

            cl.idpoint as idpoint,

           cl.adresse

        from VUE_CMD_LIV_DTLS_ALIVRER liv

        join commandeLivraison cl on cl.idclient=liv.idmere

        
/

insert into livreur values ('LIV001','Donne','0337408645',1);
insert into livreur values ('LIV002','Fidy','0336821901',1);
insert into livreur values ('LIV004','Tanjona','0346604962',1);
insert into livreur values ('LIV005','Naina','0337212147',1);
insert into livreur values ('LIV006','Mimi','0339052300',1);
insert into livreur values ('LIV007','Libre 1','-',1);
insert into livreur values ('LIV008','Libre 2','-',1);
insert into livreur values ('LIV009','Libre 3','-',1);
insert into livreur values ('LIV010','Libre 4','-',1);
commit;

alter table livraison drop constraint LIVRAISON_MERE_FK;


CREATE OR REPLACE VIEW LIVRAISONLIB
(ID, DATY, HEURE, IDCOMMNADEMERE, IDPOINT, 
 ADRESSE, IDLIVREUR, ETAT)
AS 
select

            liv.id,

           liv.daty,

           liv.heure,

           liv.idcommnademere,

            pt.VAL as idpoint,

           liv.adresse,

           l.nom||'--'|| l.contact as idlivreur,

           liv.etat



        from livraison liv

        join AS_DETAILSCOMMANDE dc on liv.idcommnademere = dc.ID

        join POINT pt on pt.ID=liv.idpoint

        join livreur l on liv.idlivreur = l.id
/

CREATE OR REPLACE VIEW LIVRAISONLIBValide
(ID, DATY, HEURE, IDCOMMNADEMERE, IDPOINT, 
 ADRESSE, IDLIVREUR, ETAT)
AS 
select * from  LIVRAISONLIB liv where liv.etat>=1
/

CREATE OR REPLACE VIEW LIVRAISONLIBVALIDECOMPLET
AS 
select

            liv.id,

           liv.daty,

           liv.heure,

           dc.produit||'--'||dc.IDACCOMPAGNEMENTSAUCE as idcommnademere,

            pt.VAL as idpoint,

           liv.adresse,

           l.nom||'--'|| l.contact as idlivreur,

           liv.etat,
           clt.nom||'--'||clt.telephone as client,
           (dc.quantite*dc.pu) as montant


        from livraison liv

        join AS_DETAILSCOMMANDELIB dc on liv.idcommnademere = dc.ID

        join POINT pt on pt.ID=liv.idpoint
        join as_commandeclient cmd on dc.idmere=cmd.id
        join as_client clt on cmd.client=clt.id
        join livreur l on liv.idlivreur = l.id
        where liv.etat>=1
/

CREATE TABLE as_restriction
(
  id            VARCHAR2(100),
  idrole        VARCHAR2(100),
  idaction      VARCHAR2(100),
  tablename     VARCHAR2(100),
  autorisation  VARCHAR2(100),
  descritpion   VARCHAR2(100),
  iddirection   VARCHAR2(100)
);


ALTER TABLE as_restriction ADD (
  CONSTRAINT as_restriction_PK
 PRIMARY KEY
 (id));

insert into point values ('-','-','-');
commit;

create or replace view pointtrie as select * from point order by id asc;


