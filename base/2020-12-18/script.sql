
insert into menudynamique(id, libelle, icone, href, rang, niveau) values ('MEN0008', 'Dashboard', 'fa fa-bar-chart', '#', 4, 1);
insert into menudynamique(id, libelle, icone, href, rang, niveau, id_pere) values ('MEN0009', 'Efficacit&eacute; p&eacute;riodique', 
'fa fa-line-chart', '#', 1, 2, 'MEN0008');
insert into menudynamique(id, libelle, icone, href, rang, niveau, id_pere) values ('MEN0010', 'Classement des &eacute;quipes',
'fa fa-th', '#', 2, 2, 'MEN0008');