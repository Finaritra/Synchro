create view Vue_cmd_dtls_clot_voaray as
select
id,
nomtable,
typeproduit,
datecommande,
heureliv,
restaurant,
produit,
quantite,
pu,
montant,
etat,
acco_sauce,
prioriter,
observation,
idtype,
idmere as responsable
from VUE_CMD_CLT_DETS_REST_RESP
where (etat = 9 or etat =10) and idtype != 'TPD00001' and produit != 'Barquette'
order by DATECOMMANDE , HEURELIV ASC,prioriter asc
/