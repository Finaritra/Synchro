CREATE OR REPLACE FORCE VIEW AS_DETAILSCOMMANDE_groupe
(
   ID,
   IDMERE,
   PRODUIT,
   IDACCOMPAGNEMENT,
   PHOTO,
   IDCLIENT,
   QUANTITE,
   PU,
   REMISE,
   OBSERVATION,
   ETAT,
   TYPECOMMANDE
)
AS
     SELECT max(dt.id),
            dt.idmere,
            dt.PRODUIT,
            dt.IDACCOMPAGNEMENT,
            dt.PHOTO,
            dt.IDCLIENT,
            sum(dt.QUANTITE),
            avg(dt.PU),
            sum(dt.QUANTITE) * avg(dt.PU),
            '-' as OBSERVATION,
            dt.ETAT,
            dt.typecommande
       FROM AS_DETAILSCOMMANDE_LIB_SAUCE2 dt
       group by dt.idmere,dt.produit,dt.IDACCOMPAGNEMENT,
            dt.PHOTO,
            dt.IDCLIENT,dt.etat,dt.typecommande
       ;