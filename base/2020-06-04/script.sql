CREATE OR REPLACE VIEW FACTUREFOURNISSEURGLOBALMAX
(IDFOURNISSEUR, PU, IDINGREDIENT, DATY)
AS 
(select max(idfournisseur),avg(pu),idingredient,daty
    from facturefournisseurglobal factF
    where factF.daty=(select max(daty) from facturefournisseurglobal factfI  where factfI.IDINGREDIENT=factF.IDINGREDIENT)
    group by idingredient,daty
    )
/

CREATE OR REPLACE VIEW AS_RECETTEPRODUITSCOMPOSE
(RANG, ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, 
 COMPOSE, MERE, QTEAV)
AS 
select  level rang,rec.id, idproduits,idingredients,rec.quantite,compose,CONNECT_BY_ROOT idproduits mere,
nvl(to_number(SUBSTR((SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1))), 
(INSTR(SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1)), '/', -1))+1)),1) 
as qteAv
 from as_recettecompose rec where rec.compose=0
  start with idproduits in (select p.id from as_produits p)
  connect by prior idingredients = idproduits
/


create or replace view as_recPrdCompFF as
  select r.id,r.IDINGREDIENTS,r.QUANTITE*r.QTEAV as quantite,r.mere,ing.PU,cast ((ing.PU*r.QUANTITE*r.QTEAV) as number(10,2)) as montant
   from as_recetteProduitsCompose r,as_ingredients_ff ing
  where r.IDINGREDIENTS=ing.id;
  
  create or replace view as_produitsPU as 
  select r.mere,sum(r.montant) as montant from as_recPrdCompFF r group by r.mere;


  CREATE OR REPLACE VIEW AS_DETCOMVALIDEREV
AS 
select det.PRODUIT,rev.MONTANT as revient,det.QUANTITE,det.IDMERE,det.id from as_detailscommande det,as_produitsPU rev where det.etat>0 and det.PRODUIT=rev.MERE(+)
/
  
  CREATE OR REPLACE VIEW MONTANTREVIENTCOMMANDE
(ID, revient)
AS 
select idmere as id, sum(quantite*revient) as revient from AS_DETCOMVALIDEREV group by idmere
/


CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE2
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, ETAT, COURSIER, MONTANT, 
 SECTEUR, VENTE, RESTAURANT, PRENOM,revient)
AS 
select
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT" || ' ' || cmd."RESPONSABLE" as client ,cmd."TYPECOMMANDE",
    cmd."REMARQUE", mcmd.ADRESSE as "ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",       
       case  cmdlivre.nombrelivre WHEN cmdlivre.totalcommande then 20
       else (select min(etat) from as_detailscommande det where det.idmere=cmd.id and det.etat>0)
       end  "ETAT" ,  livr.idlivreur as coursier, mtnt.montant, livr.idlivreur as secteur ,cmd.vente,cmd.restaurant,cmd.prenom,cast(rev.revient as number(10,2)) as revient
from
    as_commandeclient_libelle cmd,
    montantparcommande mtnt,
    commandelivraisonMax mcmd,
    AS_NOMBRECOMMANDELIVRE  cmdlivre,
    livraisonLibMereMax livr,
    MONTANTREVIENTCOMMANDE rev
where
    mtnt.id = cmd.id 
    and cmd.ID=mcmd.idclient(+)
    and cmd.id=cmdlivre.idmere(+)
    and cmd.id=livr.IDMERE(+)
    and cmd.id=rev.id(+)
    ORDER BY datecommande asc,  HEURELIV asc
/

CREATE OR REPLACE VIEW AS_DETAILS_COMMANDE_ETAT_TPPRD
(ID, IDMERE, PRODUIT, QUANTITE, PU, 
 REMISE, OBSERVATION, IDPRODUIT, ETAT, TYPEPRODUIT,revient)
AS 
select de."ID",de."IDMERE",de."PRODUIT",de."QUANTITE",de."PU",de."REMISE",de."OBSERVATION",de."IDPRODUIT",de."ETAT",
p.typeproduit, cast(rev.REVIENT as number(10,2)) as revient
from AS_DETAILS_COMMANDE_ETAT de,AS_DETCOMVALIDEREV rev, as_produits p 
where de.idproduit=p.id and de.id=rev.id
/