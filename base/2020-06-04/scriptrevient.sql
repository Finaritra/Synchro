
alter table as_detailscommande add revient NUMBER(10,2);

create or replace view as_produitsrevient as 
select 
pr.*,
CAST(rev.montant as NUMBER(10,2))  as revient
from As_produits_type_libelleprix pr
left join as_produitsPU rev on
pr.id=rev.mere;


CREATE OR REPLACE VIEW "DETAILSCOMMANDECLIENT" AS 
  select
det."ID",det."IDMERE",det."PRODUIT",det."QUANTITE",det."PU",det."REMISE",det."OBSERVATION",det."ETAT",det."IDACCOMPAGNEMENTSAUCE",det."PRIORITER",
clt.id as idclient,
det."REVIENT"
from AS_DETAILSCOMMANDE det
join AS_COMMANDECLIENT cml on cml.id=det.IDMERE
join AS_CLIENT clt on clt.id=cml.CLIENT;



CREATE OR REPLACE VIEW "VUE_CMD_CLIENT_DETAILS_TYPEP" AS 
  select
      det.id ,
      tab.val as nomtable,
      tp.val as typeproduit,cmd.datecommande,cmd.heureliv,
      prod.nom as produit,
      det.quantite,det.pu,
      (det.quantite*det.pu) as montant,
      det.etat,
      accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
      det.revient
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join tableclientvue2 tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;
 
