create view  AS_DETAILSCOMMANDELIB as
select
       detCom.id,
       detCom.IDMERE,
       prd.NOM as PRODUIT,
       detCom.QUANTITE,
       detCom.pu,
       detCom.REMISE,
       detCom.OBSERVATION,
       detCom.ETAT,
       acc.IDSAUCE||' '||acc.IDACCOMPAGNEMENT as IDACCOMPAGNEMENTSAUCE,
       detCom.PRIORITER

    from AS_DETAILSCOMMANDE detCom
    join AS_PRODUITS prd on prd.id=detCom.PRODUIT
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB acc on acc.id=detCom.IDACCOMPAGNEMENTSAUCE;
	
alter table livraison modify idcommnademere  constraint livraison_s_fk references AS_DETAILSCOMMANDE;

create or replace view livraisonlib as
    select
            liv.id,
           liv.daty,
           liv.heure,
           liv.idcommnademere,
            pt.VAL as idpoint,
           liv.adresse,
           l.nom as idlivreur,
           liv.etat

        from livraison liv
        join AS_DETAILSCOMMANDE dc on liv.idcommnademere = dc.ID
        join POINT pt on pt.ID=liv.idpoint
        join livreur l on liv.idlivreur = l.id;

create view commandeLivraisonlibp as
    select            cd.id,
            cd.IDMERE,
            comLi.id as idlivraison1,
            prod.NOM as produit,
            cd.QUANTITE,
            cd.pu,
            cd.REMISE,
            cd.OBSERVATION,
            cd.etat,
            lib.IDSAUCE ||' '||lib.IDACCOMPAGNEMENT as IDACCOMPAGNEMENTSAUCE,
            cd.PRIORITER,
            comLi.idclient,
            comLi.adresse,
            comLi.idPoint,
            comLi.daty,
            comLi.heure,
            comLi.frais


        from commandeLivraison comLi
        join AS_CLIENT AC on comLi.idclient = AC.ID
        join AS_COMMANDECLIENT cmcli on cmcli.CLIENT=ac.id
        join AS_DETAILSCOMMANDE cd on cd.IDMERE=cmcli.id
        left join AS_ACCOMPAGNEMENT_SAUCE_lib lib on lib.id=cd.IDACCOMPAGNEMENTSAUCE
        left join AS_PRODUITS prod on prod.id=cd.PRODUIT;

create or replace view commandeLivraisonlibpoint as
select
       p.* ,
       POINT.VAL,
       POINT.DESCE
       from commandeLivraisonlibp  p, point where p.idpoint = point.id(+) and p.idpoint is null;