CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_DETS_REST_RESP as 
 select
det.id,
tab.nom as nomtable,
tab.telephone,
tab.id as idclient,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
det.prioriter,
det.observation,
tp.id as idtype,
resp.NOM as responsable
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
join AS_RESPONSABLE resp on resp.id=cmd.RESPONSABLE
order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;
 
 
CREATE OR REPLACE FORCE VIEW VUE_CMD_LIV_DTLS_TOUS as 
select
        vccdrr.id,
       vccdrr.nomtable,
       vccdrr.typeproduit,
       vccdrr.datecommande,
       vccdrr.heureliv,
       vccdrr.restaurant,
      vccdrr.produit,
       vccdrr.quantite,
       vccdrr.pu,
       vccdrr.montant,
      vccdrr.etat,
       vccdrr.acco_sauce,
       vccdrr.prioriter,
       vccdrr.observation,
       vccdrr.idtype,
       vccdrr.responsable,
	   cl.id as idcl,
	   cl.daty as datycl,
	   vccdrr.telephone,
     cl.HEURE as heurecl
from VUE_CMD_CLT_DETS_REST_RESP vccdrr join commandelivraison cl on vccdrr.idclient = cl.idclient
where vccdrr.idtype != 'TPD00001' and vccdrr.produit != 'Barquette'
order by vccdrr.DATECOMMANDE , vccdrr.HEURELIV ASC, vccdrr.prioriter asc;

CREATE OR REPLACE FORCE VIEW VUE_CMD_LIV_DTLS_ACLOT as 
select * from VUE_CMD_LIV_DTLS_TOUS where etat = 8;

CREATE OR REPLACE FORCE VIEW VUE_CMD_LIV_DTLS_ANNULERPAY as 
select * from VUE_CMD_LIV_DTLS_TOUS where etat = 6;


CREATE OR REPLACE FORCE VIEW VUE_CMD_LIV_DTLS_ALIVRER as 
select * from VUE_CMD_LIV_DTLS_TOUS where etat = 12;