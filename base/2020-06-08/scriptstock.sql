alter table FACTUREFOURNISSEUR add ENTITE varchar2(50) default 'defaut' constraint entite_tfk references POINT;
CREATE OR REPLACE  VIEW "INVENTAIREDETAILSLIBINVENTCPS" ("ID", "IDMERE", "IDINGREDIENT", "IDUSER", "QUANTITER", "EXPLICATION", "ETAT", "IDCATEGORIEINGREDIENT", "DATY", "IDING", "UNITE") AS 
  select
            invdet.id,
           invDet.idmere,
           ing.LIBELLE as idingredient,
           invDet.idUser,
           invDet.quantiter,
            im.depot as explication,
           im.etat,
           catIng.VAL as idcategorieingredient,
           im.DATY,
           invDet.IDINGREDIENT,
           uni.val as unite
        from inventairedetails invDet
        left join AS_INGREDIENTS ing on ing.id=invDet.idingredient
        left join CATEGORIEINGREDIENT catIng on catIng.ID=invDet.IDCATEGORIEINGREDIENT
        join inventaireMere im on invDet.idmere=im.id
        join as_unite uni on ing.unite=uni.id
        where im.etat>=11;
        
        
        
        CREATE OR REPLACE FORCE VIEW "AS_MVTSTOCK_FDTLIBUNITECPS" ("ID", "INGREDIENTS", "ENTREE", "SORTIE", "DATY", "UNITE", "IDINGREDIENTS", "POINT") AS 
  select fille.id,grd.libelle as ingredients, fille.entree, fille.sortie, mere.daty, u.val as unite,fille.INGREDIENTS,mere.depot
  from as_mvt_stock_fille fille
    join as_mvt_stock mere on fille.idmvtstock = mere.id
    join as_ingredients grd on fille.ingredients = grd.id
    join as_unite u on grd.UNITE=u.id
    where mere.etat>=11;