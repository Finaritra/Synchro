create view VUE_CMD_LIV_DTLS_ALIVRER_PT as
    select
            liv.*,
            cli.telephone as idclient,
            cl.adresse ,
            pt.val as idpoint

        from VUE_CMD_LIV_DTLS_ALIVRER liv
        join commandeLivraison cl on cl.id=liv.idcl
        left join as_client cli on cli.id = cl.idclient
        left join point pt on pt.id=cl.idPoint;

create view VUE_CMD_LIV_DTLS_ALIVRER_PT_AD AS
    SELECT *
        FROM VUE_CMD_LIV_DTLS_ALIVRER_PT WHERE adresse IS NULL OR adresse='';


create  or replace view VUE_CMD_CLT_DETS_REST_RESP as
select
det.id,
tab.nom as nomtable,
tab.telephone,
tab.id as idclient,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
det.prioriter,
det.observation,
tp.id as idtype,
resp.NOM as responsable,
cmd.id as idmere
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
join AS_RESPONSABLE resp on resp.id=cmd.RESPONSABLE
order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;


    CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_TOUS
    (ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV,
     RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT,
     ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE,
     RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL,idmere)
    AS
    select

            vccdrr.id,

           vccdrr.nomtable,

           vccdrr.typeproduit,

           vccdrr.datecommande,

           vccdrr.heureliv,

           vccdrr.restaurant,

          vccdrr.produit,

           vccdrr.quantite,

           vccdrr.pu,

           vccdrr.montant,

          vccdrr.etat,

           vccdrr.acco_sauce,

           vccdrr.prioriter,

           vccdrr.observation,

           vccdrr.idtype,

           vccdrr.responsable,

           '-' as idcl,

           vccdrr.datecommande as datycl,

           vccdrr.telephone,

         '-' as heurecl,

           vccdrr.idmere

    from VUE_CMD_CLT_DETS_REST_RESP vccdrr

    order by vccdrr.DATECOMMANDE , vccdrr.HEURELIV ASC, vccdrr.prioriter asc;



create or replace view VUE_CMD_LIV_DTLS_ACLOT as
select "ID",
       "NOMTABLE",
       "TYPEPRODUIT",
       "DATECOMMANDE",
       "HEURELIV",
       "RESTAURANT",
       "PRODUIT","QUANTITE","PU","MONTANT","ETAT","ACCO_SAUCE","PRIORITER","OBSERVATION","IDTYPE","RESPONSABLE","IDCL","DATYCL","TELEPHONE","HEURECL",idmere
    from VUE_CMD_LIV_DTLS_TOUS where etat = 8;

create or replace view VUE_CMD_LIV_DTLS_ANNULERPAY as
select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","RESTAURANT","PRODUIT",
       "QUANTITE","PU","MONTANT","ETAT","ACCO_SAUCE","PRIORITER","OBSERVATION","IDTYPE","RESPONSABLE","IDCL","DATYCL","TELEPHONE","HEURECL" ,idmere
from VUE_CMD_LIV_DTLS_TOUS where etat = 6;



create view VUE_CMD_CLT_TYPEP_REST_NOT AS
    select
    id,
    nomtable,
    typeproduit,
    datecommande,
    heureliv,
    restaurant,
    produit,
    quantite,
    pu,
    montant,
    etat,
    acco_sauce,
    idclient
from  VUE_CMD_CLT_DETAIL_TYPEP_REST where etat > 0 and etat <= 20 and idclient like 'CASM%';


create or replace view VUE_CMD_LIV_DTLS_ALIVRER  as
select "ID",
"NOMTABLE",
"TYPEPRODUIT",
"DATECOMMANDE",
"HEURELIV",
"RESTAURANT",
"PRODUIT",
"QUANTITE",
"PU",
"MONTANT",
"ETAT",
"ACCO_SAUCE",
"PRIORITER",
"OBSERVATION",
"IDTYPE",
"RESPONSABLE",
"IDCL",
"DATYCL",
"TELEPHONE",
"HEURECL",
idmere 
from VUE_CMD_LIV_DTLS_TOUS where etat = 12;

create or replace view VUE_CMD_LIV_DTLS_ALIVRER_PT as
    select
            liv.*,
            pt.VAL as idpoint,
           cl.adresse
        from VUE_CMD_LIV_DTLS_ALIVRER liv
        join commandeLivraison cl on cl.idclient=liv.idmere
        left join point pt on pt.id=cl.idPoint;
    
create or replace view VUE_CMD_LIV_DTLS_ALIVRER_PT_AD AS
    SELECT *
        FROM VUE_CMD_LIV_DTLS_ALIVRER_PT WHERE adresse IS NULL OR adresse='';

create or replace view VUE_CMD_LIV_DTLS_ALIVRER_PTVID AS
    SELECT *
        FROM VUE_CMD_LIV_DTLS_ALIVRER_PT WHERE idpoint IS NULL OR idpoint='';