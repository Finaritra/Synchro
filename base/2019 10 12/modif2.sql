CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE2
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, ETAT, COURSIER, MONTANT, 
 SECTEUR, VENTE, RESTAURANT, PRENOM)
AS 
select 
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE",cmd."ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",cmd."ETAT",  '-', mtnt.montant, sect.val as secteur ,cmd.vente,cmd.restaurant,cmd.prenom
from
    as_commandeclient_libelle cmd,
    montantparcommande mtnt,
    as_secteur sect
where
    mtnt.id = cmd.id and sect.id(+)=cmd.sect;


    CREATE OR REPLACE FORCE VIEW AS_COMMANDECLIENT_LIBLIVRER AS 
  select * from as_commandeclient_libelle2 cmd

where
     cmd.etat=20;
     
     
CREATE OR REPLACE FORCE VIEW as_commandeclient_livp AS 
  select * from as_commandeclient_libelle2 cmd

where     cmd.etat=40;

CREATE OR REPLACE FORCE VIEW AS_COMMANDECLIENT_libcloturer AS 
  select * from as_commandeclient_libelle2 cmd

where    cmd.etat=9;

CREATE OR REPLACE FORCE VIEW as_commandeclient_libvalider AS 
  select * from as_commandeclient_libelle2 cmd

where    cmd.etat=11;