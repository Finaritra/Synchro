insert into caisse values ('-','-','2','etaCs1');
insert into caisse values ('CE000606','A payer','2','etaCs1');
commit;
CREATE OR REPLACE VIEW MVTCAISSELETTRERESTE
(ID, DATY, DESIGNATION, DEBIT, CREDIT, 
 IDDEVISE, IDMODE, IDCAISSE, REMARQUE, TIERS, 
 NUMPIECE, NUMCHEQUE, TYPEMVT, DATYVALEUR, IDORDRE, 
 ETAT, AGENCE, IDMVTOR, ETABLISSEMENT, RESTE)
AS 
select m."ID",m."DATY",m."DESIGNATION",m."DEBIT",m."CREDIT",m."IDDEVISE",
m."IDMODE",m."IDCAISSE",m."REMARQUE",m."TIERS",l.idlivreur,m."NUMCHEQUE",
m."TYPEMVT",m."DATYVALEUR",m."IDORDRE",m."ETAT",m."AGENCE",m."IDMVTOR",
m."ETABLISSEMENT", cast((m.credit-m.debit) as NUMBER(10,2)) as reste from MVTCAISSELETTRE m,
livraisonlib l where m.idordre=l.IDCOMMNADEMERE(+);
CREATE OR REPLACE VIEW VUE_CMD_LIV_DTLS_ALIVRER_PT
(ID, NOMTABLE, TYPEPRODUIT, DATECOMMANDE, HEURELIV, 
 RESTAURANT, PRODUIT, QUANTITE, PU, MONTANT, 
 ETAT, ACCO_SAUCE, PRIORITER, OBSERVATION, IDTYPE, 
 RESPONSABLE, IDCL, DATYCL, TELEPHONE, HEURECL, 
 IDMERE, IDPOINT, ADRESSE)
AS 
select
 liv."ID",liv."NOMTABLE",liv."TYPEPRODUIT",liv."DATECOMMANDE",liv."HEURELIV",liv."RESTAURANT",liv."PRODUIT",liv."QUANTITE",liv."PU",liv."MONTANT",
 liv."ETAT",liv."ACCO_SAUCE",liv."PRIORITER",liv."OBSERVATION",liv."IDTYPE",liv."IDMERE",liv."IDCL",liv."DATYCL",liv."TELEPHONE",liv."HEURECL",liv."IDMERE",
 pt.VAL as idpoint,
cl.adresse
 from VUE_CMD_LIV_DTLS_ALIVRER liv
 join commandeLivraisonMax cl on cl.idclient=liv.idmere
 left join point pt on pt.id=cl.idPoint
/







create or replace view mvtCaisseLettreComandeMere as
select m.ID, m.daty, m.designation, m.debit, m.credit, m.iddevise,
          m.idmode, m.idcaisse, m.remarque,
          m.tiers AS tiers, dcmd.IDMERE as numpiece, m.numcheque, m.typemvt,
          m.datyvaleur, m.idordre,m.etat,m.agence,m.IDMVTOR,m.ETABLISSEMENT
from mvtCaisse m, as_detailscommande dcmd where m.IDORDRE=dcmd.ID;

create or replace view mvtCaisseLettreCmdMereGroup as
select max(m.ID) as id, max(m.daty) as daty, '-' as designation, sum(m.debit) as debit, sum(m.credit) as credit, max(m.iddevise) as iddevise,
          max(m.idmode) as idmode, max(c.DESCCAISSE) as idcaisse, max(m.remarque) as remarque,
          max(m.tiers) AS tiers, numpiece, max(m.numcheque) as numcheque, max(m.typemvt) as typemvt,
          max(m.datyvaleur) as datyvaleur, '-' as idordre,max (m.etat) as etat, max(m.agence) as agence,
          max(m.IDMVTOR) as idmvtor, max(m.ETABLISSEMENT) as etablissement
from mvtCaisseLettreComandeMere m, caisse c where m.IDCAISSE=c.IDCAISSE group by numpiece;


CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, VENTE, ETAT, SECT, 
 RESTAURANT, PRENOM)
AS 
select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, tbl.TELEPHONE as responsable, tbl.nom as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect, cmd.point , 
    mvt.idcaisse
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    as_client tbl,
    vente,mvtCaisseLettreCmdMereGroup mvt
    
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur
    and cmd.client = tbl.id
    and cmd.vente=vente.id(+)
    and cmd.id=mvt.NUMPIECE(+);