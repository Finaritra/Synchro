--mojito PRDTBOI0026
--sucre roux 20g IG000501	
--citron 1.5 INGS000776
--rhum blanc 5cl IG000500 - dzama 42
--feuille de menthe 5piece IG000358
--glacon 6
--cristal 8cl INGBOINS28(unité)
--UNT00001:g UNT00004:ml UNT00005:unité
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0001', 'PRDTBOI0026', 'IG000501', 20 , 'UNT00001');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0002', 'PRDTBOI0026', 'INGS000776', 1.5 , 'UNT00005');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0003', 'PRDTBOI0026', 'IG000500', 50, 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0004', 'PRDTBOI0026', 'IG000358', 5 , 'UNT00005');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0005', 'PRDTBOI0026', 'INGBOINS28', 0.8 , 'UNT00004');
--Caiprina PRDTBOI0027
--citron vert 2
--sucre roux 2
--jus de citron concentré 1cl INGBOINS4
--glacon pillé 10piece
--rhum blanc 6cl IG000493
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0006', 'PRDTBOI0027', 'IG000501', 2 , 'UNT00001');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0007', 'PRDTBOI0027', 'INGS000776', 2 , 'UNT00005');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0008', 'PRDTBOI0027', 'INGBOINS4', 10 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0009', 'PRDTBOI0027', 'IG000493', 60 , 'UNT00004');

--Cuba libre PRDTBOI0028
--rhum brun 5cl - rio IG000492
--coca 11cl INGBOI0007
--jus de citron concentré 2cl
--glacon 5
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0010', 'PRDTBOI0028', 'IG000492', 50 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0011', 'PRDTBOI0028', 'INGBOINS4', 20 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0012', 'PRDTBOI0028', 'INGBOI0007', 20 , 'UNT00004');

--Planteur PRDTBOI0029
--rhum brun 5cl
--jus d'orange + ananas 12cl
--jus de citron 2cl
--glacon 5
--sirop de grenadine 1cl ING000199
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0013', 'PRDTBOI0029', 'IG000492', 50 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0014', 'PRDTBOI0029', 'INGBOINS4', 20 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0015', 'PRDTBOI0029', 'ING000199', 10 , 'UNT00004');
--insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0016', 'PRDTBOI0029', 'INGS000816', 40 , 'UNT00004');

--Tentation PRDTBOI0033
--vodka smirnoff 4cl INGR000186
--sirop mangue passion 2cl IG000223
--limonade 10cl INGBOI0013
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0017', 'PRDTBOI0033', 'INGR000186', 40 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0018', 'PRDTBOI0033', 'IG000223', 20 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0019', 'PRDTBOI0033', 'INGBOI0013', 100 , 'UNT00004');

--Gin Fizz PRDTBOI0034
--gin harpoon 5cl IG000334
--jus de citron 2cl
--sirop de canne 2cl INGBOINS2
--glacon 4
--crystal 8cl INGBOINS28
--citron vert 0.5
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0020', 'PRDTBOI0034', 'IG000334', 50 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0021', 'PRDTBOI0034', 'INGBOINS4', 20 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0022', 'PRDTBOI0034', 'INGBOINS2', 20 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0023', 'PRDTBOI0034', 'INGBOINS28', 0.8 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0024', 'PRDTBOI0034', 'INGS000776', 0.5 , 'UNT00005');

--Tequila sunrise PRDTBOI0036
--tequila municion 5cl INGBOINS6
--jus d'orange 8cl
--jus de cocktail 4cl IG000423
--jus de citron 2cl
--glacon 3unite
--sirop de fraise 1cl IG000345
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0025', 'PRDTBOI0036', 'INGBOINS6', 50 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0026', 'PRDTBOI0036', 'IG000423', 40 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0027', 'PRDTBOI0036', 'INGBOINS4', 20 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0028', 'PRDTBOI0036', 'IG000345', 10 , 'UNT00004');

--Pink PRDTBOI0030
--yaourt 1
--jus d'orange 2cl
--sirop de grenadine 2cl
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0030', 'PRDTBOI0030', 'ING000199', 20 , 'UNT00004');
--insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0031', 'PRDTBOI0030', 'INGS000816', 40 , 'UNT00004');

--Bora  PRDTBOI0031
--Jus d'ananas 12cl
--Jus de fruit de la passion 6cl
--jus de citron 1cl
--grenadine 1cl
--glacon 5
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0032', 'PRDTBOI0031', 'ING000199', 10 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0033', 'PRDTBOI0031', 'INGBOINS4', 40 , 'UNT00004');
--insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0034', 'PRDTBOI0031', 'INGS000816', 40 , 'UNT00004');
--insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0035', 'PRDTBOI0031', 'INGS000816', 40 , 'UNT00004');

--Soda float PRDTBOI0032
--Sprite 15cl INGBOI0011
-- sirop de fraise 1cl IG000342
--glace 1boule
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0036', 'PRDTBOI0032', 'IG000342', 10 , 'UNT00004');
--insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0037', 'PRDTBOI0032', 'INGBOI0011', 10 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0038', 'PRDTBOI0032', 'INGBOI0011', 0.15 , 'UNT00001');

--Pina colada PRDTBOI0035
--Ananas 100g
--Jus d'ananas 12cl
--Lait de coco 4cl
--sucre de canne 2cl
--glacon 5
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0039', 'PRDTBOI0035', 'INGS000766', 100 , 'UNT00001');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0040', 'PRDTBOI0035', 'IG000494', 40 , 'UNT00004');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0041', 'PRDTBOI0035', 'INGBOINS2', 20 , 'UNT00004');
--insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0042', 'PRDTBOI0035', 'INGS000816', 40 , 'UNT00004');

--Virgin Mojito PRDTBOI0025
--sucre
--citron
--feuille de menthe
--glacon
--cristal
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0043', 'PRDTBOI0025', 'IG000501', 20 , 'UNT00001');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0044', 'PRDTBOI0025', 'INGS000776', 1.5 , 'UNT00005');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0045', 'PRDTBOI0025', 'IG000358', 5 , 'UNT00005');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCC0046', 'PRDTBOI0025', 'INGBOINS28', 0.8 , 'UNT00004');