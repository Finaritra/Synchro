ALTER TABLE AS_RESERVATION ADD  remarque VARCHAR(250);


create or replace view RESERVATIONLIBTOUS as
        select
            reserv.id,
            reserv.SOURCE,
            reserv.NBPERSONNE,
            reserv.DATESAISIE,
            reserv.HEURESAISIE,
            reserv.DATERESERVATION,
            reserv.HEUREDEBUTRESERVATION,
            reserv.HEUREFINRESERVATION,
            clt.nom ||' '||clt.PRENOM as idclient,
            clt.TELEPHONE,
            reserv.ETAT,
            tab.VAL as tablenom,
            reserv.remarque
        from AS_RESERVATION reserv
        left join RESA_TABLE RT on reserv.ID = RT.IDRESERVATION
        left join AS_CLIENT clt on clt.ID=reserv.IDCLIENT
        left join TABLECLIENTVUE2 tab on tab.id=rt.IDTABLE;
