Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('TPD00001','Boisson','1');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('TPD00004','Fabrication','10');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('ASTP000001','Soupes','5');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('ASTP000002','Poisson','5');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('ASTP000003','Sakafo Malagasy ','5');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('ASTP000004','Poulet ','5');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('ASTP000005','Pâtes','4');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('ASTP000006','Viande','5');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('ASTP000007','Accompagnement','3');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('ASTP000008','Riz','5');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('ASTP000009','Autres','2');
Insert into AS_TYPEPRODUIT (ID,VAL,DESCE) values ('ASTP000010','Crepe','6');



ALTER TABLE AS_DETAILSCOMMANDE ADD PRIORITER INTEGER;
