
CREATE OR REPLACE FORCE VIEW AS_ETATSTOCK_LIBELLE1 AS 

select as_ingredients.libelle as ingredients, unite.val as unite
, as_etatstock1.entree, as_etatstock1.sortie, as_etatstock1.reste, as_etatstock1.report, as_etatstock1.daty,as_etatstock1.point 
from as_etatstock1 
join  unite on  unite.id = as_etatstock1.unite 
join as_ingredients on  as_etatstock1.ingredients = as_ingredients.id ;


 




CREATE OR REPLACE FORCE VIEW Livraisondetail as  
  select cmdcl.id , tcv.val as NomTable , pro.nom as Produit, ac.idsauce as SauceAcoompagnement , cmdcl.DATECOMMANDE as DateCommande,
  cmdcl.HEURELIV as HeureCommande , liv.DATY as DateLivraison, liv.HEURE as HeureLivraison,
  cast( ( 60 *( to_number( substr(liv.HEURE,1,2) ) - to_number( substr(cmdcl.HEURELIV,1,2) ) ) ) + (  to_number( substr(liv.HEURE,4,2) ) - to_number( substr(cmdcl.HEURELIV,4,2) ) )  as number(20,2) )  as difference 
  from AS_LIVRAISON liv join AS_DETAILSCOMMANDE cmdcld  on liv.COMMANDE = cmdcld.id 
	join  AS_COMMANDECLIENT cmdcl on cmdcl.id = cmdcld.IDMERE
	join TABLECLIENTVUE2 tcv on cmdcl.client = tcv.id
	join AS_PRODUITS pro on cmdcld.PRODUIT = pro.id   
	left join AS_ACCOMPAGNEMENT_SAUCE_LIB ac on cmdcld.IDACCOMPAGNEMENTSAUCE  = ac.id ;







CREATE OR REPLACE FORCE VIEW Livraisondetail as  
  select cmdcl.id , tcv.val as NomTable , pro.nom as Produit, ac.idsauce as SauceAcoompagnement , cmdcl.DATELIV as DateCommande,
  cmdcl.HEURELIV as HeureCommande , liv.DATY as DateLivraison, liv.HEURE as HeureLivraison,
  ( to_number( substr(liv.HEURE,1,2) ) * to_number( substr(liv.HEURE,4,2) ) * 3600 ) - ( to_number( substr(cmdcl.HEURELIV,1,2) ) * to_number( substr(cmdcl.HEURELIV,4,2) ) * 3600 ) as difference
  from AS_LIVRAISON liv join AS_COMMANDECLIENT cmdcl on liv.COMMANDE = cmdcl.id 
	join AS_DETAILSCOMMANDE cmdcld on cmdcl.id = cmdcld.IDMERE
	join TABLECLIENTVUE2 tcv on cmdcl.client = tcv.id
	join AS_PRODUITS pro on cmdcld.PRODUIT = pro.id   
	left join AS_ACCOMPAGNEMENT_SAUCE_LIB ac on cmdcld.IDACCOMPAGNEMENTSAUCE  = ac.id ;


---- 

update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-commande-mobile-liste.jsp'  where id='ASM0000012';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=fiches/etat.jsp'  where id='ASM0000013';
update MENUDYNAMIQUE set href=''  where id='ASM0000501';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=stock/mvtStock/inventaire-saisie.jsp'  where id='ASM00008034';
update MENUDYNAMIQUE set href=''  where id='ASM0000902';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=secteur/as-quartier-saisie.jsp'  where id='ASM00009021';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=secteur/as-quartier-liste.jsp'  where id='ASM00009022';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=ded/situationop-liste.jsp'  where id='ASM00006023';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=configuration/idvaldesce.jsp&ciblename=point'  where id='ASM0000904';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-commande-saisie.jsp'  where id='ASC0000101';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-commande-liste.jsp'  where id='ASC0000102';
update MENUDYNAMIQUE set href=''  where id='ASC0000103';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-paiement-saisie.jsp'  where id='ASP00001031';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-paiement-liste.jsp'  where id='ASP00001032';
update MENUDYNAMIQUE set href=''  where id='ASC0000104';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-livraison-saisie.jsp'  where id='ASC00001041';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-livraison-liste.jsp'  where id='ASC00001042';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=appel/as-appel-saisie.jsp'  where id='ASM0000301';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=appel/as-appel-liste.jsp'  where id='ASM0000302';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=produits/as-produitsprix-saisie.jsp'  where id='ASM0000401';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=produits/as-produits-liste.jsp'  where id='ASM0000402';
update MENUDYNAMIQUE set href=''  where id='ASM0000403';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=produits/as-tarifproduits-saisie.jsp'  where id='ASM00004031';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=produits/as-tarifproduits-liste.jsp'  where id='ASM00004032';
update MENUDYNAMIQUE set href=''  where id='ASM0000201';
update MENUDYNAMIQUE set href=''  where id='ASM0000202';
update MENUDYNAMIQUE set href=''  where id='ASM0000203';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=tiers/as-vehicule-saisie.jsp'  where id='ASM00002021';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=tiers/as-vehicule-liste.jsp'  where id='ASM00002022';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=tiers/as-client-saisie.jsp'  where id='ASM00002011';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=tiers/as-client-liste.jsp'  where id='ASM00002012';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=tiers/as-responsable-saisie.jsp'  where id='ASM00002031';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=tiers/as-responsable-liste.jsp'  where id='ASM00002032';
update MENUDYNAMIQUE set href=''  where id='ASM00005';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=facture/factureclient-saisie.jsp'  where id='ASM00005011';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=facture/facture_client_liste.jsp'  where id='ASM00005012';
update MENUDYNAMIQUE set href=''  where id='ASM0000502';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=facture/as-ordonnerpaiement-saisie.jsp'  where id='ASM00005031';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=facture/as-ordonnerpaiement-liste.jsp'  where id='ASM00005032';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=facture/FactureClientGroupe-liste.jsp'  where id='ASM00005012';
update MENUDYNAMIQUE set href=''  where id='ASM00001';
update MENUDYNAMIQUE set href=''  where id='ASM00002';
update MENUDYNAMIQUE set href=''  where id='ASM00003';
update MENUDYNAMIQUE set href=''  where id='ASM00004';
update MENUDYNAMIQUE set href=''  where id='ASM0000906';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/commandepoint-saisie.jsp'  where id='ASM0001908';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/commandepoint-liste.jsp'  where id='ASM0000908';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=produits/ingredientsPrecuit-saisie.jsp'  where id='ASM0000909';
update MENUDYNAMIQUE set href=''  where id='ASM00008';
update MENUDYNAMIQUE set href=''  where id='ASM0000801';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=stock/bc/asBonDeCommande-saisie.jsp'  where id='ASM00008011';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=stock/bc/asBonDeCommande-liste.jsp'  where id='ASM00008012';
update MENUDYNAMIQUE set href=''  where id='ASM0000802';
update MENUDYNAMIQUE set href=''  where id='ASM00008021';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=stock/bl/asBonDeLivraison-liste.jsp'  where id='ASM00008022';
update MENUDYNAMIQUE set href=''  where id='ASM0000803';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=stock/mvtStock/entree-stock.jsp'  where id='ASM00008031';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=stock/mvtStock/sortie-stock.jsp'  where id='ASM00008032';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=stock/mvtStock/asMvtStock-liste.jsp'  where id='ASM00008033';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=stock/mvtStock/etatstock-liste.jsp&premier=true'  where id='ASM0000804';
update MENUDYNAMIQUE set href=''  where id='ASM0000404';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=produits/as-ingredients-saisie.jsp'  where id='ASM00004041';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=produits/as-ingredients-liste.jsp'  where id='ASM00004042';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=configuration/idvaldesce.jsp&ciblename=AS_SECTEUR'  where id='ASM0000901';
update MENUDYNAMIQUE set href=''  where id='ASM00009';
update MENUDYNAMIQUE set href='/phobo/index.jsp?but=secteur/as-tariflivraison-saisie.jsp'  where id='ASM0000903';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=configuration/idvaldesce.jsp&ciblename=vente'  where id='ASM0000909A';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-commande-analyse.jsp'  where id='ASC0000105';
update MENUDYNAMIQUE set href=''  where id='ASM00006';
update MENUDYNAMIQUE set href=''  where id='ASM0000601';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=facturefournisseur/facturefournisseur-saisie.jsp'  where id='ASM00006011';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=facturefournisseur/facturefournisseur-liste.jsp'  where id='ASM00006012';
update MENUDYNAMIQUE set href=''  where id='ASM0000602';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=ded/ordonnerpayement-direct.jsp'  where id='ASM00006021';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=ded/ordonnerpayement-liste.jsp'  where id='ASM00006022';
update MENUDYNAMIQUE set href=''  where id='ASM00007';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=fin/entreecaisse-saisie.jsp'  where id='ASM0000701';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=fin/sortiecaisse-saisie.jsp'  where id='ASM0000702';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=fin/mvtcaisse-liste.jsp'  where id='ASM0000703';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=fin/etatcaisse-liste.jsp&premier=true'  where id='ASM0000704';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=ded/report-saisie.jsp'  where id='ASM0000705';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=tables/idvaldesce.jsp&ciblename=tables'  where id='ASM00010';
update MENUDYNAMIQUE set href=''  where id='ASM000001';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-commande-mobile-saisie.jsp'  where id='ASM0000011';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-commande-liste-details.jsp'  where id='ASM0000907';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-commande-liste-details.jsp&table='  where id='ASM0000910';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-commande-liste-details-fait.jsp&table='  where id='ASM0000911';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-commande-liste-details-valider.jsp&table='  where id='ASM0000912';
update MENUDYNAMIQUE set href='/phobo/pages/module.jsp?but=commande/as-commande-liste-details-livrer.jsp&table='  where id='ASM0000913';





 CREATE OR REPLACE FORCE VIEW Livraisondetail as 
select cmdcl.id , tcv.val as NomTable , pro.nom as Produit, ac.idsauce as SauceAcoompagnement , cmdcl.DATELIV as DateCommande,
  cmdcl.HEURELIV as HeureCommande , liv.DATY as DateLivraison, liv.HEURE as HeureLivraison,
  cast(to_number(to_char( TO_TIMESTAMP( liv.HEURE ,'HH24:MI' ) -  TO_TIMESTAMP(  cmdcl.HEURELIV ,'HH24:MI' ) , 's' )) as number(20,2)) as difference  from AS_LIVRAISON liv join AS_COMMANDECLIENT cmdcl on liv.COMMANDE = cmdcl.id 
	join AS_DETAILSCOMMANDE cmdcld on cmdcl.id = cmdcld.IDMERE
	join TABLECLIENTVUE2 tcv on cmdcl.client = tcv.id
	join AS_PRODUITS pro on cmdcld.PRODUIT = pro.id   
  left join AS_ACCOMPAGNEMENT_SAUCE_LIB ac on cmdcld.IDACCOMPAGNEMENTSAUCE  = ac.id ;

  SELECT TIMEDIFF('6:5', '5:23');
  
  
  
  select cmdcl.id , tcv.val as NomTable , pro.nom as Produit, ac.idsauce as SauceAcoompagnement , cmdcl.DATELIV as DateCommande,
  cmdcl.HEURELIV as HeureCommande , liv.DATY as DateLivraison, liv.HEURE as HeureLivraison,
--  24 * 60 *(to_date( to_char(liv.DATY)' '||liv.HEURE , 'dd/mm/yy hh24:mi') - to_date( to_char(DateCommande)' '|| HeureCommande , 'dd/mm/yy hh24:mi') ) as diff_hours
  ( to_number( substr(liv.HEURE,1,2) ) * to_number( substr(liv.HEURE,4,2) ) * 3600 ) - ( to_number( substr(cmdcl.HEURELIV,1,2) ) * to_number( substr(cmdcl.HEURELIV,4,2) ) * 3600 ) as difference
  from AS_LIVRAISON liv join AS_COMMANDECLIENT cmdcl on liv.COMMANDE = cmdcl.id 
	join AS_DETAILSCOMMANDE cmdcld on cmdcl.id = cmdcld.IDMERE
	join TABLECLIENTVUE2 tcv on cmdcl.client = tcv.id
	join AS_PRODUITS pro on cmdcld.PRODUIT = pro.id   
  left join AS_ACCOMPAGNEMENT_SAUCE_LIB ac on cmdcld.IDACCOMPAGNEMENTSAUCE  = ac.id ;




  
 CREATE OR REPLACE FORCE VIEW Livraisondetail as 
select cmdcl.id , tcv.val as NomTable , pro.nom as Produit, ac.idsauce as SauceAcoompagnement , cmdcl.DATELIV as DateCommande, cmdcl.HEURELIV as HeureCommande , liv.DATY as DateLivraison, liv.HEURE as HeureLivraison,  to_number(to_char( TO_TIMESTAMP( liv.HEURE ,'HH24:MI' ) -  TO_TIMESTAMP(  cmdcl.HEURELIV ,'HH24:MI' ) , 's' )) as difference  from AS_LIVRAISON liv join AS_COMMANDECLIENT cmdcl on liv.COMMANDE = cmdcl.id 
	join AS_DETAILSCOMMANDE cmdcld on cmdcl.id = cmdcld.IDMERE
	join TABLECLIENTVUE2 tcv on cmdcl.client = tcv.id
	join AS_PRODUITS pro on cmdcld.PRODUIT = pro.id   
  left join AS_ACCOMPAGNEMENT_SAUCE_LIB ac on cmdcld.IDACCOMPAGNEMENTSAUCE  = ac.id

  
 CREATE OR REPLACE FORCE VIEW Livraisondetail as 
select cmdcl.id , tcv.val as NomTable , pro.nom as Produit, ac.idsauce as SauceAcoompagnement , cmdcl.DATELIV as DateCommande, cmdcl.HEURELIV as HeureCommande , liv.DATY as DateLivraison, liv.HEURE as HeureLivraison,  TO_TIMESTAMP( liv.HEURE ,'HH24:MI' ) -  TO_TIMESTAMP(  cmdcl.HEURELIV ,'HH24:MI' ) as difference  from AS_LIVRAISON liv join AS_COMMANDECLIENT cmdcl on liv.COMMANDE = cmdcl.id 
	join AS_DETAILSCOMMANDE cmdcld on cmdcl.id = cmdcld.IDMERE
	join TABLECLIENTVUE2 tcv on cmdcl.client = tcv.id
	join AS_PRODUITS pro on cmdcld.PRODUIT = pro.id   
  left join AS_ACCOMPAGNEMENT_SAUCE_LIB ac on cmdcld.IDACCOMPAGNEMENTSAUCE  = ac.id




