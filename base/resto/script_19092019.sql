


CREATE OR REPLACE FORCE VIEW AS_DETAILSCOMMANDE_LIB_SAUCE as  
select
    dt.id, dt.idmere, art.NOM as PRODUIT , CONCAT ( sac.idsauce , sac.IDACCOMPAGNEMENT ) as IDACCOMPAGNEMENT , art.PHOTO , cc.CLIENT as IDCLIENT , dt.QUANTITE, dt.PU, dt.REMISE, dt.OBSERVATION , dt.ETAT
from 
as_detailscommande dt 
join as_commandeclient cc on cc.id = dt.idmere 
join as_produits art on dt.produit = art.ID 
inner join AS_ACCOMPAGNEMENT_SAUCE_LIB sac on sac.id = dt.IDACCOMPAGNEMENTSAUCE  where cc.client like '%CASM%' ORDER BY dt.ID;


/*----------------------------*/


CREATE OR REPLACE FORCE VIEW as_produits_libPrix_souscat
	as
select
    prd.id, prd.nom ,prd.designation ,  prd.IDSOUSCATEGORIE  , prd.photo, tpd.val as TYPEPRODUIT , prd.calorie, prd.pa, prd.poids, sum(trf.montant) keep (dense_rank first order by trf.dateapplication desc) as PU
from
    as_produits prd,
    as_typeproduit tpd,
    as_prixproduit trf
where
    prd.typeproduit = tpd.id
    and trf.produit = prd.id
group by prd.id, prd.nom, prd.designation, prd.photo ,  prd.IDSOUSCATEGORIE , tpd.val, prd.calorie, prd.pa, prd.poids ;


/*----------------------------*/


CREATE OR REPLACE FORCE VIEW AS_DETAILSCOMMANDE_LIB_SAUCE
	as
  select
    dt.id, dt.idmere, art.NOM as PRODUIT , CONCAT ( sac.idsauce , sac.IDACCOMPAGNEMENT ) as IDACCOMPAGNEMENT , art.PHOTO , cc.CLIENT as IDCLIENT , dt.QUANTITE, dt.PU, dt.REMISE, dt.OBSERVATION , dt.ETAT
from
    as_detailscommande dt,
  	as_commandeclient cc,
    as_produits art,
    AS_ACCOMPAGNEMENT_SAUCE_LIB sac
where
    sac.id = dt.IDACCOMPAGNEMENTSAUCE and dt.produit = art.ID and cc.id = dt.idmere and cc.client like '%CASM%' ORDER BY dt.ID;
	


/*--------------------------*/




ALTER TABLE AS_DETAILSCOMMANDE ADD IDACCOMPAGNEMENTSAUCE VARCHAR2(50 BYTE);
ALTER TABLE AS_DETAILSCOMMANDE ADD CONSTRAINT FK_ACCOMPAGNEMENTSAUCE  FOREIGN KEY(IDACCOMPAGNEMENTSAUCE) REFERENCES AS_ACCOMPAGNEMENT_SAUCE(ID) ;



  CREATE OR REPLACE FORCE VIEW AS_ACCOMPAGNEMENT_SAUCE_LIB AS 
  select
      sac.ID , s.val as IDSAUCE , ac.VAL as IDACCOMPAGNEMENT ,  sac.REMARQUE , sac.ETAT 
  from
      AS_ACCOMPAGNEMENT_SAUCE sac,
      AS_SAUCE s,
      AS_ACCOMPAGNEMENT ac
  where
    s.ID =  sac.IDSAUCE and sac.IDACCOMPAGNEMENT = ac.ID ;




  CREATE OR REPLACE FORCE VIEW AS_DETAILSCOMMANDE_LIB_PHOTO AS 
  select
    dt.id, dt.idmere, art.NOM as PRODUIT, cc.CLIENT as IDCLIENT , dt.QUANTITE, dt.PU, dt.REMISE, dt.OBSERVATION , art.PHOTO , dt.ETAT
from
    as_detailscommande dt,
    as_produits art,
  	as_commandeclient cc
where
    dt.produit = art.ID and cc.id = dt.idmere and cc.client like '%CASM%' ORDER BY dt.ID;
 






ALTER TABLE AS_PRODUITS ADD IDSOUSCATEGORIE VARCHAR2(50 BYTE);
ALTER TABLE AS_PRODUITS ADD CONSTRAINT FK_souscategorie_produit  FOREIGN KEY(IDSOUSCATEGORIE) REFERENCES AS_SOUSCATEGORIE(ID) ;

CREATE TABLE AS_SAUCE (
	ID VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	VAL VARCHAR2(100 BYTE), 
	DESCE VARCHAR2(100 BYTE), 
	constraint PK_SAUCE PRIMARY KEY (ID)
);
CREATE SEQUENCE  SEQ_AS_SAUCE  MINVALUE 1 MAXVALUE 9999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;   
create or replace FUNCTION GETSEQ_AS_SAUCE RETURN NUMBER IS retour NUMBER; BEGIN SELECT SEQ_AS_SAUCE.nextval INTO retour FROM dual; return retour; END;


CREATE TABLE AS_ACCOMPAGNEMENT (
	ID VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	VAL VARCHAR2(100 BYTE), 
	DESCE VARCHAR2(100 BYTE), 
	constraint PK_ACCOMPAGNEMENT PRIMARY KEY (ID)
);
CREATE SEQUENCE  SEQ_AS_ACCOMPAGNEMENT  MINVALUE 1 MAXVALUE 9999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;   
create or replace FUNCTION GETSEQ_AS_ACCOMPAGNEMENT RETURN NUMBER IS retour NUMBER; BEGIN SELECT SEQ_AS_ACCOMPAGNEMENT.nextval INTO retour FROM dual; return retour; END;


CREATE TABLE AS_ACCOMPAGNEMENT_SAUCE (
	ID VARCHAR2(50 BYTE) NOT NULL ENABLE,
	IDSAUCE VARCHAR2(50 BYTE) NOT NULL ENABLE,
	IDACCOMPAGNEMENT VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	REMARQUE VARCHAR2(2000 BYTE),
	ETAT INTEGER,
	constraint PK_ACCOMPAGNEMENT_SAUCE PRIMARY KEY (ID),
	CONSTRAINT FK_SAUCE  FOREIGN KEY (IDSAUCE) REFERENCES AS_SAUCE(ID),
	CONSTRAINT FK_ACCOMPAGNEMENT  FOREIGN KEY (IDACCOMPAGNEMENT) REFERENCES AS_ACCOMPAGNEMENT(ID)
);
CREATE SEQUENCE  SEQ_ACCOMPAGNEMENT_SAUCE  MINVALUE 1 MAXVALUE 9999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;   
create or replace FUNCTION GETSEQ_ACCOMPAGNEMENT_SAUCE RETURN NUMBER IS retour NUMBER; BEGIN SELECT SEQ_ACCOMPAGNEMENT_SAUCE.nextval INTO retour FROM dual; return retour; END;







CREATE OR REPLACE FORCE VIEW "AS_DETAILSCOMMANDE_LIB_2"
	as
  select
    dt.id, dt.idmere, art.NOM as PRODUIT, cc.CLIENT as IDCLIENT , dt.QUANTITE, dt.PU, dt.REMISE, dt.OBSERVATION , dt.ETAT
from
    as_detailscommande dt,
    as_produits art,
  	as_commandeclient cc
where
    dt.produit = art.ID and cc.id = dt.idmere and cc.client like '%CASM%' ORDER BY dt.ID;
	

alter table AS_TYPEPRODUIT  add CONSTRAINT typep_desc_sous_categorie_id  FOREIGN KEY (DESCE) REFERENCES AS_SOUSCATEGORIE(ID) ;




CREATE TABLE "AS_SOUSCATEGORIE" (
	ID VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	VAL VARCHAR2(100 BYTE), 
	DESCE VARCHAR2(100 BYTE), 
PRIMARY KEY (ID)
);
CREATE SEQUENCE  "SEQ_AS_SOUSCATEGORIE"  MINVALUE 1 MAXVALUE 9999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;   
create or replace FUNCTION GETSEQ_AS_SOUSCATEGORIE RETURN NUMBER IS retour NUMBER; BEGIN SELECT SEQ_AS_SOUSCATEGORIE.nextval INTO retour FROM dual; return retour; END;



CREATE OR REPLACE FORCE VIEW "AS_DETAILS_COMMANDE_DESC_ETAT" ("ID", "IDMERE", "PRODUIT", "QUANTITE", "PU", "REMISE", "OBSERVATION", "IDPRODUIT","ETAT","STATUE" ) AS 
  select
    dt.ID, dt.IDMERE, pr.NOM, dt.QUANTITE, dt.PU, dt.REMISE, dt.OBSERVATION, dt.PRODUIT , dt.ETAT ,
    CASE 
        WHEN dt.ETAT = 9 THEN 'CLOTURE'
        WHEN dt.ETAT = 10 THEN 'FAIT'
        WHEN dt.ETAT = 11 THEN 'VALIDER'
        WHEN dt.ETAT = 40 THEN 'PAYER'
        WHEN dt.ETAT = 20 THEN 'LIVRE'
    END as STATUE
from
    AS_DETAILSCOMMANDE dt,
    AS_PRODUITS pr
where
    dt.produit = pr.id ORDER BY dt.ID;



CREATE OR REPLACE FORCE VIEW "AS_DETAILS_COMMANDE_DESC_ETAT2" ("ID", "IDMERE", "PRODUIT", "QUANTITE", "PU", "REMISE", "OBSERVATION", "IDPRODUIT","ETAT") AS 
  select
    dt.ID, dt.IDMERE, pr.NOM, dt.QUANTITE, dt.PU, dt.REMISE, dt.OBSERVATION, dt.PRODUIT ,
    CASE 
        WHEN dt.ETAT = 9 THEN 'CLOTURE'
        WHEN dt.ETAT = 10 THEN 'FAIT'
        WHEN dt.ETAT = 11 THEN 'VALIDER'
        WHEN dt.ETAT = 40 THEN 'PAYER'
        WHEN dt.ETAT = 20 THEN 'LIVRE'
    END as ETAT
from
    AS_DETAILSCOMMANDE dt,
    AS_PRODUITS pr
where
    dt.produit = pr.id ORDER BY dt.ID;


  CREATE OR REPLACE FORCE VIEW "AS_DETAILSCOMMANDE_LIB_2"
	as
  select
    dt.id, dt.idmere, art.NOM as PRODUIT, cc.CLIENT as IDCLIENT , dt.QUANTITE, dt.PU, dt.REMISE, dt.OBSERVATION , cc.ETAT
from
    as_detailscommande dt,
    as_produits art,
  	as_commandeclient cc
where
    dt.produit = art.ID and cc.id = dt.idmere and cc.client like '%CASM%' ORDER BY dt.ID;
	
	


  CREATE OR REPLACE FORCE VIEW "AS_DETAILS_COMMANDE_DESC_ETAT" ("ID", "IDMERE", "PRODUIT", "QUANTITE", "PU", "REMISE", "OBSERVATION", "IDPRODUIT","ETAT") AS 
  select
    dt.ID, dt.IDMERE, pr.NOM, dt.QUANTITE, dt.PU, dt.REMISE, dt.OBSERVATION, dt.PRODUIT , dt.ETAT
from
    AS_DETAILSCOMMANDE dt,
    AS_PRODUITS pr
where
    dt.produit = pr.id ORDER BY dt.ID;


CREATE OR REPLACE FORCE VIEW "ALLOSAKAFO"."TABLECLIENTVUE2" ("ID", "VAL", "DESCE") AS 
  select ID as id,NOM as val,NOM as desce
from AS_CLIENT where ID like 'CASM%';

insert into as_client( id ,nom )  values ('CASM001','Table 1');
insert into as_client( id ,nom )  values ('CASM002','Table 2');
insert into as_client( id ,nom )  values ('CASM003','Table 3');
insert into as_client( id ,nom )  values ('CASM004','Table 4');
insert into as_client( id ,nom )  values ('CASM005','Table 5');
insert into as_client( id ,nom )  values ('CASM006','Table 6');
insert into as_client( id ,nom )  values ('CASM007','Table 7');
insert into as_client( id ,nom )  values ('CASM008','Table 8');
insert into as_client( id ,nom )  values ('CASM009','Table 9');
insert into as_client( id ,nom )  values ('CASM010','Table 10');
insert into as_client( id ,nom )  values ('CASM011','Table 11');
insert into as_client( id ,nom )  values ('CASM012','Table 12');
insert into as_client( id ,nom )  values ('CASM013','Table 13');
insert into as_client( id ,nom )  values ('CASM014','Table 14');
insert into as_client( id ,nom )  values ('CASM015','Table 15');
insert into as_client( id ,nom )  values ('CASM016','Table 16');
insert into as_client( id ,nom )  values ('CASM017','Table 17');
insert into as_client( id ,nom )  values ('CASM018','Table 18');
insert into as_client( id ,nom )  values ('CASM019','Table 19');
insert into as_client( id ,nom )  values ('CASM020','Table 20');
