
 
   CREATE OR REPLACE FORCE VIEW "VUE_CMD_CLT_DETS_TYPP_PRIO" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE", "PRIORITER") AS 
  select
      det.id ,
      tab.val as nomtable,
      tp.val as typeproduit,cmd.datecommande,cmd.heureliv,
      prod.nom as produit,
      det.quantite,det.pu,
      (det.quantite*det.pu) as montant,
      det.etat,
      accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
      det.prioriter
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join tableclientvue2 tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande asc,cmd.heureliv asc, det.prioriter asc, det.etat desc;
 









  CREATE OR REPLACE FORCE VIEW "AS_COMMANDECLIENT_LIBELLE_ID" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "VENTE", "ETAT", "SECT") AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM as responsable, cmd.client as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    vente 
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur 
    and cmd.vente=vente.id(+) 
    order by cmd.id desc;
 




  CREATE OR REPLACE FORCE VIEW "AS_DETAILSCOMMANDE_LIB_SAUCE" ("ID", "IDMERE", "PRODUIT", "IDACCOMPAGNEMENT", "PHOTO", "IDCLIENT", "QUANTITE", "PU", "REMISE", "OBSERVATION", "ETAT") AS 
  select
    dt.id, dt.idmere, art.NOM as PRODUIT , CONCAT ( sac.idsauce , sac.IDACCOMPAGNEMENT ) as IDACCOMPAGNEMENT , art.PHOTO , cc.CLIENT as IDCLIENT , dt.QUANTITE, dt.PU,  dt.QUANTITE * dt.PU  ,  dt.OBSERVATION , dt.ETAT
from 
as_detailscommande dt 
join as_commandeclient cc on cc.id = dt.idmere 
join as_produits art on dt.produit = art.ID 
left join AS_ACCOMPAGNEMENT_SAUCE_LIB sac on sac.id = dt.IDACCOMPAGNEMENTSAUCE  where cc.client like '%CASM%' ORDER BY dt.ID;
 
 
 
 
  CREATE OR REPLACE FORCE VIEW AS_COMMANDECLIENT_LIBELLE_ID AS 
  select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM as responsable, cmd.client as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    vente 
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur 
    and cmd.vente=vente.id(+) 
    order by cmd.id desc;
 




  CREATE OR REPLACE FORCE VIEW AS_DETAILSCOMMANDE_LIB_SAUCE AS 
  select
    dt.id, dt.idmere, art.NOM as PRODUIT , CONCAT ( sac.idsauce , sac.IDACCOMPAGNEMENT ) as IDACCOMPAGNEMENT , art.PHOTO , cc.CLIENT as IDCLIENT , dt.QUANTITE, dt.PU,  dt.QUANTITE * dt.PU  ,  dt.OBSERVATION , dt.ETAT
from 
as_detailscommande dt 
join as_commandeclient cc on cc.id = dt.idmere 
join as_produits art on dt.produit = art.ID 
left join AS_ACCOMPAGNEMENT_SAUCE_LIB sac on sac.id = dt.IDACCOMPAGNEMENTSAUCE  where cc.client like '%CASM%' ORDER BY dt.ID;
 
