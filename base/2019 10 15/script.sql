create view achat_pu_moyenne as	
	select 
		idingredient,
		avg(prixunitaire) as pu_moyenne
	from 
		achat
		group by idingredient
		

create view ingredient_produit as
	select
		ingr.id, ingr.libelle, ingr.unite, rec.idproduits ,sum(rec.quantite) as quantite
	from
		as_ingredients ingr
		join as_recette rec on rec.idingredients = ingr.id
	group by ingr.id, ingr.libelle, ingr.unite, rec.idproduits

create view vue_besoin_quotidien as
	select
		ingr.id, ingr.libelle, unit.val as unite,
		(ingr.quantite * bsn.quantite) as quantite
	from ingredient_produit ingr
	join besoin bsn on bsn.idproduit = ingr.idproduits
	join unite unit on unit.id = ingr.unite
		
create view vue_besoin_quotidien_groupe as 
	select 
		besoin.id, besoin.libelle, besoin.unite, sum(besoin.quantite) as quantite
	from vue_besoin_quotidien besoin
	group by besoin.id, besoin.libelle, besoin.unite
	
***************************************************************************************************************
create view vue_ingredient_fournisseur as
	select
		ingr.id as id,
		ingr.libelle as idingredient,
		unite.val as idunite,
		nvl(achatpu.pu_moyenne,0) as prixunitaire,
		'--' as idfournisseur,
		'..' as fournisseur
	from
		as_ingredients ingr
    join unite unite on unite.id = ingr.unite
		left join achat_pu_moyenne achatpu on ingr.id = achatpu.idingredient;
		
