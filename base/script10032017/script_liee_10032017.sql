CREATE OR REPLACE FORCE VIEW AS_COMMANDECLIENT_LIBELLE2 ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select distinct
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE",cmd."ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",cmd."ETAT",  liv.responsable, mtnt.montant, cmd.secteur
from
    as_commandeclient_libelle cmd,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    liv.commande (+)=cmd.id and
    mtnt.id = cmd.id;
	
CREATE OR REPLACE FORCE VIEW AS_COMMANDECLIENT_LIBFAIT ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select distinct 
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.CLIENT ,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant, cmd.secteur
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and liv.commande (+)=cmd.id
    and mtnt.id = cmd.id
    and cmd.etat < 20
    and cmd.etat >= 5;

CREATE OR REPLACE FORCE VIEW AS_COMMANDECLIENT_LIVNP ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR") AS 
  select distinct 
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM, cmd.client,
    tp.VAL, cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, cmd.ETAT, liv.responsable, mtnt.montant, cmd.secteur
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    (select as_livraison_complet.commande, res.NOM ||' '||res.PRENOM as responsable from as_livraison_complet join as_responsable res on as_livraison_complet.responsable = res.id) liv,
    montantparcommande mtnt
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and mtnt.id = cmd.id
     and liv.commande (+)=cmd.id
    and cmd.etat = 20;

/* actif = 1, inactif = 0*/
alter table as_ingredients add actif integer default 1;

CREATE OR REPLACE FORCE VIEW AS_INGREDIENTS_LIBELLE2 ("ID", "LIBELLE", "SEUIL", "UNITE", "QUANTITEPARPACK", "PU") AS 
  select as_ingredients.id, as_ingredients.libelle, as_ingredients.seuil, as_unite.val as unite, as_ingredients.quantiteparpack, as_ingredients.pu
from as_ingredients join as_unite on as_ingredients.unite = as_unite.id where as_ingredients.actif=1 ;

create or replace view as_mvt_stock_libfille as 
select igr.libelle as ingredients, igr.UNITE,  sum(entree) as entree, sum(sortie) as sortie, sum(entree)-sum(sortie) as reste, daty, 0 as report 
from as_mvt_stock_fille fille 
    join as_mvt_stock mere on fille.idmvtstock=mere.id
    join as_ingredients_libelle igr on igr.ID = fille.ingredients 
group by igr.libelle, daty, igr.UNITE;

insert into menudynamique values ('ASM00008035', 'Mouvement de stock (Ingredients)', 'fa fa-list', '/allosakafo-war/pages/module.jsp?but=stock/mvtStock/mvtStockFille-etat.jsp', 5, 3, 'ASM0000803');