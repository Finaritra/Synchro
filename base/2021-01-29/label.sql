INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDBOI00019', 'Johnny Walker Black Label 100cl', 'Johnny Walker Black Label 100cl', null,'TPD00001','1','0',null,null,null,null,'350000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRB00021', 'PRDBOI00019', TO_DATE('10-01-2021','DD-MM-YYYY'), 250000);
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0028', 'Johnny Walker Black Label 100cl', 'UNT00004', 1, 0, 'CI00001');
