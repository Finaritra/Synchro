
	  CREATE OR REPLACE VIEW FFLETTRE_ENTITE AS 
	  SELECT ff.id, ff.daty, c.nom as idFournisseur, ff.idtva, ff.montantttc,ff.remarque,
	  ff.dateemission, ff.designation, ff.iddevise, ff.numfact, ff.datyecheance, ff.entite, CASE
	   WHEN ff.etat=1 THEN 'CREER'
	   WHEN ff.etat=11 THEN 'VALIDER'
	   ELSE 'AUTRE'
	  END AS ETAT
	FROM facturefournisseurmontant ff, fournisseurProduits c where c.id (+)= ff.IDFOURNISSEUR;


CREATE OR REPLACE VIEW ETATFACTURE_ENTITE AS 
  select f.ID,f.DATY,f.IDFOURNISSEUR,f.IDTVA,f.MONTANTTTC,f.REMARQUE,f.DATEEMISSION,f.DESIGNATION,f.IDDEVISE,f.NUMFACT,f.DATYECHEANCE,f.ETAT,f.ENTITE ,cast (nvl(op.montant,0) as NUMBER(10,2)) as montantPaye,cast ((f.montantTTC-nvl(op.montant,0)) as NUMBER(10,2)) as reste from FFLettre_entite f,ORDONNERPAYEMENTGROUPEFACT op
where f.ID=op.DED_ID(+);


CREATE OR REPLACE VIEW ETATFACTURE_ENTITEVALIDE AS 
select ID,DATY,IDFOURNISSEUR,IDTVA,MONTANTTTC,REMARQUE,DATEEMISSION,DESIGNATION,IDDEVISE,NUMFACT,DATYECHEANCE,ETAT,ENTITE,MONTANTPAYE,CAST(RESTE as NUMBER(12,2)) as RESTE from ETATFACTURE_ENTITE where etat='VALIDER';