  CREATE OR REPLACE VIEW BESOINLIBPOINT ("ID", "IDPRODUIT", "REMARQUE", "QUANTITE") AS 
  select
    bes.id,
    prd.nom as idproduit,
    p.val as remarque,
    bes.quantite
    from besoin bes
    LEFT join produitEtIngredient prd on prd.id=bes.idproduit
    left join point p on p.id=bes.remarque ;
