

CREATE OR REPLACE  VIEW FABRICATIONLIBPOINT (ID, DATY, INGREDIENT, QTE, REMARQUE, DEPOT,  ETAT) AS 
select
	fab.id,
    fab.Daty,
    ing.LIBELLE as ingredient,
    fab.qte,
    fab.Remarque,
	p.val AS depot,
    fab.Etat
from fabrication fab
join AS_INGREDIENTS ing on ing.ID=fab.Ingredient
LEFT JOIN point p ON  fab.DEPOT=p.id;