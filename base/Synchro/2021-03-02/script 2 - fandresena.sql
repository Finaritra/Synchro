CREATE OR REPLACE  VIEW ReservationPointLib 
(ID, SOURCE, NBPERSONNE, DATESAISIE, HEURESAISIE, DATERESERVATION, HEUREDEBUTRESERVATION, HEUREFINRESERVATION, IDCLIENT, TELEPHONE, ETAT, TABLENOM, REMARQUE,POINT,idpointlibelle) AS 
  select
            reserv.id,
            reserv.SOURCE,
            reserv.NBPERSONNE,
            reserv.DATESAISIE,
            reserv.HEURESAISIE,
            reserv.DATERESERVATION,
            reserv.HEUREDEBUTRESERVATION,
            reserv.HEUREFINRESERVATION,
            clt.nom ||' '||clt.PRENOM as idclient,
            clt.TELEPHONE,
            reserv.ETAT,
            tab.VAL as tablenom,
            reserv.remarque,
            reserv.point,
            po.val as idpointlibelle
        from AS_RESERVATION reserv
        left join RESA_TABLE RT on reserv.ID = RT.IDRESERVATION
        left join AS_CLIENT clt on clt.ID=reserv.IDCLIENT
        left join TABLECLIENTVUE2 tab on tab.id=rt.IDTABLE
        left join point po on po.id=reserv.point;

create or replace view ReservationLibtous as
select *from ReservationPointLib ;

CREATE OR REPLACE  VIEW RESERVATIONLIBCREE AS 
  SELECT*from RESERVATIONLIBTOUS where etat = 1;

    CREATE OR REPLACE  VIEW ReservationLibvalide  AS 
  SELECT*from RESERVATIONLIBTOUS where etat = 11;

  CREATE OR REPLACE  VIEW RESERVATIONLIBEFFECTUE as 
  select * from  RESERVATIONLIBTOUS where ETAT=12;