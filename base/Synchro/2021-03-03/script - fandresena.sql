-- Ovaina par point
ALTER TABLE AS_LIVRAISON ADD POINT VARCHAR2(100) DEFAULT 'defaut';
ALTER TABLE AS_LIVRAISON ADD FOREIGN KEY (point) REFERENCES POINT(ID);
commit;
