create table indisponibilite (
	id varchar(50) primary key,
	idpoint varchar(50),
	idproduit varchar(50),
	foreign key(idpoint) references point(id),
	foreign key(idproduit) references as_produits(id)
);

CREATE SEQUENCE seq_indisponibilite
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION GETSEQINDISPONIBILITE
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seq_indisponibilite.nextval INTO retour FROM dual;
  return retour;
END;
