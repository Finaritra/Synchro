-- Union produit et  points
create or replace view produits_point as select pt.id as idpoint, pt.val, prod.designation, prod.id as id, prod.idtypeproduit, prod.idsouscategorie, prod.nom, prod.pa, prod.photo, prod.poids, prod.prixl, prod.pu, prod.typeproduit 
from AS_PRODUITTYPE_LIBELLE prod, point pt;

--Left join produits_points - indisponibilite
create or replace view disponibilite_produit as 
    select  prod.id, prod.idpoint, prod.val as point, designation, nom, pa, photo , prod.idtypeproduit, typeproduit, prod.idsouscategorie, poids, prixl, pu, indisp.idpoint as pointindisp, 
case when indisp.idpoint is null then 'DISPONIBLE' else 'INDISPONIBLE' end as calorie, idproduit
from produits_point prod left join indisponibilite indisp on prod.idpoint = indisp.idpoint and prod.id = indisp.idproduit;

commit;
