CREATE OR REPLACE VIEW FACTUREFOURNISSEURLETTREMTT AS 
  SELECT ff.id, ff.daty, c.nom as idFournisseur, ff.idtva,cast (ff.montantttc as NUMBER) as montantttc,ff.remarque,
  ff.dateemission, ff.designation, ff.iddevise, ff.numfact, ff.datyecheance,p.val as entite, p.id as identite, CASE
   WHEN ff.etat=1 THEN 'CREER'
   WHEN ff.etat=11 THEN 'VALIDER'
   ELSE 'AUTRE'
  END AS ETAT
FROM facturefournisseurmontant ff, fournisseurProduits c, point p where c.id (+)= ff.IDFOURNISSEUR and ff.entite=p.id;
  
  
CREATE OR REPLACE VIEW ETATFACTUREMONTANT AS 
  SELECT f."ID",
          f."DATY",
          f."IDFOURNISSEUR",
          f."IDTVA",
          CAST (f."MONTANTTTC" AS NUMBER (18, 2)) AS MONTANTTTC,
          f."REMARQUE",
          f."DATEEMISSION",
          f."DESIGNATION",
          f."IDDEVISE",
          f."NUMFACT",
          f."DATYECHEANCE",
          f."ETAT",
          CAST (NVL (op.montant, 0) AS NUMBER (18, 2)) AS montantPaye,
          CAST ( (f.montantTTC - NVL (op.montant, 0)) AS NUMBER (18, 2))
             AS reste,
          f.entite,
          f.identite
     FROM FACTUREFOURNISSEURLETTREMTT f, ORDONNERPAYEMENTGROUPEFACT op
    WHERE f.ID = op.DED_ID(+);
    
    
 