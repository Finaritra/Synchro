CREATE OR REPLACE VIEW VUE_CMD_CLT_DETAIL_TYPEP_REST AS 
  select
det.id,
tab.nom as nomtable,
tp.val as typeproduit,
cmd.datecommande,cmd.heureliv,cmd.point as restaurant,pt.val as restaurantlib, cmd.CLIENT as idclient,
prod.nom as produit,
det.quantite,det.pu,
(det.quantite*det.pu) as montant,
det.etat,
accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join point pt on pt.id = cmd.point
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;


CREATE OR REPLACE FORCE VIEW LIVRAISONDETAIL AS 
  select
cmdcl.id , cmdcl.NOMTABLE , cmdcl.PRODUIT, cmdcl.RESTAURANT,cmdcl.RESTAURANTLIB, cmdcl.ACCO_SAUCE as SauceAcoompagnement , cmdcl.DATECOMMANDE as DateCommande,
cmdcl.HEURELIV as HeureCommande , liv.DATY as DateLivraison, liv.HEURE as HeureLivraison,
cast( ( 60 *( to_number( substr(liv.HEURE,1,2) ) - to_number( substr(cmdcl.HEURELIV,1,2) ) ) ) + (  to_number( substr(liv.HEURE,4,2) ) - to_number( substr(cmdcl.HEURELIV,4,2) ) )  as number(20,2) )  as difference
  from
AS_LIVRAISON liv 
join  VUE_CMD_CLT_DETAIL_TYPEP_REST cmdcl on cmdcl.id = liv.COMMANDE;
 