  CREATE OR REPLACE VIEW ASMVTSTOCKPOINTNONANNULE AS 
  select 
    mvt.id,
    mvt.daty,
    mvt.designation,
    mvt.typemvt,
    mvt.observation,
    pt.val as depot,
    mvt.typebesoin,
    mvt.etat,
    mvt.fournisseur,
    mvt.beneficiaire,
    mvt.numbc,
    mvt.numbs
    from as_mvt_stock mvt
    join point pt on mvt.depot = pt.id
  where mvt.etat>=1;