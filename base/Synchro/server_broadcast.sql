delete from server_broadcast;
insert into server_broadcast (id, nom, adresse_ip, utilisateur, mdp, driver, service_name, port) values ('BRD001', 'Ampasa', 'localhost', 'allosakafoamp', 'allosakafo', 'oracle.jdbc.driver.OracleDriver', 'orcl', 1521);
insert into server_broadcast (id, nom, adresse_ip, utilisateur, mdp, driver, service_name, port) values ('BRD002', 'Ankoro', 'localhost', 'allosakafoank', 'allosakafo', 'oracle.jdbc.driver.OracleDriver', 'orcl', 1521);
insert into server_broadcast (id, nom, adresse_ip, utilisateur, mdp, driver, service_name, port) values ('BRD003', 'Bel', 'localhost', 'allosakafoba', 'allosakafo', 'oracle.jdbc.driver.OracleDriver', 'orcl', 1521);
insert into server_broadcast (id, nom, adresse_ip, utilisateur, mdp, driver, service_name, port) values ('BRD004', 'Bypass', 'localhost', 'allosakafobp', 'allosakafo', 'oracle.jdbc.driver.OracleDriver', 'orcl', 1521);
commit;