

  CREATE OR REPLACE VIEW AS_DETAILSCOMMANDE_FM (ID, IDMERE, PRODUIT, QUANTITE, PU, REMISE, OBSERVATION, ETAT, IDACCOMPAGNEMENTSAUCE, PRIORITER, REVIENT, DATESAISIE, DATECOMMANDE, NUMCOMMANDE, RESPONSABLE, CLIENT, TYPECOMMANDE, DATELIV, ADRESSELIV, HEURELIV, DISTANCE, REMARQUE, SECTEUR, QUARTIER, VENTE, POINT, ETATMERE) AS 
  SELECT 
  asdccl.ID,asdccl.IDMERE,asdccl.PRODUIT,asdccl.QUANTITE,asdccl.PU,asdccl.REMISE,asdccl.OBSERVATION,asdccl.ETAT,asdccl.IDACCOMPAGNEMENTSAUCE,asdccl.PRIORITER,asdccl.REVIENT,
  asccl.DATESAISIE,
  asccl.DATECOMMANDE,
  asccl.NUMCOMMANDE,
  asccl.RESPONSABLE,
  asccl.CLIENT,
  asccl.TYPECOMMANDE,
  asccl.DATELIV,
  asccl.ADRESSELIV,
  asccl.HEURELIV,
  asccl.DISTANCE,
  asccl.REMARQUE,
  asccl.SECTEUR,
  asccl.QUARTIER,
  asccl.VENTE,
  asccl.POINT,
  asccl.ETAT AS ETATMERE
FROM AS_COMMANDECLIENT asccl
JOIN AS_DETAILSCOMMANDE asdccl ON asccl.id = asdccl.idmere;
 


commit;