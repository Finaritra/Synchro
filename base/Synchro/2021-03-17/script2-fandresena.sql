CREATE OR REPLACE VIEW LIVRAISONAPRESCOMMANDEPOINT AS 
SELECT 
  llcmg.*,
  pt.ID AS POINT,
  pt.val AS POINTLIB
FROM LIVRAISONLIBCOMPLETMEREGROUPE llcmg
LEFT JOIN AS_COMMANDECLIENT acc ON llcmg.IDCOMMNADEMERE = acc.ID
LEFT JOIN POINT pt ON acc.POINT = pt.ID;


 CREATE OR REPLACE VIEW AS_MVT_STOCKLIBPOINT AS 
  SELECT mvt.id,
          mvt.daty,
          mvt.designation,
          mvt.typemvt,
          mvt.observation,
          pt.val as depot,
          mvt.typebesoin,
          mvt.etat,
          mvt.fournisseur,
          mvt.beneficiaire,
          mvt.numbc,
          det.PRODUIT AS numbs
     FROM AS_MVT_STOCK mvt
     LEFT JOIN POINT pt ON mvt.depot = pt.id
     LEFT JOIN AS_DETAILSCOMMANDE_LIB det ON mvt.NUMBC = det.id;