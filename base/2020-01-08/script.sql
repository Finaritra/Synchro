create or replace view inventairemerelibcomplet as
    select
        im.*,
        tpi.val as valtypeinventaire,
        tpi.desce as descetypeinventaire,
        idet.id as idinventairedetails,
        idet.idmere, idingredient, iduser, quantiter, explication, etatinventairedetails, libelleingredient, refuser, loginuser, pwduser, nomuser, adruser, teluser, idrole
    from inventairemere im
        join typeinventaire tpi on tpi.id=im.type
        join inventairedetailslibcomplet idet on idet.idmere=im.id;
		
create or replace view as_recette_libcomplet as
    select
        rc.id, idproduits, idingredients, quantite, rc.unite as idunite,
        igdr.libelle as libelleingredient,
        igdr.seuil, igdr.unite, quantiteparpack, pu, actif, photo, calorie, durre,
        unit.val as valunite,
        unit.desce as desceunite
    from as_recette rc
        join as_ingredients igdr on rc.idingredients = igdr.id
        join as_unite unit on rc.unite = unit.id;
		
create or replace view besoinlibcomplet as
    select
        b.*,
        prod.NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE, NUMEROPLAT
    from besoin b
        join as_produits prod on prod.id=b.idproduit;
		
create or replace view besoinlib as
    select
        id,
        nom as idproduit,
        remarque,
        quantite
    from besoinlibcomplet;