insert into as_ingredients (id, libelle, seuil, unite, quantiteparpack, pu, compose, categorieingredient) values ('EP0001', 'Epice Pho en portion', '10', 'UNT00005', 1, 5729, 1, 'CI00007');
-- grains de coriandre	21	g INGS000786
-- grain de poivre	24	g INGS000787
-- cardamome noir	20	g INGS000774
-- anis etoilé	20	g INGS000768
-- canelle	30	g INGS000773
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCE0001', 'EP0001', 'INGS000786', 21, 'UNT00001');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCE0002', 'EP0001', 'INGS000787', 24, 'UNT00001');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCE0003', 'EP0001', 'INGS000774', 20, 'UNT00001');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCE0004', 'EP0001', 'INGS000768', 20, 'UNT00001');
insert into as_recette (id, idproduits, idingredients, quantite, unite) values ('RCE0005', 'EP0001', 'INGS000773', 30, 'UNT00001');
