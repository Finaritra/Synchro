rollback;
-- Kojakoja ao @ Salle BA ---------
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0011', 'Machine à café Lavazza', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0012', 'Réfrigérateur', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0013', 'Ordinateur', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0014', 'Imprimante', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0015', 'Ventilateur', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0016', 'Tablette', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0017', 'Onduleur', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0018', 'Baffle (Bose)', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0019', 'Television', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0020', 'Chargeur ordinateur', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0021', 'Telephone', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0022', 'Tele commande', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0023', 'Chargeur tablette', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0024', 'Chargeur telephone', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0025', 'Capteur recepteur bleu', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0026', 'Alimentation Bose', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0027', 'RCA Bose', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0028', 'Batterie panneaux solaires', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0029', 'Multi-prise', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0030', 'Thermomètre frontal', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0031', 'Prise BTE', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0032', 'Saliere', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0033', 'Menu carte', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0034', 'Vase table', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0035', 'Chaise rouge', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0036', 'Chaise noire (box)', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0037', 'Table 2 pers', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0038', 'Table 4 pers', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0039', 'Agraffeuse', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0040', 'Chaise individuelle', 'UNT00005', 'CI011');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0041', 'Boite caisse', 'UNT00005', 'CI011');

commit;
--- Ustensiles de cuisine BA ------------
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0042', 'Vilany rice cooker', 'UNT00005', 'CI010');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0043', 'Cuvette carrée en plastique', 'UNT00005', 'CI010');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0044', 'Bac a legumes', 'UNT00005', 'CI010');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0045', 'Fatana gaz', 'UNT00005', 'CI010');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0046', 'Fatana gasy (charbon)', 'UNT00005', 'CI010');
insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0047', 'Plateau en plastique GM', 'UNT00005', 'CI010');
--insert into as_ingredients (id, libelle, unite, categorieingredient) values ('IGN0048', 'Fitotokena electrique', 'UNT00005', 'CI010');
commit;