delete from MVTCAISSE where 1<2;
delete from VENTEFACTURE where 1<2;
delete from ANNULATIONAVOIR where 1<2;
delete from avoir where 1<2;
delete from achat where 1<2;
delete from VISAFACTURECLIENT where 1<2;
delete from VISAFACTURECLIENTPROFORMA where 1<2;
delete from ORFCLC where 1<2;
delete  from PUBFACTURE where 1<2;
delete from FACTUREFILLE where 1<2;
delete from FACTUREFOURNISSEURPREVISION where 1<2;
delete from FACTUREFOURNISSEUR where 1<2;
delete from FACTUREFILLEPROFORMA where 1<2;
delete from FACTURECLIENTPROFORMA where 1<2;
delete from FACTUREPROFFOURNISSEUR where 1<2;
delete from FFOURNISSEUR where 1<2;
delete from FACTUREMERE where 1<2;
delete from FACTUREMEREPROFORMA where 1<2;
delete from FACTURECLIENTLC where 1<2;
delete from FACTURECLIENT where 1<2;


alter table FACTUREFILLE add etat int;
alter table FACTUREFOURNISSEURPREVISION add etat int;
alter table FACTUREFILLEPROFORMA add etat int;
alter table FACTURECLIENTPROFORMA add etat int;
alter table FACTUREPROFFOURNISSEUR add etat int;
alter table FFOURNISSEUR add etat int;
alter table FACTUREMERE add etat int;
alter table FACTUREMEREPROFORMA add etat int;
alter table FACTURECLIENT add etat int;



create table FACTUREFOURNISSEURLC
(
    ID          VARCHAR2(50) not null
        primary key,
    ID1         VARCHAR2(50),
    ID2         VARCHAR2(50),
    REMARQUE    VARCHAR2(100),
    MONTANTMERE NUMBER(30, 2) default 0,
    ETAT        NUMBER        default 0
);

CREATE SEQUENCE SEQFACTUREFOURNISSEURLC
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQFACTUREFOURNISSEURLC
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQFACTUREFOURNISSEURLC.nextval INTO retour FROM dual;
  return retour;
END;

    create table FACTURELIGNECREDIT
(
    ID          VARCHAR2(50) not null
        constraint FACTURELIGNECREDIT_PK
            primary key,
    ID1         VARCHAR2(50),
    ID2         VARCHAR2(50),
    REMARQUE    VARCHAR2(100),
    MONTANTMERE NUMBER(10, 2) default 0,
    ETAT        NUMBER        default 0
);

CREATE SEQUENCE SEQFACTURELIGNECREDIT
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQFACTURELIGNECREDIT
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQFACTURELIGNECREDIT.nextval INTO retour FROM dual;
  return retour;
END;

    create table FOURNISSEUR
(
    ID_FOURNISSEUR VARCHAR2(50) not null
        constraint PK_FOURNISSEUR
            primary key,
    NOM            VARCHAR2(100),
    ADRESSE        VARCHAR2(100) default '-',
    TELEPHONE      VARCHAR2(100) default '-',
    FAX            VARCHAR2(100) default '-',
    NUMSTAT        VARCHAR2(100) default '-',
    NIF            VARCHAR2(100) default '-',
    RC             VARCHAR2(100) default '-',
    TP             VARCHAR2(100) default '-',
    QUITTANCE      VARCHAR2(100) default '-',
    etat int
);

CREATE SEQUENCE SEQFOURNISSEUR
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQFOURNISSEUR
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQFOURNISSEUR.nextval INTO retour FROM dual;
  return retour;
END;
----------------------------------------------------------------------------
create table DED
(
    ID          VARCHAR2(50) not null
        constraint PK_DED
            primary key,
    DESIGNATION VARCHAR2(50),
    DATY        DATE,
    MONTANTHT   NUMBER(10, 2),
    TAXE        NUMBER(10, 2),
    REMARQUE    VARCHAR2(100),
    ETAT        int,
    TIERS       VARCHAR2(20)
        constraint FK_DED_REFERENCE_CLIENT
            references CLIENT,
    IDLIGNE     VARCHAR2(250)
        constraint FK_DED_REFERENCE_LIGNECRE
            references LIGNECREDIT
);
CREATE SEQUENCE SEQDED
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQDED
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQDED.nextval INTO retour FROM dual;
  return retour;
END;

create table LIGNECREDIT
(
    IDLIGNE        VARCHAR2(250) not null
        constraint LLIGNECREDIT_PK
            primary key,
    DESIGNATION    VARCHAR2(250),
    CREDITINITIAL  NUMBER(30, 2),
    CREDITMODIFIER FLOAT,
    MONTANTENG     FLOAT,
    MONTANTVIS     FLOAT,
    MONTANTFAC     FLOAT,
    IDTYPELIGNE    VARCHAR2(250)
        references TYPELCDEPENSETOUS,
    NUMCOMPTE      VARCHAR2(250)
        references COMPTEG,
    IDENTITE       VARCHAR2(250)
        constraint FK03_LIGNECREDIT
            references BENEFICIAIRE,
    IDDIRECTION    VARCHAR2(250)
        constraint FK04_LIGNECREDIT
            references DIRECTION,
    MOIS           VARCHAR2(250)
        constraint FK05_LIGNECREDIT
            references MOIS,
    ANNEE          NUMBER
        constraint FK06_LIGNECREDIT
            references ANNEE,
    PARUTION       VARCHAR2(250),
    constraint LIGNECREDIT_U01
        unique (ANNEE, IDDIRECTION, IDENTITE, IDTYPELIGNE, MOIS, NUMCOMPTE)
);

comment on column LIGNECREDIT.MONTANTVIS is 'montant paye facture';

create index IDXMOISLCR
    on LIGNECREDIT (MOIS);

create index IDXANNEELCR
    on LIGNECREDIT (ANNEE);

CREATE SEQUENCE SEQLIGNECREDIT
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQLIGNECREDIT
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQLIGNECREDIT.nextval INTO retour FROM dual;
  return retour;
END;


create table LCDETAILS
(
    ID           VARCHAR2(50) not null
        constraint PK_LCDETAILS
            primary key,
    IDLC         VARCHAR2(250)
        constraint FK_LCDETAIL_REFERENCE_LIGNECRE
            references LIGNECREDIT,
    COMPTEDETAIL VARCHAR2(50),
    CREDIT       NUMBER,
    MONTANTENG   NUMBER,
    MONTANTVIS   NUMBER,
    MONTANTFACT  NUMBER,
    ETAT         NUMBER(3)
);
CREATE SEQUENCE SEQLCDETAILS
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQLCDETAILS
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQLCDETAILS.nextval INTO retour FROM dual;
  return retour;
END;

    create table LCDETAILDED
(
    ID          VARCHAR2(50) not null
        constraint PK_DEDFLCDETAIL
            primary key,
    ID1         VARCHAR2(50)
        constraint FK_DEDFLCDE_REFERENCE_LCDETAIL
            references LCDETAILS,
    ID2         VARCHAR2(50)
        constraint FK_DEDFLCDE_REFERENCE_DED
            references DED,
    REMARQUE    VARCHAR2(100),
    MONTANTMERE NUMBER(10, 2),
    ETAT        NUMBER
)
;
CREATE SEQUENCE SEQLCDETAILDED
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQLCDETAILDED
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQLCDETAILDED.nextval INTO retour FROM dual;
  return retour;
END;


    create table LCDETAILMVTCAISSE
(
    ID          VARCHAR2(50) not null
        constraint PKMVTCAISSELCDETAIL
            primary key,
    ID1         VARCHAR2(50)
        constraint FK_MVTCAISS_REFERENCE_LCDETAIL
            references LCDETAILS,
    ID2         VARCHAR2(50)
        constraint FK_MVTCAISS_REFERENCE_MVTCAISS
            references MVTCAISSE,
    REMARQUE    VARCHAR2(100),
    MONTANTMERE NUMBER(10, 2),
    ETAT        NUMBER
);

CREATE SEQUENCE SEQLCDETAILMVTCAISSE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQLCDETAILMVTCAISSE
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQLCDETAILMVTCAISSE.nextval INTO retour FROM dual;
  return retour;
END;


    create table LCDETAILSRECETTE
(
    ID           VARCHAR2(50) not null
        constraint PK_LCDETAILSRECETTE
            primary key,
    IDLC         VARCHAR2(250)
        constraint FK_LCDET_REFERENCE_LIGNECRER
            references LIGNECREDITRECETTE,
    COMPTEDETAIL VARCHAR2(50),
    CREDIT       NUMBER(10, 2),
    MONTANTENG   NUMBER(10, 2),
    MONTANTVIS   NUMBER(10, 2),
    MONTANTFACT  NUMBER(10, 2),
    ETAT         NUMBER(3)
);

CREATE SEQUENCE SEQLCDETAILSRECETTE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQLCDETAILSRECETTE
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQLCDETAILSRECETTE.nextval INTO retour FROM dual;
  return retour;
END;


create table LCDETAILRECETTEOR
(
    ID          VARCHAR2(50) not null
        constraint PK_OPLCDETAILRECETTEOR
            primary key,
    ID1         VARCHAR2(50)
        constraint FK_OPLCDETAROR_REF_LCDETAIL
            references LCDETAILSRECETTE,
    ID2         VARCHAR2(50)
        constraint FK_OPLCDETRECETTEOR_REF_ORDR
            references ORDONNERRECETTE,
    REMARQUE    VARCHAR2(100),
    MONTANTMERE NUMBER(10, 2),
    ETAT        NUMBER
);

CREATE SEQUENCE SEQLCDETAILRECETTEOR
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQLCDETAILRECETTEOR
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQLCDETAILRECETTEOR.nextval INTO retour FROM dual;
  return retour;
END;
------------------------------------------------------------
create table RAPPRO_LETTRE
(
    ID           VARCHAR2(50) not null
        primary key,
    LETTRE       VARCHAR2(50) not null,
    DATELETTRAGE DATE,
    TYPELETTRAGE VARCHAR2(255)
);
CREATE SEQUENCE SEQRAPPRO_LETTRE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQRAPPRO_LETTRE
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQRAPPRO_LETTRE.nextval INTO retour FROM dual;
  return retour;
END;
    create table RAPPRO_SOUS_MVTCAISSE
(
    ID             VARCHAR2(100) not null
        primary key,
    IDRAPPROLETTRE VARCHAR2(100),
    IDMVTCAISSE    VARCHAR2(100)
);

CREATE SEQUENCE SEQRAPPRO_SOUS_MVTCAISSE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQRAPPRO_SOUS_MVTCAISSE
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQRAPPRO_SOUS_MVTCAISSE.nextval INTO retour FROM dual;
  return retour;
END;

    create table RAPPRO_SOUS_RELEVE
(
    ID             VARCHAR2(100) not null
        primary key,
    IDRAPPROLETTRE VARCHAR2(100),
    IDRELEVE       VARCHAR2(100)
);

CREATE SEQUENCE SEQRAPPRO_SOUS_RELEVE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQRAPPRO_SOUS_RELEVE
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQRAPPRO_SOUS_RELEVE.nextval INTO retour FROM dual;
  return retour;
END;


    create table RAPPROCHEMENT
(
    ID            VARCHAR2(50),
    DATY          DATE,
    DESIGNATION   VARCHAR2(4000),
    IDMOUVEMENT   VARCHAR2(50),
    DATEOPERATION DATE,
    IDRIB         VARCHAR2(50)
);
CREATE SEQUENCE SEQRAPPROCHEMENT
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQRAPPROCHEMENT
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQRAPPROCHEMENT.nextval INTO retour FROM dual;
  return retour;
END;

    -----------------------------------------
    create view FACTURECLIENTLCCOMPTE as
select
  f.id,
  f.id1 as idfacture,
  f.id2 as idlc,
  lc.designation,
  lc.identite as entite,
  f.MONTANTMERE as montant,
  t.DESCE as compte
from
  factureclientlc f,
  lignecreditrecette lc,
  typelcrecettetous t
where
  f.id2 = lc.idligne and lc.idtypeligne = t.id;

create or replace view FACTURECLIENTLCVUE_2 as
SELECT
d.idfacturefournisseur,
d.daty,
cl.nom,
d.idtva,
flc.MONTANTMERE ,
d.remarque,
d.dateemission,
d.designation,
d.iddevise,
d.numfact,
l.designation AS designationlc,
l.creditinitial,
l.creditmodifier,
l.montanteng,
l.montantvis,
l.montantfac,
l.idtypeligne,
l.numcompte,
l.identite,
l.iddirection,
l.mois,
l.annee,
l.parution,
l.idligne,
ag.val,
cl.NUMSTAT,
cl.nif,
cl.ADRESSE,
       idfournisseur,
       montantttc,
       resp
     FROM factureclientvise d,
          factureclientlc flc,
          lcrecettelettre l,
          client cl,agence ag
    WHERE d.idfacturefournisseur = flc.id1(+)
      AND flc.id2 = l.idligne
      AND d.idfournisseur = cl.idclient and flc.etat=1 and d.resp=ag.id
;

create or replace view FACTURECLIENTLCVUE_3 as
SELECT
d.idfacturefournisseur,
d.daty,
cl.idclient,
cl.nom,
cl.telephone,
d.idtva,
flc.MONTANTMERE ,
d.remarque,
d.dateemission,
d.designation,
d.iddevise,
d.numfact,
l.designation AS designationlc,
l.creditinitial,
l.creditmodifier,
l.montanteng,
l.montantvis,
l.montantfac,
l.idtypeligne,
l.numcompte,
l.identite,
l.iddirection,
l.mois,
l.annee,
l.parution,
l.idligne,
ag.val,
cl.NUMSTAT,
cl.nif,
cl.ADRESSE,
       idfournisseur,
       montantttc,
       resp
     FROM factureclientvise d,
          factureclientlc flc,
          lcrecettelettre l,
          client cl,agence ag
    WHERE d.idfacturefournisseur = flc.id1(+)
      AND flc.id2 = l.idligne
      AND d.idfournisseur = cl.idclient and flc.etat=1 and d.resp=ag.id;

create or replace view FACTURECLIENTORGROUPE_2 as
SELECT
fc.idfacturefournisseur,
fc.daty,
fc.idfournisseur,
fc.idtva,
fc.montantttc,
fc.remarque,
fc.dateemission,
fc.designation,
fc.iddevise,
fc.numfact,
fc.designationlc,
fc.creditinitial,
fc.creditmodifier,
fc.montanteng,
fc.montantvis,
fc.montantfac,
fc.idtypeligne,
fc.resp,
fc.identite,
fc.iddirection,
fc.mois,
fc.annee,
fc.parution,
fc.idligne,

cast(sum (nvl(op.montant,0)) as number(20,2)) AS opmontant,
cast(fc.montantttc - sum (nvl(op.montant,0)) as number(20,2)) AS reste,
fm.datelimitepaiement  as datyecheance,
fc.nif,
fc.numstat,
fc.adresse,
fc.numcompte
     FROM factureclientlcvue_2 fc, orlc op, facturemere fm
    WHERE fc.idfacturefournisseur = op.ded_id(+) and fc.IDLIGNE=op.IDLIGNE(+) and fc.MONTANTTTC!=0 and
fm.idfacturemere = fc.numfact group by
op.ded_id, op.etat, op.idLigne, fc.idfacturefournisseur, fc.daty, fc.idfournisseur, fc.idtva,
          fc.montantttc, fc.remarque, fc.dateemission, fc.designation,
fc.iddevise, fc.numfact, fc.designationlc, fc.creditinitial,
          fc.creditmodifier, fc.montanteng, fc.montantvis, fc.montantfac,
          fc.idtypeligne, fc.resp, fc.identite, fc.iddirection, fc.mois,
fc.annee, fc.parution, fc.idligne,
          fm.datelimitepaiement , fc.nif,
fc.numstat,
fc.adresse,fc.numcompte
;
create view FACTURECLIENTORGROUPE_3 as
SELECT
fc.idfacturefournisseur,
fc.daty,
fc.idfournisseur,
fc.nom,
fc.telephone,
fc.idtva,
fc.montantttc,
fc.remarque,
fc.dateemission,
fc.designation,
fc.iddevise,
fc.numfact,
fc.designationlc,
fc.creditinitial,
fc.creditmodifier,
fc.montanteng,
fc.montantvis,
fc.montantfac,
fc.idtypeligne,
fc.resp,
fc.identite,
fc.iddirection,
fc.mois,
fc.annee,
fc.parution,
fc.idligne,
cast(sum (nvl(op.montant,0)) as number(20,2)) AS opmontant,
cast(fc.montantttc - sum (nvl(op.montant,0)) as number(20,2)) AS reste,
fm.datelimitepaiement  as datyecheance,
fc.nif,
fc.numstat,
fc.adresse
     FROM factureclientlcvue_3 fc, orlc op, facturemere fm
    WHERE fc.idfacturefournisseur = op.ded_id(+) and fc.IDLIGNE=op.IDLIGNE(+) and fc.MONTANTTTC!=0 and
fm.idfacturemere = fc.numfact group by
op.ded_id, op.etat, op.idLigne, fc.idfacturefournisseur, fc.daty, fc.idfournisseur, fc.idtva,
          fc.montantttc, fc.remarque, fc.dateemission, fc.designation,
fc.iddevise, fc.numfact, fc.designationlc, fc.creditinitial,
          fc.creditmodifier, fc.montanteng, fc.montantvis, fc.montantfac,
          fc.idtypeligne, fc.resp, fc.identite, fc.iddirection, fc.mois,
fc.annee, fc.parution, fc.idligne,
          fm.datelimitepaiement , fc.nif,
fc.numstat,
fc.adresse,fc.telephone,fc.nom;

create view FACTURECLIENTORNONVISE_2 as
SELECT fc.idfacturefournisseur, fc.daty, fc.idfournisseur, fc.idtva,
          fc.montantttc, fc.remarque, fc.dateemission, fc.designation,
          fc.iddevise, fc.numfact, fc.designationlc, fc.creditinitial,
          fc.creditmodifier, fc.montanteng, fc.montantvis, fc.montantfac,
          fc.idtypeligne, fc.numcompte, fc.identite, fc.iddirection, fc.mois,
          fc.annee, fc.parution, fc.idligne, fc.opmontant,
          fc.reste,
          fc.datyecheance,
          fc.nif,
          fc.numstat,
          fc.adresse
     FROM factureclientorgroupe_2 fc
    WHERE fc.idfacturefournisseur not in (select idfacturefournisseur from factureclientorvise);

create or replace view FACTURECLIENTORVISE_2 as
SELECT fc.idfacturefournisseur, fc.daty, fc.idfournisseur, fc.idtva,
          fc.montantttc, fc.remarque, fc.dateemission, fc.designation,
          fc.iddevise, fc.numfact, fc.designationlc, fc.creditinitial,
          fc.creditmodifier, fc.montanteng, fc.montantvis, fc.montantfac,
          fc.idtypeligne, fc.resp, fc.identite, fc.iddirection, fc.mois,
          fc.annee, fc.parution, fc.idligne, NVL (op.montant, 0) AS opmontant,
          fc.montantttc - NVL (op.montant, 0) AS reste,
          fm.datelimitepaiement  as datyecheance,
          fc.nif,
          fc.numstat,
          fc.adresse
     FROM factureclientlcvue_2 fc, ordonnerrecettegroupefact op, facturemere fm
    WHERE fc.idfacturefournisseur = op.ded_id(+) and fc.IDLIGNE=op.IDLIGNE(+) and fc.MONTANTTTC!=0 and
          fm.idfacturemere = fc.numfact and
          fc.idfacturefournisseur in (select visafactureclient.IDOBJET from visafactureclient);
create or replace view FACTUREFOURNISSEURLCCOMPTE as
select
  f.id,
  f.id1 as idfacture,
  f.id2 as idlc,
  lc.designation,
  lc.identite as entite,
  f.MONTANTMERE as montant,
  t.DESCE as compte
from
  facturefournisseurlc f,
  lignecredit lc,
  typelcdepensetous t
where
  f.id2 = lc.idligne and lc.idtypeligne = t.id
;
create or replace view FCORGROUPEIMPAYE_2 as
select IDFACTUREFOURNISSEUR,
       DATY,
       IDFOURNISSEUR,
       IDTVA,
       MONTANTTTC,
       REMARQUE,
       DATEEMISSION,
       DESIGNATION,
       IDDEVISE,
       NUMFACT,
       DESIGNATIONLC,
       CREDITINITIAL,
       CREDITMODIFIER,
       MONTANTENG,
       MONTANTVIS,
       MONTANTFAC,
       IDTYPELIGNE,
       NUMCOMPTE,
       IDENTITE,
       IDDIRECTION,
       MOIS,
       ANNEE,
       PARUTION,
       IDLIGNE,
       OPMONTANT,
       RESTE,
       DATYECHEANCE,
       NIF,
       NUMSTAT,
       ADRESSE
from factureclientorgroupe_2 f
where f.reste > 0
;

create or replace view FCORGROUPENONVISE_2 as
select IDFACTUREFOURNISSEUR,
       DATY,
       IDFOURNISSEUR,
       IDTVA,
       MONTANTTTC,
       REMARQUE,
       DATEEMISSION,
       DESIGNATION,
       IDDEVISE,
       NUMFACT,
       DESIGNATIONLC,
       CREDITINITIAL,
       CREDITMODIFIER,
       MONTANTENG,
       MONTANTVIS,
       MONTANTFAC,
       IDTYPELIGNE,
       NUMCOMPTE,
       IDENTITE,
       IDDIRECTION,
       MOIS,
       ANNEE,
       PARUTION,
       IDLIGNE,
       OPMONTANT,
       RESTE,
       NIF,
       NUMSTAT,
       ADRESSE
from factureclientorgroupe_2 f
where f.reste = 0
  and IDFACTUREFOURNISSEUR not in (select IDFACTUREFOURNISSEUR from fcorgroupevise);
create or replace view FCORGROUPEPAYE_2 as
select
IDFACTUREFOURNISSEUR,
DATY,
IDFOURNISSEUR,
IDTVA,
MONTANTTTC,
REMARQUE,
DATEEMISSION,
DESIGNATION,
IDDEVISE,
NUMFACT,
DESIGNATIONLC,
CREDITINITIAL,
CREDITMODIFIER,
MONTANTENG,
MONTANTVIS,
MONTANTFAC,
IDTYPELIGNE,
NUMCOMPTE,
IDENTITE,
IDDIRECTION,
MOIS,
ANNEE,
PARUTION,
IDLIGNE,
OPMONTANT,
RESTE,
DATYECHEANCE ,
NIF,
ADRESSE,
NUMSTAT
from factureclientorgroupe_2 f where f.reste=0;

create view FCORGROUPEVISE_2 as
select IDFACTUREFOURNISSEUR,
       DATY,
       IDFOURNISSEUR,
       IDTVA,
       MONTANTTTC,
       REMARQUE,
       DATEEMISSION,
       DESIGNATION,
       IDDEVISE,
       NUMFACT,
       DESIGNATIONLC,
       CREDITINITIAL,
       CREDITMODIFIER,
       MONTANTENG,
       MONTANTVIS,
       MONTANTFAC,
       IDTYPELIGNE,
       NUMCOMPTE,
       IDENTITE,
       IDDIRECTION,
       MOIS,
       ANNEE,
       PARUTION,
       IDLIGNE,
       OPMONTANT,
       RESTE,
       NIF,
       NUMSTAT,
       ADRESSE
from factureclientorgroupe_2 f
where f.reste = 0
  and f.NUMFACT in (select idobjet from visaor);

create view MVTCAISSELC_2TEMP as
SELECT o.ID,
       o.DATY,
       o.DESIGNATION,
       op.DESIGNATIONLC,
       cast(op.montant as number(25, 2)) as debit,
       cast(o.CREDIT as number(25, 2)),
       o.IDDEVISE,
       o.IDMODE,
       c.desccaisse                      AS idcaisse,
       o.REMARQUE,
       o.AGENCE,
       o.TIERS,
       o.NUMPIECE,
       o.TYPEMVT,
       o.DATYVALEUR,
       op.idligne                        AS IDORDRE,
       op.MOIS,
       op.ANNEE,
       op.IDDIRECTION,
       op.IDTYPELIGNE,
       op.IDENTITE
FROM mvtcaissevalide o,
     oplc op,
     caisse c
WHERE o.idordre = op.ID
  and o.IDCAISSE = c.idcaisse
UNION
SELECT o.ID,
       o.DATY,
       o.DESIGNATION,
       opr.DESIGNATIONLC,
       cast(o.DEBIT as number(25, 2)),
       cast(opr.MONTANT as number(25, 2)),
       o.IDDEVISE,
       o.IDMODE,
       c.desccaisse AS idcaisse,
       o.REMARQUE,
       o.AGENCE,
       o.TIERS,
       o.NUMPIECE,
       o.TYPEMVT,
       o.DATYVALEUR,
       opr.idligne  AS IDORDRE,
       opr.MOIS,
       opr.ANNEE,
       opr.IDDIRECTION,
       opr.IDTYPELIGNE,
       opr.IDENTITE
FROM mvtcaissevalide o,
     orlc opr,
     caisse c
WHERE o.idordre = opr.ID
  and o.IDCAISSE = c.idcaisse;

create view MVTCAISSELC_2 as
select ID,
       DATY,
       DESIGNATION,
       DESIGNATIONLC,
       cast(DEBIT as number(30, 2))  as debit,
       cast(CREDIT as number(30, 2)) as credit,
       IDDEVISE,
       IDMODE,
       IDCAISSE,
       REMARQUE,
       AGENCE,
       TIERS,
       NUMPIECE,
       TYPEMVT,
       DATYVALEUR,
       IDORDRE,
       MOIS,
       ANNEE,
       IDDIRECTION,
       IDTYPELIGNE,
       IDENTITE
from MVTCAISSELC_2TEMP;


create table OPFFLC
(
    ID          VARCHAR2(50) not null
        primary key,
    ID1         VARCHAR2(50)
        constraint OPFFLC_R01
            references ORDONNERPAYEMENT,
    ID2         VARCHAR2(50)
        references FACTUREFOURNISSEURLC,
    REMARQUE    VARCHAR2(1000),
    MONTANTMERE NUMBER(15, 2) default 0,
    ETAT        NUMBER        default 0
)
;

create index OPFFLCID2
    on OPFFLC (ID2)
;

create index OPFFLCID1
    on OPFFLC (ID1)
;

create index ETATOPFFLC
    on OPFFLC (ETAT)
;
CREATE SEQUENCE SEQOPFFLC
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQOPFFLC
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQLCDETAILS.nextval INTO retour FROM dual;
  return retour;
END;
    alter table mvtCaisse add agence varchar2(50);
alter table mvtCaisse add IDMVTOR varchar2(50);
alter table mvtCaisse add ETABLISSEMENT varchar2(50);
    create or replace view MVTCAISSEVALIDE as
SELECT o."ID",o."DATY",o."DESIGNATION",o."DEBIT",o."CREDIT",o."IDDEVISE",o."IDMODE",o."IDCAISSE",o."REMARQUE",
o."AGENCE",o."TIERS",o."NUMPIECE",o."TYPEMVT",o."DATYVALEUR",o."IDORDRE",o.IDMVTOR,
o.NUMCHEQUE,o.ETABLISSEMENT
     FROM mvtCaisse o
    WHERE (o.DEBIT+o.CREDIT !=0) and NOT EXISTS (SELECT v.*
                        FROM autorisationannulationvalide v
                       WHERE v.idobjet = o.ID );

;
create or replace view MVTCAISSELC_ID as
SELECT o."ID", o."DATY", o."DESIGNATION", op.montant as debit, o."CREDIT",
          o."IDDEVISE", o."IDMODE",o.idcaisse, o."REMARQUE", o."AGENCE",
          o."TIERS", o."NUMPIECE", o."TYPEMVT", o."DATYVALEUR",
          op.idligne AS "IDORDRE", op.MOIS,op.ANNEE,op.IDDIRECTION,op.IDTYPELIGNE,op.IDENTITE
     FROM mvtcaissevalide o, oplc op
    WHERE o.idordre = op.ID
   UNION
   SELECT o."ID", o."DATY", o."DESIGNATION", o."DEBIT",opr.MONTANT,
          o."IDDEVISE", o."IDMODE",o.idcaisse, o."REMARQUE", o."AGENCE",
          o."TIERS", o."NUMPIECE", o."TYPEMVT", o."DATYVALEUR",
          opr.idligne AS "IDORDRE", opr.MOIS,opr.ANNEE,opr.IDDIRECTION,opr.IDTYPELIGNE,opr.IDENTITE
     FROM mvtcaissevalide o, orlc opr
    WHERE o.idordre = opr.ID;

create table MVTCAISSEPREVISIONLC
(
    ID            VARCHAR2(50) not null
        primary key,
    DATY          DATE         not null,
    DESIGNATION   VARCHAR2(1500),
    DEBIT         NUMBER(15, 4),
    CREDIT        NUMBER(15, 4),
    IDDEVISE      VARCHAR2(50),
    IDMODE        VARCHAR2(50),
    IDCAISSE      VARCHAR2(50),
    REMARQUE      VARCHAR2(100),
    AGENCE        VARCHAR2(50),
    TIERS         VARCHAR2(50),
    NUMPIECE      VARCHAR2(50),
    TYPEMVT       VARCHAR2(50),
    DATYVALEUR    DATE,
    IDORDRE       VARCHAR2(50),
    NUMCHEQUE     VARCHAR2(50),
    ETABLISSEMENT VARCHAR2(50),
    IDMVTOR       VARCHAR2(250),
    IDLIGNE       VARCHAR2(50) not null
);
CREATE SEQUENCE SEQMVTCAISSEPREVISIONLC
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQMVTCAISSEPREVISIONLC
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQMVTCAISSEPREVISIONLC.nextval INTO retour FROM dual;
  return retour;
END;
create view MVTCAISSEPREVISIONLIGNEC as
(select prev.ID, prev.DATY, prev.DESIGNATION, prev.DEBIT , prev.CREDIT , prev.IDDEVISE , mp.VAL as IDMODE , cs.DESCCAISSE as IDCAISSE, prev.REMARQUE , prev.AGENCE ,
prev.TIERS , prev.NUMPIECE , prev.TYPEMVT, prev.DATYVALEUR ,
    prev.IDORDRE , prev.NUMCHEQUE, prev.ETABLISSEMENT, prev.IDMVTOR, prev.IDLIGNE, lc.designation as designationlc, tp.VAL as typelc, bn.VAL as entite
    from MVTCAISSEPREVISIONLC prev, lignecredit lc, TYPELCDEPENSETOUS tp, beneficiaire bn, caisse cs, MODEPAIEMENT mp
where prev.IDLIGNE = lc.idligne and lc.IDTYPELIGNE = tp.ID and lc.IDENTITE = bn.id and prev.IDCAISSE = cs.IDcaisse and prev.IDMODE = mp.ID
union
select prev.ID, prev.DATY, prev.DESIGNATION, prev.DEBIT , prev.CREDIT , prev.IDDEVISE , mp.VAL as IDMODE , cs.DESCCAISSE as IDCAISSE, prev.REMARQUE , prev.AGENCE ,
prev.TIERS , prev.NUMPIECE , prev.TYPEMVT, prev.DATYVALEUR ,
    prev.IDORDRE , prev.NUMCHEQUE, prev.ETABLISSEMENT, prev.IDMVTOR, prev.IDLIGNE, lc.designation as designationlc, tp.VAL as typelc, bn.VAL as entite
from MVTCAISSEPREVISIONLC prev, lignecreditrecette lc, TYPELCRECETTETOUS tp, beneficiaire bn, caisse cs, MODEPAIEMENT mp
where prev.IDLIGNE = lc.idligne and lc.IDTYPELIGNE = tp.ID and lc.IDENTITE = bn.id and prev.IDCAISSE = cs.IDcaisse and prev.IDMODE = mp.ID)
;

create view MVTCAISSESOMMEDEBIT as
select idordre as ID, sum(debit) as SOMMEMONTANT from mvtcaisse group by idordre;

create view MVTCAISSETABLEAU1 as
select mvtclc.ID,
         mvtclc.DATY                    as daty,
         sum(mvtclc.DEBIT)                   as DEBITPREVU,
         sum(mvtclc.CREDIT)                  as CREDITPREVU,
         sum((mvtclc.DEBIT - mvtclc.CREDIT)) as SOLDEPREVU,
         mvtclc.IDDEVISE,
         mvtclc.IDMODE,
         mvtclc.IDCAISSE,
         mvtclc.AGENCE,
         mvtclc.TIERS,
         mvtclc.TYPEMVT,
         sum(mvtc.DEBIT) as debit,
         sum(mvtc.CREDIT) as credit,
         sum((mvtc.DEBIT - mvtc.CREDIT))     as SOLDE,
         mvtc.IDENTITE,
         mvtc.IDDIRECTION,
         mvtc.MOIS,
         mvtc.ANNEE,
         mvtc.IDTYPELIGNE as typelc
  from MVTCAISSEPREVISIONLC mvtclc
         join MVTCAISSELC mvtc on mvtclc.DATY = mvtc.DATY
  group by mvtclc.ID,
           mvtclc.DATY,
           mvtclc.DEBIT,
           mvtclc.CREDIT,
           mvtclc.IDDEVISE,
           mvtclc.IDMODE,
           mvtclc.IDCAISSE,
           mvtclc.AGENCE,
           mvtclc.TIERS,
           mvtclc.TYPEMVT,
           mvtc.DEBIT,
           mvtc.CREDIT,
           mvtc.IDENTITE,
           mvtc.IDDIRECTION,
           mvtc.MOIS,
           mvtc.ANNEE,
           mvtc.IDTYPELIGNE;
create view MVTCAISSETABLEAU2 as
select
mvtclc.ID,
       mvtclc.DATY,
       mvtclc.DESIGNATION,
       mvtclc.DEBIT,
       mvtclc.CREDIT,
       mvtclc.IDDEVISE,
       mvtclc.IDMODE,
       mvtclc.IDCAISSE,
       mvtclc.REMARQUE,
       mvtclc.AGENCE,
       mvtclc.TIERS,
       mvtclc.NUMPIECE,
       mvtclc.TYPEMVT

from MVTCAISSEPREVISIONLC mvtclc
union
select mvtc.ID,
       mvtc.DATY,
       mvtc.DESIGNATION,
       mvtc.DEBIT,
       mvtc.CREDIT,
       mvtc.IDDEVISE,
       mvtc.IDMODE,
       mvtc.IDCAISSE,
       mvtc.REMARQUE,
       mvtc.AGENCE,
       mvtc.TIERS,
       mvtc.NUMPIECE,
       mvtc.TYPEMVT


from MVTCAISSELC mvtc;

create view MVTCAISSETABLEAU2_CAISSE_PREV as
select mvtc.ID,
         mvtc.DATY,
         mvtc.DESIGNATION,
         mvtc.DEBIT,
         mvtc.CREDIT,
         mvtc.IDDEVISE,
         mvtc.IDMODE,
         mvtc.IDCAISSE,
         mvtc.REMARQUE,
         mvtc.AGENCE,
         mvtc.TIERS,
         mvtc.NUMPIECE,
         mvtc.TYPEMVT,

         mvtc.DATYVALEUR,
         mvtc.IDORDRE,
         to_number(l.mois) as mois,
          l.annee,
         d.libelledir as IDDIRECTION,
         t.val as IDTYPELIGNE,
         b.val as IDENTITE
  from MVTCAISSE mvtc, ordonnerpayement d, lignecredit l, typesortieliste t, beneficiaire b, direction d, opfflc o, facturefournisseurlc flc, RAPPRO_SOUS_MVTCAISSE rp, caisse c
  where (mvtc.DEBIT+mvtc.CREDIT !=0) and mvtc.IDORDRE = d.id and mvtc.daty <= (select sysdate from dual) and flc.id2 = l.idligne and o.ID2=flc.ID
  and l.idtypeligne = t.ID and o.ID1=d.ID and l.identite = b.ID AND l.iddirection = d.IDDIR and mvtc.id = rp.IDMVTCAISSE and mvtc.IDCAISSE = c.idcaisse and c.respcaisse = 1
  union
    select mvtc.ID,
         mvtc.DATY,
         mvtc.DESIGNATION,
         mvtc.DEBIT,
         mvtc.CREDIT,
         mvtc.IDDEVISE,
         mvtc.IDMODE,
         mvtc.IDCAISSE,
         mvtc.REMARQUE,
         mvtc.AGENCE,
         mvtc.TIERS,
         mvtc.NUMPIECE,
         mvtc.TYPEMVT,

         mvtc.DATYVALEUR,
         mvtc.IDORDRE,
         to_number(l.mois) as mois,
          l.annee,
         d.libelledir as IDDIRECTION,
         t.val as IDTYPELIGNE,
         b.val as IDENTITE
  from MVTCAISSE mvtc, ordonnerrecette d, lignecreditrecette l, typeentreeliste t, beneficiaire b, direction d, orfclc o, factureclientlc flc, RAPPRO_SOUS_MVTCAISSE rp, caisse c
  where (mvtc.DEBIT+mvtc.CREDIT !=0) and mvtc.IDORDRE = d.id and mvtc.daty <= (select sysdate from dual) and flc.id2 = l.idligne and o.ID2=flc.ID
  and l.idtypeligne = t.ID and o.ID1=d.ID and l.identite = b.ID AND l.iddirection = d.IDDIR and mvtc.id = rp.IDMVTCAISSE and mvtc.IDCAISSE = c.idcaisse and c.respcaisse = 1
  union
  select mvtc.ID,
         mvtc.DATY,
         mvtc.DESIGNATION,
         mvtc.DEBIT,
         mvtc.CREDIT,
         mvtc.IDDEVISE,
         mvtc.IDMODE,
         mvtc.IDCAISSE,
         mvtc.REMARQUE,
         mvtc.AGENCE,
         mvtc.TIERS,
         mvtc.NUMPIECE,
         mvtc.TYPEMVT,

         mvtc.DATYVALEUR,
         mvtc.IDORDRE,
         to_number(l.mois) as mois,
          l.annee,
         d.libelledir as IDDIRECTION,
         t.val as IDTYPELIGNE,
         b.val as IDENTITE
  from MVTCAISSE mvtc, ordonnerpayement d, lignecredit l, typesortieliste t, beneficiaire b, direction d, opfflc o, facturefournisseurlc flc, caisse c
  where (mvtc.DEBIT+mvtc.CREDIT !=0) and mvtc.IDORDRE = d.id and mvtc.daty <= (select sysdate from dual) and flc.id2 = l.idligne and o.ID2=flc.ID
  and l.idtypeligne = t.ID and o.ID1=d.ID and l.identite = b.ID AND l.iddirection = d.IDDIR and mvtc.IDCAISSE = c.idcaisse and c.respcaisse != 1
  union
    select mvtc.ID,
         mvtc.DATY,
         mvtc.DESIGNATION,
         mvtc.DEBIT,
         mvtc.CREDIT,
         mvtc.IDDEVISE,
         mvtc.IDMODE,
         mvtc.IDCAISSE,
         mvtc.REMARQUE,
         mvtc.AGENCE,
         mvtc.TIERS,
         mvtc.NUMPIECE,
         mvtc.TYPEMVT,

         mvtc.DATYVALEUR,
         mvtc.IDORDRE,
         to_number(l.mois) as mois,
          l.annee,
         d.libelledir as IDDIRECTION,
         t.val as IDTYPELIGNE,
         b.val as IDENTITE
  from MVTCAISSE mvtc, ordonnerrecette d, lignecreditrecette l, typeentreeliste t, beneficiaire b, direction d, orfclc o, factureclientlc flc, caisse c
  where (mvtc.DEBIT+mvtc.CREDIT !=0) and mvtc.IDORDRE = d.id and mvtc.daty <= (select sysdate from dual) and flc.id2 = l.idligne and o.ID2=flc.ID
  and l.idtypeligne = t.ID and o.ID1=d.ID and l.identite = b.ID AND l.iddirection = d.IDDIR and mvtc.IDCAISSE = c.idcaisse and c.respcaisse != 1
  union
  select mvtclc.ID,
         mvtclc.DATY,
         mvtclc.DESIGNATION,
         mvtclc.DEBIT,
         mvtclc.CREDIT,
         mvtclc.IDDEVISE,
         mvtclc.IDMODE,
         mvtclc.IDCAISSE,
         mvtclc.REMARQUE,
         mvtclc.AGENCE,
         mvtclc.TIERS,
         mvtclc.NUMPIECE,
         mvtclc.TYPEMVT,

         mvtclc.DATYVALEUR,
         mvtclc.IDORDRE,
         0 as mois,
         0 as annee,
         '' as IDDIRECTION,
         '' as IDTYPELIGNE,
         '' as IDENTITE

  from MVTCAISSEPREVISIONLC mvtclc
  where mvtclc.daty > (select sysdate from dual);

create table MVTOR
(
    ID    VARCHAR2(50) not null,
    IDMVT VARCHAR2(50),
    IDOR  VARCHAR2(1024)
);
CREATE SEQUENCE SEQMVTOR
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQMVTOR
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQMVTOR.nextval INTO retour FROM dual;
  return retour;
END;

create view MVTCAISSETVAHT (id, daty, designation, tva, ht, debit, credit, iddevise, idmode, idcaisse, remarque, agence, tiers, numpiece, typemvt, datyvaleur, idordre, numcheque, etablissement) as
(SELECT mv.IDmvt, m.daty, m.designation, sum(frs.idtva) as TVA, cast((sum(m.debit) -  sum(frs.idtva)) as number (20,2)) as HT, cast(sum(m.debit) as number(20,4)), CAST(sum(m.credit) as number(20,4)), m.iddevise,
          m.idmode, m.idcaisse, m.remarque,
          m.agence, m.tiers, m.numpiece, m.typemvt,
          m.datyvaleur, mv.idor,m.numcheque,m.etablissement
     FROM mvtcaisselettrevalide m, mvtor mv,  ordonnerpayement op, facturefournisseur frs
     where
        m.idmvtor = mv.ID
        and m.idordre = op.id
        and op.ded_id = frs.id
     group by
     mv.IDmvt, m.daty, m.designation,m.iddevise,
     m.idmode, m.idcaisse, m.remarque,
     m.agence, m.tiers, m.numpiece, m.typemvt,
     m.datyvaleur, mv.idor,m.numcheque,m.etablissement)
     union
(SELECT mv.IDmvt, m.daty, m.designation, sum(frs.idtva) as TVA, cast((sum(m.credit) -  sum(frs.idtva)) as number (20,2)) as HT, cast(sum(m.debit) as number(20,4)), CAST(sum(m.credit) as number(20,4)), m.iddevise,
          m.idmode, m.idcaisse, m.remarque,
          m.agence, m.tiers, m.numpiece, m.typemvt,
          m.datyvaleur, mv.idor,m.numcheque,m.etablissement
     FROM mvtcaisselettrevalide m, mvtor mv,  ordonnerrecette op, factureclient frs
     where
        m.idmvtor = mv.ID
        and m.idordre = op.id
        and op.ded_id = frs.idfacturefournisseur
     group by
     mv.IDmvt, m.daty, m.designation,m.iddevise,
     m.idmode, m.idcaisse, m.remarque,
     m.agence, m.tiers, m.numpiece, m.typemvt,
     m.datyvaleur, mv.idor,m.numcheque,m.etablissement);

create table RETOURMONNAIEMVTCAISSE
(
    ID          VARCHAR2(50) not null
        primary key,
    ID1         VARCHAR2(50),
    ID2         VARCHAR2(50),
    REMARQUE    VARCHAR2(100),
    MONTANTMERE NUMBER(25, 2) default 0,
    ETAT        NUMBER        default 0
);
CREATE SEQUENCE SEQRETOURMONNAIEMVTCAISSE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQRETOURMONNAIEMVTCAISSE
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQRETOURMONNAIEMVTCAISSE.nextval INTO retour FROM dual;
  return retour;
END;

create view MVTNONRAPPROCHE
            (ID, DATY, DESIGNATION, DEBIT, CREDIT, IDDEVISE, IDMODE, IDCAISSE, REMARQUE, AGENCE, TIERS, NUMPIECE,
             TYPEMVT, DATYVALEUR, IDORDRE, NUMCHEQUE, ETABLISSEMENT)
as
select id, daty, designation, cast(sum(debit) as number(20,2)), CAST(sum(credit) as number(20,2)),
  iddevise, val, idcaisse, remarque, agence, tiers, numpiece, type, datyvaleur, idor, numcheque, etablissement
  from
  (select distinct
  m.ID, m.daty, m.designation, cast(m.debit as number(20,2)) as debit, m.credit as credit, m.iddevise,
mp.val, c.idcaisse, CAST(m.remarque as varchar2(3000)) as remarque,
CAST(a.val as varchar2(500)) as agence, m.tiers, m.numpiece, t.val as type,
m.datyvaleur, mv.idor,CAST(m.numcheque as varchar2(500)) as numcheque,CAST(m.etablissement as varchar2(500)) as etablissement
from
  mvtcaissevalide m, typemvt t, agence a, caisse c,
  modepaiement mp, mvtor mv, rappro_sous_mvtcaisse r,
  retourmonnaiemvtcaisse retour
where
  m.typemvt = t.ID AND
  m.agence = a.ID  AND
  m.idcaisse = c.idcaisse AND
  m.idmode = mp.ID AND
  m.idmvtor = mv.ID
  and m.ID = r.idmvtcaisse(+) and r.idmvtcaisse is null
  and m.ID = retour.ID2(+) and retour.ID2 is null) nonrappro
  group by id, daty, designation, iddevise, val, idcaisse, remarque, agence, tiers, numpiece, type, datyvaleur, idor, numcheque, etablissement;

create view MVTNONRAPPROCHE_LIBELLE
            (ID, DATY, DESIGNATION, DEBIT, CREDIT, IDDEVISE, IDMODE, IDCAISSE, REMARQUE, AGENCE, TIERS, NUMPIECE,
             TYPEMVT, DATYVALEUR, IDORDRE, NUMCHEQUE, ETABLISSEMENT)
as
select
  m.ID, m.daty, m.designation, cast(sum(m.debit) as number(20,2)), CAST(sum(m.credit) as number(20,2)), m.iddevise,
mp.val, c.desccaisse as idcaisse, CAST(m.remarque as varchar2(3000)),
CAST(a.val as varchar2(500)), m.tiers, m.numpiece, t.val,
m.datyvaleur, mv.idor,CAST(m.numcheque as varchar2(500)),CAST(m.etablissement as varchar2(500))
from
  mvtcaissevalide m, typemvt t, agence a, caisse c,
  modepaiement mp, mvtor mv, rappro_sous_mvtcaisse r,
  retourmonnaiemvtcaisse retour
where
  m.typemvt = t.ID AND
  m.agence = a.ID  AND
  m.idcaisse = c.idcaisse AND
  m.idmode = mp.ID AND
  m.idmvtor = mv.ID
  and m.ID = r.idmvtcaisse(+) and r.idmvtcaisse is null
  and m.ID = retour.ID2(+) and retour.ID2 is null
  group by
  m.ID, m.daty, m.designation, m.iddevise,
  mp.val, c.desccaisse, m.remarque,
  a.val, m.tiers, m.numpiece, t.val,
  m.datyvaleur, mv.idor,m.numcheque,m.etablissement;
create view MVTNONRAPPROCHE_SANSRETOUR
            (ID, DATY, DESIGNATION, DEBIT, CREDIT, IDDEVISE, IDMODE, IDCAISSE, REMARQUE, AGENCE, TIERS, NUMPIECE,
             TYPEMVT, DATYVALEUR, IDORDRE, NUMCHEQUE, ETABLISSEMENT)
as
select id, daty, designation, cast(sum(debit) as number(20,2)), CAST(sum(credit) as number(20,2)),
  iddevise, val, idcaisse, remarque, agence, tiers, numpiece, type, datyvaleur, idor, numcheque, etablissement
  from
  (select distinct
  m.ID, m.daty, m.designation, cast(m.debit as number(20,2)) as debit, m.credit as credit, m.iddevise,
mp.val, c.idcaisse, CAST(m.remarque as varchar2(3000)) as remarque,
CAST(a.val as varchar2(500)) as agence, m.tiers, m.numpiece, t.val as type,
m.datyvaleur, m.IDMVTOR as idor ,CAST(m.numcheque as varchar2(500)) as numcheque,CAST(m.etablissement as varchar2(500)) as etablissement
from
  mvtcaissevalide m, typemvt t, agence a, caisse c,
  modepaiement mp, rappro_sous_mvtcaisse r
where
  m.typemvt = t.ID AND
  m.agence = a.ID  AND
  m.idcaisse = c.idcaisse AND
  m.idmode = mp.ID
  and m.ID = r.idmvtcaisse(+) and r.idmvtcaisse is null
  ) nonrappro
  group by id, daty, designation, iddevise, val, idcaisse, remarque, agence, tiers, numpiece, type, datyvaleur, idor, numcheque, etablissement;
create table MVTCAISSEPREVISION
(
    ID            VARCHAR2(50) not null
        primary key,
    DATY          DATE         not null,
    DESIGNATION   VARCHAR2(1500),
    DEBIT         NUMBER(15, 4),
    CREDIT        NUMBER(15, 4),
    IDDEVISE      VARCHAR2(50),
    IDMODE        VARCHAR2(50),
    IDCAISSE      VARCHAR2(50),
    REMARQUE      VARCHAR2(100),
    AGENCE        VARCHAR2(50),
    TIERS         VARCHAR2(50),
    NUMPIECE      VARCHAR2(50),
    TYPEMVT       VARCHAR2(50),
    DATYVALEUR    DATE,
    IDORDRE       VARCHAR2(50),
    NUMCHEQUE     VARCHAR2(50),
    ETABLISSEMENT VARCHAR2(50),
    IDMVTOR       VARCHAR2(250),
    IDLIGNE       VARCHAR2(50) not null
);
CREATE SEQUENCE SEQMVTCAISSEPREVISION
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQMVTCAISSEPREVISION
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQMVTCAISSEPREVISION.nextval INTO retour FROM dual;
  return retour;
END;
create view MVTPREVISIONLC as
select mvp.id as id,
       mvp.DATY as daty,
       mvp.DESIGNATION as DESIGNATION,
       mvp.DEBIT as DEBIT,
       mvp.CREDIT CREDIT,
       d.DESCE as IDDEVISE,
       mp.DESCE as IDMODE,
       mvp.IDCAISSE as IDCAISSE,
       mvp.REMARQUE as REMARQUE,
       mvp.AGENCE as AGENCE,
       mvp.TIERS,
       mvp.NUMPIECE,
       mvp.TYPEMVT,
       mvp.DATYVALEUR,
       mvp.IDORDRE,
       extract(month from mvp.DATY) as mois,
       extract(year from mvp.DATY) as annee,
       lc.IDDIRECTION,
       lc.IDTYPELIGNE,
       lc.IDENTITE

from MVTCAISSEPREVISION mvp join LIGNECREDIT lc on mvp.IDLIGNE = lc.IDLIGNE
join devise d on d.id = mvp.IDDEVISE
join MODEPAIEMENT mp on mp.id = mvp.IDMODE
where mvp.DATY > (select sysdate dt from dual);


create table NATURELC
(
    ID    VARCHAR2(50) not null
        constraint PK_NATURELC
            primary key,
    VAL   VARCHAR2(250),
    DESCE VARCHAR2(250)
)
;
CREATE SEQUENCE SEQNATURELC
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 10
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQNATURELC
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQNATURELC.nextval INTO retour FROM dual;
  return retour;
END;
INSERT INTO NATURELC (ID, VAL, DESCE) VALUES ('NTLC000001', 'Fonctionnement depense', null);
INSERT INTO NATURELC (ID, VAL, DESCE) VALUES ('NTLC000002', 'Investissement depense', null);
INSERT INTO NATURELC (ID, VAL, DESCE) VALUES ('NTLC000003', 'Recette normale', null);
INSERT INTO NATURELC (ID, VAL, DESCE) VALUES ('NTLC000004', 'Autre recette', null);
create table COMPTECOMPTABILITE
(
    ID    VARCHAR2(50)  not null
        constraint PK_COMPTECOMPTABILITE
            primary key,
    VAL   VARCHAR2(250) not null,
    DESCE VARCHAR2(250) not null
);
CREATE SEQUENCE SEQCOMPTECOMPTABILITE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 10
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQCOMPTECOMPTABILITE
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQCOMPTECOMPTABILITE.nextval INTO retour FROM dual;
  return retour;
END;
alter table typelcdepensetous add nature varchar2(50);
create view COMPTECOMPTABILITE_LIBELLE as
select "ID","VAL","DESCE" from comptecomptabilite where length(val)=6;


create table VISAFACTUREF
(
    ID       VARCHAR2(50) not null
        constraint VISAFACTUREF_PK
            primary key,
    DATY     DATE,
    REMARQUE VARCHAR2(100),
    IDOBJET  VARCHAR2(50)
)
;
create index IDXOBJFACTF
    on VISAFACTUREF (IDOBJET);

CREATE SEQUENCE SEQVISAFACTUREF
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 10
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION GETSEQVISAFACTUREF
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SEQVISAFACTUREF.nextval INTO retour FROM dual;
  return retour;
END;

create or replace view SUMFACTFLC as
SELECT   fflc.id2 AS idligne, SUM (fflc.montantmere) AS facture
       FROM facturefournisseurlc fflc
      WHERE EXISTS (SELECT v.*
                    FROM visafacturef v
                   WHERE v.idobjet = fflc.ID1 )
   GROUP BY fflc.id2;




create or replace view TYPELCDEPENSENATURE as
select typelcdepensetous.id, typelcdepensetous.val as typelc,
typelcdepensetous.desce as numcompte, compte.desce as comptes, naturelc.val as nature from typelcdepensetous
join naturelc on naturelc.id = typelcdepensetous.nature
join comptecomptabilite_libelle compte on compte.val = typelcdepensetous.desce;
create or replace view LIGNECREDITDEPENSENATURE
            (IDLIGNE, DESIGNATION, CREDITINITIAL, CREDITMODIFIER, MONTANTENG, MONTANTVIS, MONTANTFAC, MOIS, ANNEE,
             TYPELC, NUMCOMPTE, NATURE, DESCCOMPTE, LIBELLEDIR, ABBREVDIR, IDDIRECTEUR, ENTITE, IDDIRECTION, PARUTION,
             RESTE, IMPAYE)
as
SELECT l.idligne, l.designation, l.creditinitial, l.creditmodifier,
          NVL (mvlc.debit, 0) AS montanteng, NVL (so.op, 0) AS montantvis,
          NVL (sf.facture, 0) AS montantfac, TO_NUMBER (l.mois), l.annee,
          typesortie.typelc AS typelc, typesortie.numcompte , typesortie.nature  ,
          typesortie.comptes,  direction.libelledir,
          direction.abbrevdir, direction.iddirecteur,
          beneficiaire.val AS entite, direction.iddir AS iddirection,
          TO_NUMBER (l.parution),l.creditmodifier-NVL (sf.facture, 0), NVL (sf.facture, 0)- NVL (mvlc.debit, 0) as impaye
     FROM lignecredit l,
          direction,
          beneficiaire,
		  compteg,
          typelcdepensenature typesortie,
          mvtdeplc mvlc,
          sumoplc so,
          sumfactflc sf
    WHERE direction.iddir = l.iddirection
      AND beneficiaire.ID = l.identite
	  AND compteg.ID = l.numcompte
      AND typesortie.ID = l.idtypeligne
      AND mvlc.idligne(+) = l.idligne
      AND so.idligne(+) = l.idligne
      AND sf.idligne(+) = l.idligne;

create view LIGNECREDITPREVISIONLC as
SELECT l.idligne,
       l.designation,
       l.creditinitial,
       l.creditmodifier,
       NVL(mvlc.debit, 0) AS montanteng,
       NVL(so.op, 0)      AS montantvis,
       NVL(sf.facture, 0) AS montantfac,
       TO_NUMBER(l.mois) as mois,
       l.annee,
       typesortie.val     AS typelc,
       l.numcompte,
       compteg.val        AS desccompte,
       direction.libelledir,
       direction.abbrevdir,
       direction.iddirecteur,
       beneficiaire.val   AS entite,
       direction.iddir    AS iddirection,
       TO_NUMBER(l.parution) as parution,
       l.creditmodifier - NVL(sf.facture, 0) as reste,
       sum(mcp.DEBIT) as prevision

FROM lignecredit l,
     direction,
     compteg,
     beneficiaire,
     typesortieliste typesortie,
     mvtdeplc mvlc,
     sumoplc so,
     sumfactflc sf,
     MVTCAISSEPREVISIONLC mcp
WHERE direction.iddir = l.iddirection
  AND compteg.ID = l.numcompte
  AND beneficiaire.ID = l.identite
  AND typesortie.ID = l.idtypeligne
  AND mvlc.idligne (+) = l.idligne
  AND so.idligne (+) = l.idligne
  AND sf.idligne (+) = l.idligne
  and mcp.IDLIGNE = l.IDLIGNE
group by l.idligne,
         l.designation,
         l.creditinitial,
         l.creditmodifier,
         l.annee,
         typesortie.val,
         direction.libelledir,
         direction.abbrevdir,
         direction.iddirecteur,
         beneficiaire.val,
         direction.iddir,
         l.parution,
         mvlc.debit,
         so.op,
         sf.facture,
         l.mois,
         l.numcompte,
         compteg.val;

create view LIGNECREDITPREVISIONLC_2 as
SELECT l.idligne,
       l.designation,
       cast ( l.creditinitial as number(20 , 2) ) creditinitial,
       cast ( l.creditmodifier as number(20 , 2) ) creditmodifier,
       cast ( NVL(mvlc.debit, 0) as number(20 , 2) ) AS montanteng,
       cast ( NVL(so.op, 0)  as number(20 , 2) )     AS montantvis,
       cast ( NVL(sf.facture, 0) as number(20 , 2) )  AS montantfac,
       TO_NUMBER(l.mois) as mois,
       l.annee,
       typesortie.val     AS typelc,
       l.numcompte,
       compteg.val        AS desccompte,
       direction.libelledir,
       direction.abbrevdir,
       direction.iddirecteur,
       beneficiaire.val   AS entite,
       direction.iddir    AS iddirection,
       TO_NUMBER(l.parution) as parution,
       cast ( l.creditmodifier - NVL(sf.facture, 0) as number(20 , 2) )  as reste,
       cast ( sum(mcp.DEBIT) as number(20 , 2) )  as prevision

FROM lignecredit l,
     direction,
     compteg,
     beneficiaire,
     typesortieliste typesortie,
     mvtdeplc mvlc,
     sumoplc so,
     sumfactflc sf,
     MVTCAISSEPREVISIONLC mcp
WHERE direction.iddir = l.iddirection
  AND compteg.ID = l.numcompte
  AND beneficiaire.ID = l.identite
  AND typesortie.ID = l.idtypeligne
  AND mvlc.idligne (+) = l.idligne
  AND so.idligne (+) = l.idligne
  AND sf.idligne (+) = l.idligne
  and mcp.IDLIGNE = l.IDLIGNE
group by l.idligne,
         l.designation,
         l.creditinitial,
         l.creditmodifier,
         l.annee,
         typesortie.val,
         direction.libelledir,
         direction.abbrevdir,
         direction.iddirecteur,
         beneficiaire.val,
         direction.iddir,
         l.parution,
         mvlc.debit,
         so.op,
         sf.facture,
         l.mois,
         l.numcompte,
         compteg.val;

alter table typelcrecettetous add nature varchar2(50);
create view TYPELCRECETTENATURE as
select typelcrecettetous.id, typelcrecettetous.val as typelc,
typelcrecettetous.desce as numcompte, compte.desce as comptes, naturelc.val as nature from typelcrecettetous
join naturelc on naturelc.id = typelcrecettetous.nature
join comptecomptabilite_libelle compte on compte.val = typelcrecettetous.desce;

create view LIGNECREDITRECETTENATURE
            (IDLIGNE, DESIGNATION, CREDITINITIAL, CREDITMODIFIER, MONTANTENG, MONTANTVIS, MONTANTFAC, MOIS, ANNEE,
             TYPELC, NUMCOMPTE, NATURE, DESCCOMPTE, LIBELLEDIR, ABBREVDIR, IDDIRECTEUR, ENTITE, IDDIRECTION, PARUTION,
             RESTE, IMPAYE)
as
SELECT l.idligne, l.designation, l.creditinitial, l.creditmodifier,
          NVL (mvlc.debit, 0) AS montanteng, NVL (so.op, 0) AS montantvis,
          NVL (sf.facture, 0) AS montantfac, TO_NUMBER (l.mois), l.annee,
          typesortie.typelc AS typelc, typesortie.numcompte , typesortie.nature  ,
          typesortie.comptes,  direction.libelledir,
          direction.abbrevdir, direction.iddirecteur,
          beneficiaire.val AS entite, direction.iddir AS iddirection,
          TO_NUMBER (l.parution),l.creditmodifier-NVL (sf.facture, 0), NVL (sf.facture, 0)- NVL (mvlc.debit, 0) as impaye
     FROM lignecreditrecette l,
          direction,
          beneficiaire,
          typelcrecettenature typesortie,
          mvtreclc mvlc,
          sumorlc so,
          sumfactclc sf
    WHERE direction.iddir = l.iddirection
      AND beneficiaire.ID = l.identite
      AND typesortie.ID = l.idtypeligne
      AND mvlc.idligne(+) = l.idligne
      AND so.idligne(+) = l.idligne
      AND sf.idligne(+) = l.idligne;

      create or replace view FACTUREFOURNISSEURVISE as
select f.id,f.DATY,f.IDFOURNISSEUR,f.IDTVA,f.MONTANTTTC,f.REMARQUE,
  f.DATEEMISSION,f.DESIGNATION,f.IDDEVISE,f.NUMFACT,f.RESP,f.DATYECHEANCE from FACTUREFOURNISSEUR f
  where f.etat>1 and f.montantttc>0;

  create or replace view FACTUREFOURNISSEURTVAHT
            (id, DATY, IDFOURNISSEUR, IDTVA, MONTANTHT, MONTANTTTC, REMARQUE, DATEEMISSION,
             DESIGNATION, IDDEVISE, NUMFACT, RESP, DATYECHEANCE)
as
select
    fc.id, fc.DATY, cl.NOM , fc.IDTVA, fc.MONTANTTTC - fc.IDTVA,
    fc.MONTANTTTC, fc.REMARQUE, fc.DATEEMISSION, fc.DESIGNATION, fc.IDDEVISE, fc.NUMFACT,
    rsp.VAL, fc.DATYECHEANCE
from
    FACTUREFOURNISSEUR fc,
    CLIENT cl,
    AGENCE rsp
where
    fc.IDFOURNISSEUR = cl.IDCLIENT
    and fc.RESP = rsp.ID(+)
;


alter table FACTUREFOURNISSEUR add idfacturefournisseur varchar2(50);

create or replace view FACTUREFOURNISSEURVISE as
select f.id,f."DATY",f."IDFOURNISSEUR",f."IDTVA",f."MONTANTTTC",f."REMARQUE",
  f."DATEEMISSION",f."DESIGNATION",f."IDDEVISE",f."NUMFACT",f."RESP",f."DATYECHEANCE" , idfacturefournisseur from FACTUREFOURNISSEUR f
  where f.etat>1 and f.montantttc>0
;
create or replace view FACTUREFLCVUE as
SELECT d.idfacturefournisseur, d.daty, cl.nom, d.idtva, flc.MONTANTMERE ,
          d.remarque, d.dateemission, d.designation, d.iddevise, d.numfact,
l.designation AS designationlc, l.creditinitial, l.creditmodifier,
l.montanteng, l.montantvis, l.montantfac, l.idtypeligne,
          l.numcompte, l.identite, l.iddirection, l.mois, l.annee, l.parution,
          l.idligne,
          d.datyecheance,
       montantttc,
       idfournisseur
     FROM facturefournisseurvise d,
          facturefournisseurlc flc,
          lclettre l,
          client cl
    WHERE d.idfacturefournisseur = flc.id1(+)
      AND flc.id2 = l.idligne
      AND d.idfournisseur = cl.idclient and flc.ETAT=1;


create or replace view FACTUREFOURNISSEUROPGROUPE as
SELECT fc.idfacturefournisseur, fc.daty, fc.idfournisseur, fc.idtva,
          fc.montantttc, fc.remarque, fc.dateemission, fc.designation,
fc.iddevise, fc.numfact, fc.designationlc, fc.creditinitial,
          fc.creditmodifier, fc.montanteng, fc.montantvis, fc.montantfac,
          fc.idtypeligne, fc.numcompte, fc.identite, fc.iddirection, fc.mois,
fc.annee, fc.parution, fc.idligne, NVL (op.montant, 0) AS opmontant,
          fc.montantttc - NVL (op.montant, 0) AS reste,
          fc.datyecheance
     FROM factureflcvue fc, ordonnerpayementgroupefact op
    WHERE fc.idfacturefournisseur = op.ded_id(+) and fc.IDLIGNE=op.IDLIGNE(+) and fc.MONTANTTTC!=0;