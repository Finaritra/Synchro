
create or replace view achatfournisseurvue as
select a.id, idingredient, a.quantite,a.idunite, a.prixunitaire, a.montant, a.daty, a.iduser, a.idfournisseur,f.NOMFOURNISSEUR
	from
		achat a
join FFOURNISSEUR f on f.IDFOURNISSEUR = a.IDFOURNISSEUR
;
create or replace view VUE_INGREDIENT_FOURNISSEUR as
select
		ingr.id as id,
		ingr.libelle as idingredient,
		unite.val as idunite,
		nvl(MIN(achatpu.PrixUnitaire) KEEP (DENSE_RANK FIRST ORDER BY achatpu.Daty) OVER (PARTITION BY achatpu.idIngredient),0) as prixunitaire,
		nvl(MIN(achatpu.Idfournisseur) KEEP (DENSE_RANK FIRST ORDER BY achatpu.Daty) OVER (PARTITION BY achatpu.idIngredient),'-') as idfournisseur,
		nvl(MIN(achatpu.NOMFOURNISSEUR) KEEP (DENSE_RANK FIRST ORDER BY achatpu.Daty) OVER (PARTITION BY achatpu.idIngredient),'-') as fournisseur
	from
		as_ingredients ingr
    join unite unite on unite.id = ingr.unite
		left join achatfournisseurvue achatpu on ingr.id = achatpu.idingredient;


create or replace view VUE_ACHAT_FOURNISSEUR as
select besoin.idingredient,
       ingr.idingredient as ingredient,
       besoin.quantite,
       besoin.idunite,
       ingr.idunite      as unite,
       ingr.prixunitaire,
       besoin.daty,
       besoin.iduser,
       ingr.idfournisseur,
       ingr.fournisseur
from besoinquotidien besoin
         join vue_ingredient_fournisseur ingr on ingr.id = besoin.idingredient;
