Insert into CAISSE
   (IDCAISSE, DESCCAISSE, RESPCAISSE, IDETATCAISSE)
 Values
   ('CRD', 'Caisse credit', '2', 'etaCs1');
COMMIT;

CREATE TABLE LiaisonMvtIntraCaisse
(
  id        VARCHAR2(50),
  daty      DATE,
  source    VARCHAR2(100),
  contre    VARCHAR2(100),
  remarque  VARCHAR2(150)
);


ALTER TABLE LiaisonMvtIntraCaisse ADD (
  CONSTRAINT LiaisonMvtIntraCaisse_PK
  PRIMARY KEY
  (id)
  ENABLE VALIDATE);


ALTER TABLE LIAISONMVTINTRACAISSE ADD 
 FOREIGN KEY (SOURCE)
 REFERENCES MVTINTRACAISSE (ID)
 ENABLE
 VALIDATE;

 ALTER TABLE LIAISONMVTINTRACAISSE ADD 
 FOREIGN KEY (CONTRE)
 REFERENCES MVTINTRACAISSE (ID)
 ENABLE
 VALIDATE;

 create or replace view MvtIntraCaisseSource as 
select m.*,l.source from MvtIntraCaisse m,LiaisonMvtIntracaisse l where M.ID=l.contre and m.etat>=11;

create or replace view MvtIntraCaisseLibSource as 
select m.*,l.source from MVTINTRACAISSELIB m,LiaisonMvtIntracaisse l where M.ID=l.contre and m.etat>=11;

create or replace view MvtIntraCaisseSourceGroupe as
select '-' as id, max(daty) as daty, max(caissedepart) as caissedeepart,max(caissearrive) as caissearrive, sum(montant) as montant,'-' as remarque,
max(iduser) as iduser,min(etat) as etat,source from MvtIntraCaisseSource group by source;

create or replace view MvtIntraCaisse_SumSource as
select m.*,nvl(s.montant,0) as rembourse,m.montant-nvl(s.montant,0) as reste from MvtIntraCaisse m,MvtIntraCaisseSourceGroupe s
where m.id=s.source(+);

create or replace view MvtIntraCaisseLib_SumSource as
select m.*,nvl(s.montant,0) as rembourse,m.montant-nvl(s.montant,0) as reste from MVTINTRACAISSELIB m,MvtIntraCaisseSourceGroupe s
where m.id=s.source(+);

create sequence seqLiaisonIntraCaisse;

CREATE OR REPLACE FORCE VIEW AS_COMMANDECLIENT_LIBELLE
(
   ID,
   NUMCOMMANDE,
   DATESAISIE,
   DATECOMMANDE,
   RESPONSABLE,
   CLIENT,
   TYPECOMMANDE,
   REMARQUE,
   ADRESSELIV,
   HEURELIV,
   DISTANCE,
   DATELIV,
   VENTE,
   ETAT,
   SECT,
   RESTAURANT,
   PRENOM,
   idclient
)
AS
   SELECT cmd.ID,
          cmd.NUMCOMMANDE,
          cmd.DATESAISIE,
          cmd.DATECOMMANDE,
          tbl.TELEPHONE AS responsable,
          tbl.nom AS client,
          tp.VAL AS typecommande,
          cmd.REMARQUE,
          cmd.ADRESSELIV,
          cmd.HEURELIV,
          cmd.DISTANCE,
          cmd.DATELIV,
          vente.val AS vente,
          cmd.ETAT,
          sct.val AS sect,
          cmd.point,
          mvt.idcaisse,
          cmd.client as idclient
     FROM as_commandeclient cmd,
          as_responsable resp,
          as_typecommande tp,
          as_secteur sct,
          as_client tbl,
          vente,
          mvtCaisseLettreCmdMereGroup mvt
    WHERE     cmd.RESPONSABLE = resp.ID
          AND cmd.TYPECOMMANDE = tp.ID
          AND sct.id(+) = cmd.secteur
          AND cmd.client = tbl.id
          AND cmd.vente = vente.id(+)
          AND cmd.id = mvt.NUMPIECE(+);


CREATE OR REPLACE FORCE VIEW AS_COMMANDECLIENT_LIBELLE2
(
   ID,
   NUMCOMMANDE,
   DATESAISIE,
   DATECOMMANDE,
   RESPONSABLE,
   CLIENT,
   TYPECOMMANDE,
   REMARQUE,
   ADRESSELIV,
   HEURELIV,
   DISTANCE,
   DATELIV,
   ETAT,
   COURSIER,
   MONTANT,
   SECTEUR,
   VENTE,
   RESTAURANT,
   PRENOM,
   REVIENT,
   idclient
)
AS
     SELECT cmd."ID",
            cmd."NUMCOMMANDE",
            cmd."DATESAISIE",
            cmd."DATECOMMANDE",
            cmd."RESPONSABLE",
            cmd."CLIENT" || ' ' || cmd."RESPONSABLE" AS client,
            cmd."TYPECOMMANDE",
            cmd."REMARQUE",
            mcmd.ADRESSE AS "ADRESSELIV",
            cmd."HEURELIV",
            cmd."DISTANCE",
            cmd."DATELIV",
            CASE cmdlivre.nombrelivre
               WHEN cmdlivre.totalcommande
               THEN
                  20
               ELSE
                  (SELECT MIN (etat)
                     FROM as_detailscommande det
                    WHERE det.idmere = cmd.id AND det.etat > 0)
            END
               "ETAT",
            livr.idlivreur AS coursier,
            mtnt.montant,
            livr.idlivreur AS secteur,
            cmd.vente,
            cmd.restaurant,
            cmd.prenom,
            CAST (rev.revient AS NUMBER (10, 2)) AS revient,
            cmd.idclient
       FROM as_commandeclient_libelle cmd,
            montantparcommande mtnt,
            commandelivraisonMax mcmd,
            AS_NOMBRECOMMANDELIVRE cmdlivre,
            livraisonLibMereMax livr,
            MONTANTREVIENTCOMMANDE rev
      WHERE     mtnt.id = cmd.id
            AND cmd.ID = mcmd.idclient(+)
            AND cmd.id = cmdlivre.idmere(+)
            AND cmd.id = livr.IDMERE(+)
            AND cmd.id = rev.id(+)
   ORDER BY datecommande ASC, HEURELIV ASC;

   update as_typecommande set val='livraison',desce='livraison' where id like '%1';
update as_typecommande set val='table',desce='table' where id like '%2';
   Insert into AS_TYPECOMMANDE(ID, VAL, DESCE)Values('TPC00003', 'Resa', 'Resa');
   update as_commandeclient set typecommande='TPC00001' where client not like 'CASM%';
COMMIT;

create or replace view as_reservationSansTable as select * from as_reservation where id not in (select idreservation from resa_table) ;

create or replace view as_detailscommandeTypeCmd as 
select d.*,cmd.typecommande from as_detailscommande d,as_commandeclient cmd where d.idmere=cmd.id;

CREATE OR REPLACE FORCE VIEW ALLOSAKAFO.AS_DETAILSCOMMANDE_LIB_SAUCE2
(
   ID,
   IDMERE,
   PRODUIT,
   IDACCOMPAGNEMENT,
   PHOTO,
   IDCLIENT,
   QUANTITE,
   PU,
   REMISE,
   OBSERVATION,
   ETAT,
   typecommande
)
AS
     SELECT dt.id,
            dt.idmere,
            art.NOM AS PRODUIT,
            CONCAT (sac.idsauce, sac.IDACCOMPAGNEMENT) AS IDACCOMPAGNEMENT,
            art.PHOTO,
            cc.CLIENT AS IDCLIENT,
            dt.QUANTITE,
            dt.PU,
            dt.QUANTITE * dt.PU,
            dt.OBSERVATION,
            dt.ETAT,
            cc.typecommande
       FROM as_detailscommande dt
            JOIN as_commandeclient cc ON cc.id = dt.idmere
            JOIN as_produits art ON dt.produit = art.ID
            LEFT JOIN AS_ACCOMPAGNEMENT_SAUCE_LIB sac
               ON sac.id = dt.IDACCOMPAGNEMENTSAUCE
   ORDER BY dt.ID;


   CREATE OR REPLACE FORCE VIEW ALLOSAKAFO.AS_COMMANDECLIENT_LIBELLE
(
   ID,
   NUMCOMMANDE,
   DATESAISIE,
   DATECOMMANDE,
   RESPONSABLE,
   CLIENT,
   TYPECOMMANDE,
   REMARQUE,
   ADRESSELIV,
   HEURELIV,
   DISTANCE,
   DATELIV,
   VENTE,
   ETAT,
   SECT,
   RESTAURANT,
   PRENOM,
   IDCLIENT
)
AS
   SELECT cmd.ID,
          cmd.NUMCOMMANDE,
          cmd.DATESAISIE,
          cmd.DATECOMMANDE,
          tbl.TELEPHONE AS responsable,
          tbl.nom AS client,
          '-' AS typecommande,
          cmd.REMARQUE,
          cmd.ADRESSELIV,
          cmd.HEURELIV,
          cmd.DISTANCE,
          cmd.DATELIV,
          vente.val AS vente,
          cmd.ETAT,
          sct.val AS sect,
          cmd.point,
          mvt.idcaisse,
          cmd.client AS idclient
     FROM as_commandeclient cmd,
          as_responsable resp,
          as_secteur sct,
          as_client tbl,
          vente,
          mvtCaisseLettreCmdMereGroup mvt
    WHERE     cmd.RESPONSABLE = resp.ID
          AND sct.id(+) = cmd.secteur
          AND cmd.client = tbl.id
          AND cmd.vente = vente.id(+)
          AND cmd.id = mvt.NUMPIECE(+);


CREATE OR REPLACE FORCE VIEW TABLECLIENTVUECOMPLET
(
   ID,
   VAL,
   DESCE
)
AS
   SELECT ID AS id, NOM AS val, NOM AS desce
     FROM AS_CLIENT
    WHERE ID LIKE 'CASM%';





