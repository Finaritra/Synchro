CREATE OR REPLACE FORCE VIEW VUE_CMD_CLIENT_DETAILS_TYPEP AS 
  select
      det.id ,
      tab.val as nomtable,
      tp.val as typeproduit,cmd.datecommande,cmd.heureliv,
      prod.nom as produit,
      det.quantite,det.pu,
      (det.quantite*det.pu) as montant,
      det.etat,
      accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join tableclientvue2 tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;
    

 CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_DTLS_TYPEP_CLOTURE AS 
  select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE" from vue_cmd_client_details_typeP where etat = 9;


  CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_DTLS_TYPEP_FAIT AS 
  select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE" from vue_cmd_client_details_typeP where etat = 10;
 


CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_DTLS_TYPEP_VALIDE AS 
  select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE" from vue_cmd_client_details_typeP where etat = 11;
 

 CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_DTLS_TYPEP_LIVRE AS 
  select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE" from vue_cmd_client_details_typeP where etat = 20;
 


