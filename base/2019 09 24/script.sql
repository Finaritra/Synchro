create view vue_cmd_client_details_typeP as
  select
      det.id ,
      tab.val as nomtable,
      tp.val as typeproduit,cmd.datecommande,cmd.heureliv,
      prod.nom as produit,
      det.quantite,det.pu,
      (det.quantite*det.pu) as montant,
      det.etat
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join tableclientvue2 tab on tab.id=cmd.client
    order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc
	
create view vue_cmd_clt_dtls_typeP_cloture as
	select * from vue_cmd_client_details_typeP where etat = 9;
	
create view vue_cmd_clt_dtls_typeP_fait as
	select * from vue_cmd_client_details_typeP where etat = 10;
	
create view vue_cmd_clt_dtls_typeP_valide as
	select * from vue_cmd_client_details_typeP where etat = 11;

create view vue_cmd_clt_dtls_typeP_livre as
	select * from vue_cmd_client_details_typeP where etat = 20;
	
create view vue_cmd_clt_dtls_typeP_paye as
	select * from vue_cmd_client_details_typeP where etat = 40;
	
insert into AS_TYPEPRODUIT values ('TPD00004','Fabrication',10);

create or replace view as_produits_fabriquer as
select * from  as_produits where typeproduit='TPD00004';

update menudynamique set ID='ASM0001908' where LIBELLE='Creation';

update usermenu set INTERDIT=1 where IDMENU ='ASM000001';

insert into menudynamique values('ASM0000910','Cloturer','fa fa-liste','/allosakafo-war/pages/module.jsp?but=commande/as-commande-liste-details.jsp'||'&'||'table=',1,3,'ASM0000907');
insert into menudynamique values('ASM0000911','Fait','fa fa-liste','/allosakafo-war/pages/module.jsp?but=commande/as-commande-liste-details-fait.jsp'||'&'||'table=',2,3,'ASM0000907');
insert into menudynamique values('ASM0000912','Valider','fa fa-liste','/allosakafo-war/pages/module.jsp?but=commande/as-commande-liste-details-valider.jsp'||'&'||'table=',3,3,'ASM0000907');
insert into menudynamique values('ASM0000913','Livrer','fa fa-liste','/allosakafo-war/pages/module.jsp?but=commande/as-commande-liste-details-livrer.jsp'||'&'||'table=',4,3,'ASM0000907');
