create table commandeLivraison
(
    id varchar2(100) constraint commandeLivraison_pk primary key ,
    idclient varchar2(100)constraint commandeLiv_cli references as_client,
    adresse varchar2(100),
    idPoint varchar2(100) constraint commandeliv_poi references POINT,
    daty date,
    heure varchar2(100),
    frais number(10,2),
    etat number(10)
);

CREATE SEQUENCE SeqcommandeLivraison
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getSeqcommandeLivraison
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT SeqcommandeLivraison.nextval INTO retour FROM dual;
  return retour;
END;
/
create view commandeLivraisonlib as
    select
            cmdliv.id,
            cli.TELEPHONE as idclient,
            cmdliv.adresse,
            pt.VAL as idpoint,
            cmdliv.daty,
           cmdliv.heure,
           cmdliv.frais,
           cmdliv.etat

        from commandeLivraison cmdliv
        join AS_CLIENT cli on cli.id =cmdliv.idclient
        join POINT pt on pt.id=cmdliv.idPoint;

create table livreur
(
    id varchar2(100) constraint livreur_pk primary key ,
        nom varchar2(100) ,
        contact varchar2(100),
        etat number(10)
);

CREATE SEQUENCE Seqlivreur
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getSeqlivreur
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT Seqlivreur.nextval INTO retour FROM dual;
  return retour;
END;
/

create table livraison
(
    id varchar2(100) constraint livraison_pk primary key ,
    daty date,
    heure varchar2(100),
    idcommnademere varchar2(100) constraint livraison_mere_fk references AS_COMMANDECLIENT,
    idpoint  varchar2(100) constraint livraison_poi references POINT,
    adresse varchar2(100),
    idlivreur varchar2(100) constraint  livraison_livreur references  livreur,
    etat number(10)
);

CREATE SEQUENCE Seqlivraison
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getSeqlivraison
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT Seqlivraison.nextval INTO retour FROM dual;
  return retour;
END;
/

create view livraisonlib as
    select
            liv.id,
           liv.daty,
           liv.heure,
           liv.idcommnademere,
            pt.VAL as idpoint,
           liv.adresse,
           l.nom as idlivreur,
           liv.etat

        from livraison liv
        join AS_COMMANDECLIENT AC on liv.idcommnademere = AC.ID
        join POINT pt on pt.ID=liv.idpoint
        join livreur l on liv.idlivreur = l.id;

create view commandeLivraisonCloture as
    select
        *
        from commandeLivraisonlib cmdlib where etat=9;

create view commandeLivraisonAnnulerPayer as
select *
from commandeLivraisonlib where etat=6;


create view commandeLivraisonACloture as
    select
        *
        from commandeLivraisonlib  where etat=8;

create view LIVRAISONLIB_CREE as
    select
        *from LIVRAISONLIB where etat=1;

create view LIVRAISONLIB_LIVREE as
    select
        *from LIVRAISONLIB where etat=20;


create view commandelivraisonlibl as
    select
            cd.id,
            cd.IDMERE,
            comLi.id as idlivraison,
            prod.NOM as produit,
            cd.QUANTITE,
            cd.pu,
            cd.REMISE,
            cd.OBSERVATION,
            cd.etat,
            lib.IDSAUCE ||' '||lib.IDACCOMPAGNEMENT as IDACCOMPAGNEMENTSAUCE,
            cd.PRIORITER
        from commandeLivraison comLi
        join AS_CLIENT AC on comLi.idclient = AC.ID
        join AS_COMMANDECLIENT cmcli on cmcli.CLIENT=ac.id
        join AS_DETAILSCOMMANDE cd on cd.IDMERE=cmcli.id
        left join AS_ACCOMPAGNEMENT_SAUCE_lib lib on lib.id=cd.IDACCOMPAGNEMENTSAUCE
        left join AS_PRODUITS prod on prod.id=cd.PRODUIT;


create view commandelivraisonliblAcloturer as
    select
        *from commandelivraisonlibl where etat=8;

create view commandelivraisonlannulerPayer as
    select
        *from commandelivraisonlibl where etat=6;

create view commandelivraisonlible as
    select
            cd.id,
            cd.IDMERE,
            comLi.id as idlivraison,
            prod.NOM as produit,
            cd.QUANTITE,
            cd.pu,
            cd.REMISE,
            cd.OBSERVATION,
            cd.etat,
            lib.IDSAUCE ||' '||lib.IDACCOMPAGNEMENT as IDACCOMPAGNEMENTSAUCE,
            cd.PRIORITER,
            comli.adresse,
            pt.VAL as idpoint
        from commandeLivraison comLi
        join AS_CLIENT AC on comLi.idclient = AC.ID
        join AS_COMMANDECLIENT cmcli on cmcli.CLIENT=ac.id
        join AS_DETAILSCOMMANDE cd on cd.IDMERE=cmcli.id
        left join AS_ACCOMPAGNEMENT_SAUCE_lib lib on lib.id=cd.IDACCOMPAGNEMENTSAUCE
        left join AS_PRODUITS prod on prod.id=cd.PRODUIT
        join POINT  pt on pt.id=comLi.idPoint ;

create view commandelivraisonliblead as
    select *
    from commandelivraisonlible where adresse='';
	
	
	create view commandelivraisonliblAlivrer as
    select *
from commandelivraisonlibl where etat=12;