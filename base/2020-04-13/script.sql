create or replace view livraisonLibMere as 
select liv.id,liv.daty,liv.heure,liv.IDCOMMNADEMERE,liv.idpoint,liv.adresse,
liv.idlivreur||' Heure : '||liv.heure as idlivreur,liv.etat,det.IDMERE from livraisonlib liv, AS_DETAILSCOMMANDE det
where liv.IDCOMMNADEMERE(+)=det.id;

create or replace view livraisonLibMereMax as 
select liv.* from livraisonLibMere liv where liv.id=
(select max(liv2.id) from livraisonLibMere liv2 where liv2.IDMERE=liv.idmere );

CREATE OR REPLACE VIEW AS_COMMANDECLIENT_LIBELLE2
(ID, NUMCOMMANDE, DATESAISIE, DATECOMMANDE, RESPONSABLE, 
 CLIENT, TYPECOMMANDE, REMARQUE, ADRESSELIV, HEURELIV, 
 DISTANCE, DATELIV, ETAT, COURSIER, MONTANT, 
 SECTEUR, VENTE, RESTAURANT, PRENOM)
AS 
select
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE", mcmd.ADRESSE as "ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",       
       case  cmdlivre.nombrelivre WHEN cmdlivre.totalcommande then 20
       else (select min(etat) from as_detailscommande det where det.idmere=cmd.id and det.etat>0)
       end  "ETAT" ,  livr.idlivreur as coursier, mtnt.montant, livr.idlivreur as secteur ,cmd.vente,cmd.restaurant,cmd.prenom
from
    as_commandeclient_libelle cmd,
    montantparcommande mtnt,
    commandelivraisonMax mcmd,
    AS_NOMBRECOMMANDELIVRE  cmdlivre,
    livraisonLibMereMax livr
where
    mtnt.id = cmd.id 
    and cmd.ID=mcmd.idclient(+)
    and cmd.id=cmdlivre.idmere(+)
    and cmd.id=livr.IDMERE(+)
    ORDER BY  HEURELIV asc;
