--CI00003
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0001', 'Vin rouge 75cl', 'UNT00005', 1, 0, 'CI00003');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0002', 'Sauce pho', 'UNT00001', 1, 0, 'CI00003');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0003', 'Poudre pho', 'UNT00001', 1, 0, 'CI00003');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0004', 'Filtre cafe n°4', 'UNT00005', 1, 0, 'CI00003');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0005', 'Riz basmati', 'UNT00001', 1, 0, 'CI00003');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0006', 'Champignon de Paris', 'UNT00001', 1, 0, 'CI00003');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0008', 'Baie rose', 'UNT00001', 1, 0, 'CI00003');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0009', 'Cornichons', 'UNT00001', 1, 0, 'CI00003');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0010', 'Couscous', 'UNT00001', 1, 0, 'CI00003');

--Autre
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0011', 'Catch', 'UNT00005', 1, 0, 'CI00005');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0014', 'Chiffon', 'UNT00005', 1, 0, 'CI00005');
update as_ingredients set unite = 'UNT00005' where id in ('IGF0011', 'IGF0014', 'IGF0015', 'IGF0018' );
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0015', 'Nettoyant tapis', 'UNT00005', 1, 0, 'CI00005');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0017', 'Cire neutre', 'UNT00001', 1, 0, 'CI00005');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0018', 'Allume gaz', 'UNT00005', 1, 0, 'CI00005');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0019', 'Allumettes', 'UNT00005', 1, 0, 'CI00005');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0020', 'Diffuseur nuvan', 'UNT00005', 1, 0, 'CI00005');

insert into inventairedetails(ID, IDMERE, IDINGREDIENT, QUANTITER, ETAT, IDCATEGORIEINGREDIENT, DATY) values('IVD0001', 'IM001011', 'IGF0017', 2925, 1, 'CI00005', '15-01-2021');
insert into inventairedetails(ID, IDMERE, IDINGREDIENT, QUANTITER, ETAT, IDCATEGORIEINGREDIENT, DATY) values('IVD0002', 'IM001011', 'IGF0018', 15, 1, 'CI00005', '15-01-2021');
insert into inventairedetails(ID, IDMERE, IDINGREDIENT, QUANTITER, ETAT, IDCATEGORIEINGREDIENT, DATY) values('IVD0003', 'IM001011', 'IGF0019', 20, 1, 'CI00005', '15-01-2021');
insert into inventairedetails(ID, IDMERE, IDINGREDIENT, QUANTITER, ETAT, IDCATEGORIEINGREDIENT, DATY) values('IVD0004', 'IM001011', 'IGF0020', 2, 1, 'CI00005', '15-01-2021');

--Boisson
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0012', 'Dzama Blanc 42°', 'UNT00004', 1, 0, 'CI00001');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0013', 'Dzama 52° ambre', 'UNT00004', 1, 0, 'CI00001');
insert into as_ingredients (ID, LIBELLE, UNITE, QUANTITEPARPACK, COMPOSE, CATEGORIEINGREDIENT) VALUES('IGF0016', 'Jus citron', 'UNT00004', 1, 0, 'CI00001');
