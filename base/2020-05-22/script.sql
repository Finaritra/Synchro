  CREATE OR REPLACE  VIEW "AS_INGREDIENTS_LIBELLE_COMPTA" ("ID", "LIBELLE", "SEUIL", "UNITE", "QUANTITEPARPACK", "PU", "CATEGORIEINGREDIENT","PHOTO") AS 
  select as_ingredients.id, as_ingredients.libelle, as_ingredients.seuil, u.val as unite, 
as_ingredients.quantiteparpack, as_ingredients.pu,CATEGORIEINGREDIENT.val as CATEGORIEINGREDIENT,as_ingredients.photo
from as_ingredients join as_unite u on as_ingredients.unite = u.id
join CATEGORIEINGREDIENT on as_ingredients.CATEGORIEINGREDIENT=CATEGORIEINGREDIENT.id;

alter table detailsfacturefournisseur add compte VARCHAR2(100);


 CREATE OR REPLACE VIEW "FACTUREFOURNISSEURMONTANTTTC" ("ID", "DATY", "IDFOURNISSEUR", "IDTVA", "MONTANTTTC", "REMARQUE", "DATEEMISSION", "DESIGNATION", "IDDEVISE", "NUMFACT", "RESP", "DATYECHEANCE", "ETAT", "IDFACTUREFOURNISSEUR") AS 
  select 
    ff.id,
    ff.daty,
    ff.idfournisseur,
    ff.montantttc as idtva,
    (select sum (pu*qte) from detailsfacturefournisseur where ff.id=idmere) as montantttc,
    ff.remarque,
    ff.dateemission,
    ff.designation,
    ff.iddevise,
    ff.numfact,
    ff.resp,
    ff.datyecheance,
    ff.etat,
    ff.idfacturefournisseur
from facturefournisseur ff;