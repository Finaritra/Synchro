--Accompagnement misao
update as_accompagnement_sauce set etat=0 where idaccompagnement in ('ACC00005', 'ACC00006');

--Accompagnement assiette de boulettes de poisson
update as_accompagnement_sauce set etat=0 where idsauce in ('SAUM00001', 'SAUM00002', 'SAUM00003') and remarque like 'PRDTMP000006';
update as_produits set designation = 'Boulette de poisson sauce huitre', nom='Boulette de poisson sauce huitre' where id like 'PRDTMP000006';

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTMP000201', 'Boulette de poisson sauce champignons noirs', 'Boulette de poisson sauce champignons noirs', null,'ASTP000002','1','0',null,null,null,null,'12000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRP000251', 'PRDTMP000201', TO_DATE('01-01-2021','DD-MM-YYYY'), 10000);
INSERT INTO AS_RECETTE(ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, UNITE) VALUES ('RCE00015','PRDTMP000201','IG000716','1','UNT00005');


INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTMP000202', 'Boulette de poisson sauce aigre doux', 'Boulette de poisson sauce aigre doux', null,'ASTP000002','1','0',null,null,null,null,'12000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRP000252', 'PRDTMP000202', TO_DATE('01-01-2021','DD-MM-YYYY'), 10000);
INSERT INTO AS_RECETTE(ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, UNITE) VALUES ('RCE00016','PRDTMP000202','IG000716','1','UNT00005');


--Accompagnement poisson farci
update as_accompagnement_sauce set etat=0 where idsauce in ('SAUM00001', 'SAUM00002', 'SAUM00003') and remarque like 'PRDTMP000003';
update as_produits set nom = 'Poisson farci sauce huitre', designation='Poisson farci sauce huitre' where id like 'PRDTMP000003';
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTMP000203', 'Poisson farci sauce champignons noirs', 'Poisson farci sauce champignons noirs', null,'ASTP000002','1','0',null,null,null,null,'15000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRP000253', 'PRDTMP000203', TO_DATE('01-01-2021','DD-MM-YYYY'), 14000);
INSERT INTO AS_RECETTE(ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, UNITE) VALUES ('RCE00017','PRDTMP000203','INGS000837','1','UNT00005');


INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDTMP000204', 'Poisson farci sauce aigre doux', 'Poisson farci sauce aigre doux', null,'ASTP000002','1','0',null,null,null,null,'15000','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRP000254', 'PRDTMP000204', TO_DATE('01-01-2021','DD-MM-YYYY'), 14000);
INSERT INTO AS_RECETTE(ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, UNITE) VALUES ('RCE00018','PRDTMP000204','INGS000837','1','UNT00005');

commit;