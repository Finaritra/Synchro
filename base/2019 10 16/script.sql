create or replace view INGREDIENT_PRODUIT as
	select
		ingr.id, ingr.libelle, ingr.unite, rec.idproduits ,sum(rec.quantite) as quantite,rec.UNITE as idunite
	from
		as_ingredients ingr
		join as_recette rec on rec.idingredients = ingr.id
	group by ingr.id, ingr.libelle, ingr.unite, rec.idproduits,rec.UNITE;
	
create or replace view VUE_BESOIN_QUOTIDIEN as
	select
		ingr.id, ingr.libelle, unit.val as unite,
		(ingr.quantite * bsn.quantite) as quantite,
       idunite
	from ingredient_produit ingr
	join besoin bsn on bsn.idproduit = ingr.idproduits
	join unite unit on unit.id = ingr.unite;
	
create or replace view VUE_BESOIN_QUOTIDIEN_GROUPE as
	select
		besoin.id as idingredient, besoin.libelle as ingredient, besoin.unite, sum(besoin.quantite) as quantite,idunite
	from vue_besoin_quotidien besoin
	group by besoin.id, besoin.libelle, besoin.unite,idunite;
	
CREATE TABLE "BESOINQUOTIDIEN" 
   (	"ID" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"DATY" DATE, 
	"IDUNITE" VARCHAR2(50 BYTE), 
	"QUANTITE" NUMBER, 
	"IDINGREDIENT" VARCHAR2(50 BYTE), 
	"QUANTITERESTANT" NUMBER, 
	"IDUNITERESTANT" VARCHAR2(50 BYTE), 
	"IDUSER" NUMBER, 
	 CONSTRAINT "BESOINQUOTIDIEN_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE, 
	 CONSTRAINT "BESOINQUOTIDIEN_FK1" FOREIGN KEY ("IDUNITE")
	  REFERENCES "SAKAFO"."UNITE" ("ID") ENABLE, 
	 CONSTRAINT "BESOINQUOTIDIEN_FK2" FOREIGN KEY ("IDINGREDIENT")
	  REFERENCES "SAKAFO"."AS_INGREDIENTS" ("ID") ENABLE, 
	 CONSTRAINT "BESOINQUOTIDIEN_FK3" FOREIGN KEY ("IDUNITERESTANT")
	  REFERENCES "SAKAFO"."UNITE" ("ID") ENABLE, 
	 CONSTRAINT "BESOINQUOTIDIEN_FK4" FOREIGN KEY ("IDUSER")
	  REFERENCES "SAKAFO"."UTILISATEUR" ("REFUSER") ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;