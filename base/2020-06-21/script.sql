CREATE OR REPLACE VIEW AS_MVTSTOCK_FDTLIBUNITE
(ID, INGREDIENTS, ENTREE, SORTIE, DATY, 
 UNITE, IDINGREDIENTS, POINT,ECART,categorieIngredient)
AS 
select fille.id,grd.libelle as ingredients, fille.entree, fille.sortie, fille.daty, grd.unite,
fille.INGREDIENTS,fille.point, fille.entree-fille.sortie as ecart,grd.categorieIngredient
  from as_mvt_groupeDecomp fille, as_ingredients_libelle grd
    where fille.ingredients = grd.id
    and fille.etat>=11
/


INSERT INTO MENUDYNAMIQUE ( ID, LIBELLE, ICONE, HREF, RANG, NIVEAU,
ID_PERE ) VALUES ( 
'ASMST00410s', 'Analyse decompose', 'fa fa-plus', '/phobo/pages/module.jsp?but=stock/mvtStock/analyseStDecompose.jsp'
, 6, 3, 'ASM0000803'); 
COMMIT;
