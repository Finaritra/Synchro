
CREATE SEQUENCE seqbesoin
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getseqbesoin
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqbesoin.nextval INTO retour FROM dual;
  return retour;
END;
/
update menudynamique set href='/phobo/pages/module.jsp?but=stock/besoin/saisie-besoin.jsp' where id='MEN00108';
commit;



update as_mvt_stock set depot='defaut' where depot like 'Ampasamadini%' or depot or is null;
	commit;