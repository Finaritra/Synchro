CREATE OR REPLACE VIEW "AS_PRODUITS_LIBELLEPRIX" ("ID", "NOM", "DESIGNATION", "PHOTO", "TYPEPRODUIT", "CALORIE", "PA", "POIDS", "PU") AS
 select
   prd.id, prd.nom, prd.designation, prd.photo, tpd.val,
   case when prd.calorie = '1' then 'DISPONIBLE'
   else 'INDISPONIBLE' end,
   prd.pa, prd.poids, sum(trf.montant) keep (dense_rank first order by trf.dateapplication desc)
from
   as_produits prd,
   as_typeproduit tpd,
   as_prixproduit trf
where
   prd.typeproduit = tpd.id
   and trf.produit = prd.id
group by prd.id, prd.nom, prd.designation,
prd.photo, tpd.val, prd.calorie, prd.pa,
prd.poids;


   CREATE OR REPLACE VIEW AS_NOMBRECOMMANDELIVRE AS
   select t1.idmere, t1.nombrelivre, t2.total as totalcommande from
   (
   select det.idmere, count(*) as nombrelivre,0 as total  from  as_commandeclient mere inner join as_detailscommande det on mere.id=det.idmere
   where  det.produit NOT LIKE '%LIVR%' and det.etat>=20 and det.etat>0
   group by idmere
   ) t1
   inner join
   (
   select det.idmere,0 nombrelivre, count(*) as total  from  as_commandeclient mere inner join as_detailscommande det on mere.id=det.idmere
   where  det.produit  NOT LIKE '%LIVR%' and det.etat>0
   group by idmere
   )
   t2
   on t1.idmere=t2.idmere;
   
   CREATE OR REPLACE  VIEW "AS_COMMANDECLIENT_LIBELLE2" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR", "VENTE", "RESTAURANT", "PRENOM")
   as
  select
      cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
      cmd."REMARQUE", mcmd.ADRESSE as "ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",
         case  cmdlivre.nombrelivre WHEN cmdlivre.totalcommande then 20
         else (select min(etat) from as_detailscommande det where det.idmere=cmd.id)
         end  "ETAT" ,  '-', mtnt.montant, sect.val as secteur ,cmd.vente,cmd.restaurant,cmd.prenom
  from
      as_commandeclient_libelle cmd,
      montantparcommande mtnt,
      as_secteur sect,
      commandelivraisonMax mcmd,
      AS_NOMBRECOMMANDELIVRE  cmdlivre
  where
      mtnt.id = cmd.id and sect.id(+)=cmd.sect
      and cmd.ID=mcmd.idclient(+)
      and cmd.id=cmdlivre.idmere(+)
      ORDER BY  DATELIV,HEURELIV asc;
