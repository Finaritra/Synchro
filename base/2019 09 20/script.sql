create or replace view as_typeproduit_souscategorie as
	select idtypeproduit,libelle_type,LISTAGG(idsouscategorie, ';') WITHIN GROUP (ORDER BY idsouscategorie) as idsouscategories
	  from(
		select tp.id as idtypeproduit, tp.val as libelle_type,scat.id as idsouscategorie
		  from 
			as_produits prd
			join as_typeproduit tp on tp.id = prd.typeproduit
			left join as_souscategorie scat on scat.id = prd.idsouscategorie
		  group by tp.id,tp.val,scat.id)
	  group by idtypeproduit,libelle_type
	  
