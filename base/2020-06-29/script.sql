CREATE OR REPLACE FORCE VIEW AS_ETATSTOCKVIDE
AS
   SELECT '-' AS ingredients,
          '-' AS unite,
          CAST (0 AS NUMBER (10, 2)) AS entree,
          CAST (0 AS NUMBER (10, 2)) AS sortie,
          CAST (0 AS NUMBER (10, 2)) AS reste,
          CAST (0 AS NUMBER (10, 2)) AS report,
          '' AS daty,
          '' AS idcategorieingredient,
          '-' AS point,
          CAST (0 AS NUMBER (10, 2)) AS besoin,
          '-' AS idfournisseur,
          CAST (0 AS NUMBER (10, 2)) AS pu,
          CAST (0 AS NUMBER (10, 2)) AS montant,
          CAST (0 AS NUMBER (10, 2)) AS montantStock,
          '-' AS idingredient,
          '-' AS fournisseur,
          CAST (0 AS NUMBER (10, 2)) AS reportSuiv

     FROM DUAL;



CREATE INDEX idxDt ON INVENTAIREMERE
(DATY)
LOGGING
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

CREATE INDEX idxDet ON DETAILSFACTUREFOURNISSEUR
(IDMERE)
LOGGING
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

CREATE INDEX idxIdInv ON INVENTAIREDETAILS
(IDINGREDIENT, IDMERE)
LOGGING
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

CREATE INDEX idxRec ON AS_RECETTE
(IDPRODUITS, IDINGREDIENTS)
LOGGING
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

create view bondelivraisonavecfournisseur as
select
    bl.*,
    ff.IDFOURNISSEUR,
    fp.nom as libelleFournisseur

from AS_BONDELIVRAISON bl
    join FACTUREFOURNISSEUR ff on ff.id=bl.idbc
    left join  FOURNISSEURPRODUITS fp on fp.id=ff.IDFOURNISSEUR;

create view blmerefille as
    select
            bf.*,
           b.REMARQUE AS REMARQUEMERE,
           b.IDBC AS IDBCMERE,
           B.DATY AS DATYMERE,
           B.ETAT AS ETATMERE
        from AS_BONDELIVRAISON_FILLE bf
        join AS_BONDELIVRAISON b on b.id=bf.NUMBL;