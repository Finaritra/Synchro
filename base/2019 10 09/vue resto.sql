
CREATE OR REPLACE FORCE VIEW "AS_DETAILSCOMMANDE_LIB_SAUCE" ("ID", "IDMERE", "PRODUIT", "IDACCOMPAGNEMENT", "PHOTO", "IDCLIENT", "QUANTITE", "PU", "REMISE", "OBSERVATION", "ETAT") AS 
select
    dt.id, dt.idmere, art.NOM as PRODUIT , CONCAT ( sac.idsauce , sac.IDACCOMPAGNEMENT ) as IDACCOMPAGNEMENT , art.PHOTO , cc.CLIENT as IDCLIENT , dt.QUANTITE, dt.PU,  dt.QUANTITE * dt.PU  ,  dt.OBSERVATION , dt.ETAT
from 
as_detailscommande dt 
join as_commandeclient cc on cc.id = dt.idmere 
join as_produits art on dt.produit = art.ID 
left join AS_ACCOMPAGNEMENT_SAUCE_LIB sac on sac.id = dt.IDACCOMPAGNEMENTSAUCE  ORDER BY dt.ID;
 

CREATE OR REPLACE FORCE VIEW "VUE_CMD_CLT_DETS_TYPP_REST" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "RESTAURANT", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE", "PRIORITER", "OBSERVATION", "IDTYPE") AS 
	select
	det.id,
	tab.nom as nomtable,
	tp.val as typeproduit,
	cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
	prod.nom as produit,
	det.quantite,det.pu,
	(det.quantite*det.pu) as montant,det.etat,
	accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
	det.prioriter,
	det.observation,
	tp.id as idtype
from as_commandeclient cmd
join as_detailscommande det on det.idmere = cmd.id
join as_produits prod on prod.id=det.produit
join as_typeproduit tp on tp.id=prod.typeproduit
join as_client tab on tab.id=cmd.client
left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;


CREATE OR REPLACE FORCE VIEW "VUE_CMD_CLT_DETAIL_TYPEP_REST" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "RESTAURANT", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  select
		det.id,
		tab.nom as nomtable,
		tp.val as typeproduit,
		cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
		prod.nom as produit,
		det.quantite,det.pu,
		(det.quantite*det.pu) as montant,
		det.etat,
		accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join as_client tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;


CREATE OR REPLACE FORCE VIEW "AS_COMMANDECLIENT_LIBELLE" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "VENTE", "ETAT", "SECT", "RESTAURANT", "PRENOM") AS 
select
    cmd.ID, cmd.NUMCOMMANDE, cmd.DATESAISIE, cmd.DATECOMMANDE, resp.NOM ||' '||resp.PRENOM as responsable, tbl.nom as client, tp.VAL as typecommande,
    cmd.REMARQUE, cmd.ADRESSELIV, cmd.HEURELIV, cmd.DISTANCE, cmd.DATELIV, vente.val as vente,cmd.ETAT, sct.val as sect, cmd.point , tbl.prenom
from
    as_commandeclient cmd,
    as_responsable resp,
    as_typecommande tp,
    as_secteur sct,
    as_client tbl,
    vente 
where
    cmd.RESPONSABLE = resp.ID
    and cmd.TYPECOMMANDE = tp.ID
    and sct.id (+) = cmd.secteur 
    and cmd.client = tbl.id
    and cmd.vente=vente.id(+);


CREATE OR REPLACE FORCE VIEW "AS_COMMANDECLIENT_LIBELLE2" ("ID", "NUMCOMMANDE", "DATESAISIE", "DATECOMMANDE", "RESPONSABLE", "CLIENT", "TYPECOMMANDE", "REMARQUE", "ADRESSELIV", "HEURELIV", "DISTANCE", "DATELIV", "ETAT", "COURSIER", "MONTANT", "SECTEUR", "VENTE", "RESTAURANT", "PRENOM") AS 
select distinct
    cmd."ID",cmd."NUMCOMMANDE",cmd."DATESAISIE",cmd."DATECOMMANDE",cmd."RESPONSABLE",cmd."CLIENT",cmd."TYPECOMMANDE",
    cmd."REMARQUE",cmd."ADRESSELIV",cmd."HEURELIV",cmd."DISTANCE",cmd."DATELIV",cmd."ETAT",  liv.responsable, mtnt.montant, sect.val as secteur ,cmd.vente,cmd.restaurant,cmd.prenom
from
    as_commandeclient_libelle cmd,
    (select cmd.id as commande, liv.id, veh.nom as responsable 
    from as_commande_livre cmd, as_livraison liv, as_vehicule veh 
    where   liv.commande like '%'||cmd.id||'%' and veh.id(+)=liv.cuisinier) liv,
    montantparcommande mtnt,
	as_secteur sect
where
    mtnt.id = cmd.id and liv.commande(+)=cmd.id and sect.id(+)=cmd.sect;


CREATE OR REPLACE FORCE VIEW "TABLECLIENT_COMMANDE" ("ID", "VAL", "DESCE") AS 
select 
	ID as id,
	nom,
	case 
	when id like 'CASM%' 
	    then 'Normale'
	when id like 'CLT%'
	    then 'Nouveau'
	end as desce
from AS_CLIENT where nom like 'Table%' or nom like 'Emporter%';

CREATE OR REPLACE FORCE VIEW "TABLECLIENT_COMMANDEDET" ("ID", "VAL", "DESCE") AS 
select
	id,
	val ||' '|| desce as val,
	val as desce
from TABLECLIENT_COMMANDE
