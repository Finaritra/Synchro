create view vue_cmd_clt_dets_typp_rest as
	select
		det.id,
		tab.val as nomtable,
		tp.val as typeproduit,
		cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
		prod.nom as produit,
		det.quantite,det.pu,
		(det.quantite*det.pu) as montant,det.etat,
		accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
		det.prioriter,
		det.observation,
		tp.id as idtype
	from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join tableclientvue2 tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;
	
create view vue_cmd_dtls_typep_clot_rest as
	select
	  id, nomtable, typeproduit, datecommande, heureliv, restaurant, produit, quantite, pu, montant, etat, acco_sauce, prioriter, observation, idtype
	from vue_cmd_clt_dets_typp_rest where etat = 9 and idtype != 'TPD00001' and produit != 'Barquette'  order by DATECOMMANDE , HEURELIV ASC,prioriter asc;
	
create view vue_cmd_clt_detail_typep_rest as
	select
		det.id,
		tab.val as nomtable,
		tp.val as typeproduit,
		cmd.datecommande,cmd.heureliv,cmd.point as restaurant,
		prod.nom as produit,
		det.quantite,det.pu,
		(det.quantite*det.pu) as montant,
		det.etat,
		accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join tableclientvue2 tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;
	
create view vue_cmd_clt_dtls_valide_rest as
	select 
		id, nomtable, typeproduit, datecommande, heureliv, restaurant, produit, quantite, pu, montant, etat, acco_sauce
	from  vue_cmd_clt_detail_typep_rest where etat = 11 order by datecommande asc, heureliv asc;
	
create view vue_cmd_dtls_livre_payer_rest as
	select 
		id, nomtable, typeproduit, datecommande, heureliv, restaurant, produit, quantite, pu, montant, etat, acco_sauce
	from  vue_cmd_clt_detail_typep_rest where etat > 0 and etat <= 20;
	
create view vue_cmd_dtls_clot_bois_rest as
	select 
		id, nomtable, typeproduit, datecommande, heureliv, restaurant, produit, quantite, pu, montant, etat, acco_sauce, prioriter, observation, idtype
	from vue_cmd_clt_dets_typp_rest where etat = 9 and (idtype = 'TPD00001' or produit like '%Barquet%');
	
CREATE OR REPLACE FORCE VIEW AS_DETAILSCOMMANDE_LIB_SAUCE2 ("ID", "IDMERE", "PRODUIT", "IDACCOMPAGNEMENT", "PHOTO", "IDCLIENT", "QUANTITE", "PU", "REMISE", "OBSERVATION", "ETAT") AS 
  select
    dt.id, dt.idmere, art.NOM as PRODUIT , CONCAT ( sac.idsauce , sac.IDACCOMPAGNEMENT ) as IDACCOMPAGNEMENT , art.PHOTO , cc.CLIENT as IDCLIENT , dt.QUANTITE, dt.PU,  dt.QUANTITE * dt.PU  ,  dt.OBSERVATION , dt.ETAT
from 
as_detailscommande dt 
join as_commandeclient cc on cc.id = dt.idmere 
join as_produits art on dt.produit = art.ID 
left join AS_ACCOMPAGNEMENT_SAUCE_LIB sac on sac.id = dt.IDACCOMPAGNEMENTSAUCE ORDER BY dt.ID;