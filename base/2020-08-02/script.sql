CREATE OR REPLACE FORCE VIEW AS_MVT_STOCKLIBELLE
(
   ID,
   DATY,
   DESIGNATION,
   TYPEMVT,
   OBSERVATION,
   DEPOT,
   TYPEBESOIN,
   ETAT,
   FOURNISSEUR,
   BENEFICIAIRE,
   NUMBC,
   NUMBS
)
AS
   SELECT mvt.id,
          mvt.daty,
          mvt.designation,
          mvt.typemvt,
          mvt.observation,
          mvt.depot,
          mvt.typebesoin,
          mvt.etat,
          mvt.fournisseur,
          mvt.beneficiaire,
          mvt.numbc,
          det.PRODUIT AS numbs
     FROM as_mvt_stock mvt, as_detailscommande_lib det 
    WHERE mvt.NUMBC = det.id(+);