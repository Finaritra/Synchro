CREATE OR REPLACE FORCE VIEW COMMANDECLIENTDETAILSWITHPOINT
(
   ID,
   IDMERE,
   PRODUIT,
   QUANTITE,
   PU,
   REMISE,
   OBSERVATION,
   ETAT,
   IDACCOMPAGNEMENTSAUCE,
   PRIORITER,
   CLIENT,
   POINT,
   revient
)
AS
   SELECT details."ID",
          details."IDMERE",
          details."PRODUIT",
          details."QUANTITE",
          details."PU",
          details."REMISE",
          details."OBSERVATION",
          details."ETAT",
          details."IDACCOMPAGNEMENTSAUCE",
          details."PRIORITER",
          client,
          point,
          DETAILS.REVIENT
     FROM as_detailscommande details
          JOIN as_commandeclient commande ON commande.id = details.idmere;