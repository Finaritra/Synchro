
update as_ingredients set compose=1 where id  in(select idproduits from as_recette);

update as_ingredients set compose=0 where id not in(select idproduits from as_recette);

create or replace view as_recettecompose as select r.*,ing.compose from as_recette r,as_ingredients ing
  where r.IDINGREDIENTS=ing.id;

  delete from as_recette where idproduits=idingredients;

create or replace view as_recettebesoincompose as 
select  level rang,rec.id, idproduits,idingredients,rec.quantite,compose,CONNECT_BY_ROOT idproduits mere,
nvl(to_number(SUBSTR((SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1))), 
(INSTR(SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1)), '/', -1))+1)),1) 
as qteAv
 from as_recettecompose rec where rec.compose=0
  start with idproduits in (select bes.idproduit from besoin bes)
  connect by prior idingredients = idproduits;


  create or replace view besoinrecetteManque as 
  select rec.idingredients,sum(b.QUANTITE*rec.QUANTITE*rec.QTEAV) as besoin from 
  as_recettebesoincompose rec, besoin b
  where rec.mere=b.IDPRODUIT group by rec.idingredients;

  create or replace view besoinrecette as
  select ing.id as idingredients,nvl(bes.besoin,0) as besoin from as_ingredients ing, besoinrecetteManque bes
  where ing.id=bes.IDINGREDIENTS(+);


create or replace view as_recetteinventairecompose as 
select  level rang,rec.id, idproduits,idingredients,rec.quantite,compose,CONNECT_BY_ROOT idproduits mere,
nvl(to_number(SUBSTR((SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1))), 
(INSTR(SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1)), '/', -1))+1)),1) 
as qteAv
 from as_recettecompose rec where compose=0
  start with idproduits in (select inv.idingredient from inventairedetails inv)
  connect by prior idingredients = idproduits;
  



  create or replace view inventaireDetailsIngDecompose as
  select det.id, det.idmere,det.idingredient,det.idUser,det.quantiter,det.explication,im.etat as etat,
  det.IDCATEGORIEINGREDIENT,im.daty as daty ,nvl(rec.IDINGREDIENTS, det.idingredient) as idingredients,nvl(rec.QUANTITE*rec.qteav,1) as quantite 
  from inventairedetails det, as_recetteinventairecompose rec,inventaireMere im
  where det.IDINGREDIENT=rec.mere(+)  and det.idmere=im.id;


 create or replace view inventaireDetailsDecompose as 
 select '-' as id,max(inv.idmere) as idmere,inv.IDINGREDIENTS as idingredient,inv.IDUSER,
 sum((inv.QUANTITER*inv.QUANTITE)) as quantiter, inv.EXPLICATION,inv.etat,
 ing.CATEGORIEINGREDIENT as IDCATEGORIEINGREDIENT ,inv.DATY from inventaireDetailsIngDecompose inv, as_ingredients ing
 where ing.id=inv.IDINGREDIENTS
 group by inv.IDINGREDIENTS,inv.IDUSER,
  inv.EXPLICATION,inv.etat,
  ing.CATEGORIEINGREDIENT,inv.DATY;

 CREATE OR REPLACE VIEW INVENTAIREDETAILSLIBINVENT
(ID, IDMERE, IDINGREDIENT, IDUSER, QUANTITER, 
 EXPLICATION, ETAT, IDCATEGORIEINGREDIENT, DATY, IDING, 
 UNITE)
AS 
select
            invdet.id,
           invDet.idmere,
           ing.LIBELLE as idingredient,
           invDet.idUser,
           invDet.quantiter,
            im.depot as explication,
           im.etat,
           catIng.VAL as idcategorieingredient,
           im.DATY,
           invDet.IDINGREDIENT,
           uni.val as unite
        from inventaireDetailsDecompose invDet
        left join AS_INGREDIENTS ing on ing.id=invDet.idingredient
        left join CATEGORIEINGREDIENT catIng on catIng.ID=invDet.IDCATEGORIEINGREDIENT
        join inventaireMere im on invDet.idmere=im.id
        join as_unite uni on ing.unite=uni.id
        where im.etat>=11 and ing.compose=0
/

create or replace view as_recetteMvtStockcompose as 
select  level rang,rec.id, idproduits,idingredients,rec.quantite,compose,CONNECT_BY_ROOT idproduits mere,
nvl(to_number(SUBSTR((SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1))), 
(INSTR(SUBSTR(SYS_CONNECT_BY_PATH(quantite, '/'),0, (INSTR(SYS_CONNECT_BY_PATH(quantite, '/'), '/',-1)-1)), '/', -1))+1)),1) 
as qteAv
 from as_recettecompose rec where compose=0
  start with idproduits in (select mvt.ingredients from as_mvt_stock_fille mvt)
  connect by prior idingredients = idproduits;


  create or replace view as_mvt_st_F_IngDecomp as
  select det.*, nvl(rec.IDINGREDIENTS,det.ingredients) as idingredients,
  nvl(rec.QUANTITE*rec.qteav,1) as quantite, mere.DATY,mere.ETAT,mere.DEPOT  
  from as_mvt_stock_fille det, as_recetteMvtStockcompose rec, as_mvt_stock mere
  where det.ingredients=rec.mere(+) and det.IDMVTSTOCK=mere.id;

create or replace view as_mvt_groupeDecomp as  
select '-' as id,'-' as mere, mvt.IDINGREDIENTS as INGREDIENTS,sum(mvt.entree*mvt.QUANTITE) as entree 
,sum(mvt.SORTIE*mvt.QUANTITE) as sortie, mvt.DATY,mvt.depot as point,mvt.etat 
from as_mvt_st_F_IngDecomp mvt
group by mvt.IDINGREDIENTS, mvt.DATY,mvt.depot,mvt.etat;

CREATE OR REPLACE VIEW AS_MVTSTOCK_FDTLIBUNITE
(ID, INGREDIENTS, ENTREE, SORTIE, DATY, 
 UNITE, IDINGREDIENTS, POINT)
AS 
select fille.id,grd.libelle as ingredients, fille.entree, fille.sortie, fille.daty, u.val as unite,
fille.INGREDIENTS,fille.point
  from as_mvt_groupeDecomp fille, as_ingredients grd,as_unite u
    where fille.ingredients = grd.id
    and grd.UNITE=u.id
    and fille.etat>=11
/
 

create or replace view produitEtIngredient as
select id,nom, designation from as_produits
union
select id, libelle as nom,categorieingredient as designation from as_ingredients;


CREATE OR REPLACE VIEW BESOINLIB
(ID, IDPRODUIT, REMARQUE, QUANTITE)
AS 
select
    bes.id,
    prd.nom as idproduit,
    bes.remarque,
    bes.quantite
    from besoin bes
    join produitEtIngredient prd on prd.id=bes.idproduit
/
  

  
