--Pate seche
INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDT000103', 'Pate seche', 'Pate seche', null,'ASTP000005','1','0',null,null,null,null,'5000','0');
INSERT INTO AS_RECETTE(ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, UNITE) VALUES ('RCN00011','PRDT000100','ING000200','1','UNT00005');

insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRP000250', 'PRDT000100', TO_DATE('01-01-2021','DD-MM-YYYY'), 4000);

--Pate seche garnie

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDT000101', 'Pate seche garnie', 'Pate seche garnie', null,'ASTP000005','1','0',null,null,null,null,'9000','0');
INSERT INTO AS_RECETTE(ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, UNITE) VALUES ('RCN00012','PRDT000101','ING000200','1','UNT00005');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRP000251', 'PRDT000101', TO_DATE('01-01-2021','DD-MM-YYYY'), 8000);


--Sambos 

INSERT INTO AS_PRODUITS (ID, NOM, DESIGNATION, PHOTO, TYPEPRODUIT, CALORIE, POIDS, PA, IDINGREDIENT, IDSOUSCATEGORIE,
                         NUMEROPLAT, PRIXL, POIDSPRECUIT)
VALUES ('PRDT000102', 'Sambos aux fruits de mer', 'Sambos aux fruits de mer', null,'ASTP000005','1','0',null,null,null,null,'800','0');
insert into as_prixproduit (id, produit, dateapplication, montant) values ('TRP000252', 'PRDT000102', TO_DATE('01-01-2021','DD-MM-YYYY'), 800);