CREATE OR REPLACE FORCE VIEW INVENTAIREDETAILSLIB
(
   ID,
   IDMERE,
   IDINGREDIENT,
   IDUSER,
   QUANTITER,
   EXPLICATION,
   ETAT,
   IDCATEGORIEINGREDIENT,
   DATY,
   depot
)
AS
   SELECT invdet.id,
          invDet.idmere,
          ing.LIBELLE AS idingredient,
          invDet.idUser,
          invDet.quantiter,
          invDet.idingredient AS explication,
          im.etat,
          catIng.VAL AS idcategorieingredient,
          im.DATY,
          IM.DEPOT
     FROM inventairedetails invDet
          LEFT JOIN AS_INGREDIENTS ing ON ing.id = invDet.idingredient
          LEFT JOIN CATEGORIEINGREDIENT catIng
             ON catIng.ID = invDet.IDCATEGORIEINGREDIENT
          JOIN inventaireMere im ON invDet.idmere = im.id
    WHERE im.etat >= 1;