CREATE OR REPLACE FORCE VIEW AS_PRODUITS_TYPE_LIBELLEPRIX
(
   ID,
   NOM,
   DESIGNATION,
   PHOTO,
   TYPEPRODUIT,
   PRIXL,
   VAL_CALORIE,
   CALORIE,
   PA,
   POIDS,
   PU,
   PRIORITE
)
AS
     SELECT prd.id,
            prd.nom,
            prd.designation,
            prd.photo,
            prd.typeproduit,
            prd.prixl,
            CAST (prd.calorie AS INT),
            CASE
               WHEN prd.calorie = '1'
               THEN
                  '<b style="color:green">DISPONIBLE</b>'
               ELSE
                  '<b style="color:red">INDISPONIBLE</b>'
            END,
            prd.pa,
            prd.poids,
            trf.montant,
            CAST (tpd.desce AS INT)
            
       FROM as_produits prd, as_typeproduit tpd, as_prixproduit trf
      WHERE prd.typeproduit = tpd.id AND trf.produit = prd.id;



      CREATE OR REPLACE FORCE VIEW AS_PRODUITSREVIENT
(
   ID,
   NOM,
   DESIGNATION,
   PHOTO,
   TYPEPRODUIT,
   VAL_CALORIE,
   CALORIE,
   PA,
   POIDS,
   PU,
   PRIORITE,
   REVIENT,
   prixL
)
AS
   SELECT pr."ID",
          pr."NOM",
          pr."DESIGNATION",
          pr."PHOTO",
          pr."TYPEPRODUIT",
          pr."VAL_CALORIE",
          pr."CALORIE",
          pr."PA",
          pr."POIDS",
          pr."PU",
          pr."PRIORITE",
          CAST (rev.montant AS NUMBER (10, 2)) AS revient,
          PR.PRIXL
     FROM As_produits_type_libelleprix pr
          LEFT JOIN as_produitsPU rev ON pr.id = rev.mere;



          CREATE OR REPLACE FORCE VIEW AS_PRODUIT_PRIX
(
   ID,
   NOM,
   DESIGNATION,
   PHOTO,
   TYPEPRODUIT,
   CALORIE,
   POIDS,
   PA,
   DATEAPPLICATION,
   MONTANT,
   OBSERVATION,
   PRIXL
)
AS
   SELECT as_produits.id,
          nom,
          designation,
          photo,
          typeproduit,
          calorie,
          poids,
          pa,
          dateapplication,
          montant,
          observation,
          prixl
     FROM as_produits
          JOIN as_prixproduit ON as_produits.id = as_prixproduit.PRODUIT;



          CREATE OR REPLACE FORCE VIEW AS_PRODUITS_LIBELLEPRIX
(
   ID,
   NOM,
   DESIGNATION,
   PHOTO,
   TYPEPRODUIT,
   CALORIE,
   PA,
   POIDS,
   PU,
   prixL
)
AS
   SELECT prd.id,
          prd.nom,
          prd.designation,
          prd.photo,
          tpd.val,
          CASE
             WHEN prd.calorie = '1' THEN 'DISPONIBLE'
             ELSE 'INDISPONIBLE'
          END,
          NVL (rev.MONTANT, 0) AS pa,
          NVL (rev.Montant, 0) AS poids,
          trf.montant,
          prd.prixl
     FROM as_produits prd,
          as_typeproduit tpd,
          as_prixproduit trf,
          AS_PRODUITSPU rev
    WHERE     prd.typeproduit = tpd.id
          AND trf.produit(+) = prd.id
          AND prd.ID = rev.MERE(+);
   