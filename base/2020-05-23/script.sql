CREATE OR REPLACE  VIEW "DETAILSFFLIB"  AS 
  select
    df.id ,
    p.libelle as idingredient,
    df.QTE,
    df.PU,
    df.IDMERE,
    df.COMPTE
    from 
   detailsfacturefournisseur df 
    join as_ingredients p on df.idingredient = p.id ;


create or replace view detailsfflibcomplet as
select 
dt.*,
ff.DATY,
ff.IDFOURNISSEUR,
ff.IDTVA,
dt.pu*dt.qte as MONTANTTTC,
ff.REMARQUE,
ff.DATEEMISSION,
ff.DESIGNATION,
ff.IDDEVISE,
ff.NUMFACT,
ff.DATYECHEANCE,
ff.ETAT
from DETAILSFFLIB dt join
facturefournisseurlettre ff on ff.id=dt.idmere;

  insert into menudynamique values('MEN000054','Analyse details facture fournisseur','fa fa list','/phobo/pages/module.jsp?but=facturefournisseur/details-analyse-facture-fournisseur.jsp','5','3','ASM0000601');
 commit;