  CREATE OR REPLACE VIEW "INVENTAIREDETAILSLIBINVENTCPS" ("ID", "IDMERE", "IDINGREDIENT", "IDUSER", "QUANTITER", "EXPLICATION", "ETAT", "IDCATEGORIEINGREDIENT", "DATY", "IDING", "UNITE","COMPOSE") AS 
  select
            invdet.id,
           invDet.idmere,
           ing.LIBELLE as idingredient,
           invDet.idUser,
           invDet.quantiter,
            im.depot as explication,
           im.etat,
           catIng.VAL as idcategorieingredient,
           im.DATY,
           invDet.IDINGREDIENT,
           uni.val as unite,
           ing.compose
        from inventairedetails invDet
        left join AS_INGREDIENTS ing on ing.id=invDet.idingredient
        left join CATEGORIEINGREDIENT catIng on catIng.ID=invDet.IDCATEGORIEINGREDIENT
        join inventaireMere im on invDet.idmere=im.id
        join as_unite uni on ing.unite=uni.id
        where im.etat>=11;


create or replace view besoinetrecette as
select 
   
    rec.idingredients,
    sum(rec.quantite * be.quantite)as besoin

    from as_recette rec 
    left join besoin be on be.idproduit=rec.idproduits
    group by rec.idingredients ;

insert into menudynamique values ('ASM00099','liste besoin compose','fa fa-list','/phobo/pages/module.jsp?but=stock/besoin/liste-achat-besoin-compose.jsp',2,2,'ASM00014');
commit;

 