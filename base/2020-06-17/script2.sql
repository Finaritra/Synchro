CREATE OR REPLACE VIEW FACTUREFOURNISSEURMONTANT
(ID, DATY, IDFOURNISSEUR, IDTVA, MONTANTTTC, 
 REMARQUE, DATEEMISSION, DESIGNATION, IDDEVISE, NUMFACT, 
 RESP, DATYECHEANCE, ETAT, IDFACTUREFOURNISSEUR,entite)
AS 
select
    ff.id,
    ff.daty,
    ff.idfournisseur,
    ff.idtva,
    (select sum (pu*qte) from detailsfacturefournisseur where ff.id=idmere) as montantttc,
    ff.remarque,
    ff.dateemission,
    ff.designation,
    ff.iddevise,
    ff.numfact,
    ff.resp,
    ff.datyecheance,
    ff.etat,
    ff.idfacturefournisseur,
    ff.entite
from facturefournisseur ff
/

CREATE OR REPLACE VIEW FACTUREFOURNISSEURLETTREMTT
(ID, DATY, IDFOURNISSEUR, IDTVA, MONTANTTTC, 
 REMARQUE, DATEEMISSION, DESIGNATION, IDDEVISE, NUMFACT, 
 DATYECHEANCE,entite, ETAT)
AS 
SELECT ff.id, ff.daty, c.nom as idFournisseur, ff.idtva,cast (ff.montantttc as NUMBER) as montantttc,ff.remarque,
  ff.dateemission, ff.designation, ff.iddevise, ff.numfact, ff.datyecheance,p.val, CASE
   WHEN ff.etat=1 THEN 'CREER'
   WHEN ff.etat=11 THEN 'VALIDER'
   ELSE 'AUTRE'
  END
FROM facturefournisseurmontant ff, fournisseurProduits c, point p where c.id (+)= ff.IDFOURNISSEUR and ff.entite=p.id
/

CREATE OR REPLACE VIEW ETATFACTUREMONTANT
(ID, DATY, IDFOURNISSEUR, IDTVA, MONTANTTTC, 
 REMARQUE, DATEEMISSION, DESIGNATION, IDDEVISE, NUMFACT, 
 DATYECHEANCE, ETAT, MONTANTPAYE, RESTE,entite)
AS 
select f."ID",f."DATY",f."IDFOURNISSEUR",f."IDTVA",cast (f."MONTANTTTC" as NUMBER(18,2)) as MONTANTTTC,f."REMARQUE",f."DATEEMISSION",f."DESIGNATION",f."IDDEVISE",f."NUMFACT",f."DATYECHEANCE",f."ETAT" ,cast (nvl(op.montant,0) as NUMBER(18,2)) as montantPaye,cast ((f.montantTTC-nvl(op.montant,0)) as NUMBER(18,2)) as reste,f.entite
 from FACTUREFOURNISSEURLETTREMTT f,ORDONNERPAYEMENTGROUPEFACT op
where f.ID=op.DED_ID(+)
/

CREATE OR REPLACE VIEW ETATFACTUREVALIDERMONTANT AS select * from ETATFACTUREMONTANT where etat = 'VALIDER';

CREATE OR REPLACE VIEW ETATFACTURECREERMONTANT AS select * from ETATFACTUREMONTANT where etat = 'CREER';



