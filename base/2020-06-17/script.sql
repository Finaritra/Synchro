--nasina idcating
CREATE OR REPLACE VIEW "INVENTAIREDETAILSLIBINVENT"  AS 
            select
            invdet.id,
           invDet.idmere,
           ing.LIBELLE as idingredient,
           invDet.idUser,
           invDet.quantiter,
            im.depot as explication,
           im.etat,
           catIng.VAL as idcategorieingredient,
           im.DATY,
           invDet.IDINGREDIENT as IDING,
           uni.val as unite,
           cating.id as idcating
        from inventaireDetailsDecompose invDet
        left join AS_INGREDIENTS ing on ing.id=invDet.idingredient
        left join CATEGORIEINGREDIENT catIng on catIng.ID=invDet.IDCATEGORIEINGREDIENT
        join inventaireMere im on invDet.idmere=im.id
        join as_unite uni on ing.unite=uni.id
        where im.etat>=11 and ing.compose=0;

create or  replace view resteingredient as
select
        inC.id,
       inc.daty,
       inc.idingredient as idproduits,
       ing.LIBELLE as libelleproduit,
       inc.idingredients,
       ing2.LIBELLE as libelleingredient,
       (inc.quantite*inc.quantiter) as quantite

   from INVENTAIREDETAILSINGDECOMPOSE inC ,AS_INGREDIENTS ing ,AS_INGREDIENTS ing2 where ing2.id=inc.IDINGREDIENTS and  ing.id=inc.idingredient;
   commit;
        