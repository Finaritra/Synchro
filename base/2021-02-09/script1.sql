create view ETATFACTUREMONTANTCREE  as
    select *from  ETATFACTUREMONTANT where etat = 'CREER';

create view ETATFACTUREMONTANTVALIDE  as
    select *from  ETATFACTUREMONTANT where etat = 'VALIDER';