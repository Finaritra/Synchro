insert into menudynamique values('MEN0000902','liste utilisateur','fa fa-list','/phobo/pages/module.jsp?but=utilisateur/utilisateur-liste.jsp','6','2','ASM00009');
CREATE OR REPLACE VIEW ETATORDONNERPAYEMENT
(ID, DED_ID, IDLIGNE, DATY, MONTANT, 
 REMARQUE, ETAT, ETATL, TIERS, MONTANTPAYE, 
 RESTE)
AS 
SELECT o."ID",o."DED_ID",o."IDLIGNE",o."DATY",o."MONTANT",o."REMARQUE",o."ETAT",o."ETATLETTRE",o."TIERS", CAST (NVL (mvt.debit, 0) AS NUMBER (10, 2)) AS montantpaye,
          CAST ((o.MONTANT - NVL (mvt.debit, 0)) AS NUMBER (10, 2)
               ) AS reste
     FROM ordonnerpayementlettre o, mvtcaissegroupefopor mvt
    WHERE o.ID = mvt.idordre(+);
