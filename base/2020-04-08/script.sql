create or replace view as_ing_prod as
    select
        id,
           libelle as libelleproduit
           from AS_INGREDIENTS
union
select
    id,
       nom as libelleproduit
from AS_PRODUITS;

create or replace view AS_RECETTE_LIBCOMPLET as
select rec.*,
ing.LIBELLE as libelleingredient,
       prod.libelleproduit,
un.id as idunite,
un.val as valunite,
ing.pu
from as_recette rec
join as_ingredients ing on
ing.id=rec.idingredients
join as_unite un on
un.id=rec.unite
join as_ing_prod prod on prod.id = rec.IDPRODUITS;



CREATE OR REPLACE VIEW MVTCAISSELETTRERESTE
(ID, DATY, DESIGNATION, DEBIT, CREDIT, 
 IDDEVISE, IDMODE, IDCAISSE, REMARQUE, TIERS, 
 NUMPIECE, NUMCHEQUE, TYPEMVT, DATYVALEUR, IDORDRE, 
 ETAT, AGENCE, IDMVTOR, ETABLISSEMENT, RESTE)
AS 
select m."ID",m."DATY",m."DESIGNATION",m."DEBIT",m."CREDIT",m."IDDEVISE",
m."IDMODE",m."IDCAISSE",m."REMARQUE",m."TIERS",l.idlivreur, cmd.IDMERE as NUMCHEQUE,
m."TYPEMVT",m."DATYVALEUR",m."IDORDRE",m."ETAT",m."AGENCE",m."IDMVTOR",
m."ETABLISSEMENT", cast((m.credit-m.debit) as NUMBER(10,2)) as reste from MVTCAISSELETTRE m,
livraisonlib l, AS_DETAILSCOMMANDE cmd where m.idordre=l.IDCOMMNADEMERE(+) and m.idordre=cmd.id(+);

CREATE OR REPLACE VIEW MVTCAISSELETTREGROUPE
(ID, DATY, DESIGNATION, DEBIT, CREDIT, 
 IDDEVISE, IDMODE, IDCAISSE, REMARQUE, TIERS, 
 NUMPIECE, NUMCHEQUE, TYPEMVT, DATYVALEUR, IDORDRE, 
 ETAT, AGENCE, IDMVTOR, ETABLISSEMENT)
AS 
SELECT m.ID, m.daty, m.designation, m.debit, m.credit, m.iddevise,
          mp.val AS idmode, c.desccaisse AS idcaisse, m.remarque,
          m.tiers AS tiers, m.numpiece, m.numcheque, t.val AS typemvt,
          m.datyvaleur, m.idordre,m.etat,m.agence,m.IDMVTOR,m.ETABLISSEMENT
     FROM mvtcaisse m, typemvt t, caisse c, modepaiement mp
    WHERE m.typemvt = t.ID(+) AND m.idcaisse = c.idcaisse(+) AND m.idmode = mp.ID(+);




CREATE OR REPLACE VIEW AS_ETATSTOCK_LIBELLE1
(INGREDIENTS, UNITE, ENTREE, SORTIE, RESTE, 
 REPORT, DATY, POINT,idCategorieIngredient)
AS 
select '-', '-'
, 0, 0, 0, 0, '29/04/2020','-','-'
from dual;

CREATE OR REPLACE VIEW AS_RECETTE_LIBCOMPLET
(ID, IDPRODUITS, IDINGREDIENTS, QUANTITE, UNITE, 
 LIBELLEINGREDIENT, LIBELLEPRODUIT, IDUNITE, VALUNITE, PU)
AS 
select rec.*,
ing.LIBELLE as libelleingredient,
       prod.libelleproduit,
un.id as idunite,
un.val as valunite,
ing.pu
from as_recette rec
join as_ingredients ing on
ing.id=rec.idingredients
left join as_unite un on
un.id=ing.unite
join as_ing_prod prod on prod.id = rec.IDPRODUITS
/

