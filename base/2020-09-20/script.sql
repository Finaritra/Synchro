CREATE OR REPLACE FORCE VIEW MVTSTOCKDETLIBCOMPLETvalide
(
   ID,
   IDMVTSTOCK,
   INGREDIENTS,
   ENTREE,
   SORTIE,
   SOLDE,
   PU,
   IDMVT,
   DATY,
   DESIGNATION,
   TYPEMVT,
   OBSERVATION,
   DEPOT,
   TYPEBESOIN,
   ETAT,
   FOURNISSEUR,
   BENEFICIAIRE,
   NUMBC,
   NUMBS
)
AS
   SELECT det."ID",
          det."IDMVTSTOCK",
          det."INGREDIENTS",
          det."ENTREE",
          det."SORTIE",
          det."SOLDE",
          det."PU",
          det."IDMVT",
          mere.DATY,
          mere.DESIGNATION,
          mere.TYPEMVT,
          mere.OBSERVATION,
          mere.DEPOT,
          mere.TYPEBESOIN,
          mere.ETAT,
          mere.FOURNISSEUR,
          mere.BENEFICIAIRE,
          mere.NUMBC,
          mere.NUMBS
     FROM AS_MVTSTOCK_FILLE_LIB det, as_mvt_stockLibelle mere
    WHERE det.IDMVTSTOCK = mere.id(+) and etat>=11;
    
CREATE OR REPLACE FORCE VIEW MVTSTOCKDETLIBCOMPLETcree
(
   ID,
   IDMVTSTOCK,
   INGREDIENTS,
   ENTREE,
   SORTIE,
   SOLDE,
   PU,
   IDMVT,
   DATY,
   DESIGNATION,
   TYPEMVT,
   OBSERVATION,
   DEPOT,
   TYPEBESOIN,
   ETAT,
   FOURNISSEUR,
   BENEFICIAIRE,
   NUMBC,
   NUMBS
)
AS
   SELECT det."ID",
          det."IDMVTSTOCK",
          det."INGREDIENTS",
          det."ENTREE",
          det."SORTIE",
          det."SOLDE",
          det."PU",
          det."IDMVT",
          mere.DATY,
          mere.DESIGNATION,
          mere.TYPEMVT,
          mere.OBSERVATION,
          mere.DEPOT,
          mere.TYPEBESOIN,
          mere.ETAT,
          mere.FOURNISSEUR,
          mere.BENEFICIAIRE,
          mere.NUMBC,
          mere.NUMBS
     FROM AS_MVTSTOCK_FILLE_LIB det, as_mvt_stockLibelle mere
    WHERE det.IDMVTSTOCK = mere.id(+) and etat=1;