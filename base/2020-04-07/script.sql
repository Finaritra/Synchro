CREATE OR REPLACE VIEW AS_MVTSTOCK_FDTLIBUNITE
(ID, INGREDIENTS, ENTREE, SORTIE, DATY, 
 UNITE, IDINGREDIENTS, POINT)
AS 
select fille.id,grd.libelle as ingredients, fille.entree, fille.sortie, mere.daty, u.val as unite,fille.INGREDIENTS,mere.depot
  from as_mvt_stock_fille fille 
    join as_mvt_stock mere on fille.idmvtstock = mere.id 
    join as_ingredients grd on fille.ingredients = grd.id
    join as_unite u on grd.UNITE=u.id
    where mere.etat>=11;

CREATE OR REPLACE VIEW INVENTAIREDETAILSLIBINVENT
(ID, IDMERE, IDINGREDIENT, IDUSER, QUANTITER, 
 EXPLICATION, ETAT, IDCATEGORIEINGREDIENT, DATY,idIng,unite)
AS 
select
            invdet.id,
           invDet.idmere,
           ing.LIBELLE as idingredient,
           invDet.idUser,
           invDet.quantiter,
            im.depot as explication,
           invDet.etat,
           catIng.VAL as idcategorieingredient,
           im.DATY,
           invDet.IDINGREDIENT,
           uni.val as unite

        from inventairedetails invDet
        left join AS_INGREDIENTS ing on ing.id=invDet.idingredient
        left join CATEGORIEINGREDIENT catIng on catIng.ID=invDet.IDCATEGORIEINGREDIENT
        join inventaireMere im on invDet.idmere=im.id
        join as_unite uni on ing.unite=uni.id
        where im.etat>=11
/

ALTER TABLE AS_PRODUITS MODIFY(PHOTO VARCHAR2(200 BYTE));

delete from INVENTAIREDETAILS where id='INDO132';