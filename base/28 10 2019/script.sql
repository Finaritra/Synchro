create or replace view CommandeClientDetailsWithPoint as
select 
	details.*, client,point
from 
	as_detailscommande details
join as_commandeclient commande on commande.id=details.idmere;


truncate table point;
insert into point values('DIR000006','Ampandrana','Ampandrana');
insert into point values('DIR000008','Ankorondrano','Ankorondrano');


create table FACTUREFOURNISSEUR
(
    id VARCHAR2(50) not null
        constraint PK_FACTUREFOURNISSEUR
            primary key,
    DATY                 DATE,
    IDFOURNISSEUR        VARCHAR2(50),
    IDTVA                NUMBER(10, 2) default 0,
    MONTANTTTC           FLOAT,
    REMARQUE             VARCHAR2(100) default '-',
    DATEEMISSION         DATE,
    DESIGNATION          VARCHAR2(500),
    IDDEVISE             VARCHAR2(5),
    NUMFACT              VARCHAR2(100),
    RESP                 VARCHAR2(50)  default '-',
    DATYECHEANCE         DATE,
    etat int
);
drop sequence seqFACTUREFOURNISSEUR;
CREATE SEQUENCE seqFACTUREFOURNISSEUR
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  NOCACHE
  NOORDER
  NOCYCLE;

create or replace
  FUNCTION getseqFACTUREFOURNISSEUR
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqFACTUREFOURNISSEUR.nextval INTO retour FROM dual;
  return retour;
END;