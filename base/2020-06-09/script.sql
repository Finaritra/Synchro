  create or replace view besoinetingredient as 
  select comp.quantite*comp.qteav*b.QUANTITE as quantite,p.NOM as libelleproduit ,p.id as idproduits,ing.nom as libelleingredient ,ing.id as idingredients  from 
  as_recettebesoincompose comp, produitEtIngredient p,produitEtIngredient ing, besoin b
  where comp.MERE=p.ID and comp.IDINGREDIENTS=ing.ID 
  and b.IDPRODUIT=p.id;


create or replace view as_ingredients_ff_lib as
select
ff.*,
fp.nom
from as_ingredients_ff ff left join
fournisseurproduits fp on
ff.idfournisseur=fp.id;

--napina libellefournisseur
CREATE OR REPLACE VIEW "AS_ETATSTOCKVIDE" AS 
select 
'-' as ingredients,
'-' as unite,
CAST(0 as NUMBER(10,2)) as entree,
CAST(0 as NUMBER(10,2)) as sortie,
CAST(0 as NUMBER(10,2)) as reste,
CAST(0 as NUMBER(10,2)) as report,
'' as daty,
'' as idcategorieingredient,
'-' as point,
CAST(0 as NUMBER(10,2)) as besoin,
'-' as idfournisseur,
CAST(0 as NUMBER(10,2)) as pu,
'-' as idingredient,
'-' as fournisseur
 from dual;