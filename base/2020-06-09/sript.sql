  

CREATE OR REPLACE VIEW INVENTAIREDETAILSDECOMPOSE
(ID, IDMERE, IDINGREDIENT, IDUSER, QUANTITER, 
 EXPLICATION, ETAT, IDCATEGORIEINGREDIENT, DATY)
AS 
select '-' as id,nvl(max(inv.idmere),'INV2') as idmere,ing.id as idingredient,inv.IDUSER,
 nvl(sum((inv.QUANTITER*inv.QUANTITE)),0) as quantiter, inv.EXPLICATION,nvl(inv.etat,11),
 ing.CATEGORIEINGREDIENT as IDCATEGORIEINGREDIENT ,nvl(inv.DATY,'01/01/2020') as daty from inventaireDetailsIngDecompose inv, as_ingredients ing
 where ing.id=inv.IDINGREDIENTS(+)
 group by ing.id,inv.IDUSER,
  inv.EXPLICATION,inv.etat,
  ing.CATEGORIEINGREDIENT,inv.DATY
/