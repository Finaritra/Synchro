CREATE OR REPLACE VIEW DETAILSFFLIB (ID, IDINGREDIENT, QTE, PU, IDMERE, COMPTE, IDTYPEFACTURELIB,IDING) AS 
  SELECT df.id,
          p.libelle AS idingredient,
          df.QTE,
          df.PU,
          df.IDMERE,
          df.COMPTE,
          f.val as idtypefacturelib,
          p.ID AS iding
     FROM detailsfacturefournisseur df
          JOIN as_ingredients p ON df.idingredient = p.id
          left join typefacture f on DF.IDTYPEFACTURE=f.id;
          
		  
  CREATE OR REPLACE VIEW FACTUREFOURNISSEURLETTRE (ID, DATY, IDFOURNISSEUR, IDTVA, MONTANTTTC, REMARQUE, DATEEMISSION, DESIGNATION, IDDEVISE, NUMFACT, DATYECHEANCE, ETAT, IDFRN) AS 
  SELECT ff.id, ff.daty, c.nom as idFournisseur, ff.idtva, ff.montantttc,ff.remarque,
  ff.dateemission, ff.designation, ff.iddevise, ff.numfact, ff.datyecheance, CASE
   WHEN ff.etat=1 THEN 'CREER'
   WHEN ff.etat=11 THEN 'VALIDER'
   ELSE 'AUTRE'
  END, c.ID AS idfrn
FROM facturefournisseurmontant ff, fournisseurProduits c where c.id (+)= ff.IDFOURNISSEUR;

CREATE OR REPLACE VIEW DETAILSFFLIBCOMPLET (ID, IDINGREDIENT, QTE, PU, IDMERE, COMPTE, DATY, IDFOURNISSEUR, IDTVA, MONTANTTTC, REMARQUE, DATEEMISSION, DESIGNATION, IDDEVISE, NUMFACT, DATYECHEANCE, ETAT, IDTYPEFACTURELIB, LIVRE, RESTE, IDFRN,IDING) AS 
  SELECT dt.ID,
          dt.IDINGREDIENT,
          dt.QTE,
          dt.PU,
          dt.IDMERE,
          dt.COMPTE,
          ff.DATY,
          ff.IDFOURNISSEUR,
          ff.IDTVA,
          dt.pu * dt.qte AS MONTANTTTC,
          ff.REMARQUE,
          ff.DATEEMISSION,
          ff.DESIGNATION,
          ff.IDDEVISE,
          ff.NUMFACT,
          ff.DATYECHEANCE,
          ff.ETAT,
          dt.idtypefacturelib,
          bl.quantite as livre,
          dt.qte-bl.quantite as reste,
          ff.IDFRN,
          dt.IDING 
     FROM DETAILSFFLIB dt, facturefournisseurlettre ff,AS_BONDELIVRASION_FILLE_GRP bl
          where ff.id = dt.idmere
          and bl.IDDETAILSFACTUREFOURNISSEUR(+)=dt.id  
    and FF.ETAT = 'VALIDER';