
CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_DTLS_TYPEP_CLO_PRT AS 
select vue_cmd_client_details_typeP.ID,vue_cmd_client_details_typeP.NOMTABLE, vue_cmd_client_details_typeP.TYPEPRODUIT, vue_cmd_client_details_typeP.DATECOMMANDE, vue_cmd_client_details_typeP.HEURELIV, vue_cmd_client_details_typeP.PRODUIT, vue_cmd_client_details_typeP.QUANTITE, vue_cmd_client_details_typeP.PU, vue_cmd_client_details_typeP.MONTANT, vue_cmd_client_details_typeP.ETAT, vue_cmd_client_details_typeP.ACCO_SAUCE, as_detailscommande.prioriter
  from vue_cmd_client_details_typeP join as_detailscommande on as_detailscommande.id = vue_cmd_client_details_typeP.id where vue_cmd_client_details_typeP.etat = 9;


 
CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_DETS_TYPP_PRIO AS 
  select
      det.id ,
      tab.val as nomtable,
      tp.val as typeproduit,cmd.datecommande,cmd.heureliv,
      prod.nom as produit,
      det.quantite,det.pu,
      (det.quantite*det.pu) as montant,
      det.etat,
      accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
      det.prioriter
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join tableclientvue2 tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;




CREATE OR REPLACE FORCE VIEW VUE_CMD_DTLS_TYPEP_CLOT_PRIO AS 
select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE", "PRIORITER"
from VUE_CMD_CLT_DETS_TYPP_PRIO where etat = 9;

insert into menudynamique values('ASM0000915','Tous','fa fa-liste','/phobo/pages/module.jsp?but=commande/as-commande-liste-etat.jsp'||'&'||'table=',1,3,'ASM0000907');


----------------------------------------------------------------------
 

  	CREATE OR REPLACE FORCE VIEW "AS_PAIEMENT_LIBELLE_TABLE" ("ID", "DATY", "MONTANT", "MODEPAIEMENT", "IDOBJET", "OBSERVATION", "TVA", "CLIENT", "NUMPIECE", "CAISSE") AS 
	select
	    pmt.ID, pmt.DATY, pmt.MONTANT, mp.VAL, pmt.IDOBJET, pmt.OBSERVATION, pmt.TVA, cli.val as nom, pmt.NUMPIECE, caisse.desccaisse
	from
	    as_paiement pmt,
	    modepaiement mp,
	    caisse,
	    tableclientvue2 cli
	where
	    pmt.MODEPAIEMENT = mp.ID AND caisse.idcaisse(+)= pmt.caisse and pmt.client = cli.id;


    CREATE OR REPLACE FORCE VIEW "VUE_CMD_DTLS_TYPEP_LIVRE_PAYER" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  	select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE" from vue_cmd_client_details_typeP where etat > 0 and etat <= 20;


  	CREATE OR REPLACE FORCE VIEW "AS_DETAILS_COMMANDE_ETAT" ("ID", "IDMERE", "PRODUIT", "QUANTITE", "PU", "REMISE", "OBSERVATION", "IDPRODUIT", "ETAT") AS 
	select
	    dt.ID, dt.IDMERE, pr.NOM, dt.QUANTITE, dt.PU, dt.REMISE, dt.OBSERVATION, dt.PRODUIT, dt.etat
	from
	    AS_DETAILSCOMMANDE dt,
	    AS_PRODUITS pr
	where
	    dt.produit = pr.id ;


--------------------------------------------------------------------------------------------- 30/09/2019

CREATE OR REPLACE FORCE VIEW VUE_CMD_CLT_DETS_TYPP_PRIO AS 
  select
      det.id ,
      tab.val as nomtable,
      tp.val as typeproduit,cmd.datecommande,cmd.heureliv,
      prod.nom as produit,
      det.quantite,det.pu,
      (det.quantite*det.pu) as montant,
      det.etat,
      accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce,
      det.prioriter,
      det.observation,
      tp.id as idtype
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join tableclientvue2 tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande desc,cmd.heureliv desc, det.prioriter asc, det.etat desc;


  	CREATE OR REPLACE FORCE VIEW "VUE_CMD_DTLS_TYPEP_CLOT_PRIO" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE", "PRIORITER", "OBSERVATION", "IDTYPE") AS 
  	select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE", "PRIORITER", "OBSERVATION", "IDTYPE"
	from VUE_CMD_CLT_DETS_TYPP_PRIO where etat = 9 and idtype != 'TPD00001';

	insert into menudynamique values('ASM0000917','Boisson','fa fa-liste','/phobo/pages/module.jsp?but=commande/as-commande-liste-boisson.jsp'||'&'||'table=',1,3,'ASM0000907');

	CREATE OR REPLACE FORCE VIEW "VUE_CMD_DTLS_CLOT_PRIO_BOIS" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE", "PRIORITER", "OBSERVATION", "IDTYPE") AS 
  	select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE", "PRIORITER", "OBSERVATION", "IDTYPE"
	from VUE_CMD_CLT_DETS_TYPP_PRIO where etat = 9 and idtype = 'TPD00001';


	insert into menudynamique values('ASM0000918','Cloture update','fa fa-liste','/phobo/pages/module.jsp?but=commande/as-commande-liste-cloture.jsp'||'&'||'table=',1,3,'ASM0000907');


	CREATE OR REPLACE FORCE VIEW "VUE_CMD_CLT_DTLS_TYPEP_VALIDE" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "PRODUIT", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  	select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE" from vue_cmd_client_details_typeP where etat = 11 order by datecommande asc, heureliv asc;