insert into MENUDYNAMIQUE (ID,LIBELLE,ICONE,HREF,RANG,NIVEAU,ID_PERE) values ('ASM000153','Analyse','fa fa-list','/phobo/pages/module.jsp?but=stock/gestionDeFabrication/fabrication-analyse.jsp','3','2','ASM00015');

CREATE OR REPLACE  VIEW  FABRICATIONCATLIB as
select 
	f.id , f.daty , rl.libelleproduit as produit ,
	rl.libelleingredient as ingredient , f.remarque ,
    c.val as categorie,
	cast( nvl(   f.qte * rl.quantite,  0 ) as number(10,2) ) as qte ,
    f.depot ,f.etat  
	from fabrication f 
join AS_RECETTE_LIBCOMPLET rl on rl.idproduits=f.ingredient 
left join AS_INGREDIENTS ing on ing.ID=rl.IDINGREDIENTS 
join categorieingredient c on  ing.categorieingredient= c.id;
