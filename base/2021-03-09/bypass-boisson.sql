--Bypass
--Star GM
update as_prixproduit set montant = 5000 where produit in ('PRDBS0014', 'PRDBS0008', 'PRDBS0007', 'PRDBS0006', 'PRDBS0011', 'PRDBS0013', 'PRDBS0012', 'PRDBS0015', 'PRDBS0042', 'PRDBS0010', 'PRDBS0040', 'PRDBS0026', 'ASP000147' );

--Cristalline
update as_prixproduit set montant = 3500 where produit in ('PRDBS0005');

--Fresh
update as_prixproduit set montant = 4500 where produit in ('PRDBS0028');

--Heineken bouteille
update as_prixproduit set montant = 9000 where produit in ('PRDBS0029');

-- Heineken canette
update as_prixproduit set montant = 10000 where produit in ('PRDBS0030');

-- Gold Blanche
update as_prixproduit set montant = 7500 where produit in ('PRDBS0041');

--eau vive 150cl
update as_prixproduit set montant = 4000 where produit in ('PRDBS0003');

--Gold - THB 33
update as_prixproduit set montant = 3500 where produit in ('PRDBS0027', 'APS000298');
commit;

