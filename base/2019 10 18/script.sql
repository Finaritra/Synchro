alter table AS_COMMANDECLIENT add primary key (id);
alter table AS_DETAILSCOMMANDE add primary key (id);

update AS_DETAILSCOMMANDE set IDMERE = null where id in  (
select id
from AS_DETAILSCOMMANDE where IDMERE not in (select id from AS_COMMANDECLIENT));

ALTER TABLE AS_DETAILSCOMMANDE
    ADD CONSTRAINT fk_AS_DETAILS_COMMANDE_mere
        FOREIGN KEY (IDMERE)
            REFERENCES AS_COMMANDECLIENT (id);
	