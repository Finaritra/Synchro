create or replace view  as_comandeDetIngRevient as
select rev.IDINGREDIENTS ,rev.MONTANT as revient,rev.QUANTITE,det.IDMERE,det.id,ing.LIBELLE from 
as_detailscommande det,as_recPrdCompFF rev, as_ingredients ing
 where det.etat>0 and det.PRODUIT=rev.MERE(+) and ing.id=rev.idingredients;

create or replace view as_cmdDetIng as
select det.*,ingrev.REVIENT,ingrev.quantite as qteIng,ingrev.LIBELLE || ' (' ||  ing.UNITE || ')' as libelle, ing.CATEGORIEINGREDIENT
from as_comandeDetIngRevient ingrev,VUE_CMD_CLIENT_DETAILS_REST det,as_ingredients_lib ing
where det.ID=ingrev.id and ingrev.IDINGREDIENTS=ing.id;

create or replace view as_cmdDetIngNonCloture as select * from as_cmdDetIng where etat<9;

create or replace view as_cmdDetIngCloture as select * from as_cmdDetIng where etat>=9;


	INSERT INTO MENUDYNAMIQUE ( ID, LIBELLE, ICONE, HREF, RANG, NIVEAU,
ID_PERE ) VALUES ( 
'MEN000AREV007', 'Analyse Revient', 'fa fa-list', '/phobo/pages/module.jsp?but=commande/as-commande-analyseRev.jsp'
, 11, 2, 'ASM00001'); 
COMMIT;
