create table Typefacture
(
    Id varchar2(100)constraint type_facture_pk primary key ,
        Val varchar2(500),
        Desce varchar2(500)
);

alter table DetailsFactureFournisseur add idtypefacture varchar2(100) null constraint DetailsFactureFourn_type_fk references TYPEFACTURE;

 INSERT INTO TYPEFACTURE (ID, VAL, DESCE) VALUES ('TYF0001', 'Facture', 'Facture');

INSERT INTO TYPEFACTURE (ID, VAL, DESCE) VALUES ('TYF0002', 'Ingredients', 'Ingredients');
commit;