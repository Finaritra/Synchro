create or replace view facturefournisseurmontant
as select 
    ff.id,
    ff.daty,
    ff.idfournisseur,
    ff.idtva,
    (select sum (pu*qte) from detailsfacturefournisseur where ff.id=idmere) as montantttc,
    ff.remarque,
    ff.dateemission,
    ff.designation,
    ff.iddevise,
    ff.numfact,
    ff.resp,
    ff.datyecheance,
    ff.etat,
    ff.idfacturefournisseur
from facturefournisseur ff;

 --ilaina refa msupprimer
  CREATE TABLE AS_RESTRICTION 
   (	ID VARCHAR2(100), 
	IDACTION VARCHAR2(100), 
	TABLENAME VARCHAR2(100), 
	AUTORISATION VARCHAR2(100), 
	DESCRIPTION VARCHAR2(100), 
	IDDIRECTION VARCHAR2(100), 
	 PRIMARY KEY (ID)
	 ) ;
 