create table source(
    id varchar2(100) constraint source_pk primary key ,
    val varchar2(100),
    desce varchar2(100)
);

CREATE SEQUENCE seqsource
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1061
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getseqsource
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqsource.nextval INTO retour FROM dual;
  return retour;
END;
select *
from AS_RESERVATION;

create or replace view Reservation_table  as
select
    reserv.id,
    reserv.SOURCE,
    reserv.NBPERSONNE,
    reserv.DATESAISIE,
    reserv.HEURESAISIE,
    reserv.DATERESERVATION,
    reserv.HEUREDEBUTRESERVATION,
    reserv.HEUREFINRESERVATION,
    clt.nom ||' '||clt.PRENOM as idclient,
    clt.TELEPHONE,
    reserv.ETAT,
    tab.VAL as tablenom



from AS_RESERVATION reserv
left join RESA_TABLE RT on reserv.ID = RT.IDRESERVATION
left join AS_CLIENT clt on clt.ID=reserv.IDCLIENT
left join TABLECLIENTVUE2 tab on tab.id=rt.IDTABLE ;

create or replace view ReservationLibcree as
select
    reserv.*
from Reservation_table reserv
where reserv.ETAT=1
order by reserv.id asc ;

create or replace view ReservationLibvalide as
select
    reserv.*
from Reservation_table reserv
where reserv.ETAT=11
order by reserv.id asc ;
create or replace view ReservationLibeffectue as
select
    reserv.*

from Reservation_table reserv
where reserv.ETAT=12
order by reserv.id asc ;


create or replace view RESERVATIONLIBTOUS as
select
    reserv.id,
    reserv.SOURCE,
    reserv.NBPERSONNE,
    reserv.DATESAISIE,
    reserv.HEURESAISIE,
    reserv.DATERESERVATION,
    reserv.HEUREDEBUTRESERVATION,
    reserv.HEUREFINRESERVATION,
    clt.nom ||' '||clt.PRENOM as idclient,
    clt.TELEPHONE,
    reserv.ETAT,
    tab.VAL as tablenom
from AS_RESERVATION reserv
left join RESA_TABLE RT on reserv.ID = RT.IDRESERVATION
left join AS_CLIENT clt on clt.ID=reserv.IDCLIENT
left join TABLECLIENTVUE2 tab on tab.id=rt.IDTABLE;


ALTER TABLE resa_table drop CONSTRAINT fk_table;

update as_client set id='pk_simonette' where id is null;

ALTER TABLE AS_CLIENT ADD 
 PRIMARY KEY (ID)
 ENABLE
 VALIDATE;

ALTER TABLE RESA_TABLE ADD 
 FOREIGN KEY (IDTABLE)
 REFERENCES AS_CLIENT (ID) ENABLE
 VALIDATE;