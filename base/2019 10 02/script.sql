CREATE OR REPLACE FORCE VIEW "ALLOSAKAFO"."VUE_CMD_CLIENT_DETAILS_TYPEP" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "PRODUIT", "REMARQUE", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  select
      det.id ,
      tab.val as nomtable,
      tp.val as typeproduit,cmd.datecommande,cmd.heureliv,
      prod.nom as produit,
	  det.observation,
      det.quantite,det.pu,
      (det.quantite*det.pu) as montant,
      det.etat,
      accsauce.IDSAUCE || ' ' || accsauce.IDACCOMPAGNEMENT as acco_sauce
    from as_commandeclient cmd
    join as_detailscommande det on det.idmere = cmd.id
    join as_produits prod on prod.id=det.produit
    join as_typeproduit tp on tp.id=prod.typeproduit
    join tableclientvue2 tab on tab.id=cmd.client
    left join AS_ACCOMPAGNEMENT_SAUCE_LIB accsauce on det.IDACCOMPAGNEMENTSAUCE = accsauce.id
    order by cmd.datecommande desc,cmd.heureliv desc,det.etat desc;
	
CREATE OR REPLACE FORCE VIEW "ALLOSAKAFO"."VUE_CMD_CLT_DTLS_TYPEP_VALIDE" ("ID", "NOMTABLE", "TYPEPRODUIT", "DATECOMMANDE", "HEURELIV", "PRODUIT", "REMARQUE", "QUANTITE", "PU", "MONTANT", "ETAT", "ACCO_SAUCE") AS 
  select "ID","NOMTABLE","TYPEPRODUIT","DATECOMMANDE","HEURELIV","PRODUIT","REMARQUE","QUANTITE","PU","MONTANT","ETAT", "ACCO_SAUCE" from vue_cmd_client_details_typeP where etat = 11 order by datecommande asc, heureliv asc;
  
 UPDATE as_client set id='CAS001825' where id is null;
 delete from as_prixproduit where produit is null
 
 ALTER TABLE as_client ADD CONSTRAINT pk_asclient PRIMARY KEY (id);
 ALTER TABLE as_typecommande ADD CONSTRAINT pk_astypecommande PRIMARY KEY (id);
 ALTER TABLE as_detailscommande ADD CONSTRAINT pk_detailscommande PRIMARY KEY (id);
 ALTER TABLE as_commandeclient ADD CONSTRAINT pk_commandeclient PRIMARY KEY (id);
 ALTER TABLE as_produits ADD CONSTRAINT pk_asproduits PRIMARY KEY (id);
 
 ALTER TABLE as_commandeclient ADD CONSTRAINT fk_commandeclient FOREIGN KEY (client) REFERENCES as_client(id);
 ALTER TABLE as_commandeclient ADD CONSTRAINT fk_typecommandeclient FOREIGN KEY (typecommande) REFERENCES as_typecommande(id);
 ALTER TABLE as_detailscommande ADD CONSTRAINT fk_idmere FOREIGN KEY (idmere) REFERENCES as_commandeclient(id);
 ALTER TABLE as_detailscommande ADD CONSTRAINT fk_produit FOREIGN KEY (produit) REFERENCES as_produits(id);
 ALTER TABLE as_paiement ADD CONSTRAINT fk_modepaiement FOREIGN KEY (modepaiement) REFERENCES modepaiement(id);
 ALTER TABLE as_paiement ADD CONSTRAINT fk_clientpaiement FOREIGN KEY (client) REFERENCES as_client(id);
 ALTER TABLE as_paiement ADD CONSTRAINT fk_caissepaiement FOREIGN KEY (caisse) REFERENCES caisse(idcaisse);
 ALTER TABLE as_prixproduit ADD CONSTRAINT fk_prixproduit FOREIGN KEY (produit) REFERENCES as_produits(id);
 ALTER TABLE as_produits ADD CONSTRAINT fk_typeproduit FOREIGN KEY (typeproduit) REFERENCES as_typeproduit(id);