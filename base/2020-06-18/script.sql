insert into menudynamique values('ASMINV0013','liste inventaire details','fa fa-list','/phobo/pages/module.jsp?but=stock/inventaire/inventaire-details-liste.jsp','4','2','ASINV0803');

update menudynamique set id_pere='-' where id='ASM00006021';
commit; 
CREATE OR REPLACE VIEW INVENTAIREDETAILSLIBCOMPLET  AS
  select
            invDet.id,
            invDet.idmere,
            invDet.idingredient,
            invDet.iduser,
            invDet.quantiter,
            invDet.explication,
            invDet.etat as etatfille,
            invDet.idcategorieingredient,
            invDet.daty as datyfille,
            invDet.qtetheorique,
            ing.LIBELLE as libelleingredient,
            ing.SEUIL,
            ing.UNITE,
            ing.QUANTITEPARPACK,
            ing.PU,
            ing.ACTIF,
            ing.PHOTO,
            ing.CALORIE,
            ing.DURRE,
            ing.COMPOSE,
            catIng.DESCE as descecategorieingredient,
            catIng.VAL as categorieingredient,
            im.DATY ,
            im.DEPOT ,
            im.TYPE,
            im.OBSERVATION,
            im.ETAT,
            p.VAL as libelledepot
            from inventairedetails invDet
        left join AS_INGREDIENTS ing on ing.id=invDet.idingredient
        left join CATEGORIEINGREDIENT catIng on catIng.ID=invDet.IDCATEGORIEINGREDIENT
        join inventaireMere im on invDet.idmere=im.id
         left join point p on im.depot=p.id;


 CREATE OR REPLACE  VIEW "INVENTAIREDETAILSLIBINVENTCPS"  as select
            invdet.id,
           invDet.idmere,
           ing.LIBELLE as idingredient,
           invDet.idUser,
           invDet.quantiter,
            im.depot as explication,
           im.etat,
           catIng.VAL as idcategorieingredient,
           im.DATY,
           invDet.IDINGREDIENT as IDING,
           uni.val as unite,
           ing.compose,
           catIng.ID as IDCATING
        from inventairedetails invDet
        left join AS_INGREDIENTS ing on ing.id=invDet.idingredient
        left join CATEGORIEINGREDIENT catIng on catIng.ID=invDet.IDCATEGORIEINGREDIENT
        join inventaireMere im on invDet.idmere=im.id
        join as_unite uni on ing.unite=uni.id
        where im.etat>=11;