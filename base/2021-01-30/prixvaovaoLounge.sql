--pho sans port
update as_produits set prixl = 16000 where id like 'ASP000141';

--pho
update as_produits set prixl = 16000 where id like 'PRDTMP000001';

--pho avec tsatsiou
update as_produits set prixl = 17000 where id like 'APS000294';

--pho sans tsatsiou
update as_produits set prixl = 14000 where id like 'APS000239';

--tsock special
update as_produits set prixl = 14000 where id like 'PRDTMP000002';

--soupe van tan mine garnie 
update as_produits set prixl = 18000 where id like 'ASP000161';

--soupe van tan garnie 
update as_produits set prixl = 17000 where id like 'ASP000165';

--soupe delice pate jaune 
update as_produits set prixl = 8000 where id like 'ASP000164';

--soupe tongotr'omby
update as_produits set prixl = 8500 where id like 'APS000297';
 
--poisson farci sauce aigre douce 
update as_produits set prixl = 16000 where id like 'PRDTMP000204';

--poisson farci sauce champignon 
update as_produits set prixl = 16000 where id like 'PRDTMP000203';

--poisson farci sauce huitre 
update as_produits set prixl = 16000 where id like 'PRDTMP000003';

--boullette de poisson sauce champinion noir 
update as_produits set prixl = 13000 where id like 'PRDTMP000201';

--boullette de poisson sauce aigre douce 
update as_produits set prixl = 13000 where id like 'PRDTMP000202';

--boullette de poisson sauce huitre 
update as_produits set prixl = 13000 where id like 'PRDTMP000006';

--misao poulet 
update as_produits set prixl = 11000 where id like 'PRDTMP000019';

--misao special tsasiou 
update as_produits set prixl = 13000 where id like 'PRDTMP000020';

--misao special crevette 
update as_produits set prixl = 13000 where id like 'APS000328';
update as_produits set prixl = 13000 where id like 'APS000311';

--misao Tsatsiou et crevette 
update as_produits set prixl = 16000 where id like 'PRDTMP000100';

--sphagetti bolognaise 
update as_produits set prixl = 12000 where id like 'PRDTMP000022';

--sphagetti beurre 
update as_produits set prixl = 6500 where id like 'ASP000166';

--sphagetti carbonara 
update as_produits set prixl = 14000 where id like 'PRDTMP000023';

--vary mena  
update as_produits set prixl = 1500 where id like 'PRDTMP000031';

--vary fotsy  
update as_produits set prixl = 1500 where id like 'PRDTMP000030';

--hena homby ritra
update as_produits set prixl = 10000 where id like 'PRDTMP000010';

--henakisoa sy ravitoto
update as_produits set prixl = 10000 where id like 'PRDTMP000011';

--henakisoa sy tripe
update as_produits set prixl = 10000 where id like 'PRDTMP000013';

--langue au poivre vert
update as_produits set prixl = 10000 where id like 'PRDTMP000014';

--henakisoa sy amalona
update as_produits set prixl = 12000 where id like 'PRDTMP000015';

--rougail
update as_produits set prixl = 1500 where id like 'PRDTMP000034';

--salade
update as_produits set prixl = 1500 where id like 'PRDTMP000027';

--aile de poulet sauce caramel
update as_produits set prixl = 10000 where id like 'PRDTMP000016';

--poulet oriental 
update as_produits set prixl = 12000 where id like 'PRDTMP000017';

--poulet gasy demi
update as_produits set prixl = 17000 where id like 'ASP000139';

--poulet gasy entier
update as_produits set prixl = 29000 where id like 'PRDTMP000018';

--steack
update as_produits set prixl = 14000 where id like 'PRDTMP000024';

--steack du chef
update as_produits set prixl = 16000 where id like 'ASP000203';

--cote pannée
update as_produits set prixl = 15000 where id like 'PRDTMP000026';

--brohette de zebu
update as_produits set prixl = 13000 where id like 'PRDTMP000025';

--saucisse sambaina
update as_produits set prixl = 9000 where id like 'APS000242';

--saucisse fromage
update as_produits set prixl = 12000 where id like 'ASP000247';

--saucisse tsatsiou
update as_produits set prixl = 13000 where id like 'APS000248';

--saucisse crevette
update as_produits set prixl = 11000 where id like 'APS000241';

--assiette de tsasiou
update as_produits set prixl = 13000 where id like 'PRDTMP000041';

--salade sofikisoa 
update as_produits set prixl = 9000 where id like 'APS000272';

--legumes sautes
update as_produits set prixl = 6000 where id like 'PRDTMP000028';

--oignons panes
update as_produits set prixl = 7000 where id like 'PRDTMP000035';

--nem porc
update as_produits set prixl = 10000 where id like 'PRDTMP000037';

--nem fruit de mer
update as_produits set prixl = 10000 where id like 'PRDTMP000036';

--rouleaux de printemps
update as_produits set prixl = 12000 where id like 'PRDTMP000038';

--van tan frite
update as_produits set prixl = 8000 where id like 'ASP000207';

--cuisse de nymphe
update as_produits set prixl = 27000 where id like 'PRDTMP000040';

--saucisse chinoise
update as_produits set prixl = 13000 where id like 'ASP000140';

--assiette de frite
update as_produits set prixl = 7000 where id like 'PRDTMP000029';

--salade exotique
update as_produits set prixl = 10000 where id like 'ASP000146';

--riz contonais
update as_produits set prixl = 13000 where id like 'PRDTMP000032';

--bol reverse
update as_produits set prixl = 15000 where id like 'PRDTMP000033';

--crepe sale(poisson)
update as_produits set prixl = 12000 where id like 'ASP000162';

--crepe sale(tsatsiou et poulet)
update as_produits set prixl = 12000 where id like 'ASP000163';





































