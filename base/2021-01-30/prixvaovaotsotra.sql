--pho sans port
update as_prixproduit set MONTANT = 15000 where produit like 'ASP000141';

--pho
update as_prixproduit set MONTANT = 15000 where produit like 'PRDTMP000001';

--pho avec tsatsiou
update as_prixproduit set MONTANT = 16000 where produit like 'APS000294';

--pho sans tsatsiou
update as_prixproduit set MONTANT = 13000 where produit like 'APS000239';

--tsock special
update as_prixproduit set MONTANT = 13000 where produit like 'PRDTMP000002';

--soupe van tan mine garnie 
update as_prixproduit set MONTANT = 16000 where produit like 'ASP000161';

--soupe van tan garnie 
update as_prixproduit set MONTANT = 15000 where produit like 'ASP000165';

--soupe delice pate jaune 
update as_prixproduit set MONTANT = 7000 where produit like 'ASP000164';

--soupe tongotr'omby
update as_prixproduit set MONTANT = 7500 where produit like 'APS000297';
 
--poisson farci sauce aigre douce 
update as_prixproduit set MONTANT = 15000 where produit like 'PRDTMP000204';

--poisson farci sauce champignon 
update as_prixproduit set MONTANT = 15000 where produit like 'PRDTMP000203';

--poisson farci sauce huitre 
update as_prixproduit set MONTANT = 15000 where produit like 'PRDTMP000003';

--boullette de poisson sauce champinion noir 
update as_prixproduit set MONTANT = 12000 where produit like 'PRDTMP000201';

--boullette de poisson sauce aigre douce 
update as_prixproduit set MONTANT = 12000 where produit like 'PRDTMP000202';

--boullette de poisson sauce huitre 
update as_prixproduit set MONTANT = 12000 where produit like 'PRDTMP000006';

--misao poulet 
update as_prixproduit set MONTANT = 10000 where produit like 'PRDTMP000019';

--misao special tsasiou 
update as_prixproduit set MONTANT = 12500 where produit like 'PRDTMP000020';
update as_prixproduit set MONTANT = 12500 where produit like 'APS000311';

--misao special crevette 
update as_prixproduit set MONTANT = 12500 where produit like 'APS000328';

--misao Tsatsiou et crevette 
update as_prixproduit set MONTANT = 15000 where produit like 'PRDTMP000100';

--sphagetti bolognaise 
update as_prixproduit set MONTANT = 11000 where produit like 'PRDTMP000022';

--sphagetti beurre 
update as_prixproduit set MONTANT = 6000 where produit like 'ASP000166';

--sphagetti carbonara 
update as_prixproduit set MONTANT = 13000 where produit like 'PRDTMP000023';

--vary mena  
update as_prixproduit set MONTANT = 1000 where produit like 'PRDTMP000031';

--vary fotsy  
update as_prixproduit set MONTANT = 1000 where produit like 'PRDTMP000030';

--hena homby ritra
update as_prixproduit set MONTANT = 9000 where produit like 'PRDTMP000010';

--henakisoa sy ravitoto
update as_prixproduit set MONTANT = 9000 where produit like 'PRDTMP000011';

--henakisoa sy tripe
update as_prixproduit set MONTANT = 9000 where produit like 'PRDTMP000013';

--langue au poivre vert
update as_prixproduit set MONTANT = 9000 where produit like 'PRDTMP000014';

--henakisoa sy amalona
update as_prixproduit set MONTANT = 10000 where produit like 'PRDTMP000015';

--rougail
update as_prixproduit set MONTANT = 1000 where produit like 'PRDTMP000034';

--salade
update as_prixproduit set MONTANT = 1000 where produit like 'PRDTMP000027';

--aile de poulet sauce caramel
update as_prixproduit set MONTANT = 9000 where produit like 'PRDTMP000016';

--poulet oriental 
update as_prixproduit set MONTANT = 10000 where produit like 'PRDTMP000017';

--poulet gasy demi
update as_prixproduit set MONTANT = 16000 where produit like 'ASP000139';

--poulet gasy entier
update as_prixproduit set MONTANT = 28000 where produit like 'PRDTMP000018';

--steack
update as_prixproduit set MONTANT = 13000 where produit like 'PRDTMP000024';

--steack du chef
update as_prixproduit set MONTANT = 15000 where produit like 'ASP000203';

--cote pannée
update as_prixproduit set MONTANT = 12000 where produit like 'PRDTMP000026';

--brohette de zebu
update as_prixproduit set MONTANT = 12000 where produit like 'PRDTMP000025';

--saucisse sambaina
update as_prixproduit set MONTANT = 8000 where produit like 'APS000242';

--saucisse fromage
update as_prixproduit set MONTANT = 10000 where produit like 'ASP000247';

--saucisse tsatsiou
update as_prixproduit set MONTANT = 12000 where produit like 'APS000248';

--saucisse crevette
update as_prixproduit set MONTANT = 10000 where produit like 'APS000241';

--assiette de tsasiou
update as_prixproduit set MONTANT = 11000 where produit like 'PRDTMP000041';

--salade sofikisoa 
update as_prixproduit set MONTANT = 8000 where produit like 'APS000272';

--legumes sautes
update as_prixproduit set MONTANT = 5000 where produit like 'PRDTMP000028';

--oignons panes
update as_prixproduit set MONTANT = 6000 where produit like 'PRDTMP000035';

--nem porc
update as_prixproduit set MONTANT = 9000 where produit like 'PRDTMP000037';

--nem fruit de mer
update as_prixproduit set MONTANT = 9000 where produit like 'PRDTMP000036';

--rouleaux de printemps
update as_prixproduit set MONTANT = 10000 where produit like 'PRDTMP000038';

--van tan frite
update as_prixproduit set MONTANT = 7000 where produit like 'ASP000207';

--cuisse de nymphe
update as_prixproduit set MONTANT = 25000 where produit like 'PRDTMP000040';

--saucisse chinoise
update as_prixproduit set MONTANT = 12000 where produit like 'ASP000140';

--assiette de frite
update as_prixproduit set MONTANT = 6000 where produit like 'PRDTMP000029';

--salade exotique
update as_prixproduit set MONTANT = 8000 where produit like 'ASP000146';

--riz contonais
update as_prixproduit set MONTANT = 12000 where produit like 'PRDTMP000032';

--bol reverse
update as_prixproduit set MONTANT = 14000 where produit like 'PRDTMP000033';

--crepe sale 
update as_prixproduit set MONTANT = 11000 where produit like 'ASP000162';

--crepe sale(tsatsiou et poulet)
update as_prixproduit set MONTANT = 11000 where produit like 'ASP000163';





































