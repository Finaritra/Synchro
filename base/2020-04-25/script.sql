CREATE OR REPLACE FORCE VIEW LIVRAISONLIB ("ID", "DATY", "HEURE", "IDCOMMNADEMERE", "IDPOINT", "ADRESSE", "IDLIVREUR", "ETAT") AS 
select
max(liv.id),
max(liv.daty),
max(liv.heure),
liv.idcommnademere,
max(pt.VAL) as idpoint,
max(liv.adresse),
max(l.nom||'--'|| l.contact) as idlivreur,
max(liv.etat)
from livraison liv
join AS_DETAILSCOMMANDE dc on liv.idcommnademere = dc.ID
join POINT pt on pt.ID=liv.idpoint
join livreur l on liv.idlivreur = l.id group by liv.idcommnademere;



CREATE OR REPLACE VIEW INVENTAIREDETAILSLIBINVENT
(ID, IDMERE, IDINGREDIENT, IDUSER, QUANTITER, 
 EXPLICATION, ETAT, IDCATEGORIEINGREDIENT, DATY, IDING, 
 UNITE)
AS 
select
            invdet.id,
           invDet.idmere,
           ing.LIBELLE as idingredient,
           invDet.idUser,
           invDet.quantiter,
            im.depot as explication,
           im.etat,
           catIng.VAL as idcategorieingredient,
           im.DATY,
           invDet.IDINGREDIENT,
           uni.val as unite
        from inventairedetails invDet
        left join AS_INGREDIENTS ing on ing.id=invDet.idingredient
        left join CATEGORIEINGREDIENT catIng on catIng.ID=invDet.IDCATEGORIEINGREDIENT
        join inventaireMere im on invDet.idmere=im.id
        join as_unite uni on ing.unite=uni.id
        where im.etat>=11
/

insert into usermenu values('USMINV0021','1078','ASINV0803','drl',null,null,null);


CREATE OR REPLACE VIEW INVENTAIREDETAILSLIB
(ID, IDMERE, IDINGREDIENT, IDUSER, QUANTITER, 
 EXPLICATION, ETAT, IDCATEGORIEINGREDIENT, DATY)
AS 
select
            invdet.id,
           invDet.idmere,
           ing.LIBELLE as idingredient,
           invDet.idUser,
           invDet.quantiter,
            invDet.idingredient as explication,
           im.etat,
           catIng.VAL as idcategorieingredient,
           im.DATY

        from inventairedetails invDet
        left join AS_INGREDIENTS ing on ing.id=invDet.idingredient
        left join CATEGORIEINGREDIENT catIng on catIng.ID=invDet.IDCATEGORIEINGREDIENT
        join inventaireMere im on invDet.idmere=im.id
        where im.etat>=1
/