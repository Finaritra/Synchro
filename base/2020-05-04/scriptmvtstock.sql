
  CREATE OR REPLACE FORCE VIEW "AS_ETATSTOCK1" AS 
  select igdr.id as ingredients, igdr.unite as unite, cast(nvl(tab.entree,0) as NUMBER(10,2) )as entree, cast(nvl(tab.sortie, 0)as NUMBER(10,2) ) as sortie,
CAST(nvl(reste, 0)as NUMBER(10,2) ) as reste, cast(nvl(report, 0)as NUMBER(10,2) ) as report, nvl(tab.daty, (select current_date from dual)) as daty,tab.point,'-' as idcategorieingredient
from as_ingredients igdr,
(select as_mvt_stock_fille.ingredients as ingredients,  cast(sum(entree) as number(20,2)) as entree,
cast(sum(sortie) as number(20,2)) as sortie, cast((sum(entree)-sum(sortie)) as number(20,2)) as reste,
cast(as_mvt_stock_fille.stock_init as number(20,2)) as report, point.val as point,as_mvt_stock.daty from as_mvt_stock_fille
join as_mvt_stock on as_mvt_stock_fille.idmvtstock = as_mvt_stock.id
left join point on as_mvt_stock.depot=point.id
group by as_mvt_stock.daty, as_mvt_stock_fille.ingredients,point.val,as_mvt_stock_fille.stock_init) tab
where tab.ingredients (+)=igdr.id;

 CREATE OR REPLACE VIEW "AS_ETATSTOCKVIDE" AS 
select 
'-' as ingredients,
'-' as unite,
CAST(0 as NUMBER(10,2)) as entree,
CAST(0 as NUMBER(10,2)) as sortie,
CAST(0 as NUMBER(10,2)) as reste,
CAST(0 as NUMBER(10,2)) as report,
'' as daty,
'' as idcategorieingredient,
'-' as point from dual;