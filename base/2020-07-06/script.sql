create  view as_bondelivrasion_fille_frs as
    select
            blfd.*,
            fp1.id as idfournisseur,
            fp1.NOM as nomfournisseur
        from AS_BONDELIVRAISON_FILLE_DATE blfd
        join DETAILSFACTUREFOURNISSEUR ff on ff.id=blfd.IDDETAILSFACTUREFOURNISSEUR
        left join FACTUREFOURNISSEUR fp on fp.id=ff.IDMERE
        left join FOURNISSEURPRODUITS fp1 on fp1.id=fp.IDFOURNISSEUR;


create or replace view as_recettebesoincompose_lib as
select rec.*,ing."UNITE",
ing.LIBELLE as libelleingredient,
       prod.libelleproduit,
un.id as idunite,
un.val as valunite,
ing.pu
from AS_RECETTEBESOINCOMPOSE rec
join as_ingredients ing on
ing.id=rec.idingredients
left join as_unite un on
un.id=ing.unite
join as_ing_prod prod on prod.id = rec.IDPRODUITS
order by rec.id asc;
