CREATE or replace VIEW as_produittype_libelle AS 
	select prd.*,ingr.ingredients from
	  ( select
			prd.id, prd.nom, prd.designation, prd.photo, prd.typeproduit as idtypeproduit, prd.idsouscategorie, tpd.val as typeproduit, prd.calorie, prd.pa, prd.poids, sum(trf.montant) keep (dense_rank first order by trf.dateapplication desc) as pu
		  from
			  as_produits prd,
			  as_typeproduit tpd,
			  as_prixproduit trf
		  where
			  prd.typeproduit = tpd.id
			  and trf.produit = prd.id
		  group by prd.id, prd.nom, prd.designation, prd.photo, prd.typeproduit, prd.idsouscategorie, tpd.val, prd.calorie, prd.pa, prd.poids) prd,
	  ( select 
			prd.id as idproduit,LISTAGG(ingr.libelle, ';') WITHIN GROUP (ORDER BY ingr.libelle) as ingredients
		  from
			  as_produits prd,
			  as_recette rct,
			  as_ingredients ingr
		  where
			  rct.idproduits = prd.id
			  and rct.idingredients = ingr.id
		  group by prd.id) ingr
	  where
		prd.id = ingr.idproduit