create table typeInventaire(
    id varchar2(50),
    val varchar2(500),
    desce varchar2(500),
    constraint PK_TYPEINVENTAIRE primary key (id)
);

CREATE SEQUENCE seqtypeInventaire
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 30
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getseqtypeInventaire
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqtypeInventaire.nextval INTO retour FROM dual;
  return retour;
END;

create or replace view inventairedetailslibcomplet as
    select
        idet.id,
        idet.idmere,
        idet.idingredient,
        idet.iduser,
        idet.quantiter,
        idet.explication,
        idet.etat as etatinventairedetails,
        ip.libelle as libelleingredient,
        u.*
    from inventairedetails idet
        join ingredient_produit ip on ip.id=idet.idingredient
        join utilisateur u on u.refuser=idet.iduser;
		
create or replace view inventairedetailslib as
    select
        id,
        idmere,
        libelleingredient as idingredient,
        nomuser as iduser,
        quantiter,
        explication,
        etatinventairedetails as etat
    from inventairedetailslibcomplet;
	
CREATE SEQUENCE seqpropositionachat
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

create or replace FUNCTION getseqpropositionachat
  RETURN NUMBER IS
  retour NUMBER;
BEGIN
  SELECT seqpropositionachat.nextval INTO retour FROM dual;
  return retour;
END;