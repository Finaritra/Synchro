

CREATE OR REPLACE VIEW "AS_ETATSTOCKVIDE" AS 
select 
'-' as ingredients,
'-' as unite,
CAST(0 as NUMBER(10,2)) as entree,
CAST(0 as NUMBER(10,2)) as sortie,
CAST(0 as NUMBER(10,2)) as reste,
CAST(0 as NUMBER(10,2)) as report,
'' as daty,
'' as idcategorieingredient,
'-' as point,
CAST(0 as NUMBER(10,2)) as besoin,
'-' as idfournisseur,
CAST(0 as NUMBER(10,2)) as pu
 from dual;

--napina idingredient
CREATE OR REPLACE VIEW "AS_ETATSTOCKVIDE" AS 
select 
'-' as ingredients,
'-' as unite,
CAST(0 as NUMBER(10,2)) as entree,
CAST(0 as NUMBER(10,2)) as sortie,
CAST(0 as NUMBER(10,2)) as reste,
CAST(0 as NUMBER(10,2)) as report,
'' as daty,
'' as idcategorieingredient,
'-' as point,
CAST(0 as NUMBER(10,2)) as besoin,
'-' as idfournisseur,
CAST(0 as NUMBER(10,2)) as pu,
'-' as idingredient
 from dual;